<?php

$config["firbase_server_key"] = "AAAAbFqwdVY:APA91bG2KhsfQCMNeVbfMO3tIPwJclauPjj_uBCa1pC4Ius6M_UV2hiewMnZtlkFF2z4SpyRLn4XHqTAR-mjK52rZV0DwlbgL3EpCzaLrxvgEEOdHzdM6dWPjUiTawlgqokZYRGbnWzJ";

$config["pickup_timings"] = [
    1 => "Every Monday",
    2 => "Every Tuesday",
    3 => "Every Wednesday",
    4 => "Every Thursday",
    5 => "Every Friday",
    6 => "Every Saturday",
    7 => "Every Sunday",
    8 => "15th of Every Month",
    9 => "1st of Every Month",
];

$config["schedule_emails"] = [
    "testingg37@gmail.com",
    "aftab.schedule@yopmail.com",
    "aftab.l2l@yopmail.com",
    "aftab.test@yopmail.com",
    "aftabafzaal@live.com",
    "aftab.golpik@gmail.com"
];

$config["address_types"] = [
    "home" => [
        "title" => "Home",
        "label" => "Address details",
        "place_holder" => "Add address details (apartment name, number, floor ...)",
        "validation_message" => "Please specify any extra address details",
        "class" => "fas fa-home mr-1",
        "required" => false
    ],
    "office" =>[
        "title" => "Office",
        "label" => "Address details",
        "place_holder" => "Add address details (apartment name, number, floor ...)",
        "validation_message" => "Please specify any extra address details",
        "class" => "fas fa-building mr-1",
        "required" => false
    ],
    "hotel" => [
        "title" => "Hotel",
        "label" => "Your room number",
        "place_holder" => "Enter your hotel room number",
        "validation_message" => "Please enter your hotel room number",
        "class" => "fas fa-hotel mr-1",
        "required" => true
    ],
];


include("params/" . SERVER . ".php");
