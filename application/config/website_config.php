<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// TEXT Values
$config['admin_header_title'] = 'Love2Laundry | Administrator';
$config['website_name'] = 'Love2Laundry';
$config['website_email_sender'] = 'info@love2laundry.com';
$config['front_header_title'] = 'Love2Laundry';

$config['show_date_format'] = 'd-m-Y';
$config['show_date_time_format'] = 'd-m-Y h:i a';
$config['query_date_format'] = 'Y-m-d';
$config['front_asset_image_folder_url'] = PATH . 'assets/images/front/';
$config['front_upload_folder_url'] = PATH . 'uploads/';
$config['front_upload_image_folder_url'] = $config['front_upload_folder_url'] . 'images/';
$config['front_dummy_image_name'] = 'logo.jpg';
$config['front_dummy_image_url'] = $config['front_upload_image_folder_url'] . 'logo.jpg';
$config['front_dummy_mobile_service_image_name'] = 'mobile-logo-placeholder.jpg';
$config['front_dummy_mobile_service_image_url'] = $config['front_upload_image_folder_url'] . 'mobile-logo-placeholder.jpg';
$config['front_dummy_mobile_category_image_name'] = 'mobile-l2l-placeholder.png';
$config['front_dummy_mobile_category_image_url'] = $config['front_upload_image_folder_url'] . 'mobile-l2l-placeholder.png';
$config['front_banner_folder_url'] = $config['front_upload_folder_url'] . 'banners/';
$config['front_widget_folder_url'] = $config['front_upload_folder_url'] . 'widgets/';
$config['front_category_folder_url'] = $config['front_upload_folder_url'] . 'categories/';
$config['front_service_folder_url'] = $config['front_upload_folder_url'] . 'services/';
$config['front_header_sign_in_heading_text'] = 'Sign In';
$config['front_header_sign_up_heading_text'] = 'Sign Up';
$config['front_header_dashboard_heading_text'] = 'Dashboard';
$config['front_header_log_out_heading_text'] = 'Logout';
$config['front_footer_service_menu_heading_text'] = 'Site Links';
$config['front_footer_contact_heading_text'] = 'Contact Us';
$config['front_footer_cleaner_heading_text'] = 'We are Expanding Day By day';
$config['front_footer_cleaner_sub_heading_text'] = 'Love2Laundry UK';
$config['front_footer_site_secure_heading_text'] = 'Site Secured by';
$config['front_footer_secure_payment_heading_text'] = 'Secured Payments by';
$config['front_footer_order_preference_heading_text'] = 'Customer Preferences';
$config['front_home_work_section_heading_text'] = 'Dry Cleaning & Laundry Made Easy';
$config['front_home_mailing_section_heading_text'] = 'Join our mailing list.';
$config['front_home_order_section_content_text'] = 'GET <em>25%</em> OFF ON YOUR FIRST ORDER WITH CODE:';
$config['front_home_price_section_heading_text'] = 'FREE collection & delivery';
$config['front_home_price_section_link_heading_text'] = 'SEE ALL PRICES HERE';
$config['front_home_why_section_heading_text'] = 'Convenient, Professional, Fast';
$config['front_home_about_section_heading_text'] = 'Experience the savvy workmanship of our employees';
$config['front_home_testimonial_section_heading_text'] = 'hear them out';
$config['front_home_testimonial_section_client_heading_text'] = 'Our Clients';
$config['front_home_testimonial_section_link_text'] = 'Say Some thing About Us';
$config['front_member_header_control_heading_text'] = 'Dashboard';
$config['front_member_header_account_heading_text'] = 'Account Setting';
$config['front_member_header_payment_heading_text'] = 'Payment Cards';
$config['front_member_header_laundry_heading_text'] = 'Wash Preferences';
$config['front_member_header_discount_heading_text'] = 'Discount History';
$config['front_member_header_loyalty_heading_text'] = 'Loyalty History';
$config['front_member_dashboard_heading_text'] = 'Dashboard';
$config['front_member_account_heading_text'] = 'Account Setting';
$config['front_member_payment_heading_text'] = 'Payment Cards';
$config['front_member_laundry_heading_text'] = 'Wash Preferences';
$config['front_member_discount_heading_text'] = 'Discount History';
$config['front_member_loyalty_heading_text'] = 'Loyalty History';
$config['front_member_account_first_heading_text'] = 'Personal Information';
$config['front_member_account_second_heading_text'] = 'Address Information';
$config['front_member_account_third_heading_text'] = 'Change Password';
$config['front_member_account_fourth_heading_text'] = 'Security Information';
$config['front_member_laundry_first_heading_text'] = 'Wash and Fold Preferences';
$config['front_member_laundry_second_heading_text'] = 'Dry Cleaning and Laundered Shirt Options';
$config['front_member_discount_top_heading_text'] = 'New Codes';
$config['front_member_discount_bottom_heading_text'] = 'Used Codes';
$config['front_app_widget_heading_text'] = 'Get App Link Directly';
$config['front_app_widget_first_heading_text'] = 'Or Download App Now!';
$config['front_post_code_search_heading_text'] = 'Place order now!';
$config['front_login_page_url'] = 'login';
$config['front_register_page_url'] = 'register';
$config['front_logout_page_url'] = 'logout';
$config['front_dashboard_page_url'] = 'dashboard';
$config['front_cap_image_page_url'] = 'cap-image';
$config['front_term_page_url'] = 't-c';
$config['front_contact_page_url'] = 'contact-us';
$config['front_member_account_page_url'] = 'account-setting';
$config['front_member_payment_page_url'] = 'payment-cards';
$config['front_member_laundry_page_url'] = 'wash-preferences';
$config['front_member_discount_page_url'] = 'discount-history';
$config['front_member_loyalty_page_url'] = 'loyalty-history';
$config['front_member_schedule_order'] = 'schedule/order';
$config['front_member_order_listing_page_url'] = 'order-listing';
$config['front_member_order_detail_page_url'] = 'order-detail';
$config['front_member_order_cancel_page_url'] = 'order-cancel';
$config['front_member_reorder_page_url'] = 'reorder';
$config['front_select_item_page_url'] = 'select-items';
$config['front_time_selection_page_url'] = 'select-times';
$config['front_address_selection_page_url'] = 'address';
$config['front_check_out_page_url'] = 'checkout';
$config['front_payment_page_url'] = 'payment';
$config['front_thank_you_page_url'] = 'thank-you';
$config['front_contact_thank_you_page_url'] = 'contact-thank-you';
$config['front_post_code_not_found_page_url'] = 'post-code-not-found';
$config['front_store_link_generator_page_url'] = 'store-link-generator';
$config['front_airbnb_laundry_service_page_url'] = 'airbnb-laundry-service';
$config['store_link_short_url'] = 'http://goo.gl/aPD6wo';