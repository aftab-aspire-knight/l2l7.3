<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'home';
$route['booking'] = "booking/booking";
$route['validatestep1'] = "booking/validatestep1";
$route['order-items'] = "booking/orderitems";
$route['addremoveitems'] = "booking/addremoveitems";
$route['bookingrightview'] = "booking/bookingrightview";
$route['booking/register'] = "booking/register";
$route['booking/login'] = "booking/login";
$route['booking/forgotpassword'] = "booking/forgotpassword";
$route['booking/storepreferences'] = "booking/storepreferences";
$route['booking/savecard'] = "booking/savecard";
$route['booking/confirmorder'] = "booking/confirmorder";
$route['thank-you'] = "booking/thankyou";
$route['booking/applydiscountcode'] = "booking/applydiscountcode";
$route['booking/savetimings'] = "booking/savetimings";
$route['booking/countservices'] = "booking/countservices";
$route['booking/removecartservice'] = "booking/removecartservice";
$route['booking/notifyneworder'] = "booking/notifyneworder";
$route['addresses/google'] = "addresses/google";

$route['install-app'] = "installapp/index";

$route['pricing'] = "prices/index";
$route['showprices'] = "prices/showprices";
$route['prices/continueorder'] = "prices/continueorder";

$route['orders/edit'] = "orders/edit";
$route['orders/savetimings'] = "orders/savetimings";
$route['edit-order-items'] = "orders/items";
$route['orders/addremoveitems'] = "orders/addremoveitems";
$route['orders/update'] = "orders/update";
$route['orders/listing'] = "orders/listing";

$route['schedules'] = "schedule/index";
$route['schedule/confirmorder'] = "schedule/confirmorder";
$route['schedule/updatephone'] = "schedule/updatephone";
$route['schedule/updatephone'] = "schedule/updatephone";

$route['social/login'] = "social/login";
$route['messages/abandon-cart'] = "messages/abandon_cart";

$route['404_override'] = 'home/page';
$route['admin'] = "admin/login";
$route['offers-dry-cleaning'] = 'landing/dryCleaning';
$route['offers-laundry'] = 'landing/laundry';
$route['offers-ironing'] = 'landing/ironing';
$route['offers-shoe-repair'] = 'landing/shoeRepair';
$route['offers-shirt'] = 'landing/shirt';
$route['offers-alteration'] = 'landing/alteration';
$route['stayalert20'] = 'landing/stayalert20';
$route['translate_uri_dashes'] = FALSE;




include("routes/" . SERVER . ".php");
