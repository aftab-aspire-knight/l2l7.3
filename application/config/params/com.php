<?php

$config['website_email_sender'] = 'info@love2laundry.com';

$config['currency'] = 'pound sterling';
$config['currency_symbol'] = '£';
$config['intl_tel'] = 'gb';
$config['currency_to'] = 'gbp';
$config['currency_code'] = 'gbp';
$config['currency_sign'] = '&pound;';
$config['iso_code'] = 'uk';
$config['iso_country_code'] = 'gb';

$config['country_phone_code'] = '44';
$config['sms_service'] = true;
$config['phone_number_length'] = 10;
$config['sms_api_username'] = "nadeem@love2laundry.com";
$config['sms_api_password'] = "2u5VYeZALmQs";
$config['include_vat'] = true;

$config['phone_placeholder'] = '79xxxxxxxx';
$config['post_code_length'] = 5;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "55.3781,3.4360";
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['recaptcha_copy_key'] = '6Le8ddkZAAAAAN7u-vRgqGgLky_YoTiBENC6LTSS';
$config['recaptcha_secret_key'] = '6Le8ddkZAAAAANOOzSbB4ZN-mHn8VKIG1cIRodO-';
$config['version'] = 1.0;
$config['refer_friend_discount'] = "5.00";
$config['standard_wash'] = "2.50";
$config['discount_off'] = "25%";
$config['has_schedule'] = true;

$config['minimum_order_value'] = "25.00";

$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-1";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "51.4996389";
$config['laundry_longitude'] = "-0.0529017";

$config['north'] = "51.4996389";
$config['west'] = "-0.0529017";
$config['south'] = "51.4996389";
$config['east'] = "-0.0529017";

$config['addressian_api_key'] = "Cj6Dv0n0DR9XykzBaLpYo1oTijnK6qR255Q7E4UR";

$config['postal_code_type'] = array("title" => "Postal Code", 
"placeholder" => "E.G. E12 PE", 
"validation_message" => "Please enter your full postcode e.g. E12PE",
"no_service_message" => "Oops, we are not there yet, will be there soon.",
 "label" => "POSTCODE"
);

$config['default_postal_code'] = "E12PE";
$config['default_franchise_id'] = 1;
$config['tookan_timezone'] = "-60";
//$config['tookan_timezone'] = "0";
$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'UK';
$config['onfleet_enabled'] = false;

$config['prices_popup'] = true;

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['prices_popup'] = true;
$config['show_prices_pdf'] = true;

$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
$config['payment_methods'][] = array("code" => "stripe_checkout", "title" => "checkout with stripe", "description" => "Klarna description");
//$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
