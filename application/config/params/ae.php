<?php

date_default_timezone_set("Asia/Dubai");

$config['website_email_sender'] = 'info@love2laundry.ae';

$config['dir_assets'] = $_SERVER['DOCUMENT_ROOT'] . '/assets/';
$config['dir_uploads'] = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';

$config['currency'] = 'UAE';
$config['currency_to'] = 'aed';
$config['currency_symbol'] = 'AED';
$config['intl_tel'] = 'ae';
$config['currency_code'] = 'aed';
$config['currency_sign'] = 'AED';
$config['currency_sign_db'] = 'AED';
$config['iso_code'] = 'ae';
$config['iso_country_code'] = 'ae';
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['country_phone_code'] = '971';
$config['sms_service'] = true;
$config['phone_number_length'] = 9;

$config['sms_api_username'] = "p3hf8k67";
$config['sms_api_password'] = "RU983stb";
$config['include_vat'] = false;

$config['phone_placeholder'] = '508xxxxxx';
$config['post_code_length'] = 4;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "55.3781,3.4360";
$config['recaptcha_copy_key'] = '6Le8ddkZAAAAAN7u-vRgqGgLky_YoTiBENC6LTSS';
$config['recaptcha_secret_key'] = '6Le8ddkZAAAAANOOzSbB4ZN-mHn8VKIG1cIRodO-';
$config['version'] = 1.0;
$config['refer_friend_discount'] = "20.00";
$config['standard_wash'] = "60.00";
$config['discount_off'] = "AED40";
$config['has_schedule'] = false;

$config['minimum_order_value'] = "45.00";

$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-4";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "25.1972";
$config['laundry_longitude'] = "55.2744";


$config['north'] = "25.1972";
$config['west'] = "51.2744";
$config['south'] = "25.1990";
$config['east'] = "55.2755";



$config['postal_code_type'] = array("title" => "Area", 
"placeholder" => "Start with your street...", 
"validation_message" => "Oops, we are not there yet, will be there soon.", 
"no_service_message" => "Oops, we are not there yet, will be there soon.",
"label" => "Search for your area to see collections in your area");

$config['default_postal_code'] = "dubai";
$config['default_franchise_id'] = 1;

$config['tookan_timezone'] = "-240";
$config['tookan_key'] = "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94";
$config['tookan_team_id'] = "21339";

$config['geofence'] = 0;

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['show_prices_pdf'] = true;
$config['prices_popup'] = false;


$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'AE';
$config['onfleet_enabled'] = false;

$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
// $config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
