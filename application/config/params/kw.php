<?php

date_default_timezone_set("Asia/Dubai");

$config['website_email_sender'] = 'info@love2laundry.ae';
$config['currency'] = 'Kuwaiti dinar';
$config['currency_symbol'] = 'KWD';
$config['currency_to'] = 'gbp';
$config['intl_tel'] = 'kwd';
$config['currency_code'] = 'kwd';
$config['currency_sign'] = 'KWD';
$config['currency_sign_db'] = 'KWD';
$config['iso_code'] = 'kwt';
$config['iso_country_code'] = 'KW';
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['country_phone_code'] = '965';
$config['sms_service'] = false;
$config['phone_number_length'] = 8;
$config['phone_placeholder'] = '98xxxxxx';
$config['post_code_length'] = 4;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "29.3117,47.4818";

$config['sms_api_username'] = "nadeem@love2laundry.com";
$config['sms_api_password'] = "xjkdw";
$config['include_vat'] = false;


$config['recaptcha_copy_key'] = '6Le8ddkZAAAAAN7u-vRgqGgLky_YoTiBENC6LTSS';
$config['recaptcha_secret_key'] = '6Le8ddkZAAAAANOOzSbB4ZN-mHn8VKIG1cIRodO-';
$config['version'] = 1.0;
$config['refer_friend_discount'] = "1.00";
$config['standard_wash'] = "4.00";
$config['discount_off'] = "50%";
$config['has_schedule'] = false;

$config['standard_wash_currency'] = "KWD";
$config['minimum_order_value'] = 4.00;
$config['corporate_widget_first_title'] = "Hotel";
$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-3";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "29.3117";
$config['laundry_longitude'] = "47.4818";


$config['north'] = "29.3117";
$config['west'] = "47.4818";
$config['south'] = "29.3117";
$config['east'] = "48.4818";

$config['postal_code_type'] = array(
    "title" => "Area",
    "placeholder" => "Start with your street...",
    "validation_message" => "Oops, we are not there yet, will be there soon.",
    "no_service_message" => "Oops, we are not there yet, will be there soon.",
    "label" => "Search for your area to see collections in your area"
);

$config['default_postal_code'] = "Kuwait";
$config['default_franchise_id'] = 1;
$config['tookan_timezone'] = "-180";
$config['geofence'] = 0;

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['prices_popup'] = true;
$config['show_prices_pdf'] = true;

$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'NL';

$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
//$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");