<?php

$config['website_email_sender'] = 'info@love2laundry.com';

$config['currency'] = 'Euro';
$config['currency_symbol'] = '€';
$config['currency_to'] = 'eur';
$config['currency_code'] = 'eur';
$config['currency_sign'] = '&euro;';

$config['phone_number_length'] = 9;
$config['currency_sign_payment'] = 'EUR';
$config['currency_sign_db'] = 'EUR';
$config['iso_code'] = 'nl';
$config['iso_country_code'] = 'nl';
$config['intl_tel'] = 'nl';
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['country_phone_code'] = '31';
$config['phone_placeholder'] = '612345678';
$config['sms_service'] = true;
$config['post_code_length'] = 6;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "52.3676,4.9041";
$config['include_vat'] = false;

$config['sms_api_username'] = "nadeem@love2laundry.com";
$config['sms_api_password'] = "2u5VYeZALmQs";

$config['recaptcha_copy_key'] = '6Le8ddkZAAAAAN7u-vRgqGgLky_YoTiBENC6LTSS';
$config['recaptcha_secret_key'] = '6Le8ddkZAAAAANOOzSbB4ZN-mHn8VKIG1cIRodO-';
$config['version'] = 1.0;
$config['refer_friend_discount'] = "10.00";
$config['standard_wash'] = "21.00";
$config['discount_off'] = "€10.00";
$config['has_schedule'] = false;

$config['standard_wash_currency'] = "€";
$config['minimum_order_value'] = "45.00";

$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-7";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "52.3676";
$config['laundry_longitude'] = "4.9041";

$config['postal_code_type'] = array("title" => "Postal Code", "placeholder" => "E.G. 1043AJ", "validation_message" => "Please enter your full postcode for e.g. 1043AJ", "label" => "POSTCODE");

$config['default_postal_code'] = "1043AJ";
$config['default_franchise_id'] = 2;
$config['tookan_timezone'] = "0";

$config['geofence'] = 0;

$config['north'] = "52.3676";
$config['west'] = "4.9041";
$config['south'] = "52.3990";
$config['east'] = "52.3677";


$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['prices_popup'] = true;
$config['show_prices_pdf'] = true;


$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'NL';
$config['onfleet_enabled'] = false;


$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
//$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
?>