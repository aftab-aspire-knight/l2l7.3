<?php

date_default_timezone_set("Asia/Karachi");

$config['website_email_sender'] = 'info@love2laundry.com';

$config['dir_assets'] = $_SERVER['DOCUMENT_ROOT'] . '/assets/';
$config['dir_uploads'] = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';

$config['currency'] = 'PKR';
$config['currency_to'] = 'pkr';
$config['currency_symbol'] = 'Rs.';
$config['intl_tel'] = 'pk';
$config['currency_code'] = 'pkr';
$config['currency_sign'] = 'PKR';
$config['currency_sign_db'] = 'PKR';
$config['iso_code'] = 'pk';
$config['iso_country_code'] = 'pk';
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['country_phone_code'] = '92';
$config['sms_service'] = true;
$config['phone_number_length'] = 10;

$config['sms_api_username'] = "p3hf8k67";
$config['sms_api_password'] = "RU983stb";
$config['include_vat'] = false;

$config['phone_placeholder'] = '03337*****';
$config['post_code_length'] = 4;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "24.8607,67.0011";
$config['recaptcha_copy_key'] = '6Le8ddkZAAAAAN7u-vRgqGgLky_YoTiBENC6LTSS';
$config['recaptcha_secret_key'] = '6Le8ddkZAAAAANOOzSbB4ZN-mHn8VKIG1cIRodO-';
$config['version'] = 1.0;
$config['refer_friend_discount'] = "100.00";
$config['standard_wash'] = "600.00";
$config['discount_off'] = "PKR200";
$config['has_schedule'] = false;

$config['minimum_order_value'] = "500.00";

$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-9";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "31.3665409";
$config['laundry_longitude'] = "74.1772193";

$config['north'] = "31.3665409";
$config['west'] = "74.1772193";
$config['south'] = "31.3360073";
$config['east'] = "74.1727594";

$config['postal_code_type'] = array(
    "title" => "Area", 
    "placeholder" => "Start with your street...", 
    "validation_message" => "Oops, we are not there yet, will be there soon.",
    "no_service_message" => "Oops, we are not there yet, will be there soon.",
    "label" => "Search for your area to see collections in your area"
);

$config['default_postal_code'] = "lahore";
$config['default_franchise_id'] = 1;

$config['tookan_timezone'] = "-240";
$config['tookan_key'] = "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94";
$config['tookan_team_id'] = "21339";

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['geofence'] = 0;

$config['prices_popup'] = true;
$config['show_prices_pdf'] = true;

$config['prices_popup'] = true;

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'PK';

$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
