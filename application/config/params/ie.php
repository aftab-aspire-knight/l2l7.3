<?php


$config['website_email_sender'] = 'info@love2laundry.com';

$config['currency'] = 'Euro';
$config['currency_symbol'] = '€';
$config['currency_to'] = 'eur';
$config['currency_code'] = 'eur';
$config['currency_sign'] = '&euro;';

$config['currency_sign_payment'] = 'EUR';
$config['currency_sign_db'] = 'EUR';
$config['iso_code'] = 'ie';
$config['iso_country_code'] = 'ie';
$config['intl_tel'] = 'irl';
$config['loqate_key'] = 'BA23-YX42-UF92-GZ81'; //live
$config['country_phone_code'] = '353';
$config['sms_service'] = true;
$config['phone_number_length'] = 9;
$config['phone_placeholder'] = '68xxxxxxx';
$config['post_code_length'] = 6;
$config['loqate_api_url'] = "https://api.addressy.com/";
$config['loqate_origin'] = "52.377956,4.897070";
$config['sms_api_username'] = "nadeem@love2laundry.com";
$config['sms_api_password'] = "xjkdw";
$config['include_vat'] = false;
$config['has_schedule'] = false;

$config['recaptcha_copy_key'] = '6LetfMAZAAAAAC8xhrUbEjk2tsNVrou5kPUqmuzY';
$config['recaptcha_secret_key'] = '6LetfMAZAAAAALfflho5svkXsGY-3kbGk5Yqp737';

$config['addressian_api_key'] = "Cj6Dv0n0DR9XykzBaLpYo1oTijnK6qR255Q7E4UR";

$config['version'] = 1.0;
$config['refer_friend_discount'] = "10.00";
$config['standard_wash'] = "17.50";
$config['discount_off'] = "€10.00";
$config['standard_wash_currency'] = "€";


$config['minimum_order_value'] = "25.00";

$config['google_map_api_key'] = "AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY";
$config['google_client_id'] = "465377981782-b2qdnlf5bku573c3666v50d5605l748a.apps.googleusercontent.com";
$config['google_analytics_id'] = "UA-54717435-1";
/////love2laundry@gmail.com
$config['facebook_app_id'] = "2245234445609869";

$config['laundry_latitude'] = "51.4996389";
$config['laundry_longitude'] = "-0.0529017";

$config['north'] = "51.4996389";
$config['west'] = "-0.0529017";
$config['south'] = "51.4996389";
$config['east'] = "-0.0529017";

$config['postal_code_type'] = array(
    "title" => "Eir Code",
    "placeholder" => "E.G. D22 YD82",
    "validation_message" => "Please enter your full eircode for e.g. D22 YD82",
    "no_service_message" => "Oops, we are not there yet, will be there soon.",
    "label" => "EIRCODE"
);

$config['default_postal_code'] = "D22YD82";
$config['default_franchise_id'] = 1;
$config['tookan_timezone'] = "0";
$config['geofence'] = 0;



$config['onfleet_api_key'] = '922a20c61dceba683ff8eed126b824ba';
$config['onfleet_app_name'] = 'Love2Laundry Live';
$config['onfleet_country'] = 'NL';

$config['tookan'] = [
    "api_key" => "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94",
    "team_id" => "21339",
    "auto_assignment" => 0,
    "has_pickup" => "1",
    "has_delivery" => "1",
    "layout_type" => "0",
    'tracking_link' => "1",
    'geofence' => 0,
    'timezone' => "-60"
];

$config['prices_popup'] = true;
$config['show_prices_pdf'] = true;

//$config['payment_methods'][] = array("code" => "stripe", "title" => "Credit Card", "description" => "Credit Card description");
$config['payment_methods'][] = array("code" => "stripe_checkout", "title" => "checkout with stripe", "description" => "Klarna description");
//$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
//$config['payment_methods'][] = array("code" => "cash", "title" => "Cash on delivery", "description" => "Cash on delivery description");
