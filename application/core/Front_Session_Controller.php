<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_Session_Controller extends CI_Controller {

    protected $database;
    protected $country;

    public function __construct() {
        parent::__construct();


        $library = SERVER . "library";
        $this->load->library($library);
        $class = ucfirst($library);
        $this->country = new $class();

        $this->output->enable_profiler(FALSE);
        $this->load->model('model_database');
        $this->tbl_settings = "cms_settings";
        $this->config->load('params');
        if ($this->GetWebsiteStatus() != "Online") {
            echo "<h1>" . $this->GetWebsiteName() . " is offline for maintenance.</h1>";
            echo '<p>We are currently updating our website.Please check back in few hours.</p>';
            die();
        }
        $this->load->library('emailsender');
        $this->load->library('simplecaptcha');
        $this->load->library('facebook', array('appId' => '1141754259209609', 'secret' => 'fdc3ac8f9e0a490f293f0369c2dc6d07'));
        $this->load->helper('cookie');
        $this->tbl_payment_informations = "ws_payment_informations";
        $this->tbl_payment_informations_testing = "ws_payment_informations_testing";
        $this->tbl_user_menus = "cms_admin_menus";
        $this->tbl_email_responders = "cms_email_responders";
        $this->tbl_sms_responders = "cms_sms_responders";
        $this->tbl_tags = "cms_tags";
        $this->tbl_menus = "cms_menus";
        $this->tbl_navigations = "cms_navigations";
        $this->tbl_navigation_sections = "cms_navigation_sections";
        $this->tbl_pages = "cms_pages";
        $this->tbl_page_areas = "cms_page_areas";
        $this->tbl_page_area_sections = "cms_page_area_sections";
        $this->tbl_users = "cms_users";
        $this->tbl_cms_cron = "cms_cron";
        $this->tbl_widgets = "cms_widgets";
        $this->tbl_widget_navigations = "cms_widget_navigation_sections";
        $this->tbl_widget_sections = "cms_widget_sections";
        $this->tbl_banners = "ws_banners";
        $this->tbl_categories = "ws_categories";
        $this->tbl_discounts = "ws_discounts";
        $this->tbl_invoices = "ws_invoices";
        $this->tbl_invoice_preferences = "ws_invoice_preferences";
        $this->tbl_invoice_services = "ws_invoice_services";
        $this->tbl_invoice_payment_informations = "ws_invoice_payment_informations";
        $this->tbl_invoices_testing = "ws_invoices_testing";
        $this->tbl_invoice_preferences_testing = "ws_invoice_preferences_testing";
        $this->tbl_invoice_services_testing = "ws_invoice_services_testing";
        $this->tbl_invoice_payment_informations_testing = "ws_invoice_payment_informations_testing";
        $this->tbl_locations = "ws_locations";
        $this->tbl_lockers = "ws_lockers";
        $this->tbl_preferences = "ws_preferences";
        $this->tbl_services = "ws_services";
        $this->tbl_testimonials = "ws_testimonials";
        $this->tbl_members = "ws_members";
        $this->tbl_member_cards = "ws_member_cards";
        $this->tbl_member_preferences = "ws_member_preferences";
        $this->tbl_websites = "ws_websites";
        $this->tbl_disable_date_slots = "ws_disable_date_slots";
        $this->tbl_disable_time_slots = "ws_disable_time_slots";
        $this->tbl_franchises = "ws_franchises";
        /* Hassan changes 23-01-2018 */
        $this->tbl_franchise_timings = "ws_franchise_timings";
        /* ends */
        $this->tbl_franchise_post_codes = "ws_franchise_post_codes";
        $this->tbl_franchise_services = "ws_franchise_services";

        $this->tbl_subscribers = "ws_subscribers";
        $this->tbl_loyalty_points = "ws_loyalty_points";
        $this->tbl_loyalty_points_testing = "ws_loyalty_points_testing";
        $this->tbl_search_post_codes = "ws_search_post_codes";
        $this->tbl_sms_logs = "ws_sms_logs";
        $this->tbl_devices = "ws_devices";
        $this->tbl_devices_new = "ws_devices_new";
        $this->tbl_device_login_informations = "ws_device_login_informations";
        $this->header_menu_section_id = 1;
        $this->footer_menu_section_id = 2;
        $this->cleaner_menu_section_id = 3;
        $this->services_menu_section_id = 4;
        $this->email_header_menu_section_id = 5;
        $this->email_footer_menu_section_id = 6;
        $this->home_work_widget_section_id = 1;
        $this->home_video_widget_id = 4;
        $this->home_price_widget_id = 10;
        $this->home_price_widget_section_id = 3;
        $this->home_why_widget_section_id = 2;
        $this->home_about_widget_id = 9;
        $this->login_cookie_name = "love_2_laundry_cookie_record";
        $this->session_post_code = "post_code_filter";
        $this->pickup_date = "pickup_date";
        $this->pickup_time = "pickup_time";
        $this->delivery_date = "delivery_date";
        $this->delivery_time = "delivery_time";
        $this->postal_code = "postal_code";
        $this->address1 = "address1";
        $this->address2 = "address2";
        $this->town = "town";
        $this->franchise_id = "franchise_id";

        $this->session_franchise_record = "franchise_post_code_filter";
        $this->sms_api_username = "nadeem@love2laundry.com";
        $this->sms_api_password = "xjkdw";

        $this->edit_order_hour_limit = 3;
    }

    public function get_curl_request($url) {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

    public function post_stripe_curl_request($url, $param_array, $is_test = false) {
        try {
            //sk_test_7enLa8QuWrL8ZMO3nEL8ADnZ
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            if ($is_test) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer sk_test_7enLa8QuWrL8ZMO3nEL8ADnZ'));
            } else {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Authorization: Bearer sk_live_Usg6A1wTW1ss9iXwIAraY4mY'));
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch, APPPATH.'config/cacert.pem');
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param_array));
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

    public function post_curl_request($param_array) {
        try {
            $header = array();
            $header[] = 'Content-Type: application/json';
            $header[] = 'Content-Length: ' . strlen($param_array);
            $header[] = 'Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODFjODNhZTFmYTBlMTU3MjhhM2QyMDgiLCJpYXQiOjE0NzgyNjM3MjZ9.6DayeSvoYeqZd4ek3gevA9QHS-BHZ-5XHVKErzU6M-s';
            $ch = curl_init("https://api.routific.com/v1/vrp");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param_array);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

    //Teamid 21339
    public function tookan_api_post_request($url, $param_array = array()) {
        try {
            $header = array();
            $header[] = 'Content-Type: application/json';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch, APPPATH.'config/cacert.pem');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param_array);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            echo $e->getMessage();
            return null;
        }
    }

    public function verifyGoogleCaptcha($token) {

        $url = "https://www.google.com/recaptcha/api/siteverify";
        $data["secret"] = $this->config->item("recaptcha_secret_key");
        $data["response"] = $token;
        $data["remoteip"] = $_SERVER["REMOTE_ADDR"];

        // call curl to POST request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $arrResponse = json_decode($response, true);
        return $arrResponse;
    }

    public function SendMessage($responder_id, $invoice_number, $title, $number, $message) {

        $sms_service = $this->config->item("sms_service");
        $serviceEnabled = isset($sms_service) ? $sms_service : false;
        //$response["result"]="error";
        if ($serviceEnabled == true) {
            $this->country->SendMessage($responder_id, $invoice_number, $title, $number, $message, $this->input->ip_address(), str_replace(" ", "", str_replace("Love2", "Lov2", $this->GetWebsiteName())));
        }
    }

    public function verifyPhoneNumber($countryCode = "", $phone) {
        try {


            $length = $this->config->item('phone_number_length');
            //$this->load->library('loqate');

            $phone = ltrim($phone, "+");
            $phone = ltrim($phone, "0");

            $countryCode = ltrim($countryCode, "+");
            $countryCode = ltrim($countryCode, "0");

            //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));

            /*
              if (!$loqate->validatePhone($countryCode . $phone)) {
              return false;
              }
             */


            if (!is_numeric($phone)) {
                return false;
            }

            if (strlen($phone) < 6 || strlen($phone) >= 13) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function MaskedInput($value, $length) {
        $masked = str_pad(substr($value, $length), strlen($value), 'X', STR_PAD_LEFT);
        return $masked;
    }

    public function GetDistanceMatrixFromAPI($origin, $destination) {
        $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($origin) . "&destinations=" . urlencode($destination) . "&mode=driving&language=en-EN&sensor=false&units=imperial&key=" . $this->google_map_api_key;
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $miles = 0;
        if (isset($result['rows'][0]['elements'][0]['status'])) {
            if (strtolower($result['rows'][0]['elements'][0]['status']) == "ok") {
                $miles_value = $result['rows'][0]['elements'][0]['distance']['text'];
                $miles_array = explode(" ", $miles_value);
                if ($miles_array[1] != "ft") {
                    $miles = floatval($miles_array[0]);
                }
            }
        }
        return $miles;
    }

    public function GetDirectionContentFromAPI($origin, $destination) {
        $response_result = "";
        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=" . urlencode($origin) . "&destination=" . urlencode($destination) . "&mode=driving&language=en-EN&sensor=false&units=imperial&key=" . $this->google_map_api_key;
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        if (isset($result['status'])) {
            if (strtolower($result['status']) == "ok") {
                $response_result = $result_string;
            }
        }
        return $response_result;
    }

    public function GetFullDayName($id) {
        $day_array = array(
            0 => 'Sunday',
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday'
        );
        return $day_array[$id];
    }

    /* Hassan IsOffDay 13-10-2018 */

    public function IsOffDay($date, $franchise_id) {
        $day_name = $this->GetFullDayName(date('w', strtotime($date)));
        $return_condition = false;
        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID", "PKFranchiseID = " . $franchise_id . " AND OffDays like '%" . $day_name . "%'");
        if ($franchise_record != false) {
            $return_condition = true;
        }
        return $return_condition;
    }

    public function IsDisableDateSlot($date, $search_type, $franchise_id) {
        $return_condition = false;
        $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='All Day' and (Type='Both' or Type='" . $search_type . "')");
        //echo "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='All Day' and (Type='Both' or Type='" . $search_type . "')";
        if ($disable_slot_record != false) {
            $return_condition = true;
        }
        return $return_condition;
    }

    public function IsDisableTimeSlot($date, $search_with, $franchise_id) {
        $date = date($this->config->item('query_date_format'), strtotime($date));
        $return_condition = false;
        $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='All Day' and (Type='Both' or Type='" . $search_with . "')");
        if ($disable_slot_record != false) {
            $return_condition = true;
        }
        return $return_condition;
    }

    public function GetFranchiseDateTimeCollectionUpdated($date, $total_hours, $search_with, $franchise_record, $opening_franchise_time, $franchise_timings_record = array()) {


        $start = date("H", strtotime($franchise_record["OpeningTime"]));

        $end = date("H", strtotime($franchise_record["ClosingTime"]));

        if ($search_with == "Pickup") {
            $DifferenceHour = isset($franchise_record["PickupDifferenceHour"]) ? $franchise_record["PickupDifferenceHour"] : 3;
        } else {
            $DifferenceHour = isset($franchise_record["DeliveryDifferenceHour"]) ? $franchise_record["DeliveryDifferenceHour"] : 3;
        }

        $today_date = date('Y-m-d');
        $is_today_date = false;

        if ($today_date == $date) {
            $is_today_date = true;
            $today_hour_add_3_date_time = date('Y-m-d H:i:s', strtotime('+' . $DifferenceHour . ' hours'));
            $today_hour_add_3_date = date('Y-m-d', strtotime($today_hour_add_3_date_time));
            if ($today_date != $today_hour_add_3_date) {
                return array();
            }
        }



        $franchise_id = 0;
        if (isset($franchise_record['PKFranchiseID'])) {
            $franchise_id = $franchise_record['PKFranchiseID'];
        } else if (isset($franchise_record['ID'])) {
            $franchise_id = $franchise_record['ID'];
        }

        $time_array = array();
        $time_array_others = array();

        $franchise_record["GapTime"] = ($franchise_record["GapTime"] == 0) ? 1 : $franchise_record["GapTime"];


        for ($i = 0; $i < $total_hours; $i += $franchise_record["GapTime"]) {
            $time_class = "Available";
            $hour = (int) date('H', strtotime('+' . $i . ' hours', strtotime($opening_franchise_time)));
            $from = $hour;
            $to = (int) date('H', strtotime('+' . ($i + $franchise_record["GapTime"]) . ' hours', strtotime($opening_franchise_time)));

            if ($to > $end) {
                $to = $end;
            }


            $search_time = sprintf("%02d", $from) . ":00" . '-' . sprintf("%02d", $to) . ":00";
            $current_hour_add_3 = (int) date("H", strtotime('+' . $DifferenceHour . ' hours'));

            if ($is_today_date) {

                if ($hour < $current_hour_add_3) {
                    $time_class = "Disable";
                }
            }




            if ($time_class == "Available") {

                $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='" . trim($search_time) . "' and (Type='Both' or Type='" . $search_with . "')");


                if ($disable_slot_record != false) {
                    $time_class = "Disable";
                }
            }
            if ($time_class == "Available") {
                if ($franchise_record['AllowSameTime'] == "No") {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $date . "' and PickupTime='" . $search_time . "') or (DeliveryDate='" . $date . "' and DeliveryTime='" . $search_time . "')");
                    if ($invoice_record_count > 0) {
                        $time_class = "Disable";
                    }
                } else {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and " . $search_with . "Date='" . $date . "' and " . $search_with . "Time='" . $search_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                    if ($invoice_record_count > 0) {
                        if ($search_with == "Pickup") {
                            if ($invoice_record_count >= $franchise_record['PickupLimit']) {
                                $time_class = "Disable";
                            }
                        } else {
                            if ($invoice_record_count >= $franchise_record['DeliveryLimit']) {
                                $time_class = "Disable";
                            }
                        }
                    }
                }
            }

            $time_array[] = array(
                'Class' => $time_class,
                'Time' => $search_time,
                'Hour' => $hour
            );
            $hours[] = $search_time;
        }
        $val = end($time_array);

        /* Hassan changes 23-01-2018 */


        if (!empty($franchise_timings_record)) {
            $opening_franchise_timeTMPArray = array();
            $closing_franchise_timeTMPArray = array();
            $time_array_others = array();
            foreach ($franchise_timings_record as $key => $val) {
                $PickupLimitTMP = $val['PickupLimit'];
                $DeliveryLimitTMP = $val['DeliveryLimit'];
                $OpeningTimeTMP = $val['OpeningTime'];
                $ClosingTimeTMP = $val['ClosingTime'];
                $GapTimeTMP = ($val["GapTime"] == 0) ? 1 : $val["GapTime"];

                $opening_franchise_timeTMP = $opening_franchise_timeTMPArray[] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $OpeningTimeTMP));
                $closing_franchise_timeTMP = $closing_franchise_timeTMPArray[] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $ClosingTimeTMP));
                $total_hoursTMP = ((strtotime($closing_franchise_timeTMP) - strtotime($opening_franchise_timeTMP)) / ( 60 * 60 ));
                //$total_hours+=$total_hoursTMP;
                for ($i = 0; $i < $total_hoursTMP; $i += $GapTimeTMP) {


                    $time_class = "Available";
                    $hour = (int) date('H', strtotime('+' . $i . ' hours', strtotime($opening_franchise_timeTMP)));
                    $search_time = date('H:00', strtotime('+' . $i . ' hours', strtotime($opening_franchise_timeTMP))) . '-' . date('H:00', strtotime('+' . ($i + $GapTimeTMP) . ' hours', strtotime($opening_franchise_timeTMP)));
                    if ($is_today_date) {
                        $current_hour_add_3 = (int) date("H", strtotime('+' . $DifferenceHour . ' hours'));
                        if ($hour <= $current_hour_add_3) {

                            $time_class = "Disable";
                        }
                    }
                    if ($time_class == "Available") {
                        $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='" . $search_time . "' and (Type='Both' or Type='" . $search_with . "')");
                        if ($disable_slot_record != false) {

                            $time_class = "Disable";
                        }
                    }
                    if ($time_class == "Available") {

                        if ($franchise_record['AllowSameTime'] == "No") {
                            $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $date . "' and PickupTime='" . $search_time . "') or (DeliveryDate='" . $date . "' and DeliveryTime='" . $search_time . "')");
                            if ($invoice_record_count > 0) {

                                $time_class = "Disable";
                            }
                        } else {
                            $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and " . $search_with . "Date='" . $date . "' and " . $search_with . "Time='" . $search_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");

                            if ($invoice_record_count > 0) {
                                if ($search_with == "Pickup") {
                                    if ($invoice_record_count >= $PickupLimitTMP) {
                                        $time_class = "Disable";
                                    }
                                } else {
                                    if ($invoice_record_count >= $DeliveryLimitTMP) {
                                        $time_class = "Disable";
                                    }
                                }
                            }
                        }
                    }

                    $time_array_others[$key][$i] = array(
                        'Class' => $time_class,
                        'Time' => $search_time,
                        'Hour' => $hour
                    );
                }
            }
        }
        if (isset($time_array_others) && !empty($time_array_others)) {
            foreach ($time_array_others as $key => $val) {
                $time_array = array_merge($time_array, $val);
            }
        }
        /* ends */
        $is_time_found = false;
        if (sizeof($time_array) > 0) {
            foreach ($time_array as $rec) {
                if ($rec["Class"] == "Available") {
                    $is_time_found = true;
                    break;
                }
            }
        }
        if ($is_time_found) {
            $day_name = $this->GetFullDayName(date('w', strtotime($date)));
            return array(
                'Date_Full' => date($this->config->item('show_date_format'), strtotime($date)),
                'Date_Name' => date('dS', strtotime($date)),
                'Date_Number' => date('d', strtotime($date)),
                'Month_Number' => date('m', strtotime($date)),
                'Year_Number' => date('Y', strtotime($date)),
                'Month_Short_Name' => date('M', strtotime($date)),
                'Month_Full_Name' => date('F', strtotime($date)),
                'Day_Short_Name' => substr($day_name, 0, 3),
                'Day_Full_Name' => $day_name,
                'Times' => $time_array,
                'Hours' => $hours
            );
        } else {
            return array();
        }
    }

    public function GetFranchiseDateTimeCollection($date, $total_hours, $search_with, $franchise_record, $opening_franchise_time, $closing_franchise_time) {
        $create_date = date($this->config->item('show_date_format'), strtotime($date));
        $today_date = date('Y-m-d');
        $create_date_only = date('Y-m-d', strtotime($create_date));
        $is_today_date = false;
        if ($today_date == $create_date_only) {
            $is_today_date = true;
            $today_hour_add_3_date_time = date('Y-m-d H:i:s', strtotime('+3 hours'));
            $today_hour_add_3_date = date('Y-m-d', strtotime($today_hour_add_3_date_time));
            if ($today_date != $today_hour_add_3_date) {
                return array();
            }
        }
        $franchise_id = 0;
        if (isset($franchise_record['PKFranchiseID'])) {
            $franchise_id = $franchise_record['PKFranchiseID'];
        } else if (isset($franchise_record['ID'])) {
            $franchise_id = $franchise_record['ID'];
        }
        $day_name = $this->GetFullDayName(date('w', strtotime($create_date)));
        $date_array = array(
            'Date_Full' => date($this->config->item('show_date_format'), strtotime($create_date)),
            'Date_Name' => date('dS', strtotime($create_date)),
            'Date_Number' => date('d', strtotime($create_date)),
            'Month_Number' => date('m', strtotime($create_date)),
            'Year_Number' => date('Y', strtotime($create_date)),
            'Month_Short_Name' => date('M', strtotime($create_date)),
            'Month_Full_Name' => date('F', strtotime($create_date)),
            'Day_Short_Name' => substr($day_name, 0, 3),
            'Day_Full_Name' => $day_name
        );
        $time_array = array();
        for ($i = 0; $i < $total_hours; $i++) {
            $time_class = "Available";
            $hour = (int) date('H', strtotime('+' . $i . ' hours', strtotime($opening_franchise_time)));
            $search_time = date('H:00', strtotime('+' . $i . ' hours', strtotime($opening_franchise_time))) . '-' . date('H:00', strtotime('+' . ($i + 1) . ' hours', strtotime($opening_franchise_time)));
            if ($is_today_date) {
                $current_hour_add_3 = (int) date("H", strtotime('+3 hours'));
                if ($hour <= $current_hour_add_3) {
                    $time_class = "Disable";
                }
            }
            if ($time_class == "Available") {
                $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . date($this->config->item('query_date_format'), strtotime($create_date)) . "' and Time='" . $search_time . "' and (Type='Both' or Type='" . $search_with . "')");
                if ($disable_slot_record != false) {
                    $time_class = "Disable";
                }
            }
            if ($time_class == "Available") {
                if ($franchise_record['AllowSameTime'] == "No") {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . date($this->config->item('query_date_format'), strtotime($create_date)) . "' and PickupTime='" . $search_time . "') or (DeliveryDate='" . date($this->config->item('query_date_format'), strtotime($create_date)) . "' and DeliveryTime='" . $search_time . "')");
                    if ($invoice_record_count > 0) {
                        $time_class = "Disable";
                    }
                } else {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and " . $search_with . "Date='" . date($this->config->item('query_date_format'), strtotime($create_date)) . "' and " . $search_with . "Time='" . $search_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                    if ($invoice_record_count > 0) {
                        if ($search_with == "Pickup") {
                            if ($invoice_record_count >= $franchise_record['PickupLimit']) {
                                $time_class = "Disable";
                            }
                        } else {
                            if ($invoice_record_count >= $franchise_record['DeliveryLimit']) {
                                $time_class = "Disable";
                            }
                        }
                    }
                }
            }
            $time_array[] = array(
                'Class' => $time_class,
                'Time' => $search_time,
                'Hour' => $hour
            );
        }
        $is_time_found = false;
        if (sizeof($time_array) > 0) {
            foreach ($time_array as $rec) {
                if ($rec["Class"] == "Available") {
                    $is_time_found = true;
                    break;
                }
            }
        }
        if ($is_time_found) {
            $date_array['Times'] = $time_array;
            return $date_array;
        } else {
            return array();
        }
    }

    public function clean_json($string) {

        $string = str_replace("\\n", "", $string);
        return $string = strip_slashes($string);
    }

    public function CreateCookie($cookie_name, $value, $expire_time) {
        $cookie = array(
            'name' => $cookie_name,
            'value' => $value,
            'expire' => $expire_time
        );
        set_cookie($cookie);
    }

    public function GetCookie($cookie_name) {
        $data_cookie = "";
        if (get_cookie($cookie_name) != null) {
            $data_cookie = get_cookie($cookie_name);
        }
        return $data_cookie;
    }

    public function DeleteCookie($cookie_name) {
        delete_cookie($cookie_name);
    }

    public function IsAdminLogin() {
        if ($this->session->userdata('is_love_2_laundry_admin_logged_in') == null) {
            return false;
        } else {
            return true;
        }
    }

    public function AddSessionItem($name, $value) {
        $this->session->set_userdata($name, $value);
    }

    public function AddSessionItems($data) {
        $this->session->set_userdata($data);
    }

    public function IsSessionItem($session_item_name) {
        if ($this->session->userdata($session_item_name) == null) {
            return false;
        } else {
            return true;
        }
    }

    public function GetSessionItem($session_item_name) {
        if ($this->session->userdata($session_item_name) == null) {
            return false;
        } else {
            return $this->session->userdata($session_item_name);
        }
    }

    public function RemoveSessionItems($data) {
        $this->session->unset_userdata($data);
    }

    // Session Member Exist
    public function IsMemberLogin() {
        if ($this->session->userdata('is_love_2_laundry_member_logged_in') == null) {
            return false;
        } else {
            return true;
        }
    }

    // Get Current Member ID
    public function GetCurrentMemberID() {
        return $this->GetSessionItem('member_love_2_laundry_id');
    }

    // Get Current Member Detail
    public function GetCurrentMemberDetail() {
        return $this->GetSessionItem('member_love_2_laundry_detail');
    }

    // Get Current Member Detail
    public function IsMemberLoginFromAdmin() {
        if ($this->session->userdata('is_love_2_laundry_admin_member_logged_in') == null) {
            return false;
        } else {
            return $this->session->userdata('is_love_2_laundry_admin_member_logged_in');
        }
    }

    public function IsMemberLoginFromAdminSession() {
        return $this->session->userdata('is_love_2_laundry_admin_member_logged_in');
    }

    public function CaptchaImage($value) {
        echo $this->simplecaptcha->CreateImage($value);
    }

    public function CaptchaCode($value) {
        return $this->simplecaptcha->CurrentCaptchaCode($value);
    }

    public function GetWidgetSectionRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_widget_sections, 'PKSectionID as ID,Title', array($column_name => $column_value));
    }

    public function GetWidgetRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_widgets, 'PKWidgetID as ID,Title,ImageName,Content,Link', array($column_name => $column_value));
    }

    public function GetWidgetNavigationRecords($section_id, $widget_limit = false) {
        $widget_array = array();
        $widget_navigation_records = $this->model_database->GetRecords($this->tbl_widget_navigations, "R", 'FKWidgetID', array('FKSectionID' => $section_id), $widget_limit, false, false, 'Position', 'asc');
        if (sizeof($widget_navigation_records) > 0) {
            foreach ($widget_navigation_records as $key => $value) {
                $widget_record = $this->GetWidgetRecord("PKWidgetID", $value['FKWidgetID']);
                if ($widget_record !== false) {
                    $widget_array[] = $widget_record;
                }
            }
        }
        return $widget_array;
    }

    public function GetNavigationSectionRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_navigation_sections, 'PKSectionID as ID,Title', array($column_name => $column_value));
    }

    public function GetSettingRecord($title) {
        return $this->model_database->GetRecord($this->tbl_settings, 'Content', array('Title' => $title));
    }

    function GenerateMenus($section_id, $limit = false) {
        return $this->model_database->GetRecords($this->tbl_navigations, "R", 'PKNavigationID,FKPageID,Name as PageName,LinkWithUrl as PageURL', array('FKSectionID' => $section_id), $limit, false, false, 'Position', 'asc');
    }

    function GetPageRecord($column_name, $column_value, $status_check = true) {
        if ($status_check == false) {
            $page_record = $this->model_database->GetRecord($this->tbl_pages, 'PKPageID,Template,MetaTitle,MetaKeyword,MetaDescription,HeaderImageName,Title,Content,AccessURL,NoIndexFollowTag,IsPermanentRedirect,PermanentRedirectURL,Latitude,Longitude,HeaderTitle', array($column_name => $column_value));
        } else {
            $page_record = $this->model_database->GetRecord($this->tbl_pages, 'PKPageID,Template,MetaTitle,MetaKeyword,MetaDescription,HeaderImageName,Title,Content,AccessURL,NoIndexFollowTag,IsPermanentRedirect,PermanentRedirectURL,Latitude,Longitude,HeaderTitle', array($column_name => $column_value, 'Status' => 'Enabled'));
        }
        if ($page_record != false) {
            if ($page_record['IsPermanentRedirect'] == "Yes" && !empty($page_record['PermanentRedirectURL'])) {
                redirect(base_url($page_record['PermanentRedirectURL']), 'location', 301);
            }
            if ($page_record['MetaTitle'] == "" || $page_record['MetaTitle'] == null) {
                unset($page_record['MetaTitle']);
            }
            if ($page_record['MetaKeyword'] == "" || $page_record['MetaKeyword'] == null) {
                unset($page_record['MetaKeyword']);
            }
            if ($page_record['MetaDescription'] == "" || $page_record['MetaDescription'] == null) {
                unset($page_record['MetaDescription']);
            }
            if ($page_record['HeaderImageName'] == "" || $page_record['HeaderImageName'] == null) {
                unset($page_record['HeaderImageName']);
            }
            if ($page_record['NoIndexFollowTag'] == "No") {
                unset($page_record['NoIndexFollowTag']);
            }
            if ($page_record['Template'] != "None") {
                $page_section_records = $this->model_database->GetRecords($this->tbl_page_area_sections, "R", "Title,Content,ImageName", array("FKPageID" => $page_record['PKPageID']), false, false, false, "PKSectionID", "asc");
                if (sizeof($page_section_records) > 0) {
                    $count = 1;
                    foreach ($page_section_records as $rec) {
                        $page_record["Section_" . $count . "_Title"] = $rec['Title'];
                        $page_record["Section_" . $count . "_Content"] = $rec['Content'];
                        $page_record["Section_" . $count . "_Image_Name"] = $rec['ImageName'];
                        $count += 1;
                    }
                }
                if ($page_record['Template'] == "Area") {
                    $area_page_array = array();
                    $area_page_records = $this->model_database->GetRecords($this->tbl_page_areas, "R", "FKAreaDetailID", array('FKPageID' => $page_record['PKPageID']));
                    if (sizeof($area_page_records) > 0) {
                        foreach ($area_page_records as $rec) {
                            $area_detail_page_record = $this->model_database->GetRecord($this->tbl_pages, "Title,AccessURL,Latitude,Longitude", array('PKPageID' => $rec['FKAreaDetailID'], 'Status' => 'Enabled'));
                            if ($area_detail_page_record != false) {
                                $area_page_array[] = $area_detail_page_record;
                            }
                        }
                    }
                    if (sizeof($area_page_array) > 0) {
                        $page_record['area_page_array'] = $area_page_array;
                    }
                }
            }
        }
        return $page_record;
    }

    public function GetEmailHeaderMenus() {
        $menu_html = "";
        $menu_records = $this->GenerateMenus($this->email_header_menu_section_id);
        if (sizeof($menu_records) > 0) {
            foreach ($menu_records as $menu) {
                $menu_html .= '<li style="display:block; float:left;margin-left:0px;"><a href="' . CreateWebURL($menu['PageURL']) . '" target="_blank" style="color:#fff;text-transform:uppercase;padding:10px;font-size:12px;text-decoration:none;border:1px solid #fff;margin:0 5px;display:block">' . $menu['PageName'] . '</a></li>';
            }
        }
        return $menu_html;
    }

    public function GetEmailFooterMenus() {
        $menu_html = "";
        $menu_records = $this->GenerateMenus($this->email_footer_menu_section_id);
        if (sizeof($menu_records) > 0) {
            $menu_html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            $menu_total_count = round(sizeof($menu_records) / 4);
            $menu_count = 0;
            foreach ($menu_records as $menu) {
                if ($menu_count == 0) {
                    $menu_html .= '<tr>';
                }
                $menu_html .= '<td><a href="' . CreateWebURL($menu['PageURL']) . '" target="_blank" style="color:#fff;text-transform:capitalize;font-size:11px;text-decoration:none;">' . $menu['PageName'] . '</a></td>';
                $menu_count += 1;
                if ($menu_count == $menu_total_count) {
                    $menu_html .= '</tr>';
                    $menu_count = 0;
                }
            }
            if ($menu_count != 0) {
                $menu_html .= '</tr>';
            }
            $menu_html .= '</table>';
        }
        return $menu_html;
    }

    public function ReplaceTags($content, $field_array) {
        $email_tags = $this->model_database->GetEmailTags($field_array);
        foreach ($email_tags as $v) {
            $Tag = $v["Tag"];
            $MapField = $v["Title"];
            $content = str_replace($Tag, $field_array[$MapField], $content);
        }
        return $content;
    }

    public function GetVatAmount($price, $vat) {

        $price = numberformat($price);
        $vatAmount = ($price * $vat / 100);
        return $vatAmount;
    }

    public function SendInvoiceEmail($invoice_number, $invoice_date_type) {
        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
        if ($invoice_record != false) {

            $response["invoice"] = $invoice_record;

            $includeVat = $this->config->item("include_vat");

            $vatPercent = $this->GetAppVat();
            $vat = $this->GetVatAmount($invoice_record['GrandTotal'], $vatPercent);

            if ($includeVat == true) {
                $text = "Total (INC. " . $invoice_record['Currency'] . numberformat($vat) . " VAT)";
            } else {
                $text = "Total";
            }

            $email_field_array = array(
                'Email Sender' => "order@love2laundry.com",
                'Email Receivers' => "order@love2laundry.com",
                'Website Name' => $this->GetWebsiteName(),
                'Website URL' => $this->GetWebsiteURLAddress(),
                'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                'Website Email Address' => $this->GetWebsiteEmailAddress(),
                'Header Menus' => $this->GetEmailHeaderMenus(),
                'Footer Menus' => $this->GetEmailFooterMenus(),
                'Footer Title' => $this->GetEmailFooterTitle(),
                'Invoice Number' => $invoice_number,
                'Payment Method' => $invoice_record['PaymentMethod'],
                'Payment Status' => $invoice_record['PaymentStatus'],
                'Order Status' => $invoice_record['OrderStatus'],
                'Loyalty Points' => intval($invoice_record['GrandTotal']),
                'Pick Date Time' => date('d-m-Y', strtotime($invoice_record['PickupDate'])) . '<br/>' . $invoice_record['PickupTime'],
                'Delivery Date Time' => date('d-m-Y', strtotime($invoice_record['DeliveryDate'])) . '<br/>' . $invoice_record['DeliveryTime'],
                'Currency' => $invoice_record['Currency'],
                'Sub Total' => $invoice_record['SubTotal'],
                'Preference Total' => $invoice_record['PreferenceTotal'],
                'Discount Total' => $invoice_record['DiscountTotal'],
                'Grand Total Text' => $text,
                'Grand Total' => $invoice_record['GrandTotal'],
                'Order Notes' => isset($invoice_record['OrderNotes']) ? $invoice_record['OrderNotes'] : "",
                'Account Notes' => isset($invoice_record['AccountNotes']) ? $invoice_record['AccountNotes'] : "",
                'Additional Instructions' => isset($invoice_record['AdditionalInstructions']) ? $invoice_record['AdditionalInstructions'] : ""
            );
            if ($invoice_record['DiscountType'] == "None") {
                $email_field_array['Discount Type'] = "Discount";
            } else {
                $email_field_array['Discount Type'] = $invoice_record['DiscountType'];
            }
            $discount_detail_html = "";
            if (!empty($invoice_record['DiscountCode'])) {
                $discount_detail_html .= '<br/><small>Code:' . $invoice_record['DiscountCode'] . '</small>';
                if ($invoice_record['DType'] == "Percentage") {
                    $discount_detail_html .= '<br/><small>Worth:' . $invoice_record['DiscountWorth'] . '%</small>';
                } else if ($invoice_record['DType'] == "Price") {
                    $discount_detail_html .= '<br/><small>Worth:' . $invoice_record['Currency'] . $invoice_record['DiscountWorth'] . '</small>';
                }
            }
            $email_field_array['Discount Detail'] = $discount_detail_html;
            $address_html = "";

            $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['BuildingName'] . '</p>';

            if (!empty($invoice_record['StreetName'])) {
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['StreetName'] . '</p>';
            }

            if ($invoice_record['OrderPostFrom'] == "Mobile") {
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['PostalCode'] . '</p>';
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['Town'] . '</p>';
            }
            $email_field_array['Address'] = $address_html;

            if ($invoice_date_type == "C") {
                $email_field_array['Date Time Type'] = "Created Date Time";
                $email_field_array['Date Time'] = date('d-m-Y', strtotime($invoice_record['CreatedDateTime'])) . '<br/>' . date('h:i a', strtotime($invoice_record['CreatedDateTime']));
            } else {
                $email_field_array['Date Time Type'] = "Updated Date Time";
                $email_field_array['Date Time'] = date('d-m-Y', strtotime($invoice_record['UpdatedDateTime'])) . '<br/>' . date('h:i a', strtotime($invoice_record['UpdatedDateTime']));
            }
            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('PKMemberID' => $invoice_record['FKMemberID']));
            if ($member_record != false) {
                $email_field_array['Full Name'] = $member_record['FirstName'] . ' ' . $member_record['LastName'];
                $email_field_array['Email Address'] = $member_record['EmailAddress'];
                $email_field_array['Phone Number'] = $member_record['Phone'];
            }
            $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoiceServiceID", "asc");
            $invoice_services_html = "";
            $response["invoice_services"] = $invoice_service_records;


            if (sizeof($invoice_service_records) > 0) {
                $invoice_services_html = '<p style="background: #353535;  color:#fff; line-height: 3; padding: 0 10px; margin-bottom: 0; text-align: left;"> <strong>Shopping Cart</strong></p>';
                $invoice_services_html .= '<table style="width: 100%; margin-bottom: 30px; line-height: 1.5;">';
                $invoice_services_html .= '<thead>';
                $invoice_services_html .= '<tr >';
                $invoice_services_html .= '<th width="30%" style="padding: 10px 0;">Title</th>';
                $invoice_services_html .= '<th width="22%" style="padding: 10px 0; text-align: right;">Quantity</th>';
                $invoice_services_html .= '<th width="22%" style="padding: 10px 0; text-align: right;">Price</th>';
                $invoice_services_html .= '<th width="22%" style="padding: 10px 0; text-align: right;">Total</th>';
                $invoice_services_html .= '</tr>';
                $invoice_services_html .= '</thead>';
                $invoice_services_html .= '<tbody>';
                foreach ($invoice_service_records as $record) {

                    //$vat = $this->GetVatAmount($record['Price'], $vatPercent);
                    //$price = numberformat($record['Price']) - $vat;

                    $invoice_services_html .= '<tr>';
                    $invoice_services_html .= '<td width="30%" style="padding: 10px 0;">' . $record['Title'] . '</td>';
                    $invoice_services_html .= '<td width="22%" style="padding: 10px 0; text-align: right;">' . $record['Quantity'] . '</td>';
                    $invoice_services_html .= '<td width="22%" style="padding: 10px 0; text-align: right;">' . $invoice_record['Currency'] . $record['Price'] . '</td>';
                    $invoice_services_html .= '<td width="22%" style="padding: 10px 0; text-align: right;">' . $invoice_record['Currency'] . $record['Total'] . '</td>';
                    $invoice_services_html .= '</tr>';
                }
                $invoice_services_html .= '</tbody>';
                $invoice_services_html .= '</table>';
            }
            $email_field_array['Invoice Service Table'] = $invoice_services_html;
            $invoice_preference_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", false, array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoicePreferenceID", "asc");
            $invoice_preferences_html = "";

            if (sizeof($invoice_preference_records) > 0) {
                //$invoice_preferences_html = '<h4 style="margin-bottom:0">Customer Preferences</h4>';
                $invoice_preferences_html .= '<table  border="0" cellpadding="5" cellspacing="0" bordercolor="#DDDDDD" style="margin-bottom: 20px; width: 100%;">';
                $invoice_preferences_html .= '<tbody>';
                $up_area = "";
                $down_area = "";
                $count = 0;
                $bg_count = 0;

                foreach ($invoice_preference_records as $record) {
                    if ($record['ParentTitle'] == "Starch on Shirts") {
                        $down_area .= '<tr>';
                        $down_area .= '<td width="20%">' . $record['ParentTitle'] . '</td>';
                        $down_area .= '<td width="30%">' . $record['Title'] . '</td>';
                        $down_area .= '</tr>';
                    } else {
                        if ($count == 0) {
                            $up_area .= '<tr>';
                        }
                        $up_area .= '<td width="20%"';
                        if ($bg_count < 2) {
                            $up_area .= ' bgcolor="#EAEAEA"';
                        }
                        $up_area .= '>' . $record['ParentTitle'] . '</td>';
                        $up_area .= '<td width="30%"';
                        if ($bg_count < 2) {
                            $up_area .= ' bgcolor="#EAEAEA"';
                        }
                        $up_area .= '>' . $record['Title'] . '</td>';
                        if ($count == 1) {
                            $up_area .= '</tr>';
                            $count = 0;
                        } else {
                            $count = 1;
                        }
                        $bg_count += 1;
                        if ($bg_count == 4) {
                            $bg_count = 0;
                        }
                    }
                    $response["invoice_preferences"][] = $record;
                }

                $invoice_preferences_html .= '<tr>';
                $invoice_preferences_html .= '<th colspan="4"> <p style="background: #353535;  color:#fff; line-height: 3; padding: 0 10px; margin-bottom: 0; text-align: left;"> <strong>Customer Preferences</strong> <span style="float: right;">' . $this->config->item('front_member_laundry_first_heading_text') . '</span></p>';
                $invoice_preferences_html .= '</th>';
                $invoice_preferences_html .= '</tr>';
                $invoice_preferences_html .= $up_area;
                if (!empty($down_area)) {
                    $invoice_preferences_html .= '<tr>';
                    $invoice_preferences_html .= '<th colspan="4"> <p style="background: #353535;  color:#fff; line-height: 3; padding: 0 10px; margin-bottom: 0; text-align: left;"> <strong>Customer Preferences</strong> <span style="float: right;">' . $this->config->item('front_member_laundry_second_heading_text') . '</span></p>';
                    $invoice_preferences_html .= '</th>';
                    $invoice_preferences_html .= '</tr>';
                    $invoice_preferences_html .= $down_area;
                }
                $invoice_preferences_html .= '</tbody>';
                $invoice_preferences_html .= '</table>';
            }

            $email_field_array['Invoice Preference Table'] = $invoice_preferences_html;

            if ($invoice_date_type == "C") {
                $this->emailsender->send_email_responder(5, $email_field_array, false, $invoice_number);
                $this->emailsender->send_email_responder(10, $email_field_array, false, $invoice_number);
            } elseif ($invoice_date_type == "U") {
                $this->emailsender->send_email_responder(15, $email_field_array, false, $invoice_number);
                $this->emailsender->send_email_responder(16, $email_field_array, false, $invoice_number);
            }
            /* $this->emailsender->send_email_responder(5,$email_field_array,false,$invoice_number);
              $this->emailsender->send_email_responder(10,$email_field_array,false,$invoice_number); */
            $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "EmailAddress", array('PKFranchiseID' => $invoice_record['FKFranchiseID']));
            if ($franchise_record != false) {
                $email_field_array['Email Receivers'] = $franchise_record['EmailAddress'];

                $email_field_array['Email Address'] = '';
                $email_field_array['Phone Number'] = '';
                /* Hassan changes 23-01-2018 */
                /* $email_field_array['Address']='';

                  $email_field_array['Full Name']=''; */
                /* ends */

                if ($invoice_date_type == "C") {

                    $this->emailsender->send_email_responder(10, $email_field_array, false, $invoice_number);
                } elseif ($invoice_date_type == "U") {

                    $this->emailsender->send_email_responder(16, $email_field_array, false, $invoice_number);
                } elseif ($invoice_date_type == "X") {

                    $this->emailsender->send_email_responder(17, $email_field_array, false, $invoice_number);
                }
            }
        }
        return $response;
    }

    function GetInvoiceNumber($invoice_id) {
        $invoice_id = $invoice_id + 93;
        $invoiceNumber = sprintf('%04d', $invoice_id);
        return $invoiceNumber;
    }

    public function RandomPassword($length = 8) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function GetHeaderTitle() {
        $setting_record = $this->GetSettingRecord('Front Header Title');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? $this->config->item('front_header_title') : $setting_record['Content'];
        } else {
            return $this->config->item('front_header_title');
        }
    }

    public function GetFooterTitle() {
        $setting_record = $this->GetSettingRecord("Front Footer Title");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetWebsiteEmailSender() {
        $setting_record = $this->GetSettingRecord("Website Email Sender");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == '#') ? '' : strtolower($setting_record['Content']);
        } else {
            return "";
        }
    }

    public function GetWebsiteEmailReceivers() {
        $setting_record = $this->GetSettingRecord("Website Email Receivers");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == '#') ? '' : strtolower($setting_record['Content']);
        } else {
            return "";
        }
    }

    public function GetWebsiteName() {
        $setting_record = $this->GetSettingRecord("Website Name");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return $this->config->item('website_name');
        }
    }

    public function GetWebsiteStatus() {
        $setting_record = $this->GetSettingRecord('Website Status');
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "Online";
        }
    }

    public function GetEmailFooterTitle() {
        $setting_record = $this->GetSettingRecord("Email Footer Title");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetWebsiteURLAddress() {
        $setting_record = $this->GetSettingRecord("Website URL Address");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetGoogleAnalyticsID() {
        $setting_record = $this->GetSettingRecord('Google Analytics ID');
        if ($setting_record !== false) {
            return trim($setting_record['Content']);
        } else {
            return "#";
        }
    }

    public function GetWebsiteTelephoneNo() {
        $setting_record = $this->GetSettingRecord("Front Telephone Number");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetWebsiteEmailAddress() {
        $setting_record = $this->GetSettingRecord("Front Email Address");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetFacebookLink() {
        $setting_record = $this->GetSettingRecord("Facebook Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetTwitterLink() {
        $setting_record = $this->GetSettingRecord("Twitter Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetYouTubeLink() {
        $setting_record = $this->GetSettingRecord("You Tube Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetLinkedInLink() {
        $setting_record = $this->GetSettingRecord("Linkedin Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetPinterestLink() {
        $setting_record = $this->GetSettingRecord("Pinterest Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetGooglePlusLink() {
        $setting_record = $this->GetSettingRecord("Google Plus Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetInstagramLink() {
        $setting_record = $this->GetSettingRecord("Instagram Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetGooglePlayLink() {
        $setting_record = $this->GetSettingRecord("Google Play Store Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetAppleStoreLink() {
        $setting_record = $this->GetSettingRecord("Apple Store Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetStripeMode() {
        $setting_record = $this->GetSettingRecord('Stripe Payment Mode');
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "Test";
        }
    }

    public function GetStripeJQueryKey() {
        $setting_record = false;
        if ($this->GetStripeMode() == "Test") {
            $setting_record = $this->GetSettingRecord('Stripe JQuery Test Key');
        } else {
            $setting_record = $this->GetSettingRecord('Stripe JQuery Live Key');
        }
        if ($setting_record != false) {
            return $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetStripeAPIKey() {
        $setting_record = false;
        if ($this->GetStripeMode() == "Test") {
            $setting_record = $this->GetSettingRecord('Stripe API Test Key');
        } else {
            $setting_record = $this->GetSettingRecord('Stripe API Live Key');
        }
        if ($setting_record != false) {
            return $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetOrderAmountLater() {
        $setting_record = $this->GetSettingRecord('Minimum Order Amount Later');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 15 : intval($setting_record['Content']);
        } else {
            return 15;
        }
    }

    public function GetOrderAmount() {
        $setting_record = $this->GetSettingRecord('Minimum Order Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 15 : intval($setting_record['Content']);
        } else {
            return 15;
        }
    }

    public function GetReferralCodeAmount() {
        $setting_record = $this->GetSettingRecord('Referral Code Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 5 : intval($setting_record['Content']);
        } else {
            return 5;
        }
    }

    public function GetLoyaltyCodeAmount() {
        $setting_record = $this->GetSettingRecord('Loyalty Code Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 5 : intval($setting_record['Content']);
        } else {
            return 5;
        }
    }

    public function GetMinimumLoyaltyPoints() {
        $setting_record = $this->GetSettingRecord('Minimum Loyalty Points');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 100 : intval($setting_record['Content']);
        } else {
            return 100;
        }
    }

    public function GetAppCategoryServiceVersion() {
        $setting_record = $this->GetSettingRecord('App Categories Services Version');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 0 : intval($setting_record['Content']);
        } else {
            return 0;
        }
    }

    public function GetAppPreferenceVersion() {
        $setting_record = $this->GetSettingRecord('App Preferences Version');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 0 : intval($setting_record['Content']);
        } else {
            return 0;
        }
    }

    public function GetAppSettingVersion() {
        $setting_record = $this->GetSettingRecord('App Settings Version');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 0 : intval($setting_record['Content']);
        } else {
            return 0;
        }
    }

    public function GetAppVat() {
        $setting_record = $this->GetSettingRecord('Vat');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 0 : floatval($setting_record['Content']);
        } else {
            return 0;
        }
    }

    // Call view with Menu
    public function show_view_with_menu($view_name, $data = false) {
        $this->AddSessionItem("Last_Visited_Page_URL", current_url());
        $data['current_url'] = isset($this->uri->segments[1]) ? $this->uri->segments[1] : 'home';
        $data['website_name'] = $this->GetWebsiteName();
        $data['header_title'] = $this->GetHeaderTitle();
        $data['minimum_order_amount_later'] = $this->GetOrderAmountLater();
        $data['minimum_order_amount'] = $this->GetOrderAmount();
        if ($this->IsSessionItem($this->session_franchise_record)) {
            $franchise_record = $this->GetSessionItem($this->session_franchise_record);
            $data['minimum_order_amount_later'] = $franchise_record['MinimumOrderAmountLater'];
            $data['minimum_order_amount'] = $franchise_record['MinimumOrderAmount'];
        }
        if ($this->IsSessionItem($this->session_post_code)) {
            $data['session_post_code'] = $this->GetSessionItem($this->session_post_code);
        }
        $data['website_telephone_no'] = $this->GetWebsiteTelephoneNo();
        if ($data['website_telephone_no'] == "#") {
            unset($data['website_telephone_no']);
        }
        $data['website_email_address'] = $this->GetWebsiteEmailAddress();
        if ($data['website_email_address'] == "#") {
            unset($data['website_email_address']);
        }
        $data['facebook_link'] = $this->GetFacebookLink();
        if ($data['facebook_link'] == "#") {
            unset($data['facebook_link']);
        }
        $data['twitter_link'] = $this->GetTwitterLink();
        if ($data['twitter_link'] == "#") {
            unset($data['twitter_link']);
        }
        $data['you_tube_link'] = $this->GetYouTubeLink();
        if ($data['you_tube_link'] == "#") {
            unset($data['you_tube_link']);
        }
        $data['google_analytics_id'] = $this->GetGoogleAnalyticsID();
        if ($data['google_analytics_id'] == "#") {
            unset($data['google_analytics_id']);
        }
        if (!isset($data['is_order_page'])) {
            if (isset($data['is_work_widget_show'])) {
                $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_work_widget_section_id);
                if ($widget_section_record != false) {
                    $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
                    if (sizeof($widget_navigation_records) > 0) {
                        $data['widget_work_section_record'] = $widget_section_record;
                        $data['widget_work_section_navigation_records'] = $widget_navigation_records;
                    }
                }
            }
            if (isset($data['is_why_widget_show'])) {
                $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_why_widget_section_id);
                if ($widget_section_record != false) {
                    $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
                    if (sizeof($widget_navigation_records) > 0) {
                        $data['widget_why_section_record'] = $widget_section_record;
                        $data['widget_why_section_navigation_records'] = $widget_navigation_records;
                    }
                }
            }
            /* $google_review_data = json_decode($this->get_curl_request("https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ4XUBAiQDdkgRxJJKtrf97hg&key=" . $this->google_map_api_key),true);
              if($google_review_data["status"] == "OK"){
              $data['google_review_data'] = $google_review_data;
              } */
            $data['footer_title'] = $this->GetFooterTitle();
            if ($data['footer_title'] == "#") {
                unset($data['footer_title']);
            }
            $data['header_menus_records'] = $this->GenerateMenus($this->header_menu_section_id);
            if (sizeof($data['header_menus_records']) == 0) {
                unset($data['header_menus_records']);
            }
            $data['footer_menus_records'] = $this->GenerateMenus($this->footer_menu_section_id);
            if (sizeof($data['footer_menus_records']) == 0) {
                unset($data['footer_menus_records']);
            }
            if (!isset($data['only_copy_right_section'])) {
                $data['cleaner_menus_records'] = $this->GenerateMenus($this->cleaner_menu_section_id);
                if (sizeof($data['cleaner_menus_records']) == 0) {
                    unset($data['cleaner_menus_records']);
                }
                $data['service_menus_records'] = $this->GenerateMenus($this->services_menu_section_id);
                if (sizeof($data['service_menus_records']) == 0) {
                    unset($data['service_menus_records']);
                }
                $data['linked_in_link'] = $this->GetLinkedInLink();
                if ($data['linked_in_link'] == "#") {
                    unset($data['linked_in_link']);
                }
                $data['google_plus_link'] = $this->GetGooglePlusLink();
                if ($data['google_plus_link'] == "#") {
                    unset($data['google_plus_link']);
                }
                $data['pinterest_link'] = $this->GetPinterestLink();
                if ($data['pinterest_link'] == "#") {
                    unset($data['pinterest_link']);
                }
                $data['instagram_link'] = $this->GetInstagramLink();
                if ($data['instagram_link'] == "#") {
                    unset($data['instagram_link']);
                }
                $data['google_play_link'] = $this->GetGooglePlayLink();
                if ($data['google_play_link'] == "#") {
                    unset($data['google_play_link']);
                }
                $data['apple_store_link'] = $this->GetAppleStoreLink();
                if ($data['apple_store_link'] == "#") {
                    unset($data['apple_store_link']);
                }
            }
        }
        $data['stripe_js_key'] = $this->GetStripeJQueryKey();
        if ($this->IsMemberLogin() == true) {
            $data['member_detail'] = $this->GetCurrentMemberDetail();

            $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';

            if (($current_url == $this->config->item('front_member_account_page_url') ) && isset($data['member_detail']) && (is_null($data['member_detail']['ReferralCode']) || trim($data['member_detail']['ReferralCode']) == '' || trim($data['member_detail']['ReferralCode']) == 'Later')) {

                $FirstName = (isset($data['member_detail']) && isset($data['member_detail']['FirstName'])) ? $data['member_detail']['FirstName'] : '';

                $member_id = $this->GetCurrentMemberID();
                $data['member_detail']['ReferralCode'] = $this->random_str($FirstName);
                $update_member = array(
                    'ReferralCode' => $data['member_detail']['ReferralCode'],
                    'ID' => $member_id
                );
                $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                $this->AddSessionItem('member_love_2_laundry_detail', $data['member_detail']);
            }


            if ($this->IsMemberLoginFromAdmin() == false) {
                $data['is_member_login'] = 'Yes';
                $data['member_full_name'] = $data['member_detail']['FirstName'] . ' ' . $data['member_detail']['LastName'];
                $data['member_loyalty_points'] = intval($data['member_detail']['TotalLoyaltyPoints']) - intval($data['member_detail']['UsedLoyaltyPoints']);
            }
        }
        if ($this->IsSessionItem("invoice_type")) {
            $data['invoice_type'] = $this->GetSessionItem("invoice_type");
        }
        $data['edit_order_hour_limit'] = $this->edit_order_hour_limit;
        $data['main_content'] = $view_name;
        /* Uae pages check 22-55-2018 */
        if ($data['current_url'] == 'manchester' || $data['current_url'] == 'birmingham' || $data['current_url'] == 'liverpool' || $data['current_url'] == 'oxford' || $data['current_url'] == 'amsterdam') {

            $this->load->view('ae_template', $data);
        } else {
            if (!isset($data['template'])) {
                $data['template'] = "master_template";
            }
            $this->load->view($data['template'], $data);
        }
    }

    public function show_area_view_with_menu($view_name, $data = false) {
        $this->AddSessionItem("Last_Visited_Page_URL", current_url());
        $data['current_url'] = isset($this->uri->segments[1]) ? $this->uri->segments[1] : 'home';
        $data['website_name'] = $this->GetWebsiteName();
        $data['header_title'] = $this->GetHeaderTitle();
        $data['minimum_order_amount_later'] = $this->GetOrderAmountLater();
        $data['minimum_order_amount'] = $this->GetOrderAmount();
        if ($this->IsSessionItem($this->session_post_code)) {
            $data['session_post_code'] = $this->GetSessionItem($this->session_post_code);
        }
        $data['website_telephone_no'] = $this->GetWebsiteTelephoneNo();
        if ($data['website_telephone_no'] == "#") {
            unset($data['website_telephone_no']);
        }
        $data['website_email_address'] = $this->GetWebsiteEmailAddress();
        if ($data['website_email_address'] == "#") {
            unset($data['website_email_address']);
        }
        $data['google_analytics_id'] = $this->GetGoogleAnalyticsID();
        if ($data['google_analytics_id'] == "#") {
            unset($data['google_analytics_id']);
        }
        $data['footer_title'] = $this->GetFooterTitle();
        if ($data['footer_title'] == "#") {
            unset($data['footer_title']);
        }
        $data['header_menus_records'] = $this->GenerateMenus($this->header_menu_section_id);
        if (sizeof($data['header_menus_records']) == 0) {
            unset($data['header_menus_records']);
        }
        $data['footer_menus_records'] = $this->GenerateMenus($this->footer_menu_section_id);
        if (sizeof($data['footer_menus_records']) == 0) {
            unset($data['footer_menus_records']);
        }
        $data['cleaner_menus_records'] = $this->GenerateMenus($this->cleaner_menu_section_id);
        if (sizeof($data['cleaner_menus_records']) == 0) {
            unset($data['cleaner_menus_records']);
        }
        $data['service_menus_records'] = $this->GenerateMenus($this->services_menu_section_id);
        if (sizeof($data['service_menus_records']) == 0) {
            unset($data['service_menus_records']);
        }
        $data['facebook_link'] = $this->GetFacebookLink();
        if ($data['facebook_link'] == "#") {
            unset($data['facebook_link']);
        }
        $data['twitter_link'] = $this->GetTwitterLink();
        if ($data['twitter_link'] == "#") {
            unset($data['twitter_link']);
        }
        $data['you_tube_link'] = $this->GetYouTubeLink();
        if ($data['you_tube_link'] == "#") {
            unset($data['you_tube_link']);
        }
        $data['linked_in_link'] = $this->GetLinkedInLink();
        if ($data['linked_in_link'] == "#") {
            unset($data['linked_in_link']);
        }
        $data['google_plus_link'] = $this->GetGooglePlusLink();
        if ($data['google_plus_link'] == "#") {
            unset($data['google_plus_link']);
        }
        $data['pinterest_link'] = $this->GetPinterestLink();
        if ($data['pinterest_link'] == "#") {
            unset($data['pinterest_link']);
        }
        $data['instagram_link'] = $this->GetInstagramLink();
        if ($data['instagram_link'] == "#") {
            unset($data['instagram_link']);
        }
        $data['google_play_link'] = $this->GetGooglePlayLink();
        if ($data['google_play_link'] == "#") {
            unset($data['google_play_link']);
        }
        $data['apple_store_link'] = $this->GetAppleStoreLink();
        if ($data['apple_store_link'] == "#") {
            unset($data['apple_store_link']);
        }
        /* $google_review_data = json_decode($this->get_curl_request("https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ4XUBAiQDdkgRxJJKtrf97hg&key=" . $this->google_map_api_key),true);
          if($google_review_data["status"] == "OK"){
          $data['google_review_data'] = $google_review_data;
          } */
        $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_price_widget_id);
        if ($widget_record != false) {
            $data['widget_price_record'] = $widget_record;
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_price_widget_section_id);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_work_widget_section_id);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['stripe_js_key'] = $this->GetStripeJQueryKey();
        if ($this->IsMemberLogin() == true) {
            $data['member_detail'] = $this->GetCurrentMemberDetail();
            if ($this->IsMemberLoginFromAdmin() == false) {
                $data['is_member_login'] = 'Yes';
                $data['member_full_name'] = $data['member_detail']['FirstName'] . ' ' . $data['member_detail']['LastName'];
                $data['member_loyalty_points'] = intval($data['member_detail']['TotalLoyaltyPoints']) - intval($data['member_detail']['UsedLoyaltyPoints']);
            }
        }
        $data['edit_order_hour_limit'] = $this->edit_order_hour_limit;
        $data['main_content'] = $view_name;
        $this->load->view('area/master_template', $data);
    }

    public function GetCurrentFranchiseID() {
        $FranchiseID = $this->GetSessionItem('admin_love_2_laundry_detail');

        return (isset($FranchiseID['FKFranchiseID']) && $FranchiseID['FKFranchiseID'] > 0) ? $FranchiseID['FKFranchiseID'] : 0;
    }

    /**
     * Generate a random string, using a cryptographically secure 
     * pseudorandom number generator (random_int)
     * 
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     * 
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    function random_str($FirstName, $member_id = 0, $str = 20) {
        $FirstName = preg_replace('/\s+/', '', $FirstName);
        $unique = false;

        // Store tested results in array to not test them again
        $tested = [];


        do {


            $random_str = $FirstName . ($str);
            // Check if it's already testing
            // If so, don't query the database again
            if (in_array($random_str, $tested)) {
                $str++;
                continue;
            }

            // Check if it is unique in the database
            $count = 0;

            //'PKMemberID !='=>$member_id,
            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('ReferralCode' => $random_str));

            if ($member_record != false) {
                $count = 1;
            }


            // Store the random character in the tested array
            // To keep track which ones are already tested
            $tested[] = $random_str;

            // String appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

            // If unique is still false at this point
            // it will just repeat all the steps until
            // it has generated a random string of characters
        } while (!$unique);


        return $random_str;
    }

    // input is numeric or not 
    function isInteger($input) {
        return(ctype_digit(strval($input)));
    }

    protected function _URLCthhecker() {
//        if(strpos($_SERVER["REQUEST_URI"],".html") == false){
//            redirect(site_url(str_replace("/lovetolaundry","",$_SERVER["REQUEST_URI"])),'location',301);
//        }
        if (strpos($_SERVER["REQUEST_URI"], ".html") !== false) {
            redirect(base_url(str_replace("/lovetolaundrylive", "", str_replace(".html", "", $_SERVER["REQUEST_URI"]))), 'location', 301);
        }
    }

    function saveToTookaan($invoice_id, $member_record, $action) {


        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));
        $invoice_number = $invoice_record["InvoiceNumber"];

        if ($member_record == false) {
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $invoice_record["FKMemberID"]));
        }


        $tookan_array['team_id'] = "21339";

        $tookan_array = array(
            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
            'order_id' => $invoice_number,
            'auto_assignment' => 0,
            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
            'job_pickup_email' => $member_record['EmailAddress'],
            'customer_email' => $member_record['EmailAddress'],
            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
            'has_pickup' => "1",
            'has_delivery' => "1",
            'layout_type' => "0",
            'timezone' => $this->config->item('tookan_timezone'),
            'custom_field_template' => "",
            'meta_data' => array(),
            'tracking_link' => "1",
            'geofence' => 1
        );

        if ($action == "insert") {

            $tookan_url = "https://api.tookanapp.com/v2/create_task";
        } else {
            $tookan_url = "https://api.tookanapp.com/v2/edit_task";
            $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);



            if (!empty($invoice_record['TookanResponse'])) {
                if (isset($tookan_array['team_id'])) {
                    unset($tookan_array['team_id']);
                }

                if (isset($tookan_array['auto_assignment'])) {
                    //  unset($tookan_array['auto_assignment']);
                }

                $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];
                $tookan_response_result = "";
            }
        }




        if ($invoice_record['OrderPostFrom'] == "Desktop") {
            $addresses = $this->country->tookaanAddress($invoice_record['BuildingName'], $invoice_record['StreetName'], $invoice_record['PostalCode'], $invoice_record['Town']);
        } else {
            $addresses = $this->country->tookaanAddress($invoice_record['BuildingName'], $invoice_record['StreetName'], $invoice_record['PostalCode'], $invoice_record['Town']);
        }

        $tookan_array["job_pickup_address"] = $addresses["address_tookan"];
        $tookan_array["customer_address"] = $addresses["address_tookan"];

        $pick_time_array = explode("-", $invoice_record['PickupTime']);
        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);

        $description_tookan = $addresses["address_description"];
        $description_tookan .= '
							';

        if ($invoice_record['InvoiceType'] == "Items") {
            $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Price,Quantity,Total", array('FKInvoiceID' => $invoice_id));
        }

        if (!empty($invoice_record['service_records'])) {

            $k = 0;
            foreach ($invoice_record["service_records"] as $valueis) {
                $description_tookan .= '
									' . ($k + 1) . ' # Service : ' . $valueis['Title'] . '
									';
                $description_tookan .= '# Quantity : ' . $valueis['Quantity'] . '
									';
                $description_tookan .= '# Price : ' . $valueis['Price'] . '
									';
                $description_tookan .= '# Total : ' . $valueis['Total'] . '
									
									';
                $k++;
            }
        }



        $update_invoice_record = array();
        $tookan_array["job_description"] = $invoice_record['OrderNotes'] . $description_tookan;

        $tookan_array["job_pickup_datetime"] = date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00';
        $tookan_array["job_delivery_datetime"] = date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00';

        $tookan_response_result = $this->tookan_api_post_request($tookan_url, json_encode($tookan_array));
        $response_array = json_decode($tookan_response_result, true);

        if (!empty($response_array["data"])) {
            $update_invoice_record['TookanResponse'] = $tookan_response_result;

            $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
            $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
            $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
        }

        if (isset($tooken_response_array['data']['delivery_job_id'])) {
            $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
            $tookan_response_result = $this->tookan_api_post_request($tookan_url, json_encode($tookan_array));
            $update_invoice_record['TookanResponse'] = $tookan_response_result;
            $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
        }

        return $tookan_response_result;
    }

    function getTimeSlot($post_code = '') {
        $is_valid = false;
        if (isset($post_code) && !empty($post_code)) {
            $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);

            if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) > 5) {
                $franchise_id = 0;
                $short_post_code_array = $this->country->ValidatePostalCode($post_code);

                if ($short_post_code_array['validate'] == false) {
                    return "error||Please enter your full postcode e.g. E12PE";
                } else {
                    $short_post_code = $short_post_code_array['prefix'];
                    $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                    if ($franchise_post_code_record != false) {
                        $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                    }
                }
                $insert_search_post_code = array(
                    'Code' => $post_code,
                    'IPAddress' => $this->input->ip_address()
                );

                $franchise_record = $this->_GetFranchiseRecord($franchise_id);
                if ($franchise_record != false) {
                    $insert_search_post_code['FKFranchiseID'] = $franchise_id;
                    $opening_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['OpeningTime']));
                    $closing_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['ClosingTime']));
                    $total_hours = ((strtotime($closing_franchise_time) - strtotime($opening_franchise_time)) / ( 60 * 60 ));
                    /* Hassan changes 23-01-2018 */
                    $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $franchise_id), false, false, false, false, 'asc');
                    /* ends */
                    if ($total_hours > 0) {
                        $time_collection_array = array();
                        if (intval($franchise_record['MinimumOrderAmount']) > 0) {
                            $time_collection_array['minimum_order_amount_later'] = $franchise_record['MinimumOrderAmount'];
                        } else {
                            $time_collection_array['minimum_order_amount_later'] = $this->GetOrderAmountLater();
                        }
                        if (intval($franchise_record['MinimumOrderAmount']) > 0) {
                            $time_collection_array['minimum_order_amount'] = $franchise_record['MinimumOrderAmount'];
                        } else {
                            $time_collection_array['minimum_order_amount'] = $this->GetOrderAmount();
                        }
                        $time_collection_array['delivery_difference_hour'] = $franchise_record['DeliveryDifferenceHour'];
                        $time_collection_array['delivery_option'] = $franchise_record['DeliveryOption'];
                        $time_collection_array['pick_up'] = array();
                        $time_collection_array['delivery'] = array();
                        $pick_array_count = 0;
                        $delivery_array_count = 0;
                        for ($i = 0; $i <= 100; $i++) {
                            if ($pick_array_count <= 15) {
                                $search_type = "Pickup";
                                $create_date = date($this->config->item('query_date_format'), strtotime("+" . $i . ' days'));
                                if ($this->IsDisableDateSlot($create_date, $search_type, $franchise_record['PKFranchiseID']) == false) {
                                    //$return_response = $this->GetFranchiseDateTimeCollectionUpdated($create_date,$total_hours,$search_type,$franchise_record,$opening_franchise_time);
                                    /* Hassan changes 23-01-2018 */
                                    $return_response = $this->GetFranchiseDateTimeCollectionUpdated($create_date, $total_hours, $search_type, $franchise_record, $opening_franchise_time, $franchise_timings_record);
                                    /* ends */
                                    if (sizeof($return_response) > 0) {
                                        $time_collection_array['pick_up'][] = $return_response;
                                        $pick_array_count += 1;
                                    }
                                }
                            }
                            if ($delivery_array_count <= 23 && $pick_array_count > 0) {
                                $search_type = "Delivery";
                                $create_date = date($this->config->item('query_date_format'), strtotime("+" . ($i + 1) . ' days'));
                                if ($this->IsDisableDateSlot($create_date, $search_type, $franchise_record['PKFranchiseID']) == false) {
                                    //$return_response = $this->GetFranchiseDateTimeCollectionUpdated($create_date,$total_hours,$search_type,$franchise_record,$opening_franchise_time);
                                    /* Hassan changes 23-01-2018 */
                                    $return_response = $this->GetFranchiseDateTimeCollectionUpdated($create_date, $total_hours, $search_type, $franchise_record, $opening_franchise_time, $franchise_timings_record);
                                    /* ends */
                                    if (sizeof($return_response) > 0) {
                                        $time_collection_array['delivery'][] = $return_response;
                                        $delivery_array_count += 1;
                                    }
                                }
                            }
                            if ($pick_array_count == 16 && $delivery_array_count == 24) {
                                break;
                            }
                        }
                        $is_valid = true;
                        return $time_collection_array;
                    }
                }
                $insert_search_post_code['CreatedDateTime'] = date('Y-m-d H:i:s');
                $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
            }
        }
        if (!$is_valid) {
            return "error||Sorry, We are not in your area yet.";
        }
    }

    function GetInvoices() {
        $where_condition = '(Regularly = 1 OR Regularly = 2) AND OrderStatus<>"Cancel"  AND PKInvoiceID>1000 AND DATEDIFF(NOW(),CreatedDateTime)<20';
        $invoices = $this->model_database->GetRecords($this->tbl_invoices, "R", false, $where_condition, false, false, false, "PKInvoiceID", "asc");
        return (isset($invoices) && !empty($invoices)) ? $invoices : array();
    }

    function date_diffrence($d1 = '', $d2 = '') {
        if ($d1 != '' && $d2 != '') {
            //$date1 = new DateTime($d1);
            //$date2 = new DateTime($d2);
            $datetime1 = date_create($d1);
            $datetime2 = date_create($d2);
            $interval = date_diff($datetime1, $datetime2);



            //return $diff = $date2->diff($date1)->format("%a");
            return $interval->format('%R%a days');
        } else {
            return false;
        }
    }

    protected function _URLChecker() {
//        if(strpos($_SERVER["REQUEST_URI"],".html") == false){
//            redirect(site_url(str_replace("/lovetolaundry","",$_SERVER["REQUEST_URI"])),'location',301);
//        }
        if (strpos($_SERVER["REQUEST_URI"], ".html") !== false) {
            redirect(base_url(str_replace("/lovetolaundrylive", "", str_replace(".html", "", $_SERVER["REQUEST_URI"]))), 'location', 301);
        }
    }

    protected function _GetPreferences() {
        $preference_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title', array('Status' => 'Enabled', 'ParentPreferenceID' => 0), false, false, false, 'Position', 'asc');
        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {
                $preference_child_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title,Price,PriceForPackage as PricePackage', array('Status' => 'Enabled', 'ParentPreferenceID' => $value['PKPreferenceID']), false, false, false, 'Price', 'asc');
                if (sizeof($preference_child_records) > 0) {
                    $preference_records[$key]['child_records'] = $preference_child_records;
                } else {
                    unset($preference_records[$key]);
                }
            }
        }
        return $preference_records;
    }

    protected function _GetPreferencesRegistration() {
        $preference_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title', array('Status' => 'Enabled', 'ParentPreferenceID' => 0), false, false, false, 'Position', 'asc');
        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {
                $preference_child_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title,Price,PriceForPackage as PricePackage', array('Status' => 'Enabled', 'ParentPreferenceID' => $value['PKPreferenceID']), 1, false, false, 'Position', 'asc');
                if (sizeof($preference_child_records) > 0) {
                    $preference_records[$key]['child_records'] = $preference_child_records;
                } else {
                    unset($preference_records[$key]);
                }
            }
        }
        return $preference_records;
    }

    protected function _GetMemberPreferences() {
        $member_preference_array = array();
        if ($this->IsMemberLogin() == true) {
            $member_preference_records = $this->model_database->GetRecords($this->tbl_member_preferences, "R", "FKPreferenceID", array('FKMemberID' => $this->GetCurrentMemberID()));
            foreach ($member_preference_records as $key => $value) {
                $member_preference_array[] = $value['FKPreferenceID'];
            }
        }
        return $member_preference_array;
    }

    protected function _page_not_found($data = false) {
        $this->_URLChecker();
        $data['page_data'] = $this->GetPageRecord("AccessURL", "404", false);
        header("HTTP/1.1 404 Not Found");
        $data['is_order_page'] = false;
        $this->show_view_with_menu('pages/404', $data);
    }

    protected function _log_out() {
        $member = array(
            'member_love_2_laundry_detail',
            'member_love_2_laundry_id',
            'is_love_2_laundry_member_logged_in',
            'is_love_2_laundry_admin_member_logged_in'
        );
        $this->RemoveSessionItems($member);
    }

    protected function _GetFranchiseRecord($id) {
        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, false, array("PKFranchiseID" => $id, 'Status' => 'Enabled'));
        return $franchise_record;
    }

    /* Hassan changes 23-01-2018 */

    protected function _GetFranchiseTimingsRecord($id) {
        $franchise_timings_record = $this->model_database->GetRecord($this->tbl_franchise_timings, false, array("FKFranchiseID" => $id));
        return $franchise_timings_record;
    }

    /**/

    protected function _CreateMemberSession($id, $is_admin_member = false) {
        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $id));
        if ($member_record != false) {
            $this->RemoveSessionItems("invoice_type");
            $this->RemoveSessionItems("admin_invoice_id");
            $this->RemoveSessionItems("member_invoice_id");
            $member = array(
                'member_love_2_laundry_detail' => $member_record,
                'member_love_2_laundry_id' => $member_record["PKMemberID"],
                'is_love_2_laundry_member_logged_in' => true,
                'is_love_2_laundry_admin_member_logged_in' => $is_admin_member
            );
            $this->AddSessionItems($member);
        }
    }

    function validateEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    function dateTime_difference($d1 = '', $d2 = '') {
        if ($d1 != '' && $d2 != '') {
            //$date1 = new DateTime($d1);
            //$date2 = new DateTime($d2);
            $datetime1 = date_create($d1);
            $datetime2 = date_create($d2);
            $interval = date_diff($datetime1, $datetime2);



            //return $diff = $date2->diff($date1)->format("%a");
            $days = $interval->format('%a');
            return (int) ((int) $interval->format('%h') + (int) ($days * 24));
        } else {
            return false;
        }
    }

    function validatePhoneNumber($phone) {



        if (is_numeric($phone) && strlen($phone) >= 8 && strlen($phone) < 13) {
            return true;
        }

        return false;
    }

}
