<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_Admin_Controller extends CI_Controller {

    protected $database;
    protected $country;

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
        $this->load->library('datatables');
        $this->load->library('emailsender');
        $this->config->load('params');
        $this->load->helper('common');

        $this->tbl_payment_informations = "ws_payment_informations";
        $this->tbl_payment_refund_informations = "ws_payment_refund_informations";
        $this->tbl_user_menus = "cms_admin_menus";
        $this->tbl_email_responders = "cms_email_responders";
        $this->tbl_sms_responders = "cms_sms_responders";
        $this->tbl_tags = "cms_tags";
        $this->tbl_menus = "cms_menus";
        $this->tbl_navigations = "cms_navigations";
        $this->tbl_navigation_sections = "cms_navigation_sections";
        $this->tbl_pages = "cms_pages";
        $this->tbl_page_areas = "cms_page_areas";
        $this->tbl_page_area_sections = "cms_page_area_sections";
        $this->tbl_settings = "cms_settings";
        $this->tbl_users = "cms_users";
        $this->tbl_widgets = "cms_widgets";
        $this->tbl_widget_navigations = "cms_widget_navigation_sections";
        $this->tbl_widget_sections = "cms_widget_sections";
        $this->tbl_banners = "ws_banners";
        $this->tbl_categories = "ws_categories";
        $this->tbl_discounts = "ws_discounts";
        $this->tbl_invoices = "ws_invoices";
        $this->tbl_invoice_preferences = "ws_invoice_preferences";
        $this->tbl_invoice_services = "ws_invoice_services";
        $this->tbl_invoice_payment_informations = "ws_invoice_payment_informations";
        $this->tbl_locations = "ws_locations";
        $this->tbl_lockers = "ws_lockers";
        $this->tbl_preferences = "ws_preferences";
        $this->tbl_services = "ws_services";
        $this->tbl_testimonials = "ws_testimonials";
        $this->tbl_members = "ws_members";
        $this->tbl_member_cards = "ws_member_cards";
        $this->tbl_member_preferences = "ws_member_preferences";
        $this->tbl_websites = "ws_websites";
        $this->tbl_disable_date_slots = "ws_disable_date_slots";
        $this->tbl_disable_time_slots = "ws_disable_time_slots";
        $this->tbl_franchises = "ws_franchises";  /* Hassan changes 23-01-2018 */ $this->tbl_franchise_timings = "ws_franchise_timings";  /* ends */
        $this->tbl_franchise_post_codes = "ws_franchise_post_codes";
        $this->tbl_subscribers = "ws_subscribers";
        $this->tbl_loyalty_points = "ws_loyalty_points";
        $this->tbl_search_post_codes = "ws_search_post_codes";
        $this->tbl_sms_logs = "ws_sms_logs";
        $this->tbl_devices = "ws_devices";
        /* Franchise services */
        $this->tbl_franchise_services = "ws_franchise_services";
        $this->tbl_notifications = "notifications";


        $this->stripe_get_customer_url = "https://api.stripe.com/v1/customers";
        $this->session_post_code = "post_code_filter";
        $this->session_franchise_record = "franchise_post_code_filter";
        $this->sms_api_username = "nadeem@love2laundry.com";
        $this->sms_api_password = "xjkdw";
        $this->google_map_api_key = "AIzaSyBPzCxWhMOqi3mvnSHcs85MlXZw-NEiERg";
        $this->laundry_latitude = "51.4996389";
        $this->laundry_longitude = "-0.0529017";


        $library = SERVER . "library";
        $this->load->library($library);
        $class = ucfirst($library);
        $this->country = new $class();
    }

    /* public function GetLatitudeLongitude($postcode){
      $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($postcode)."&sensor=false";
      $result_string = file_get_contents($url);
      $result = json_decode($result_string, true);
      if(strtolower($result['status']) == "ok"){
      $result1[]=$result['results'][0];
      $result2[]=$result1[0]['geometry'];
      $result3[]=$result2[0]['location'];
      return $result3[0]['lat'] .',' . $result3[0]['lng'];
      }
      else{
      return "";
      }
      } */

    public function GetLatitudeLongitude($postcode) {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($postcode) . "&key=" . $this->google_map_api_key . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $suffix = substr($postcode, -3);
        //var_dump($result);die();
        if (strtolower($result['status']) == "ok") {
            $result1[] = $result['results'][0];
            $result2[] = $result1[0]['geometry'];
            $result3[] = $result2[0]['location'];
            return $result3[0]['lat'] . ',' . $result3[0]['lng'];
        } elseif ($suffix) {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($suffix) . "&key=" . $this->google_map_api_key . "&sensor=false";
            $result_string = file_get_contents($url);
            $result = json_decode($result_string, true);
            if (strtolower($result['status']) == "ok") {
                $result1[] = $result['results'][0];
                $result2[] = $result1[0]['geometry'];
                $result3[] = $result2[0]['location'];
                return $result3[0]['lat'] . ',' . $result3[0]['lng'];
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function GetDistanceMatrixFromAPI($origin, $destination) {
        $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $origin . "&destinations=" . $destination . "&mode=driving&language=en-EN&sensor=false&units=imperial&key=" . $this->google_map_api_key;
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $miles = 0;
        if (isset($result['rows'][0]['elements'][0]['status'])) {
            if (strtolower($result['rows'][0]['elements'][0]['status']) == "ok") {
                $miles_value = $result['rows'][0]['elements'][0]['distance']['text'];
                $miles_array = explode(" ", $miles_value);
                if ($miles_array[1] != "ft") {
                    $miles = floatval($miles_array[0]);
                }
            }
        }
        return $miles;
    }

    public function GetDirectionContentFromAPI($origin, $destination) {
        $response_result = "";
        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=" . urlencode($origin) . "&destination=" . urlencode($destination) . "&mode=driving&language=en-EN&sensor=false&units=imperial&key=" . $this->google_map_api_key;
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        if (isset($result['status'])) {
            if (strtolower($result['status']) == "ok") {
                $response_result = $result_string;
            }
        }
        return $response_result;
    }

    public function ValidatePostalCode($post_code) {
        /*
         * * This function takes in a string, cleans it up, and returns an array with the following information:
         * * ["validate"] => TRUE/FALSE
         * * ["prefix"] => first part of postcode
         * * ["suffix"] => second part of postcode
         */
        $post_code = str_replace(' ', '', $post_code); // remove any spaces;
        $post_code = strtoupper($post_code); // force to uppercase;
        $valid_postcode_exp = "/^(([A-PR-UW-Z]{1}[A-IK-Y]?)([0-9]?[A-HJKS-UW]?[ABEHMNPRVWXY]?|[0-9]?[0-9]?))\s?([0-9]{1}[ABD-HJLNP-UW-Z]{2})$/i";
        // set default output results (assuming invalid postcode):
        $output = array();
        $output['validate'] = FALSE;
        $output['prefix'] = '';
        $output['suffix'] = '';

        if (preg_match($valid_postcode_exp, strtoupper($post_code))) {
            $output['validate'] = TRUE;
            $suffix = substr($post_code, -3);
            $prefix = str_replace($suffix, '', $post_code);
            $output['prefix'] = $prefix;
            $output['suffix'] = $suffix;
        }
        return $output;
    }

    public function api_request($request_type, $url, $param_array = false) {
        try {
            $headers = array(
                'Authorization: Bearer ' . $this->GetStripeAPIKey(),
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            if ($request_type == "GET") {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            } else {
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param_array));
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CAINFO, realpath(dirname(__FILE__) . '/../config') . '/cacert.pem');
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

    //Teamid 21339
    public function tookan_api_post_request($url, $param_array) {
        try {
            $header = array();
            $header[] = 'Content-Type: application/json';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch, APPPATH.'config/cacert.pem');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param_array);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

    public function AddSessionItem($name, $value) {
        $this->session->set_userdata($name, $value);
    }

    public function AddSessionItems($data) {
        $this->session->set_userdata($data);
    }

    public function GetSessionItem($session_item_name) {
        if ($this->session->userdata($session_item_name) === null) {
            return null;
        } else {
            return $this->session->userdata($session_item_name);
        }
    }

    public function IsSessionItem($session_item_name) {
        if ($this->session->userdata($session_item_name) == null) {
            return false;
        } else {
            return true;
        }
    }

    public function RemoveSessionItems($data) {
        $this->session->unset_userdata($data);
    }

    public function IsAdminLogin() {
        if ($this->session->userdata('is_love_2_laundry_admin_logged_in') === null) {
            return false;
        } else {
            return true;
        }
    }

    function IsAdminLoginRedirect() {
        if ($this->IsAdminLogin() === false) {
            redirect('admin/login');
        }
    }

    public function GetCurrentAdminID() {
        return $this->GetSessionItem('admin_love_2_laundry_id');
    }

    /* Hassan changes 23-01-2018 */

    public function GetCurrentFranchiseID() {
        $FranchiseID = $this->GetSessionItem('admin_love_2_laundry_detail');
        return (isset($FranchiseID['FKFranchiseID']) && $FranchiseID['FKFranchiseID'] > 0) ? $FranchiseID['FKFranchiseID'] : 0;
    }

    /* ends */

    public function GetCurrentAdminDetail() {
        return $this->GetSessionItem('admin_love_2_laundry_detail');
    }

    function IsCurrentAdminAccess($page_url) {
        if ($this->model_database->IsAccessToPage($page_url, $this->GetCurrentAdminID()) === false) {
            redirect(site_url('admin/error/unauthorized'));
        }
    }

    public function MaskedInput($value, $length) {
        $masked = str_pad(substr($value, $length), strlen($value), 'X', STR_PAD_LEFT);
        return $masked;
    }

    public function GetSettingRecord($title) {
        return $this->model_database->GetRecord($this->tbl_settings, 'Content', array('Title' => $title));
    }

    public function GetWidgetSectionRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_widget_sections, 'PKSectionID as ID,Title', array($column_name => $column_value));
    }

    public function GetWidgetRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_widgets, 'PKWidgetID as ID,Title,ImageName,Content,Link', array($column_name => $column_value));
    }

    public function GetWidgetNavigationRecords($section_id, $widget_limit = false) {
        $widget_array = array();
        $widget_navigation_records = $this->model_database->GetRecords($this->tbl_widget_navigations, "R", 'FKWidgetID', array('FKSectionID' => $section_id), $widget_limit, false, false, 'Position', 'asc');
        if (sizeof($widget_navigation_records) > 0) {
            foreach ($widget_navigation_records as $key => $value) {
                $widget_record = $this->GetWidgetRecord("PKWidgetID", $value['FKWidgetID']);
                if ($widget_record !== false) {
                    $widget_array[] = $widget_record;
                }
            }
        }
        return $widget_array;
    }

    public function GetNavigationSectionRecord($column_name, $column_value) {
        return $this->model_database->GetRecord($this->tbl_navigation_sections, 'PKSectionID as ID,Title', array($column_name => $column_value));
    }

    function GenerateMenus($section_id, $limit = false) {
        return $this->model_database->GetRecords($this->tbl_navigations, "R", 'PKNavigationID,FKPageID,Name as PageName,LinkWithUrl as PageURL', array('FKSectionID' => $section_id), $limit, false, false, 'Position', 'asc');
    }

    public function GetEmailHeaderMenus() {
        $menu_html = "";
        $menu_records = $this->GenerateMenus(5);
        if (sizeof($menu_records) > 0) {
            foreach ($menu_records as $menu) {
                $menu_html .= '<li style="display:block; float:left;margin-left:0px;"><a href="' . CreateWebURL($menu['PageURL']) . '" target="_blank" style="color:#fff;text-transform:uppercase;padding:10px;font-size:12px;text-decoration:none;border:1px solid #fff;margin:0 5px;display:block">' . $menu['PageName'] . '</a></li>';
            }
        }
        return $menu_html;
    }

    public function GetEmailFooterMenus() {
        $menu_html = "";
        $menu_records = $this->GenerateMenus(6);
        if (sizeof($menu_records) > 0) {
            $menu_html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            $menu_total_count = round(sizeof($menu_records) / 4);
            $menu_count = 0;
            foreach ($menu_records as $menu) {
                if ($menu_count == 0) {
                    $menu_html .= '<tr>';
                }
                $menu_html .= '<td><a href="' . CreateWebURL($menu['PageURL']) . '" target="_blank" style="color:#fff;text-transform:capitalize;font-size:11px;text-decoration:none;">' . $menu['PageName'] . '</a></td>';
                $menu_count += 1;
                if ($menu_count == $menu_total_count) {
                    $menu_html .= '</tr>';
                    $menu_count = 0;
                }
            }
            if ($menu_count != 0) {
                $menu_html .= '</tr>';
            }
            $menu_html .= '</table>';
        }
        return $menu_html;
    }

    public function ReplaceTags($content, $field_array) {
        $email_tags = $this->model_database->GetEmailTags($field_array);
        foreach ($email_tags as $v) {
            $Tag = $v["Tag"];
            $MapField = $v["Title"];
            $content = str_replace($Tag, $field_array[$MapField], $content);
        }
        return $content;
    }

    // Get Header Title
    public function GetHeaderTitle() {
        $setting_record = $this->GetSettingRecord('Admin Header Title');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? $this->config->item('admin_header_title') : $setting_record['Content'];
        } else {
            return $this->config->item('admin_header_title');
        }
    }

    public function GetWebsiteEmailSender() {
        $setting_record = $this->GetSettingRecord("Website Email Sender");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == '#') ? '' : strtolower($setting_record['Content']);
        } else {
            return "";
        }
    }

    public function GetWebsiteEmailReceivers() {
        $setting_record = $this->GetSettingRecord("Website Email Receivers");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == '#') ? '' : strtolower($setting_record['Content']);
        } else {
            return "";
        }
    }

    public function GetWebsiteName() {
        $setting_record = $this->GetSettingRecord("Website Name");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return $this->config->item('website_name');
        }
    }

    public function GetWebsiteStatus() {
        $setting_record = $this->GetSettingRecord('Website Status');
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "Online";
        }
    }

    public function GetEmailFooterTitle() {
        $setting_record = $this->GetSettingRecord("Email Footer Title");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetWebsiteURLAddress() {
        $setting_record = $this->GetSettingRecord("Website URL Address");
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? '' : $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetGoogleAnalyticsID() {
        $setting_record = $this->GetSettingRecord('Google Analytics ID');
        if ($setting_record !== false) {
            return trim($setting_record['Content']);
        } else {
            return "#";
        }
    }

    public function GetWebsiteTelephoneNo() {
        $setting_record = $this->GetSettingRecord("Front Telephone Number");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetWebsiteEmailAddress() {
        $setting_record = $this->GetSettingRecord("Front Email Address");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetFacebookLink() {
        $setting_record = $this->GetSettingRecord("Facebook Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetTwitterLink() {
        $setting_record = $this->GetSettingRecord("Twitter Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetYouTubeLink() {
        $setting_record = $this->GetSettingRecord("You Tube Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetLinkedInLink() {
        $setting_record = $this->GetSettingRecord("Linkedin Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetPinterestLink() {
        $setting_record = $this->GetSettingRecord("Pinterest Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetGooglePlusLink() {
        $setting_record = $this->GetSettingRecord("Google Plus Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetInstagramLink() {
        $setting_record = $this->GetSettingRecord("Instagram Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetGooglePlayLink() {
        $setting_record = $this->GetSettingRecord("Google Play Store Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetAppleStoreLink() {
        $setting_record = $this->GetSettingRecord("Apple Store Link URL");
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "#";
        }
    }

    public function GetStripeMode() {
        $setting_record = $this->GetSettingRecord('Stripe Payment Mode');
        if ($setting_record !== false) {
            return $setting_record['Content'];
        } else {
            return "Test";
        }
    }

    public function GetStripeJQueryKey() {
        $setting_record = false;
        if ($this->GetStripeMode() == "Test") {
            $setting_record = $this->GetSettingRecord('Stripe JQuery Test Key');
        } else {
            $setting_record = $this->GetSettingRecord('Stripe JQuery Live Key');
        }
        if ($setting_record != false) {
            return $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetStripeAPIKey() {
        $setting_record = false;
        if ($this->GetStripeMode() == "Test") {
            $setting_record = $this->GetSettingRecord('Stripe API Test Key');
        } else {
            $setting_record = $this->GetSettingRecord('Stripe API Live Key');
        }
        if ($setting_record != false) {
            return $setting_record['Content'];
        } else {
            return "";
        }
    }

    public function GetOrderAmountLater() {
        $setting_record = $this->GetSettingRecord('Minimum Order Amount Later');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 15 : $setting_record['Content'];
        } else {
            return "15";
        }
    }

    public function GetOrderAmount() {
        $setting_record = $this->GetSettingRecord('Minimum Order Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 15 : $setting_record['Content'];
        } else {
            return "15";
        }
    }

    public function GetReferralCodeAmount() {
        $setting_record = $this->GetSettingRecord('Referral Code Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 5 : $setting_record['Content'];
        } else {
            return "5";
        }
    }

    public function GetLoyaltyCodeAmount() {
        $setting_record = $this->GetSettingRecord('Loyalty Code Amount');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 5 : $setting_record['Content'];
        } else {
            return "5";
        }
    }

    public function GetMinimumLoyaltyPoints() {
        $setting_record = $this->GetSettingRecord('Minimum Loyalty Points');
        if ($setting_record !== false) {
            return ($setting_record['Content'] == "#") ? 100 : $setting_record['Content'];
        } else {
            return "100";
        }
    }

    public function GetFullDayName($id) {
        $day_array = array(
            0 => 'Sunday',
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
        );
        return $day_array[$id];
    }

    public function SendMessage($responder_id, $invoice_number, $title, $number, $message) {
        try {
            $msg_array = array(
                'USER_NAME' => $this->sms_api_username,
                'PASSWORD' => $this->sms_api_password,
                'ORIGINATOR' => str_replace(" ", "", str_replace("Love2", "Lov2", $this->GetWebsiteName())),
                'RECIPIENT' => $number,
                'ROUTE' => "mglobal",
                'MESSAGE_TEXT' => $message
            );
            $url = "http://app.mobivatebulksms.com/gateway/api/simple/MT?" . http_build_query($msg_array, '', "&");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            $insert_sms_log = array(
                'PKSMSID' => $responder_id,
                'InvoiceNumber' => $invoice_number,
                'Title' => $title,
                'Content' => $message,
                'Phone' => $number,
                'Response' => $res,
                'IPAddress' => $this->input->ip_address(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $this->model_database->InsertRecord($this->tbl_sms_logs, $insert_sms_log);
            $xml = new SimpleXMLElement($res);
            return $xml;
        } catch (Exception $e) {
            return null;
        }
    }

    public function SendInvoiceEmail($invoice_number, $invoice_date_type) {
        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
        if ($invoice_record != false) {
            $email_field_array = array(
                'Email Sender' => "order@love2laundry.com",
                'Email Receivers' => "order@love2laundry.com",
                'Website Name' => $this->GetWebsiteName(),
                'Website URL' => $this->GetWebsiteURLAddress(),
                'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                'Website Email Address' => $this->GetWebsiteEmailAddress(),
                'Header Menus' => $this->GetEmailHeaderMenus(),
                'Footer Menus' => $this->GetEmailFooterMenus(),
                'Footer Title' => $this->GetEmailFooterTitle(),
                'Invoice Number' => $invoice_number,
                'Payment Method' => $invoice_record['PaymentMethod'],
                'Payment Status' => $invoice_record['PaymentStatus'],
                'Order Status' => $invoice_record['OrderStatus'],
                'Loyalty Points' => intval($invoice_record['GrandTotal']),
                'Pick Date Time' => date('d-m-Y', strtotime($invoice_record['PickupDate'])) . '<br/>' . $invoice_record['PickupTime'],
                'Delivery Date Time' => date('d-m-Y', strtotime($invoice_record['DeliveryDate'])) . '<br/>' . $invoice_record['DeliveryTime'],
                'Currency' => $invoice_record['Currency'],
                'Sub Total' => $invoice_record['SubTotal'],
                'Preference Total' => $invoice_record['PreferenceTotal'],
                'Discount Total' => $invoice_record['DiscountTotal'],
                'Grand Total' => $invoice_record['GrandTotal'],
                'Order Notes' => $invoice_record['OrderNotes'],
                'Account Notes' => $invoice_record['AccountNotes'],
                'Additional Instructions' => $invoice_record['AdditionalInstructions']
            );
            if ($invoice_record['DiscountType'] == "None") {
                $email_field_array['Discount Type'] = "Discount";
            } else {
                $email_field_array['Discount Type'] = $invoice_record['DiscountType'];
            }
            $discount_detail_html = "";
            if (!empty($invoice_record['DiscountCode'])) {
                $discount_detail_html .= '<br/><small>Code:' . $invoice_record['DiscountCode'] . '</small>';
                if ($invoice_record['DType'] == "Percentage") {
                    $discount_detail_html .= '<br/><small>Worth:' . $invoice_record['DiscountWorth'] . '%</small>';
                } else if ($invoice_record['DType'] == "Price") {
                    $discount_detail_html .= '<br/><small>Worth:&pound;' . $invoice_record['DiscountWorth'] . '</small>';
                }
            }
            $email_field_array['Discount Detail'] = $discount_detail_html;
            $address_html = "";
            if ($invoice_record['Location'] != "Add" && $invoice_record['Location'] != "0" && $invoice_record['Location'] != null) {
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['Location'] . '</p>';
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['Locker'] . '</p>';
            } else {
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['BuildingName'] . '</p>';
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['StreetName'] . '</p>';
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['PostalCode'] . '</p>';
                $address_html .= '<p style="margin: 0; font-size: 14px;">' . $invoice_record['Town'] . '</p>';
            }
            $email_field_array['Address'] = $address_html;
            if ($invoice_date_type == "C") {
                $email_field_array['Date Time Type'] = "Created Date Time";
                $email_field_array['Date Time'] = date('d-m-Y', strtotime($invoice_record['CreatedDateTime'])) . '<br/>' . date('h:i a', strtotime($invoice_record['CreatedDateTime']));
            } else {
                $email_field_array['Date Time Type'] = "Updated Date Time";
                $email_field_array['Date Time'] = date('d-m-Y', strtotime($invoice_record['UpdatedDateTime'])) . '<br/>' . date('h:i a', strtotime($invoice_record['UpdatedDateTime']));
            }
            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('PKMemberID' => $invoice_record['FKMemberID']));
            if ($member_record != false) {
                $email_field_array['Full Name'] = $member_record['FirstName'] . ' ' . $member_record['LastName'];
                $email_field_array['Email Address'] = $member_record['EmailAddress'];
                $email_field_array['Phone Number'] = $member_record['Phone'];
            }
            $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoiceServiceID", "asc");
            $invoice_services_html = "";
            if (sizeof($invoice_service_records) > 0) {
                $invoice_services_html = '<h4 style="margin-bottom:0">Shopping Cart</h4>';
                $invoice_services_html .= '<table border="1" cellpadding="5" cellspacing="0" bordercolor="#DDDDDD" style="margin-bottom: 20px; width: 100%;">';
                $invoice_services_html .= '<thead>';
                $invoice_services_html .= '<tr>';
                $invoice_services_html .= '<th width="55%" style="text-align:left">Title</th>';
                $invoice_services_html .= '<th width="15%" style="text-align:center">Quantity</th>';
                $invoice_services_html .= '<th width="15%" style="text-align:center">Price</th>';
                $invoice_services_html .= '<th width="15%" style="text-align:center">Total</th>';
                $invoice_services_html .= '</tr>';
                $invoice_services_html .= '</thead>';
                $invoice_services_html .= '<tbody>';
                foreach ($invoice_service_records as $record) {
                    $invoice_services_html .= '<tr>';
                    $invoice_services_html .= '<td>' . $record['Title'] . '</td>';
                    $invoice_services_html .= '<td style="text-align:right">' . $record['Quantity'] . '</td>';
                    $invoice_services_html .= '<td style="text-align:right">' . $invoice_record['Currency'] . $record['Price'] . '</td>';
                    $invoice_services_html .= '<td style="text-align:right">' . $invoice_record['Currency'] . $record['Total'] . '</td>';
                    $invoice_services_html .= '</tr>';
                }
                $invoice_services_html .= '</tbody>';
                $invoice_services_html .= '</table>';
            }
            $email_field_array['Invoice Service Table'] = $invoice_services_html;
            $invoice_preference_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", false, array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoicePreferenceID", "asc");
            $invoice_preferences_html = "";
            if (sizeof($invoice_preference_records) > 0) {
                $invoice_preferences_html = '<h4 style="margin-bottom:0">Customer Preferences</h4>';
                $invoice_preferences_html .= '<table  border="0" cellpadding="5" cellspacing="0" bordercolor="#DDDDDD" style="margin-bottom: 20px; width: 100%;">';
                $invoice_preferences_html .= '<tbody>';
                $up_area = "";
                $down_area = "";
                $count = 0;
                $bg_count = 0;
                foreach ($invoice_preference_records as $record) {
                    if ($record['ParentTitle'] == "Starch on Shirts") {
                        $down_area .= '<tr>';
                        $down_area .= '<td width="20%">' . $record['ParentTitle'] . '</td>';
                        $down_area .= '<td width="30%">' . $record['Title'] . '</td>';
                        $down_area .= '</tr>';
                    } else {
                        if ($count == 0) {
                            $up_area .= '<tr>';
                        }
                        $up_area .= '<td width="20%"';
                        if ($bg_count < 2) {
                            $up_area .= ' bgcolor="#EAEAEA"';
                        }
                        $up_area .= '>' . $record['ParentTitle'] . '</td>';
                        $up_area .= '<td width="30%"';
                        if ($bg_count < 2) {
                            $up_area .= ' bgcolor="#EAEAEA"';
                        }
                        $up_area .= '>' . $record['Title'] . '</td>';
                        if ($count == 1) {
                            $up_area .= '</tr>';
                            $count = 0;
                        } else {
                            $count = 1;
                        }
                        $bg_count += 1;
                        if ($bg_count == 4) {
                            $bg_count = 0;
                        }
                    }
                }
                $invoice_preferences_html .= '<tr>';
                $invoice_preferences_html .= '<th class="text-center" colspan="4" bgcolor="#444"> <h5 style="margin:0; text-transform:uppercase; color:#fff;"><strong>' . $this->config->item('front_member_laundry_first_heading_text') . '</strong></h5>';
                $invoice_preferences_html .= '</th>';
                $invoice_preferences_html .= '</tr>';
                $invoice_preferences_html .= $up_area;
                if (!empty($down_area)) {
                    $invoice_preferences_html .= '<tr>';
                    $invoice_preferences_html .= '<th class="text-center" colspan="4" bgcolor="#444"> <h5 style="margin:0; text-transform:uppercase; color:#fff;"><strong>' . $this->config->item('front_member_laundry_second_heading_text') . '</strong></h5>';
                    $invoice_preferences_html .= '</th>';
                    $invoice_preferences_html .= '</tr>';
                    $invoice_preferences_html .= $down_area;
                }
                $invoice_preferences_html .= '</tbody>';
                $invoice_preferences_html .= '</table>';
            }
            $email_field_array['Invoice Preference Table'] = $invoice_preferences_html;

            if ($invoice_date_type == "C") {
                $this->emailsender->send_email_responder(5, $email_field_array, false, $invoice_number);
                $this->emailsender->send_email_responder(10, $email_field_array, false, $invoice_number);
            } elseif ($invoice_date_type == "U") {
                $this->emailsender->send_email_responder(15, $email_field_array, false, $invoice_number);
                $this->emailsender->send_email_responder(16, $email_field_array, false, $invoice_number);
            } elseif ($invoice_date_type == "X") {
                $email_field_array['Email Receivers'] = $member_record['EmailAddress'];
                $this->emailsender->send_email_responder(17, $email_field_array, false, $invoice_number);
                $email_field_array['Email Receivers'] = "order@love2laundry.com";
                $this->emailsender->send_email_responder(17, $email_field_array, false, $invoice_number);
            }

            $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "EmailAddress", array('PKFranchiseID' => $invoice_record['FKFranchiseID']));
            if ($franchise_record != false) {

                $email_field_array['Email Receivers'] = $franchise_record['EmailAddress'];

                if ($invoice_date_type == "C") {

                    $this->emailsender->send_email_responder(10, $email_field_array, false, $invoice_number);
                } elseif ($invoice_date_type == "U") {

                    $this->emailsender->send_email_responder(16, $email_field_array, false, $invoice_number);
                } elseif ($invoice_date_type == "X") {
                    $this->emailsender->send_email_responder(17, $email_field_array, false, $invoice_number);
                }
            }
        }
    }

    public function GetParentMenuName($page_url) {
        $return_value = "";
        $current_menu_record = $this->model_database->GetRecord($this->tbl_menus, "PKMenuID,Name", array('AccessURL' => $page_url));
        if ($current_menu_record != false) {
            $current_admin_menu_record = $this->model_database->GetRecord($this->tbl_user_menus, "ParentMenuID", array('FKMenuID' => $current_menu_record['PKMenuID'], 'FKUserID' => $this->GetCurrentAdminID()));
            if ($current_admin_menu_record != false) {
                if ($current_admin_menu_record['ParentMenuID'] != 0) {
                    $current_parent_menu_record = $this->model_database->GetRecord($this->tbl_user_menus, "FKMenuID", array('PKAdminMenuID' => $current_admin_menu_record['ParentMenuID']));
                    if ($current_parent_menu_record != false) {
                        $menu_record = $this->model_database->GetRecord($this->tbl_menus, "Name", array('PKMenuID' => $current_parent_menu_record['FKMenuID']));
                        if ($menu_record != false) {
                            $return_value = $menu_record['Name'];
                        }
                    }
                } else {
                    $return_value = $current_menu_record['Name'];
                }
            }
        }
        return $return_value;
    }

    public function RandomPassword($length = 8) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    // Call view with Menu
    public function show_view_with_menu($view_name, $data = false) {
        $this->AddSessionItem("Last_Admin_Visited_Page_URL", current_url());
        $data['current_page_url'] = isset($this->uri->segments[2]) ? $this->uri->segments[2] : 'login';



        $data['parent_page_name'] = $this->GetParentMenuName($data['current_page_url']);


        if ($data['parent_page_name'] == "") {
            unset($data['parent_page_name']);
        }
        if ($this->IsSessionItem("AdminMessage")) {
            $data['admin_message'] = $this->GetSessionItem("AdminMessage");
            $this->RemoveSessionItems("AdminMessage");
        }

        $data['admin_detail'] = $this->GetCurrentAdminDetail();
        $data['admin_menus'] = $this->model_database->GetAdminMenus($this->GetCurrentAdminID());
        $data['header_title'] = $this->GetHeaderTitle();
        $data['website_name'] = $this->GetWebsiteName();
        $data['main_content'] = $view_name;
        $this->load->view("admin/master_template", $data);
    }

    // Image Manipulation
    public function create_image_thumb($result, $path, $width, $height) {
        $this->load->library('image_lib');
        $configResize = array(
            'image_library' => 'gd2',
            'source_image' => $result['upload_data']['full_path'],
            'new_image' => $path,
            'maintain_ratio' => TRUE,
            'width' => $width,
            'height' => $height
        );
        $this->image_lib->initialize($configResize);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

}
