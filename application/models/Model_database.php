<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_Database extends CI_Model {

    function __construct() {
        parent:: __construct();
    }

    function GetEmailTags($email_tag_array) {
        $this->db->select("Tag,Title");
        $this->db->from("cms_tags");
        $this->db->where_in('Title', array_keys($email_tag_array));
        $query = $this->db->get();
        return $query->result_array();
    }

    function GetParentMenus($admin_id) {
        if ($admin_id != "") {
            $sqlQuery = "SELECT menu.*,admin_menu.PKAdminMenuID as AdminMenuID FROM cms_menus menu";
            $sqlQuery .= " INNER JOIN cms_admin_menus admin_menu ON menu.PKMenuID = admin_menu.FKMenuID";
            $sqlQuery .= " WHERE admin_menu.ParentMenuID=0 and admin_menu.FKUserID={$admin_id} ORDER BY admin_menu.Position";
            $query = $this->db->query($sqlQuery);
            return $query->result_array();
        } else {
            return array();
        }
    }

    function GetAdminMenus($admin_id) {
        $tree = array();
        $res = $this->GetParentMenus($admin_id);
        foreach ($res as $key => $value) {
            $tree[$key] = array(
                'MenuID' => $value['PKMenuID'],
                'Name' => $value['Name'],
                'MenuIcon' => $value['MenuIcon'],
                'AccessURL' => $value['AccessURL']
            );
            $tree[$key]['children'] = $this->GetChildMenus($admin_id, $value['AdminMenuID']);
        }
        return $tree;
    }

    function GetChildMenus($admin_id, $menu_id) {
        $sqlQuery = "SELECT menu.PKMenuID as MenuID,menu.Name,menu.MenuIcon,menu.AccessURL FROM cms_menus menu";
        $sqlQuery .= " INNER JOIN cms_admin_menus admin_menu ON menu.PKMenuID = admin_menu.FKMenuID";
        $sqlQuery .= " WHERE admin_menu.ParentMenuID={$menu_id} and admin_menu.FKUserID={$admin_id} ORDER BY admin_menu.Position";
        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function GetUnassignedMenus($admin_id, $is_franchise = false) {
        $sqlQuery = "select PKMenuID as MenuID,Name,MenuIcon,AccessURL from cms_menus where PKMenuID not in (select FKMenuID from cms_admin_menus where FKUserID={$admin_id})";
        if ($is_franchise) {
            $sqlQuery = "select PKMenuID as MenuID,Name,MenuIcon,AccessURL from cms_menus where PKMenuID=16 and PKMenuID not in (select FKMenuID from cms_admin_menus where FKUserID={$admin_id})";
        }
        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function IsAccessToPage($page_url, $admin_id) {
        $sqlQuery = "SELECT menu.PKMenuID FROM cms_menus menu";
        $sqlQuery .= " INNER JOIN cms_admin_menus admin_menu ON menu.PKMenuID = admin_menu.FKMenuID";
        $sqlQuery .= " WHERE menu.AccessURL='{$page_url}' and admin_menu.FKUserID={$admin_id} limit 1";
        $query = $this->db->query($sqlQuery);
        if ($query->num_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    /* Hassan changes 23-01-2018 add params $FKFranchiseID */

    function GenerateTable($table_name, $column_name = false, $page_url, $page_type, $param = false, $FKFranchiseID = 0) {
        if ($column_name) {
            $this->datatables->select($column_name);
        } else {
            $this->datatables->select("*");
        }
        if ($page_type == "Preference") {
            $this->datatables->from("ws_preferences as a");
        } else {
            $this->datatables->from($table_name);
        }

        if ($page_type == "Banner" || $page_type == "Page") {
            $this->datatables->edit_column('Image', '$1', 'ImagePrint(Image,' . $this->config->item('path_upload_banner_thumb') . ')');
        } else if ($page_type == "Category") {
            $this->datatables->edit_column('MobileImageName', '$1', 'ImagePrint(MobileImageName,' . $this->config->item('path_upload_category_thumb') . ')');
        } else if ($page_type == "Service") {
            $this->datatables->edit_column('DesktopImageName', '$1', 'ImagePrint(DesktopImageName,' . $this->config->item('path_upload_service_thumb') . ')');
            $this->datatables->edit_column('MobileImageName', '$1', 'ImagePrint(MobileImageName,' . $this->config->item('path_upload_service_thumb') . ')');
            $this->datatables->join('ws_categories', 'ws_categories.PKCategoryID=ws_services.FKCategoryID', 'INNER');
        } else if ($page_type == "Preference") {
            $this->datatables->join('ws_preferences as b', 'a.ParentPreferenceID=b.PKPreferenceID', 'LEFT');
            $this->datatables->edit_column('ParentTitle', '$1', 'BlankToNone(ParentTitle)');
            $this->datatables->edit_column('Price', '$1', 'BlankToNone(Price)');
        } else if ($page_type == "Discount") {
            $this->datatables->join('ws_members', 'ws_members.PKMemberID=ws_discounts.FKMemberID', 'LEFT');
            $this->datatables->edit_column('EmailAddress', '$1', 'BlankToNone(EmailAddress)');
        } else if ($page_type == "Post Code Logs") {
            $this->datatables->join('ws_franchises', 'ws_franchises.PKFranchiseID=ws_search_post_codes.FKFranchiseID', 'LEFT');
            $this->datatables->edit_column('FranchiseTitle', '$1', 'BlankToNone(FranchiseTitle)');
        } else if ($page_type == "Widget") {
            $this->datatables->edit_column('Image', '$1', 'ImagePrint(Image,' . $this->config->item('path_upload_widget_thumb') . ')');
        } else if ($page_type == "Member") {
            $this->datatables->edit_column('ReferralCode', '$1', 'BlankToNone(ReferralCode)');
            $this->datatables->edit_column('TotalLoyaltyPoints', '$1', 'MemberLoyaltyCalculate(TotalLoyaltyPoints,UsedLoyaltyPoints)');
        } else if ($page_type == "Unavailable Time") {
            $this->datatables->where(array('FKFranchiseID' => $param));
            $this->datatables->edit_column('DisableDateTo', '$1', 'BlankToNone(DisableDateTo)');
        } else if ($page_type == "Administrator") {
            //$this->datatables->where(array('PKUserID !=' => $param));
            $this->datatables->where(array('FKFranchiseID' => 0));
        } else if ($page_type == "User") {
            $this->datatables->where(array('FKFranchiseID !=' => 0));
            $this->datatables->join('ws_franchises', 'ws_franchises.PKFranchiseID=cms_users.FKFranchiseID', 'INNER');
        } else if ($page_type == "Invoice") {
            $this->datatables->join('ws_members', 'ws_members.PKMemberID=ws_invoices.FKMemberID', 'INNER');
            $this->datatables->join('ws_franchises', 'ws_franchises.PKFranchiseID=ws_invoices.FKFranchiseID', 'INNER');
            if ($FKFranchiseID == 0) {
                $this->datatables->edit_column('UpdatedDateTime', '$1', 'BlankToNone(UpdatedDateTime)');
            }

            $this->datatables->edit_column('CheckID', '$1', 'CreateCheckBox(CheckID)');

            if ($FKFranchiseID == 0) {
                $this->datatables->edit_column('Regularly', '$1', 'CreateRegularity(Regularly)');
            }

            if ($param) {
                $this->datatables->where($param);
            }
        } else if ($page_type == "PaymentInvoice") {
            $this->datatables->join('ws_members', 'ws_members.PKMemberID=ws_invoices.FKMemberID', 'INNER');
            $this->datatables->join('ws_franchises', 'ws_franchises.PKFranchiseID=ws_invoices.FKFranchiseID', 'INNER');
            //$this->datatables->edit_column('UpdatedDateTime','$1','BlankToNone(UpdatedDateTime)');
            $this->datatables->edit_column('CheckID', '$1', 'CreateCheckBox(CheckID)');
            if ($param) {
                $this->datatables->where($param);
            }
        } else if ($page_type == "Customer1") {
            $this->datatables->distinct("EmailAddress");
            $this->datatables->join('ws_invoices', 'ws_members.PKMemberID=ws_invoices.FKMemberID', 'LEFT');
            $this->datatables->where("ws_invoices.FKMemberID is null");
        } else if ($page_type == "Customer2") {
            //$this->datatables->distinct("EmailAddress");
            // $this->datatables->join('ws_invoices', 'ws_members.PKMemberID=ws_invoices.FKMemberID AND  `ws_invoices`.`PaymentStatus` =  "Completed" And ws_invoices.CreatedDateTime >= NOW()-INTERVAL 1 MONTH', 'LEFT');
            //$this->datatables->where("ws_invoices.PKInvoiceID is null");
            //$this->datatables->group_by("ws_members.EmailAddress");
            $this->datatables->where("`PKMemberID` NOT IN (SELECT distinct(`FKMemberID`) FROM `ws_invoices` where `PaymentStatus`='Completed' and `CreatedDateTime` >= NOW() - INTERVAL 1 MONTH and `FKMemberID` is not null)");
        }
        if ($page_type == "Franchise") {
            $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="time" href="' . site_url('admin/unavailabletimes/') . '?f=$1"><i class="fa fa-calendar"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" href="' . site_url('admin/' . $page_url . '/view/$1') . '"><i class="fa fa-eye"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="edit" href="' . site_url('admin/' . $page_url . '/edit/$1') . '"><i class="fa fa-pencil-square-o"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="remove" class="removeRow" data-type="' . $page_type . '" href="' . site_url('admin/' . $page_url . '/delete/$1') . '"><i class="fa fa-trash-o"></i></a></div></div>', 'ID');
        } else if ($page_type == "Invoice") {
            //$this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" target="_blank" href="' . site_url('order-detail/$1') . '?t=v"><i class="fa fa-eye"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="edit" href="' . site_url('admin/' . $page_url . '/edit/$1') .'"><i class="fa fa-pencil-square-o"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="remove" class="removeRow" data-type="' . $page_type . '" href="' . site_url('admin/' . $page_url . '/delete/$1') . '"><i class="fa fa-trash-o"></i></a></div></div>', 'ID');
            /* Hassan changes 23-01-2018 */
            if ($FKFranchiseID > 0) {
                $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" target="_blank" href="' . site_url('order-detail/$1') . '?t=v"><i class="fa fa-eye"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="edit" href="' . site_url('admin/' . $page_url . '/edit/$1') . '"><i class="fa fa-pencil-square-o"></i></a></div>', 'ID');
            } else {
                $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" target="_blank" href="' . site_url('order-detail/$1') . '?t=v"><i class="fa fa-eye"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="View Invoice PDF" target="_blank" href="' . site_url('admin/' . $page_url . '/pdf/$1') . '?t=v"><i class="fa fa-file"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="edit" href="' . site_url('admin/' . $page_url . '/edit/$1') . '"><i class="fa fa-pencil-square-o"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="remove" class="removeRow" data-type="' . $page_type . '" href="' . site_url('admin/' . $page_url . '/delete/$1') . '"><i class="fa fa-trash-o"></i></a></div></div>', 'ID');
            }
            /* ends */
        } else if ($page_type != "Post Code Logs" && $page_type != "Dashboard") {
            $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" href="' . site_url('admin/' . $page_url . '/view/$1') . '"><i class="fa fa-eye"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="edit" href="' . site_url('admin/' . $page_url . '/edit/$1') . '"><i class="fa fa-pencil-square-o"></i></a></div><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="remove" class="removeRow" data-type="' . $page_type . '" href="' . site_url('admin/' . $page_url . '/delete/$1') . '"><i class="fa fa-trash-o"></i></a></div></div>', 'ID');
        }
        if ($page_type == "PaymentInvoice") {
            if ($FKFranchiseID > 0) {
                $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" target="_blank" href="' . site_url('order-detail/$1') . '?t=v"><i class="fa fa-eye"></i></a></div>', 'CheckID');
            } else {
                $this->datatables->add_column('Options', '<div class="row"><div class="col-md-3 col-xs-2 col-lg-3 cursor"><a title="view" target="_blank" href="' . site_url('order-detail/$1') . '?t=v"><i class="fa fa-eye"></i></a></div></div>', 'CheckID');
            }
        }
        return $this->datatables->generate();
    }

    function GetRecords($table_name, $result_type = "R", $column_name = false, $params = false, $limit = false, $per_pg = false, $offset = false, $order_column = false, $order_type = 'desc') {
        if ($column_name) {
            $this->db->select($column_name);
        } else {
            $this->db->select("*");
        }
        if ($params) {
            $this->db->where($params);
        }
        if ($order_column) {
            $this->db->order_by($order_column, $order_type);
        }
        $this->db->from($table_name);
        if ($per_pg !== false) {
            $this->db->limit($per_pg, $offset);
            $query = $this->db->get();
            //$query = $this->db->get($table_name,$per_pg,$offset);
        } else {
            if ($limit) {
                $this->db->limit($limit);
            }
            $query = $this->db->get();
        }
        if ($result_type == "R") {
            return $query->result_array();
        } else {
            return $query->num_rows();
        }
    }

    function GetDistinctRecords($table_name, $result_type = "R", $column_name = false, $params = false, $limit = false, $order_column = false, $order_type = 'desc') {
        if ($column_name) {
            $this->db->select($column_name);
        } else {
            $this->db->select("*");
        }
        $this->db->from($table_name);
        if ($params) {
            $this->db->where($params);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        if ($order_column) {
            $this->db->order_by($order_column, $order_type);
        }
        $this->db->distinct();
        $query = $this->db->get();
        if ($result_type == "R") {
            return $query->result_array();
        } else {
            return $query->num_rows();
        }
    }

    function GetRecord($table_name, $column_name = false, $params = false) {
        if ($column_name) {
            $this->db->select($column_name);
        } else {
            $this->db->select("*");
        }
        $this->db->limit(1);
        $query = $this->db->get_where($table_name, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0];
        } else {
            return FALSE;
        }
    }

    function query($sql) {
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function InsertRecord($table_name, $data) {

        if ($this->db->insert($table_name, $data)) {
            return intval($this->db->insert_id());
        } else {
            $error = $this->db->error();
            $error['data'] = json_encode($data);
            log_message("error", $table_name . " " . json_encode($error));
            return false;
        }
    }

    function UpdateRecord($table_name, $data, $column_name) {
        $id = $data['ID'];
        unset($data['ID']);
        $this->db->update($table_name, $data, array($column_name => $id));
    }

    function RemoveRecord($table_name, $id, $column_name) {
        $this->db->delete($table_name, array($column_name => $id));
    }

    function GetFranchiseByPrefix($code) {
        $q = "SELECT fpc.`FKFranchiseID` FROM `ws_franchise_post_codes` as fpc join `ws_franchises` as f on f.PKFranchiseID = fpc.FKFranchiseID WHERE f.Status = 'Enabled' and fpc.`Code`='$code' limit 1";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result;
        } else {
            return false;
        }
    }

    function DashboardCustomer1Export() {
        $query = $this->db->query("SELECT DISTINCT `ws_members`.`FirstName`,`ws_members`.`LastName`,`ws_members`.`EmailAddress`,`ws_members`.`Phone` FROM `ws_members` LEFT JOIN `ws_invoices` ON `ws_members`.`PKMemberID`=`ws_invoices`.`FKMemberID` WHERE `ws_invoices`.`FKMemberID` is null ORDER BY `EmailAddress` DESC");
        return $query->result_array();
    }

    function DashboardCustomer2Export() {
        $query = $this->db->query("SELECT `ws_members`.`FirstName`,`ws_members`.`LastName`,`ws_members`.`EmailAddress`,`ws_members`.`Phone` from `ws_members` WHERE `PKMemberID` NOT IN (SELECT distinct(`FKMemberID`) FROM `ws_invoices` where `PaymentStatus`='Completed' and `CreatedDateTime` >= NOW() - INTERVAL 1 MONTH and `FKMemberID` is not null)");
        return $query->result_array();
    }

    /* Hassan changes */

    function GetRecordsInvoices($tbl_invoices = '', $where_clause = '') {
        $sqlQuery = "SELECT i.InvoiceNumber,i.OwnerPaymentStatus,i.PaymentStatus,i.OrderStatus,i.GrandTotal,CONCAT(i.PickupDate,' ',i.PickupTime) AS PickupDateTime,CONCAT(i.DeliveryDate,' ',i.DeliveryTime) AS DeliveryDateTime,i.CreatedDateTime,CONCAT(m.FirstName,m.LastName) AS EmailAddress,i.DiscountType,i.DiscountCode,i.DiscountWorth,i.DType,(Subtotal + PreferenceTotal ) as GrossTotal
			FROM `ws_invoices` i
			LEFT JOIN ws_members m ON i.FKMemberID=m.PKMemberID
			WHERE " . $where_clause;
        $query = $this->db->query($sqlQuery);
        $result = $query->result_array();
        return (isset($result) ? $result : array());
    }

    function FranchiseCustomServices($FKFranchiseID) {
        $query = $this->db->query("SELECT `ws_services`.`PKServiceID`,`ws_franchise_services`.`Title`,`ws_services`.`Status`,ws_franchise_services.Price,ws_franchise_services.DiscountPercentage,ws_franchise_services.PKFranchiseServiceID from `ws_services` JOIN ws_franchise_services ON ws_franchise_services.FKServiceID=ws_services.PKServiceID WHERE ws_franchise_services.FKFranchiseID=" . $FKFranchiseID . " AND `ws_services`.status ='Enabled'");
        return $query->result_array();
    }

    function FranchiseCustomServicesSelectItems($FKFranchiseID = 0, $PKCategoryID = 0) { //package, 
        $sql = "SELECT FKCategoryID as CategoryID,Content,DesktopImageName as DesktopImage,MobileImageName as MobileImage,IsPackage,PreferencesShow,ws_services.PKServiceID as ID,ws_services.PKServiceID,ws_franchise_services.Title,ws_services.Content,ws_franchise_services.Price,ws_franchise_services.DiscountPercentage,ws_services.DesktopImageName,ws_services.MobileImageName,ws_services.PreferencesShow,ws_services.IsPackage as Package from `ws_services` JOIN ws_franchise_services ON ws_franchise_services.FKServiceID=ws_services.PKServiceID WHERE  `ws_services`.status ='Enabled' AND ws_services.FKCategoryID=" . $PKCategoryID . " AND  ws_franchise_services.FKFranchiseID = " . $FKFranchiseID . " ORDER BY ws_services.Position asc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function FranchiseCustomServicesSelectItemsWithPercentage($FKFranchiseID = 0, $PKCategoryID = 0) { //package, 
        $sql = "SELECT FKCategoryID as CategoryID,Content,DesktopImageName as DesktopImage,MobileImageName as MobileImage,IsPackage,PreferencesShow,ws_services.PKServiceID as ID,ws_services.PKServiceID,ws_franchise_services.Title,ws_services.Content,ws_franchise_services.Price,ws_franchise_services.DiscountPercentage,(round(ws_franchise_services.Price-((ws_franchise_services.DiscountPercentage/100)*ws_franchise_services.Price),2)) as OfferPrice ,ws_services.DesktopImageName,ws_services.MobileImageName,ws_services.PreferencesShow,ws_services.IsPackage as Package from `ws_services` JOIN ws_franchise_services ON ws_franchise_services.FKServiceID=ws_services.PKServiceID WHERE  `ws_services`.status ='Enabled' AND ws_services.FKCategoryID=" . $PKCategoryID . " AND  ws_franchise_services.FKFranchiseID = " . $FKFranchiseID . " ORDER BY ws_services.Position asc";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function FranchiseCustomServicesSelectItemsApi($FKFranchiseID = 0, $PKCategoryID = 0) { //package, 
        $query = $this->db->query("SELECT FKCategoryID as CategoryID,Content,ws_services.DesktopImageName as DesktopImage,ws_services.MobileImageName as MobileImage,IsPackage,PreferencesShow,ws_services.PKServiceID as ID,ws_services.PKServiceID,ws_franchise_services.Title,ws_services.Content,ws_franchise_services.Price,ws_franchise_services.DiscountPercentage,ws_services.DesktopImageName,ws_services.MobileImageName,ws_services.PreferencesShow,ws_services.IsPackage as Package from `ws_services` JOIN ws_franchise_services ON ws_franchise_services.FKServiceID=ws_services.PKServiceID WHERE  `ws_services`.status ='Enabled' AND ws_services.FKCategoryID=" . $PKCategoryID . " AND  ws_franchise_services.FKFranchiseID = " . $FKFranchiseID . " ORDER BY ws_services.Position asc");
        return $query->result_array();
    }

}
