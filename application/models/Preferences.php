<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Preferences extends CI_Model {

    private $table = "ws_preferences";

    function __construct() {
        parent:: __construct();
    }

    public function where_in($column, $where) {
        
        $this->db->where_in($column, $where);
        $query =$this->db->get($this->table);
        return $query->result_array();
    }
    
    public function get_all() {
        $sqlQuery="SELECT p.`PKPreferenceID`,p.`ParentPreferenceID`,p.Title,c.Title as parentTitle,p.Price,p.`PriceForPackage` FROM `ws_preferences` p left join ws_preferences c on c.`PKPreferenceID`= p.`ParentPreferenceID` WHERE p.`ParentPreferenceID`>0";
        $query = $this->db->query($sqlQuery);
        return $query->result_array();        
    }
    
    

}
