<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends CI_Model {

    private $table = "carts";

    function __construct() {
        parent:: __construct();
    }

    public function insert($data) {

        $data["created_at"] = date("Y-m-d H:i:s");
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function first($data) {

        $this->db->where($data);
        $query = $this->db->get($this->table, 1);
        if (!empty($query->result_array())) {
            $result = $query->result_array();
            return $result[0];
        } else {
            return array();
        }
    }

    public function get($data, $limit = 1, $offset = 0) {

        $this->db->where($data);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function update($data, $where) {
        $data["updated_at"] = date("Y-m-d H:i:s");
        return $this->db->update($this->table, $data, $where);
    }

    public function delete($where) {
        return $this->db->delete($this->table, $where);
    }

    function deleteAllExceptCurrentPostalCode($postalCode, $sessionId) {

        echo $sqlQuery = "select * from cart where postal_code!='$postalCode' and session_id='$sessionId'";
        die;
        
        //return $query;
    }

}
