<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cartservices extends CI_Model {

    private $table = "cart_services";

    function __construct() {
        parent:: __construct();
    }

    public function insert($data) {

        $data["created_at"] = date("Y-m-d H:i:s");
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get($data, $limit = 1, $offset = 0) {

        $this->db->where($data);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_services($where) {

        $this->db->select('`cart_services`.`id` as `cart_services_id`,cart_services.*,carts.*');
        $this->db->from($this->table);
        $this->db->join('carts', 'carts.id=' . $this->table . '.cart_id');
        $this->db->where('carts.id', $where['cart_id']);
        if (isset($where['postal_code'])) {
            $this->db->where('carts.postal_code', $where['postal_code']);
        }


        $query = $this->db->get();

        return $query->result();
    }

    public function update($data, $where) {
        $data["updated_at"] = date("Y-m-d H:i:s");
        return $this->db->update($this->table, $data, $where);
    }

    public function delete($where) {
        return $this->db->delete($this->table, $where);
    }

    
    public function has_preferences($where) {
        
        $ls = $this->table . '.category';
        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('carts', 'carts.id=' . $this->table . '.cart_id');
        $this->db->where('carts.id', $where['cart_id']);
        if (isset($where['postal_code'])) {
            $this->db->where('carts.postal_code', $where['postal_code']);
        }
        
        $where_au = "($ls = 'Bundles' OR $ls = 'LAUNDRY')";
        $this->db->where($where_au);

        $query = $this->db->get();
        
        if (count($query->result()) > 0) {
            $hasPreferences = true;
        } else {
            $hasPreferences = false;
        }

        return $hasPreferences;
    }

}
