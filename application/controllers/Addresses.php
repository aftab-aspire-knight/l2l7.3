<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Addresses extends Base_Controller {

    function __construct() {
        parent::__construct();
    }

    function google() {
        $post = $this->input->get(NULL, TRUE);
        $code = urlencode($post["code"]);
        $key = $this->config->item("google_map_api_key");
        $latitude = $this->config->item("laundry_latitude");
        $longitude = $this->config->item("laundry_longitude");
        $countryCode = $this->config->item("iso_country_code");

        $reg = ("&region=" . $countryCode);

        $q = ("key=" . $key . "&input=" . $code . "&types=sublocality&language=en&location=" . $latitude . "," . $longitude . "&radius=500" . $reg . "&strictbounds");
        $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?" . $q;
        $response = $this->get_curl_request($url);
        $arr = json_decode($response, true);
        
        $predictions = array();
        if ($arr['status'] == "OK") {
            $predictions = $arr['predictions'];
        }
        $data['predictions'] = $predictions;
        $data['is_order_page'] = true;
        $this->load->view('booking/google_addresses', $data);
    }

}
