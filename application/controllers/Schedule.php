<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Schedule extends Base_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('user_agent');
        $this->load->helper('common');
        $this->load->library('stripe');


        if ($this->IsMemberLogin() === false) {
            redirect('/');
            die;
        }
    }

    function index() {

        $current_url = "schedule";
        $member_id = $this->GetCurrentMemberID();

        $member = $this->GetCurrentMemberDetail();
        $schedules = $this->model_database->GetRecords("schedules", "R", false, array('member_id' => $member_id));

        $data["schedules"] = $schedules;
        $data["member"] = $member;
        $data["member_id"] = $member_id;
        $data["current_url"] = $current_url;
        $this->show_view_with_menu('schedule/list', $data);
    }

    function order() {
        $current_url = "schedule";
        $member_id = $this->GetCurrentMemberID();

        $member = $this->GetCurrentMemberDetail();

        $stripe = new Stripe();

        $error = false;
        if (empty($member["Phone"])) {
            $error = true;
            // d($member,1);
            // $response["errors"]["payment_phone"] = $response['message'] = "Please enter your phone number.";
        } else {
            if ($this->verifyPhoneNumber($member["CountryCode"], $member["Phone"]) == false) {
                $error = true;
                //$response["errors"]["payment_phone"] = $response['message'] = "Please enter correct phone number.";
            }
        }
        $stripe_customer_id = $member['StripeCustomerID'];
        $dataIntent = array(
            'payment_method_types' => ['card'],
            'usage' => "off_session",
            //'confirm' => true,
            'customer' => $stripe_customer_id,
        );


        $stripeKey = $this->GetStripeAPIKey();
        try {

            $setupIntent = $stripe->saveSetupIntent($stripeKey, $dataIntent);
            //d($setupIntent,1);
            $data["client_secret"] = $setupIntent->client_secret;
            $data["setup_intent_id"] = $setupIntent->id;
        } catch (Stripe\Exception\CardException $e) {
            $response['exception'] = "CardException";
            $response['errors']["payment_error"] = $e->getMessage();
        }
        //d($response,1);

        $data['member_id'] = $member_id;
        $data['member'] = $member;

        if ($error == true) {
            $current_url = "schedule_phone";
            $this->show_view_with_menu('schedule/phone', $data);
        } else {

            $vat = $this->GetAppVat();
            $date = date("Y-m-d H:i:s");

            $data["vat"] = $vat;

            $data['postal_code'] = $member["PostalCode"];
            $data['address'] = $member["BuildingName"];
            $data['address2'] = $member["StreetName"];
            $data['date'] = $date;

            $franchise_id = 0;

            if (!empty($data['postal_code'])) {
                $short_post_code_array = $this->country->ValidatePostalCode($data['postal_code']);
                $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code_array['prefix']);
                if (isset($franchise_post_code_record['FKFranchiseID'])) {
                    $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                }
            }

            $sql = "select PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code from " . $this->tbl_member_cards . " where FKMemberID=" . $member_id . " and CreatedDateTime > curdate() - interval (dayofmonth(curdate()) - 1) day - interval 6 month order by PKCardID desc limit 6;";

            $cards = $this->model_database->query($sql);

            $data['cards'] = $cards;

            $data['franchise_id'] = $franchise_id;

            $data["postal_code_type"] = $this->config->item("postal_code_type");
            $data['current_url'] = $current_url;

            //$sql = "SELECT * FROM `schedules` where member_id= " . $member_id . " limit 1;";
            //$schedules = $this->model_database->query($sql);
            //$schedule = $this->model_database->GetRecord("schedules", false, array('member_id' => $member_id));

            $data["schedule"] = [];
            $this->show_view_with_menu('schedule/create', $data);

            /*
              if (empty($schedule)) {

              } else {
              $current_url = "schedule_edit";
              $this->show_view_with_menu('schedule/edit', $data);
              }
             */
        }
    }

    function updatephone() {
        $post = $this->input->post(NULL, TRUE);

        $phone = $post['phone_number'];
        $countryCode = $post['countryCode'];
        $member_id = $this->GetCurrentMemberID();
        //$member = $this->GetCurrentMemberDetail();
        $header = 400;
        $response["error"] = false;
        if (empty($phone)) {
            $response["error"] = true;
            $response["errors"]["payment_phone"] = $response['message'] = "Please enter your phone number.";
        } else {
            if ($this->verifyPhoneNumber($countryCode, $phone) == false) {
                $response["error"] = true;
                $response["errors"]["payment_phone"] = $response['message'] = "Please enter correct phone number.";
            }
        }


        if ($response["error"] == false) {
            $update_member = array(
                //'ReferralCode' => $post_data['referral_code'],
                'Phone' => $phone,
                'CountryCode' => $countryCode,
                'ID' => $member_id,
                'UpdatedDateTime' => date('Y-m-d H:i:s')
            );
            $this->_CreateMemberSession($member_id);
            $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");

            $response["member_id"] = $member_id;
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
            if ($member_record !== false) {
                $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
            }
            $header = 200;
        }
        echo jsonencode($response, $header);
    }

    function confirmorder() {

        $post = $this->input->post(NULL, TRUE);

        $member_id = $this->GetCurrentMemberID();
        $member = $this->GetCurrentMemberDetail();

        $client_secret = $post["client_secret"];
        $setup_intent_id = $post["setup_intent_id"];

        $response["error"] = false;
        $response["errors"] = array();
        $header = 400;
        $postal_code_type = $this->config->item("postal_code_type");

        if ($postal_code_type["title"] == "Area") {
            $post['addresses'] = $post['postal_code'];
        }

        //d($post,1);

        $hasAddress = NULL;
        if (empty($post['addresses'])) {
            $response["error"] = true;
            $response["errors"]["address"] = "Choose your address.";
        } else if ($post['addresses'] == "-1") {

            if (empty($post['address'])) {
                $response["error"] = true;
                $response["errors"]["address"] = "Please enter your address.";
            }
            $address = $post['address'];
            $hasAddress = $post['addresses'];
        } else {
            $address = $post['addresses'];
            $post['city'] = end(explode(",", $address));
        }

        if (empty($post["addresses"])) {
            $response["error"] = true;
            $response["errors"]["franchise_id"] = "Please enter correct address";
        }

        if (empty($post["franchise_id"])) {
            $response["error"] = true;
            $response["errors"]["franchise_id"] = "Please enter correct address";
        }

        if (!is_numeric($post["schedule"])) {
            $response["error"] = true;
            $response["errors"]["schedule"] = "Please select collection schedule";
        }

        if (empty($member["Phone"])) {
            $response["error"] = true;
            $response["errors"]["payment_phone"] = $response['message'] = "Please enter your phone number.";
        } else {
            if ($this->verifyPhoneNumber($member["CountryCode"], $member["Phone"]) == false) {
                $response["error"] = true;
                $response["errors"]["payment_phone"] = $response['message'] = "Please enter correct phone number.";
            }
        }

        if ($response["error"] == false) {

            $franchise_id = $post["franchise_id"];
            $town = $post["town"];
            $address = str_replace(", " . $town, "", $address);
            $type = $post["payment_type"];
            $payment_id = $post["payment_method_id"];
            $stripe_customer_id = $member['StripeCustomerID'];

            if ($type == "new") {

                $data = array(
                    'payment_method_types' => ['card'],
                    "payment_method" => $post['payment_method_id'],
                    "customer" => $stripe_customer_id,
                    'confirm' => true,
                    'usage' => "off_session",
                );

                $res = $this->saveCard($member_id, $post['payment_method_id'], $setup_intent_id, $data);
                $card_id = $res["id"];
            } else {
                $card_id = $payment_id;
            }

            //d($post, 1);
            $scheduleData = array(
                "member_id" => $member_id,
                "card_id" => $card_id,
                "postcode" => $post["postal_code"],
                "franchise_id" => $franchise_id,
                "address" => $address,
                "address2" => $post["address2"],
                "town" => $town,
                "schedule" => $post["schedule"],
                "pickup_time" => $post["pickup_time"],
                "created_at" => date("Y-m-d H:i:s"),
            );
            //d($scheduleData, 1);

            $id = $this->model_database->InsertRecord("schedules", $scheduleData);
            $response["id"] = $id;
            $header = 200;
        }
        echo jsonencode($response, $header);
    }

    function saveCard($member_id, $payment_method_id, $setup_intent_id, $data) {

        $header = 400;
        try {
            $stripe = new Stripe();
            $stripeKey = $this->GetStripeAPIKey();

            $params["id"] = $setup_intent_id;
            $params["payment_method"] = $payment_method_id;

            $payment_method = $stripe->getPaymentMethod($stripeKey, $payment_method_id);
            //d($payment_method);

            $member_card = array(
                'FKMemberID' => $member_id,
                'Title' => "************" . $payment_method->card->last4,
                'Name' => "************" . $payment_method->card->last4,
                'Number' => $payment_method->card->last4,
                'Month' => $payment_method->card->exp_month,
                'Year' => $payment_method->card->exp_year,
                'Code' => $payment_method_id,
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );

            $attach = $payment_method->attach(['customer' => $data["customer"]]);


            $setupIntent = $stripe->confirmSetupIntent($stripeKey, $setup_intent_id, $payment_method_id);


            $card_id = $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
            $response["error"] = false;
            $response["message"] = "Card Added Successfully.";
            //$response["intent"] = $setupIntent;
            $response["id"] = $card_id;
            $header = 200;
        } catch (Stripe\Exception\CardException $e) {
            $response['exception'] = "CardException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['exception'] = "ApiErrorException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['exception'] = "RateLimitException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['exception'] = "ApiConnectionException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['exception'] = "InvalidRequestException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\UnknownApiErrorException $e) {
            $response['exception'] = "UnknownApiErrorException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception $e) {
            $response['exception'] = "Exception";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\PermissionException $e) {
            $response['exception'] = "PermissionException";
            $response['errors']["payment_error"] = $e->getMessage();
        } catch (Stripe\Exception\UnexpectedValueException $e) {
            $response['exception'] = "UnexpectedValueException";
            $response['errors']["payment_error"] = $e->getMessage();
        }
        return $response;
    }

    function confirmdeleteschedule() {

        $post = $this->input->post(NULL, TRUE);
        $current_url = "schedule";
        $member_id = $this->GetCurrentMemberID();
        $member = $this->GetCurrentMemberDetail();

        $id = $post["id"];
        $header = 200;
        $sql = "delete from schedules where member_id=" . $member_id . " and id=" . $id;
        $this->db->query($sql);
        $response["error"] = false;
        $response["message"] = "Schedule deleted successfully.";
        echo jsonencode($response, $header);
    }

}
