<?php
// ALTER TABLE `carts` ADD `address_type` VARCHAR(50) NULL DEFAULT '' AFTER `has_address`;
// ALTER TABLE `ws_invoices` ADD `AddressType` VARCHAR(50) NULL DEFAULT '' AFTER `HasAddress`;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Booking extends Base_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('cart');
        $this->load->model('cartservices');
        $this->load->database();

        //$this->load->library('onfleet');
        // $onfleet = new Onfleet();
        // d($onfleet->task_create(), 1);
    }

    public function validatepostcode()
    {
        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $post_data = $this->input->post(NULL, TRUE);

        $this->RemoveSessionItems("invoice_type");
        $this->RemoveSessionItems("admin_invoice_id");
        $this->RemoveSessionItems("member_invoice_id");


        $header = 400;
        $response["error"] = true;
        $response["error_type"] = "postcode";

        $data["postal_code_type"] = $this->config->item("postal_code_type");
        if (isset($post_data['postcode']) && !empty($post_data['postcode'])) {
            $is_found = true;



            $short_post_code_array = $this->country->ValidatePostalCode($post_data['postcode']);
            if ($short_post_code_array['validate'] == false) {
                $response["errors"]["postal_code"] = $data["postal_code_type"]["validation_message"];
                $is_found = false;
            } else {

                $short_post_code = $short_post_code_array['prefix'];
                $short_suffix = $short_post_code_array['suffix'];
                $tmpPostCode = $short_post_code . $short_suffix;

                $addresses = $this->country->getAddresses($post_data['postcode']);

                if ($addresses["error"] == true) {
                    $response["error_type"] = "t";
                    $response["url"] = base_url($this->config->item('front_post_code_not_found_page_url'));
                    $is_found = false;
                } else {

                    $post_code = $post_data['postcode'];
                    $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code_array['prefix']);

                    if ($franchise_post_code_record != false) {
                        $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                    } else {
                        $franchise_id = 0;
                        $response["message"] = $this->config->item('validation_message');
                        $is_found = false;
                    }
                    $insert_search_post_code = array(
                        'FKFranchiseID' => $franchise_id,
                        'Code' => $post_code,
                        'IPAddress' => $this->input->ip_address(),
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                    if ($franchise_id != 0) {
                        $franchise_record = $this->_GetFranchiseRecord($franchise_id);
                        if ($franchise_record != false) {

                            $response["franchise_id"] = $franchise_id;
                            $response["minimum_order_amount"] = $franchise_record["MinimumOrderAmount"];

                            $this->AddSessionItem("franchise_id", $franchise_id);
                            $this->AddSessionItem($this->session_post_code, $post_code);
                            $this->AddSessionItem($this->session_franchise_record, $franchise_record);
                        } else {
                            $is_found = false;
                        }
                    } else {
                        $is_found = false;
                    }
                }
            }
            if ($is_found) {
                $header = 200;
                $response["error"] = false;
                $response["postal_code"] = $post_code;
                unset($response["error_type"]);
                $response["url"] = base_url($this->config->item('front_select_item_page_url'));
            } else {
                $response["errors"]["postal_code"] = $data["postal_code_type"]['validation_message'];
            }
        } else {
            $response["errors"]["postal_code"] = $data["postal_code_type"]['validation_message'];
        }
        echo jsonencode($response, $header);
        die;
    }

    public function thankyou()
    {

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $data['current_url'] = $current_url;
       
            //$this->IsSessionItem("Invoice_Success")
        if (1) {
            $this->_URLChecker();
            $data['is_invoice_page'] = true;
            if ($data['page_data'] != false) {
                $data['page_data']["Content"] = str_replace("[Telephone_Detail_Area]", "<a href='tel:" . $this->GetWebsiteTelephoneNo() . "'>" . $this->GetWebsiteTelephoneNo() . "</a>", $data['page_data']["Content"]);
                $data['page_data']["Content"] = str_replace("[Contact_Detail_Area]", "<a href='" . base_url($this->config->item('front_contact_page_url')) . "'>Contact Us</a>", $data['page_data']["Content"]);
            }
            $data["referral_code_amount"] = $this->GetReferralCodeAmount();
            if ($this->IsSessionItem("Invoice_FB_Total")) {
                $data['invoice_fb_total'] = $this->GetSessionItem("Invoice_FB_Total");
            }
            if ($this->IsSessionItem("Google_ECommerce_Invoice_Record")) {
                $data['google_e_commerce_invoice_record'] = $this->GetSessionItem("Google_ECommerce_Invoice_Record");
            }

            

            $this->RemoveSessionItems("Invoice_Success");
            $this->RemoveSessionItems("Invoice_FB_Total");
            $this->RemoveSessionItems("Google_ECommerce_Invoice_Record");
            $this->show_view_with_menu('booking/thank_you', $data);
        } else {
            //redirect(base_url());
            $this->show_view_with_menu('booking/thank_you', $data);
        }
    }

    public function savecart()
    {

        $post = $this->input->post(NULL, TRUE);

        //savecad($post,1);



        $member_id = null;

        if ($this->IsMemberLogin()) {
            $member_id = $this->GetCurrentMemberID();
            $checkData['member_id'] = $member_id;
        }


        // sublocality_level_1

        $address1 = array();

        foreach ($post["address_components"] as $component) {

            if (in_array("sublocality_level_1", $component["types"])) {
                $post["area"] = $component["long_name"];
            }

            if (in_array("locality", $component["types"])  || in_array("premise", $component["types"]) || in_array("sublocality_level_1", $component["types"]) || in_array("sublocality_level_2", $component["types"]) || in_array("sublocality_level_3", $component["types"])) {
                $address1[] = $component["long_name"];
            }
        }
        $address1[] = $post["name"];
        $address1[] = $post["area"];

        $address1 = array_unique($address1);
        $address1 = implode(", ", $address1);

        // $checkData['session_id'] = session_id();
        $this->cart->delete(array("session_id" => session_id()));
        // $data = $this->cart->get($checkData);

        $location = $post["latitude"] . "," . $post["longitude"];

        $cartData = array(
            "session_id" => session_id(),
            "member_id" => $member_id,
            "postal_code" => $address1,
            "franchise_id" => $post["franchise_id"],
            "address1" => $address1,
            "address2" => null,
            "has_address" => 1,
            "location" => $location,
            "city" => $post["city"],
        );
        $cart_id = $this->cart->insert($cartData);


        $this->AddSessionItem("cart_id", $cart_id);
        $this->AddSessionItem("franchise_id", $post["franchise_id"]);
        $this->AddSessionItem($this->postal_code, $post['postal_code']);
        $this->AddSessionItem($this->address1, $post["name"]);
        $this->AddSessionItem($this->town, $post["city"]);
        $this->AddSessionItem($this->session_post_code, $post['area']);
        $this->AddSessionItem("has_address", 1);

        $response = ["cart_id" => $cart_id, "success" => true];

        $response = array_merge($response, $cartData);

        echo jsonencode($response, 200);
    }

    public function booking()
    {

        $vat = $this->GetAppVat();

        $data['postal_code'] = $this->GetSessionItem($this->session_post_code);

        if (empty($data['postal_code'])) {
            $data['postal_code'] = "";
        }

        $data["vat"] = $vat;

        $data["postal_code_type"] = $this->config->item("postal_code_type");

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $cart_id = $this->GETSessionItem("cart_id");
        $cart = $this->cart->get(array("id" => $cart_id));
        $is_login = $this->IsMemberLogin();

        $franchise_id = $this->GetSessionItem("franchise_id");

        $check = 1;

        $data["member"] = array();
        //"postal_code" => $data["postal_code"]
        $hasPreferences = $this->cartservices->has_preferences(array("cart_id" => $cart_id));

        if (empty($cart)) {
            $data["order_address"] = "";
            $data['cart'] = array();
            $data["preferences"] = array();
        } else {
            $data['cart'] = $cart[0];
            $preferences = $this->GetSessionItem("order_preferences");
            $data["preferences"] = $preferences;
        }

        $cards = array();
        if ($is_login) {
            $data['member'] = $this->GetCurrentMemberDetail();

            //$cards = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code", array('FKMemberID' => $data['member']["PKMemberID"]), false, false, false, "PKCardID");

            $sql = "select PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code from " . $this->tbl_member_cards . " where FKMemberID=" . $data['member']["PKMemberID"] . ";";
            $cards = $this->model_database->query($sql);
            //d($cards,1);
        }


        $data['preference_records'] = $this->_GetPreferences();
        if (sizeof($data['preference_records']) == 0) {
            unset($data['preference_records']);
        } else {
            $data['member_preference_records'] = $this->_GetMemberPreferences();
        }
        //"postal_code" => $data['postal_code']

        $data['cart_services'] = $this->cartservices->get_services(array("cart_id" => $cart_id));
        $payment_methods = $this->config->item("payment_methods");
        $data['payment_methods'] = $payment_methods;
        $data['preferences'] = $preferences;
        $data['is_login'] = $is_login;
        $data['is_order_page'] = true;
        $data['show_minimum_order'] = false;
        $data['current_url'] = $current_url;
        $data['has_preferences'] = $hasPreferences;
        $data['cards'] = $cards;

        $data['franchise'] = isset($franchise_id) ? $this->_GetFranchiseRecord($franchise_id) : array();
        $this->show_view_with_menu('booking/index', $data);
    }

    public function validatestep1()
    {

        $post_data = $this->input->post(NULL, TRUE);

        $response["error"] = true;
        $response["errors"] = [];
        $i = 0;
        $header = 400;
        //if ($this->input->is_ajax_request() == true) {
        $postal_code_type = $this->config->item("postal_code_type");
        if (isset($post_data['postal_code']) && !empty($post_data['postal_code'])) {
            $postCodeFranchise = $this->country->ValidatePostalCode($post_data['postal_code']);

            if ($postCodeFranchise["validate"] == false) {
                $error = true;
                $response["errors"]["postal_code"] = $postal_code_type['validation_message'];
            } else {
                if ($postal_code_type["title"] == "Area") {
                    $post_data['addresses'] = $post_data['postal_code'];
                }
            }

            $hasAddress = NULL;
            $location = $post_data["location"];

            if (empty($post_data['addresses'])) {
                $error = true;
                $response["errors"]["address"] = "Choose your address.";
            } else if ($post_data['addresses'] == "-1") {

                if (empty($post_data['address'])) {
                    $error = true;
                    $response["errors"]["address"] = "Please enter your address.";
                }
                $address = $post_data['address'];
                $hasAddress = $post_data['addresses'];
            } else {
                $address = $post_data['addresses'];
                $post_data['city'] = end(explode(",", $address));
                // $this->country->GetLatitudeLongitude($post_code);
            }

            if ($error == false) {


                if (isset($postCodeFranchise["franchise"]['FKFranchiseID'])) {
                    $header = 200;

                    $sessionPostCode = $this->GETSessionItem($this->session_post_code);

                    if (empty($location)) {
                        $location = $this->country->GetLatitudeLongitude($post_data['postal_code']);
                    }

                    $franchise_id = $postCodeFranchise["franchise"]['FKFranchiseID'];
                    $response["error"] = false;
                    $address2 = empty($post_data['address2']) ? "" : $post_data['address2'];
                    $town = empty($post_data['city']) ? "" : $post_data['city'];
                    $this->AddSessionItem($this->postal_code, $post_data['postal_code']);
                    $this->AddSessionItem($this->address1, $address);
                    $this->AddSessionItem($this->address2, $address2);
                    $this->AddSessionItem($this->town, $town);
                    $this->AddSessionItem($this->session_post_code, $post_data['postal_code']);
                    $this->AddSessionItem($this->address_type, $post_data['address_type']);

                    $this->AddSessionItem("has_address", $hasAddress);

                    $response["address1"] = $address;
                    $response["address2"] = $address2;
                    $response["town"] = $town;
                    $response["postal_code"] = $post_data['postal_code'];
                    $response["has_address"] = $hasAddress;
                    $response["address_type"] = $post_data['address_type'];
                    $response["franchise_id"] = $franchise_id;
                    $member_id = NULL;



                    if ($this->IsMemberLogin()) {
                        $member_id = $this->GetCurrentMemberID();
                        $checkData['member_id'] = $member_id;
                    }
                    $checkData['session_id'] = session_id();
                    $data = $this->cart->get($checkData);



                    $cartData = array(
                        "session_id" => session_id(),
                        "member_id" => $member_id,
                        "postal_code" => $response["postal_code"],
                        "franchise_id" => $franchise_id,
                        "address1" => $response["address1"],
                        "address2" => $response["address2"],
                        "has_address" => $response["has_address"],
                        "location" => $location,
                        "address_type" => $post_data['address_type'],
                        "city" => $town,
                    );
                    //d($cartData,1);

                    //d($cartData,1);

                    $hasPreferences = false;


                    if ($postal_code_type["title"] == "Area") {
                        $data = $this->cart->get(array("session_id" => session_id()));

                        if (count($data) == 0) {
                            $cart_id = $this->cart->insert($cartData);
                        } else {
                            $cart_id = $data[0]->id;
                            $hasPreferences = $this->cartservices->has_preferences(array("cart_id" => $cart_id));
                            $this->cart->update($cartData, array("id" => $cart_id));
                        }
                    } else {

                        $checkData["postal_code"] = $response["postal_code"];

                        if (count($data) == 0) {
                            $cart_id = $this->cart->insert($cartData);
                        } else {
                            $cart_id = $data[0]->id;
                            $hasPreferences = $this->cartservices->has_preferences(array("cart_id" => $cart_id));
                            $this->cart->update($cartData, array("id" => $cart_id));
                        }
                    }

                    $services = $this->cartservices->get_services(array("cart_id" => $cart_id));
                    $data["services"] = $services;
                    $response["items_right"] = $this->load->view('booking/items_right', $data, true);

                    unset($response["errors"]);
                    $franchise = $this->_GetFranchiseRecord($franchise_id);
                    $response["minimum_order_amount"] = !empty($franchise["MinimumOrderAmount"]) ? $franchise["MinimumOrderAmount"] : 12;
                    $response["minimum_order_amount_later"] = !empty($franchise["MinimumOrderAmountLater"]) ? $franchise["MinimumOrderAmountLater"] : 0;
                    $response["services_count"] = count($services);
                    $response["has_preferences"] = $hasPreferences;
                    $response["cart_id"] = $cart_id;
                    $this->AddSessionItem("cart_id", $cart_id);
                    $this->AddSessionItem("franchise_id", $franchise_id);
                } else {
                    $response["errors"]["postal_code"] = "SORRY! Unfortunately, we're not serving the address that you requested at the moment. As we're steadily expanding, we hope to be serving your area soon.";
                }
            }
        } else {
            $response["errors"]["postal_code"] = $postal_code_type['validation_message'];
        }

        echo jsonencode($response, $header);
    }

    public function orderitems()
    {


        $this->_URLChecker();

        $respose["error"] = false;
        $franchise_id = $_GET["fid"];
        $cart_id = $this->GetSessionItem("cart_id");
        $cartServices = $this->cartservices->get(array("cart_id" => $cart_id));
        $services = array();
        foreach ($cartServices as $c) {
            $services[$c->service_id] = $c->quantity;
        }
        $data['services'] = $services;
        $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');

        if (sizeof($data['category_records']) > 0) {
            foreach ($data['category_records'] as $key => $value) {
                $service_records = $this->model_database->FranchiseCustomServicesSelectItems($franchise_id, $value['PKCategoryID']);
                if (sizeof($service_records) > 0) {
                    $data['category_records'][$key]['service_records'] = $service_records;
                } else {
                    unset($data['category_records'][$key]);
                }
            }
        }

        if (sizeof($data['category_records']) == 0) {

            $respose["error"] = true;
        } else {
            $data['is_order_page'] = true;
            $data['preference_records'] = $this->_GetPreferences();
            if (sizeof($data['preference_records']) == 0) {
                unset($data['preference_records']);
            } else {
                $data['member_preference_records'] = $this->_GetMemberPreferences();
            }

            if (!$this->IsSessionItem("invoice_type")) {
                $this->RemoveSessionItems("invoice_type");
                $this->RemoveSessionItems("admin_invoice_id");
                $this->RemoveSessionItems("member_invoice_id");
                if ($this->IsMemberLogin() == true && $this->IsMemberLoginFromAdmin() == true) {
                    $this->AddSessionItem("is_love_2_laundry_admin_member_logged_in", false);
                }
            }

            $this->load->view('booking/items', $data);
        }
    }

    public function addremoveitems()
    {

        $get = $this->input->get(NULL, TRUE);
        $response["error"] = true;
        $response["errors"] = [];
        $i = 0;
        $header = 400;
        $cart_id = $this->GetSessionItem("cart_id");
        $postal_code = $this->GetSessionItem($this->session_post_code);
        $franchise_id = $this->GetSessionItem("franchise_id");
        $service_id = $get["service_id"];
        $category = $get["category"];
        $category_id = $get["category_id"];
        $preference = $get["preference"];
        $package = $get["package"];
        $qty = $get["qty"];

        $desktop_image = $get["desktop_image"];
        $mobile_image = $get["mobile_image"];

        $service = $this->model_database->GetRecord($this->tbl_franchise_services, "PKFranchiseServiceID,Title,FKServiceID,Title,Price,DiscountPercentage,FKFranchiseID", array('FKServiceID' => $service_id, 'FKFranchiseID' => $franchise_id));


        if (!empty($service["DiscountPercentage"]) && $service["DiscountPercentage"] > 0) {
            $discount = $service["DiscountPercentage"];
            $discountAmount = ($service["Price"] * $discount) / 100;
            $price = (float) $service["Price"] - (float) $discountAmount;
        } else {
            $discount = 0;
            $price = $service["Price"];
        }


        $cartData['cart_id'] = $cart_id;
        $cartData['category'] = $category;
        $cartData['category_id'] = $category_id;
        $cartData['preference'] = $preference;
        $cartData['package'] = $package;
        $cartData['price'] = $price;
        $cartData['franchise_service_id'] = $service["PKFranchiseServiceID"];
        $cartData['service_id'] = $service["FKServiceID"];
        $cartData['title'] = $service["Title"];
        $cartData['discount'] = $discount;
        $cartData['desktop_image'] = $desktop_image;
        $cartData['mobile_image'] = $mobile_image;

        $response["service_id"] = $service["FKServiceID"];
        $response["franchise_service_id"] = $cartData['franchise_service_id'];
        $where["cart_id"] = $cart_id;
        $where["service_id"] = $service_id;
        $where["franchise_service_id"] = $cartData['franchise_service_id'];

        if ($get["type"] == "add") {

            if ($qty == 0) {
                $qty = 1;
                $cartData['quantity'] = $qty;
                $cartData['total'] = $price * $qty;

                if (!empty($cartData['total'])) {
                    $this->cartservices->insert($cartData);
                }
            } else {
                $qty = $qty + 1;
                $cartData['quantity'] = $qty;
                $cartData['total'] = $price * $qty;
                if (!empty($cartData['total'])) {
                    $this->cartservices->update($cartData, $where);
                }
            }

            $response["error"] = false;
            $header = 200;
        } else {
            if ($qty > 0) {
                $qty = $qty - 1;

                if ($qty == 0) {
                    $cartData['quantity'] = 0;
                    $cartData['total'] = 0;

                    $this->cartservices->delete($where);
                } else {

                    $cartData['quantity'] = $qty;
                    $cartData['total'] = $price * $qty;
                    if (!empty($cartData['total'])) {
                        $this->cartservices->update($cartData, $where);
                    }
                }
            }
            $header = 200;
        }

        $this->cart->update(array('type' => "items"), array("id" => $cart_id));
        $hasPreferences = $this->cartservices->has_preferences(array("cart_id" => $cart_id, "postal_code" => $postal_code));
        $response["has_preferences"] = $hasPreferences;
        $response["total"] = $cartData['total'];
        $response["qty"] = $qty;
        echo jsonencode($response, $header);
    }

    public function countservices()
    {
        $cart_id = $this->GetSessionItem("cart_id");
        $postal_code = $this->GetSessionItem($this->session_post_code);
        $franchise_id = $this->GetSessionItem("franchise_id");
        $franchise = $this->_GetFranchiseRecord($franchise_id);
        $cart = $this->cart->get(array("id" => $cart_id));
        $post = $this->input->get(NULL, TRUE);

        $header = 400;
        $response["error"] = false;
        $response["errors"] = [];
        $response["has_preferences"] = false;
        $response["minimum_order_amount"] = numberformat($franchise["MinimumOrderAmount"]);
        //"postal_code" => $postal_code

        if (trim($cart[0]->address1) == "") {
            $response["error"] = true;
            $response["errors"]["address"] = "Please enter your address from previous step.";
            echo jsonencode($response, $header);
            die;
        }


        $services = $this->cartservices->get_services(array("cart_id" => $cart_id));

        if ($post["type"] == "later") {
            $count = 0;
            $header = 200;
            foreach ($services as $services) {
                $this->cartservices->delete(array("id" => $services->cart_services_id));
            }
        } else {
            $total = 0;
            foreach ($services as $services) {
                $total += $services->total;
            }

            if ($total < $franchise["MinimumOrderAmount"]) {

                $response["error"] = true;
                $header = 400;
                $response["errors"]["minimum_orders"] = "Minimum order allowed is " . $this->config->item("currency_symbol") . $franchise["MinimumOrderAmount"] . " or skip item selection.";
            } else {
                $response["has_preferences"] = $this->cartservices->has_preferences(array("cart_id" => $cart_id, "postal_code" => $postal_code));
                $count = count($services);
                $header = 200;
            }
        }

        if ($response["error"] == false) {
            $this->cart->update(array("type" => $post["type"], "services_count" => $count), array("id" => $cart_id));
        }

        $response['services_count'] = (int) $count;
        echo jsonencode($response, $header);
    }

    public function removecartservice()
    {
        $cart_id = $this->GetSessionItem("cart_id");
        $service_id = $_GET["service_id"];
        $postal_code = $this->GetSessionItem($this->session_post_code);
        $services = $this->cartservices->delete(array("cart_id" => $cart_id, "service_id" => $service_id));
        $header = 200;
        $count = count($services);
        $this->cart->update(array("services_count" => $count), array("id" => $cart_id));
        $response['services_count'] = (int) $count;
        echo jsonencode($response, $header);
    }

    public function bookingrightview()
    {

        $postal_code = $this->GetSessionItem($this->session_post_code);
        $cart_id = $this->GetSessionItem("cart_id");
        $franchise_id = $this->GetSessionItem("franchise_id");
        $franchise = $this->_GetFranchiseRecord($franchise_id);
        $services = $this->cartservices->get_services(array("cart_id" => $cart_id));
        $discount_id = $this->GetSessionItem("discount_id");
        $discount_type = $this->GetSessionItem("discount_type");
        $discount_code = $this->GetSessionItem("discount_code");

        $vat = $this->GetAppVat();

        $dtype = $this->GetSessionItem("dtype");
        $serviceTotal = 0;
        $response['discount_amount'] = 0;
        $this->load->model('preferences');
        $preferencesTotal = 0;
        $preferencesList = $this->GetSessionItem("order_preferences");

        if (count($preferencesList) > 0) {
            $preferences = $this->preferences->where_in("PKPreferenceID", $preferencesList);
            foreach ($preferences as $preference) {

                if ($preference["PriceForPackage"] == "Yes") {
                    $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                }
            }
        }

        if (count($services) > 0) {
            $sum = 0;
            $discount = 0;
            if (!empty($discount_id)) {

                $discountAmount = 0.00;
                foreach ($services as $service) {
                    $sum += $service->total;
                    //&& $service->discount == 0 old
                    if ($service->package == "No") {
                        $discountAmount += $service->total;
                    }
                    $serviceTotal += $service->total;
                }

                if ($discount_type == "discount") {
                    $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('PKDiscountID' => $discount_id, 'Status' => 'Active'));
                    $allow = false;
                    if ($sum > $discount_record["MinimumOrderAmount"]) {
                        $allow = true;
                    }
                    $worth = $discount_record["Worth"];
                } else {
                    $worth = $this->GetReferralCodeAmount();
                    $allow = true;
                }

                if ($allow == true) {
                    if ($dtype == "Percentage") {

                        $discount = ($discountAmount / 100) * $worth;
                        $sum = $sum - $discount;
                    } else {
                        $sum = $sum - $worth;
                        $discount = $worth;
                    }
                } elseif ($sum == 0) {
                    $sum = $worth;
                }
                $response['discount_amount'] = numberformat($discount);
            } else {
                foreach ($services as $service) {
                    $sum += $service->total;
                    $serviceTotal += $service->total;
                }
            }
            $total = numberformat($sum);
            $vatAmount = ($total * $vat / 100);
            $response["vat_amount"] = $vatAmount;
            $serviceTotal = $serviceTotal;
        } else {
            $total = numberformat($franchise['MinimumOrderAmount']);
            $response["vat_amount"] = 0;
        }

        $data["preferences_total"] = $preferencesTotal;
        $data["services"] = $services;

        $response['services_total'] = numberformat($serviceTotal);
        //$response["items_right"] = $this->load->view('booking/items_right', $data, true);
        $header = 200;
        $response['discount_code'] = $discount_code;
        $response['total'] = numberformat($total);
        $response['minimum_order_amount'] = numberformat($franchise['MinimumOrderAmount']);
        $response['currency_symbol'] = $this->config->item("currency_symbol");
        echo jsonencode($response, $header);
    }

    public function login()
    {
        $post = $this->input->post(NULL, TRUE);
        //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));
        $response["error"] = false;
        $response["errors"] = [];
        $i = 0;
        $header = 400;
        //if ($this->input->is_ajax_request() == true) {
        $cart_id = $this->GetSessionItem("cart_id");

        if (empty($post['email'])) {
            $response["error"] = true;
            $response["errors"]["email"] = "Enter your email address.";
        }

        /* if (!$loqate->validateEmailAddress($post['email'])) {
          $response["error"] = true;
          $response["errors"]["email"] = "Please enter correct email address.";
          } */

        if (empty($post['password'])) {
            $response["error"] = true;
            $response["errors"]["password"] = "Enter your password.";
        }


        if ($response["error"] == false) {

            $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,CountryCode,Phone,StripeCustomerID,PKMemberID", array('EmailAddress' => $post['email'], 'Password' => encrypt_decrypt('encrypt', $post['password']), 'Status' => 'Enabled'));

            if ($member_record != false) {
                $header = 200;
                if (empty($member_record["StripeCustomerID"])) {
                    $this->load->library('stripe');

                    $stripeKey = $this->GetStripeAPIKey();
                    $stripe = new Stripe();
                    $customer = $stripe->saveCustomer($stripeKey, ["email" => $member_record['EmailAddress'], "name" => $member_record['FirstName'] . " " . $member_record['LastName']]);


                    $update_member = array(
                        'ID' => $member_record['PKMemberID'],
                        'StripeCustomerID' => $customer->id
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                }

                $response["member_id"] = $member_record['PKMemberID'];
                $phone = ltrim($member_record['Phone'], 0);
                $response["phone"] = $phone;
                $response["country_phone_code"] = "+" . $this->config->item("country_phone_code");
                $response["email"] = $member_record['EmailAddress'];
                $response["first_name"] = $member_record['FirstName'];
                $response["last_name"] = $member_record['LastName'];
                $response["full_name"] = $member_record['FirstName'] . " " . $member_record['LastName'];
                $response["country_code"] = $member_record['CountryCode'];

                $response["is_login"] = true;


                //$cards = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code", array('FKMemberID' => $member_record["PKMemberID"]), false, false, false, "PKCardID");//and CreatedDateTime BETWEEN NOW() - INTERVAL 365 DAY AND NOW()


                $sql = "select PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code from " . $this->tbl_member_cards . " where FKMemberID=" . $response["member_id"] . " ";
                $cards = $this->model_database->query($sql);

                $response["cards"] = $cards;
                $this->cart->update(array("member_id" => $member_record['PKMemberID']), array("id" => $cart_id));

                $this->_CreateMemberSession($member_record['PKMemberID']);
                $response["error"] = false;
                $response["message"] = "Login Successfully...";
            } else {
                $header = 401;
                $response["error"] = true;
                $response["errors"]["login"] = "Incorrect Username/Password";
            }
        }

        echo jsonencode($response, $header);
        //}
    }

    public function forgotpassword()
    {
        $post = $this->input->post(NULL, TRUE);
        //d($post,1);

        $response["error"] = false;
        $response["errors"] = [];
        $i = 0;
        $header = 400;

        if (empty($post['email'])) {
            $response["error"] = true;
            $response["errors"]["email"] = "Enter your email address.";
        } elseif (!$this->validateEmail($post['email'])) {
            $response["error"] = true;
            $response["errors"]["email"] = "Please enter correct email address.";
        } else {
            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('EmailAddress' => $post['email'], 'Status' => 'Enabled'));

            if ($member_record !== false) {

                $header = 200;

                $random_password = $this->RandomPassword();
                //$random_password = "passpass";
                $update_member = array(
                    'ID' => $member_record['PKMemberID'],
                    'Password' => encrypt_decrypt("encrypt", $random_password)
                );
                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                $email_field_array = array(
                    'Email Sender' => $this->GetWebsiteEmailSender(),
                    'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                    'Website Name' => $this->GetWebsiteName(),
                    'Website URL' => $this->GetWebsiteURLAddress(),
                    'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                    'Website Email Address' => $this->GetWebsiteEmailAddress(),
                    'Header Menus' => $this->GetEmailHeaderMenus(),
                    'Footer Menus' => $this->GetEmailFooterMenus(),
                    'Footer Title' => $this->GetEmailFooterTitle(),
                    'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                    'Password' => $random_password,
                    'Email Address' => $member_record['EmailAddress'],
                    'Phone Number' => $member_record['Phone']
                );
                $this->emailsender->send_email_responder(3, $email_field_array);
                $this->emailsender->send_email_responder(8, $email_field_array);

                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 10, 'Status' => 'Enabled'));
                if ($sms_responder_record != false) {
                    $sms_field_array = array(
                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                        'Password' => $random_password
                    );
                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                    $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                }
                $response["error"] = false;
                $response["message"] = "Password send to your desired email please check your inbox";
            } else {
                $response["error"] = true;
                $response["errors"]["email"] = "Email address not found in our records.";
            }
        }
        echo jsonencode($response, $header);
    }

    public function storepreferences()
    {
        $post = $this->input->post(NULL, TRUE);

        $header = 200;
        $preferencesTotal = 0;
        $response["error"] = true;

        if (count($post["preferences_list"]) > 0) {
            $this->load->model('preferences');
            $this->AddSessionItem("order_preferences", $post["preferences_list"]);
            $preferences = $this->preferences->where_in("PKPreferenceID", $post["preferences_list"]);

            foreach ($preferences as $preference) {

                if ($preference["PriceForPackage"] == "Yes") {
                    $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                }
            }
        }
        $response["error"] = false;
        $response["total"] = numberformat($preferencesTotal);
        echo jsonencode($response, $header);
    }

    public function register()
    {
        $post = $this->input->post(NULL, TRUE);
        $response["error"] = false;
        $response["errors"] = [];
        $i = 0;
        $header = 400;


        //if ($this->input->is_ajax_request() == true) {
        $cart_id = $this->GetSessionItem("cart_id");
        $cart = $this->cart->get(array("id" => $cart_id));

        $post['phone'] = preg_replace('/\s+/', '', $post['phone']);
        $post['phone'] = $phone = ltrim($post['phone'], 0);

        $post['country_code'] = preg_replace('/\s+/', '', $post['country_code']);
        $post['country_code'] = ltrim($post['country_code'], 0);


        if (empty($post['first_name'])) {
            $response["error"] = true;
            $response["errors"]["first_name"] = "Enter first name.";
        }

        if (empty($post['last_name'])) {
            $response["error"] = true;
            $response["errors"]["last_name"] = "Enter last name.";
        }

        if (empty($post['last_name'])) {
            $response["error"] = true;
            $response["errors"]["last_name"] = "Enter last name.";
        }

        if (empty($post['email'])) {
            $response["error"] = true;
            $response["errors"]["email"] = "Enter email address.";
        }



        if (!$this->validateEmail($post['email'])) {
            $response["error"] = true;
            $response["errors"]["email"] = "Please enter correct email address.";
        }

        $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('EmailAddress' => $post['email']));
        if ($member_record != false) {
            $response["error"] = true;
            $response["errors"]["email"] = "Email address already exist in our records.";
        }

        if (empty($post['phone'])) {
            $response["error"] = true;
            $response["errors"]["phone"] = "Enter Your phone number.";
        } else {
            if ($this->verifyPhoneNumber($post['country_code'], $post['phone']) == false) {
                $response["error"] = true;
                $response["errors"]["phone"] = "Incorrect phone number.";
            }
        }

        //$response["errors"]["country_code"] = $post['country_code'];

        $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('Phone' => $post['phone']));
        if ($member_record != false) {
            $response["error"] = true;
            $response["errors"]["email"] = "Phone Number already exist in our records.";
        }


        if (empty($post['password'])) {
            $response["error"] = true;
            $response["errors"]["password"] = "Enter your password.";
        }

        if (empty($post['confirm_password'])) {
            $response["error"] = true;
            $response["errors"]["confirm_password"] = "Enter confirm your password.";
        }

        if ($post['password'] != $post['confirm_password']) {
            $response["error"] = true;
            $response["errors"]["confirm_password"] = "Password and confirmed password must be same.";
        }






        if ($response['error'] == false) {

            $this->load->library('stripe');
            $header = 200;
            $stripeKey = $this->GetStripeAPIKey();
            $stripe = new Stripe();
            $customer = $stripe->saveCustomer($stripeKey, ["email" => $post['email'], "name" => $post['first_name'] . " " . $post['last_name']]);

            $insert_member = array(
                'FirstName' => $post['first_name'],
                'LastName' => $post['last_name'],
                'EmailAddress' => $post['email'],
                'Password' => encrypt_decrypt('encrypt', $post['password']),
                'CountryCode' => $post['country_code'],
                'Phone' => $post['phone'],
                'PostalCode' => $cart[0]->postal_code,
                'BuildingName' => $cart[0]->address1,
                'StreetName' => $cart[0]->address2,
                'Town' => $cart[0]->city,
                'FKFranchiseID' => $cart[0]->franchise_id,
                'Status' => 'Enabled',
                'RegisterFrom' => 'Desktop',
                'PopShow' => 'Yes',
                'StripeCustomerID' => $customer->id,
                'ReferralCode' => $this->random_str($post['first_name']),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );

            $type = $this->GetSessionItem("type");

            if ($this->GetSessionItem("social_id")) {
                $social_id = $this->GetSessionItem("social_id");

                if ($social_id == $post_data["social_id"]) {
                    if ($type == "facebook") {
                        $insert_member['FacebookID'] = $this->GetSessionItem("social_id");
                    } else {
                        $insert_member['GoolgleID'] = $this->GetSessionItem("social_id");
                    }
                }
            }


            $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);
            $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID,FirstName,LastName,EmailAddress,Phone", array('PKMemberID' => $member_id));
            $response["member_id"] = $member_id;
            $response["is_login"] = true;

            $phone = ltrim($member_record['Phone'], 0);
            $response["phone"] = "+" . $this->config->item("country_phone_code") . $phone;
            $response["email"] = $member_record['EmailAddress'];
            $response["first_name"] = $member_record['FirstName'];
            $response["last_name"] = $member_record['LastName'];
            $response["full_name"] = $member_record['FirstName'] . " " . $member_record['LastName'];
            $cart = $this->cart->update(array("member_id" => $member_id), array("id" => $cart_id));



            if ($member_record !== false) {


                $MemberPreferences = $this->_GetMemberPreferences();
                if (empty($MemberPreferences)) {
                    $Preferences = $this->_GetPreferencesRegistration();

                    if (isset($Preferences) && $Preferences !== false) {
                        foreach ($Preferences as $key => $value) {
                            $child_records = (isset($value['child_records']) && !empty($value['child_records'])) ? $value['child_records'] : array();
                            if (isset($child_records[0]['PKPreferenceID']) && $child_records[0]['PKPreferenceID'] > 0) {
                                $insert_member_preference = array(
                                    'FKMemberID' => $member_id,
                                    'FKPreferenceID' => $child_records[0]['PKPreferenceID']
                                );
                                $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                            } else {
                                continue;
                            }
                        }
                    }
                }

                $this->_CreateMemberSession($member_record['PKMemberID']);
                $member_record = $this->GetCurrentMemberDetail();
                $email_field_array = array(
                    'Email Sender' => $this->GetWebsiteEmailSender(),
                    'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                    'Website Name' => $this->GetWebsiteName(),
                    'Website URL' => $this->GetWebsiteURLAddress(),
                    'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                    'Website Email Address' => $this->GetWebsiteEmailAddress(),
                    'Header Menus' => $this->GetEmailHeaderMenus(),
                    'Footer Menus' => $this->GetEmailFooterMenus(),
                    'Footer Title' => $this->GetEmailFooterTitle(),
                    'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                    'First Name' => $member_record['FirstName'],
                    'Last Name' => $member_record['LastName'],
                    'Email Address' => $member_record['EmailAddress'],
                    'Phone Number' => $member_record['Phone'],
                    'Postal Code' => $member_record['PostalCode'],
                    'Building Name' => $member_record['BuildingName'],
                    'Street Name' => $member_record['StreetName'],
                    'Town' => $member_record['Town'],
                    'Register From' => $member_record['RegisterFrom']
                );

                //$this->emailsender->send_email_responder(1, $email_field_array);
                //$this->emailsender->send_email_responder(6, $email_field_array);

                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 11, 'Status' => 'Enabled'));
                if ($sms_responder_record != false) {
                    $sms_field_array = array(
                        'Store Link' => $this->config->item('store_link_short_url'),
                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                        'Email Address' => $post_data['reg_email_address'],
                        'Password' => $post_data['reg_password']
                    );
                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                    // $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                }
                $this->RemoveSessionItems(array("social_id", "type", "reg_email", "reg_first_name", "reg_last_name"));
                $this->AddSessionItem("Register_New_Member", true);
                //echo "success||t||" . base_url('welcome');
                $response['message'] = "success";
            }
        }
        /* } else {
          $response["error"] = true;
          $response["errors"]["request"] = "Invalid request";
          } */
        echo jsonencode($response, $header);
    }

    public function applydiscountcode()
    {

        $post = $this->input->post(NULL, TRUE);
        $member_id = $this->GetCurrentMemberID();
        $response["error"] = false;
        $header = 400;

        if ($post["discount_type"] == "referral") {
            $member_detail = $this->GetCurrentMemberDetail();
            $member_referral_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", "ReferralCode='" . $post['code'] . "' and Status='Enabled' and Phone != '" . $member_detail['Phone'] . "' and BuildingName !='" . $member_detail['BuildingName'] . "' and PKMemberID !=" . $member_detail['PKMemberID']);


            if ($member_referral_record != false) {

                $response["discount"] = "success||Referral|" . $member_referral_record['PKMemberID'] . '|' . $this->GetReferralCodeAmount() . '|Price|' . $post['code'];

                $response["id"] = $member_referral_record['PKMemberID'];
                $response["worth"] = $this->GetReferralCodeAmount();
                $response["dtype"] = "Price";
                $response["Code"] = $post['code'];
                $response["discount_type"] = $post['discount_type'];
                $this->AddSessionItem("discount_type", $post["discount_type"]);
                $this->AddSessionItem("discount_id", $response["id"]);
                $this->AddSessionItem("discount", $response["discount"]);
                $this->AddSessionItem("dtype", $response["dtype"]);
                $this->AddSessionItem("discount_code", $response["Code"]);
                $header = 200;
            } else {
                $response["error"] = true;
                $response["errors"][] = "Refferel code incorrect.";
            }
        } elseif ($post["discount_type"] == "discount") {
            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('Code' => $post['code'], 'Status' => 'Active'));

            if ($discount_record != false) {

                if (!empty($discount_record['FKMemberID'])) {
                    if ($discount_record['FKMemberID'] != $member_id) {
                        $response["error"] = true;
                        $response["errors"][] = "Discount code incorrect.";
                    }
                }

                $current_date = date('Y-m-d');
                $cartTotal = $this->checkCartTotal();
                if (!empty($discount_record['StartDate'])) {
                    $start_date = date('Y-m-d', strtotime($discount_record['StartDate']));
                    if ($current_date < $start_date) {
                        $response["error"] = true;
                        $response["errors"][] = "Discount code is not valid";
                    }
                }

                if ($response["error"] == false) {
                    if (!empty($discount_record['ExpireDate'])) {
                        $expire_date = date('Y-m-d', strtotime($discount_record['ExpireDate']));
                        if ($current_date > $expire_date) {
                            $update_discount = array(
                                'Status' => 'Expire',
                                'ID' => $discount_record['PKDiscountID'],
                                'UpdatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                            $response["error"] = true;
                            $response["errors"][] = "Discount code is already expire";
                        }
                    }
                    if ($discount_record['CodeUsed'] == "One Time") {
                        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'PKInvoiceID', array('FKDiscountID' => $discount_record['PKDiscountID'], 'FKMemberID' => $this->GetCurrentMemberID()));
                        if ($invoice_record !== false) {
                            $response["error"] = true;
                            $response["errors"][] = "You already used this discount code";
                        }
                    }

                    $cart_id = $this->GETSessionItem("cart_id");
                    $cart = $this->cart->first(array("id" => $cart_id));

                    if ($cartTotal["sum"] < $discount_record["MinimumOrderAmount"]) {
                        $response["error"] = true;
                        $response["errors"][] = "Dear Customer, to avail this discount code your grand total must be a minimum of " . $this->config->item("currency_symbol") . numberformat($discount_record['MinimumOrderAmount']) . ". ";
                    }

                    if ($response["error"] == false) {
                        $response["discount"] = "success||Discount|" . $discount_record['PKDiscountID'] . '|' . $discount_record['Worth'] . '|' . $discount_record['DType'] . '|' . $discount_record['Code'];

                        $response["id"] = $discount_record['PKDiscountID'];
                        $response["worth"] = $discount_record['Worth'];
                        $response["dtype"] = $discount_record['DType'];
                        $response["Code"] = $discount_record['Code'];
                        $this->AddSessionItem("discount_id", $response["id"]);
                        $this->AddSessionItem("discount", $response["discount"]);
                        $this->AddSessionItem("discount_type", $post["discount_type"]);
                        $this->AddSessionItem("dtype", $response['dtype']);
                        $this->AddSessionItem("discount_code", $response["Code"]);
                        $header = 200;
                    }
                } else {
                    $response["error"] = true;
                    $response["errors"][] = "You already used this discount code";
                }
            } else {
                $response["error"] = true;
                $response["errors"][] = "discount not found";
            }
        }
        echo jsonencode($response, $header);
    }

    public function canceldiscount()
    {

        $this->RemoveSessionItems(array("discount_id", "discount", "discount_type", "dtype", "discount_code"));
        $response["error"] = false;
        echo jsonencode($response, 200);
    }

    public function checkCartTotal()
    { /////without preferences
        $cart_id = $this->GetSessionItem("cart_id");
        $services = $this->cartservices->get(array("cart_id" => $cart_id));
        $discountAmount = 0.00;
        $sum = 0.00;

        foreach ($services as $service) {

            $sum += $service->total;
            if ($service->package == "No") {
                $discountAmount += $service->total;
            }
        }

        return array("discount_amount" => $discountAmount, "sum" => $sum);
    }

    public function calculateDiscount($type, $worth)
    {
        $cart_id = $this->GetSessionItem("cart_id");
        $services = $this->cartservices->get(array("cart_id" => $cart_id));
        $sum = 0;
        $discountAmount = 0.00;
        foreach ($services as $service) {

            $sum += $service->total;
            if ($service->package == "No") {
                $discountAmount += $service->total;
            }
        }
        $discount = 0.00;
        if ($discountAmount > 0) {
            if ($type == "Percentage") {
                $discount = ($discountAmount / 100) * $worth;
                $sum = $sum - $discount;
            } else {
                $discount = $worth;
            }
        }
        return numberformat($discount);
    }

    public function confirmorder()
    {

        $this->db->trans_start();

        $post = $this->input->post(NULL, TRUE);


        $franchise_id = $this->GetSessionItem("franchise_id");
        $response["error"] = false;
        $franchise_record = $this->_GetFranchiseRecord($franchise_id);
        $website_currency = $this->config->item('currency_sign');
        $cart_id = $this->GETSessionItem("cart_id");
        $member_id = $this->GetCurrentMemberID();
        $cart = $this->cart->first(array("id" => $cart_id));
        $member_detail = $this->GetCurrentMemberDetail();
        $order_notes = isset($cart['order_notes']) ? strip_slashes($cart['order_notes']) : "";

        $response["errors"] = array();
        $header = 400;

        if (empty($post["first_name"])) {
            $response["error"] = true;
            $response["errors"]["payment_first_name"] = "Please enter your first name.";
        }

        if (empty($post["last_name"])) {
            $response["error"] = true;
            $response["errors"]["payment_last_name"] = $response['message'] = "Please enter your last name.";
        }

        if (empty($post["phone"])) {
            $response["error"] = true;
            $response["errors"]["payment_phone"] = $response['message'] = "Please enter your phone number.";
        } else {
            if ($this->verifyPhoneNumber($member_detail["CountryCode"], $post["phone"]) == false) {
                $response["error"] = true;
                $response["errors"]["payment_phone"] = $response['message'] = "Please enter correct phone number.";
            }
        }

        //d($post,1);

        if (empty($response["errors"])) {

            if (!empty($cart)) {

                $discount_id = $this->GetSessionItem("discount_id");
                $dType = $this->GetSessionItem("dtype");
                $discount_type = $this->GetSessionItem("discount_type");


                $post['pickup_date'] = $cart['pickup_date'];
                $post['pickup_time'] = $cart['pickup_time'];
                $post['delivery_date'] = $cart['delivery_date'];
                $post['delivery_time'] = $cart['delivery_time'];

                $pick_dateTMP = $cart['pickup_date'];
                $pick_timeTMP = $cart['pickup_time'];
                $card_id = $post['card_id'];

                $pick_timeArray = explode('-', $pick_timeTMP);
                $pick_timeClean = isset($pick_timeArray[1]) ? $pick_timeArray[1] . ':00' : '';

                $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

                $now = date("Y-m-d H:i:s");
                $diffrencePickup = $this->dateTime_difference($pickupDateTimeTMP, $now);

                $delivery_dateTMP = $cart['delivery_date'];
                $delivery_timeTMP = $cart['delivery_time'];


                $delivery_timeArray = explode('-', $delivery_timeTMP);
                $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';

                $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

                $diffrenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);

                if ($diffrencePickup < $franchise_record["PickupDifferenceHour"] && empty($response["errors"])) {
                    $response["error"] = true;
                    $response["errors"][] = "Dear Customer,<br/>Order Pickup difference should be at least " . $franchise_record["PickupDifferenceHour"] . " hours , please contact our customer support";
                } elseif ($diffrenceTMP < $franchise_record["DeliveryDifferenceHour"] && empty($response["errors"])) {
                    $response["error"] = true;
                    $response["errors"][] = "Dear Customer,<br/>Order delivery difference should be at least " . $franchise_record["DeliveryDifferenceHour"] . " hours , please contact our customer support";
                } else {

                    $member_card = $this->model_database->GetRecord($this->tbl_member_cards, false, array('PKCardID' => $post['card_id'], "FKMemberID" => $member_id));

                    $header = 200;

                    $member_detail = $this->GetCurrentMemberDetail();
                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                    $stripe_customer_id = $member_record['StripeCustomerID'];

                    $cartServices = $this->cartservices->get(array("cart_id" => $cart_id));

                    $subTotal = 0;
                    $servicesTotal = 0;
                    $grandTotal = 0;
                    $preferencesTotal = 0;

                    if (count($cartServices) > 0) {
                        $invoiceType = "Items";
                        $this->load->model('preferences');
                        foreach ($cartServices as $cartService) {
                            $servicesTotal += $cartService->total;
                        }
                        $subTotal += $servicesTotal;

                        $hasPreferences = $this->cartservices->has_preferences(array("cart_id" => $cart_id, "postal_code" => $cart["postal_code"]));

                        if ($hasPreferences) {
                            $preferences = $this->GetSessionItem("order_preferences");
                            $preferences = $this->preferences->where_in("PKPreferenceID", $preferences);

                            foreach ($preferences as $preference) {

                                if ($preference["PriceForPackage"] == "Yes") {
                                    $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                                }
                            }
                            $subTotal += $preferencesTotal;
                        }

                        $grandTotal = $subTotal;
                    } else {
                        $invoiceType = "After";
                        $grandTotal = $franchise_record["MinimumOrderAmount"];
                        $subTotal = $grandTotal;
                    }


                    $discountTotal = 0.00;

                    $errorInDiscount = false;
                    $discountKey = "FKDiscountID";
                    if (!empty($discount_id)) {

                        if ($discount_type == "discount") {
                            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('PKDiscountID' => $discount_id, 'Status' => 'Active'));
                            $worth = $discount_record['Worth'];

                            if ($grandTotal < $discount_record['MinimumOrderAmount']) {
                                $errorInDiscount = true;
                            }
                        } else {
                            $worth = $this->GetReferralCodeAmount();
                            $dType = "Price";
                            $discountKey = "ReferralID";
                        }
                        $code = $this->GetSessionItem("discount_code");
                        $discountTotal = $this->calculateDiscount($dType, $worth);
                        $grandTotal -= $discountTotal;
                        $discountType = ucfirst($discount_type);
                        $discountCode = $code;
                        $discountWorth = $worth;

                        if ($grandTotal < $discount_record['MinimumOrderAmount']) {
                            $errorInDiscount = true;
                        }
                    } else {
                        $discount_id = NULL;
                        $discountType = "None";
                        $discountCode = NULL;
                        $discountWorth = NULL;
                        $dType = "None";
                        $discountKey = "ReferralID";
                    }

                    $subTotal = numberformat((float) $subTotal, 2, '.', '');
                    $preferencesTotal = numberformat((float) $preferencesTotal, 2, '.', '');
                    //$invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");

                    $grandTotal = numberformat($grandTotal);
                    $paymentTokan = "";


                    if ($errorInDiscount == true) {

                        $response["error"] = true;
                        $header = 400;
                        $response["errors"][] = "Dear Customer, to avail this discount code your grand total must be a minimum of " . $this->config->item("currency_symbol") . numberformat($discount_record['MinimumOrderAmount']) . ".";
                    } else {

                        $minimumOrderAmount = $franchise_record["MinimumOrderAmount"];
                        if ($grandTotal >= $franchise_record["MinimumOrderAmount"]) {

                            if ($post["payment_method"] == "stripe") {

                                $this->load->library('stripe');
                                $stripeKey = $this->GetStripeAPIKey();

                                $convertAmount = $this->country->convertAmount($grandTotal);
                                $amount = (int) ($convertAmount * 100);

                                $data = [
                                    'payment_method' => $member_card["Code"],
                                    'amount' => $amount,
                                    'customer' => $stripe_customer_id,
                                    'payment_method_types' => ['card'],
                                    'currency' => $this->config->item('currency_to'),
                                    'confirmation_method' => 'manual',
                                    'confirm' => true,
                                    'off_session' => true,
                                    //'description' => "Invoice #" . $invoice_number
                                ];

                                try {
                                    $stripe = new Stripe();
                                    $intent = $stripe->purchase($stripeKey, $data);

                                    if (isset($intent["error"])) {
                                        $response["error"] = true;
                                        $response["errors"][] = $intent["message"];
                                    } else {
                                        $paymentTokan = $intent->id;
                                        $response["error"] = false;
                                        $paymentStatus = "Completed";
                                    }
                                } catch (Exception $ex) {
                                    $response["error"] = true;
                                    $header = 400;
                                    $response["errors"][] = $response['message'] = $ex->getMessage();
                                }
                            } else {
                                $response["error"] = false;
                                $paymentStatus = "Pending";
                            }



                            if ($response["error"] == false) {


                                $update_member = array(
                                    'ID' => $member_detail['PKMemberID'],
                                    'FirstName' => $post["first_name"],
                                    'LastName' => $post["last_name"],
                                    'Phone' => ltrim($post["phone"], 0),
                                    'CountryCode' => trim($post["country_code"]),
                                );

                                //$this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');


                                $paymentMethod = $post["payment_method"];

                                $invoice_data = array(
                                    'FKMemberID' => $member_id,
                                    'FKCardID' => $member_card["PKCardID"],
                                    'FKFranchiseID' => $franchise_record['PKFranchiseID'],
                                    'InvoiceType' => $invoiceType,
                                    'Currency' => $website_currency,
                                    //'InvoiceNumber' => $invoice_number,
                                    'PaymentMethod' => ucfirst($paymentMethod),
                                    'PaymentToken' => $paymentTokan,
                                    'PaymentReference' => $member_card["Code"],
                                    'OrderNotes' => $order_notes,
                                    'SubTotal' => $subTotal,
                                    'ServicesTotal' => $servicesTotal,
                                    $discountKey => $discount_id,
                                    'DiscountType' => $discountType,
                                    'DiscountCode' => $discountCode,
                                    'DiscountWorth' => $discountWorth,
                                    'DType' => $dType,
                                    'DiscountTotal' => $discountTotal,
                                    'GrandTotal' => $grandTotal,
                                    'PreferenceTotal' => $preferencesTotal,
                                    'IPAddress' => $this->input->ip_address(),
                                    'PickupDate' => $post['pickup_date'],
                                    'PickupTime' => $post['pickup_time'],
                                    'DeliveryDate' => $post['delivery_date'],
                                    'DeliveryTime' => $post['delivery_time'],
                                    'Regularly' => (isset($regularly_array[1])) ? trim((int) $regularly_array[1]) : 0
                                );


                                $invoice_data['HasAddress'] = 1;

                                if ($cart["has_address"] == -1) {
                                    $invoice_data['HasAddress'] = 0;
                                }

                                $invoice_data['BuildingName'] = $cart["address1"];
                                $invoice_data['StreetName'] = $cart["address2"];
                                $invoice_data['Town'] = $cart["city"];
                                $invoice_data['PostalCode'] = $cart["postal_code"];
                                $invoice_data['AddressType'] = $cart["address_type"];
                                $invoice_data['Location'] = $cart["location"];
                                $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                $invoice_data['PaymentStatus'] = $paymentStatus;
                                $invoice_data['OrderStatus'] = "Processed";
                                ///$invoice_data['PaymentMethod'] = "Stripe";
                                $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);

                                $invoice_number = $this->GetInvoiceNumber($invoice_id);
                                $updateArray = array("InvoiceNumber" => $invoice_number, "ID" => $invoice_id);

                                $this->model_database->UpdateRecord($this->tbl_invoices, $updateArray, 'PKInvoiceID');



                                if (isset($intent->id)) {
                                    try {

                                        $data = array(
                                            'description' => "Invoice #" . $invoice_number,
                                            'metadata' => [
                                                'Invoice ID' => $invoice_id,
                                                'Invoice Number' => $invoice_number,
                                                'Member ID' => $member_id,
                                                'Member Email' => $member_record["EmailAddress"],
                                                'Member Name' => $member_record["FirstName"] . " " . $member_record["LastName"],
                                            ]
                                        );
                                        $intent = $stripe->updatePurchase($stripeKey, $intent->id, $data);
                                    } catch (Exception $ex) {
                                    }
                                }

                                $action = "insert";

                                /* update letter */
                                if (isset($chk_news)) {
                                    $update_member['NewsLetter'] = (isset($chk_news)) ? trim((int) $chk_news) : 0;
                                }
                                unset($_SESSION['rand']);
                                foreach ($cartServices as $cartService) {
                                    $insert_invoice_service = array(
                                        'FKInvoiceID' => $invoice_id,
                                        'FKServiceID' => $cartService->service_id,
                                        'FKCategoryID' => $cartService->category_id,
                                        'Title' => $cartService->title,
                                        'DesktopImageName' => urldecode($cartService->desktop_image),
                                        'MobileImageName' => urldecode($cartService->mobile_image),
                                        'IsPackage' => $cartService->package,
                                        'PreferencesShow' => $cartService->preference,
                                        'Quantity' => $cartService->quantity,
                                        'Price' => $cartService->price,
                                        'Total' => $cartService->total
                                    );
                                    $tookan_invoice_service[] = $cartService;
                                    $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                }

                                if ($hasPreferences) {
                                    foreach ($preferences as $preference) {

                                        $insert_invoice_preference["FKInvoiceID"] = $invoice_id;
                                        $insert_invoice_preference["FKPreferenceID"] = $preference["PKPreferenceID"];
                                        $insert_invoice_preference["Title"] = $preference["Title"];
                                        $insert_invoice_preference["Price"] = $preference["Price"];
                                        $insert_invoice_preference["Total"] = $preference["Price"];
                                        if ($preference["ParentPreferenceID"] != 0) {

                                            $p = $this->model_database->GetRecord($this->tbl_preferences, 'Title', array('PKPreferenceID' => $preference["ParentPreferenceID"]));
                                            $insert_invoice_preference["ParentTitle"] = $p["Title"];
                                        }
                                        $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                    }
                                }

                                if ($paymentMethod != "cash") {

                                    $insert_invoice_payment_information = array(
                                        'FKInvoiceID' => $invoice_id,
                                        'FKCardID' => $member_card["PKCardID"],
                                        'Amount' => $grandTotal,
                                        'PaymentReference' => $member_card["Code"],
                                        'PaymentToken' => $paymentTokan,
                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                    );
                                    $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                                }

                                if (isset($discount_id)) {

                                    $is_referral_discount_create = true;
                                    /*

                                    if ($invoice_record != false) {
                                        if ($invoice_record["PaymentStatus"] != "Pending") {
                                            $is_referral_discount_create = false;
                                        }
                                    }
                                    */

                                    if ($is_referral_discount_create) {
                                        // $discount_cart_array = explode("|", $discount_cart);
                                        if ($discount_type != "discount") {
                                            $insert_discount = array(
                                                'FKMemberID' => $discount_id,
                                                'Code' => $this->RandomPassword(6),
                                                'Worth' => $worth,
                                                'DType' => $dType,
                                                'DiscountFor' => ucfirst($discount_type),
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                        }
                                    }
                                } else {

                                    $loyalty_points = intval($grandTotal);
                                    $minus_loyalty_points = 0;
                                    $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_id));
                                    $loyalty_data = array(
                                        'FKMemberID' => $member_id,
                                        'FKInvoiceID' => $invoice_id,
                                        'InvoiceNumber' => $invoice_number,
                                        'GrandTotal' => $grandTotal,
                                        'Points' => $loyalty_points
                                    );
                                    
                                    if ($invoice_loyalty_record != false) {
                                        $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                        $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                        $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                        $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                    } else {
                                        $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                        $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                    }

                                    $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                    $total_loyalty_points = intval($member_detail['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                    $used_loyalty_points = intval($member_detail['UsedLoyaltyPoints']);
                                    $total_loyalty_points += $loyalty_points;
                                    $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                    if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                        $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                        for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                            $loyalty_code = $this->RandomPassword(6);
                                            $insert_discount = array(
                                                'FKMemberID' => $member_id,
                                                'Code' => $loyalty_code,
                                                'Worth' => $this->GetLoyaltyCodeAmount(),
                                                'DType' => "Price",
                                                'DiscountFor' => "Loyalty",
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                            $used_loyalty_points += $minimum_loyalty_points;
                                            $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                            if ($sms_responder_record != false) {
                                                $sms_field_array = array(
                                                    'Loyalty Points' => $remain_loyalty_points,
                                                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                    'Currency' => $this->config->item("currency_symbol"),
                                                    'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                    'Discount Code' => $loyalty_code
                                                );
                                                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                                $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                                            }
                                        }
                                    }
                                    $update_member['TotalLoyaltyPoints'] = $total_loyalty_points;
                                    $update_member['UsedLoyaltyPoints'] = $used_loyalty_points;


                                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_detail['PKMemberID']));
                                    if ($member_record !== false) {
                                        $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                                    }
                                }

                                $update_member['ID'] = $member_id;
                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');

                                $this->cart->delete(array("id" => $cart_id));
                                $this->cartservices->delete(array("cart_id" => $cart_id));


                                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,InvoiceType,GrandTotal", array('PKInvoiceID' => $invoice_id));
                                $invoice_record['service_records']=[];
                                if ($invoice_record != false) {
                                    if ($invoice_record['InvoiceType'] == "Items") {

                                        $invoice_record['service_records'] = $tookan_invoice_service;
                                    }

                                    //d($invoice_record,1);

                                    $this->AddSessionItem("Google_ECommerce_Invoice_Record", $invoice_record);
                                }

                                //$response["tookaan"] = $this->saveToTookaan($invoice_id, $member_record, $action);
                                $this->AddSessionItem("OrderAction", $action);
                                $this->AddSessionItem("invoice_id", $invoice_id);
                                $this->AddSessionItem("invoice_number", $invoice_number);
                                $this->AddSessionItem("Invoice_Success", true);
                                $this->AddSessionItem("Google_ECommerce_Invoice_Record", $invoice_record);
                                $this->AddSessionItem("Invoice_FB_Total", $amount);


                                $this->RemoveSessionItems($this->session_post_code);
                                $this->RemoveSessionItems("cart_id");
                                $this->RemoveSessionItems("order_preferences");
                                $this->RemoveSessionItems("discount_type");
                                $this->RemoveSessionItems("discount_id");
                                $this->RemoveSessionItems("discount");
                                $this->RemoveSessionItems("dtype");
                                $this->RemoveSessionItems("discount_code");

                                $response["error"] = false;
                                $response["id"] = $invoice_id;
                                $response["number"] = $invoice_number;
                                $response["message"] = "invoice created";

                                $this->db->trans_complete();
                            } else {
                                $response["error"] = true;
                                $header = 400;
                                $response["errors"][] = $response['message'] = "There is an error with you current payment information. Try different one.";
                            }
                        } else {
                            $response["error"] = true;
                            $header = 400;
                            $response["errors"][] = "The order has to meet a minimum order value of " . $this->config->item("currency_symbol") . $minimumOrderAmount . " including the discount code for a free pickup and delivery.";
                        }
                    }
                }
            } else {
                $response["error"] = true;
                $response["errors"][] = "Dear Customer, some thing went wrong , please contact our customer support or refreash the page.";
            }
        }


        echo jsonencode($response, $header);
    }


    public function notifyneworder2()
    {


        $post = $this->input->get(NULL, TRUE);

        $invoice_id = isset($post['id']) ? $post['id'] : '';
        $invoice_number = isset($post['number']) ? $post['number'] : '';
        $action = isset($post['type']) ? $post['type'] : '';

        // $invoice_id = $this->GetSessionItem("invoice_id");
        // $invoice_number = $this->GetSessionItem("invoice_number");
        // $action = $this->GetSessionItem("OrderAction");


        if ($invoice_id) {

            $member_id = $this->GetCurrentMemberID();
            $member_detail = $this->GetCurrentMemberDetail();



            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));

            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));
            $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_id), false, false, false, "PKInvoiceServiceID", "asc");


            $record["invoice"] = $invoice_record;
            $record["services"] = $invoice_service_records;
            $record["member"] = $member_record;


            if ($action == "insert") {


                $this->saveToTookaan($record, $action);
                $this->SendInvoiceEmail($record, "C");



                /*
                if ($this->config->item("onfleet_enabled")==true) {
                    
                    $response = $this->country->createTask($record, $member_record);
                    $update_invoice_record["OnfleetResponse"] = $response;
                    $update_invoice_record["ID"] = $invoice_id;
                    $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                } else {
                    
                }*/

                $invoice_record = $record['invoice'];
                $pick_date = $invoice_record["PickupDate"];
                $pick_time = $invoice_record["PickupTime"];
                $PaymentStatus = $invoice_record['PaymentStatus'];
                $PaymentMethod = $invoice_record['PaymentMethod'];


                //if ($PaymentStatus == 'Completed' || $PaymentStatus == 'completed') {
                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));

                $sms_field_array = array(
                    'Invoice Number' => $invoice_number,
                    'Loyalty Points' => $member_detail['TotalLoyaltyPoints'] - $member_detail['UsedLoyaltyPoints'],
                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                    'Currency' => $this->config->item("currency_symbol"),
                    'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                    'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                    'Pick Date' => date('d M Y', strtotime($pick_date)),
                    'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                );
                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                // $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                // }
            } else {
                $this->saveToTookaan($record, $action);
                $record = $this->SendInvoiceEmail($record, "U");

                /* in edit order
                if (SERVER == "com") {

                    //$updateResponse = $this->country->updateTaskServices($record, $member_record);
                } else {
                    
                }
                */
            }



            $this->RemoveSessionItems("OrderAction");
            $this->RemoveSessionItems("invoice_number");
            $this->RemoveSessionItems("invoice_id");
            die("done");
        } else {
            die("already done");
        }
    }

    public function notifyneworder()
    {


        $invoice_id = $this->GetSessionItem("invoice_id");
        $invoice_number = $this->GetSessionItem("invoice_number");
        $action = $this->GetSessionItem("OrderAction");

        if ($this->IsSessionItem("invoice_id")) {

            $member_id = $this->GetCurrentMemberID();
            $member_detail = $this->GetCurrentMemberDetail();

            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));

            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));
            $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_id), false, false, false, "PKInvoiceServiceID", "asc");


            $record["invoice"] = $invoice_record;
            $record["services"] = $invoice_service_records;
            $record["member"] = $member_record;

            //d($record);
            //d($action);

            $tookan_response_result = $tookan_response_result = $this->saveToTookaan($record, $action);

            if ($action == "insert") {

                $type = "C";

                /*
                if ($this->config->item("onfleet_enabled")==true) {
                    
                    $response = $this->country->createTask($record, $member_record);
                    $update_invoice_record["OnfleetResponse"] = $response;
                    $update_invoice_record["ID"] = $invoice_id;
                    $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                } else {
                    
                }*/

                $invoice_record = $record['invoice'];
                $pick_date = $invoice_record["PickupDate"];
                $pick_time = $invoice_record["PickupTime"];
                $PaymentStatus = $invoice_record['PaymentStatus'];
                $PaymentMethod = $invoice_record['PaymentMethod'];


                //if ($PaymentStatus == 'Completed' || $PaymentStatus == 'completed') {
                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));

                $sms_field_array = array(
                    'Invoice Number' => $invoice_number,
                    'Loyalty Points' => $member_detail['TotalLoyaltyPoints'] - $member_detail['UsedLoyaltyPoints'],
                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                    'Currency' => $this->config->item("currency_symbol"),
                    'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                    'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                    'Pick Date' => date('d M Y', strtotime($pick_date)),
                    'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                );
                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                // $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                // }
            } else {



                /* in edit order
                if (SERVER == "com") {

                    //$updateResponse = $this->country->updateTaskServices($record, $member_record);
                } else {
                    
                }
                */
                $type = "U";
            }
            $this->SendInvoiceEmail($record, $type);
            $this->RemoveSessionItems("OrderAction");
            $this->RemoveSessionItems("invoice_number");
            $this->RemoveSessionItems("invoice_id");
            die("done");
        } else {
            die("already done");
        }
    }

    public function savecard()
    {

        $this->load->library('stripe');
        $stripeKey = $this->GetStripeAPIKey();
        $post = $this->input->post(NULL, TRUE);
        $member_id = $this->GetCurrentMemberID();
        $card_id = $post['card_id'] ? $post['card_id'] : "";
        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
        $stripe_customer_id = $member_record['StripeCustomerID'];

        $stripe = new Stripe();
        $response["error"] = true;
        $header = 400;
        $response["errors"] = array();

        if (empty($post["first_name"])) {
            $response["errors"]["payment_first_name"] = "Please enter your first name.";
        }

        if (empty($post["last_name"])) {
            $response["errors"]["payment_last_name"] = $response['message'] = "Please enter your last name.";
        }

        if (empty($post["country_code"]) && $post["country_code"] != 0) {
            $response["errors"]["payment_phone"] = $response['message'] = "Please enter country code.";
        }


        if (empty($post["phone"])) {
            $response["errors"]["payment_phone"] = "Please enter your phone number.";
        } else if ($this->verifyPhoneNumber($post["country_code"], $post["phone"]) == false) {
            $response["errors"]["payment_phone"] = $response['message'] = "Incorrect phone numbers.";
        }

        if (empty($response["errors"])) {

            $data = array(
                'payment_method_types' => ['card'],
                "payment_method" => $post['payment_method_id'],
                "customer" => $stripe_customer_id,
                "confirm" => true
            );

            try {
                $setupIntent = $stripe->saveSetupIntent($stripeKey, $data);
                $payment_method = $stripe->getPaymentMethod($stripeKey, $post['payment_method_id']);

                $member_card = array(
                    'FKMemberID' => $member_id,
                    'Title' => "************" . $payment_method->card->last4,
                    'Name' => "************" . $payment_method->card->last4,
                    'Number' => $payment_method->card->last4,
                    'Month' => $payment_method->card->exp_month,
                    'Year' => $payment_method->card->exp_year,
                    'Code' => $post['payment_method_id'],
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
                $payment_method->attach(['customer' => $stripe_customer_id]);

                $card_id = $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
                $response["error"] = false;
                $response["message"] = "Card Added Successfully.";

                $update_member = array(
                    'ID' => $member_id,
                    'FirstName' => $post["first_name"],
                    'LastName' => $post["last_name"],
                    'CountryCode' => $post["country_code"],
                    'Phone' => ltrim($post["phone"], 0),
                );

                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');


                $response["id"] = $card_id;
                $header = 200;
            } catch (Stripe\Exception\CardException $e) {
                $response['exception'] = "CardException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\ApiErrorException $e) {
                $response['exception'] = "ApiErrorException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\RateLimitException $e) {
                $response['exception'] = "RateLimitException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\ApiConnectionException $e) {
                $response['exception'] = "ApiConnectionException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\InvalidRequestException $e) {
                $response['exception'] = "InvalidRequestException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\UnknownApiErrorException $e) {
                $response['exception'] = "UnknownApiErrorException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception $e) {
                $response['exception'] = "Exception";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\PermissionException $e) {
                $response['exception'] = "PermissionException";
                $response['errors']["payment_error"] = $e->getMessage();
            } catch (Stripe\Exception\UnexpectedValueException $e) {
                $response['exception'] = "UnexpectedValueException";
                $response['errors']["payment_error"] = $e->getMessage();
            }
        }


        echo jsonencode($response, $header);
    }

    public function savetimings()
    {

        $post = $this->input->post(NULL, TRUE);
        $cart_id = $this->GETSessionItem("cart_id");
        $response["error"] = true;
        $header = 400;
        $error = false;
        $response["error"] = true;

        if (empty($post['pickup_date'])) {
            $error = true;
            $response["errors"][] = "Please enter your collection date.";
        }

        if (empty($post['pickup_time'])) {
            $error = true;
            $response["errors"][] = "Please enter your collection time.";
        }

        if (empty($post['delivery_date'])) {
            $error = true;
            $response["errors"][] = "Please enter your delivery date.";
        }

        if (empty($post['delivery_time'])) {
            $error = true;
            $response["errors"][] = "Please enter your delivery time.";
        }

        if ($error == false) {


            $pick_dateTMP = $post['pickup_date'];
            $pick_timeTMP = $post['pickup_time'];

            $pick_timeArray = explode('-', $pick_timeTMP);
            $pick_timeClean = isset($pick_timeArray[1]) ? $pick_timeArray[1] . ':00' : '';

            $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;
            $now = date("Y-m-d H:i:s");
            $diffrencePickup = $this->dateTime_difference($pickupDateTimeTMP, $now);


            $delivery_dateTMP = $post['delivery_date'];
            $delivery_timeTMP = $post['delivery_time'];

            $delivery_timeArray = explode('-', $delivery_timeTMP);
            $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';

            $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

            $diffrenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);
            $franchise_id = $this->GetSessionItem("franchise_id");

            $franchise_record = $this->_GetFranchiseRecord($franchise_id);
            if ($diffrencePickup < $franchise_record["PickupDifferenceHour"] && empty($response["errors"])) {
                $response["errors"][] = "Dear Customer,<br/>Order Pickup difference should be at least " . $franchise_record["PickupDifferenceHour"] . " hours , please contact our customer support";
            } elseif ($diffrenceTMP < $franchise_record["DeliveryDifferenceHour"] && empty($response["errors"])) {

                $response["errors"][] = "Dear Customer,<br/>Order delivery difference should be at least " . $franchise_record["DeliveryDifferenceHour"] . " hours , please contact our customer support";
            } else {
                $header = 200;
                $response["error"] = false;
                $this->cart->update($post, array("id" => $cart_id));
            }
        }

        echo jsonencode($response, $header);
    }
}
