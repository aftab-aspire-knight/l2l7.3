<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Installapp extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {

        $this->load->library('mobiledetect');

        $storeLink = $this->config->item('store_link_short_url');
        $url = "https://www.love2laundry.com/store-link-generator";
        if ($this->mobiledetect->isiOS()) {
            $url = "https://apps.apple.com/gb/app/love-2-laundry/id964037034?ls=1";
        }

        if ($this->mobiledetect->isAndroidOS()) {
            $url = "https://play.google.com/store/apps/details?id=com.love2laundry.app";
            
        }
        header("location:$url");
        die();
    }
}