<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
        //$this->load->library('loqate');
        $this->load->library('excel');
        $this->load->model('cart');
        $this->load->model('cartservices');
        $this->load->helper('common');
    }

    function GetTrustPilotData($limit = false)
    {
        $trust_data_array = array();
        $page_content = $this->get_curl_request("https://s.trustpilot.com/tpelements/8309100/f.json.gz");
        if ($page_content != false) {
            $trust_review_data_array = array();
            $page_content_decode = gzdecode($page_content);
            $json_data = json_decode($page_content_decode);
            $count_record = 0;
            foreach ($json_data->Reviews as $key => $object) {
                if ($object->TrustScore->Stars == 5) {
                    $trust_review_data_array[$count_record]['Stars'] = $object->TrustScore->Stars;
                    $trust_review_data_array[$count_record]['content'] = $object->Content;
                    $trust_review_data_array[$count_record]['humanDate'] = $object->Created->HumanDate;
                    $trust_review_data_array[$count_record]['title'] = $object->Title;
                    $trust_review_data_array[$count_record]['userName'] = $object->User->Name;
                    $trust_review_data_array[$count_record]['Url'] = $object->Url;
                    $trust_review_data_array[$count_record]['Image'] = $object->User->ImageUrls->i24;
                    $trust_review_data_array[$count_record]['Locals'] = $object->User->Locale;
                    $count_record += 1;
                    if ($limit) {
                        if ($count_record == $limit) {
                            break;
                        }
                    }
                }
            }
            if (sizeof($trust_review_data_array) > 0) {
                $trust_data_array['review_ranking'] = $json_data->TrustScore->Human;
                $trust_data_array['review_count'] = $json_data->ReviewCount->Total;
                $trust_data_array['review_stars'] = $json_data->TrustScore->Stars;
                $trust_data_array['reviews'] = $trust_review_data_array;
            }
        }
        return $trust_data_array;
    }

    function subscribenewsletters()
    {

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $post_data = $this->input->post(NULL, TRUE);
        //$this->load->library('loqate');
        //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));

        $response["error"] = true;
        $response["error_type"] = "subscription_email";

        if (isset($post_data['email']) && !empty($post_data['email'])) {
            $subscriber_record = $this->model_database->GetRecord($this->tbl_subscribers, 'PKSubscriberID', array('EmailAddress' => $post_data['email']));
            if ($subscriber_record == false) {
                $insert_subscriber = array(
                    'EmailAddress' => $post_data['email'],
                    'Source' => isset($post_data['source']) ? $post_data['source'] : '',
                    'Status' => 'Enabled',
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
                $this->model_database->InsertRecord($this->tbl_subscribers, $insert_subscriber);
                $email_field_array = array(
                    'Email Sender' => $this->GetWebsiteEmailSender(),
                    'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                    'Website Name' => $this->GetWebsiteName(),
                    'Website URL' => $this->GetWebsiteURLAddress(),
                    'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                    'Website Email Address' => $this->GetWebsiteEmailAddress(),
                    'Header Menus' => $this->GetEmailHeaderMenus(),
                    'Footer Menus' => $this->GetEmailFooterMenus(),
                    'Footer Title' => $this->GetEmailFooterTitle(),
                    'Email Address' => $post_data['email']
                );
                $this->emailsender->send_email_responder(11, $email_field_array);
                $response["error"] = false;
                unset($response["error_type"]);
                $response["message"] = "You have successfully subscribed with us";
            } else {
                $response["error"] = "Email Address already exist in our records";
            }
        } else {
            $response["message"] = "Please enter your Email Address.";
        }
        echo json_encode($response);
        die;
    }

    public function getdownloadlink()
    {

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $post_data = $this->input->post(NULL, TRUE);
        //$this->load->library('loqate');
        //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));

        $response["error"] = true;
        $response["error_type"] = "mobile";

        if (isset($post_data['mobile']) && !empty($post_data['mobile'])) {
            $mobile = $post_data['mobile'];

            if ($this->verifyPhoneNumber($this->config->item("country_phone_code"), $post_data['phone_number']) != false) {
                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 4, 'Status' => 'Enabled'));
                if ($sms_responder_record != false) {
                    $sms_field_array = array(
                        'Store Link' => $this->config->item('store_link_short_url'),
                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName())
                    );
                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);



                    $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($mobile, 0), $msg_content);
                    unset($response["error_type"]);
                    $response["error"] = false;
                    $response["message"] = "Application link has been sent.";
                } else {
                    $response["message"] = "Sms sending failed";
                }
            } else {
                $response["message"] = "Please enter correct mobile number.";
            }
        } else {
            $response["message"] = "Please enter your mobile number.";
        }

        echo json_encode($response);
        die;
    }

    public function error_log()
    {

        try {

            $email = "aftab@yopmail.com";
            $source = "website";

            $insert_subscriber = array(
                'EmailaAddress' => $email,
                'Sourcea' => $source,
                'Status' => 'Enabled',
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $this->model_database->InsertRecord($this->tbl_subscribers, $insert_subscriber);
            print("ssss");
        } catch (ErrorException $e) {
            echo "ErrorException";
            //echo $e->getMessage();
        }
        $this->db->error();
    }

    public function index()
    {

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        if ($current_url == "home") {
            $this->_page_not_found();
            return;
        }
        $data['page_data'] = $this->GetPageRecord("Title", "Home", false);
        $data['is_work_widget_show'] = true;
        $data['is_why_widget_show'] = true;
        $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_video_widget_id);
        if ($widget_record != false) {
            $data['widget_video_record'] = $widget_record;
        }
        $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_about_widget_id);
        if ($widget_record != false) {
            $data['widget_about_record'] = $widget_record;
        }
        $data['testimonial_records'] = $this->model_database->GetRecords($this->tbl_testimonials, "R", "Title,Name,EmailAddress,Content,Rating,TestimonialDate", array('Status' => 'Enabled'), false, false, false, "TestimonialDate");
        if (sizeof($data['testimonial_records']) == 0) {
            unset($data['testimonial_records']);
        }
        $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_price_widget_id);
        if ($widget_record != false) {
            $data['widget_price_record'] = $widget_record;
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_price_widget_section_id);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_navigation_records;
            }
        }

        $data['is_home_page'] = true;

        $this->show_view_with_menu('home', $data);
    }

    // Page function
    function page()
    {
        $this->load->library('stripe');
        //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));


        $stripeKey = $this->GetStripeAPIKey();
        $current_url = isset($this->uri->segments[1]) ? $this->uri->segments[1] : '';
        $current_url = strtolower($current_url);
        $current_second_url = isset($this->uri->segments[2]) ? $this->uri->segments[2] : false;
        $current_third_url = isset($this->uri->segments[3]) ? $this->uri->segments[3] : false;
        if ($current_second_url) {
            $current_second_url = strtolower($current_second_url);
        }
        if ($current_third_url) {
            $current_third_url = strtolower($current_third_url);
        }

        $post_data = $this->input->post(NULL, TRUE);

        if ($post_data) {
            $data["postal_code_type"] = $this->config->item("postal_code_type");
            if ($this->input->is_ajax_request() == true) {

                if ($current_url == "check-availability") {
                    $this->RemoveSessionItems("invoice_type");
                    $this->RemoveSessionItems("admin_invoice_id");
                    $this->RemoveSessionItems("member_invoice_id");
                    $error_msg = "Please post the required values";
                    if (isset($post_data['post_code']) && !empty($post_data['post_code'])) {
                        $is_found = true;
                        $short_post_code_array = $this->country->ValidatePostalCode($post_data['post_code']);



                        if ($short_post_code_array['validate'] == false) {
                            echo "error||post_code||" . $data["postal_code_type"]["validation_message"];
                            return;
                            $is_found = false;
                        } else {
                            $short_post_code = $short_post_code_array['prefix'];
                            $short_suffix = $short_post_code_array['suffix'];
                            $tmpPostCode = $short_post_code . $short_suffix;

                            /*
                              $post_latitude_longitude = $this->country->GetLatitudeLongitude($tmpPostCode);

                              if ($post_latitude_longitude == "") {
                              $tmpPostCode = $short_post_code . ' ' . $short_suffix;
                              $post_latitude_longitude = $this->country->GetLatitudeLongitude($tmpPostCode);
                              }

                              if ($post_latitude_longitude == "") {

                              $post_latitude_longitude = $this->country->getAddresses($post_data['post_code']);
                              }
                             */

                            $addresses = $this->country->getAddresses($post_data['post_code']);
                            if ($addresses["error"] == true) {
                                $is_found = false;
                            } else {

                                $post_code = $post_data['post_code'];
                                //$franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code_array['prefix']));

                                $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code_array['prefix']);

                                if ($franchise_post_code_record != false) {
                                    $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                                } else {
                                    $franchise_id = 0;
                                    $response["message"] = $this->config->item('validation_message');
                                    $is_found = false;
                                }

                                $insert_search_post_code = array(
                                    'FKFranchiseID' => $franchise_id,
                                    'Code' => $post_code,
                                    'IPAddress' => $this->input->ip_address(),
                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                                if ($franchise_id != 0) {
                                    $franchise_record = $this->_GetFranchiseRecord($franchise_id);

                                    if ($franchise_record != false) {

                                        $this->AddSessionItem("franchise_id", $franchise_id);
                                        $this->AddSessionItem($this->session_post_code, $post_code);
                                        $this->AddSessionItem($this->session_franchise_record, $franchise_record);
                                    } else {
                                        $is_found = false;
                                    }
                                } else {
                                    $is_found = false;
                                }
                            }
                        }


                        if ($is_found) {
                            $cart_id = $this->GETSessionItem("cart_id");
                            if (isset($cart_id)) {
                                $this->cart->delete(array("id" => $cart_id));
                                $this->cartservices->delete(array("cart_id" => $cart_id));
                            }
                            echo "success||t||" . base_url("booking");
                        } else {
                            echo "error||t||" . base_url($this->config->item('front_post_code_not_found_page_url'));
                        }
                    } else {
                        echo "error||msg||" . $error_msg;
                    }
                } else if ($current_url == "time-collections") {
                    $is_valid = false;
                    $more_pickup = $post_data['more_pickup'];

                    if (isset($post_data['post_code']) && !empty($post_data['post_code'])) {
                        $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_data['post_code']);
                        if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) > 5) {
                            $franchise_id = 0;
                            $short_post_code_array = $this->country->ValidatePostalCode($post_data['post_code']);
                            if ($short_post_code_array['validate'] == false) {
                                echo "error||Please enter your full postcode e.g. E12PE";
                                return;
                            } else {
                                $short_post_code = $short_post_code_array['prefix'];
                                //$franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                                $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code);
                                if ($franchise_post_code_record != false) {
                                    $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                                }
                            }
                            $insert_search_post_code = array(
                                'Code' => $post_data['post_code'],
                                'IPAddress' => $this->input->ip_address()
                            );
                            $franchise_record = $this->_GetFranchiseRecord($franchise_id);

                            if ($franchise_record != false) {
                                $insert_search_post_code['FKFranchiseID'] = $franchise_id;
                                $opening_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['OpeningTime']));
                                $closing_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['ClosingTime']));
                                $total_hours = ((strtotime($closing_franchise_time) - strtotime($opening_franchise_time)) / (60 * 60));
                                /* Hassan changes 23-01-2018 */
                                // $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $franchise_id), false, false, false, false, 'asc');
                                /* ends */
                                $time_collection_array = array();
                                $time_collection_array["id"] = $franchise_id;
                                if ($total_hours > 0) {

                                    if (intval($franchise_record['MinimumOrderAmount']) > 0) {
                                        $time_collection_array['minimum_order_amount_later'] = $franchise_record['MinimumOrderAmount'];
                                    } else {
                                        $time_collection_array['minimum_order_amount_later'] = $this->GetOrderAmountLater();
                                    }
                                    if (intval($franchise_record['MinimumOrderAmount']) > 0) {
                                        $time_collection_array['minimum_order_amount'] = $franchise_record['MinimumOrderAmount'];
                                    } else {
                                        $time_collection_array['minimum_order_amount'] = $this->GetOrderAmount();
                                    }
                                    $time_collection_array['delivery_difference_hour'] = $franchise_record['DeliveryDifferenceHour'];
                                    $time_collection_array['delivery_option'] = $franchise_record['DeliveryOption'];
                                    $time_collection_array['pick_up'] = array();
                                    $time_collection_array['delivery'] = array();
                                    $pick_array_count = 0;
                                    $delivery_array_count = 0;

                                    $today = date("Y-m-d H:i:s");
                                    $now = date("H:i:s");

                                    //for ($i = 0; $i <= 100; $i++) {}

                                    $is_valid = true;

                                    echo "success||" . json_encode($time_collection_array, true);
                                }
                            }
                            $insert_search_post_code['CreatedDateTime'] = date('Y-m-d H:i:s');
                            $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                        }
                    }
                    if (!$is_valid) {
                        echo "error||Sorry, We are not in your area yet.";
                    }
                } else if ($current_url == "subscribe") {
                    $subscriber_record = $this->model_database->GetRecord($this->tbl_subscribers, 'PKSubscriberID', array('EmailAddress' => $post_data['subscriber_email_address']));
                    if ($subscriber_record == false) {
                        $insert_subscriber = array(
                            'EmailAddress' => $post_data['subscriber_email_address'],
                            'Source' => isset($post_data['source']) ? $post_data['source'] : '',
                            'Status' => 'Enabled',
                            'CreatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->InsertRecord($this->tbl_subscribers, $insert_subscriber);
                        $email_field_array = array(
                            'Email Sender' => $this->GetWebsiteEmailSender(),
                            'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                            'Website Name' => $this->GetWebsiteName(),
                            'Website URL' => $this->GetWebsiteURLAddress(),
                            'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                            'Website Email Address' => $this->GetWebsiteEmailAddress(),
                            'Header Menus' => $this->GetEmailHeaderMenus(),
                            'Footer Menus' => $this->GetEmailFooterMenus(),
                            'Footer Title' => $this->GetEmailFooterTitle(),
                            'Email Address' => $post_data['subscriber_email_address']
                        );
                        $this->emailsender->send_email_responder(11, $email_field_array);
                        echo "success||field||subscriber_email_address||You are successfully subscribed with us";
                    } else {
                        echo "error||subscriber_email_address||Email Address already exist in our records";
                    }
                } else if ($current_url == "app-request") {
                    $mobile_no = "";

                    $response = $this->verifyGoogleCaptcha($post_data["_token"]);

                    if ($response["success"] == 1) {
                        if (isset($post_data['app_mobile_no'])) {
                            $mobile_no = $post_data['app_mobile_no'];
                        } else if (isset($post_data['app_mobile_no_inner'])) {
                            $mobile_no = $post_data['app_mobile_no_inner'];
                        }
                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 4, 'Status' => 'Enabled'));
                        if ($sms_responder_record != false) {
                            $sms_field_array = array(
                                'Store Link' => $this->config->item('store_link_short_url'),
                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName())
                            );
                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                            $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($mobile_no, 0), $msg_content);
                            echo "success||msg||Message Send Successfully";
                        } else {
                            echo "error||app_mobile_no||sms sending failed";
                        }
                    }
                } else if ($current_url == "login") {
                    if (isset($post_data['t'])) {
                        if ($post_data['t'] == "login-cookie") {
                            echo $this->GetCookie($this->login_cookie_name);
                        } else {
                            echo "";
                        }
                    } else {
                        if ($this->IsMemberLogin() == false) {
                            $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,StripeCustomerID,PKMemberID", array('EmailAddress' => $post_data['login_email_address'], 'Password' => encrypt_decrypt('encrypt', $post_data['login_password']), 'Status' => 'Enabled'));

                            //d($member_record,1);
                            if ($member_record != false) {

                                if (empty($member_record["StripeCustomerID"])) {

                                    $stripe = new Stripe();
                                    $customer = $stripe->saveCustomer($stripeKey, ["email" => $member_record['EmailAddress'], "name" => $member_record['FirstName'] . " " . $member_record['LastName']]);


                                    $update_member = array(
                                        'ID' => $member_record['PKMemberID'],
                                        'StripeCustomerID' => $customer->id
                                    );
                                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                }


                                $this->_CreateMemberSession($member_record['PKMemberID']);

                                $member_record = $this->GetCurrentMemberDetail();
                                if (!isset($post_data['is_order_area'])) {
                                    if (isset($post_data['login_remember'])) {
                                        $this->CreateCookie($this->login_cookie_name, $post_data['login_email_address'] . "||" . $post_data['login_password'], time() + 86400 * 2);
                                    } else {
                                        $this->DeleteCookie($this->login_cookie_name);
                                    }
                                    echo "success||t||" . base_url($this->config->item('front_dashboard_page_url'));
                                } else {
                                    $member_array = array(
                                        'FirstName' => $member_record['FirstName'],
                                        'LastName' => $member_record['LastName'],
                                        'EmailAddress' => $member_record['EmailAddress'],
                                        'Phone' => $member_record['Phone']
                                    );
                                    if (!empty($member_record['PostalCode'])) {
                                        $member_array['Text'] = "Use Default Address";
                                        $member_array['BuildingName'] = $member_record['BuildingName'];
                                        $member_array['StreetName'] = $member_record['StreetName'];
                                        $member_array['PostalCode'] = $member_record['PostalCode'];
                                        $member_array['Town'] = $member_record['Town'];
                                    }
                                    $member_array['member_card_records'] = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as ID,Title,Name,Number as CardNumber,Year as ExpireYear,Month as ExpireMonth,Code as SecurityCode", array('FKMemberID' => $member_record["PKMemberID"]), false, false, false, "PKCardID");
                                    if (sizeof($member_array['member_card_records']) == 0) {
                                        unset($member_array['member_card_records']);
                                    } else {
                                        foreach ($member_array['member_card_records'] as $key => $value) {
                                            $member_array['member_card_records'][$key]['CardNumber'] = $value['CardNumber'];
                                            $member_array['member_card_records'][$key]['CardNumberMasked'] = $member_array['member_card_records'][$key]['CardNumber'];
                                            $member_array['member_card_records'][$key]['SecurityCode'] = $value['SecurityCode'];
                                            $member_array['member_card_records'][$key]['SecurityCodeMasked'] = $member_array['member_card_records'][$key]['SecurityCode'];
                                        }
                                    }
                                    echo "success||login||Login Successfully...||" . json_encode($member_array);
                                }
                            } else {
                                echo "error||login_password||Incorrect Username/Password";
                            }
                        } else {
                            echo "error||login_password||you are already login please refresh the page.";
                        }
                    }
                } else if ($current_url == "forgot") {
                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('EmailAddress' => $post_data['forgot_email_address'], 'Status' => 'Enabled'));
                    if ($member_record !== false) {
                        $random_password = $this->RandomPassword();
                        $update_member = array(
                            'ID' => $member_record['PKMemberID'],
                            'Password' => encrypt_decrypt("encrypt", $random_password)
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                        $email_field_array = array(
                            'Email Sender' => $this->GetWebsiteEmailSender(),
                            'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                            'Website Name' => $this->GetWebsiteName(),
                            'Website URL' => $this->GetWebsiteURLAddress(),
                            'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                            'Website Email Address' => $this->GetWebsiteEmailAddress(),
                            'Header Menus' => $this->GetEmailHeaderMenus(),
                            'Footer Menus' => $this->GetEmailFooterMenus(),
                            'Footer Title' => $this->GetEmailFooterTitle(),
                            'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                            'Password' => $random_password,
                            'Email Address' => $member_record['EmailAddress'],
                            'Phone Number' => $member_record['Phone']
                        );
                        $this->emailsender->send_email_responder(3, $email_field_array);
                        $this->emailsender->send_email_responder(8, $email_field_array);

                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 10, 'Status' => 'Enabled'));
                        if ($sms_responder_record != false) {
                            $sms_field_array = array(
                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                'Password' => $random_password
                            );
                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                            $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                        }
                        echo "success||forgot||Password send to your desired email please check your inbox";
                    } else {
                        echo "error||forgot_email_address||Email address not found in our records";
                    }
                } else if ($current_url == "register") {

                    $response = $this->verifyGoogleCaptcha($post_data["_token"]);

                    $data["postal_code_type"] = $this->config->item("postal_code_type");


                    if (empty($post_data['reg_first_name'])) {
                        echo "error||reg_first_name||Please enter your first name.";
                        return false;
                    }

                    if (empty($post_data['reg_last_name'])) {
                        echo "error||reg_last_name||Please enter your last name.";
                        return false;
                    }

                    if (!$this->validateEmail($post_data['reg_email_address'])) {
                        echo "error||reg_email_address||Please enter correct email address.";
                        return false;
                    }

                    if (empty($post_data['reg_password'])) {
                        echo "error||reg_password||Please enter your password.";
                        return false;
                    }

                    if (strlen($post_data['reg_password']) < 6) {
                        echo "error||reg_password||Password length should be more than 6 characters.";
                        return false;
                    }

                    if (strlen($post_data['countryCode']) < 1) {
                        echo "error||reg_phone_number||Please select country code.";
                        return false;
                    }

                    $post_data['reg_phone_number'] = preg_replace('/\s+/', '', $post_data['reg_phone_number']);
                    $post_data['reg_phone_number'] = $phone = ltrim($post_data['reg_phone_number'], 0);


                    if ($this->verifyPhoneNumber($post_data['countryCode'], $phone) == false) {
                        echo "error||reg_phone_number||Incorrect phone number.";
                        return false;
                    }

                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('EmailAddress' => $post_data['reg_email_address']));
                    if ($member_record != false) {
                        echo "error||reg_email_address||Email address already exist in our records";
                        return false;
                    }

                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('CountryCode' => $post_data['countryCode'], 'Phone' => $post_data['reg_phone_number']));
                    if ($member_record != false) {
                        echo "error||reg_phone_number||Phone Number already exist in our records";
                        return false;
                    }


                    $short_post_code_array = $this->country->ValidatePostalCode($post_data['register_post_code']);

                    if ($short_post_code_array["validate"] == false) {
                        echo "error||register_post_code||Incorrect postal code.";
                        return false;
                    }

                    if ($data["postal_code_type"]["title"] == "Area") {
                        $buildingName = $post_data['register_post_code'];
                    } else {
                        if (empty($post_data['addresses'])) {
                            echo "error||addresses||Please enter your address.";
                            return false;
                        }
                        //$data["postal_code_type"] = $this->config->item("postal_code_type");


                        if ($post_data['addresses'] == "-1") {
                            if (empty($post_data['reg_address'])) {
                                echo "error||reg_address||Please enter your address.";
                                return false;
                            }
                            $buildingName = $post_data['reg_address'];
                        } else {
                            $buildingName = $post_data['addresses'];
                        }
                    }

                    /*
                      if (empty($post_data['town'])) {
                      echo "error||town||There is a problem in your postal code.";
                      return false;
                      }
                     */


                    $FirstName = ($post_data['reg_first_name']) ? $post_data['reg_first_name'] : '';

                    $stripe = new Stripe();
                    $customer = $stripe->saveCustomer($stripeKey, ["email" => $post_data['reg_email_address'], "name" => $post_data['reg_first_name'] . " " . $post_data['reg_last_name']]);



                    //$address = explode(",", $post_data['addresses']);
                    //$post_data['reg_building_name'] = $post_data['addresses'];

                    $insert_member = array(
                        'FirstName' => $post_data['reg_first_name'],
                        'LastName' => $post_data['reg_last_name'],
                        'EmailAddress' => $post_data['reg_email_address'],
                        'Password' => encrypt_decrypt('encrypt', $post_data['reg_password']),
                        'CountryCode' => $post_data['countryCode'],
                        'Phone' => $post_data['reg_phone_number'],
                        'PostalCode' => $post_data['register_post_code'],
                        'BuildingName' => $buildingName,
                        'StreetName' => $post_data['reg_address2'],
                        'Town' => $post_data['town'],
                        'Status' => 'Enabled',
                        'RegisterFrom' => 'Desktop',
                        'PopShow' => 'Yes',
                        'StripeCustomerID' => $customer->id,
                        'ReferralCode' => $this->random_str($FirstName),
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $type = $this->GetSessionItem("type");

                    if ($this->GetSessionItem("social_id")) {
                        $social_id = $this->GetSessionItem("social_id");

                        if ($social_id == $post_data["social_id"]) {
                            if ($type == "facebook") {
                                $insert_member['FacebookID'] = $this->GetSessionItem("social_id");
                            } else {
                                $insert_member['GoogleID'] = $this->GetSessionItem("social_id");
                            }
                        }
                    }


                    // $post_latitude_longitude = $this->GetLatitudeLongitude($post_data['reg_post_code']);

                    $short_post_code_array = $this->country->ValidatePostalCode($post_data['reg_post_code']);
                    $short_post_code = $short_post_code_array['prefix'];
                    //$franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));

                    $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code);

                    if ($franchise_post_code_record != false) {
                        $insert_member['FKFranchiseID'] = $franchise_post_code_record['FKFranchiseID'];
                    }


                    $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);
                    $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('PKMemberID' => $member_id));
                    if ($member_record !== false) {


                        $MemberPreferences = $this->_GetMemberPreferences();
                        if (empty($MemberPreferences)) {
                            $Preferences = $this->_GetPreferencesRegistration();

                            if (isset($Preferences) && $Preferences !== false) {
                                foreach ($Preferences as $key => $value) {
                                    $child_records = (isset($value['child_records']) && !empty($value['child_records'])) ? $value['child_records'] : array();
                                    if (isset($child_records[0]['PKPreferenceID']) && $child_records[0]['PKPreferenceID'] > 0) {
                                        $insert_member_preference = array(
                                            'FKMemberID' => $member_id,
                                            'FKPreferenceID' => $child_records[0]['PKPreferenceID']
                                        );
                                        $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                                    } else {
                                        continue;
                                    }
                                }
                            }
                        }

                        $this->_CreateMemberSession($member_record['PKMemberID']);
                        $member_record = $this->GetCurrentMemberDetail();
                        $email_field_array = array(
                            'Email Sender' => $this->GetWebsiteEmailSender(),
                            'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                            'Website Name' => $this->GetWebsiteName(),
                            'Website URL' => $this->GetWebsiteURLAddress(),
                            'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                            'Website Email Address' => $this->GetWebsiteEmailAddress(),
                            'Header Menus' => $this->GetEmailHeaderMenus(),
                            'Footer Menus' => $this->GetEmailFooterMenus(),
                            'Footer Title' => $this->GetEmailFooterTitle(),
                            'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                            'First Name' => $member_record['FirstName'],
                            'Last Name' => $member_record['LastName'],
                            'Email Address' => $member_record['EmailAddress'],
                            'Phone Number' => $member_record['Phone'],
                            'Postal Code' => $member_record['PostalCode'],
                            'Building Name' => $member_record['BuildingName'],
                            'Street Name' => $member_record['StreetName'],
                            'Town' => $member_record['Town'],
                            'Register From' => $member_record['RegisterFrom']
                        );

                        $this->emailsender->send_email_responder(1, $email_field_array);
                        $this->emailsender->send_email_responder(6, $email_field_array);

                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 11, 'Status' => 'Enabled'));
                        if ($sms_responder_record != false) {
                            $sms_field_array = array(
                                'Store Link' => $this->config->item('store_link_short_url'),
                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                'Email Address' => $post_data['reg_email_address'],
                                'Password' => $post_data['reg_password']
                            );
                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                            $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                        }
                        $this->RemoveSessionItems(array("social_id", "type", "reg_email", "reg_first_name", "reg_last_name"));

                        $this->AddSessionItem("Register_New_Member", true);
                        echo "success||t||" . base_url('welcome');
                    } else {
                        echo "error||security_code||some thing went wrong";
                    }
                } else if ($current_url == "update-referral-code") {
                    $member_id = $this->GetCurrentMemberID();
                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $member_id, 'ReferralCode' => $post_data['referral_code']));
                    if ($member_record != false) {
                        echo "error||referral_code||Referral Code already exist in our records";
                        return false;
                    }
                    $update_member = array(
                        'ReferralCode' => $post_data['referral_code'],
                        'ID' => $member_id,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    echo "success||field||referral_code||Referral Code Updated Successfully.";
                } else if ($current_url == "update-account") {
                    $captcha_code = trim(strtolower($post_data['security_code']));
                    if ($captcha_code != $this->CaptchaCode("account")) {
                        echo "error||security_code||Incorrect Security Code";
                        return;
                    }


                    $phone = isset($post_data['phone_number']) ? $post_data['phone_number'] : '';
                    $countryCode = isset($post_data['countryCode']) ? $post_data['countryCode'] : '';

                    $member_id = $this->GetCurrentMemberID();
                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $member_id, 'EmailAddress' => $post_data['email_address']));
                    if ($member_record != false) {
                        echo "error||email_address||Email address already exist in our records";
                        return false;
                    }

                    if (empty($phone)) {
                        echo "error||phone_number||Please enter your phone number.";
                        return false;
                    }

                    if ($this->verifyPhoneNumber($countryCode, $phone) == false) {
                        echo "error||phone_number||Please enter correct phone number.";
                        return false;
                    }


                    $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $member_id, 'Phone' => $phone, 'CountryCode' => ltrim($countryCode, "+")));
                    if ($member_record != false) {
                        echo "error||phone_number||Phone Number already exist in our records.";
                        return false;
                    }

                    if (!empty($post_data['referral_code'])) {
                        $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $member_id, 'ReferralCode' => $post_data['referral_code']));
                        if ($member_record != false) {
                            echo "error||referral_code||Referral Code already exist in our records";
                            return false;
                        }
                    }
                    $update_member = array(
                        //'ReferralCode' => $post_data['referral_code'],
                        'FirstName' => $post_data['first_name'],
                        'LastName' => $post_data['last_name'],
                        'EmailAddress' => $post_data['email_address'],
                        'Phone' => $phone,
                        'CountryCode' => $countryCode,
                        //'PostalCode' => $post_data['postal_code'],
                        //'BuildingName' => $post_data['building_name'],
                        //'StreetName' => $post_data['street_name'],
                        //'Town' => $post_data['town'],
                        'ID' => $member_id,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );

                    /*
                      $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_data['postal_code']);
                      if ($post_latitude_longitude != "") {
                      if (strlen($post_latitude_longitude) > 5) {
                      $short_post_code_array = $this->country->ValidatePostalCode($post_data['postal_code']);
                      if ($short_post_code_array['validate'] == true) {
                      $short_post_code = $short_post_code_array['prefix'];
                      //$franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                      $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code);
                      if ($franchise_post_code_record != false) {
                      $update_member['FKFranchiseID'] = $franchise_post_code_record['FKFranchiseID'];
                      }
                      }
                      }
                      } */
                    if (!empty($post_data['u_password'])) {
                        $update_member['Password'] = encrypt_decrypt('encrypt', $post_data['u_password']);
                    }
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                    if ($member_record !== false) {
                        $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                    }
                    echo "success||msg-t||Your Account Setting Updated Successfully.||" . base_url($this->config->item('front_member_account_page_url'));
                } else if ($current_url == "update-preferences") {
                    $member_id = $this->GetCurrentMemberID();
                    $this->model_database->RemoveRecord($this->tbl_member_preferences, $member_id, "FKMemberID");
                    foreach ($post_data['preferences_list'] as $preference_id) {
                        if ($preference_id != "") {
                            $insert_member_preference = array(
                                'FKMemberID' => $member_id,
                                'FKPreferenceID' => $preference_id
                            );
                            $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                        }
                    }

                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                    if ($member_record !== false) {
                        $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                    }
                    echo "success||msg-t||Your Wash Preferences Updated Successfully.||" . base_url($this->config->item('front_member_laundry_page_url'));
                } else if ($current_url == "payment-card") {
                    if ($post_data['c_type'] == "Remove" || $post_data['c_type'] == "Edit") {
                        if ($post_data['c_type'] == "Remove") {
                            $this->model_database->RemoveRecord($this->tbl_member_cards, $post_data['card_id'], "PKCardID");
                            echo "success";
                        } else {
                            $member_detail = $this->model_database->GetRecord($this->tbl_member_cards, "PKCardID as ID,Title,Name,Number,Year,Month,Code", array('FKMemberID' => $this->GetCurrentMemberID(), "PKCardID" => $post_data['card_id']));
                            if ($member_detail != false) {
                                $member_detail['Number'] = $member_detail['Number'];
                                $member_detail['NumberMasked'] = $member_detail['Number'];
                                $member_detail['Code'] = $member_detail['Code'];
                                $member_detail['CodeMasked'] = $member_detail['Code'];
                            }
                            echo json_encode($member_detail);
                        }
                    } else if ($post_data['c_type'] == "Update" || $post_data['c_type'] == "Add") {
                        $member_id = $this->GetCurrentMemberID();
                        $card_id = $post_data['card_id'];
                        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                        $stripe_customer_id = $member_record['StripeCustomerID'];
                        ///aftab payment
                        $stripe = new Stripe();

                        $data = array(
                            'payment_method_types' => ['card'],
                            "payment_method" => $post_data['payment_method_id'],
                            "customer" => $stripe_customer_id,
                            "confirm" => true
                            //"on_behalf_of"=>$stripe_customer_id
                        );
                        try {
                            $setupIntent = $stripe->saveSetupIntent($stripeKey, $data);


                            $payment_method = $stripe->getPaymentMethod($stripeKey, $post_data['payment_method_id']);


                            $member_card = array(
                                'FKMemberID' => $member_id,
                                'Title' => "************" . $payment_method->card->last4,
                                'Name' => "************" . $payment_method->card->last4,
                                'Number' => $payment_method->card->last4,
                                'Month' => $payment_method->card->exp_month,
                                'Year' => $payment_method->card->exp_year,
                                'Code' => $post_data['payment_method_id'],
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $payment_method->attach(['customer' => $stripe_customer_id]);
                            $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
                            if (empty($card_id)) {
                                echo "success||msg-t||Card Added Successfully.||" . base_url($this->config->item('front_member_payment_page_url'));
                                die;
                            } else {
                                $member_card['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                $member_card['ID'] = $card_id;
                                $this->model_database->UpdateRecord($this->tbl_member_cards, $member_card, 'PKCardID');
                                echo "success||msg-t||Card Updated Successfully.||" . base_url($this->config->item('front_member_payment_page_url'));
                                die;
                            }
                        } catch (Stripe\Exception\CardException $e) {
                            $response['exception'] = "CardException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\ApiErrorException $e) {
                            $response['exception'] = "ApiErrorException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\RateLimitException $e) {
                            $response['exception'] = "RateLimitException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\ApiConnectionException $e) {
                            $response['exception'] = "ApiConnectionException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\InvalidRequestException $e) {
                            $response['exception'] = "InvalidRequestException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\UnknownApiErrorException $e) {
                            $response['exception'] = "UnknownApiErrorException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception $e) {
                            $response['exception'] = "Exception";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\PermissionException $e) {
                            $response['exception'] = "PermissionException";
                            $response['message'] = $e->getMessage();
                        } catch (Stripe\Exception\UnexpectedValueException $e) {
                            $response['exception'] = "UnexpectedValueException";
                            $response['message'] = $e->getMessage();
                        }

                        echo "error||btn_back||" . $response['message'];
                        die;
                    }
                } else if ($current_url == "contact-us") {
                    $captcha_code = trim(strtolower($post_data['security_code']));
                    if ($captcha_code != $this->CaptchaCode("contact")) {
                        echo "error||security_code||Incorrect Security Code";
                        return;
                    }
                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $post_data['name'],
                        'Email Address' => $post_data['email_address'],
                        'Phone Number' => $post_data['phone_number'],
                        'Address' => $post_data['address'],
                        'Message' => $post_data['message']
                    );
                    $this->emailsender->send_email_responder(2, $email_field_array);
                    $this->emailsender->send_email_responder(7, $email_field_array);
                    $this->AddSessionItem("Contact_Success", true);
                    echo "success||t||" . base_url('contact-thank-you');
                } else if ($current_url == "crowd-funding") {
                    $captcha_code = trim(strtolower($post_data['security_code']));
                    if ($captcha_code != $this->CaptchaCode("crowd-funding")) {
                        echo "error||security_code||Incorrect Security Code";
                        return;
                    }
                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $post_data['name'],
                        'Email Address' => $post_data['email_address'],
                        'Grand Total' => $post_data['amount']
                    );
                    $this->emailsender->send_email_responder(14, $email_field_array);
                    echo "success||t||" . base_url('crowd-funding-thank-you');
                } else if ($current_url == "business-enquiry") {
                    $captcha_code = trim(strtolower($post_data['security_code']));

                    /* if ($captcha_code != $this->CaptchaCode("business-enquiry")) {
                      echo "error||security_code||Incorrect Security Code";
                      return;
                      } */


                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $post_data['name'],
                        'Email Address' => $post_data['email_address'],
                        'Phone Number' => $post_data['phone_number'],
                        'Nature Of Enquiry' => $post_data['nature_of_enquiry'],
                        'Message' => $post_data['message']
                    );
                    $this->emailsender->send_email_responder(12, $email_field_array);
                    $this->emailsender->send_email_responder(13, $email_field_array);
                    $this->AddSessionItem("Business_Service_Success", true);
                    echo "success||t||" . base_url('business-service-thank-you');
                } else if ($current_url == "feedback") {
                    $captcha_code = trim(strtolower($post_data['security_code']));
                    if ($captcha_code != $this->CaptchaCode("feedback")) {
                        echo "error||security_code||Incorrect Security Code";
                        return;
                    }
                    $insert_testimonial = array(
                        'Title' => $post_data['testimonial_title'],
                        'Name' => $post_data['testimonial_name'],
                        'EmailAddress' => $post_data['testimonial_email_address'],
                        'Content' => $post_data['testimonial_comment'],
                        'Rating' => $post_data['testimonial_rating'],
                        'Status' => "Disabled",
                        'TestimonialDate' => date('Y-m-d'),
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->InsertRecord($this->tbl_testimonials, $insert_testimonial);
                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $post_data['testimonial_name'],
                        'Email Address' => $post_data['testimonial_email_address'],
                        'Title' => $post_data['testimonial_title'],
                        'Rating' => $post_data['testimonial_rating'],
                        'Message' => $post_data['testimonial_comment']
                    );
                    $this->emailsender->send_email_responder(4, $email_field_array);
                    $this->emailsender->send_email_responder(9, $email_field_array);
                    echo "success||testimonial||Thank you for your feedback.";
                } else if ($current_url == "order-cancel") {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $post_data['id']));
                    if ($invoice_record != false) {
                        $update_invoice = array(
                            'OrderStatus' => 'Cancel',
                            'ID' => $invoice_record['PKInvoiceID'],
                            'TookanResponse' => null,
                            'OnfleetResponse' => null,
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        //$this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                        if ($this->config->item("onfleet_enabled") == true) {

                            $update_invoice["OnfleetResponse"] = $this->country->cancelTask($invoice_record);
                        } else {
                        }
                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                        if (isset($tooken_response_array['data']['job_id'])) {
                            $tookan_array = array(
                                'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                'job_id' => $tooken_response_array['data']['job_id']
                            );
                            $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                            $update_invoice['TookanResponse'] = null;
                            $update_invoice['TookanUpdateResponse'] = $tookan_response_result;
                        }

                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");


                        echo "success";
                    } else {
                        echo "error";
                    }
                } else if ($current_url == "reorder") {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('InvoiceNumber' => $post_data['id']));
                    if ($invoice_record != false) {
                        $update_invoice = array(
                            'OrderStatus' => 'Pending',
                            'ID' => $invoice_record['PKInvoiceID'],
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                        echo "success";
                    } else {
                        echo "error";
                    }
                } else if ($current_url == "address") {

                    $is_found = true;


                    if (!empty($post_data['postal_code'])) {
                        $address = "";

                        if ($post_data['location'] == 'Add' || $post_data['location'] == '0') {
                            if (empty($post_data['addresses'])) {
                                echo "error||address||Please choose address.";
                                return false;
                            } else {
                                $address = $post_data['addresses'];
                            }

                            if ($post_data['addresses'] == "-1") {
                                if (empty($post_data['address'])) {
                                    echo "error||address||Please enter address.";
                                    return false;
                                }
                                $address = $post_data['address'];
                            }
                        }

                        $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_data['postal_code']);
                        if ($post_latitude_longitude == "") {
                            $is_found = false;
                        } else {
                            if (strlen($post_latitude_longitude) < 5) {
                                $is_found = false;
                            } else {
                                $post_code = $post_data['postal_code'];

                                $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                                if ($short_post_code_array['validate'] == false) {
                                    echo "error||postal_code||Please enter your full postcode e.g. E12PE";
                                    return;
                                    $is_found = false;
                                } else {
                                    $short_post_code = $short_post_code_array['prefix'];
                                    //$franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                                    $franchise_post_code_record = $this->model_database->GetFranchiseByPrefix($short_post_code);
                                    if ($franchise_post_code_record != false) {
                                        $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                                    }
                                    $insert_search_post_code = array(
                                        'FKFranchiseID' => $franchise_id,
                                        'Code' => $post_code,
                                        'IPAddress' => $this->input->ip_address(),
                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                    );
                                    $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                                    if ($franchise_id != 0) {
                                        $franchise_record = $this->_GetFranchiseRecord($franchise_id);
                                        if ($franchise_record != false) {
                                            $this->AddSessionItem($this->session_post_code, $post_code);
                                            $this->AddSessionItem($this->session_franchise_record, $franchise_record);


                                            $this->AddSessionItem($this->address1, $address);
                                            $this->AddSessionItem($this->address2, $post_data['street_name']);
                                            $this->AddSessionItem($this->town, $post_data['town']);


                                            $this->AddSessionItem($this->pickup_date, $post_data['pickup_date']);
                                            $this->AddSessionItem($this->pickup_time, $post_data['pickup_time']);
                                            $this->AddSessionItem($this->delivery_date, $post_data['delivery_date']);
                                            $this->AddSessionItem($this->delivery_time, $post_data['delivery_time']);
                                        } else {
                                            $is_found = false;
                                        }
                                    } else {
                                        $is_found = false;
                                    }
                                }
                            }
                        }
                    }
                    if ($is_found) {
                        if ($this->IsMemberLogin() == false) {


                            if (!$this->validateEmail($post_data['email_address'])) {
                                echo "error||email_address||Please enter a valid email address.";
                                return false;
                            }


                            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('EmailAddress' => $post_data['email_address']));
                            if ($member_record != false) {
                                echo "error||email_address||Email address already exist in our records";
                                return false;
                            }

                            $post_data['phone_number'] = preg_replace('/\s+/', '', $post_data['phone_number']);
                            $post_data['phone_number'] = ltrim($post_data['phone_number'], 0);


                            if ($this->verifyPhoneNumber($this->config->item("country_phone_code"), $post_data['phone_number']) == false) {
                                echo "error||phone_number||Phone number is not valid";
                                return false;
                            }


                            $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('Phone' => $post_data['phone_number']));
                            if ($member_record != false) {
                                echo "error||phone_number||Phone Number already exist in our records";
                                return false;
                            }
                            $FirstName = ($post_data['first_name']) ? $post_data['first_name'] : '';

                            $stripe = new Stripe();
                            $customer = $stripe->saveCustomer($stripeKey, ["email" => $post_data['email_address'], "name" => $post_data['first_name'] . " " . $post_data['last_name']]);

                            $insert_member = array(
                                'FirstName' => $post_data['first_name'],
                                'LastName' => $post_data['last_name'],
                                'EmailAddress' => $post_data['email_address'],
                                'StripeCustomerID' => $customer->id,
                                'Phone' => $post_data['phone_number'],
                                'BuildingName' => $address,
                                'StreetName' => $post_data['street_name'],
                                'PostalCode' => $post_data['postal_code'],
                                'Town' => $post_data['town'],
                                'Password' => encrypt_decrypt('encrypt', $post_data['u_password']),
                                'Status' => 'Enabled',
                                'RegisterFrom' => 'Desktop',
                                'ReferralCode' => $this->random_str($FirstName),
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );

                            $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);
                            $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('PKMemberID' => $member_id));
                            if ($member_record !== false) {
                                /* default member preferences 9/10/2018 */
                                $MemberPreferences = $this->_GetMemberPreferences();
                                if (empty($MemberPreferences)) {
                                    $Preferences = $this->_GetPreferencesRegistration();

                                    if (isset($Preferences) && $Preferences !== false) {
                                        foreach ($Preferences as $key => $value) {
                                            $child_records = (isset($value['child_records']) && !empty($value['child_records'])) ? $value['child_records'] : array();
                                            if (isset($child_records[0]['PKPreferenceID']) && $child_records[0]['PKPreferenceID'] > 0) {
                                                $insert_member_preference = array(
                                                    'FKMemberID' => $member_id,
                                                    'FKPreferenceID' => $child_records[0]['PKPreferenceID']
                                                );
                                                $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                                            } else {
                                                continue;
                                            }
                                        }
                                    }
                                }
                                /* default member preferences 9/10/2018 */
                                $this->_CreateMemberSession($member_record['PKMemberID']);
                                $member_record = $this->GetCurrentMemberDetail();
                                $email_field_array = array(
                                    'Email Sender' => $this->GetWebsiteEmailSender(),
                                    'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                                    'Website Name' => $this->GetWebsiteName(),
                                    'Website URL' => $this->GetWebsiteURLAddress(),
                                    'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                                    'Website Email Address' => $this->GetWebsiteEmailAddress(),
                                    'Header Menus' => $this->GetEmailHeaderMenus(),
                                    'Footer Menus' => $this->GetEmailFooterMenus(),
                                    'Footer Title' => $this->GetEmailFooterTitle(),
                                    'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                    'First Name' => $member_record['FirstName'],
                                    'Last Name' => $member_record['LastName'],
                                    'Email Address' => $member_record['EmailAddress'],
                                    'Phone Number' => $member_record['Phone'],
                                    'Postal Code' => $member_record['PostalCode'],
                                    'Building Name' => $member_record['BuildingName'],
                                    'Street Name' => $member_record['StreetName'],
                                    'Town' => $member_record['Town'],
                                    'Register From' => $member_record['RegisterFrom']
                                );
                                $this->emailsender->send_email_responder(1, $email_field_array);
                                $this->emailsender->send_email_responder(6, $email_field_array);

                                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 11, 'Status' => 'Enabled'));
                                if ($sms_responder_record != false) {
                                    $sms_field_array = array(
                                        'Store Link' => $this->config->item('store_link_short_url'),
                                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                        'Email Address' => $post_data['email_address']
                                    );
                                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                    $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                                }

                                echo "success||" . base_url($this->config->item('front_check_out_page_url'));
                            }
                        } else {
                            echo "success||" . base_url($this->config->item('front_check_out_page_url'));
                        }
                    } else {
                        echo "error||postal_code||Sorry, We Don't know this Postal Code";
                        return false;
                    }
                } else if ($current_url == "apply-discount-code") {

                    $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('Code' => $post_data['code'], 'Status' => 'Active'));
                    if ($discount_record != false) {
                        if (!empty($discount_record['FKMemberID'])) {
                            if ($discount_record['FKMemberID'] != $this->GetCurrentMemberID()) {
                                echo "error||Discount code incorrect";
                                return;
                            }
                        }
                        /* Hassan changes 3-9-2018 */
                        $price_of_package = (isset($post_data['price_of_package']) ? (float) $post_data['price_of_package'] : 0);
                        $total = (isset($post_data['total']) ? (float) $post_data['total'] : 0);

                        $price_after_package = (float) ($total - $price_of_package);
                        //echo $price_of_package.'--'.$total.'=='.$price_after_package;die;
                        if (!empty($discount_record['MinimumOrderAmount']) && $discount_record['MinimumOrderAmount'] > $total) {

                            echo "error||Discount minimum order amount is " . $discount_record['MinimumOrderAmount'];
                            return;
                        }
                        /* Hassan changes 3-9-2018 */

                        /* Hassan changes 26-5-2018 */
                        $contains_package = (isset($post_data['contains_package']) ? (int) $post_data['contains_package'] : 0);
                        if (isset($contains_package) && $contains_package > 0) {

                            if (!empty($discount_record['MinimumOrderAmount']) && $discount_record['MinimumOrderAmount'] > $price_after_package) {

                                echo "error||Discount code is not valid on laundry packages";
                                return;
                            }
                        }
                        /* Hassan changes 26-5-2018 */

                        $current_date = date('Y-m-d');
                        if (!empty($discount_record['StartDate'])) {
                            $start_date = date('Y-m-d', strtotime($discount_record['StartDate']));
                            if ($current_date < $start_date) {
                                echo "error||Discount code is not valid";
                                return;
                            }
                        }
                        if (!empty($discount_record['ExpireDate'])) {
                            $expire_date = date('Y-m-d', strtotime($discount_record['ExpireDate']));
                            if ($current_date > $expire_date) {
                                $update_discount = array(
                                    'Status' => 'Expire',
                                    'ID' => $discount_record['PKDiscountID'],
                                    'UpdatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                                echo "error||Discount code is already expire";
                                return;
                            }
                        }
                        if ($discount_record['CodeUsed'] == "One Time") {
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'PKInvoiceID', array('FKDiscountID' => $discount_record['PKDiscountID'], 'FKMemberID' => $this->GetCurrentMemberID()));
                            if ($invoice_record !== false) {
                                echo "error||You already used this discount code";
                                return;
                            }
                        }

                        echo "success||Discount|" . $discount_record['PKDiscountID'] . '|' . $discount_record['Worth'] . '|' . $discount_record['DType'] . '|' . $discount_record['Code'];
                    } else {
                        echo "error||Discount code incorrect";
                    }
                } else if ($current_url == "apply-referral-code") {
                    $member_detail = $this->GetCurrentMemberDetail();
                    $member_referral_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", "ReferralCode='" . $post_data['code'] . "' and Status='Enabled' and Phone != '" . $member_detail['Phone'] . "' and BuildingName !='" . $member_detail['BuildingName'] . "' and PKMemberID !=" . $member_detail['PKMemberID']);
                    if ($member_referral_record != false) {
                        echo "success||Referral|" . $member_referral_record['PKMemberID'] . '|' . $this->GetReferralCodeAmount() . '|Price|' . $post_data['code'];
                    } else {
                        echo "error||Referral code incorrect";
                    }
                } else if ($current_url == "order-checkout") {

                    if ($this->IsMemberLogin()) {
                        $invoice_id = 0;
                        $invoice_record = false;
                        if (isset($post_data['randcheck']) && $_SESSION['rand'] && $post_data['randcheck'] == $_SESSION['rand']) {
                        } else {
                            echo "error||Dear Customer,<br/>You cannot re-submit from the cart, please contact our customer support";
                            return;
                        }

                        $grand_total = floatval($post_data['grand_total']);
                        if ($this->IsSessionItem("invoice_type") && $this->GetSessionItem("invoice_type") == "edit") {
                            if ($this->IsSessionItem("admin_invoice_id")) {
                                $invoice_id = $this->GetSessionItem("admin_invoice_id");
                            } else if ($this->IsSessionItem("member_invoice_id")) {
                                $invoice_id = $this->GetSessionItem("member_invoice_id");
                            }

                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,PaymentStatus,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('PKInvoiceID' => $invoice_id));
                            if ($invoice_record != false) {
                                if ($invoice_record["PaymentStatus"] == "Completed") {
                                    if ($invoice_record["GrandTotal"] > $grand_total) {
                                        echo "error||Dear Customer,<br/>You cannot reduce your order amount from the cart, please contact our customer support";
                                        return;
                                    }
                                }
                            }
                        }

                        $post_data["pickup_date"] = $this->GetSessionItem($this->pickup_date);
                        $post_data["pickup_time"] = $this->GetSessionItem($this->pickup_time);
                        $post_data["delivery_date"] = $this->GetSessionItem($this->delivery_date);
                        $post_data["delivery_time"] = $this->GetSessionItem($this->delivery_time);


                        $franchise_record = $this->GetSessionItem($this->session_franchise_record);
                        $website_currency = $this->config->item('currency_sign');

                        $session_cart = $post_data['session_cart'];
                        $discount_cart = $post_data['discount_cart'];

                        $preference_cart = htmlentities($post_data['preference_cart']);
                        $address_cart = $post_data['address_cart'];

                        /* Hassan changes 1-4-2018 */
                        if ($post_data['pickup_date'] && $post_data['delivery_date']) {
                            $pick_dateTMP = $post_data['pickup_date'];
                            $pick_timeTMP = $post_data['pickup_time'];
                            if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                                $pick_timeArray = explode('-', $pick_timeTMP);
                                $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
                            } else {
                                $pick_timeClean = '';
                            }
                            $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

                            $delivery_dateTMP = $post_data['delivery_date'];
                            $delivery_timeTMP = $post_data['delivery_time'];

                            if (isset($delivery_timeTMP) && $delivery_timeTMP != '') {
                                $delivery_timeArray = explode('-', $delivery_timeTMP);
                                $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
                            } else {
                                $delivery_timeClean = '';
                            }

                            $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

                            $diffrenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);

                            if ($diffrenceTMP < $franchise_record["DeliveryDifferenceHour"]) {
                                echo "error||Dear Customer,<br/>Order delivery difference should be at least " . $franchise_record["DeliveryDifferenceHour"] . " hours , please contact our customer support";
                                return;
                            }
                        }


                        $discount_cart = $post_data['discount_cart'];

                        if ($discount_cart != "null") {
                            $discount_cart_array = explode("|", $discount_cart);
                            if (isset($discount_cart_array[0]) && $discount_cart_array[0] == "Discount" && isset($discount_cart_array[1]) && ($discount_cart_array[1] != "null" || $discount_cart_array[1] != "")) {
                                $invoice_data['DiscountType'] = "Discount";
                                $invoice_data['FKDiscountID'] = $discount_cart_array[1];
                            } elseif (isset($discount_cart_array[1]) && ($discount_cart_array[1] != "null" || $discount_cart_array[1] != "")) {
                                $invoice_data['DiscountType'] = "Referral";
                                $invoice_data['ReferralID'] = $discount_cart_array[1];
                            } else {
                                echo "error||Dear Customer,<br/>Discount code is undefined, please contact our customer support";
                                return;
                            }
                        }

                        /* Hassan chanes ends */

                        /* Hassan changes 23-01-2018 */
                        $invoice_record_old = isset($invoice_record) ? $invoice_record : array();
                        /* ends */
                        //$this->load->helper('language');
                        //$this->lang->load('merchant', 'english');
                        //$this->load->library('merchant');
                        //$this->merchant->load('stripe');


                        if (isset($post_data['regularly_cart']) && $post_data['regularly_cart'] != '') {

                            $regularly_cart = $post_data['regularly_cart'];
                            $regularly_cart_array = explode("|||", $regularly_cart);
                            $regularly_array = explode("=", $regularly_cart_array[0]);
                        }
                        if (isset($post_data['chk_news']) && $post_data['chk_news'] != '') {

                            $chk_news = $post_data['chk_news'];
                        }



                        $credit_card_cart = $post_data['credit_card_cart'];
                        $credit_card_token_cart = $post_data['credit_card_token_cart'];
                        $pick_date = $post_data["pickup_date"];
                        $pick_time = $post_data["pickup_time"];
                        $delivery_date = $post_data["delivery_date"];
                        $delivery_time = $post_data['delivery_time'];
                        $sub_total = floatval($post_data['sub_total']);
                        $discount_total = floatval($post_data['discount_total']);
                        $preference_total = floatval($post_data['preference_total']);
                        $order_notes = $post_data['order_notes'];
                        $member_id = $this->GetCurrentMemberID();
                        $member_detail = $this->GetCurrentMemberDetail();
                        $credit_card_id = 0;
                        $invoice_type = "After";
                        $invoice_number = "";
                        $stripe_grand_total = 0;
                        $result = "";

                        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                        $stripe_customer_id = $member_record['StripeCustomerID'];
                        if ($invoice_record != false) {
                            $invoice_payment_records = $this->model_database->GetRecords($this->tbl_invoice_payment_informations, "R", "Amount", array('FKInvoiceID' => $invoice_record["PKInvoiceID"]));
                            if (sizeof($invoice_payment_records) > 0) {
                                $inv_total = 0;
                                foreach ($invoice_payment_records as $inv_payment_record) {
                                    $inv_total += $inv_payment_record["Amount"];
                                }
                                $stripe_grand_total = $grand_total - floatval($inv_total);
                            } else {
                                $stripe_grand_total = $grand_total;
                            }
                            $invoice_number = $invoice_record['InvoiceNumber'];
                        } else {
                            $stripe_grand_total = $grand_total;
                            $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");
                        }
                        $stripe_grand_total = number_format((float) $stripe_grand_total, 2, '.', '');
                        $stripe = new Stripe();
                        if ($stripe_grand_total > 0) {
                            $amount = (int) ($stripe_grand_total * 100);


                            $data = [
                                'payment_method' => $post_data['payment_method_id'],
                                'amount' => $amount,
                                'customer' => $stripe_customer_id,
                                'payment_method_types' => ['card'],
                                'currency' => $this->config->item("currency_code"),
                                'confirmation_method' => 'manual',
                                'confirm' => true,
                                'setup_future_usage' => 'off_session',
                                'description' => "Invoice #" . $invoice_number
                            ];
                            $intent = $stripe->purchase($stripeKey, $data);

                            if (isset($intent["error"])) {
                                echo "error||" . $intent["message"];
                                return;
                            }
                        }

                        if ($session_cart != "null") {
                            $invoice_type = "Items";
                        }



                        $cards_record = $this->model_database->GetRecord($this->tbl_member_cards, false, array('Code' => $post_data['payment_method_id']));


                        if ($cards_record == false) {
                            $payment_method = $stripe->getPaymentMethod($stripeKey, $post_data['payment_method_id']);

                            $member_card = array(
                                'FKMemberID' => $member_id,
                                'Title' => "************" . $payment_method->card->last4,
                                'Name' => "************" . $payment_method->card->last4,
                                'Number' => $payment_method->card->last4,
                                'Month' => $payment_method->card->exp_month,
                                'Year' => $payment_method->card->exp_year,
                                'Code' => $post_data['payment_method_id'],
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );

                            $payment_method->attach(['customer' => $stripe_customer_id]);
                            $credit_card_id = $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
                        } else {
                            $credit_card_id = $cards_record['PKCardID'];
                        }

                        $invoice_data = array(
                            'FKMemberID' => $member_id,
                            'FKCardID' => $credit_card_id,
                            'FKFranchiseID' => $franchise_record['PKFranchiseID'],
                            'InvoiceType' => $invoice_type,
                            'Currency' => $website_currency,
                            'InvoiceNumber' => $invoice_number,
                            'PaymentMethod' => 'Stripe',
                            'PaymentReference' => $post_data['payment_method_id'],
                            'OrderNotes' => $order_notes,
                            'SubTotal' => $sub_total,
                            'GrandTotal' => $grand_total,
                            'IPAddress' => $this->input->ip_address(),
                            'PickupDate' => $pick_date,
                            'PickupTime' => $pick_time,
                            'DeliveryDate' => $delivery_date,
                            'DeliveryTime' => $delivery_time,
                            'Regularly' => (isset($regularly_array[1])) ? trim((int) $regularly_array[1]) : 0
                        );


                        if ($invoice_record == false) {
                            $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                            $invoice_data['PaymentStatus'] = "Pending";
                            $invoice_data['OrderStatus'] = "Pending";
                        } else {
                            $invoice_data['UpdatedDateTime'] = date('Y-m-d H:i:s');
                        }
                        if ($address_cart != "null") {
                            $address_cart_array = explode("|||", $address_cart);
                            $location_array = explode("=", $address_cart_array[0]);
                            $invoice_data['Location'] = $location_array[1];
                            $post_code_array = explode("=", $address_cart_array[1]);
                            $invoice_data['PostalCode'] = $post_code_array[1];
                            $third_array = explode("=", $address_cart_array[2]);
                            if ($third_array[0] == "Locker") {
                                $invoice_data['Locker'] = $third_array[1];
                            } else {
                                $invoice_data['BuildingName'] = $third_array[1];
                                $street_array = explode("=", $address_cart_array[3]);
                                $invoice_data['StreetName'] = $street_array[1];
                                $town_array = explode("=", $address_cart_array[4]);
                                $invoice_data['Town'] = $town_array[1];
                            }
                        }
                        $invoice_data['PreferenceTotal'] = $preference_total;
                        $invoice_data['DiscountTotal'] = $discount_total;
                        if ($discount_cart != "null") {
                            $discount_cart_array = explode("|", $discount_cart);
                            if ($discount_cart_array[0] == "Discount") {
                                $invoice_data['DiscountType'] = "Discount";
                                $invoice_data['FKDiscountID'] = $discount_cart_array[1];
                            } else {
                                $invoice_data['DiscountType'] = "Referral";
                                $invoice_data['ReferralID'] = $discount_cart_array[1];
                            }
                            $invoice_data['DiscountWorth'] = $discount_cart_array[2];
                            $invoice_data['DType'] = $discount_cart_array[3];
                            $invoice_data['DiscountCode'] = $discount_cart_array[4];
                        } else {
                            $invoice_data['DiscountType'] = "None";
                        }
                        if ($member_id == 1) {
                            $invoice_data['IsTestOrder'] = "Yes";
                        }

                        /* update letter */
                        if (isset($member_id)) {
                            $update_member = array(
                                'ID' => $member_id,
                                'NewsLetter' => (isset($chk_news)) ? trim((int) $chk_news) : 0
                            );
                            $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                        }


                        if ($invoice_record != false) {
                            $invoice_data['ID'] = $invoice_id;
                            $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                            $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $invoice_id, "FKInvoiceID");
                            $this->model_database->RemoveRecord($this->tbl_invoice_services, $invoice_id, "FKInvoiceID");
                            //$this->AddSessionItem("OrderAction", "update");
                            $action = "update";
                        } else {

                            $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                            //$this->AddSessionItem("OrderAction", "insert");
                            $action = "insert";
                        }



                        unset($_SESSION['rand']);
                        $this->AddSessionItem("OrderAction", $action);
                        $this->AddSessionItem("invoice_id", $invoice_id);
                        $this->AddSessionItem("invoice_number", $invoice_number);



                        $tookan_invoice_service = array();
                        if ($invoice_type == "Items") {
                            $package_count = 0;
                            $add_preferences = false;
                            if ($session_cart != "null") {
                                $session_cart_array = json_decode($session_cart, true);
                                foreach ($session_cart_array as $cart_data) {
                                    $values = json_decode($cart_data, true);
                                    $total = floatval(intval($values['qty']) * floatval($values['price']));
                                    $insert_invoice_service = array(
                                        'FKInvoiceID' => $invoice_id,
                                        'FKServiceID' => $values['id'],
                                        'FKCategoryID' => $values['category_id'],
                                        'Title' => urldecode($values['title']),
                                        'DesktopImageName' => urldecode($values['desktop_img']),
                                        'MobileImageName' => urldecode($values['mobile_img']),
                                        'IsPackage' => $values['package'],
                                        'PreferencesShow' => $values['preference_show'],
                                        'Quantity' => $values['qty'],
                                        'Price' => $values['price'],
                                        'Total' => $total
                                    );
                                    $tookan_invoice_service[] = $insert_invoice_service;
                                    $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                    if ($values['package'] == "Yes") {
                                        $package_count += intval($values['qty']);
                                    }
                                    if ($values['preference_show'] == "Yes") {
                                        $add_preferences = true;
                                    }
                                }
                            }
                            if ($add_preferences == true) {
                                if ($preference_cart != "null") {
                                    $account_notes = "";
                                    $additional_instructions = "";
                                    $preference_cart_array = explode("[]", $preference_cart);
                                    $preference_position = 0;
                                    for ($i = 0; $i < sizeof($preference_cart_array); $i++) {
                                        $current_preference_array = explode("||", urldecode($preference_cart_array[$i]));
                                        if (sizeof($current_preference_array) > 1) {
                                            $insert_invoice_preference = array(
                                                'FKInvoiceID' => $invoice_id,
                                                'FKPreferenceID' => $current_preference_array[0],
                                                'ParentTitle' => $current_preference_array[1],
                                                'Title' => $current_preference_array[2]
                                            );
                                            if (isset($current_preference_array[3]) && !empty($current_preference_array[3])) {
                                                $preference_price = $current_preference_array[3];
                                                $preference_total_price = $preference_price;
                                                if ($current_preference_array[4] == "Yes") {
                                                    if ($package_count > 0) {
                                                        $preference_total_price = ($preference_total_price * $package_count);
                                                    }
                                                }
                                                $insert_invoice_preference["Price"] = $preference_price;
                                                $insert_invoice_preference["Total"] = $preference_total_price;
                                            }
                                            $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                        } else {
                                            if ($preference_position == 0) {
                                                //$account_notes = $current_preference_array[0];
                                                $preference_position = 1;
                                            } else {
                                                $additional_instructions = $current_preference_array[0];
                                            }
                                        }
                                    }
                                    $update_invoice = array(
                                        'ID' => $invoice_id,
                                        'AdditionalInstructions' => $additional_instructions
                                    );
                                    $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                                }
                            }
                        }

                        $is_new_invoice_create = true;
                        if ($stripe_grand_total > 0) {


                            $stripe_information = array(
                                'InvoiceNumber' => $invoice_number,
                                'Content' => json_encode($intent),
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->InsertRecord($this->tbl_payment_informations, $stripe_information);

                            if (isset($intent->id)) {
                                // mark order as complete
                                $invoice_update = array(
                                    'OrderStatus' => "Processed",
                                    'PaymentStatus' => "Completed",
                                    'PaymentToken' => $intent->id,
                                    'PaymentMethod' => 'Stripe',
                                    'ID' => $invoice_id
                                );
                                $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_update, 'PKInvoiceID');
                                $insert_invoice_payment_information = array(
                                    'FKInvoiceID' => $invoice_id,
                                    'FKCardID' => $credit_card_id,
                                    'Amount' => $stripe_grand_total,
                                    'PaymentReference' => $post_data['payment_method_id'],
                                    'PaymentToken' => $intent->id,
                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                                if ($discount_cart == "null") {
                                    $loyalty_points = intval($grand_total);
                                    $minus_loyalty_points = 0;
                                    $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_id));
                                    $loyalty_data = array(
                                        'FKMemberID' => $member_id,
                                        'FKInvoiceID' => $invoice_id,
                                        'InvoiceNumber' => $invoice_number,
                                        'GrandTotal' => $grand_total,
                                        'Points' => $loyalty_points
                                    );
                                    if ($invoice_loyalty_record != false) {
                                        $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                        $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                        $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                        $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                    } else {
                                        $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                        $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                    }
                                    $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                    $total_loyalty_points = intval($member_detail['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                    $used_loyalty_points = intval($member_detail['UsedLoyaltyPoints']);
                                    $total_loyalty_points += $loyalty_points;
                                    $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                    if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                        $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                        for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                            $loyalty_code = $this->RandomPassword(6);
                                            $insert_discount = array(
                                                'FKMemberID' => $member_detail['PKMemberID'],
                                                'Code' => $loyalty_code,
                                                'Worth' => $this->GetLoyaltyCodeAmount(),
                                                'DType' => "Price",
                                                'DiscountFor' => "Loyalty",
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                            $used_loyalty_points += $minimum_loyalty_points;
                                            $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                            if ($sms_responder_record != false) {
                                                $sms_field_array = array(
                                                    'Loyalty Points' => $remain_loyalty_points,
                                                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                    'Currency' => $this->config->item("currency_symbol"),
                                                    'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                    'Discount Code' => $loyalty_code
                                                );
                                                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                                $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                                            }
                                        }
                                    }
                                    $update_member = array(
                                        'TotalLoyaltyPoints' => $total_loyalty_points,
                                        'UsedLoyaltyPoints' => $used_loyalty_points,
                                        'ID' => $member_detail['PKMemberID']
                                    );
                                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_detail['PKMemberID']));
                                    if ($member_record !== false) {
                                        $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                                    }
                                } else {
                                    $is_referral_discount_create = true;
                                    if ($invoice_record != false) {
                                        if ($invoice_record["PaymentStatus"] != "Pending") {
                                            $is_referral_discount_create = false;
                                        }
                                    }
                                    if ($is_referral_discount_create) {
                                        $discount_cart_array = explode("|", $discount_cart);
                                        if ($discount_cart_array[0] != "Discount") {
                                            $insert_discount = array(
                                                'FKMemberID' => $discount_cart_array[1],
                                                'Code' => $this->RandomPassword(6),
                                                'Worth' => $discount_cart_array[2],
                                                'DType' => $discount_cart_array[3],
                                                'DiscountFor' => "Referral",
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                        }
                                    }
                                }
                                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,InvoiceType,GrandTotal", array('InvoiceNumber' => $invoice_number));
                                if ($invoice_record != false) {
                                    if ($invoice_record['InvoiceType'] == "Items") {
                                        $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Price,Quantity", array('FKInvoiceID' => $invoice_record['PKInvoiceID']));
                                    }

                                    $this->AddSessionItem("Google_ECommerce_Invoice_Record", $invoice_record);
                                }

                                $this->AddSessionItem("Invoice_Success", true);
                                $this->AddSessionItem("Invoice_FB_Total", $stripe_grand_total);
                                $result = "success||" . base_url('thank-you');
                            } else {
                                $is_new_invoice_create = false;
                                $this->AddSessionItem("invoice_type", "edit");
                                $this->AddSessionItem("member_invoice_id", $invoice_id);
                                $result = "error||" . $response->message();
                            }
                        } else {
                            $this->AddSessionItem("Invoice_Success", true);
                            $result = "success||" . base_url('thank-you');
                        }
                        $response = $this->saveToTookaan($invoice_record["PKInvoiceID"], $member_record, $action);
                        echo $result;
                    } else {
                        echo "error||t||" . redirect(base_url('login'));
                    }
                }
            } else {
                echo "error||t||" . redirect(base_url());
            }
        } else if ($current_url == "cron-job") {
            //error_reporting(1);
            if (isset($_REQUEST['id']) && $_REQUEST['id'] == "l2l") {

                $cron_record = $this->model_database->GetRecord($this->tbl_cms_cron, "PKCronID,Status", array('PKCronID' => 1, 'Status' => 0));

                if ($cron_record != false) {
                    $update_cron = array(
                        'Status' => 1,
                        'UpdatedDateTime' => date('Y-m-d H:i:s'),
                        'ID' => 1
                    );
                    $this->model_database->UpdateRecord($this->tbl_cms_cron, $update_cron, "PKCronID");
                } else {
                    echo 'Cron is already running ' . date('Y-m-d H:i:s');
                    die;
                }

                $current_date = date('Y-m-d');
                $search_time = date('H:00', strtotime('+1 hours')) . '-' . date('H:00', strtotime('+2 hours'));
                $pick_html = "Pick Up :";
                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 2, 'Status' => 'Enabled'));
                if ($sms_responder_record != false) {
                    $pickup_invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,FKMemberID,InvoiceNumber,PickupTime", "PaymentStatus='Completed' and OrderStatus='Processed' and PickupDate='" . $current_date . "' and PickupTime='" . $search_time . "' and PickupNotification='No'", false, false, false, "PKInvoiceID");

                    if (sizeof($pickup_invoice_records) > 0) {
                        $InvoiceArray = array();
                        foreach ($pickup_invoice_records as $key => $value) {
                            if (in_array($value['InvoiceNumber'], $InvoiceArray)) {
                                continue;
                            }
                            $member_record = $this->model_database->GetRecord($this->tbl_members, "Phone", array('PKMemberID' => $value['FKMemberID']));
                            if ($member_record != false) {
                                $sms_field_array = array(
                                    'Pick Time' => $value['PickupTime'],
                                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName())
                                );
                                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                // $this->SendMessage($sms_responder_record['PKResponderID'], $value['InvoiceNumber'], $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                                $update_invoice = array(
                                    'ID' => $value['PKInvoiceID'],
                                    'PickupNotification' => 'Yes'
                                );
                                echo $pick_html .= 'ID : ' . $value['PKInvoiceID'] . '|Number : ' . $value['InvoiceNumber'] . '<br/>';
                                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                            }
                            $InvoiceArray[] = $value['InvoiceNumber'];
                        }
                    }
                }
                $delivery_html = "Delivery :";
                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 3, 'Status' => 'Enabled'));
                if ($sms_responder_record != false) {
                    $delivery_invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,FKMemberID,InvoiceNumber,DeliveryTime", "PaymentStatus='Completed' and OrderStatus='Processed' and DeliveryDate='" . $current_date . "' and DeliveryTime='" . $search_time . "' and DeliveryNotification='No'", false, false, false, "PKInvoiceID");

                    if (sizeof($delivery_invoice_records) > 0) {
                        $InvoiceArray = array();
                        foreach ($delivery_invoice_records as $key => $value) {
                            if (in_array($value['InvoiceNumber'], $InvoiceArray)) {
                                continue;
                            }
                            $member_record = $this->model_database->GetRecord($this->tbl_members, "Phone", array('PKMemberID' => $value['FKMemberID']));
                            if ($member_record != false) {
                                $sms_field_array = array(
                                    'Invoice Number' => $value['InvoiceNumber'],
                                    'Delivery Time' => $value['DeliveryTime'],
                                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName())
                                );
                                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                // $this->SendMessage($sms_responder_record['PKResponderID'], $value['InvoiceNumber'], $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                                $update_invoice = array(
                                    'ID' => $value['PKInvoiceID'],
                                    'DeliveryNotification' => 'Yes'
                                );
                                echo $delivery_html .= 'ID : ' . $value['PKInvoiceID'] . '|Number : ' . $value['InvoiceNumber'] . '<br/>';
                                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                            }
                            $InvoiceArray[] = $value['InvoiceNumber'];
                        }
                    }
                }
                $update_setting = array(
                    'Content' => $pick_html . '|||' . $delivery_html,
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 32
                );
                $this->model_database->UpdateRecord($this->tbl_settings, $update_setting, "PKSettingID");
                $update_cron = array(
                    'Status' => 0,
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 1
                );
                $this->model_database->UpdateRecord($this->tbl_cms_cron, $update_cron, "PKCronID");
            } else {
                $this->_page_not_found();
            }
        } elseif ($current_url == "cron-regularly") {
            //echo $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices,"InvoiceNumber");die;
            //echo 'npe';
            $this->regularly_cron();
        } elseif ($current_url == "cron-regularlytest") {

            echo 'Pending';
            //4000008260000000
            //$this->regularly_cron();  
        } elseif ($current_url == "cron-referral") {
            // this is not a real cron this script is written for referral code by hassan
            //echo encrypt_decrypt('decrypt', 'nBPdWZoKri4crhjSrIOashXdGRtfS0LzEk3N/i7Kpj0=');die;
            // script for updating referral-code temporary script
            $members_record = $this->model_database->GetRecords($this->tbl_members, "R", false, array('Status' => 'Enabled', 'ReferralCode' => NULL), 2, 'PKMemberID');
            echo count($members_record); /* echo '<pre>';print_r($members_record);echo '</pre>'; */
            die;
            if (isset($members_record) && !empty($members_record)) {
                $total = 0;
                foreach ($members_record as $key => $val) {

                    if (isset($val['FirstName']) && $val['FirstName'] != '') {
                        $FirstName = $val['FirstName'];
                        $update_member = array(
                            'ID' => $val['PKMemberID'],
                            'ReferralCode' => $this->random_str($FirstName)
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                        $total++;
                    }
                }
                echo 'Total ' . $total;
            }
        } else {
            if ($this->input->is_ajax_request() == true) {
                if ($current_url == $this->config->item('front_member_order_listing_page_url')) {
                    if ($this->IsMemberLogin() == false && $this->IsMemberLoginFromAdmin() == false) {
                        echo base_url($this->config->item('front_login_page_url'));
                    }
                } else if ($current_url == $this->config->item('front_select_item_page_url') || $current_url == $this->config->item('front_address_selection_page_url')) {
                } else {
                    echo base_url($this->config->item('front_login_page_url'));
                }
            }

            if ($current_url == $this->config->item('front_logout_page_url')) {
                $this->_log_out();
                redirect(base_url($this->config->item('front_login_page_url')));
            } else if ($current_url == $this->config->item('front_cap_image_page_url')) {
                echo $this->CaptchaImage($current_second_url);
            } else if ($current_url == 'admin') {
                if ($this->IsAdminLogin() === true) {
                    redirect(base_url('admin/error/pagenotfound'));
                } else {
                    $this->_page_not_found();
                }
            } else {
                if ($current_url == $this->config->item('front_login_page_url') || $current_url == $this->config->item('front_register_page_url')) {
                    if ($this->IsMemberLogin() == true && $this->IsMemberLoginFromAdmin() == false) {
                        redirect(base_url($this->config->item('front_dashboard_page_url')));
                    }
                } else if ($current_url == $this->config->item('front_dashboard_page_url') || $current_url == $this->config->item('front_member_account_page_url') || $current_url == $this->config->item('front_member_payment_page_url') || $current_url == $this->config->item('front_member_laundry_page_url') || $current_url == $this->config->item('front_member_discount_page_url') || $current_url == $this->config->item('front_member_loyalty_page_url') || $current_url == $this->config->item('front_member_order_listing_page_url') || $current_url == $this->config->item('front_check_out_page_url') || $current_url == $this->config->item('front_payment_page_url')) {

                    $member_record = $this->GetCurrentMemberDetail();
                    //d($member_record);
                    //die($this->IsMemberLogin());

                    if ($this->IsMemberLogin() == false && ($this->IsMemberLoginFromAdminSession() == null || $this->IsMemberLoginFromAdmin() == true)) {
                        redirect(base_url($this->config->item('front_login_page_url')));
                    }
                }
                $member_id = $this->GetCurrentMemberID();
                $member_detail = $this->GetCurrentMemberDetail();
                $current_page_url = $current_url;
                if ($current_second_url) {
                    $current_page_url .= '/' . $current_second_url;
                    if ($current_third_url) {
                        $current_page_url .= '/' . $current_third_url;
                    }
                }
                $data['page_data'] = $this->GetPageRecord("AccessURL", $current_page_url);
                if ($current_url == $this->config->item('front_login_page_url')) {
                    $this->_URLChecker();
                    $data['is_order_page'] = false;
                    $this->show_view_with_menu('user/login', $data);
                } else if ($current_url == $this->config->item('front_register_page_url')) {
                    $this->_URLChecker();
                    $data['is_order_page'] = false;

                    $data['social_id'] = "";
                    $data['type'] = "";
                    $data['reg_first_name'] = "";
                    $data['reg_last_name'] = "";
                    $data['reg_email'] = "";

                    if ($this->GetSessionItem("social_id")) {
                        $data['social_id'] = $this->GetSessionItem("social_id");
                        $data['type'] = $this->GetSessionItem("type");
                        $data['reg_first_name'] = $this->GetSessionItem("reg_first_name");
                        $data['reg_last_name'] = $this->GetSessionItem("reg_last_name");
                        $data['reg_email'] = $this->GetSessionItem("reg_email");
                    }

                    $this->show_view_with_menu('user/register', $data);
                } else if ($current_url == $this->config->item('front_contact_page_url')) {
                    $this->_URLChecker();
                    $this->show_view_with_menu('pages/contact', $data);
                } else if ($current_url == $this->config->item('front_post_code_not_found_page_url')) {
                    $this->_URLChecker();
                    $this->show_view_with_menu('pages/post_code_not_found', $data);
                } else if ($current_url == "partner") {
                    $this->_URLChecker();
                    $this->show_view_with_menu('pages/partner', $data);
                } else if ($current_url == "refer-a-friend") {
                    $this->_URLChecker();
                    $this->show_view_with_menu('pages/refer', $data);
                } else if ($current_url == $this->config->item('front_thank_you_page_url')) {
                    //die($current_url);
                    if ($this->IsSessionItem("Invoice_Success")) {
                        $this->_URLChecker();
                        $data['is_invoice_page'] = true;
                        if ($data['page_data'] != false) {
                            $data['page_data']["Content"] = str_replace("[Telephone_Detail_Area]", "<a href='tel:" . $this->GetWebsiteTelephoneNo() . "'>" . $this->GetWebsiteTelephoneNo() . "</a>", $data['page_data']["Content"]);
                            $data['page_data']["Content"] = str_replace("[Contact_Detail_Area]", "<a href='" . base_url($this->config->item('front_contact_page_url')) . "'>Contact Us</a>", $data['page_data']["Content"]);
                        }
                        $data["referral_code_amount"] = $this->GetReferralCodeAmount();
                        if ($this->IsSessionItem("Invoice_FB_Total")) {
                            $data['invoice_fb_total'] = $this->GetSessionItem("Invoice_FB_Total");
                        }
                        if ($this->IsSessionItem("Google_ECommerce_Invoice_Record")) {
                            $data['google_e_commerce_invoice_record'] = $this->GetSessionItem("Google_ECommerce_Invoice_Record");
                        }


                        $data['current_url'] = $current_url;
                        $this->RemoveSessionItems("Invoice_Success");
                        $this->RemoveSessionItems("Invoice_FB_Total");
                        $this->RemoveSessionItems("Google_ECommerce_Invoice_Record");
                        $this->show_view_with_menu('pages/thank_you', $data);
                    } else {
                        redirect(base_url());
                    }
                } elseif ($current_url == "notify-new-order") {

                    $invoice_id = $this->GetSessionItem("invoice_id");
                    $invoice_number = $this->GetSessionItem("invoice_number");
                    $action = $this->GetSessionItem("OrderAction");

                    if ($this->IsSessionItem("invoice_id")) {

                        $member_id = $this->GetCurrentMemberID();
                        $member_detail = $this->GetCurrentMemberDetail();

                        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));

                        if ($action == "insert") {

                            $record = $this->SendInvoiceEmail($invoice_number, "C");

                            $invoice_record = $record['invoice'];
                            $pick_date = $invoice_record["PickupDate"];
                            $pick_time = $invoice_record["PickupTime"];
                            $PaymentStatus = $invoice_record['PaymentStatus'];

                            if ($PaymentStatus == 'Completed' || $PaymentStatus == 'completed') {
                                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));

                                $sms_field_array = array(
                                    'Invoice Number' => $invoice_number,
                                    'Loyalty Points' => $member_detail['TotalLoyaltyPoints'] - $member_detail['UsedLoyaltyPoints'],
                                    'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                    'Currency' => $this->config->item("currency_symbol"),
                                    'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                    'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                    'Pick Date' => date('d M Y', strtotime($pick_date)),
                                    'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                                );
                                $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                            }
                        } else {
                            $record = $this->SendInvoiceEmail($invoice_number, "U");
                        }


                        $this->RemoveSessionItems("OrderAction");
                        $this->RemoveSessionItems("invoice_number");
                        $this->RemoveSessionItems("invoice_id");

                        die("done");
                    } else {
                        die("already done");
                    }
                } else if ($current_url == $this->config->item('front_contact_thank_you_page_url') || $current_url == "business-service-thank-you") {
                    if ($this->IsSessionItem("Contact_Success") || $this->IsSessionItem("Business_Service_Success")) {


                        $this->_URLChecker();
                        $this->RemoveSessionItems("Contact_Success");
                        $this->RemoveSessionItems("Business_Service_Success");
                        $data["referral_code_amount"] = $this->GetReferralCodeAmount();
                        $this->show_view_with_menu('pages/thank_you', $data);
                    } else {
                        redirect(base_url());
                    }
                } else if ($current_url == $this->config->item('front_store_link_generator_page_url')) {
                    $this->show_view_with_menu('pages/store_link', $data);
                } else if ($current_url == $this->config->item('front_airbnb_laundry_service_page_url')) {
                    $this->_URLChecker();
                    $data['is_airbnb_page'] = true;
                    $data['is_work_widget_show'] = true;
                    $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_price_widget_id);
                    if ($widget_record != false) {
                        $data['widget_price_record'] = $widget_record;
                    }
                    $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_price_widget_section_id);
                    if ($widget_section_record != false) {
                        $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
                        if (sizeof($widget_navigation_records) > 0) {
                            $data['widget_price_section_navigation_records'] = $widget_navigation_records;
                        }
                    }
                    $this->show_view_with_menu('pages/airbnb_laundry_service', $data);
                } else if ($current_url == $this->config->item('front_dashboard_page_url')) {
                    $this->_URLChecker();
                    if ($member_detail['PopShow'] == "Yes") {
                        $update_member = array(
                            'PopShow' => 'No',
                            'ID' => $member_id
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                        if ($member_record !== false) {
                            $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                        }
                        $data['new_register_pop_show'] = true;
                    }
                    if ($this->IsSessionItem("Register_New_Member")) {
                        $data['is_new_member'] = true;
                        $this->RemoveSessionItems("Register_New_Member");
                    }
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_dashboard_heading_text');
                    $data['invoice_pending_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Pending', 'FKMemberID' => $member_id));
                    if ($data['invoice_pending_count'] > 99) {
                        $data['invoice_pending_count'] = '99+';
                    }
                    $data['invoice_processed_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Processed', 'FKMemberID' => $member_id));
                    if ($data['invoice_processed_count'] > 99) {
                        $data['invoice_processed_count'] = '99+';
                    }
                    $data['invoice_cancel_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Cancel', 'FKMemberID' => $member_id));
                    if ($data['invoice_cancel_count'] > 99) {
                        $data['invoice_cancel_count'] = '99+';
                    }
                    $data['invoice_completed_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Completed', 'FKMemberID' => $member_id));
                    if ($data['invoice_completed_count'] > 99) {
                        $data['invoice_completed_count'] = '99+';
                    }
                    $this->show_view_with_menu('member/dashboard', $data);
                } else if ($current_url == $this->config->item('front_payment_page_url')) {
                    if ($current_second_url) {
                    } else {
                        redirect(base_url($this->config->item('front_dashboard_page_url')));
                    }
                } else if ($current_url == $this->config->item('front_member_account_page_url')) {
                    $this->_URLChecker();
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_account_heading_text');
                    $this->show_view_with_menu('member/account_setting', $data);
                } else if ($current_url == $this->config->item('front_member_payment_page_url')) {
                    $this->_URLChecker();
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_payment_heading_text');
                    $data['card_records'] = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID,Title,Number", array('FKMemberID' => $member_id), false, false, false, "PKCardID");
                    if (sizeof($data['card_records']) == 0) {
                        unset($data['card_records']);
                    }
                    $this->show_view_with_menu('member/payment_card', $data);
                } else if ($current_url == $this->config->item('front_member_laundry_page_url')) {
                    $this->_URLChecker();
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_laundry_heading_text');
                    $data['preference_records'] = $this->_GetPreferences();
                    if (sizeof($data['preference_records']) == 0) {
                        unset($data['preference_records']);
                    } else {
                        $data['member_preference_records'] = $this->_GetMemberPreferences();
                    }
                    $this->show_view_with_menu('member/wash_preferences', $data);
                } else if ($current_url == $this->config->item('front_member_discount_page_url')) {
                    $this->_URLChecker();
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_discount_heading_text');
                    $data['new_discount_records'] = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID,Code,Worth,DType,StartDate,ExpireDate,CodeUsed,DiscountFor,Status", "FKMemberID=" . $member_id . " and Status='Active' and (DiscountFor='Discount' or DiscountFor='Referral')");
                    if (sizeof($data['new_discount_records']) > 0) {
                        foreach ($data['new_discount_records'] as $key => $value) {
                            $is_valid = true;
                            if (!empty($value['StartDate'])) {
                                if (strtotime($value['StartDate']) < strtotime(date('Y-m-d'))) {
                                    $is_valid = false;
                                }
                            }
                            if (!empty($value['ExpireDate'])) {
                                if (strtotime($value['ExpireDate']) < strtotime(date('Y-m-d'))) {
                                    $update_discount = array(
                                        'ID' => $value['PKDiscountID'],
                                        'Status' => 'Expire',
                                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                                    );
                                    $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                                    $is_valid = false;
                                }
                            }
                            if ($is_valid == true) {
                                if ($value['DType'] == "Percentage") {
                                    $data['new_discount_records'][$key]['WorthValue'] = $value['Worth'] . '%';
                                } else {
                                    $data['new_discount_records'][$key]['WorthValue'] = $this->config->item('currency_sign') . ' ' . $value['Worth'];
                                }
                                if (empty($value['ExpireDate'])) {
                                    $data['new_discount_records'][$key]['Expiry'] = "Unlimited";
                                } else {
                                    $data['new_discount_records'][$key]['Expiry'] = date('d-M-Y', strtotime($value['ExpireDate']));
                                }
                            } else {
                                unset($data['new_discount_records'][$key]);
                            }
                        }
                    }
                    if (sizeof($data['new_discount_records']) == 0) {
                        unset($data['new_discount_records']);
                    }
                    $data['discount_records'] = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,InvoiceNumber,FKDiscountID,GrandTotal,Currency", "FKMemberID=" . $member_id . " and FKDiscountID != ''");
                    if (sizeof($data['discount_records']) > 0) {
                        foreach ($data['discount_records'] as $key => $value) {
                            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, "Code", array('PKDiscountID' => $value['FKDiscountID']));
                            if ($discount_record != false) {
                                $data['discount_records'][$key]['Code'] = $discount_record['Code'];
                            } else {
                                unset($data['discount_records'][$key]);
                            }
                        }
                    }
                    if (sizeof($data['discount_records']) == 0) {
                        unset($data['discount_records']);
                    }
                    $this->show_view_with_menu('member/discount_history', $data);
                } else if ($current_url == $this->config->item('front_member_loyalty_page_url')) {
                    $this->_URLChecker();
                    $data['only_copy_right_section'] = true;
                    $data['menu_name'] = $this->config->item('front_member_loyalty_heading_text');
                    $data['discount_records'] = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID,Code,Worth,DType,StartDate,ExpireDate,CodeUsed,DiscountFor,Status", "FKMemberID=" . $member_id . " and DiscountFor='Loyalty'");
                    if (sizeof($data['discount_records']) > 0) {
                        foreach ($data['discount_records'] as $key => $value) {
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('FKDiscountID' => $value['PKDiscountID']));
                            if ($invoice_record != false) {
                                $data['discount_records'][$key]['Status'] = "Used";
                            }
                        }
                    }
                    if (sizeof($data['discount_records']) == 0) {
                        unset($data['discount_records']);
                    }
                    $data['loyalty_records'] = $this->model_database->GetRecords($this->tbl_loyalty_points, "R", "PKLoyaltyID,FKInvoiceID,InvoiceNumber,GrandTotal,Points,CreatedDateTime", array('FKMemberID' => $member_id));
                    if (sizeof($data['loyalty_records']) == 0) {
                        unset($data['loyalty_records']);
                    }
                    $data['loyalty_code_amount'] = $this->GetLoyaltyCodeAmount();
                    $this->show_view_with_menu('member/loyalty_history', $data);
                } else if ($current_url == $this->config->item('front_member_order_listing_page_url')) {
                    if (isset($_REQUEST['q']) && (strtolower($_REQUEST['q']) == "pending" || strtolower($_REQUEST['q']) == "processed" || strtolower($_REQUEST['q']) == "cancel" || strtolower($_REQUEST['q']) == "completed")) {
                        $data['order_type'] = ucfirst($_REQUEST['q']);
                        $order_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('FKMemberID' => $member_id, 'OrderStatus' => $data['order_type']));
                        if ($order_count > 0) {
                            $page_no = 0;
                            if (isset($_REQUEST['p']) && is_numeric($_REQUEST['p'])) {
                                $page_no = $_REQUEST['p'];
                            }
                            $page_no = ($page_no * 5);
                            if ($page_no >= $order_count) {
                                $data['has_more'] = "no";
                            } else {
                                $data['has_more'] = "yes";
                            }
                            $data['order_records'] = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,Currency,InvoiceNumber,GrandTotal,PaymentStatus,OrderStatus,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,CreatedDateTime", array('FKMemberID' => $member_id, 'OrderStatus' => $data['order_type']), false, 5, $page_no, "PKInvoiceID");
                            foreach ($data['order_records'] as $key => $value) {
                                $data['order_records'][$key]['CreatedDate'] = date($this->config->item('q'), strtotime($value['CreatedDateTime']));
                                $data['order_records'][$key]['CreatedTime'] = date('h:i a', strtotime($value['CreatedDateTime']));
                                $data['order_records'][$key]['PickupDate'] = $value['PickupDate'];
                                $data['order_records'][$key]['PickupTime'] = $value['PickupTime'];

                                $data['order_records'][$key]['DeliveryDate'] = $value['DeliveryDate'];
                                $data['order_records'][$key]['DeliveryTime'] = $value['DeliveryTime'];

                                $data['order_records'][$key]['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "Title", array('FKInvoiceID' => $value['PKInvoiceID']));
                            }
                        }
                        if ($this->input->is_ajax_request() == true) {
                            echo json_encode($data);
                        } else {
                            $this->_URLChecker();
                            $data['only_copy_right_section'] = true;
                            $data['menu_name'] = $data['order_type'] . ' Orders';
                            $this->show_view_with_menu('member/order_listing', $data);
                        }
                    } else {
                        redirect(base_url($this->config->item('front_dashboard_page_url')));
                    }
                } else if ($current_url == $this->config->item('front_member_order_detail_page_url')) {
                    $is_valid = false;
                    $is_admin_invoice = false;
                    if (isset($_REQUEST['t']) && $_REQUEST['t'] == "v") {
                        if ($this->IsAdminLogin()) {
                            $is_admin_invoice = true;
                            $data['FKFranchiseID'] = $this->GetCurrentFranchiseID();
                        }
                    }
                    if ($is_admin_invoice == false) {
                        if ($this->IsMemberLogin() == false) {
                            redirect(base_url($this->config->item('front_login_page_url')));
                        }
                    }
                    if ($current_second_url) {
                        if ($is_admin_invoice == true) {
                            $data['invoice_record'] = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $current_second_url));
                        } else {
                            $data['invoice_record'] = $this->model_database->GetRecord($this->tbl_invoices, false, array('FKMemberID' => $member_id, 'InvoiceNumber' => $current_second_url));
                        }
                        if ($data['invoice_record'] != false) {
                            $data['invoice_record']['preference_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", false, array('FKInvoiceID' => $data['invoice_record']['PKInvoiceID']), false, false, false, "PKInvoicePreferenceID", "asc");
                            if (sizeof($data['invoice_record']['preference_records']) == 0) {
                                unset($data['invoice_record']['preference_records']);
                            }
                            $data['invoice_record']['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $data['invoice_record']['PKInvoiceID']), false, false, false, "PKInvoiceServiceID", "asc");
                            if (sizeof($data['invoice_record']['service_records']) == 0) {
                                unset($data['invoice_record']['service_records']);
                            }
                            $is_valid = true;
                        }
                    }
                    if ($is_valid == false) {
                        $this->_page_not_found();
                    } else {
                        $this->_URLChecker();
                        $data['only_copy_right_section'] = true;
                        if ($is_admin_invoice) {
                            $data['is_admin_view'] = true;
                        }
                        $data['member_detail'] = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $data['invoice_record']['FKMemberID']));
                        $data['menu_name'] = 'Orders #' . $current_second_url;
                        $data['REGULARITY'] = array(0 => 'Just once', 1 => 'Weekly', 2 => 'Every two weeks');

                        $this->show_view_with_menu('member/order_detail', $data);
                    }
                } else if ($current_url == $this->config->item('front_select_item_page_url')) {


                    if ($this->input->is_ajax_request() == true) {

                        $is_invoice_found = false;
                        if ($this->IsSessionItem("admin_invoice_id") || $this->IsSessionItem("member_invoice_id")) {

                            $invoice_id = 0;
                            if ($this->IsSessionItem("admin_invoice_id")) {
                                $invoice_id = $this->GetSessionItem("admin_invoice_id");
                            } else if ($this->IsSessionItem("member_invoice_id")) {
                                $invoice_id = $this->GetSessionItem("member_invoice_id");
                            }


                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));

                            if ($invoice_record != false) {
                                $invoice_record["DiscountID"] = $invoice_record["FKDiscountID"];
                                unset($invoice_record["FKDiscountID"]);
                                $invoice_record['Pick_Full_Date'] = $this->GetFullDayName(date('w', strtotime($invoice_record['PickupDate']))) . ', ' . date('F', strtotime($invoice_record['PickupDate'])) . ' ' . date('dS', strtotime($invoice_record['PickupDate']));
                                $invoice_record['Delivery_Full_Date'] = $this->GetFullDayName(date('w', strtotime($invoice_record['DeliveryDate']))) . ', ' . date('F', strtotime($invoice_record['DeliveryDate'])) . ' ' . date('dS', strtotime($invoice_record['DeliveryDate']));
                                $invoice_record['preference_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID as PreferenceID,ParentTitle", array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoicePreferenceID", "asc");
                                foreach ($invoice_record['preference_records'] as $key => $value) {
                                    $invoice_record['preference_records'][$key]['FieldName'] = str_replace("-", "_", CreateAccessURL($value['ParentTitle']));
                                }
                                $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID as ServiceID,FKCategoryID as CategoryID,Title,DesktopImageName as DesktopImage,MobileImageName as MobileImage,Quantity,Price,Total,IsPackage as Package,PreferencesShow as PreferenceArea", array('FKInvoiceID' => $invoice_record['PKInvoiceID']), false, false, false, "PKInvoiceServiceID", "asc");

                                //$invoice_record['member_record'] = $this->model_database->GetRecord($this->tbl_members,false,array('PKMemberID'=>$invoice_record['FKMemberID']));
                                $invoice_record['card_record'] = $this->model_database->GetRecord($this->tbl_member_cards, "PKCardID as CardID,Title,Name,Number as CardNumber,Year as ExpireYear,Month as ExpireMonth,Code as SecurityCode", array('PKCardID' => $invoice_record['FKCardID']));
                                if ($invoice_record['card_record'] != false) {
                                    $invoice_record['card_record']['CardNumber'] = $invoice_record['card_record']['CardNumber'];
                                    $invoice_record['card_record']['CardNumberMasked'] = $invoice_record['card_record']['CardNumber'];
                                    $invoice_record['card_record']['SecurityCode'] = $invoice_record['card_record']['SecurityCode'];
                                    $invoice_record['card_record']['SecurityCodeMasked'] = $invoice_record['card_record']['SecurityCode'];
                                }
                                echo "success||" . json_encode($invoice_record, true);
                                $is_invoice_found = true;
                            }
                        }
                        if ($is_invoice_found == false) {
                            echo "error||";
                        }
                    } else {
                        if (isset($_REQUEST['t'])) {
                            if (strtolower($_REQUEST['t']) == "add" || strtolower($_REQUEST['t']) == "edit") {
                                if (strtolower($_REQUEST['t']) == "add") {
                                    if ($this->IsAdminLogin() == true) {
                                        if (isset($_REQUEST['m'])) {
                                            $id = DecodeString($_REQUEST['m']);
                                            if (!empty($id)) {
                                                $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('PKMemberID' => $id));
                                                if ($member_record !== false) {
                                                    $this->_CreateMemberSession($member_record['PKMemberID'], true);
                                                    $this->AddSessionItem("invoice_type", strtolower($_REQUEST['t']));
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (isset($_REQUEST['i'])) {
                                        $id = $_REQUEST['i'];

                                        if (!empty($id)) {
                                            if (!isset($_REQUEST['u'])) {
                                                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,FKFranchiseID,FKMemberID,PostalCode,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('PKInvoiceID' => $id));
                                                if ($invoice_record != false) {
                                                    $this->AddSessionItem($this->session_post_code, $invoice_record['PostalCode']);
                                                    $this->AddSessionItem($this->session_franchise_record, $this->_GetFranchiseRecord($invoice_record['FKFranchiseID']));
                                                    $this->_CreateMemberSession($invoice_record['FKMemberID'], true);
                                                    $this->AddSessionItem("invoice_type", strtolower($_REQUEST['t']));
                                                    $this->AddSessionItem("admin_invoice_id", $invoice_record['PKInvoiceID']);
                                                    $this->RemoveSessionItems("member_invoice_id");
                                                }
                                            } else {

                                                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,FKFranchiseID,FKMemberID,PostalCode,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('InvoiceNumber' => $id, 'FKMemberID' => $this->GetCurrentMemberID()));
                                                if ($invoice_record != false) {
                                                    $this->AddSessionItem($this->session_post_code, $invoice_record['PostalCode']);
                                                    $this->AddSessionItem($this->session_franchise_record, $this->_GetFranchiseRecord($invoice_record['FKFranchiseID']));
                                                    $this->_CreateMemberSession($invoice_record['FKMemberID']);
                                                    $this->AddSessionItem("invoice_type", strtolower($_REQUEST['t']));
                                                    $this->AddSessionItem("member_invoice_id", $invoice_record['PKInvoiceID']);
                                                    $this->RemoveSessionItems("admin_invoice_id");
                                                }
                                            }

                                            $data["PKInvoiceID"] = $id;
                                            $data["PickupDate"] = $invoice_record["PickupDate"];
                                            $data["PickupTime"] = $invoice_record["PickupTime"];
                                            $data["DeliveryDate"] = $invoice_record["DeliveryDate"];
                                            $data["DeliveryTime"] = $invoice_record["DeliveryTime"];
                                            $this->AddSessionItem("invoice_id", $data['PKInvoiceID']);
                                            //$data["invoice_id"]=$invoice_record['PKInvoiceID'];
                                        }
                                    }
                                }
                            }
                        }
                        //d($data,1);
                        if ($this->IsSessionItem($this->session_post_code) && $this->IsSessionItem($this->session_franchise_record)) {
                            $this->_URLChecker();
                            $franchise_record = $this->GetSessionItem($this->session_franchise_record);

                            $franchise_id = 0;
                            if (isset($franchise_record) && !empty($franchise_record)) {
                                $franchise_id = isset($franchise_record['PKFranchiseID']) ? $franchise_record['PKFranchiseID'] : 0;
                            }
                            $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                            if (sizeof($data['category_records']) > 0) {
                                foreach ($data['category_records'] as $key => $value) {
                                    //$service_records = $this->model_database->GetRecords($this->tbl_services,"R",'PKServiceID,Title,Content,Price,DesktopImageName,MobileImageName,PreferencesShow,IsPackage as Package',array('Status'=>'Enabled','FKCategoryID'=>$value['PKCategoryID']),false,false,false,'Position','asc');
                                    $service_records = $this->model_database->FranchiseCustomServicesSelectItems($franchise_id, $value['PKCategoryID']);
                                    if (sizeof($service_records) > 0) {
                                        $data['category_records'][$key]['service_records'] = $service_records;
                                    } else {
                                        unset($data['category_records'][$key]);
                                    }
                                }
                            }

                            //d($data,1);

                            if (sizeof($data['category_records']) == 0) {
                                redirect(base_url());
                            } else {
                                $data['is_order_page'] = true;
                                $data['preference_records'] = $this->_GetPreferences();
                                if (sizeof($data['preference_records']) == 0) {
                                    unset($data['preference_records']);
                                } else {
                                    $data['member_preference_records'] = $this->_GetMemberPreferences();
                                }
                                //d($data['member_preference_records'],1);
                                if (!$this->IsSessionItem("invoice_type")) {
                                    $this->RemoveSessionItems("invoice_type");
                                    $this->RemoveSessionItems("admin_invoice_id");
                                    $this->RemoveSessionItems("member_invoice_id");
                                    if ($this->IsMemberLogin() == true && $this->IsMemberLoginFromAdmin() == true) {
                                        $this->AddSessionItem("is_love_2_laundry_admin_member_logged_in", false);
                                    }
                                }
                                $this->show_view_with_menu('order/items', $data);
                            }
                        } else {
                            redirect(base_url());
                        }
                    }
                } else if ($current_url == $this->config->item('front_address_selection_page_url')) {



                    $PKInvoiceID = $this->GETSessionItem("invoice_id");

                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,FKFranchiseID,FKMemberID,PostalCode,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('InvoiceNumber' => $PKInvoiceID));

                    $data["PKInvoiceID"] = $PKInvoiceID;
                    $data["PickupDate"] = $invoice_record["PickupDate"];
                    $data["PickupTime"] = $invoice_record["PickupTime"];
                    $data["DeliveryDate"] = $invoice_record["DeliveryDate"];
                    $data["DeliveryTime"] = $invoice_record["DeliveryTime"];

                    if ($this->input->is_ajax_request() == true) {

                        if ($this->IsSessionItem($this->session_post_code) && $this->IsSessionItem($this->session_franchise_record)) {
                            $location_array = array();

                            $location_records = $this->model_database->GetRecords($this->tbl_locations, "R", "PKLocationID as LocationID,Title,PostalCode", array("Status" => 'Enabled', "PostalCode" => str_replace(" ", "", $this->GetSessionItem($this->session_post_code))));
                            /* if($this->GetCurrentMemberID() && $this->GetCurrentMemberID()==1)
                              {SW11 7US
                              echo $this->GetSessionItem($this->session_post_code);
                              d($location_records,1);
                              } */
                            if (sizeof($location_records) <= 0) {
                                $location_records = $this->model_database->GetRecords($this->tbl_locations, "R", "PKLocationID as LocationID,Title,PostalCode", array("Status" => 'Enabled', "PostalCode" => $this->GetSessionItem($this->session_post_code)));
                            }
                            if (sizeof($location_records) > 0) {
                                foreach ($location_records as $key => $value) {
                                    $locker_records = $this->model_database->GetRecords($this->tbl_lockers, "R", "Title", array("FKLocationID" => $value['LocationID']));
                                    if (sizeof($locker_records) > 0) {
                                        $location_records[$key]['lockers'] = $locker_records;
                                    } else {
                                        unset($location_records[$key]);
                                    }
                                }
                            }

                            $location_array[] = $location_records;
                            if ($this->IsMemberLogin() == true) {
                                if (!empty($member_detail['PostalCode'])) {
                                    $member_array = array(
                                        'Text' => 'Use Default Address',
                                        'BuildingName' => $member_detail['BuildingName'],
                                        'StreetName' => $member_detail['StreetName'],
                                        'PostalCode' => $member_detail['PostalCode'],
                                        'Town' => $member_detail['Town']
                                    );
                                    $location_array[] = $member_array;
                                }
                            }
                            echo json_encode($location_array, true);
                        } else {
                            echo "error||t||" . base_url();
                        }
                    } else {
                        if ($this->IsSessionItem($this->session_post_code) && $this->IsSessionItem($this->session_franchise_record)) {

                            $this->_URLChecker();
                            $data['is_order_page'] = true;
                            $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                            if (sizeof($data['category_records']) > 0) {
                                foreach ($data['category_records'] as $key => $value) {
                                    $service_records = $this->model_database->GetRecords($this->tbl_services, "R", 'PKServiceID,Title,Content,Price,DesktopImageName,MobileImageName,PreferencesShow,IsPackage as Package', array('Status' => 'Enabled', 'FKCategoryID' => $value['PKCategoryID']), false, false, false, 'Position', 'asc');
                                    if (sizeof($service_records) > 0) {
                                        $data['category_records'][$key]['service_records'] = $service_records;
                                    } else {
                                        unset($data['category_records'][$key]);
                                    }
                                }
                            }
                            $data['preference_records'] = $this->_GetPreferences();
                            if (sizeof($data['preference_records']) == 0) {
                                unset($data['preference_records']);
                            } else {
                                $data['member_preference_records'] = $this->_GetMemberPreferences();
                            }
                            if ($this->IsMemberLogin() == true) {
                                $data['member_card_records'] = $this->model_database->GetRecords($this->tbl_member_cards, "R", false, array('FKMemberID' => $member_id), false, false, false, "PKCardID");
                                if (sizeof($data['member_card_records']) == 0) {
                                    unset($data['member_card_records']);
                                } else {
                                    foreach ($data['member_card_records'] as $key => $value) {
                                        $data['member_card_records'][$key]['Number'] = encrypt_decrypt("decrypt", $value['Number']);
                                        $data['member_card_records'][$key]['NumberMasked'] = $this->MaskedInput($data['member_card_records'][$key]['Number'], -4);
                                        $data['member_card_records'][$key]['Code'] = encrypt_decrypt("decrypt", $value['Code']);
                                        $data['member_card_records'][$key]['CodeMasked'] = $this->MaskedInput($data['member_card_records'][$key]['Code'], -1);
                                    }
                                }
                            }

                            $data['hidden_session_postal_code'] = $this->GetSessionItem($this->session_post_code);
                            //d($data['hidden_session_postal_code'],1);
                            $this->show_view_with_menu('order/address', $data);
                        } else {
                            redirect(base_url());
                        }
                    }
                } else if ($current_url == $this->config->item('front_check_out_page_url')) {
                    if ($this->IsSessionItem($this->session_post_code) && $this->IsSessionItem($this->session_franchise_record)) {
                        if ($this->IsMemberLogin() == true) {
                            $this->_URLChecker();
                            $data['is_order_page'] = true;
                            $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                            if (sizeof($data['category_records']) > 0) {
                                foreach ($data['category_records'] as $key => $value) {
                                    $service_records = $this->model_database->GetRecords($this->tbl_services, "R", 'PKServiceID,Title,Content,Price,DesktopImageName,MobileImageName,PreferencesShow,IsPackage as Package', array('Status' => 'Enabled', 'FKCategoryID' => $value['PKCategoryID']), false, false, false, 'Position', 'asc');
                                    if (sizeof($service_records) > 0) {
                                        $data['category_records'][$key]['service_records'] = $service_records;
                                    } else {
                                        unset($data['category_records'][$key]);
                                    }
                                }
                            }
                            $data['preference_records'] = $this->_GetPreferences();
                            if (sizeof($data['preference_records']) == 0) {
                                unset($data['preference_records']);
                            } else {
                                $data['member_preference_records'] = $this->_GetMemberPreferences();
                            }
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_id));
                            if ($invoice_record != false) {
                                $data['is_first_order'] = "No";
                            }



                            if ($this->IsMemberLogin() == true) {
                                $data['member_card_records'] = $this->model_database->GetRecords($this->tbl_member_cards, "R", false, array('FKMemberID' => $member_id), false, false, false, "PKCardID");
                                if (sizeof($data['member_card_records']) == 0) {
                                    unset($data['member_card_records']);
                                } else {
                                    foreach ($data['member_card_records'] as $key => $value) {
                                        $data['member_card_records'][$key]['Number'] = $value['Number'];
                                        $data['member_card_records'][$key]['NumberMasked'] = $data['member_card_records'][$key]['Number'];
                                        $data['member_card_records'][$key]['Code'] = $value['Code'];
                                        $data['member_card_records'][$key]['CodeMasked'] = $data['member_card_records'][$key]['Code'];
                                    }
                                }
                            }
                            $data["current_url"] = $current_url;

                            $data["pickup_date"] = $this->GetSessionItem($this->pickup_date);
                            $data["pickup_time"] = $this->GetSessionItem($this->pickup_time);
                            $data["delivery_date"] = $this->GetSessionItem($this->delivery_date);
                            $data["delivery_time"] = $this->GetSessionItem($this->delivery_time);

                            $data["address1"] = $this->GetSessionItem($this->address1);
                            $data["address2"] = $this->GetSessionItem($this->address2);
                            $data["town"] = $this->GetSessionItem($this->town);
                            $this->show_view_with_menu('order/checkout', $data);
                        } else {
                            redirect(base_url($this->config->item('front_address_selection_page_url')));
                        }
                    } else {
                        redirect(base_url());
                    }
                } else if ($current_url == "areas-we-cover") {
                    $this->_URLChecker();
                    $franchise_postcode_records = $this->model_database->GetDistinctRecords($this->tbl_franchise_post_codes, "R", "Code", false, false, false, false, "Code", "asc");
                    $letter_array = array();
                    foreach ($franchise_postcode_records as $record) {
                        $letter = strtoupper(substr($record["Code"], 0, 1));
                        if (!in_array($letter, $letter_array)) {
                            $letter_array[] = $letter;
                        }
                    }
                    if (sizeof($letter_array) > 0) {
                        $data['franchise_postcode_array'] = array();
                        $counter = 0;
                        foreach ($letter_array as $value) {
                            $data['franchise_postcode_array'][$counter]["Letter"] = $value;
                            $data['franchise_postcode_array'][$counter]["Codes"] = array();
                            foreach ($franchise_postcode_records as $record) {
                                $letter = strtoupper(substr($record["Code"], 0, 1));
                                if ($value == $letter) {
                                    $data['franchise_postcode_array'][$counter]["Codes"][] = $record["Code"];
                                }
                            }
                            $counter += 1;
                        }
                    }
                    $this->show_view_with_menu('pages/areas_cover', $data);
                } else if ($current_url == "crowd-funding") {
                    if ($data['page_data'] != false) {
                        $this->_URLChecker();
                        $this->show_view_with_menu('pages/crowd', $data);
                    } else {
                        $this->_page_not_found();
                    }
                } else {

                    if ($data['page_data'] != false) {
                        $this->_URLChecker();
                        if ($data['page_data']['AccessURL'] == "pricing") {
                            $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                            if (sizeof($data['category_records']) > 0) {
                                foreach ($data['category_records'] as $key => $value) {
                                    $service_records = $this->model_database->GetRecords($this->tbl_services, "R", 'PKServiceID,Title,Content,Price', array('Status' => 'Enabled', 'FKCategoryID' => $value['PKCategoryID']), false, false, false, 'Position', 'asc');
                                    if (sizeof($service_records) > 0) {
                                        $data['category_records'][$key]['service_records'] = $service_records;
                                    } else {
                                        unset($data['category_records'][$key]);
                                    }
                                }
                            }
                            $this->show_view_with_menu('pages/pricing', $data);
                        } else if ($data['page_data']['AccessURL'] == "business-services") {
                            $widget_first_title = $this->config->item("corporate_widget_first_title");
                            $data["widget_first_title"] = isset($widget_first_title) ? $widget_first_title : "B&amp;B";
                            $this->show_view_with_menu('pages/corporate', $data);
                        } else if ($data['page_data']['AccessURL'] == "t-c") {
                            $this->show_view_with_menu('pages/terms_condition', $data);
                        } else if ($data['page_data']['AccessURL'] == "privacy-policy") {
                            $this->show_view_with_menu('pages/privacy', $data);
                        } else if ($data['page_data']['AccessURL'] == "about-love-2-laundry") {
                            $data['is_work_widget_show'] = true;
                            $widget_record = $this->GetWidgetRecord("Title", "About Love2Laundry");
                            if ($widget_record != false) {
                                $data['widget_about_record'] = $widget_record;
                            }
                            $this->show_view_with_menu('pages/page', $data);
                        } else if ($data['page_data']['AccessURL'] == "manchester" || $data['page_data']['AccessURL'] == "birmingham" || $data['page_data']['AccessURL'] == "liverpool" || $data['page_data']['AccessURL'] == "oxford" || $data['page_data']['AccessURL'] == "amsterdam") {
                            /* Uae pages check 22-55-2018 */
                            $this->show_view_with_menu('pages/aepage', $data);
                        } else if ($data['page_data']['AccessURL'] == "welcome") {
                            $data['is_work_widget_show'] = true;
                            $data['is_why_widget_show'] = true;
                            $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_video_widget_id);
                            if ($widget_record != false) {
                                $data['widget_video_record'] = $widget_record;
                            }
                            $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_about_widget_id);
                            if ($widget_record != false) {
                                $data['widget_about_record'] = $widget_record;
                            }
                            $widget_record = $this->GetWidgetRecord("PKWidgetID", $this->home_price_widget_id);
                            if ($widget_record != false) {
                                $data['widget_price_record'] = $widget_record;
                            }
                            $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", $this->home_price_widget_section_id);
                            if ($widget_section_record != false) {
                                $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 4);
                                if (sizeof($widget_navigation_records) > 0) {
                                    $data['widget_price_section_navigation_records'] = $widget_navigation_records;
                                }
                            }
                            $data['is_home_page'] = true;
                            $this->show_view_with_menu('home', $data);
                        } else {

                            if ($data['page_data']['Template'] == "None") {
                                $data['is_work_widget_show'] = true;
                                $data['is_why_widget_show'] = true;
                                $this->show_view_with_menu('pages/page', $data);
                            } else {
                                $this->show_area_view_with_menu('home', $data);
                            }
                        }
                    } else {
                        $this->_page_not_found();
                    }
                }
            }
        }
    }

    function notifyOrder()
    {
    }

    private function regularly_cron()
    {
        die("die");
        $Invoices = $this->GetInvoices();
        if (isset($Invoices) && !empty($Invoices)) {
            echo 'Current Date time of Server ' . date('d-m-Y H:i:s') . '<br>';
            foreach ($Invoices as $key => $val) {
                $date_pickup = '';
                $time_pickup = '';

                $date_delivery = '';
                $time_delivery = '';

                if (isset($val['Regularly']) && $val['Regularly'] == 1) {
                    $PickupDatelog = $val['PickupDate'] . '<br>';
                    $date_diffrence = $this->date_diffrence($val['PickupDate'], date('Y-m-d'));
                    $date_diffrencelog = $date_diffrence = (int) ($date_diffrence + 1);
                    $date_diffrence;
                    echo '<br>';

                    echo 'Logs' . $key . ' ---  Regularly = ' . $val['Regularly'] . ' ----- Invoice =  ' . $val['PKInvoiceID'] . ' --- PickupDate ' . $PickupDatelog . '--- Difference = ', $date_diffrencelog . '------- Is Testing ' . $val['IsTestOrder'] . '---ends <br>';

                    if (isset($date_diffrence) && $date_diffrence == 7) {

                        $TimeSlot = $this->getTimeSlot($val['PostalCode']);

                        $pick_up_array = $TimeSlot['pick_up'];
                        $tomorrow_timestamp = strtotime("+ 1 day");

                        $tomorrow_date = date("d-m-Y", $tomorrow_timestamp);


                        $time_pickup = '';
                        $date_pickup = '';
                        $pHour = '';
                        $found = true;
                        $pickupday = '';

                        if (isset($pick_up_array) && !empty($pick_up_array)) {
                            $tmpCont = 0;
                            while ($found) {

                                foreach ($pick_up_array as $pk => $pval) {

                                    if (isset($pval['Times']) && !empty($pval['Times']) && strtotime($pval['Date_Full']) <= strtotime($tomorrow_date)) {
                                        $times_array = $pval['Times'];

                                        if (isset($times_array) && !empty($times_array)) {
                                            foreach ($times_array as $tk => $tval) {

                                                if (isset($tval['Class']) && $tval['Class'] == 'Available') {
                                                    $time_pickup = $tval['Time'];
                                                    $date_pickup = $tomorrow_date;
                                                    $pickupday = $pval['Day_Full_Name'];
                                                    $pHour = $tval['Hour'];
                                                    $found = false;
                                                    break;
                                                }
                                            }
                                        } else {
                                            //echo '<br> Time array  not found against Post Code '.$val['PostalCode'].' -- <br>';break;
                                            $tomorrow_date = date("d-m-Y", strtotime("+ " . ($pk + 1) . " day"));
                                        }

                                        if ($time_pickup == '' && $date_pickup == '') {
                                            $tomorrow_date = date("d-m-Y", strtotime("+ " . ($pk + 1) . " day"));
                                        } else {
                                            break;
                                        }
                                    } else {
                                        //echo '<br> Pdate';echo $pval['Date_Full'];
                                        $tomorrow_date = date("d-m-Y", strtotime("+ " . ($pk + 1) . " day"));
                                    }
                                }
                                if (!$found)
                                    break;
                                $tmpCont++;
                                if ($tmpCont > count($pick_up_array))
                                    echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- Date ' . $tomorrow_date . '<br>';
                                exit;
                            }
                        } else {
                            echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                            continue;
                        }
                        $time_pickupArray = explode('-', $time_pickup);

                        if (!isset($time_pickupArray[0])) {
                            continue;
                        }

                        $date_pickupandTime = $date_pickup . ' ' . $time_pickupArray[0];

                        if ($date_pickup != '' && $time_pickup != '' && $date_pickupandTime != '') {

                            // delivery calculation
                            $delivery_time_array = $TimeSlot['delivery'];
                            $delivery_option = isset($TimeSlot['delivery_option']) ? $TimeSlot['delivery_option'] : '';

                            $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +36 hour'));
                            $delivery_time = date('H', strtotime($date_pickupandTime . ' +36 hour'));

                            //echo $date_pickupandTime.'---'.$delivery_date;die;
                            $time_delivery = '';
                            $date_delivery = '';

                            $found = true;
                            if (isset($delivery_time_array) && !empty($delivery_time_array)) {
                                $tmpCont = 0;

                                $Date_Full = array();
                                foreach ($delivery_time_array as $ktmp => $tmpval) {
                                    $Date_Full[] = $tmpval['Date_Full'];
                                }

                                while ($found) {

                                    $modified_delievery = '';
                                    $modified_delieverytime = '';
                                    $timevar = 36;

                                    foreach ($delivery_time_array as $pk => $pval) {

                                        if ($modified_delievery != '' && $modified_delieverytime != '') {
                                            $delivery_date = $modified_delievery;
                                            $delivery_time = $modified_delieverytime;
                                        } else {
                                            if (isset($pickupday) && ($pickupday == 'Saturday') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                $timevar = 44;
                                                $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +44 hour'));
                                            } elseif (isset($pickupday) && ($pickupday == 'Sunday') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                $timevar = 44;
                                                $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +44 hour'));
                                            }
                                        }
                                        echo $delivery_date;
                                        echo '----';
                                        echo $pval['Date_Full'];
                                        echo '<br>';

                                        if (in_array($delivery_date, $Date_Full)) {
                                            $times_array = $pval['Times'];
                                            if (isset($times_array) && !empty($times_array)) {

                                                foreach ($times_array as $tk => $tval) {
                                                    if (isset($tval['Class']) && $tval['Class'] == 'Available' && isset($tval['Hour']) && $tval['Hour'] >= (int) $delivery_time) {
                                                        $dayname = date("D", strtotime($delivery_date));
                                                        if (isset($dayname) && ($dayname == 'Mon') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                            if (isset($tval['Hour']) && $tval['Hour'] > 15) {
                                                                $tval['Time'];
                                                                $time_delivery = $tval['Time'];
                                                                $date_delivery = $delivery_date;
                                                                $found = false;
                                                                break;
                                                            } else {
                                                                continue;
                                                            }
                                                        } else {
                                                            $delivery_time;
                                                            $tval['Time'];
                                                            $time_delivery = $tval['Time'];
                                                            $date_delivery = $delivery_date;
                                                            $found = false;
                                                            break;
                                                        }
                                                    } else {
                                                        //$delivery_date = date("d-m-Y H:i", strtotime($delivery_date." +".($tk+1)." hour"));
                                                        //$delivery_time = date("H", strtotime($delivery_date." +".($tk+1)." hour"));
                                                        continue;
                                                    }
                                                }
                                            } else {
                                                echo '<br> Time array  not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                                                break;
                                            }
                                            if ($time_delivery == '' && $date_delivery == '') {
                                                $modified_delievery = date("d-m-Y", strtotime($delivery_date . " +" . (24) . " hour"));
                                                $modified_delieverytime = date("H", strtotime($modified_delievery));

                                                continue;
                                            } else {
                                                break;
                                            }
                                        } else {
                                            $modified_delievery = date("d-m-Y", strtotime($delivery_date . " +" . (24) . " hour"));
                                            $modified_delieverytime = date("H", strtotime($modified_delievery));
                                            continue;
                                        }
                                    }
                                    if (!$found)
                                        break;
                                    $tmpCont++;
                                    if ($tmpCont > count($delivery_time_array))
                                        echo '<br> delivery_time_array getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- Date ' . $delivery_date . '<br>';
                                    exit;
                                }
                            } else {
                                echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                            }
                        }
                    }
                } elseif (isset($val['Regularly']) && $val['Regularly'] == 2) {
                    $PickupDatelog = $val['PickupDate'] . '<br>';
                    $date_diffrence = $this->date_diffrence($val['PickupDate'], date('Y-m-d'));
                    $date_diffrencelog = $date_diffrence = (int) ($date_diffrence + 1);
                    echo '<br>';

                    echo 'Logs' . $key . ' ---  Regularly = ' . $val['Regularly'] . ' ----- Invoice =  ' . $val['PKInvoiceID'] . ' --- PickupDate ' . $PickupDatelog . '--- Difference = ', $date_diffrencelog . '------- Is Testing ' . $val['IsTestOrder'] . '---ends <br>';
                    if (isset($date_diffrence) && $date_diffrence == 14) {

                        $TimeSlot = $this->getTimeSlot($val['PostalCode']);

                        $pick_up_array = $TimeSlot['pick_up'];
                        $tomorrow_timestamp = strtotime("+ 1 day");

                        $tomorrow_date = date("d-m-Y", $tomorrow_timestamp);


                        $time_pickup = '';
                        $date_pickup = '';
                        $pHour = '';
                        $found = true;
                        $pickupday = '';

                        if (isset($pick_up_array) && !empty($pick_up_array)) {
                            $tmpCont = 0;
                            while ($found) {

                                foreach ($pick_up_array as $pk => $pval) {
                                    if (isset($pval['Times']) && !empty($pval['Times']) && strtotime($pval['Date_Full']) <= strtotime($tomorrow_date)) {
                                        $times_array = $pval['Times'];

                                        if (isset($times_array) && !empty($times_array)) {
                                            foreach ($times_array as $tk => $tval) {

                                                if (isset($tval['Class']) && $tval['Class'] == 'Available') {
                                                    $time_pickup = $tval['Time'];
                                                    $date_pickup = $tomorrow_date;
                                                    $pickupday = $pval['Day_Full_Name'];
                                                    $pHour = $tval['Hour'];
                                                    $found = false;
                                                    break;
                                                }
                                            }
                                        } else {
                                            //echo '<br> Time array  not found against Post Code '.$val['PostalCode'].' -- <br>';break;
                                            $tomorrow_date = date("d-m-Y", strtotime($tomorrow_date . "+ " . ($pk + 1) . " day"));
                                        }

                                        if ($time_pickup == '' && $date_pickup == '') {
                                            $tomorrow_date = date("d-m-Y", strtotime($tomorrow_date . "+ " . ($pk + 1) . " day"));
                                        } else {
                                            break;
                                        }
                                    } else {
                                        //echo '<br> Pdate';echo $pval['Date_Full'];
                                        $tomorrow_date = date("d-m-Y", strtotime($tomorrow_date . "+ " . ($pk + 1) . " day"));
                                    }
                                }
                                if (!$found)
                                    break;
                                $tmpCont++;
                                if ($tmpCont > count($pick_up_array))
                                    echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- Date ' . $tomorrow_date . '<br>';
                                exit;
                            }
                        } else {
                            echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                            continue;
                        }
                        $time_pickupArray = explode('-', $time_pickup);

                        if (!isset($time_pickupArray[0])) {
                            continue;
                        }

                        $date_pickupandTime = $date_pickup . ' ' . $time_pickupArray[0];

                        if ($date_pickup != '' && $time_pickup != '' && $date_pickupandTime != '') {

                            // delivery calculation
                            $delivery_time_array = $TimeSlot['delivery'];
                            $delivery_option = isset($TimeSlot['delivery_option']) ? $TimeSlot['delivery_option'] : '';

                            $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +36 hour'));
                            $delivery_time = date('H', strtotime($date_pickupandTime . ' +36 hour'));

                            $time_delivery = '';
                            $date_delivery = '';

                            $found = true;
                            if (isset($delivery_time_array) && !empty($delivery_time_array)) {
                                $tmpCont = 0;

                                $Date_Full = array();
                                foreach ($delivery_time_array as $ktmp => $tmpval) {
                                    $Date_Full[] = $tmpval['Date_Full'];
                                }
                                while ($found) {
                                    $modified_delievery = '';
                                    $modified_delieverytime = '';
                                    $timevar = 36;

                                    foreach ($delivery_time_array as $pk => $pval) {

                                        if ($modified_delievery != '' && $modified_delieverytime != '') {
                                            $delivery_date = $modified_delievery;
                                            $delivery_time = $modified_delieverytime;
                                        } else {
                                            if (isset($pickupday) && ($pickupday == 'Saturday') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                $timevar = 44;
                                                $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +44 hour'));
                                            } elseif (isset($pickupday) && ($pickupday == 'Sunday') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                $timevar = 44;
                                                $delivery_date = date('d-m-Y', strtotime($date_pickupandTime . ' +44 hour'));
                                            }
                                        }

                                        //echo $delivery_date;echo '-----';echo $pval['Date_Full'];echo '<br>';

                                        if (in_array($delivery_date, $Date_Full)) {
                                            $times_array = $pval['Times'];
                                            if (isset($times_array) && !empty($times_array)) {

                                                foreach ($times_array as $tk => $tval) {


                                                    if (isset($tval['Class']) && $tval['Class'] == 'Available' && isset($tval['Hour']) && $tval['Hour'] >= $delivery_time) {
                                                        $dayname = date("D", strtotime($delivery_date));

                                                        if (isset($dayname) && ($dayname == 'Mon') && $delivery_option == 'Saturday Sunday Both Pickup Delivery To Monday After 3') {
                                                            if (isset($tval['Hour']) && $tval['Hour'] > 15) {
                                                                $tval['Time'];
                                                                $time_delivery = $tval['Time'];
                                                                $date_delivery = $pval['Date_Full'];
                                                                $found = false;
                                                                break;
                                                            } else {
                                                                continue;
                                                            }
                                                        } else {
                                                            $delivery_time;
                                                            $tval['Time'];
                                                            $time_delivery = $tval['Time'];
                                                            $date_delivery = $pval['Date_Full'];
                                                            $found = false;
                                                            break;
                                                        }
                                                    } else {
                                                        continue;
                                                        //$delivery_date = date("d-m-Y H:i", strtotime($delivery_date." +".($tk+1)." hour"));
                                                        //$delivery_time = date("H", strtotime($delivery_date." +".($tk+1)." hour"));
                                                    }
                                                }
                                            } else {
                                                echo '<br> Time array  not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                                                break;
                                            }
                                            if ($time_delivery == '' && $date_delivery == '') {
                                                $modified_delievery = date("d-m-Y", strtotime($delivery_date . " +" . (24) . " hour"));
                                                $modified_delieverytime = date("H", strtotime($modified_delievery));

                                                continue;
                                            } else {
                                                break;
                                            }
                                        } else {

                                            $modified_delievery = date("d-m-Y", strtotime($delivery_date . " +" . (24) . " hour"));
                                            $modified_delieverytime = date("H", strtotime($modified_delievery));
                                            continue;
                                        }
                                    }
                                    if (!$found)
                                        break;
                                    $tmpCont++;
                                    if ($tmpCont > count($delivery_time_array))
                                        echo '<br> delivery_time_array getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- Date ' . $delivery_date . '<br>';
                                    exit;
                                }
                            } else {
                                echo '<br> getTimeSlot not found against Post Code ' . $val['PostalCode'] . ' -- <br>';
                            }
                        }
                    }
                } else {
                    continue;
                }
                // order creation
                if ($date_delivery != '') {
                    $date_delivery = date('d-m-Y', strtotime($date_delivery));
                }
                /* echo '<br>Current date'.date('d-m-Y H:i');
                  echo '<br> P date : ';echo $date_pickup;echo ' ';echo $time_pickup;
                  echo '<br> D date : ';echo $date_delivery;echo ' '; echo $time_delivery;continue;die; */
                if (isset($date_pickup) && $date_pickup != '' && isset($time_pickup) && $time_pickup != '' && isset($date_delivery) && $date_delivery != '' && isset($time_delivery) && $time_delivery != '') {
                    $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");
                    echo '<br> Invoice genrated ' . $invoice_number;
                    $FKMemberID = $val['FKMemberID'];
                    $InvoiceType = $val['InvoiceType'];
                    $member_detail = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $val['FKMemberID']));

                    $invoice_data = array(
                        'FKDeviceID' => $val['FKDeviceID'],
                        'OldInvoiceID' => $val['PKInvoiceID'],
                        'FKWebsiteID' => $val['FKWebsiteID'],
                        'FKFranchiseID' => $val['FKFranchiseID'],
                        'FKMemberID' => $val['FKMemberID'],
                        'FKCardID' => $val['FKCardID'],
                        //'FKDiscountID' => $val['FKDiscountID'],
                        'ReferralID' => $val['ReferralID'],
                        //'DiscountType' => $val['DiscountType'],
                        //'DiscountCode' => $val['DiscountCode'],
                        //'DiscountWorth' => $val['DiscountWorth'],
                        'DType' => $val['DType'],
                        'InvoiceType' => $val['InvoiceType'],
                        'Currency' => $val['Currency'],
                        'InvoiceNumber' => $invoice_number,
                        'Location' => $val['Location'],
                        'Locker' => $val['Locker'],
                        'BuildingName' => $val['BuildingName'],
                        'StreetName' => $val['StreetName'],
                        'PostalCode' => $val['PostalCode'],
                        'Town' => $val['Town'],
                        'CustomerNotes' => $val['CustomerNotes'],
                        'OrderNotes' => $val['OrderNotes'],
                        // 'AccountNotes' => $val['AccountNotes'],
                        //'AdditionalInstructions' => $val['AdditionalInstructions'],
                        //'PaymentMethod' => $val['PaymentMethod'],
                        //'PaymentStatus' => $val['PaymentStatus'],
                        //'PaymentReference' => $val['PaymentReference'],
                        'PaymentToken' => $val['PaymentToken'],
                        'OrderStatus' => $val['OrderStatus'],
                        'ServicesTotal' => $val['ServicesTotal'],
                        'PreferenceTotal' => $val['PreferenceTotal'],
                        'SubTotal' => $val['SubTotal'],
                        //'DiscountTotal' => $val['DiscountTotal'],//--
                        'GrandTotal' => $val['GrandTotal'],
                        'IPAddress' => $this->input->ip_address(), //--
                        'OrderPostFrom' => $val['OrderPostFrom'],
                        'PickupDate' => date('Y-m-d', strtotime($date_pickup)),
                        'PickupTime' => $time_pickup,
                        'DeliveryDate' => date('Y-m-d', strtotime($date_delivery)),
                        'DeliveryTime' => $time_delivery,
                        'Regularly' => $val['Regularly'],
                        'IsTestOrder' => $val['IsTestOrder'],
                        'CreatedDateTime' => date('Y-m-d H:i:s'),
                    );

                    $member_card_records_array = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as ID,Title,Name,Number as CardNumber,Year as ExpireYear,Month as ExpireMonth,Code as SecurityCode", array('FKMemberID' => $FKMemberID), false, false, false, "PKCardID");
                    $member_card_records = array();
                    $purchasecard = array();
                    if (isset($member_card_records_array) && !empty($member_card_records_array)) {
                        foreach ($member_card_records_array as $key => $valc) {
                            if ($valc['ID'] == $val['FKCardID']) {
                                $member_card_records['ID'] = $valc['ID'];
                                $member_card_records['Title'] = $valc['Title'];
                                $member_card_records['Name'] = $valc['Name'];
                                $member_card_records['CardNumber'] = encrypt_decrypt("decrypt", $valc['CardNumber']);
                                $member_card_records['ExpireYear'] = $valc['ExpireYear'];
                                $member_card_records['ExpireMonth'] = $valc['ExpireMonth'];
                                $member_card_records['SecurityCode'] = encrypt_decrypt("decrypt", $valc['SecurityCode']);

                                $purchasecard['exp_month'] = $valc['ExpireMonth'];
                                $purchasecard['exp_year'] = $valc['ExpireYear'];

                                $purchasecard['number'] = encrypt_decrypt("decrypt", $valc['CardNumber']);
                                $purchasecard['cvc'] = encrypt_decrypt("decrypt", $valc['SecurityCode']);
                            }
                        }
                    }
                    $this->load->helper('language');
                    $this->lang->load('merchant', 'english');
                    $this->load->library('merchant');
                    $this->merchant->load('stripe');
                    $GrandTotal_old = $val['GrandTotal'];
                    $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                    if ($InvoiceType == "Items") {
                        $service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $val['PKInvoiceID']));
                        $preference_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", false, array('FKInvoiceID' => $val['PKInvoiceID']), false, false, false, "PKInvoicePreferenceID", "asc");
                        $package_count = 0;
                        $add_preferences = false;
                        if ($service_records != "null") {

                            foreach ($service_records as $serivce_data) {


                                $insert_invoice_service = array(
                                    'FKInvoiceID' => $invoice_id,
                                    'FKServiceID' => $serivce_data['FKServiceID'],
                                    'FKCategoryID' => $serivce_data['FKCategoryID'],
                                    'Title' => ($serivce_data['Title']),
                                    'DesktopImageName' => ($serivce_data['DesktopImageName']),
                                    'MobileImageName' => ($serivce_data['MobileImageName']),
                                    'IsPackage' => $serivce_data['IsPackage'],
                                    'PreferencesShow' => $serivce_data['PreferencesShow'],
                                    'Quantity' => $serivce_data['Quantity'],
                                    'Price' => $serivce_data['Price'],
                                    'Total' => $serivce_data['Total']
                                );
                                $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);

                                if ($serivce_data['PreferencesShow'] == "Yes") {
                                    $add_preferences = true;
                                }
                            }
                        }
                        if ($add_preferences == true) {
                            if ($preference_records != "null") {
                                $account_notes = "";
                                $additional_instructions = "";
                                $preference_cart_array = $preference_records;
                                $preference_position = 0;
                                foreach ($preference_records as $key => $val) {
                                    $insert_invoice_preference['FKInvoiceID'] = $invoice_id;
                                    $insert_invoice_preference['FKPreferenceID'] = $val['FKPreferenceID'];
                                    $insert_invoice_preference['ParentTitle'] = $val['ParentTitle'];
                                    $insert_invoice_preference['Title'] = $val['Title'];
                                    $insert_invoice_preference['Price'] = $val['Price'];
                                    $insert_invoice_preference['Total'] = $val['Total'];
                                    $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                }
                            }
                        }
                    }
                    $stripe_grand_total = $GrandTotal_old;
                    $is_new_invoice_create = true;
                    if ($stripe_grand_total > 0) {
                        $merchant_settings = array(
                            //'api_key' => 'sk_test_7enLa8QuWrL8ZMO3nEL8ADnZ'
                            'api_key' => $this->GetStripeAPIKey()
                        );
                        $this->merchant->initialize($merchant_settings);
                        $response = $this->merchant->purchase(array('token' => $purchasecard, 'amount' => $stripe_grand_total, 'currency' => $this->config->item("currency_code"), 'description' => 'Invoice #' . $invoice_number));

                        $stripe_information = array(
                            'InvoiceNumber' => $invoice_number,
                            'Content' => $response->data(),
                            'CreatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->InsertRecord($this->tbl_payment_informations, $stripe_information);

                        if ($response->status() == Merchant_response::COMPLETE) {
                            // mark order as complete
                            $invoice_update = array(
                                'OrderStatus' => "Processed",
                                'PaymentStatus' => "Completed",
                                'PaymentMethod' => 'Stripe',
                                'ID' => $invoice_id
                            );
                            $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_update, 'PKInvoiceID');

                            $insert_invoice_payment_information = array(
                                'FKInvoiceID' => $invoice_id,
                                'FKCardID' => $val['FKCardID'],
                                'Amount' => $stripe_grand_total,
                                'PaymentReference' => $response->reference(),
                                'PaymentToken' => json_encode($purchasecard),
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                            if ($discount_cart == "null") {
                                $loyalty_points = intval($grand_total);
                                $minus_loyalty_points = 0;
                                $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_id));
                                $loyalty_data = array(
                                    'FKMemberID' => $member_id,
                                    'FKInvoiceID' => $invoice_id,
                                    'InvoiceNumber' => $invoice_number,
                                    'GrandTotal' => $grand_total,
                                    'Points' => $loyalty_points
                                );
                                if ($invoice_loyalty_record != false) {
                                    $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                    $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                    $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                    $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                } else {
                                    $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                    $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                }
                                $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                $total_loyalty_points = intval($member_detail['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                $used_loyalty_points = intval($member_detail['UsedLoyaltyPoints']);
                                $total_loyalty_points += $loyalty_points;
                                $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                    $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);

                                    for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                        $loyalty_code = $this->RandomPassword(6);
                                        $insert_discount = array(
                                            'FKMemberID' => $member_detail['PKMemberID'],
                                            'Code' => $loyalty_code,
                                            'Worth' => $this->GetLoyaltyCodeAmount(),
                                            'DType' => "Price",
                                            'DiscountFor' => "Loyalty",
                                            'Status' => "Active",
                                            'CodeUsed' => "One Time",
                                            'CreatedBy' => 0,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                        $used_loyalty_points += $minimum_loyalty_points;
                                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                        if ($sms_responder_record != false) {
                                            $sms_field_array = array(
                                                'Loyalty Points' => $remain_loyalty_points,
                                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                'Currency' => $this->config->item("currency_symbol"),
                                                'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                'Discount Code' => $loyalty_code
                                            );
                                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                            $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                                        }
                                    }
                                }
                                $update_member = array(
                                    'TotalLoyaltyPoints' => $total_loyalty_points,
                                    'UsedLoyaltyPoints' => $used_loyalty_points,
                                    'ID' => $member_detail['PKMemberID']
                                );
                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_detail['PKMemberID']));
                                if ($member_record !== false) {
                                    $this->AddSessionItem('member_love_2_laundry_detail', $member_record);
                                }
                            } else {
                                $is_referral_discount_create = true;
                                if ($invoice_record != false) {
                                    if ($invoice_record["PaymentStatus"] != "Pending") {
                                        $is_referral_discount_create = false;
                                    }
                                }
                                if ($is_referral_discount_create) {

                                    if ($discount_cart_array[0] != "Discount") {
                                        $insert_discount = array(
                                            'FKMemberID' => $member_detail['PKMemberID'],
                                            'Code' => $this->RandomPassword(6),
                                            'Worth' => $discount_cart_array[2],
                                            'DType' => $discount_cart_array[3],
                                            'DiscountFor' => "Referral",
                                            'Status' => "Active",
                                            'CodeUsed' => "One Time",
                                            'CreatedBy' => 0,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                    }
                                }
                            }
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,InvoiceType,GrandTotal", array('InvoiceNumber' => $invoice_number));
                            if ($invoice_record != false) {
                                if ($invoice_record['InvoiceType'] == "Items") {
                                    $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Price,Quantity", array('FKInvoiceID' => $invoice_record['PKInvoiceID']));
                                }
                                $this->AddSessionItem("Google_ECommerce_Invoice_Record", $invoice_record);
                            }
                            $this->AddSessionItem("Invoice_Success", true);
                            $this->AddSessionItem("Invoice_FB_Total", $stripe_grand_total);
                            $result = "success||" . base_url('thank-you');
                        } else {
                            $is_new_invoice_create = false;
                            $this->AddSessionItem("invoice_type", "edit");
                            $this->AddSessionItem("member_invoice_id", $invoice_id);
                            $result = "error||" . $response->message();
                        }
                    } else {
                        $this->AddSessionItem("Invoice_Success", true);
                        //$result = "success||" . base_url('thank-you');
                    }

                    //$TimeSlot = $this->getTimeSlot($val['PostalCode']);

                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
                    if ($invoice_record != false) {
                        $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone", array('PKMemberID' => $invoice_record['FKMemberID']));
                        $member_address = '';
                        if ($invoice_record['Location'] != "Add" && $invoice_record['Location'] != "0" && $invoice_record['Location'] != null) {
                            $member_address = $invoice_record['Location'] . ',' . $invoice_record['Locker'];
                        } else {
                            $member_address = $invoice_record['BuildingName'] . ', ' . $invoice_record['StreetName'] . ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];
                        }
                        $pick_time_array = explode("-", $invoice_record['PickupTime']);
                        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);

                        $tookan_array = array(
                            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                            'order_id' => $invoice_record['InvoiceNumber'],
                            'team_id' => "21339",
                            'auto_assignment' => 0,
                            'job_description' => $invoice_record['OrderNotes'],
                            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                            'job_pickup_email' => $member_record['EmailAddress'],
                            'job_pickup_address' => $member_address,
                            'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                            'customer_email' => $member_record['EmailAddress'],
                            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                            'customer_address' => $member_address,
                            'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                            'has_pickup' => "1",
                            'has_delivery' => "1",
                            'layout_type' => "0",
                            'timezone' => "0",
                            'custom_field_template' => "",
                            'meta_data' => array(),
                            'tracking_link' => "1",
                            'geofence' => 1
                        );

                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                        $update_invoice_record = array();
                        $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
                        $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
                        if (isset($tooken_response_array['data']['job_id'])) {
                            $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];

                            $tookan_response_result = "";
                            if ($invoice_record['PaymentStatus'] == "Completed") {
                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                            } else {
                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                $update_invoice_record['TookanResponse'] = null;
                            }
                            $update_invoice_record['TookanUpdateResponse'] = $tookan_response_result;
                        } else {
                            if ($invoice_record['PaymentStatus'] == "Completed") {
                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/create_task", json_encode($tookan_array));
                                $update_invoice_record['TookanResponse'] = $tookan_response_result;
                            }
                        }
                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");

                        $this->SendInvoiceEmail($invoice_number, "C");
                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));
                        if ($sms_responder_record != false) {
                            $sms_field_array = array(
                                'Invoice Number' => $invoice_number,
                                'Loyalty Points' => $member_detail['TotalLoyaltyPoints'] - $member_detail['UsedLoyaltyPoints'],
                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                'Currency' => $this->config->item("currency_symbol"),
                                'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                'Pick Date' => date('d M Y', strtotime($pick_date)),
                                'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                            );
                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                            $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                        }
                    }
                } else {
                    echo 'No Invoice generated<br>';
                }
            }
        } else {
            echo 'Invoices not found!';
        }
    }
}
