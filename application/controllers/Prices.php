<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prices extends Base_Controller {
    
    function __construct() {
        parent::__construct();

        //$this->load->library('loqate');
        $this->load->model('cart');
        $this->load->model('cartservices');
        $this->load->helper('common');
       
    }

    function index() {
        $data = array();
        $data["postal_code_type"] = $this->config->item("postal_code_type");
        $this->show_view_with_menu('prices/index', $data);
    }

    public function continueorder() {

        $post = $this->input->post(NULL, TRUE);

        $response["error"] = false;
        $response["errors"] = [];
        $header = 400;
        $currency_symbol = $this->config->item("currency_symbol");
        $franchise = $this->_GetFranchiseRecord($post["id"]);
        $data["postal_code_type"] = $this->config->item("postal_code_type");
        
        if ($franchise == false) {
            $response["error"] = true;
            $response["errors"]["minimum_orders"] = $data["postal_code_type"]["validation_message"];
        }

        if (empty($post["services"])) {
            $response["error"] = true;
            $response["errors"]["services"] = "Please add services";
        }

        if ($post["amount"] < $franchise["MinimumOrderAmount"]) {
            $response["error"] = true;
            $response["errors"]["minimum_orders"] = "Minimum order allowed is " . $currency_symbol . $franchise["MinimumOrderAmount"] . ".";
        }
        //d($response,1);

        if ($response["error"] == false && empty($response["errors"])) {


            $this->RemoveSessionItems("discount_id");
            $this->RemoveSessionItems("order_preferences");

            $franchise_id = $post["id"];

            $is_login = $this->IsMemberLogin();
            $this->AddSessionItem($this->postal_code, $post['postal_code']);

            $member_id = NULL;

            if ($is_login) {
                $member_id = $this->GetCurrentMemberID();
            }

            $this->cart->delete(array("session_id" => session_id()));

            $cartData = array(
                "session_id" => session_id(),
                "member_id" => $member_id,
                "postal_code" => $post["postal_code"],
                "franchise_id" => $franchise_id,
            );
            $cartData['type'] = "items";
            $cart_id = $this->cart->insert($cartData);

            $this->RemoveSessionItems($this->session_post_code);
            $this->RemoveSessionItems("cart_id");
            unset($response["errors"]);
            $response["cart_id"] = $cart_id;
            $this->AddSessionItem("cart_id", $cart_id);
            $this->AddSessionItem($this->franchise_id, $franchise_id);
            $this->AddSessionItem($this->session_post_code, $post['postal_code']);

            foreach ($post["services"] as $key => $service) {
                $cartData = array();

                $rec = $this->model_database->GetRecord($this->tbl_franchise_services, "PKFranchiseServiceID,Title,FKServiceID,Title,Price,DiscountPercentage,FKFranchiseID", array('FKServiceID' => $key, 'FKFranchiseID' => $franchise_id));


                if (isset($rec['DiscountPercentage']) && $rec['DiscountPercentage'] > 0) {
                    $actualPrice = $rec['Price'];
                    $DiscountPercentage = $rec['DiscountPercentage'];
                    $tmpPrice = ($DiscountPercentage / 100) * $actualPrice;
                    $finalPrice = $actualPrice - $tmpPrice;
                } else {
                    $finalPrice = $rec['Price'];
                }

                $finalPrice = number_format($finalPrice, 2);

                $cartData['cart_id'] = $response["cart_id"];
                $cartData['desktop_image'] = $service["desktop_image"];
                $cartData['mobile_image'] = $service["mobile_image"];
                $cartData['package'] = $service["package"];
                $cartData['preference'] = $service["preferences"];
                $cartData['category_id'] = $service["category_id"];
                $cartData['category'] = $service["category"];
                $cartData['service_id'] = $key;
                $cartData['franchise_service_id'] = $rec["PKFranchiseServiceID"];
                $cartData['title'] = $rec["Title"];
                $cartData['discount'] = $rec["DiscountPercentage"];

                $cartData['price'] = $finalPrice;
                $cartData['quantity'] = $service["qty"];
                $cartData['total'] = $finalPrice * $cartData['quantity'];
                $cartData['total'] = numberformat($cartData['total']);

                $this->cartservices->insert($cartData);
            }
            $header = 200;
        }
        echo jsonencode($response, $header);
    }

    public function showprices() {
        $this->_URLChecker();

        $respose["error"] = false;
        $franchise_id = $_GET["franchise_id"];
        $cart_id = $this->GetSessionItem("cart_id");
        $services = array();
        foreach ($cartServices as $c) {
            $services[$c->service_id] = $c->quantity;
        }
        $data['services'] = $services;
        $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');

        if (sizeof($data['category_records']) > 0) {
            foreach ($data['category_records'] as $key => $value) {
                $service_records = $this->model_database->FranchiseCustomServicesSelectItems($franchise_id, $value['PKCategoryID']);
                if (sizeof($service_records) > 0) {
                    $data['category_records'][$key]['service_records'] = $service_records;
                } else {
                    unset($data['category_records'][$key]);
                }
            }
        }

        //d($service_records,1);
        if (sizeof($data['category_records']) == 0) {

            $respose["error"] = true;
        } else {
            $data['is_order_page'] = true;
            $data['preference_records'] = $this->_GetPreferences();
            if (sizeof($data['preference_records']) == 0) {
                unset($data['preference_records']);
            } else {
                $data['member_preference_records'] = $this->_GetMemberPreferences();
            }

            if (!$this->IsSessionItem("invoice_type")) {
                $this->RemoveSessionItems("invoice_type");
                $this->RemoveSessionItems("admin_invoice_id");
                $this->RemoveSessionItems("member_invoice_id");
                if ($this->IsMemberLogin() == true && $this->IsMemberLoginFromAdmin() == true) {
                    $this->AddSessionItem("is_love_2_laundry_admin_member_logged_in", false);
                }
            }

            $this->load->view('prices/services', $data);
        }
    }

}
