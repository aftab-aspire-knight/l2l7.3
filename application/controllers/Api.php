<?php

/*
 * ALTER TABLE `ws_invoices` CHANGE `PaymentMethod` `PaymentMethod` ENUM('Stripe','World Pay','Cash','CCNow','Paypal','Googlepay','Applepay') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Cash';

  ALTER TABLE `carts` ADD `location` VARCHAR(100) NULL AFTER `city`;
  ALTER TABLE `ws_invoices` ADD `HasAddress` INT(2) NOT NULL DEFAULT '1' AFTER `Locker`;
 * ALTER TABLE `ws_invoices` ADD `OnfleetResponse` TEXT NULL AFTER `TookanUpdateResponse`, ADD `OnfleetUpdateResponse` TEXT NULL AFTER `OnfleetResponse`;


 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->is_test = false;
        $this->load->library('stripe');
        //$this->load->library('loqate');


    }

    function index()
    {
        $this->load->library('tracker');
        $analyticId = $this->config->item("google_analytics_id");
        $tracker = new Tracker();
        $tracker->setup(/* web property id */$analyticId, /* client id */ null, /* user id */ null);


        $tracker->send(/* hit type */'event', /* hit properties */ array(
            'eventCategory' => 'test events '. date("Y-m-d H:i:s"),
            'eventAction' => 'testing',
            'eventLabel' => '(test) Aftab ' . SERVER
        ));
        //error_reporting(1);
        $grandTotal =  (float) numberformat("40.29");
        $transaction_id = 111111;
        $tracker->send('transaction', array(
            'transactionId' => $transaction_id,
            'transactionAffiliation' => "Love2Laundry",
            'transactionRevenue' => $grandTotal, // not including tax or shipping
            // 'transactionShipping' => =,
            // 'transactionTax' => $total_tax
        ));

        // Send an item record related to the preceding transaction
        $tracker->send('item', array(
            'transactionId' => $transaction_id,
            'itemName' => "test",
            'itemCode' => "123",
            'itemCategory' => "category",
            'itemPrice' => 30,
            'itemQuantity' => 1
        ));

        $tracker->send('item', array(
            'transactionId' => $transaction_id,
            'itemName' => "test2",
            'itemCode' => "1234",
            'itemCategory' => "category",
            'itemPrice' => 5.0,
            'itemQuantity' => 2
        ));


        d($tracker->print(), 1);

        //  d(SERVER);
    }

    private function _update_device_information($device_id, $member_id = false, $token = "")
    {
        $update_device = array(
            'ID' => $device_id,
            'ActiveDateTime' => date('Y-m-d h:i:s')
        );
        if ($member_id) {
            $update_device["FKMemberID"] = $member_id;
        }

        if ($token != "") {
            $update_device["PushID"] = $token;
        }

        $this->model_database->UpdateRecord($this->tbl_devices_new, $update_device, 'PKDeviceID');
    }

    private function _franchise_record($id)
    {
        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID as ID,PickupLimit,DeliveryLimit,OpeningTime,ClosingTime,AllowSameTime,DeliveryOption,MinimumOrderAmount,MinimumOrderAmountLater,DeliveryDifferenceHour,PickupDifferenceHour,GapTime", array("PKFranchiseID" => $id, 'Status' => 'Enabled'));
        return $franchise_record;
    }

    private function _member_invoice_records($member_id, $order_status)
    {
        if ($order_status == "All") {
            $response_array = array();
            $invoice_pending_array = array();
            $invoice_processed_array = array();
            $invoice_canceled_array = array();
            $invoice_completed_array = array();
            $invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,GrandTotal,OrderStatus,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('FKMemberID' => $member_id), false, false, false, "PKInvoiceID");
        } else {
            $invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,GrandTotal,OrderStatus,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('FKMemberID' => $member_id, 'OrderStatus' => $order_status), false, false, false, "PKInvoiceID");
        }
        if (sizeof($invoice_records) > 0) {
            foreach ($invoice_records as $key => $value) {
                $invoice_records[$key]['CurrencyAmount'] = $value['GrandTotal'];
                unset($invoice_records[$key]['GrandTotal']);
                if (!empty($value['PickupDate']) && !empty($value['PickupDate'])) {
                    $invoice_records[$key]['PickupDateTimeFormatted'] = date('d-M, Y', strtotime($value['PickupDate'])) . ' - ' . $value['PickupTime'];
                } else {
                    $invoice_records[$key]['PickupDateTimeFormatted'] = "-";
                }
                unset($invoice_records[$key]['PickupDate']);
                unset($invoice_records[$key]['PickupTime']);
                if (!empty($value['DeliveryDate']) && !empty($value['DeliveryTime'])) {
                    $invoice_records[$key]['DeliveryDateTimeFormatted'] = date('d-M, Y', strtotime($value['DeliveryDate'])) . ' - ' . $value['DeliveryTime'];
                } else {
                    $invoice_records[$key]['DeliveryDateTimeFormatted'] = "-";
                }
                unset($invoice_records[$key]['DeliveryDate']);
                unset($invoice_records[$key]['DeliveryTime']);
                if ($order_status == "All") {
                    if ($value["OrderStatus"] == "Pending") {
                        $invoice_pending_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Processed") {
                        $invoice_processed_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Cancel") {
                        $invoice_canceled_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Completed") {
                        $invoice_completed_array[] = $invoice_records[$key];
                    }
                }
            }
        }
        if ($order_status == "All") {
            $response_array['pending_invoices'] = $invoice_pending_array;
            $response_array['processed_invoices'] = $invoice_processed_array;
            $response_array['canceled_invoices'] = $invoice_canceled_array;
            $response_array['completed_invoices'] = $invoice_completed_array;
            return $response_array;
        } else {
            return $invoice_records;
        }
    }

    private function _member_loyalty_records($member_id)
    {
        $response_array = array();
        $loyalty_active_records = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID as ID,Code,Worth,StartDate,ExpireDate,DType", "Status='Active' and FKMemberID=" . $member_id . " and DiscountFor='Loyalty'", false, false, false, "ID");
        if (sizeof($loyalty_active_records) > 0) {
            foreach ($loyalty_active_records as $key => $value) {
                $is_valid = true;
                if (!empty($value['StartDate'])) {
                    if (strtotime($value['StartDate']) < strtotime(date('Y-m-d'))) {
                        $is_valid = false;
                    }
                }
                if (!empty($value['ExpireDate'])) {
                    if (strtotime($value['ExpireDate']) > strtotime(date('Y-m-d'))) {
                        $update_discount = array(
                            'ID' => $value['ID'],
                            'Status' => 'Expire',
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                        $is_valid = false;
                    }
                }
                if ($is_valid) {
                    if ($value['DType'] == "Percentage") {
                        $loyalty_active_records[$key]['WorthValue'] = $value['Worth'] . '%';
                    } else {
                        $loyalty_active_records[$key]['WorthValue'] = '£' . $value['Worth'];
                    }
                    unset($loyalty_active_records[$key]['Worth']);
                    unset($loyalty_active_records[$key]['StartDate']);
                    unset($loyalty_active_records[$key]['ExpireDate']);
                    unset($loyalty_active_records[$key]['DType']);
                } else {
                    unset($loyalty_active_records[$key]);
                }
            }
        }
        $response_array["loyalty_active"] = $loyalty_active_records;
        $loyalty_history_records = $this->model_database->GetRecords($this->tbl_loyalty_points, "R", "PKLoyaltyID as ID,InvoiceNumber,GrandTotal,Points,CreatedDateTime", array('FKMemberID' => $member_id), false, false, false, "ID");
        if (sizeof($loyalty_history_records) > 0) {
            foreach ($loyalty_history_records as $key => $value) {
                $loyalty_history_records[$key]['CurrencyAmount'] = $value['GrandTotal'];
                unset($loyalty_history_records[$key]['GrandTotal']);
                $loyalty_history_records[$key]['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($value['CreatedDateTime'])) . ' ' . date('H:i', strtotime($value['CreatedDateTime']));
                unset($loyalty_history_records[$key]['CreatedDateTime']);
            }
        }
        $response_array["loyalty_history"] = $loyalty_history_records;
        return $response_array;
    }

    private function _member_discount_records($member_id)
    {
        $response_array = array();
        $member_discount_active_records = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID as ID,Code,Worth,StartDate,ExpireDate,DType", "FKMemberID=" . $member_id . " and Status='Active' and (DiscountFor='Discount' or DiscountFor='Referral')", false, false, false, "ID");
        if (sizeof($member_discount_active_records) > 0) {
            foreach ($member_discount_active_records as $key => $value) {
                $is_valid = true;
                if (!empty($value['StartDate'])) {
                    if (strtotime($value['StartDate']) < strtotime(date('Y-m-d'))) {
                        $is_valid = false;
                    }
                }
                if (!empty($value['ExpireDate'])) {
                    if (strtotime($value['ExpireDate']) > strtotime(date('Y-m-d'))) {
                        $update_discount = array(
                            'ID' => $value['ID'],
                            'Status' => 'Expire',
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                        $is_valid = false;
                    }
                }
                if ($value['Worth'] == "") {
                    $is_valid = false;
                }
                if ($is_valid == true) {
                    if ($value['DType'] == "Percentage") {
                        $member_discount_active_records[$key]['WorthValue'] = $value['Worth'] . '%';
                    } else {
                        $member_discount_active_records[$key]['WorthValue'] = '£' . $value['Worth'];
                    }
                    unset($member_discount_active_records[$key]['Worth']);
                    unset($member_discount_active_records[$key]['StartDate']);
                    unset($member_discount_active_records[$key]['ExpireDate']);
                    unset($member_discount_active_records[$key]['DType']);
                } else {
                    unset($member_discount_active_records[$key]);
                }
            }
        }
        $response_array["discount_active"] = $member_discount_active_records;
        $member_discount_history_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,DiscountCode,DiscountWorth,DType,GrandTotal,CreatedDateTime", "FKMemberID=" . $member_id . " and FKDiscountID != '' and FKDiscountID !='0' and FKDiscountID is not null", false, false, false, "PKInvoiceID");
        if (sizeof($member_discount_history_records) > 0) {
            foreach ($member_discount_history_records as $key => $value) {
                $member_discount_history_records[$key]['CurrencyAmount'] = $value['GrandTotal'];
                unset($member_discount_history_records[$key]['GrandTotal']);
                $member_discount_history_records[$key]['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($value['CreatedDateTime'])) . ' ' . date('H:i', strtotime($value['CreatedDateTime']));
                unset($member_discount_history_records[$key]['CreatedDateTime']);
                if ($value['DType'] == "Percentage") {
                    $member_discount_history_records[$key]['DiscountWorthValue'] = $value['DiscountWorth'] . '%';
                } else {
                    $member_discount_history_records[$key]['DiscountWorthValue'] = '£' . $value['DiscountWorth'];
                }
                unset($member_discount_history_records[$key]['DiscountWorth']);
                unset($member_discount_history_records[$key]['DType']);
            }
        }
        $response_array["discount_history"] = $member_discount_history_records;
        return $response_array;
    }

    private function _member_card_records($member_id)
    {
        $card_records = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as ID,Title,Name,Number,Year as ExpireYear,Month as ExpireMonth,Code", array('FKMemberID' => $member_id), false, false, false, "PKCardID");
        if (sizeof($card_records) > 0) {
            $i = 0;
            $cards = array();
            foreach ($card_records as $key => $value) {

                if (strlen($value['Number']) > 4) {
                    unset($card_records[$key]);
                } else {

                    $cards[$i]["ID"] = $value['ID'];
                    $cards[$i]["Title"] = $value['Title'];
                    $cards[$i]["Name"] = $value['Name'];
                    $cards[$i]["ExpireYear"] = $value['ExpireYear'];
                    $cards[$i]["ExpireMonth"] = $value['ExpireMonth'];
                    //$cards[$i]["Number"]=$value['Number'];
                    $cards[$i]["MaskedNumber"] = $value['Number'];
                    $cards[$i]["MaskedCode"] = $value['Code'];
                    $i++;
                }
            }
        }
        return $cards;
    }


    public function getappversion()
    {
        $response_array['result'] = "success";
        $response_array['version'] = "4.1.21";
        echo json_encode($response_array);
    }

    public function get_member_preferences($member_id)
    {

        $sql = "SELECT PKPreferenceID as ID,Title,Price,ParentPreferenceID,PriceForPackage FROM `ws_member_preferences` as wmp join
      ws_preferences as wp on wmp.FKPreferenceID=wp.PKPreferenceID where wmp.FKMemberID=" . $member_id . " and wp.Status='Enabled' order by wp.Position";
        $query = $this->db->query($sql);
        return $data = $query->result_array();
        //echo json_encode($data);
    }

    private function _member_preference_records($member_id)
    {
        return $this->model_database->GetRecords($this->tbl_member_preferences, "R", "FKPreferenceID as ID", array('FKMemberID' => $member_id));
    }

    private function _member_data_record($member_id)
    {
        $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID as ID,FKWebsiteID as WebsiteID,FKFranchiseID as FranchiseID,ReferralCode,FirstName,LastName,Phone,EmailAddress,BuildingName,StreetName,PostalCode,Town,AccountNotes,RegisterFrom,TotalLoyaltyPoints,UsedLoyaltyPoints,PopShow,InvoicePendingVersion,InvoiceProcessedVersion,InvoiceCancelVersion,InvoiceCompletedVersion,LoyaltyActiveVersion,LoyaltyHistoryVersion,DiscountActiveVersion,DiscountHistoryVersion,MemberCardVersion,MemberPreferenceVersion,MemberInformationVersion,CountryCode,StripeCustomerID", array('PKMemberID' => $member_id));
        if ($member_record != false) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_record['ID']));
            if ($invoice_record != false) {
                $member_record['ReferralCodeUsed'] = "Yes";
            } else {
                $member_record['ReferralCodeUsed'] = "No";
            }
            $member_record['RemainingLoyaltyPoints'] = intval($member_record['TotalLoyaltyPoints']) - intval($member_record['UsedLoyaltyPoints']);
        }
        return $member_record;
    }

    private function _member_login_data_record($member_id, $device_id)
    {
        $response_array = array();
        $member_record = $this->_member_data_record($member_id);
        if ($member_record != false) {
            $response_array["result"] = "Success";
            //$response_array = array_merge($response_array, $this->_member_invoice_records($member_record["ID"], "All"), $this->_member_loyalty_records($member_record["ID"]), $this->_member_discount_records($member_record["ID"]));
            $response_array['member'] = $member_record;
            //$response_array['preferences'] = $this->_member_preference_records($member_record["ID"]);
            //$response_array['cards'] = $this->_member_card_records($member_record["ID"]);
            $this->_update_device_information($device_id, $member_id);
        }
        return $response_array;
    }

    private function _category_records($FKFranchiseID = 0)
    {
        $category_records = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID as ID,MobileTitle as MTitle,MobileIcon,MobileImageName', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');

        /* check if franchise id is available */
        $franchise_id = 0;
        if (isset($FKFranchiseID) && ($FKFranchiseID) > 0) {
            $franchise_id = $FKFranchiseID;
        }

        $categories = array();
        $base_url = base_url();
        $base_url = str_replace("lovetolaundrydev/", "", $base_url);

        if (sizeof($category_records) > 0) {

            $i = 0;
            foreach ($category_records as $key => $value) {
                $categories[$i]['ID'] = $value['ID'];
                $categories[$i]['MTitle'] = strtoupper($value['MTitle']);
                $categories[$i]['MobileIcon'] = $value['MobileIcon'];
                $categories[$i]['CategoryImageName'] = $value['MobileImageName'];
                $mobile_image_name_path = $base_url . $this->config->item("front_asset_image_folder_url") . "logo-black.png";

                if ($categories[$i]['CategoryImageName'] != "") {
                    $mobile_image_name = $categories[$i]['CategoryImageName'];
                    $mobile_image_name_path = $base_url . ltrim($this->config->item("front_category_folder_url"), "/") . $mobile_image_name;
                }

                $categories[$i]['CategoryImagePath'] = $mobile_image_name_path;
                $categories[$i]['service_records'] = array();
                $service_records = $this->model_database->FranchiseCustomServicesSelectItemsApi($franchise_id, $value['ID']);


                if (sizeof($service_records) > 0) {


                    foreach ($service_records as $key1 => $value1) {
                        /* Calculate discount percentage */
                        if (isset($value1['DiscountPercentage']) && $value1['DiscountPercentage'] > 0) {
                            $actualPrice = $value1['Price'];
                            $DiscountPercentage = $value1['DiscountPercentage'];
                            $tmpPrice = ($DiscountPercentage / 100) * $actualPrice;
                            $finalPrice = $actualPrice - $tmpPrice;
                        } else {
                            $finalPrice = $value1['Price'];
                        }

                        $service_records[$key1]['Content'] = str_replace("\n", "", RemoveHtmlTags($value1['Content']));
                        $service_records[$key1]['TitleUpper'] = strtoupper($value1['Title']);
                        $service_records[$key1]['CurrencyAmount'] = $value1['Price'];
                        /* send OfferPrice and DiscountPercentage two new values */
                        $service_records[$key1]['OfferPrice'] = str_replace(",", "", number_format($finalPrice, 2));
                        $service_records[$key1]['DiscountPercentage'] = (isset($value1['DiscountPercentage']) && $value1['DiscountPercentage'] > 0) ? $value1['DiscountPercentage'] : 0;
                        $desktop_image_name = "";
                        $desktop_image_name_path = "";
                        $mobile_image_name = "";
                        $mobile_image_name_path = "";


                        if (!empty($value1['DesktopImage'])) {
                            if (file_exists($this->config->item("dir_upload_service") . $value1['DesktopImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value1['DesktopImage'])) {
                                $desktop_image_name = $value1['DesktopImage'];
                                $desktop_image_name_path = $base_url . $this->config->item("front_service_folder_url") . $value1['DesktopImage'];
                            }
                        }
                        if (!empty($value1['MobileImage'])) {
                            if (file_exists($this->config->item("dir_upload_service") . $value1['MobileImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value1['MobileImage'])) {
                                $mobile_image_name = $value1['MobileImage'];
                                $mobile_image_name_path = $base_url . $this->config->item("front_service_folder_url") . $value1['MobileImage'];
                            }
                        }
                        $service_records[$key1]['DesktopImage'] = $desktop_image_name;
                        $service_records[$key1]['MobileImage'] = $mobile_image_name;
                        $service_records[$key1]['DesktopImagePath'] = $desktop_image_name_path;
                        $service_records[$key1]['MobileImagePath'] = $mobile_image_name_path;
                    }
                    $categories[$i]['service_records'] = $service_records;
                    //$category_records[$key]['service_records'] = $service_records;
                } else {
                    unset($category_records[$key]);
                }
                $i++;
            }
        }
        return $categories;
    }

    private function _preferences_records()
    {
        $preference_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title', array('Status' => 'Enabled', 'ParentPreferenceID' => 0), false, false, false, 'Position', 'asc');
        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {
                $preference_child_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title,Price,PriceForPackage', array('Status' => 'Enabled', 'ParentPreferenceID' => $value['ID']), false, false, false, 'Price', 'asc');
                if (sizeof($preference_child_records) > 0) {
                    $preference_records[$key]['child_records'] = $preference_child_records;
                } else {
                    unset($preference_records[$key]);
                }
            }
        }
        //d($preference_records,1);
        return $preference_records;
    }

    private function _location_records()
    {
        $location_records = $this->model_database->GetRecords($this->tbl_locations, "R", "PKLocationID as ID,Title,PostalCode", array("Status" => 'Enabled'));
        if (sizeof($location_records) > 0) {
            foreach ($location_records as $key => $value) {
                $locker_records = $this->model_database->GetRecords($this->tbl_lockers, "R", "Title", array("FKLocationID" => $value['ID']));
                if (sizeof($locker_records) > 0) {
                    $location_records[$key]['lockers'] = $locker_records;
                } else {
                    unset($location_records[$key]);
                }
            }
        }
        return $location_records;
    }

    private function _setting_records($MinimumOrderAmount = 15)
    {
        // Minimum order amount with franchise
        $setting_records = array();
        $setting_records[] = array(
            'ID' => 11,
            'Title' => 'Minimum Order Amount Later',
            'Content' => $MinimumOrderAmount
            //'Content' => $this->GetOrderAmountLater()
        );
        $setting_records[] = array(
            'ID' => 16,
            'Title' => 'Website Email Receivers',
            'Content' => $this->GetWebsiteEmailReceivers()
        );
        $setting_records[] = array(
            'ID' => 17,
            'Title' => 'Website Name',
            'Content' => $this->GetWebsiteName()
        );
        $setting_records[] = array(
            'ID' => 24,
            'Title' => 'Google Play Store Link URL',
            'Content' => $this->GetGooglePlayLink()
        );
        $setting_records[] = array(
            'ID' => 25,
            'Title' => 'Apple Store Link URL',
            'Content' => $this->GetAppleStoreLink()
        );
        $setting_records[] = array(
            'ID' => 26,
            'Title' => 'Referral Code Amount',
            'Content' => $this->GetReferralCodeAmount()
        );
        $setting_records[] = array(
            'ID' => 29,
            'Title' => 'Loyalty Code Amount',
            'Content' => $this->GetLoyaltyCodeAmount()
        );
        $setting_records[] = array(
            'ID' => 30,
            'Title' => 'Minimum Loyalty Points',
            'Content' => $this->GetMinimumLoyaltyPoints()
        );
        $setting_records[] = array(
            'ID' => 31,
            'Title' => 'Minimum Order Amount',
            'Content' => $MinimumOrderAmount
            //'Content' => $this->GetOrderAmount()
        );
        $setting_records[] = array(
            'ID' => 38,
            'Title' => 'App Faq Page Link',
            'Content' => $this->GetSettingRecord("App Faq Page Link")['Content']
        );
        $setting_records[] = array(
            'ID' => 39,
            'Title' => 'App Term Condition Page Link',
            'Content' => $this->GetSettingRecord("App Term Condition Page Link")['Content']
        );
        return $setting_records;
    }

    public function get_credit_cards($member_id)
    {

        $response_array['cards'] = $this->_member_card_records($member_id);
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function get_checkout_data($member_id)
    {

        $response_array['result'] = "success";
        $post = $this->input->get(NULL, TRUE);

        $discount_id = isset($post["discount_id"]) ? $post["discount_id"] : "";
        $discount_type = isset($post["discount_type"]) ? $post["discount_type"] : "";

        $response_array['payment_methods'] = $this->config->item("payment_methods");
        $response_array['cards'] = array();
        $response_array['member'] = array();
        $cards = $this->_member_card_records($member_id);

        if (is_numeric($discount_id)) {
            $discount = $this->validateDiscount($member_id, $discount_id, $discount_type);
        } else {
            $discount["error"] = false;
            $discount["message"] = "no discount code applied";
        }
        $response_array['discount'] = $discount;



        if (!empty($cards)) {
            $response_array['cards'] = $cards;
        }

        if ($member != false) {
            $response_array['member'] = $this->_member_data_record($member_id);
        }


        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function validateDiscount($member_id, $discount_id, $discount_type)
    {

        $response["error"] = false;
        $response["message"] = "";

        if ($discount_type == "referral") {
            $discount_id = $discount_id;
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
            $member_detail = $member_record;
            $member_referral_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID,ReferralCode,Status", "PKMemberID='" . $discount_id . "' and Status='Enabled'");

            if ($member_referral_record != false) {

                $response["discount"] = "success||Referral|" . $member_referral_record['PKMemberID'] . '|' . $this->GetReferralCodeAmount() . '|Price|' . $member_referral_record["ReferralCode"];

                $response["id"] = $member_referral_record['PKMemberID'];
                $response["worth"] = $this->GetReferralCodeAmount();
                $response["dtype"] = "Price";
                $response["Code"] = $member_referral_record["ReferralCode"];
                $response["discount_type"] = $discount_type;
                $response["error"] = true;
                $header = 200;
            } else {
                $response["error"] = true;
                $response["message"] = "Referral code incorrect.";
            }
        } elseif ($discount_type == "discount") {
            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('PKDiscountID' => $discount_id, 'Status' => 'Active'));

            if ($discount_record != false) {

                if (!empty($discount_record['FKMemberID'])) {
                    if ($discount_record['FKMemberID'] != $member_id) {
                        $response["error"] = true;
                        $response["message"] = "Discount code incorrect.";
                    }
                }

                $current_date = date('Y-m-d');

                if (!empty($discount_record['StartDate'])) {
                    $start_date = date('Y-m-d', strtotime($discount_record['StartDate']));
                    if ($current_date < $start_date) {
                        $response["error"] = true;
                        $response["message"] = "Discount code is not valid";
                    }
                }

                if ($response["error"] == false) {
                    if (!empty($discount_record['ExpireDate'])) {
                        $expire_date = date('Y-m-d', strtotime($discount_record['ExpireDate']));
                        if ($current_date > $expire_date) {
                            $update_discount = array(
                                'Status' => 'Expire',
                                'ID' => $discount_record['PKDiscountID'],
                                'UpdatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                            $response["error"] = true;
                            $response["message"] = "Discount code is already expire";
                        }
                    } elseif ($discount_record['CodeUsed'] == "One Time") {
                        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'PKInvoiceID', array('FKDiscountID' => $discount_record['PKDiscountID'], 'FKMemberID' => $member_id));
                        if ($invoice_record !== false) {
                            $response["error"] = true;
                            $response["message"] = "You already used this discount code";
                        }
                    }

                    if ($response["error"] == false) {
                        $response["discount"] = "success||Discount|" . $discount_record['PKDiscountID'] . '|' . $discount_record['Worth'] . '|' . $discount_record['DType'] . '|' . $discount_record['Code'];

                        $response["id"] = $discount_record['PKDiscountID'];
                        $response["worth"] = $discount_record['Worth'];
                        $response["dtype"] = $discount_record['DType'];
                        $response["Code"] = $discount_record['Code'];
                        $header = 200;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = "You already used this discount code";
                }
            } else {
                $response["error"] = true;
                $response["message"] = "Discount code expired";
            }
        }
        return $response;
    }

    public function validategoogleaddress()
    {
        $post_data = $this->input->post(NULL, TRUE);

        $header = 400;
        $response["error"] = true;
        $response["error_type"] = "postcode";
        $data["postal_code_type"] = $this->config->item("postal_code_type");

        //d($post_data,1);

        $response = $this->country->ValidateAddress($post_data);

        $response = array_merge($post_data, $response);
        echo jsonencode($response, 200);
    }

    public function validatepostcode()
    {

        $post_data = $this->input->get(NULL, TRUE);

        $header = 400;
        $response["error"] = true;
        $response["error_type"] = "postcode";

        $data["postal_code_type"] = $this->config->item("postal_code_type");
        if (isset($post_data['postcode']) && !empty($post_data['postcode'])) {
            $is_found = true;


            if (isset($post_data["place_id"]) && $post_data["place_id"] != "") {
                $short_post_code_array = $this->country->validatePlaceId($post_data);
            } else {
                $short_post_code_array = $this->country->ValidatePostalCode($post_data['postcode']);
            }

            if ($short_post_code_array['validate'] == false) {
                $response["message"] = $data["postal_code_type"]["validation_message"];
                $is_found = false;
            } else {

                $franchise_id = 0;

                if (isset($short_post_code_array["franchise_id"])) {
                    $franchise_id = $short_post_code_array["franchise_id"];
                }

                if (isset($short_post_code_array["cordinates"])) {
                    $post_latitude_longitude = $short_post_code_array["cordinates"];
                } else {

                    $short_post_code = $short_post_code_array['prefix'];
                    $short_suffix = $short_post_code_array['suffix'];

                    $tmpPostCode = $short_post_code . $short_suffix;
                    $post_latitude_longitude = $this->country->GetLatitudeLongitude($tmpPostCode);

                    if ($post_latitude_longitude == "") {
                        $tmpPostCode = $short_post_code . ' ' . $short_suffix;
                        $post_latitude_longitude = $this->country->GetLatitudeLongitude($tmpPostCode);
                    }

                    if ($post_latitude_longitude == "") {

                        $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_data['postcode']);
                    }
                }

                if ($post_latitude_longitude == "") {
                    $response["error_type"] = "t";
                    $response["url"] = base_url($this->config->item('front_post_code_not_found_page_url'));
                    $is_found = false;
                } else {

                    if (strlen($post_latitude_longitude) < 5) {
                        $is_found = false;
                    } else {
                        $post_code = $post_data['postcode'];

                        if ($franchise_id == 0) {

                            if ($short_post_code_array['validate'] == false) {
                                $response["message"] = $this->config->item('validation_message');
                                $is_found = false;
                            } else {
                                $short_post_code = $short_post_code_array['prefix'];


                                $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));

                                if ($franchise_post_code_record != false) {
                                    $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                                }
                            }
                        }
                        $insert_search_post_code = array(
                            'FKFranchiseID' => $franchise_id,
                            'Code' => $post_code,
                            'IPAddress' => $this->input->ip_address(),
                            'CreatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                        if ($franchise_id != 0) {
                            $franchise_record = $this->_GetFranchiseRecord($franchise_id);
                            if ($franchise_record != false) {

                                $response["franchise_id"] = $franchise_id;
                                $response["minimum_order_amount"] = $franchise_record["MinimumOrderAmount"];
                                $response["minimum_order_amount_later"] = $franchise_record["MinimumOrderAmountLater"];
                                $response["city"] = ucfirst($this->config->item('default_postal_code'));
                            } else {
                                $is_found = false;
                            }
                        } else {
                            $is_found = false;
                        }
                    }
                }
            }
            if ($is_found) {
                $header = 200;
                $response["error"] = false;
                $response["postal_code"] = $post_code;
                unset($response["error_type"]);
            } else {
                $response["message"] = $data["postal_code_type"]['no_service_message'];
            }
        } else {
            $response["message"] = $data["postal_code_type"]['no_service_message'];
        }
        echo jsonencode($response, $header);
        die;
    }

    public function getaddresses()
    {

        $get = $this->input->get(NULL, TRUE);

        $addresses = [];
        if (!empty($get['postcode'])) {
            $short_post_code_array = $this->country->ValidatePostalCode($get['postcode']);
            if ($short_post_code_array['validate'] != false) {
                $addresses = $this->country->getAddresses($get['postcode']);
            }
        }
        header('Content-Type: application/json');  // <-- header declaration
        echo json_encode($addresses, true);    // <--- encode
    }

    public function getaddresslocation($cart_id)
    {

        $get = $this->input->get(NULL, TRUE);

        $addresses = $this->country->getAddresses($get['postcode']);
        header('Content-Type: application/json');  // <-- header declaration
        echo json_encode($addresses, true);    // <--- encode
    }

    public function get_d($id)
    {
        echo json_encode($this->_date_records($id)); // 200 being the HTTP response code
    }

    public function dashboard($member_id)
    {

        $response_array = array();
        $response_array["result"] = "Success";

        $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID as ID,FKWebsiteID as WebsiteID,FKFranchiseID as FranchiseID,FirstName,LastName,Phone,EmailAddress,BuildingName,StreetName,PostalCode,Town,TotalLoyaltyPoints,UsedLoyaltyPoints", array('PKMemberID' => $member_id));
        if ($member_record != false) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_record['ID']));
            if ($invoice_record != false) {
                $member_record['ReferralCodeUsed'] = "Yes";
            } else {
                $member_record['ReferralCodeUsed'] = "No";
            }
            $member_record['RemainingLoyaltyPoints'] = intval($member_record['TotalLoyaltyPoints']) - intval($member_record['UsedLoyaltyPoints']);

            $member_record['Pending'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Pending', 'FKMemberID' => $member_record['ID']));;
            $member_record['Processed'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Processed', 'FKMemberID' => $member_record['ID']));
            $member_record['Cancel'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Cancel', 'FKMemberID' => $member_record['ID']));
            $member_record['Completed'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Completed', 'FKMemberID' => $member_record['ID']));
            $response_array["data"] = $member_record;
        }
        $response_array = array_merge($response_array);
        echo json_encode($response_array);
    }

    public function get_discounts($member_id)
    {

        $response_array = array();
        $response_array["result"] = "Success";
        $response_array['data'] = $this->_member_discount_records($member_id);
        echo json_encode($response_array);
    }

    public function get_loyalty($member_id)
    {

        $response_array = array();
        $response_array["result"] = "Success";
        $response_array['data'] = $this->_member_loyalty_records($member_id);
        echo json_encode($response_array);
    }

    public function get_invoices($member_id)
    {

        $response_array = array();
        $response_array["result"] = "Success";
        $type = isset($_GET["type"]) ? $_GET["type"] : "Pending";

        if ($type == "cancelled") {
            $type = "cancel";
        }
        $response_array['data'] = $this->_member_invoice_records($member_id, $type);
        echo json_encode($response_array);
    }

    private function _date_records($id)
    {
        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;
        if ($franchise_record != false) {
            $franchise_date_array['pick'] = array();
            $franchise_date_array['delivery'] = array();
            $pick_array_count = 0;
            $delivery_array_count = 0;
            for ($i = 0; $i <= 100; $i++) {
                if ($pick_array_count <= 14) {
                    $date = date($this->config->item('query_date_format'), strtotime("+" . $i . ' days'));
                    $date_class = "Disable";
                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Pickup", $id) == false) {
                            $date_class = "Available";
                            $current_date = date('Y-m-d');
                            if ($current_date == $date) {
                                $current_date_after_add_3_hour = date('Y-m-d', strtotime(date('Y-m-d H:i:s', strtotime('+3 hours'))));
                                if ($current_date != $current_date_after_add_3_hour) {
                                    $date_class = "Disable";
                                }
                            }
                        }
                    }
                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('d-F', strtotime($date)) . ' ' . $this->GetFullDayName(date('w', strtotime($date))),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['pick'][] = $date_array;
                    $pick_array_count += 1;
                }
                if ($delivery_array_count <= 20 && $pick_array_count > 0) {
                    $date = date($this->config->item('query_date_format'), strtotime("+" . ($i + 1) . ' days'));
                    $date_class = "Disable";
                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Delivery", $id) == false) {
                            $date_class = "Available";
                        }
                    }
                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['delivery'][] = $date_array;
                    $delivery_array_count += 1;
                }
                if ($pick_array_count == 14 && $delivery_array_count == 20) {
                    break;
                }
            }
        }

        return $franchise_date_array;
    }

    private function time_records($franchise_record, $date, $search_type = "Pickup", $pick_date, $pick_time_hour = "")
    {

        $hours = array();
        if ($franchise_record != false) {

            $is_valid = true;
            $opening_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['OpeningTime']));
            $closing_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['ClosingTime']));
            $total_hours = ((strtotime($closing_franchise_time) - strtotime($opening_franchise_time)) / (60 * 60));
            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $franchise_record["ID"]), false, false, false, false, 'asc');


            //d($franchise_record["ID"],1);
            //d($franchise_record);
            if ($search_type == "Delivery") {
                $differenceHour = $franchise_record['DeliveryDifferenceHour'];
            } else {
                $differenceHour = $franchise_record['PickupDifferenceHour'];
            }

            $return_response = $this->GetFranchiseDateTimeCollectionUpdated($date, $total_hours, $search_type, $franchise_record, $opening_franchise_time, $franchise_timings_record);

            if ($search_type == "Delivery") {
                //d($return_response,1);
                $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ ' . $differenceHour . ' hours', strtotime($pick_date . " $pick_time_hour:00"))); // $now + 3 hours

                $p_date_hour = date($this->config->item('query_date_format'), strtotime($pick_date)) . " " . "$pick_time_hour:00:00";

                $d_date_hour = date($this->config->item('query_date_format'), strtotime($date)) . " " . "$pick_time_hour:00:00";

                $diff = strtotime($d_date_hour) - strtotime($p_date_hour);
                $h = $diff / (60 * 60);

                $pickupDay = date("l", strtotime($available_delivery_date));
                $deliveryDay = date("l", strtotime($date));
                if ($deliveryDay == "Monday") {

                    if ($franchise_record["DeliveryOption"] != "None") {

                        $i = 0;
                        foreach ($return_response["Times"] as $time) {

                            if ($time["Hour"] < 15) {
                                $return_response["Times"][$i]["Class"] = "Disable";
                            }
                            $i++;
                        }
                    }
                } else {


                    $p = strtotime($pick_date . " " . "$pick_time_hour:00:00");

                    $i = 0;
                    foreach ($return_response["Times"] as $time) {

                        $d_date_hour = strtotime($date . " " . $time["Hour"] . ":00:00");

                        $diff = $d_date_hour - $p;
                        $h = $diff / (60 * 60);
                        //$h--;
                        if ($h < $franchise_record['DeliveryDifferenceHour']) {
                            $return_response["Times"][$i]["Class"] = "Disable";
                        }

                        $i++;
                    }
                }
            }
            unset($response_array["message"]);
            $response_array["result"] = "Success";
            $response_array["data"] = $return_response["Times"];
            $response_array["data"] = $return_response["Times"];
            $response_array["hours_list"] = implode(",", $return_response["Hours"]);
        }
        return $response_array;
    }

    public function get_time_records()
    {

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";


        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : '';
            $search_type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $pick_date = isset($_REQUEST['pick_date']) ? $_REQUEST['pick_date'] : '';
            $pick_time_hour = isset($_REQUEST['pick_time_hour']) ? $_REQUEST['pick_time_hour'] : '';

            $delivery_after_hour = 0;

            $franchise_record = $this->_franchise_record($id);

            $response_array = $this->time_records($franchise_record, $date, $search_type, $pick_date, $pick_time_hour);
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function get_preferences_records()
    {

        $response_array["preferences"] = array("version" => $this->GetAppPreferenceVersion(), "data" => $this->_preferences_records());
        $response_array["preferences"]["member_preferences"] = array();
        $total = 0.00;
        $selected = array();

        $sql = "SELECT PKMemberID FROM `ws_members` where PKMemberID in (select FKMemberID from ws_member_preferences ) limit 1";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $member_preferences = $this->get_member_preferences($data[0]["PKMemberID"]);
        /*
          foreach($response_array["preferences"]["data"] as $preferences){
          //d($preferences,1);
          $selected[$preferences["ID"]] = $preferences["child_records"][0];
          } */

        foreach ($member_preferences as $p) {

            if (is_numeric($p["Price"]) && $p["PriceForPackage"] == "Yes") {
                $total = $total + $p["Price"];
            }
            $total = number_format($total, 2);


            $selected[$p["ParentPreferenceID"]] = $p;
        }


        $response_array["preferences"]["member_preferences"] = $selected;
        $response_array["preferences"]["total"] = $total;

        echo json_encode($response_array);
    }

    public function get_load_data()
    {

        $base_url = base_url();

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        $postal_code_type = $this->config->item("postal_code_type");
        if ($_REQUEST) {
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $versions = isset($_REQUEST['versions']) ? $_REQUEST['versions'] : '';
            /* get post code for franchise items and more info check */
            $franchise_id = isset($_REQUEST['franchise_id']) ? $_REQUEST['franchise_id'] : '';
            $more_info = isset($_REQUEST['more_info']) ? $_REQUEST['more_info'] : '';
            /* changes ends */
            if (!empty($type) && !empty($device_id)) {
                $this->_update_device_information($device_id);

                ////////////////postcode/////
                $MinimumOrderAmount = 15;

                $FKFranchiseID = $franchise_id;
                $franchise_record = $this->_franchise_record($FKFranchiseID);

                if ($franchise_record != false) {
                    $MinimumOrderAmount = isset($franchise_record['MinimumOrderAmount']) ? $franchise_record['MinimumOrderAmount'] : 15;
                    unset($response_array["message"]);
                    $response_array["result"] = "Success";
                    if (!empty($more_info)) {
                        $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", "OpeningTime,ClosingTime", array("FKFranchiseID" => $FKFranchiseID), false, false, false, false, 'asc');
                        if (isset($franchise_timings_record) && !empty($franchise_timings_record)) {
                            $franchise_record['OtherSlots'] = $franchise_timings_record;
                        } else {
                            $franchise_record['OtherSlots'] = array();
                        }
                        $response_array["franchise_detail"] = $franchise_record;
                        $date_records = $this->_date_records($franchise_record['FKFranchiseID']);
                        if (sizeof($date_records) > 0) {
                            $response_array["pick_date_data"] = $date_records["pick"];
                            $response_array["delivery_date_data"] = $date_records["delivery"];
                        }
                    }
                } else {
                    $response_array["message"] = $postal_code_type["no_service_message"];
                }

                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                    if ($type == "load") {
                        $category_records = $this->_category_records($FKFranchiseID);


                        if (sizeof($category_records) > 0) {
                            unset($response_array["message"]);
                            $response_array["result"] = "Success";
                            $response_array["categories"] = array("version" => $this->GetAppCategoryServiceVersion(), "data" => $category_records);
                            $response_array["preferences"] = array("version" => $this->GetAppPreferenceVersion(), "data" => $this->_preferences_records());
                            $response_array["settings"] = array("version" => $this->GetAppSettingVersion(), "data" => $this->_setting_records($MinimumOrderAmount));
                        } else {
                            $response_array["message"] = $postal_code_type["no_service_message"];
                        }
                    } else if ($type == "fetch" && !empty($versions)) {
                        $version_array = explode("|", $versions);
                        if (sizeof($version_array) == 3) {
                            $category_records = $this->_category_records($FKFranchiseID);
                            if (sizeof($category_records) > 0) {
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                $category_service_version = $this->GetAppCategoryServiceVersion();
                                if ($category_service_version != $version_array[0]) {
                                    $response_array["categories"] = array("version" => $category_service_version, "data" => $category_records);
                                }
                                $preference_version = $this->GetAppPreferenceVersion();
                                if ($preference_version != $version_array[1]) {
                                    $response_array["preferences"] = array("version" => $preference_version, "data" => $this->_preferences_records());
                                }
                                $setting_version = $this->GetAppSettingVersion();
                                if ($setting_version != $version_array[2]) {
                                    $response_array["settings"] = array("version" => $setting_version, "data" => $this->_setting_records($MinimumOrderAmount));
                                }
                            } else {
                                $response_array["message"] = $postal_code_type["no_service_message"];
                            }
                        }
                    }
                } else {
                    $response_array["message"] = $postal_code_type["no_service_message"];
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function savecart()
    {

        $response = array();
        $response["error"] = true;
        $response["message"] = "Required Action : POST";
        $post = $this->input->post(NULL, TRUE);
        $header = 400;


        if (!empty($post)) {

            $device_id = isset($post['device_id']) ? $post['device_id'] : '';
            $member_id = isset($post['member_id']) ? $post['member_id'] : '';
            $postcode = isset($post['postcode']) ? $post['postcode'] : '';
            $address = isset($post['address']) ? $post['address'] : '';
            $address2 = isset($post['address2']) ? $post['address2'] : '';
            $franchise_id = isset($post['franchise_id']) ? $post['franchise_id'] : '';
            $has_address = isset($post['has_address']) ? $post['has_address'] : 1;
            $city = isset($post['city']) ? $post['city'] : "";


            $this->load->model('cart');
            $this->cart->delete(array("device_id" => $device_id));


            if ($has_address == 1) {
                $location = $this->country->GetLatitudeLongitude($address);
            } else {
                $location = $this->country->GetLatitudeLongitude($postcode);
                // $this->country->GetLatitudeLongitude($post_code);
            }

            $cartData = array(
                "device_id" => $device_id,
                "member_id" => $member_id,
                "postal_code" => $postcode,
                "franchise_id" => $franchise_id,
                "address1" => $address,
                "address2" => $address2,
                "location" => $location,
                "has_address" => $has_address,
                "city" => $city,
            );

            $cart_id = $this->cart->insert($cartData);

            $response["cart_id"] = $cart_id;
            $response["message"] = "insert to cart";

            $response["error"] = false;
            $header = 200;
        }

        echo jsonencode($response, $header);
    }

    public function get_services_data()
    {

        $base_url = base_url();
        $postal_code_type = $this->config->item("postal_code_type");
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $versions = isset($_REQUEST['versions']) ? $_REQUEST['versions'] : '';
            /* get post code for franchise items and more info check */
            $post_code = isset($_REQUEST['post_code']) ? $_REQUEST['post_code'] : '';
            $more_info = isset($_REQUEST['more_info']) ? $_REQUEST['more_info'] : '';
            $services_ids = isset($_REQUEST['services_ids']) ? $_REQUEST['services_ids'] : '';

            if (!empty($type) && !empty($device_id)) {
                ////////////////postcode/////
                $MinimumOrderAmount = 15;
                $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);
                if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) >= 5) {
                    $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                    if ($short_post_code_array['validate'] != false) {
                        $short_post_code = $short_post_code_array['prefix'];
                        $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                        if ($franchise_post_code_record != false) {
                            $FKFranchiseID = $franchise_post_code_record['FKFranchiseID'];
                            $franchise_record = $this->_franchise_record($franchise_post_code_record['FKFranchiseID']);

                            if ($franchise_record != false) {
                                $MinimumOrderAmount = isset($franchise_record['MinimumOrderAmount']) ? $franchise_record['MinimumOrderAmount'] : 15;
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                $response_array["franchise_detail"] = $franchise_record;
                            } else {
                                $response_array["message"] = $postal_code_type["no_service_message"];
                            }
                        } else {
                            $response_array["message"] = $postal_code_type["no_service_message"];
                        }
                    } else {
                        $response_array["message"] = $postal_code_type["validation_message"];
                    }
                } else {
                    $response_array["message"] = $postal_code_type["validation_message"];
                }

                ////////////////////////
                /* if franchise exist */
                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {

                    //$category_records = $this->_category_records($FKFranchiseID);
                    $response_array["result"] = "Success";
                    $response_array["services"] = array();
                    if ($services_ids != "") {
                        $sql = "select s.PKServiceID,wfs.`FKFranchiseID`,s.Price as ServicePrice,wfs.`FKFranchiseID`,wfs.Price as FranchisePrice,wfs.DiscountPercentage from " . $this->tbl_services . " as s left join ws_franchise_services as wfs on s.PKServiceID = wfs.FKServiceID  where s.PKServiceID IN (" . $services_ids . ") and wfs.`FKFranchiseID` = $FKFranchiseID";
                        $query = $this->db->query($sql);
                        if ($query->num_rows() > 0) {
                            $result = $query->result_array();

                            $i = 0;
                            $services = array();
                            foreach ($result as $row) {
                                $services[$i]["ServiceID"] = $row["PKServiceID"];

                                if (!empty($row["FranchisePrice"])) {
                                    $services[$i]["Price"] = $row["FranchisePrice"];
                                } else {
                                    $services[$i]["Price"] = $row["ServicePrice"];
                                }

                                $services[$i]["Discount"] = number_format(($services[$i]["Price"] * $row["DiscountPercentage"]) / 100, 2);
                                $services[$i]["OfferPrice"] = number_format($services[$i]["Price"], 2);
                                $services[$i]["CurrencyAmount"] = $services[$i]["Price"];
                                if ($row["DiscountPercentage"] > 0) {

                                    $offerPrice = $services[$i]["Price"] - $services[$i]["Discount"];
                                    $services[$i]["OfferPrice"] = number_format($offerPrice, 2);
                                    /// $services[$i]["OfferPrice"] = 55.41;
                                }

                                $i++;
                            }
                            $response_array["services"] = $services;
                        }
                    }
                } else {
                    $response_array["message"] = $postal_code_type["no_service_message"];
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function get_member_data()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            if (!empty($id) && !empty($device_id)) {
                unset($response_array["message"]);
                unset($response_array["result"]);
                $response_array = $this->_member_login_data_record($id, $device_id);
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function get_invoice_detail()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $member_id = isset($_REQUEST['member_id']) ? $_REQUEST['member_id'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            if (!empty($id) && !empty($member_id) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID as ID,FKMemberID as MemberID,InvoiceNumber,FKFranchiseID,DiscountType,DiscountCode,DiscountWorth,DType,InvoiceType,Location,Locker,BuildingName,StreetName,PostalCode,Town,OrderStatus,ServicesTotal,PreferenceTotal,SubTotal,DiscountTotal,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,Regularly,CreatedDateTime,AccountNotes,AdditionalInstructions,OrderNotes", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                if ($invoice_record != false) {
                    $response_array["result"] = "Success";

                    $franchise_id = $invoice_record["FKFranchiseID"];

                    $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID as ID,PickupLimit,DeliveryLimit,OpeningTime,ClosingTime,AllowSameTime,DeliveryOption,MinimumOrderAmount,MinimumOrderAmountLater,DeliveryDifferenceHour,PickupDifferenceHour,GapTime", array("PKFranchiseID" => $franchise_id));
                    if (!empty($invoice_record['BuildingName'])) {
                        $invoice_record['AddressShow'] = "Building";
                    } else {
                        $invoice_record['AddressShow'] = "Location";
                    }
                    if (empty($invoice_record['PreferenceTotal'])) {
                        $invoice_record['PreferenceTotal'] = "0.00";
                    }
                    if (empty($invoice_record['DiscountTotal'])) {
                        $invoice_record['DiscountTotal'] = "0.00";
                    }

                    $invoice_record['CurrencyAmount'] = $invoice_record['GrandTotal'];
                    unset($invoice_record['GrandTotal']);
                    $invoice_record['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($invoice_record['CreatedDateTime'])) . ' ' . date('h:i a', strtotime($invoice_record['CreatedDateTime']));
                    unset($invoice_record['CreatedDateTime']);
                    if (!empty($invoice_record['PickupDate']) && !empty($invoice_record['PickupDate'])) {
                        $invoice_record['PickUpDateTimeFormatted'] = date('d M, Y', strtotime($invoice_record['PickupDate'])) . ' - ' . $invoice_record['PickupTime'];
                    } else {
                        $invoice_record['PickUpDateTimeFormatted'] = "-";
                    }

                    $pickupTime = explode("-", $invoice_record["PickupTime"]);
                    $pickupTime = $pickupTime[0];
                    $pickupDateTime = strtotime($invoice_record['PickupDate'] . " " . $pickupTime . ":00");
                    $invoice_record['PickupDate'] . " " . $pickupTime . ":00";
                    $currentDateTime = time();
                    $diff = (int) $pickupDateTime - (int) $currentDateTime;
                    $diff = $diff / 3600;

                    $invoice_record['editable'] = false;
                    $invoice_record['can_change_delivery'] = false;

                    if ($invoice_record['OrderStatus'] != 'Cancel') {
                        if ($diff > $franchise_record["PickupDifferenceHour"]) {
                            $invoice_record['editable'] = true;
                        }

                        $deliveryTime = explode("-", $invoice_record["DeliveryTime"]);
                        $deliveryTime = $deliveryTime[0];
                        $deliveryDateTime = strtotime($invoice_record['DeliveryDate'] . " " . $deliveryTime . ":00");
                        $invoice_record['DeliveryDate'] . " " . $deliveryTime . ":00";
                        $diff = (int) $deliveryDateTime - (int) $currentDateTime;
                        $diff = $diff / 3600;

                        if ($diff > $franchise_record["DeliveryDifferenceHour"]) {
                            $invoice_record['can_change_delivery'] = true;
                        }
                    }

                    $pd = $invoice_record['PickupDate'];
                    $pt = $invoice_record['PickupTime'];
                    unset($invoice_record['PickupDate']);
                    unset($invoice_record['PickupTime']);

                    if (!empty($invoice_record['DeliveryDate']) && !empty($invoice_record['DeliveryTime'])) {
                        $invoice_record['DeliveryDateTimeFormatted'] = date('d M, Y', strtotime($invoice_record['DeliveryDate'])) . ' - ' . $invoice_record['DeliveryTime'];
                    } else {
                        $invoice_record['DeliveryDateTimeFormatted'] = "-";
                    }

                    $dd = $invoice_record['DeliveryDate'];
                    $dt = $invoice_record['DeliveryTime'];

                    unset($invoice_record['DeliveryDate']);
                    unset($invoice_record['DeliveryTime']);

                    $invoice_record['preference_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID,ParentTitle,Title,Price,Total", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoicePreferenceID");

                    $i = 0;
                    foreach ($invoice_record['preference_records'] as $p) {

                        if (trim($p["Price"]) == "") {
                            $invoice_record['preference_records'][$i]["Price"] = "0.00";
                            $invoice_record['preference_records'][$i]["Total"] = "0.00";
                        }
                        $i++;
                    }

                    $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Quantity,Price,Total,MobileImageName", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoiceServiceID");
                    $response_array["data"] = $invoice_record;
                    $response_array["data"]["PickupDate"] = $pd;
                    $response_array["data"]["PickupTime"] = $pt;
                    $response_array["data"]["DeliveryDate"] = $dd;
                    $response_array["data"]["DeliveryTime"] = $pt;

                    $response_array["data"]["franchise"] = $franchise_record;
                    $response_array["data"]["franchise_id"] = $franchise_id;
                    unset($response_array["message"]);
                } else {
                    $response_array["message"] = "Love2Laundry : Invoice not found";
                }
            }
        }
        //d($response_array, 1);
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_validate_app_version()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $version = isset($post_data['version']) ? $post_data['version'] : '';
            $platform = isset($post_data['platform']) ? $post_data['platform'] : '';
            if (!empty($version) && !empty($platform)) {
                $response_array["result"] = "Success";
                $response_array["message"] = "Latest Version";
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_register_device()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);

        $post_data = $_REQUEST;

        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $model = isset($post_data['model']) ? $post_data['model'] : '';
            $platform = isset($post_data['platform']) ? $post_data['platform'] : '';
            $version = isset($post_data['version']) ? $post_data['version'] : '';
            $token = isset($post_data['token']) ? $post_data['token'] : '';

            $device_data = array(
                'ID' => $id,
                'Model' => $model,
                'Platform' => $platform,
                'Version' => $version,
                'PushID' => $token
            );

            $device_record = $this->model_database->GetRecord($this->tbl_devices_new, "PKDeviceID", array("ID" => $id, "Model" => $model, "Platform" => $platform, "Version" => $version));

            if ($device_record != false) {

                $device_data['PKDeviceID'] = $device_record['PKDeviceID'];

                $this->_update_device_information($device_record['PKDeviceID'], false, $token);
            } else {

                $device_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                $device_data['ActiveDateTime'] = date('Y-m-d h:i:s');
                $device_data['PKDeviceID'] = $this->model_database->InsertRecord($this->tbl_devices_new, $device_data);

                unset($device_data['CreatedDateTime']);
                unset($device_data['ActiveDateTime']);
            }
            unset($response_array["message"]);
            $response_array["result"] = "Success";
            $response_array["device_detail"] = $device_data;
            $response_array["settings"] = array("version" => $this->GetAppSettingVersion(), "data" => $this->_setting_records(20));
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_verify_post_code()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $more_info = isset($post_data['more_info']) ? $post_data['more_info'] : '';
            if (!empty($post_code) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $insert_search_post_code = array(
                    'FKDeviceID' => $device_id,
                    'Code' => $post_code,
                    'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
                $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);
                if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) >= 5) {
                    $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                    if ($short_post_code_array['validate'] != false) {
                        $short_post_code = $short_post_code_array['prefix'];
                        $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                        if ($franchise_post_code_record != false) {
                            $insert_search_post_code['FKFranchiseID'] = $franchise_post_code_record['FKFranchiseID'];
                            $franchise_record = $this->_franchise_record($franchise_post_code_record['FKFranchiseID']);
                            if ($franchise_record != false) {
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                if (!empty($more_info)) {
                                    $response_array["franchise_detail"] = $franchise_record;
                                    $date_records = $this->_date_records($franchise_post_code_record['FKFranchiseID']);
                                    if (sizeof($date_records) > 0) {
                                        $response_array["pick_date_data"] = $date_records["pick"];
                                        $response_array["delivery_date_data"] = $date_records["delivery"];
                                    }
                                }
                            } else {
                                $response_array["message"] = "Sorry !!! We are not in your area yet";
                            }
                        } else {
                            $response_array["message"] = "Sorry !!! We are not in your area yet";
                        }
                    } else {
                        $response_array["message"] = "Please enter valid post code";
                    }
                } else {
                    $response_array["message"] = "Please enter valid post code";
                }
                $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_login()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);



        if ($post_data) {

            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($email_address) && !empty($password) && !empty($device_id)) {
                $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,StripeCustomerID,PKMemberID", array('EmailAddress' => $email_address, 'Password' => encrypt_decrypt('encrypt', $password), 'Status' => 'Enabled'));


                if ($member_record != false) {
                    unset($response_array["message"]);
                    unset($response_array["result"]);


                    if (empty($member_record["StripeCustomerID"])) {
                        $stripeKey = $this->GetStripeAPIKey();
                        $stripe = new Stripe();
                        $customer = $stripe->saveCustomer($stripeKey, ["email" => $member_record['EmailAddress'], "name" => $member_record['FirstName'] . " " . $member_record['LastName']]);

                        $update_member = array(
                            'ID' => $member_record['PKMemberID'],
                            'StripeCustomerID' => $customer->id
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                    }
                    $response_array = $this->_member_login_data_record($member_record['PKMemberID'], $device_id);
                } else {
                    $response_array["message"] = "Incorrect Username/Password";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_forgot_password()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($email_address) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('EmailAddress' => $email_address, 'Status' => 'Enabled'));
                if ($member_record != false) {
                    $random_password = $this->RandomPassword();
                    $update_member = array(
                        'ID' => $member_record['PKMemberID'],
                        'Password' => encrypt_decrypt("encrypt", $random_password)
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                        'Password' => $random_password,
                        'Email Address' => $member_record['EmailAddress'],
                        'Phone Number' => $member_record['Phone']
                    );
                    $this->emailsender->send_email_responder(3, $email_field_array);
                    $this->emailsender->send_email_responder(8, $email_field_array);
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Password send to your desired email address please check your inbox";
                } else {
                    $response_array["message"] = "Email Address not found in our records";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_register()
    {

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $data["postal_code_type"] = $this->config->item("postal_code_type");


        if ($post_data) {
            $first_name = isset($post_data['first_name']) ? $post_data['first_name'] : '';
            $last_name = isset($post_data['last_name']) ? $post_data['last_name'] : '';
            $phone = isset($post_data['phone']) ? $post_data['phone'] : '';
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $address1 = isset($post_data['address']) ? $post_data['address'] : '';
            $address2 = isset($post_data['address2']) ? $post_data['address2'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            $country_code = isset($post_data['country_code']) ? ltrim($post_data['country_code'], "+") : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $is_valid = true;

            if (empty($first_name)) {
                $response_array["message"] = "Please enter your first name.";
                $is_valid = false;
            } elseif (empty($last_name)) {
                $response_array["message"] = "Please enter your last name.";
                $is_valid = false;
            } elseif (empty($email_address)) {
                $response_array["message"] = "Please enter your email address.";
                $is_valid = false;
            } elseif (empty($password)) {
                $response_array["message"] = "Please enter your password.";
                $is_valid = false;
            } elseif (empty($phone)) {
                $response_array["message"] = "Please enter your phone number.";
                $is_valid = false;
            } elseif (empty($address1)) {
                $response_array["message"] = "Please enter your address.";
                $is_valid = false;
            } elseif ($this->verifyPhoneNumber($country_code, $phone) == false) {
                $response_array["message"] = "Please enter correct phone number.";
                $is_valid = false;
            } elseif (!$this->validateEmail($email_address)) {
                $response_array["message"] = "Please enter correct email address.";
                $is_valid = false;
            } elseif (empty($post_code)) {
                $response_array["message"] = "Please enter your postal code.";
                $is_valid = false;
            } elseif (empty($town)) {
                $response_array["message"] = "Please enter your town.";
                $is_valid = false;
            }



            if ($is_valid == true) {

                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('EmailAddress' => $email_address));

                if ($member_record != false) {
                    $response_array["message"] = "Email Address already exist in our records";
                    $is_valid = false;
                }

                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('Phone' => $phone, 'CountryCode' => ltrim($country_code, "+")));

                if ($member_record != false) {
                    $response_array["message"] = "Phone Number already exist in our records";
                    $is_valid = false;
                }

                $short_post_code_array = $this->country->ValidatePostalCode($post_code);

                if ($short_post_code_array['validate'] == false) {
                    $response_array["message"] = $data["postal_code_type"]["validation_message"];
                    $is_valid = false;
                }



                if ($is_valid == true) {

                    $stripeKey = $this->GetStripeAPIKey();
                    $stripe = new Stripe();
                    $customer = $stripe->saveCustomer($stripeKey, ["email" => $email_address, "name" => $first_name . " " . $last_name]);

                    $insert_member = array(
                        'FirstName' => $first_name,
                        'LastName' => $last_name,
                        'EmailAddress' => $email_address,
                        'StripeCustomerId' => $customer->id,
                        'Password' => encrypt_decrypt('encrypt', $password),
                        'CountryCode' => ltrim($country_code, "+"),
                        'Phone' => $phone,
                        'PostalCode' => $post_code,
                        'BuildingName' => $address1,
                        'StreetName' => $address2,
                        'Town' => $town,
                        'Status' => 'Enabled',
                        'RegisterFrom' => 'Mobile',
                        'ReferralCode' => $this->random_str($first_name),
                        'PopShow' => 'No',
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);
                    unset($response_array["message"]);
                    $response_array["result"] = "Success";
                    //echo jsonencode($response_array, 200);
                    //die;
                    $this->_update_device_information($device_id, $member_id);
                    $response_array['data'] = $this->_member_data_record($member_id);
                    $response_array["MemberID"] = $member_id;
                    $member_record = $response_array['data'];

                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                        'First Name' => $member_record['FirstName'],
                        'Last Name' => $member_record['LastName'],
                        'Email Address' => $member_record['EmailAddress'],
                        'Phone Number' => $member_record['Phone'],
                        'Postal Code' => $member_record['PostalCode'],
                        'Building Name' => $member_record['BuildingName'],
                        'Street Name' => $member_record['StreetName'],
                        'Town' => $member_record['Town'],
                        'Register From' => $member_record['RegisterFrom']
                    );
                    $this->emailsender->send_email_responder(1, $email_field_array);
                    $this->emailsender->send_email_responder(6, $email_field_array);

                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 11, 'Status' => 'Enabled'));
                    if ($sms_responder_record != false) {
                        $sms_field_array = array(
                            'Store Link' => $this->config->item('store_link_short_url'),
                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                            'Email Address' => $member_record['EmailAddress']
                        );
                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);

                        $phone = $country_code . ltrim($member_record['Phone'], 0);
                        $response_array["phone"] = $phone;
                        $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $phone, $msg_content);
                    }
                }
            }
        }
        echo jsonencode($response_array, 200);
    }

    public function post_account_update()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {

            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $first_name = isset($post_data['first_name']) ? $post_data['first_name'] : '';
            $last_name = isset($post_data['last_name']) ? $post_data['last_name'] : '';
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            //$referral_code = isset($post_data['referral_code']) ? $post_data['referral_code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $phone = isset($post_data['phone']) ? $post_data['phone'] : '';
            $country_code = isset($post_data['country_code']) ? ltrim($post_data['country_code'], "+") : '';

            /*
              $address = isset($post_data['address']) ? $post_data['address'] : '';
              $address2 = isset($post_data['address2']) ? $post_data['address2'] : '';
              $town = isset($post_data['town']) ? $post_data['town'] : '';
              $post_code = isset($post_data['postcode']) ? $post_data['postcode'] : '';
             */
            $is_valid = true;




            if (!empty($id) && !empty($first_name) && !empty($last_name) && !empty($phone) && !empty($email_address) && !empty($device_id)) {

                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'MemberInformationVersion', array('PKMemberID' => $id));


                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                    $is_valid = false;
                }
                $member_email_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $id, 'EmailAddress' => $email_address));
                if ($member_email_record != false) {
                    $response_array["message"] = "Email address already exist in our records";
                    $is_valid = false;
                }

                $member_phone_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $id, 'Phone' => $phone, 'CountryCode' => $country_code));

                if ($member_phone_record != false) {
                    $response_array["message"] = "Phone number already exist in our records";
                    $is_valid = false;
                }



                if ($is_valid == true) {
                    $update_member = array(
                        // 'ReferralCode' => $referral_code,
                        'FirstName' => $first_name,
                        'LastName' => $last_name,
                        'EmailAddress' => $email_address,
                        'CountryCode' => ltrim($country_code, "+"),
                        'Phone' => $phone,
                        'ID' => $id,
                        'MemberInformationVersion' => (intval($member_record["MemberInformationVersion"]) + 1),
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );

                    if (!empty($password)) {
                        $update_member['Password'] = encrypt_decrypt('encrypt', $password);
                    }

                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Your Account Setting Updated Successfully";
                    $response_array["data"] = $this->_member_data_record($id);
                }
            } else {
                $response_array["message"] = "Fields Missing";
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_preferences_update()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $preference_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';



            if (!empty($member_id) && !empty($preference_data) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'MemberPreferenceVersion', array('PKMemberID' => $member_id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $this->model_database->RemoveRecord($this->tbl_member_preferences, $member_id, "FKMemberID");
                    $preference_array = explode("||", $preference_data);
                    if (sizeof($preference_array) > 0) {
                        $count = 0;
                        foreach ($preference_array as $p_data) {
                            if ($count == 0) {
                                $update_member = array(
                                    'AccountNotes' => $p_data,
                                    'ID' => $member_id
                                );
                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                            } else {
                                $insert_member_preference = array(
                                    'FKMemberID' => $member_id,
                                    'FKPreferenceID' => $p_data
                                );
                                $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                            }
                            $count += 1;
                        }
                    }
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Your Laundry Settings Updated Successfully";
                    $update_member = array(
                        'ID' => $member_id,
                        'MemberPreferenceVersion' => (intval($member_record["MemberPreferenceVersion"]) + 1),
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    $response_array["member_data"] = $this->_member_data_record($member_id);
                    $response_array["data"] = $this->_member_preference_records($member_id);
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_credit_card()
    {
        error_reporting(1);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        //$post_data = $_GET;

        $stripeKey = $this->GetStripeAPIKey();
        if (!empty($post_data)) {

            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $type = isset($post_data['type']) ? $post_data['type'] : '';
            $title = isset($post_data['title']) ? $post_data['title'] : '';
            $name = isset($post_data['name']) ? $post_data['name'] : '';
            $number = isset($post_data['number']) ? $post_data['number'] : '';

            $last4 = substr($number, 12);
            $title = "************" . $last4;
            $name = $title;



            $year = isset($post_data['year']) ? $post_data['year'] : '';
            $month = isset($post_data['month']) ? $post_data['month'] : '';
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';

            if (!empty($member_id) && !empty($type) && !empty($device_id)) {


                $this->_update_device_information($device_id);
                $c_id = intval($id);
                $is_valid = true;
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'StripeCustomerID,MemberCardVersion', array('PKMemberID' => $member_id));


                $stripe_customer_id = $member_record["StripeCustomerID"];

                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                    $is_valid = false;
                }
                if ($type == "remove") {
                    if ($c_id != 0) {
                        $card_record = $this->model_database->GetRecord($this->tbl_member_cards, "PKCardID", array('PKCardID' => $c_id, 'FKMemberID' => $member_id));
                        if ($card_record != false) {
                            $this->model_database->RemoveRecord($this->tbl_member_cards, $c_id, "PKCardID");
                            $response_array["result"] = "Success";
                            $update_member = array(
                                'ID' => $member_id,
                                'MemberCardVersion' => (intval($member_record["MemberCardVersion"]) + 1),
                                'UpdatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                            $response_array["member_data"] = $this->_member_data_record($member_id);
                            $response_array["data"] = $this->_member_card_records($member_id);
                            $response_array["message"] = "Card Deleted Successfully";
                        } else {
                            $response_array["message"] = "You are not authorized to delete this card information";
                        }
                    }
                } else {

                    if (strlen($year) < 4) {
                        $year = "20" . $year;
                    }


                    if ($is_valid) {
                        $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, 'PKCardID', array('PKCardID !=' => $c_id, 'FKMemberID' => $member_id, 'number' => $last4, 'Year' => $year, 'Month' => $month));

                        if ($member_card_record != false) {
                            $response_array["message"] = "Name already exist in your records";
                            $is_valid = false;
                        } else {

                            try {
                                $stripe = new Stripe();
                                $data = ['number' => $number, 'exp_month' => $month, 'exp_year' => $year, 'cvc' => $code];


                                $payment_method = $stripe->createCard($stripeKey, $data);



                                if (!isset($payment_method['error'])) {
                                    $data = array(
                                        'payment_method_types' => ['card'],
                                        "payment_method" => $payment_method->id,
                                        "customer" => $stripe_customer_id,
                                        "off_session" => true,
                                        "confirm" => true
                                    );
                                    $stripe->saveSetupIntent($stripeKey, $data);
                                    $payment_method->attach(['customer' => $stripe_customer_id]);
                                    $is_valid = true;
                                } else {
                                    $response_array["message"] = $payment_method["message"];
                                    $response_array["messageCode"] = "0";
                                    $is_valid = false;
                                }
                            } catch (Stripe\Exception\CardException $e) {
                                $response_array['exception'] = "CardException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\ApiErrorException $e) {
                                $response_array['exception'] = "ApiErrorException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\RateLimitException $e) {
                                $response_array['exception'] = "RateLimitException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\ApiConnectionException $e) {
                                $response_array['exception'] = "ApiConnectionException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\InvalidRequestException $e) {
                                $response_array['exception'] = "InvalidRequestException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\UnknownApiErrorException $e) {
                                $response_array['exception'] = "UnknownApiErrorException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception $e) {
                                $response_array['exception'] = "Exception";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\PermissionException $e) {
                                $response_array['exception'] = "PermissionException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\UnexpectedValueException $e) {
                                $response_array['exception'] = "UnexpectedValueException";
                                $response_array['message'] = $e->getMessage();
                            }
                        }


                        if ($is_valid) {
                            $member_card = array(
                                'FKMemberID' => $member_id,
                                'Title' => $title,
                                'Name' => $name,
                                'Month' => $month,
                                'Year' => $year
                            );
                            $response_array["name"] = $name;
                            $response_array["title"] = $title;
                            $response_array["number"] = $last4;
                            $response_array["month"] = $month;
                            $response_array["year"] = $year;
                            $response_array["code"] = $payment_method->id;

                            if (!empty($number)) {
                                $member_card['Number'] = $last4;
                            }
                            if (!empty($code)) {
                                $member_card['Code'] = $payment_method->id;
                            }
                            if ($c_id == 0) {
                                $member_card['CreatedDateTime'] = date('Y-m-d H:i:s');
                                $c_id = $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
                                $response_array["card_id"] = $c_id;
                                $response_array["message"] = "Card Added Successfully";
                            } else {
                                $member_card['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                $member_card['ID'] = $c_id;
                                $this->model_database->UpdateRecord($this->tbl_member_cards, $member_card, 'PKCardID');
                                $response_array["message"] = "Card Updated Successfully";
                            }
                            $response_array["result"] = "Success";


                            //$this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                            $response_array["member_data"] = $this->_member_data_record($member_id);
                            $response_array["data"] = $this->_member_card_records($member_id);
                        }
                    }
                    $response_array["card_id"] = $c_id;
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_discount_code()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $is_bundle = isset($post_data['is_bunddle']) ? $post_data['is_bunddle'] : 0;
            if (!empty($code) && !empty($member_id) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $is_found = true;
                $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('Code' => $code, 'Status' => 'Active'));
                if ($discount_record != false) {
                    $current_date = date('Y-m-d');
                    if ($discount_record['FKMemberID'] != 0) {
                        if ($discount_record['FKMemberID'] != $member_id) {
                            $response_array["message"] = "We did not remember the discount code you entered";
                            $is_found = false;
                        }
                    }

                    /* Hassan changes 26-5-2018 */

                    if (isset($is_bundle) && $is_bundle > 0) {

                        $response_array["message"] = "Discount code is not valid on laundry packages";
                        $is_found = false;
                    }
                    /* Hassan changes 26-5-2018 */

                    if ($is_found) {
                        if (!empty($discount_record['StartDate'])) {
                            $start_date = date('Y-m-d', strtotime($discount_record['StartDate']));
                            if ($current_date < $start_date) {
                                $is_found = false;
                                $response_array["message"] = "We did not remember the discount code you entered";
                            }
                        }
                    }
                    if ($is_found) {
                        if (!empty($discount_record['ExpireDate'])) {
                            $expire_date = date('Y-m-d', strtotime($discount_record['ExpireDate']));
                            if ($current_date > $expire_date) {
                                $update_discount = array(
                                    'Status' => 'Expire',
                                    'ID' => $discount_record['PKDiscountID'],
                                    'UpdatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                                $is_found = false;
                                $response_array["message"] = "We did not remember the discount code you entered";
                            }
                        }
                    }
                    if ($is_found) {
                        if ($discount_record['CodeUsed'] == "One Time") {
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'PKInvoiceID', array('FKDiscountID' => $discount_record['PKDiscountID'], 'FKMemberID' => $member_id));
                            if ($invoice_record != false) {
                                $is_found = false;
                                $response_array["message"] = "You have already used this discount code";
                            }
                        }
                    }
                    if ($is_found) {
                        unset($response_array["message"]);
                        $response_array["result"] = "Success";
                        $response_array["id"] = $discount_record['PKDiscountID'];
                        $response_array["code"] = $discount_record['Code'];
                        $response_array["worth"] = $discount_record['Worth'];
                        $response_array["minimum_order_amount"] = $discount_record['MinimumOrderAmount'];
                        $response_array["discount_type"] = $discount_record['DType'];
                        $response_array["type"] = "discount";


                        $response_array["data"] = "Discount|" . $discount_record['PKDiscountID'] . '|' . $discount_record['Code'] . '|' . $discount_record['Worth'] . '|' . $discount_record['DType'] . '|' . $discount_record['MinimumOrderAmount'];
                    }
                } else {
                    $response_array["message"] = "We did not remember the discount code you entered";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_referral_code()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $response_array["post"] = $post_data;

        if ($post_data) {
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            //$member_data = isset($post_data['member_data']) ? $post_data['member_data'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';



            if (!empty($code) && !empty($member_id) && !empty($device_id)) {

                $this->_update_device_information($device_id);
                $member_data_array = $this->_member_data_record($member_id);

                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_id));

                if ($invoice_record == false) {
                    $member_referral_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", "ReferralCode='" . $post_data['code'] . "' and Status='Enabled' and Phone != '" . $member_data_array['Phone'] . "' and BuildingName !='" . $member_data_array['BuildingName'] . "' and PKMemberID !=" . $member_id);

                    if ($member_referral_record != false) {
                        unset($response_array["message"]);
                        $referral_amount = $this->GetReferralCodeAmount();
                        $response_array["result"] = "Success";
                        $response_array["id"] = $member_referral_record['PKMemberID'];
                        $response_array["code"] = $code;
                        $response_array["worth"] = $referral_amount;
                        $response_array["minimum_order_amount"] = $referral_amount + 5;
                        $response_array["discount_type"] = "Price";
                        $response_array["type"] = "referral";
                        $response_array["data"] = "Referral|" . $member_referral_record['PKMemberID'] . '|' . $post_data['code'] . '|' . $referral_amount . '|Price|' . ($referral_amount + 5);
                    } else {
                        $response_array["result"] = "Error";
                        $response_array["message"] = "We did not remember the referral code you entered";
                    }
                } else {
                    $response_array["result"] = "Error";
                    $response_array["message"] = "You can only use referral code once.";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_invoice_edit()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $_REQUEST;

        $log = http_build_query($post_data);
        //file_put_contents('./application/logs/edit_invoice_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {

            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';

            if (!empty($id) && !empty($member_id) && !empty($device_id)) {

                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'BuildingName,StreetName,PostalCode,Town', array('PKMemberID' => $member_id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "FKFranchiseID,FKCardID,FKDiscountID,ReferralID,DiscountType,DiscountCode,DiscountWorth,DType,PaymentMethod,InvoiceType,Location,Locker,BuildingName,StreetName,PostalCode,Town,AccountNotes,AdditionalInstructions,ServicesTotal,PreferenceTotal,SubTotal,DiscountTotal,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,Regularly,OrderNotes", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                    if ($invoice_record != false) {
                        $invoice_record['PickupDateSelected'] = date('d-F-Y', strtotime($invoice_record['PickupDate']));
                        $invoice_record['DeliveryDateSelected'] = date('d-F-Y', strtotime($invoice_record['DeliveryDate']));
                        $is_address_change = false;
                        if ($member_record['BuildingName'] != $invoice_record['BuildingName']) {
                            $is_address_change = true;
                        }
                        if ($member_record['StreetName'] != $invoice_record['StreetName']) {
                            $is_address_change = true;
                        }
                        if ($member_record['PostalCode'] != $invoice_record['PostalCode']) {
                            $is_address_change = true;
                        }
                        if ($member_record['Town'] != $invoice_record['Town']) {
                            $is_address_change = true;
                        }
                        if ($is_address_change) {
                            $invoice_record['PickerAddressSelectedIndex'] = "1";
                        } else {
                            $invoice_record['PickerAddressSelectedIndex'] = "0";
                        }
                        if ($invoice_record['DiscountType'] == "Discount") {
                            $discount_minimum_amount = "";
                            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, "MinimumOrderAmount", array("PKDiscountID" => $invoice_record['FKDiscountID']));
                            if ($discount_record != false) {
                                $discount_minimum_amount = $discount_record['MinimumOrderAmount'];
                            }
                            $invoice_record['DiscountMinimumAmount'] = $discount_minimum_amount;
                        } else if ($invoice_record['DiscountType'] == "Referral") {
                            $invoice_record['ReferralMinimumAmount'] = ($this->GetReferralCodeAmount() + 5);
                        }
                        $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Name", array('PKCardID' => $invoice_record['FKCardID']));
                        if ($member_card_record != false) {
                            $invoice_record['CardName'] = $member_card_record['Name'];
                        }
                        $invoice_record['franchise_detail'] = $this->_franchise_record($invoice_record['FKFranchiseID']);
                        if ($invoice_record['InvoiceType'] == "Items") {
                            $invoice_record['preferences_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID,ParentTitle,Title,Price,Total", array('FKInvoiceID' => $id), false, false, false, "PKInvoicePreferenceID");
                            $invoice_record['services_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID as ID,FKCategoryID as CategoryID,Title,DesktopImageName as DesktopImage,MobileImageName as MobileImage,IsPackage,PreferencesShow,Quantity,Price,Total", array('FKInvoiceID' => $id), false, false, false, "PKInvoiceServiceID");
                            foreach ($invoice_record['services_records'] as $key => $value) {
                                $service_record = $this->model_database->GetRecord($this->tbl_services, "Content", array("PKServiceID" => $value['ID']));
                                if ($service_record != false) {
                                    $invoice_record['services_records'][$key]['Content'] = str_replace("\n", "", RemoveHtmlTags($service_record['Content']));
                                    $invoice_record['services_records'][$key]['TitleUpper'] = strtoupper($value['Title']);
                                    $invoice_record['services_records'][$key]['CurrencyAmount'] = $value['Price'];
                                    $desktop_image_name_path = "";
                                    $desktop_image_name = "";
                                    $mobile_image_name_path = "";
                                    $mobile_image_name = "";

                                    if (!empty($value['DesktopImage'])) {

                                        if (file_exists($this->config->item("dir_upload_service") . $value['DesktopImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value['DesktopImage'])) {
                                            $desktop_image_name_path = str_replace("lovetolaundrydev", "", base_url()) . $this->config->item("front_service_folder_url") . $value['DesktopImage'];
                                        }
                                        $desktop_image_name = $value['DesktopImage'];
                                    }
                                    if (!empty($value['MobileImage'])) {
                                        if (file_exists($this->config->item("dir_upload_service") . $value['MobileImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value['MobileImage'])) {
                                            $mobile_image_name_path = str_replace("lovetolaundrydev", "", base_url()) . $this->config->item("front_service_folder_url") . $value['MobileImage'];
                                        }
                                        $mobile_image_name = $value['MobileImage'];
                                    }
                                    $invoice_record['services_records'][$key]['DesktopImage'] = $desktop_image_name;
                                    $invoice_record['services_records'][$key]['DesktopImagePath'] = $desktop_image_name_path;
                                    $invoice_record['services_records'][$key]['MobileImage'] = $mobile_image_name;
                                    $invoice_record['services_records'][$key]['MobileImagePath'] = $mobile_image_name_path;
                                } else {
                                    unset($invoice_record['services_records'][$key]);
                                }
                            }
                        }
                        $response_array["result"] = "Success";
                        $response_array["data"] = $invoice_record;
                    } else {
                        $response_array["message"] = "Love2Laundry : Invoice not found";
                    }
                }
            }
        }

        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function calculateDiscount($type, $worth, $cartServices)
    {


        $discountAmount = 0.00;
        foreach ($cartServices as $service) {

            $sum += $service->Total;
            if ($service->IsPackage == "No") {
                $discountAmount += $service->Total;
            }
        }
        $discount = 0.00;
        if ($discountAmount > 0) {
            if ($type == "Percentage") {
                $discount = ($discountAmount / 100) * $worth;
                $sum = $sum - $discount;
            } else {
                $discount = $worth;
            }
        }
        return numberformat($discount);
    }

    public function post_validate_order()
    {

        $post = $this->input->post(NULL, TRUE);
        $response = $this->validate_order($post);
        echo jsonencode($response, 200);
    }

    public function validate_order($post)
    {


        $response["error"] = true;
        $response["errors"] = array();

        $franchise_id = $post["franchise_id"];
        $member_id = $post["member_id"];

        $website_currency = $this->config->item('currency_sign');
        $platform = isset($post['platform']) ? $post['platform'] : "ios";

        $header = 400;
        $franchise = $this->_GetFranchiseRecord($franchise_id);
        $response["franchise"] = $franchise;
        $response["franchise_id"] = $franchise_id;
        $response["member_id"] = $member_id;
        $response["platform"] = $platform;
        $response["website_currency"] = $website_currency;

        if (empty($post["first_name"])) {
            $response["errors"]["first_name"] = "Please enter your first name.";
        }

        if (empty($post["last_name"])) {
            $response["errors"]["last_name"] = $response['message'] = "Please enter your last name.";
        }

        if (empty($post["phone"])) {
            $response["errors"]["phone"] = $response['message'] = "Please enter your phone number.";
        }

        if (!$this->verifyPhoneNumber($post["country_code"], $post["phone"])) {
            $response["errors"]["phone"] = $response['message'] = "Please enter correct phone number.";
        }

        if (empty($post["pickup_date"])) {
            $response["errors"]["pickup_date"] = $response['message'] = "Please choose pick up date.";
        }

        if (empty($post["pickup_time"])) {
            $response["errors"]["pickup_time"] = $response['message'] = "Please choose pick up time.";
        }

        if (empty($post["delivery_date"])) {
            $response["errors"]["delivery_date"] = $response['message'] = "Please choose delivery date.";
        }

        if (empty($post["delivery_time"])) {
            $response["errors"]["delivery_date"] = $response['message'] = "Please choose delivery time.";
        }

        if (empty($response["errors"])) {

            $discount_id = isset($post['discount_id']) ? $post['discount_id'] : NULL;
            $discount_type = isset($post['discount_type']) ? $post['discount_type'] : "None";

            $pick_dateTMP = $post['pickup_date'];
            $pick_timeTMP = $post['pickup_time'];

            $pick_timeArray = explode('-', $pick_timeTMP);
            $pick_timeClean = isset($pick_timeArray[1]) ? $pick_timeArray[1] . ':00' : '';

            $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;
            $current = date("Y-m-d H:i:s");
            $difference = strtotime($pickupDateTimeTMP) - strtotime($current);


            $delivery_dateTMP = $post['delivery_date'];
            $delivery_timeTMP = $post['delivery_time'];

            $delivery_timeArray = explode('-', $delivery_timeTMP);

            $delivery_timeClean = isset($delivery_timeArray[1]) ? $delivery_timeArray[1] . ':00' : '';

            $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;


            $differenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);
            $differenceTMP = $differenceTMP + $franchise["GapTime"];


            $response["pickupDateTimeTMP"] = $pickupDateTimeTMP;
            $response["deliveryDateTimeTMP"] = $deliveryDateTimeTMP;
            $response["differenceTMP"] = $differenceTMP;
            $response["pickup"] = $pickupDateTimeTMP;

            $subTotal = 0;
            $servicesTotal = 0;
            $grandTotal = 0;
            $preferencesTotal = 0;
            $hasPreferences = false;
            $cartServices = array();
            $preferences = array();
            $discountTotal = 0;
            $invoiceType = "After";
            if ($difference <= 0 && empty($response["errors"])) {

                $response["errors"]["pickup_difference_date"] = $response['message'] = "Dear Customer, Your order collection date is past. please check.";
            } else if ($difference < $franchise["PickupDifferenceHour"] && empty($response["errors"])) {
                $response["errors"]["pickup_difference_date"] = $response['message'] = "Dear Customer, Order pick difference should be at least " . $franchise["PickupDifferenceHour"] . " hours , please contact our customer support.";
            } else if ($differenceTMP < $franchise["DeliveryDifferenceHour"] && empty($response["errors"])) {

                $response["errors"]["difference_date"] = $response['message'] = "Dear Customer, Order delivery difference should be at least " . $franchise["DeliveryDifferenceHour"] . " hours , please contact our customer support.";
            } else {

                $cartServices = json_decode($post["services"]);
                $errorInDiscount = false;
                $response["error"] = $errorInDiscount;

                if (count($cartServices) > 0) {
                    $invoiceType = "Items";
                    $this->load->model('preferences');
                    foreach ($cartServices as $cartService) {
                        $servicesTotal += $cartService->Total;

                        if (!$hasPreferences) {
                            if ($cartService->PreferencesShow == "Yes") {
                                $hasPreferences = true;
                            }
                        }
                    }
                    $subTotal += $servicesTotal;


                    if ($hasPreferences) {
                        //echo $platform;

                        if ($platform == "android") {
                            $preferences = json_decode($post["preferences"]);
                        } else {
                            $preferences = ($post["preferences"]);
                        }



                        $preferences = $this->preferences->where_in("PKPreferenceID", $preferences);

                        foreach ($preferences as $preference) {

                            if ($preference["PriceForPackage"] == "Yes") {
                                $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                            }
                        }
                        $subTotal += $preferencesTotal;
                    }

                    $grandTotal = $subTotal;
                } else {

                    $grandTotal = $franchise["MinimumOrderAmount"];
                    $subTotal = $grandTotal;
                }



                if ($grandTotal >= $franchise["MinimumOrderAmount"]) {
                    $discountType = "None";
                    $discountCode = NULL;
                    $discountWorth = NULL;
                    $dType = "None";
                    if ($discount_id != NULL) {

                        if ($discount_type == "discount") {
                            //'Status' => 'Active'
                            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('PKDiscountID' => $discount_id));

                            if ($discount_record != false) {
                                $worth = $discount_record['Worth'];
                                $dType = $discount_record["DType"];
                                $code = $discount_record["Code"];
                                if ($grandTotal < $discount_record['MinimumOrderAmount']) {
                                    $errorInDiscount = true;
                                    $errorDiscountMessage = "Dear Customer, to avail this discount code your grand total must be a minimum of " . $this->config->item("currency_symbol") . numberformat($discount_record['MinimumOrderAmount']) . ". ";
                                }
                            } else {
                                $errorInDiscount = true;
                                $errorDiscountMessage = "Your discount code has been expired.";
                            }
                        } else {

                            $discount_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $discount_id));
                            if ($discount_record != false) {
                                $code = $discount_record["ReferralCode"];
                                $worth = $this->GetReferralCodeAmount();
                                $dType = "Price";
                                $discountKey = "ReferralID";
                            } else {
                                $errorDiscountMessage = "Invalid referral code.";
                            }
                        }

                        if ($errorInDiscount == true) {
                            $discount_id = NULL;
                            $discountType = "None";
                            $discountCode = NULL;
                            $discountWorth = NULL;
                            $dType = "None";
                        } else {
                            $discountTotal = $this->calculateDiscount($dType, $worth, $cartServices);
                            $grandTotal -= $discountTotal;
                            $discountType = ucfirst($discount_type);
                            $discountCode = $code;
                            $discountWorth = $worth;

                            if ($grandTotal < $discount_record['MinimumOrderAmount']) {
                                $errorInDiscount = true;
                                $errorDiscountMessage = "Dear Customer, your minimum order amount must be atleast " . $this->config->item("currency_symbol") . numberformat($franchise["MinimumOrderAmount"]) . ".";
                                $response["errors"]["discount"] = $response['message'] = $errorDiscountMessage;
                            }
                        }
                    }

                    $response["error"] = $errorInDiscount;
                } else {
                    $response["error"] = true;
                    $header = 400;
                    $response["errors"]["discount"] = $response['message'] = "Dear Customer, your minimum order amount must be atleast " . $this->config->item("currency_symbol") . numberformat($franchise["MinimumOrderAmount"]) . ".";
                }
            }
        }

        $response["cartServices"] = $cartServices;
        $response["discount_id"] = $discount_id;
        $response["discountType"] = $discountType;
        $response["discountCode"] = $discountCode;
        $response["discountWorth"] = $discountWorth;
        $response["minimumOrderAmount"] = $franchise["MinimumOrderAmount"];
        $response["dType"] = $dType;
        $response["discountTotal"] = numberformat((float) $discountTotal);
        $response["errorInDiscount"] = $errorInDiscount;
        $response["subTotal"] = numberformat((float) $subTotal);
        $response["servicesTotal"] = numberformat((float) $servicesTotal);
        $response["grandTotal"] = numberformat((float) $grandTotal);
        $response["preferencesTotal"] = numberformat((float) $preferencesTotal);
        $response["hasPreferences"] = $hasPreferences;
        $response["preferences"] = $preferences;
        $response["invoiceType"] = $invoiceType;
        $response["validate_date"] = date("Y-m-d H:i:s");
        $response["validate_date"] = date("Y-m-d H:i:s");
        return $response;
    }

    public function order()
    {
        //error_reporting(1);
        //$this->db->trans_start();


        if (!isset($_GET["data"])) {
            $post = $this->input->post(NULL, TRUE);
        } else {

            $_GET["data"] = '{"member_id":"7639","preferences":"[12,4,25,16,7,20,42,31]","address":"Flat 7, Dean House, Tarling Street, London","device_id":"6791","town":"London","address2":"","order_notes":"","last_name":"yesy","ip_address":"192.168.10.3","delivery_time":"21:00-23:00","services":"[]","version":"Android (4.1.12)","has_address":"1","card_id":"7876","platform":"android","country_code":"44","pickup_date":"2021-05-28","delivery_date":"2021-05-30","phone":"2224224","franchise_id":"6","post_code":"e12pe","pickup_time":"19:00-21:00","first_name":"aftab","payment_method":"stripe"}';
            // echo $_GET["data"];
            $post = json_decode($_GET["data"], true);
        }
        $paymentMethod = $post["payment_method"];

        $header = 400;
        $discountKey = "FKDiscountID";
        $subTotal = 0;
        $servicesTotal = 0;
        $grandTotal = 0;
        $preferencesTotal = 0;
        $hasPreferences = false;

        $discountTotal = 0.00;
        $errorInDiscount = false;
        $errorDiscountMessage = "";

        $discountType = "None";
        $discountCode = NULL;
        $discountWorth = NULL;
        $dType = "None";


        $response = $this->validate_order($post);

        $log = http_build_query($post);
        file_put_contents('./application/logs/checkout_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);

        $franchise_id = $response["franchise_id"];
        $member_id = $response["member_id"];
        $website_currency = $this->config->item('currency_sign');
        $platform = $response['platform'];

        if ($post["version"] == "iPhone (4.1.17)") {

            $response["error"] = true;
            $header = 400;
            $response["errors"]["payment"] = $response['message'] = "App has been upgraded, to place an order please update your app.";
        } else {

            if (empty($response["errors"])) {

                $this->load->library('stripe');
                $this->load->model('cart');

                $franchise_record = $response["franchise"];
                $cartServices = $response["cartServices"];
                $discount_id = $response["discount_id"];
                $discountType = $response["discountType"];
                $discountCode = $response["discountCode"];
                $discountWorth = $response["discountWorth"];
                $dType = $response["dType"];
                $errorInDiscount = $response["errorInDiscount"];
                $subTotal = $response["subTotal"];
                $servicesTotal = $response["servicesTotal"];
                $grandTotal = $response["grandTotal"];
                $preferencesTotal = $response["preferencesTotal"];
                $hasPreferences = $response["hasPreferences"];
                $preferences = $response["preferences"];
                $discountTotal = $response["discountTotal"];
                $invoiceType = $response["invoiceType"];
                $error = $response["error"];
                $worth = $response["discountWorth"];
                $device_id = $post["device_id"];

                $cart = $this->cart->first(array("device_id" => $device_id));



                if ($error == false) {

                    $card_id = $post['card_id'];
                    $member_card = array();

                    if ($post['payment_method'] == "stripe") {
                        $member_card = $this->model_database->GetRecord($this->tbl_member_cards, false, array('PKCardID' => $card_id, "FKMemberID" => $member_id));
                        $code = $member_card['Code'];
                    } else {
                        $code = $post['payment_method_id'];
                        $card_id = null;
                    }

                    $header = 200;

                    $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));
                    $member_detail = $member_record;

                    $stripe_customer_id = $member_record['StripeCustomerID'];


                    //if ($member_record["EmailAddress"] == "testingg37@gmail.com") {

                    // echo json_encode($post);
                    // die;
                    // die("here");
                    //}



                    $subTotal = numberformat((float) $subTotal);
                    $preferencesTotal = numberformat((float) $preferencesTotal);
                    $paymentTokan = "";
                    $paymentStatus = "Pending";
                    if ($errorInDiscount == true) {

                        $response["error"] = true;
                        $header = 400;
                        $response["errors"]["discount"] = $response['message'] = $errorDiscountMessage;
                    } else {

                        $minimumOrderAmount = $franchise_record["MinimumOrderAmount"];

                        if ($grandTotal >= $franchise_record["MinimumOrderAmount"]) {


                            if ($post["payment_method"] != "cash") {


                                $stripeKey = $this->GetStripeAPIKey();

                                $convertAmount = $this->country->convertAmount($grandTotal);
                                $amount = (int) ($convertAmount * 100);

                                $data = [
                                    'payment_method' => $code,
                                    'amount' => $amount,
                                    'customer' => $stripe_customer_id,
                                    'payment_method_types' => ['card'],
                                    'currency' => $this->config->item("currency_to"),
                                    'confirmation_method' => 'manual',
                                    'confirm' => true,
                                    'setup_future_usage' => 'off_session',
                                    //'description' => "Invoice #" . $invoice_number
                                ];

                                $response["stripe_data"] = $data;


                                try {
                                    $stripe = new Stripe();
                                    $intent = $stripe->purchase($stripeKey, $data);

                                    if (isset($intent["error"])) {
                                        $response["error"] = true;
                                        $header = 400;
                                        $response["errors"]["payment_intent"] = $response['message'] = $intent["message"];
                                    } else {
                                        $paymentTokan = $intent->id;
                                        $response["error"] = false;
                                        $paymentStatus = "Completed";
                                    }
                                } catch (Exception $ex) {
                                    $response["error"] = true;
                                    $header = 400;
                                    $response["errors"]["payment_intent"] = $response['message'] = $ex->getMessage();
                                }
                            } else {
                                $response["error"] = false;
                                $paymentStatus = "Pending";
                            }

                            $response["payment_intent_id"] = $paymentTokan;
                            $response["stripe_intent"] = $intent;

                            if ($response["error"] == false) {


                                $update_member = array(
                                    'FirstName' => $post["first_name"],
                                    'LastName' => $post["last_name"],
                                    'Phone' => ltrim($post["phone"], 0),
                                    'CountryCode' => trim($post["country_code"]),
                                );

                                //$this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');


                                $order_notes = isset($post['order_notes']) ? $post['order_notes'] : "";
                                $invoice_data = array(
                                    'FKMemberID' => $member_id,
                                    'FKCardID' => $card_id,
                                    'FKFranchiseID' => $franchise_record['PKFranchiseID'],
                                    'InvoiceType' => $invoiceType,
                                    'Currency' => $website_currency,
                                    'PaymentMethod' => ucfirst($paymentMethod),
                                    'PaymentToken' => $paymentTokan,
                                    'PaymentReference' => $code,
                                    'OrderNotes' => $order_notes,
                                    'SubTotal' => $subTotal,
                                    'ServicesTotal' => $servicesTotal,
                                    $discountKey => $discount_id,
                                    'DiscountType' => $discountType,
                                    'DiscountCode' => $discountCode,
                                    'DiscountWorth' => $discountWorth,
                                    'DType' => $dType,
                                    'DiscountTotal' => $discountTotal,
                                    'GrandTotal' => $grandTotal,
                                    'PreferenceTotal' => $preferencesTotal,
                                    'IPAddress' => $this->input->ip_address(),
                                    'PickupDate' => $post['pickup_date'],
                                    'PickupTime' => $post['pickup_time'],
                                    'DeliveryDate' => $post['delivery_date'],
                                    'DeliveryTime' => $post['delivery_time'],
                                    'Regularly' => 0
                                );

                                $invoice_data['Currency'] = strtoupper($this->config->item("currency_symbol"));
                                $invoice_data['AppVersion'] = $post["version"];
                                $invoice_data['FKDeviceID'] = $post["device_id"];
                                $invoice_data['BuildingName'] = $post["address"];
                                $invoice_data['StreetName'] = $post["address2"];
                                $invoice_data['Town'] = $post["town"];
                                $invoice_data['PostalCode'] = $post["post_code"];
                                $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                $invoice_data['PaymentStatus'] = $paymentStatus;
                                $invoice_data['OrderStatus'] = "Processed";
                                $invoice_data['OrderPostFrom'] = "Mobile";
                                $invoice_data['Location'] = $cart["location"];

                                $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                                $invoice_number = $this->GetInvoiceNumber($invoice_id);
                                $updateArray = array("InvoiceNumber" => $invoice_number, "ID" => $invoice_id);
                                $this->model_database->UpdateRecord($this->tbl_invoices, $updateArray, 'PKInvoiceID');



                                if (isset($intent->id)) {
                                    try {


                                        $data = array(
                                            'description' => "Invoice #" . $invoice_number,
                                            'metadata' => [
                                                'Invoice ID' => $invoice_id,
                                                'Invoice Number' => $invoice_number,
                                                'Member ID' => $member_id,
                                                'Member Email' => $member_record["EmailAddress"],
                                                'Member Name' => $member_record["FirstName"] . " " . $member_record["LastName"],
                                            ]
                                        );

                                        $intent = $stripe->updatePurchase($stripeKey, $intent->id, $data);
                                    } catch (Exception $ex) {
                                        $response["error"] = true;
                                        $header = 400;
                                        $response["errors"]["payment_intent"] = $response['message'] = $ex->getMessage();
                                    }
                                }
                                $action = "insert";

                                $invoice_data["id"] = $invoice_id;

                                foreach ($cartServices as $cartService) {

                                    $insert_invoice_service = array(
                                        'FKInvoiceID' => $invoice_id,
                                        'FKServiceID' => $cartService->ID,
                                        'FKCategoryID' => $cartService->CategoryID,
                                        'Title' => $cartService->Title,
                                        'DesktopImageName' => urldecode($cartService->DesktopImage),
                                        'MobileImageName' => urldecode($cartService->MobileImage),
                                        'IsPackage' => $cartService->IsPackage,
                                        'PreferencesShow' => $cartService->PreferencesShow,
                                        'Quantity' => $cartService->Quantity,
                                        'Price' => $cartService->Price,
                                        'Total' => $cartService->Total
                                    );
                                    $tookan_invoice_service[] = $insert_invoice_service;
                                    $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                }




                                if ($hasPreferences) {

                                    foreach ($preferences as $preference) {

                                        $insert_invoice_preference["FKInvoiceID"] = $invoice_id;
                                        $insert_invoice_preference["FKPreferenceID"] = $preference["PKPreferenceID"];
                                        $insert_invoice_preference["Title"] = $preference["Title"];
                                        $insert_invoice_preference["Price"] = $preference["Price"];
                                        $insert_invoice_preference["Total"] = $preference["Price"];
                                        if ($preference["ParentPreferenceID"] != 0) {

                                            $p = $this->model_database->GetRecord($this->tbl_preferences, 'Title', array('PKPreferenceID' => $preference["ParentPreferenceID"]));
                                            $insert_invoice_preference["ParentTitle"] = $p["Title"];
                                        }
                                        $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                    }
                                }



                                $insert_invoice_payment_information = array(
                                    'FKInvoiceID' => $invoice_id,
                                    'FKCardID' => $card_id,
                                    'Amount' => $grandTotal,
                                    'PaymentReference' => $code,
                                    'PaymentToken' => $paymentTokan,
                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);

                                if (isset($discount_id)) {

                                    $is_referral_discount_create = true;
                                    if ($invoice_record != false) {
                                        if ($invoice_record["PaymentStatus"] != "Pending") {
                                            $is_referral_discount_create = false;
                                        }
                                    }
                                    if ($is_referral_discount_create) {
                                        // $discount_cart_array = explode("|", $discount_cart);
                                        if ($discount_type != "discount") {
                                            $insert_discount = array(
                                                'FKMemberID' => $discount_id,
                                                'Code' => $this->RandomPassword(6),
                                                'Worth' => $worth,
                                                'DType' => $dType,
                                                'DiscountFor' => ucfirst($discount_type),
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                        }
                                    }
                                } else {

                                    $loyalty_points = intval($grandTotal);
                                    $minus_loyalty_points = 0;
                                    $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_id));
                                    $loyalty_data = array(
                                        'FKMemberID' => $member_id,
                                        'FKInvoiceID' => $invoice_id,
                                        'InvoiceNumber' => $invoice_number,
                                        'GrandTotal' => $grandTotal,
                                        'Points' => $loyalty_points
                                    );
                                    if ($invoice_loyalty_record != false) {
                                        $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                        $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                        $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                        $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                    } else {
                                        $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                        $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                    }
                                    $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                    $total_loyalty_points = intval($member_detail['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                    $used_loyalty_points = intval($member_detail['UsedLoyaltyPoints']);
                                    $total_loyalty_points += $loyalty_points;
                                    $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                    if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                        $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                        for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                            $loyalty_code = $this->RandomPassword(6);
                                            $insert_discount = array(
                                                'FKMemberID' => $member_id,
                                                'Code' => $loyalty_code,
                                                'Worth' => $this->GetLoyaltyCodeAmount(),
                                                'DType' => "Price",
                                                'DiscountFor' => "Loyalty",
                                                'Status' => "Active",
                                                'CodeUsed' => "One Time",
                                                'CreatedBy' => 0,
                                                'CreatedDateTime' => date('Y-m-d H:i:s')
                                            );
                                            $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                            $used_loyalty_points += $minimum_loyalty_points;
                                        }
                                    }

                                    $update_member['TotalLoyaltyPoints'] = $total_loyalty_points;
                                    $update_member['UsedLoyaltyPoints'] = $used_loyalty_points;
                                }
                                $update_member['ID'] = $member_id;
                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');

                                $response["error"] = false;
                                $response["id"] = $invoice_id;
                                $response["number"] = $invoice_number;
                                $response["message"] = "invoice created";
                                // $response["tookaan"] = $this->saveToTookaan($invoice_id, $member_record, $action);
                                //$this->db->trans_complete();
                            } else {
                                $response["error"] = true;
                                $header = 400;
                                $response["errors"]["payment"] = $response['message'] = "There is an error with you current payment information. Try different one.";
                            }
                        } else {

                            $response["error"] = true;
                            $header = 400;
                            $response["errors"]["payment"] = $response['message'] = "Dear Customer, your minimum order amount must be atleast " . $this->config->item("currency_symbol") . numberformat($minimumOrderAmount) . ".";
                        }
                    }
                }
            }
        }

        echo jsonencode($response, 200);
    }

    public function post_invoice_update()
    {
        error_reporting(E_ALL);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["type"] = "Message";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $log = http_build_query($post_data);

        file_put_contents('./application/logs/checkout_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);

        if ($post_data) {
            $grand_total = floatval(isset($post_data['grand_total']) ? $post_data['grand_total'] : '');
            $card_id = intval(isset($post_data['card_id']) ? $post_data['card_id'] : null);
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $address = isset($post_data['building_name']) ? $post_data['building_name'] : '';
            $street_name = isset($post_data['street_name']) ? $post_data['street_name'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $pick_date = isset($post_data['pick_date']) ? date('Y-m-d', strtotime($post_data['pick_date'])) : '';
            $pick_time = isset($post_data['pick_time']) ? $post_data['pick_time'] : '';
            $delivery_date = isset($post_data['delivery_date']) ? date('Y-m-d', strtotime($post_data['delivery_date'])) : '';
            $delivery_time = isset($post_data['delivery_time']) ? $post_data['delivery_time'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $regularly_cart = isset($post_data['regularly_cart']) ? $post_data['regularly_cart'] : 0;
            $appVersion = isset($post_data['version']) ? $post_data['version'] : "";
            $paymentMethod = isset($post_data['payment_method']) ? $post_data['payment_method'] : "Stripe";
            $id = intval(isset($post_data['id']) ? $post_data['id'] : '');
            $member_id = intval(isset($post_data['member_id']) ? $post_data['member_id'] : '');
            $franchise_id = intval(isset($post_data['franchise_id']) ? $post_data['franchise_id'] : '');


            if ($grand_total != 0 && !empty($pick_date) && !empty($pick_time) && !empty($delivery_date) && !empty($delivery_time) && !empty($post_code) && !empty($address) && !empty($town) && !empty($device_id) && !empty($id)) {




                $this->_update_device_information($device_id);

                $member_data = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone,StripeCustomerID", array('PKMemberID' => $member_id));
                $member_data = $this->_member_data_record($member_id);

                $franchise_record = $this->_franchise_record($franchise_id);

                $member_data_array = $member_data;
                $member_record = $member_data;
                $franchise_data_array = $franchise_record;

                if (!empty($member_data_array) && !empty($franchise_data_array)) {

                    $stripe_customer_id = $member_record["StripeCustomerID"];

                    $discount_referral_data = isset($post_data['discount_referral_data']) ? $post_data['discount_referral_data'] : '';
                    $preferences_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
                    $service_total = floatval(isset($post_data['service_total']) ? $post_data['service_total'] : '0.00');
                    $preference_total = floatval(isset($post_data['preference_total']) ? $post_data['preference_total'] : '0.00');
                    $sub_total = floatval(isset($post_data['sub_total']) ? $post_data['sub_total'] : '');
                    $discount_total = floatval(isset($post_data['discount_total']) ? $post_data['discount_total'] : '0.00');
                    $service_data = isset($post_data['services_data']) ? $post_data['services_data'] : '';
                    $is_valid = true;



                    /* Hassan changes 1-4-2018 */


                    if ($post_data['pick_date'] && $post_data['delivery_date']) {
                        $pick_dateTMP = $post_data['pick_date'];
                        $pick_timeTMP = $post_data['pick_time'];
                        if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                            $pick_timeArray = explode('-', $pick_timeTMP);
                            $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
                        } else {
                            $pick_timeClean = '';
                        }
                        $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

                        $delivery_dateTMP = $post_data['delivery_date'];
                        $delivery_timeTMP = $post_data['delivery_time'];

                        if (isset($delivery_timeTMP) && $delivery_timeTMP != '') {
                            $delivery_timeArray = explode('-', $delivery_timeTMP);
                            $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
                        } else {
                            $delivery_timeClean = '';
                        }

                        $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

                        $differenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);

                        if ($differenceTMP < $franchise_data_array["DeliveryDifferenceHour"]) {
                            $is_valid = false;
                            $response_array["type"] = "Date Time";
                            $response_array["message"] = "Difference between Pickup and Delivery Date Time should be greater than " . $franchise_data_array["DeliveryDifferenceHour"] . " hours";
                        }
                    }

                    /* Hassan difference of 3 hours pickup 25-5-2018 */

                    $invoice_recordTmp = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,FKCardID,PaymentStatus,GrandTotal,OrderStatus,PickupTime,PickupDate", array('PKInvoiceID' => $id));

                    $card_id = $invoice_recordTmp["FKCardID"];


                    if ($invoice_recordTmp['OrderStatus'] == "Pending" || $invoice_recordTmp['OrderStatus'] == "Processed") {

                        $is_edit_button_show = true;
                        $pickup_time_array = explode("-", $invoice_recordTmp['PickupTime']);
                        $date1 = new DateTime(date('Y-m-d', strtotime($invoice_recordTmp['PickupDate'])) . 'T' . $pickup_time_array[0] . ':00');
                        $date2 = new DateTime(date('Y-m-d') . 'T' . date('H:i:s'));

                        // The diff-methods returns a new DateInterval-object...

                        $interval = $date2->diff($date1);
                        $invert = $interval->invert;

                        $h = $interval->format('%h');
                        $days = $interval->format('%d');
                        $hour = ($days * 24) + $h;

                        if ($invert > 0) {
                            $is_edit_button_show = false;
                        } else {
                            if ($hour <= 3) {
                                $is_edit_button_show = false;
                            }
                        }
                        if (!$is_edit_button_show) {
                            $is_valid = false;
                            $response_array["message"] = "Dear Customer, You cannot update order, please contact our customer support";
                        }
                    }

                    $response_array["is_valid"] = $is_valid;

                    if ($is_valid) {
                        $invoice_number = "";
                        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,PaymentStatus,GrandTotal", array('PKInvoiceID' => $id));
                        if ($invoice_record != false) {
                            $invoice_number = $invoice_record['InvoiceNumber'];
                            $response_array['InvoiceNumber'] = $invoice_number;
                            if ($invoice_record["PaymentStatus"] == "Completed") {
                                if (floatval($invoice_record["GrandTotal"]) > $grand_total) {
                                    $is_valid = false;
                                    $response_array["message"] = "Dear Customer, You cannot reduce your order amount from the cart, please contact our customer support";
                                }
                            }
                        }

                        if ($is_valid) {

                            if ($invoice_record != false) {
                                $invoice_payment_records = $this->model_database->GetRecords($this->tbl_invoice_payment_informations, "R", "Amount", array('FKInvoiceID' => $id));
                                if (sizeof($invoice_payment_records) > 0) {
                                    $payment_information_amount = floatval(0);
                                    foreach ($invoice_payment_records as $rec) {
                                        $payment_information_amount += floatval($rec["Amount"]);
                                    }
                                    $stripe_grand_total = $grand_total - $payment_information_amount;
                                } else {
                                    $stripe_grand_total = $grand_total;
                                }
                            } else {
                                $stripe_grand_total = $grand_total;
                            }

                            if ($paymentMethod == "Stripe") {
                                //$stripe_grand_total = floatval(0);
                                $is_test = false;
                                $card_token = "";

                                $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Number,Year,Month,Code", array("PKCardID" => $card_id));
                                if ($member_card_record == false) {
                                    $response_array["type"] = "Card";
                                    $response_array["message"] = "Card information not found";
                                    //$response_array['card_data'] = $this->_member_card_records($member_data_array["ID"]);
                                    $is_valid = false;
                                }
                                $orderStatus = "Pending";
                                $paymentStatus = "Processed";
                            } else {
                                $orderStatus = "Processed";
                                $paymentStatus = "Pending";
                            }

                            if ($is_valid) {

                                $invoice_data = array(
                                    'BuildingName' => $address,
                                    'StreetName' => $street_name,
                                    'Town' => $town,
                                    'PostalCode' => $post_code,
                                    'FKDeviceID' => $device_id,
                                    'FKCardID' => $card_id,
                                    'FKMemberID' => $member_id,
                                    'FKFranchiseID' => $franchise_id,
                                    'InvoiceNumber' => $invoice_number,
                                    'PaymentMethod' => $paymentMethod,
                                    'OrderNotes' => isset($post_data['account_notes']) ? $post_data['account_notes'] : '',
                                    'GrandTotal' => $grand_total,
                                    'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                                    'PickupDate' => $pick_date,
                                    'PickupTime' => $pick_time,
                                    'OrderPostFrom' => 'Mobile',
                                    'DeliveryDate' => $delivery_date,
                                    'DeliveryTime' => $delivery_time,
                                    'Regularly' => $regularly_cart,
                                    'AppVersion' => $appVersion
                                );

                                if ($service_total > 0) {
                                    $invoice_data['ServicesTotal'] = $service_total;
                                }
                                if ($preference_total > 0) {
                                    $invoice_data['PreferenceTotal'] = $preference_total;
                                }
                                if ($sub_total > 0) {
                                    $invoice_data['SubTotal'] = $sub_total;
                                }
                                if ($discount_total > 0) {
                                    $invoice_data['DiscountTotal'] = $discount_total;
                                }
                                $is_discount_found = false;
                                if (!empty($discount_referral_data)) {
                                    $discount_referral_data_array = explode('|', $discount_referral_data);
                                    if (sizeof($discount_referral_data_array) > 0) {
                                        if ($discount_referral_data_array[0] == "Discount") {
                                            $invoice_data['DiscountType'] = "Discount";
                                            $invoice_data['FKDiscountID'] = $discount_referral_data_array[1];
                                        } else {
                                            $invoice_data['DiscountType'] = "Referral";
                                            $invoice_data['ReferralID'] = $discount_referral_data_array[1];
                                        }
                                        $invoice_data['DiscountCode'] = $discount_referral_data_array[2];
                                        $invoice_data['DiscountWorth'] = $discount_referral_data_array[3];
                                        $invoice_data['DType'] = $discount_referral_data_array[4];
                                        $is_discount_found = true;
                                    }
                                }
                                if (!$is_discount_found) {
                                    $invoice_data['DiscountType'] = "None";
                                    $invoice_data['FKDiscountID'] = null;
                                    $invoice_data['ReferralID'] = null;
                                    $invoice_data['DiscountCode'] = null;
                                    $invoice_data['DiscountWorth'] = null;
                                    $invoice_data['DType'] = "None";
                                }

                                $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $id, "FKInvoiceID");
                                $this->model_database->RemoveRecord($this->tbl_invoice_services, $id, "FKInvoiceID");
                                $invoice_data['ID'] = $id;
                                $response_array["InvoiceID"] = $id;
                                $is_service_found = false;
                                $tookan_invoice_service = array();
                                $hasPreferences = false;

                                if (strlen($service_data) > 10) {
                                    $service_data_array = json_decode($service_data, true);

                                    if (sizeof($service_data_array) > 0) {

                                        foreach ($service_data_array as $cart_data) {
                                            $total = floatval(intval($cart_data['Quantity']) * floatval($cart_data['Price'])); /* Cart services calculation hassan 1-13-2019 */


                                            $insert_invoice_service = array(
                                                'FKInvoiceID' => $id,
                                                'FKServiceID' => $cart_data['ID'],
                                                'FKCategoryID' => $cart_data['CategoryID'],
                                                'Title' => $cart_data['Title'],
                                                'DesktopImageName' => $cart_data['DesktopImagePath'],
                                                'MobileImageName' => $cart_data['MobileImage'],
                                                'IsPackage' => $cart_data['IsPackage'],
                                                'PreferencesShow' => $cart_data['PreferencesShow'],
                                                'Quantity' => $cart_data['Quantity'],
                                                'Price' => $cart_data['Price'],
                                                'Total' => $total
                                            );

                                            $hasPreferences = ($hasPreferences == true) or ($cart_data['PreferencesShow'] == "Yes");


                                            $tookan_invoice_service[] = $insert_invoice_service;
                                            $invoice_service_id = $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                            if ($invoice_service_id != 0) {
                                                $is_service_found = true;
                                            }
                                        }
                                    }
                                }

                                if ($is_service_found) {
                                    $invoice_data['InvoiceType'] = "Items";
                                } else {
                                    $invoice_data['InvoiceType'] = "After";
                                }


                                $preference_record = $this->model_database->GetRecords($this->tbl_invoice_preferences, "ParentPreferenceID,Title,Price,PriceForPackage", array('FKInvoiceID' => $id));

                                if ($preference_record == false && $hasPreferences == true) {

                                    $this->load->model('preferences');
                                    $preferences = $this->preferences->get_all();

                                    foreach ($preferences as $preference) {

                                        $insert_invoice_preference["FKInvoiceID"] = $id;
                                        $insert_invoice_preference["FKPreferenceID"] = $preference["PKPreferenceID"];
                                        $insert_invoice_preference["Title"] = $preference["Title"];
                                        $insert_invoice_preference["Price"] = $preference["Price"];
                                        $insert_invoice_preference["Total"] = $preference["Price"];
                                        $insert_invoice_preference["ParentTitle"] = $preference["parentTitle"];

                                        $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                    }
                                }

                                if ($invoice_record == false) {
                                    $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                } else {
                                    $invoice_data['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                }

                                $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                                $is_success = true;

                                $response_array["grand_total"] = $grand_total;


                                if ($grand_total > 0) {

                                    if ($paymentMethod == "Stripe") {

                                        $convertAmount = $this->country->convertAmount($stripe_grand_total);
                                        $amount = (int) ($convertAmount * 100);

                                        $stripe = new Stripe();
                                        $data = [
                                            'payment_method' => $member_card_record['Code'],
                                            'amount' => $amount,
                                            'customer' => $stripe_customer_id,
                                            'payment_method_types' => ['card'],
                                            'currency' => $this->config->item("currency_to"),
                                            'confirmation_method' => 'manual',
                                            'confirm' => true,
                                            'setup_future_usage' => 'off_session',
                                            'description' => "Invoice #" . $invoice_number
                                        ];


                                        $stripeKey = $this->GetStripeAPIKey();
                                        $intent = $stripe->purchase($stripeKey, $data);

                                        $intentResponse = json_encode($intent);
                                        $intentId = $intentResponse;
                                        $orderStatus = "Pending";
                                        $paymentStatus = "Processed";
                                    } else {


                                        $member_card_record['Code'] = null;
                                        $intentResponse = "Cash";
                                        $intentId = "Cash";
                                        $credit_card_id = null;
                                        $orderStatus = "Processed";
                                        $paymentStatus = "Pending";
                                    }

                                    if (isset($intent->id)) {
                                        $payment_information_data = array(
                                            'InvoiceNumber' => $invoice_number,
                                            'Content' => $intentId,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );

                                        $this->model_database->InsertRecord($this->tbl_payment_informations, $payment_information_data);
                                        $invoice_data['PaymentToken'] = $intentId;
                                        $invoice_data['PaymentReference'] = $member_card_record['Code'];
                                        $invoice_data['OrderStatus'] = $orderStatus;
                                        $invoice_data['PaymentStatus'] = $paymentStatus;
                                        $invoice_data['PaymentMethod'] = $paymentMethod;
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');

                                        $insert_invoice_payment_information = array(
                                            'FKInvoiceID' => $id,
                                            'FKCardID' => $card_id,
                                            'Amount' => $stripe_grand_total,
                                            'PaymentReference' => $member_card_record['Code'],
                                            'PaymentToken' => $intentId,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                                    }

                                    if ($invoice_data['InvoiceType'] == "Items") {
                                        if (!$is_discount_found) {
                                            $loyalty_points = intval($grand_total);
                                            $minus_loyalty_points = 0;
                                            $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_data['ID']));
                                            $loyalty_data = array(
                                                'FKMemberID' => $member_id,
                                                'FKInvoiceID' => $id,
                                                'InvoiceNumber' => $invoice_number,
                                                'GrandTotal' => $grand_total,
                                                'Points' => $loyalty_points
                                            );
                                            if ($invoice_loyalty_record != false) {
                                                $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                                $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                                $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                                $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                            } else {
                                                $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                                $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                            }
                                            $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                            $total_loyalty_points = intval($member_data_array['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                            $used_loyalty_points = intval($member_data_array['UsedLoyaltyPoints']);
                                            $total_loyalty_points += $loyalty_points;
                                            $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                            if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                                $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                                for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                                    $loyalty_code = $this->RandomPassword(6);
                                                    $insert_discount = array(
                                                        'FKMemberID' => $member_id,
                                                        'Code' => $loyalty_code,
                                                        'Worth' => $this->GetLoyaltyCodeAmount(),
                                                        'DType' => "Price",
                                                        'DiscountFor' => "Loyalty",
                                                        'Status' => "Active",
                                                        'CodeUsed' => "One Time",
                                                        'CreatedBy' => 0,
                                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                                    );
                                                    $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                    $used_loyalty_points += $minimum_loyalty_points;
                                                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                                    if ($sms_responder_record != false) {
                                                        $sms_field_array = array(
                                                            'Loyalty Points' => $remain_loyalty_points,
                                                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                            'Currency' => $this->config->item("currency_sign"),
                                                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                            'Discount Code' => $loyalty_code
                                                        );
                                                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                                        // $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data['Phone'], 0), $msg_content);
                                                    }
                                                }
                                            }
                                            $update_member = array(
                                                'TotalLoyaltyPoints' => $total_loyalty_points,
                                                'UsedLoyaltyPoints' => $used_loyalty_points,
                                                'ID' => $member_id
                                            );
                                            $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                        } else {
                                            if ($is_discount_found) {
                                                $discount_referral_data_array = explode('|', $discount_referral_data);
                                                if ($discount_referral_data_array[0] != "Discount") {
                                                    $insert_discount = array(
                                                        'FKMemberID' => $discount_referral_data_array[1],
                                                        'Code' => $this->RandomPassword(6),
                                                        'Worth' => $discount_referral_data_array[3],
                                                        'DType' => $discount_referral_data_array[4],
                                                        'DiscountFor' => "Referral",
                                                        'Status' => "Active",
                                                        'CodeUsed' => "One Time",
                                                        'CreatedBy' => 0,
                                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                                    );
                                                    $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                }
                                            }
                                        }
                                    }
                                    //$this->SendInvoiceEmail($invoice_number,"C");
                                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));
                                    if ($sms_responder_record != false) {
                                        $sms_field_array = array(
                                            'Invoice Number' => $invoice_number,
                                            'Loyalty Points' => $member_data_array['TotalLoyaltyPoints'] - $member_data_array['UsedLoyaltyPoints'],
                                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                            'Currency' => $this->config->item("currency_sign"),
                                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                            'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                            'Pick Date' => date('d M Y', strtotime($pick_date)),
                                            'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                                        );
                                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                        // $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data_array['Phone'], 0), $msg_content);
                                    }
                                }
                                $response_array["result"] = "Success";
                                if ($is_success) {

                                    $description_tookan = '
									';
                                    if (isset($tookan_invoice_service) && !empty($tookan_invoice_service)) {

                                        foreach ($tookan_invoice_service as $keyis => $valueis) {
                                            $description_tookan .= '
											' . ($keyis + 1) . ' # Service : ' . $valueis['Title'] . '
											';
                                            $description_tookan .= '# Quantity : ' . $valueis['Quantity'] . '
											';
                                            $description_tookan .= '# Price : ' . $valueis['Price'] . '
											';
                                            $description_tookan .= '# Total : ' . $valueis['Total'] . '

											';
                                        }
                                    }

                                    if ($invoice_record != false) {
                                        //$this->SendInvoiceEmail($invoice_number, "U");
                                        $response_array["message"] = "Invoice Updated Successfully #" . $invoice_number;
                                    }

                                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
                                    if ($invoice_record != false) {
                                        $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone", array('PKMemberID' => $invoice_record['FKMemberID']));
                                        $member_address = '';


                                        $member_address = $invoice_record['BuildingName'];

                                        if (!empty($invoice_record['StreetName'])) {
                                            $member_address .= ', ' . $invoice_record['StreetName'];
                                        }


                                        $member_address .= ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];

                                        $pick_time_array = explode("-", $invoice_record['PickupTime']);
                                        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);
                                        $tookan_array = array(
                                            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                            'order_id' => $invoice_record['InvoiceNumber'],
                                            'team_id' => "21339",
                                            'auto_assignment' => 0,
                                            'job_description' => $invoice_record['OrderNotes'] . $description_tookan,
                                            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'job_pickup_email' => $member_record['EmailAddress'],
                                            'job_pickup_address' => $member_address,
                                            'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                                            'customer_email' => $member_record['EmailAddress'],
                                            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'customer_address' => $member_address,
                                            'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                                            'has_pickup' => "1",
                                            'has_delivery' => "1",
                                            'layout_type' => "0",
                                            'timezone' => "-240",
                                            'custom_field_template' => "",
                                            'meta_data' => array(),
                                            'tracking_link' => "1",
                                            //'notify' => "1",
                                            'geofence' => 1
                                        );
                                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                                        $update_invoice_record = array();
                                        $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
                                        $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
                                        if (isset($tooken_response_array['data']['job_id'])) {
                                            $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];
                                            $tookan_response_result = "";
                                            if ($invoice_record['PaymentStatus'] == "Completed") {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                /* Hassan changes 24-5-2018 */
                                                if (isset($tooken_response_array['data']['delivery_job_id'])) {
                                                    $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                                                    $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                }
                                                /* Hassan changes ends 24-5-2018 */
                                            } else {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                                $this->deleteDeliveryTask($tooken_response_array);
                                                $update_invoice_record['TookanResponse'] = null;
                                            }
                                            $update_invoice_record['TookanUpdateResponse'] = $tookan_response_result;
                                        } else {
                                            //if ($invoice_record['PaymentStatus'] == "Completed") {
                                            $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/create_task", json_encode($tookan_array));
                                            $update_invoice_record['TookanResponse'] = $tookan_response_result;
                                            //}
                                        }
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                                    }
                                }
                            }
                        }
                    } else {
                        $date_records = $this->_date_records($franchise_data_array['ID']);
                        if (sizeof($date_records) > 0) {
                            $response_array["pick_date_data"] = $date_records["pick"];
                            $response_array["delivery_date_data"] = $date_records["delivery"];
                        }
                    }
                }
            }
        }
        $response_array["InvoiceID"] = $id;
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    //// for ios 4.11 and android 4.13 
    public function post_update_order()
    {
        error_reporting(E_ALL);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["type"] = "Message";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $log = http_build_query($post_data);

        file_put_contents('./application/logs/update_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);

        if ($post_data) {
            $card_id = intval(isset($post_data['card_id']) ? $post_data['card_id'] : null);
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $appVersion = isset($post_data['version']) ? $post_data['version'] : "";
            $paymentMethod = isset($post_data['payment_method']) ? $post_data['payment_method'] : "Stripe";
            $id = intval(isset($post_data['id']) ? $post_data['id'] : '');
            $member_id = intval(isset($post_data['member_id']) ? $post_data['member_id'] : '');
            $franchise_id = intval(isset($post_data['franchise_id']) ? $post_data['franchise_id'] : '');
            $platform = intval(isset($post_data['platform']) ? $post_data['platform'] : '');

            if (!empty($device_id) && !empty($id)) {
                $this->load->model('preferences');

                $this->_update_device_information($device_id);

                $member_data = $this->_member_data_record($member_id);
                $franchise_record = $this->_franchise_record($franchise_id);

                $member_data_array = $member_data;
                $member_record = $member_data;
                $franchise_data_array = $franchise_record;

                if (!empty($member_data) && !empty($franchise_record)) {

                    $stripe_customer_id = $member_record["StripeCustomerID"];

                    $discount_referral_data = isset($post_data['discount_referral_data']) ? $post_data['discount_referral_data'] : '';
                    $preferences_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
                    $services_total = floatval(isset($post_data['service_total']) ? $post_data['service_total'] : '0.00');
                    $preference_total = floatval(isset($post_data['preference_total']) ? $post_data['preference_total'] : '0.00');
                    $sub_total = floatval(isset($post_data['sub_total']) ? $post_data['sub_total'] : '');
                    $discount_total = floatval(isset($post_data['discount_total']) ? $post_data['discount_total'] : '0.00');
                    $service_data = isset($post_data['services_data']) ? $post_data['services_data'] : '';
                    $is_valid = true;


                    $invoice_recordTmp = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
                    $invoice_record = $invoice_recordTmp;
                    $oldTotal = $invoice_recordTmp["GrandTotal"];
                    $invoice_number = $invoice_recordTmp['InvoiceNumber'];

                    $card_id = $invoice_recordTmp["FKCardID"];

                    /* Hassan changes 1-4-2018 */

                    $pick_dateTMP = $invoice_recordTmp['PickupDate'];
                    $pick_timeTMP = $invoice_recordTmp['PickupTime'];
                    if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                        $pick_timeArray = explode('-', $pick_timeTMP);
                        $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
                    } else {
                        $pick_timeClean = '';
                    }
                    $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

                    $delivery_dateTMP = $invoice_recordTmp['DeliveryDate'];
                    $delivery_timeTMP = $invoice_recordTmp['DeliveryTime'];

                    if (isset($delivery_dateTMP) && $delivery_timeTMP != '') {
                        $delivery_timeArray = explode('-', $delivery_timeTMP);
                        $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
                    } else {
                        $delivery_timeClean = '';
                    }

                    $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

                    $differenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);

                    if ($differenceTMP < $franchise_data_array["DeliveryDifferenceHour"]) {
                        $is_valid = false;
                        $response_array["type"] = "Date Time";
                        $response_array["message"] = "Difference between Pickup and Delivery Date Time should be greater than " . $franchise_data_array["DeliveryDifferenceHour"] . " hours";
                    }

                    if ($invoice_recordTmp['OrderStatus'] == "Pending" || $invoice_recordTmp['OrderStatus'] == "Processed") {

                        $is_edit_button_show = true;
                        $pickup_time_array = explode("-", $invoice_recordTmp['PickupTime']);
                        $date1 = new DateTime(date('Y-m-d', strtotime($invoice_recordTmp['PickupDate'])) . 'T' . $pickup_time_array[0] . ':00');
                        $date2 = new DateTime(date('Y-m-d') . 'T' . date('H:i:s'));

                        // The diff-methods returns a new DateInterval-object...

                        $interval = $date2->diff($date1);
                        $invert = $interval->invert;

                        $h = $interval->format('%h');
                        $days = $interval->format('%d');
                        $hour = ($days * 24) + $h;

                        if ($invert > 0) {
                            $is_edit_button_show = false;
                        } else {
                            if ($hour <= $franchise_data_array["PickupDifferenceHour"]) {
                                $is_edit_button_show = false;
                            }
                        }
                        if (!$is_edit_button_show) {
                            $is_valid = false;
                            $response_array["message"] = "Dear Customer, You cannot update order, please contact our customer support";
                        }
                    }

                    $response_array["is_valid"] = $is_valid;


                    if ($is_valid) {

                        $services_total = 0;
                        $insert_invoice_service = array();

                        $hasPreferences = false;
                        $tookan_invoice_service = array();

                        $invoice_data = array(
                            'FKDeviceID' => $device_id,
                            'FKMemberID' => $member_id,
                            'FKFranchiseID' => $franchise_id,
                            'PaymentMethod' => $paymentMethod,
                            'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                            'OrderPostFrom' => 'Mobile',
                            // 'AppVersion' => $appVersion
                        );

                        $invoice_data['ID'] = $id;
                        $discountAmount = 0.00;
                        if (strlen($service_data) > 10) {
                            $service_data_array = json_decode($service_data, true);

                            if (sizeof($service_data_array) > 0) {

                                $i = 0;
                                foreach ($service_data_array as $cart_data) {
                                    $total = floatval(intval($cart_data['Quantity']) * floatval($cart_data['Price'])); /* Cart services calculation hassan 1-13-2019 */
                                    $services_total = $services_total + $total;

                                    $insert_invoice_service = array(
                                        'FKInvoiceID' => $id,
                                        'FKServiceID' => $cart_data['ID'],
                                        'FKCategoryID' => $cart_data['CategoryID'],
                                        'Title' => $cart_data['Title'],
                                        'DesktopImageName' => $cart_data['DesktopImagePath'],
                                        'MobileImageName' => $cart_data['MobileImage'],
                                        'IsPackage' => $cart_data['IsPackage'],
                                        'PreferencesShow' => $cart_data['PreferencesShow'],
                                        'Quantity' => $cart_data['Quantity'],
                                        'Price' => $cart_data['Price'],
                                        'Total' => $total
                                    );

                                    if ($cart_data['IsPackage'] == "No") {
                                        $discountAmount += $total;
                                    }

                                    $hasPreferences = ($hasPreferences == true) or ($cart_data['PreferencesShow'] == "Yes");
                                    $tookan_invoice_service[] = $insert_invoice_service;
                                    $i++;
                                }
                                $services_total = floatval($services_total);
                                $services_total = numberformat($services_total);
                            }
                        } else {
                            $services_total = $oldTotal;
                        }


                        $preferences_total = 0;
                        $insert_invoice_preference = [];



                        if ($preferences_data != "") {
                            $preferences_data_array = explode(",", $preferences_data);



                            if (count($preferences_data_array) > 0) {
                                $preferences = $this->preferences->where_in("PKPreferenceID", $preferences_data_array);

                                $i = 0;
                                foreach ($preferences as $rec) {

                                    if (sizeof($rec) > 0) {
                                        $insert_invoice_preference[$i] = array(
                                            'FKInvoiceID' => $id,
                                            'FKPreferenceID' => $rec["PKPreferenceID"],
                                            'Title' => $rec["Title"]
                                        );
                                        $p = $this->model_database->GetRecord($this->tbl_preferences, 'Title', array('PKPreferenceID' => $rec["ParentPreferenceID"]));
                                        $insert_invoice_preference[$i]["ParentTitle"] = $p["Title"];

                                        $preference_price = floatval($rec["Price"]);
                                        if ($preference_price > 0) {
                                            $insert_invoice_preference[$i]["Price"] = $preference_price;
                                            $insert_invoice_preference[$i]["Total"] = floatval($preference_price);
                                        } else {
                                            $insert_invoice_preference[$i]["Price"] = 0;
                                            $insert_invoice_preference[$i]["Total"] = 0;
                                        }
                                        $preferences_total = $preferences_total + $preference_price;
                                        $i++;
                                    }
                                }
                            }
                            $preferences_total = numberformat($preferences_total);
                        }




                        if ($sub_total > 0) {
                            $invoice_data['SubTotal'] = $services_total + $preference_total;
                        }

                        $invoice_data['ServicesTotal'] = numberformat($services_total);
                        $invoice_data['PreferenceTotal'] = numberformat($preference_total);
                        $invoice_data['GrandTotal'] = $invoice_data['ServicesTotal'] + $invoice_data['PreferenceTotal'];

                        $errorInDiscount = false;

                        $discountKey = "FKDiscountID";

                        $discountType = "None";
                        $discountCode = NULL;
                        $discountWorth = NULL;
                        $dType = "None";

                        $discount_id = $invoice_record["FKDiscountID"];
                        $grandTotal = $invoice_data['GrandTotal'];
                        $discount = 0;



                        if ($discount_id != NULL) {
                            $discount_type = $invoice_record["DiscountType"];
                            //$cartServices = json_decode($service_data, true);
                            if ($discount_type == "Discount") {
                                $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('PKDiscountID' => $discount_id, 'Status' => 'Active'));

                                $worth = $discount_record['Worth'];
                                $dType = $discount_record["DType"];
                                $code = $discount_record["Code"];
                                if ($grandTotal < $discount_record['MinimumOrderAmount']) {
                                    $errorInDiscount = true;
                                }
                            } else {

                                $discount_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $discount_id));
                                $code = $discount_record["ReferralCode"];
                                $worth = $this->GetReferralCodeAmount();
                                $dType = "Price";
                                $discountKey = "ReferralID";
                            }


                            if ($errorInDiscount == true) {
                                $discount_id = NULL;
                                $discountType = "None";
                                $discountCode = NULL;
                                $discountWorth = NULL;
                                $dType = "None";
                            } else {



                                if ($discountAmount > 0) {
                                    if ($dType == "Percentage") {
                                        $discount = ($discountAmount / 100) * $worth;
                                    } else {
                                        $discount = $worth;
                                    }
                                }

                                $grandTotal -= $discount;
                                $discountType = ucfirst($discount_type);
                                $discountCode = $code;
                                $discountWorth = $worth;
                                $invoice_data['DiscountTotal'] = $discount;
                            }
                        }

                        $invoice_data['GrandTotal'] = numberformat($grandTotal);


                        if ($invoice_record != false) {

                            $response_array['InvoiceNumber'] = $invoice_number;
                            if ($invoice_record["PaymentStatus"] == "Completed") {
                                if (floatval($invoice_data['GrandTotal']) < $oldTotal) {
                                    $is_valid = false;
                                    $response_array["message"] = "Dear Customer, You cannot reduce your order amount from the cart, please contact our customer support";
                                }
                            }
                        }



                        if ($is_valid) {

                            //$card_id = null;

                            if ($paymentMethod == "Stripe") {
                                //$stripe_grand_total = floatval(0);
                                $is_test = false;
                                $card_token = "";


                                $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Number,Year,Month,Code", array("PKCardID" => $card_id));
                                if ($member_card_record == false) {
                                    $response_array["type"] = "Card";
                                    $response_array["message"] = "Card information not found";
                                    //$response_array['card_data'] = $this->_member_card_records($member_data_array["ID"]);
                                    $is_valid = false;
                                }
                                $orderStatus = "Processed";
                                $paymentStatus = "Processed";
                            } else if ($paymentMethod == "Googlepay" || $paymentMethod == "Applepay") {
                                $card_id = null;
                                $orderStatus = "Processed";
                                $paymentStatus = "Processed";
                            } else {
                                $card_id = null;
                                $orderStatus = "Processed";
                                $paymentStatus = "Pending";
                            }

                            if ($is_valid) {

                                $is_discount_found = false;
                                if (!empty($discount_referral_data)) {
                                    $discount_referral_data_array = explode('|', $discount_referral_data);
                                    if (sizeof($discount_referral_data_array) > 0) {
                                        if ($discount_referral_data_array[0] == "Discount") {
                                            $invoice_data['DiscountType'] = "Discount";
                                            $invoice_data['FKDiscountID'] = $discount_referral_data_array[1];
                                        } else {
                                            $invoice_data['DiscountType'] = "Referral";
                                            $invoice_data['ReferralID'] = $discount_referral_data_array[1];
                                        }
                                        $invoice_data['DiscountCode'] = $discount_referral_data_array[2];
                                        $invoice_data['DiscountWorth'] = $discount_referral_data_array[3];
                                        $invoice_data['DType'] = $discount_referral_data_array[4];
                                        $is_discount_found = true;
                                    }
                                }

                                if (!$is_discount_found) {
                                    $invoice_data['DiscountType'] = "None";
                                    $invoice_data['FKDiscountID'] = null;
                                    $invoice_data['ReferralID'] = null;
                                    $invoice_data['DiscountCode'] = null;
                                    $invoice_data['DiscountWorth'] = null;
                                    $invoice_data['DType'] = "None";
                                }


                                $this->model_database->RemoveRecord($this->tbl_invoice_services, $id, "FKInvoiceID");

                                $response_array["InvoiceID"] = $id;
                                $is_service_found = false;

                                $hasPreferences = false;



                                $service_total = 0;
                                $services = [];
                                if (strlen($service_data) > 10) {
                                    $service_data_array = json_decode($service_data, true);

                                    if (sizeof($service_data_array) > 0) {
                                        $k = 0;
                                        foreach ($service_data_array as $cart_data) {
                                            $total = floatval(intval($cart_data['Quantity']) * floatval($cart_data['Price'])); /* Cart services calculation hassan 1-13-2019 */

                                            $insert_invoice_service = array(
                                                'FKInvoiceID' => $id,
                                                'FKServiceID' => $cart_data['ID'],
                                                'FKCategoryID' => $cart_data['CategoryID'],
                                                'Title' => $cart_data['Title'],
                                                'DesktopImageName' => $cart_data['DesktopImagePath'],
                                                'MobileImageName' => $cart_data['MobileImage'],
                                                'IsPackage' => $cart_data['IsPackage'],
                                                'PreferencesShow' => $cart_data['PreferencesShow'],
                                                'Quantity' => $cart_data['Quantity'],
                                                'Price' => $cart_data['Price'],
                                                'Total' => $total
                                            );


                                            $services[$k]["service_id"] = $cart_data['ID'];
                                            $services[$k]["title"] = $cart_data['Title'];
                                            $services[$k]["price"] = $cart_data['Price'];
                                            $services[$k]["quantity"] = $cart_data['Quantity'];
                                            $services[$k]["total"] = $cart_data['Total'];

                                            $tookan_invoice_service[] = $insert_invoice_service;
                                            $k++;
                                            $invoice_service_id = $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                            if ($invoice_service_id != 0) {
                                                $is_service_found = true;
                                            }
                                        }
                                    }
                                }

                                if ($is_service_found) {
                                    $invoice_data['InvoiceType'] = "Items";
                                } else {
                                    $invoice_data['InvoiceType'] = "After";
                                }

                                $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $id, "FKInvoiceID");
                                if (count($insert_invoice_preference) > 0) {


                                    foreach ($insert_invoice_preference as $p) {

                                        $insert_preference = array();
                                        $insert_preference["FKInvoiceID"] = $id;
                                        $insert_preference["FKPreferenceID"] = $p["FKPreferenceID"];
                                        $insert_preference["Title"] = $p["Title"];
                                        $insert_preference["Price"] = $p["Price"];
                                        $insert_preference["Total"] = $p["Price"];
                                        $insert_preference["ParentTitle"] = $p["ParentTitle"];
                                        $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_preference);
                                    }
                                }



                                $invoice_data['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                //$this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");


                                $is_success = true;

                                $response_array["grand_total"] = $invoice_data['GrandTotal'];
                                $grand_total = $invoice_data['GrandTotal'];



                                if ($invoice_data['GrandTotal'] > 0) {


                                    $stripe_grand_total = $invoice_data['GrandTotal'] - $oldTotal;
                                    if ($paymentMethod != "Cash") {
                                        $intentId = "";

                                        if ($stripe_grand_total > 0) {
                                            $convertAmount = $this->country->convertAmount($stripe_grand_total);
                                            $amount = (int) ($convertAmount * 100);

                                            $stripe = new Stripe();
                                            $data = [
                                                'payment_method' => $invoice_record['PaymentReference'],
                                                'amount' => $amount,
                                                'customer' => $stripe_customer_id,
                                                'payment_method_types' => ['card'],
                                                'currency' => $this->config->item("currency_to"),
                                                'confirmation_method' => 'manual',
                                                'confirm' => true,
                                                'setup_future_usage' => 'off_session',
                                                'description' => "Invoice #" . $invoice_number
                                            ];




                                            $stripeKey = $this->GetStripeAPIKey();
                                            $intent = $stripe->purchase($stripeKey, $data);

                                            $intentResponse = json_encode($intent);
                                            $intentId = $intentResponse;
                                            $orderStatus = $invoice_record["OrderStatus"];
                                            $paymentStatus = $invoice_record["PaymentStatus"];

                                            if (isset($intent->id)) {
                                                $payment_information_data = array(
                                                    'InvoiceNumber' => $invoice_number,
                                                    'Content' => $intentId,
                                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                                );
                                                $this->model_database->InsertRecord($this->tbl_payment_informations, $payment_information_data);
                                                $invoice_data['PaymentToken'] = $intentId;
                                                $invoice_data['PaymentReference'] = $invoice_record['PaymentReference'];
                                            }
                                        }
                                    } else {

                                        $member_card_record['Code'] = null;
                                        $intentResponse = "Cash";
                                        $intentId = "Cash";
                                        $credit_card_id = null;
                                        $orderStatus = "Processed";
                                        $paymentStatus = "Pending";
                                    }

                                    if (!empty($intentId)) {


                                        $invoice_data['OrderStatus'] = $orderStatus;
                                        $invoice_data['PaymentStatus'] = $paymentStatus;
                                        $invoice_data['PaymentMethod'] = $paymentMethod;
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');

                                        $insert_invoice_payment_information = array(
                                            'FKInvoiceID' => $id,
                                            'FKCardID' => $card_id,
                                            'Amount' => $stripe_grand_total,
                                            'PaymentReference' => $invoice_record['PaymentReference'],
                                            'PaymentToken' => $intentId,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                                    }




                                    if ($invoice_data['InvoiceType'] == "Items") {


                                        if (!$is_discount_found) {
                                            $loyalty_points = intval($grand_total);
                                            $minus_loyalty_points = 0;
                                            $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_data['ID']));
                                            $loyalty_data = array(
                                                'FKMemberID' => $member_id,
                                                'FKInvoiceID' => $id,
                                                'InvoiceNumber' => $invoice_number,
                                                'GrandTotal' => $grand_total,
                                                'Points' => $loyalty_points
                                            );



                                            if ($invoice_loyalty_record != false) {
                                                $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                                $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                                $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                                $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                            } else {
                                                $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                                $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                            }
                                            $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                            $total_loyalty_points = intval($member_data_array['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                            $used_loyalty_points = intval($member_data_array['UsedLoyaltyPoints']);
                                            $total_loyalty_points += $loyalty_points;
                                            $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;


                                            if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                                $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                                for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                                    $loyalty_code = $this->RandomPassword(6);
                                                    $insert_discount = array(
                                                        'FKMemberID' => $member_id,
                                                        'Code' => $loyalty_code,
                                                        'Worth' => $this->GetLoyaltyCodeAmount(),
                                                        'DType' => "Price",
                                                        'DiscountFor' => "Loyalty",
                                                        'Status' => "Active",
                                                        'CodeUsed' => "One Time",
                                                        'CreatedBy' => 0,
                                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                                    );
                                                    $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                    $used_loyalty_points += $minimum_loyalty_points;
                                                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                                    if ($sms_responder_record != false) {
                                                        $sms_field_array = array(
                                                            'Loyalty Points' => $remain_loyalty_points,
                                                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                            'Currency' => $this->config->item("currency_sign"),
                                                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                            'Discount Code' => $loyalty_code
                                                        );
                                                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                                        // $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data['Phone'], 0), $msg_content);
                                                    }
                                                }
                                            }


                                            $update_member = array(
                                                'TotalLoyaltyPoints' => $total_loyalty_points,
                                                'UsedLoyaltyPoints' => $used_loyalty_points,
                                                'ID' => $member_id
                                            );




                                            $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                        } else {
                                            if ($is_discount_found) {
                                                $discount_referral_data_array = explode('|', $discount_referral_data);
                                                if ($discount_referral_data_array[0] != "Discount") {
                                                    $insert_discount = array(
                                                        'FKMemberID' => $discount_referral_data_array[1],
                                                        'Code' => $this->RandomPassword(6),
                                                        'Worth' => $discount_referral_data_array[3],
                                                        'DType' => $discount_referral_data_array[4],
                                                        'DiscountFor' => "Referral",
                                                        'Status' => "Active",
                                                        'CodeUsed' => "One Time",
                                                        'CreatedBy' => 0,
                                                        'CreatedDateTime' => date('Y-m-d H:i:s')
                                                    );
                                                    $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                }
                                            }
                                        }
                                    }
                                    //$this->SendInvoiceEmail($invoice_number,"C");
                                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));


                                    if ($sms_responder_record != false) {

                                        $pick_date = $invoice_recordTmp['PickupDate'];
                                        $pick_time = $invoice_recordTmp['PickupTime'];

                                        $sms_field_array = array(
                                            'Invoice Number' => $invoice_number,
                                            'Loyalty Points' => $member_data_array['TotalLoyaltyPoints'] - $member_data_array['UsedLoyaltyPoints'],
                                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                            'Currency' => $this->config->item("currency_to"),
                                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                            'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                            'Pick Date' => date('d M Y', strtotime($pick_date)),
                                            'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                                        );
                                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                        //$this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data_array['Phone'], 0), $msg_content);
                                    }
                                }
                                $response_array["result"] = "Success";
                                $response_array["message"] = "Invoice Updated Successfully #" . $invoice_number;

                                if ($is_success) {
                                    // $record = $this->SendInvoiceEmail($invoice_number, "U");

                                    if ($this->config->item("onfleet_enabled") == true) {

                                        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
                                        $updateResponse = $this->country->updateTaskServices($services, $invoice, $member_record);
                                        // $invoice_data["OnfleetResponse"] = $updateResponse;
                                        // $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');
                                    } else {
                                    }

                                    //$this->saveToTookaan($record, $member_record, "update");
                                }
                            }
                        }
                    } else {
                        $date_records = $this->_date_records($franchise_data_array['ID']);
                        if (sizeof($date_records) > 0) {
                            $response_array["pick_date_data"] = $date_records["pick"];
                            $response_array["delivery_date_data"] = $date_records["delivery"];
                        }
                    }
                }
            }
        }
        $response_array["InvoiceID"] = $id;
        echo jsonencode($response_array, 200); // 200 being the HTTP response code
    }

    function post_invoice_save_timings()
    {
        $post = $this->input->post(NULL, TRUE);
        $id = $post["id"];
        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
        $franchise = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $invoice["FKFranchiseID"]));


        $response["error"] = false;
        $response["errors"] = [];
        //$response["franchise"] = $franchise;
        $i = 0;
        $header = 200;

        if (empty($post["pickup_date"])) {
            $response["error"] = true;
            $response["errors"]["pickup_date"] = "Pickup date required.";
        }

        if (empty($post["pickup_time"])) {
            $response["error"] = true;
            $response["errors"]["pickup_time"] = "Pickup time required.";
        }

        if (empty($post["delivery_date"])) {
            $response["error"] = true;
            $response["errors"]["delivery_date"] = "Pickup date required.";
        }

        if (empty($post["delivery_time"])) {
            $response["error"] = true;
            $response["errors"]["delivery_time"] = "Pickup time required.";
        }

        if ($post['pickup_date'] && $post['delivery_date']) {
            $pick_dateTMP = $post['pickup_date'];
            $pick_timeTMP = $post['pickup_time'];
            if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                $pick_timeArray = explode('-', $pick_timeTMP);
                $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
            } else {
                $pick_timeClean = '';
            }
            $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

            $now = date("Y-m-d H:i:s");
            $diffrencePickup = $this->diffTime($now, $pickupDateTimeTMP);

            $delivery_dateTMP = $post['delivery_date'];
            $delivery_timeTMP = $post['delivery_time'];

            if (isset($delivery_timeTMP) && $delivery_timeTMP != '') {
                $delivery_timeArray = explode('-', $delivery_timeTMP);
                $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
            } else {
                $delivery_timeClean = '';
            }

            $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

            $diffrenceTMP = $this->diffTime($pickupDateTimeTMP, $deliveryDateTimeTMP);


            if ($diffrencePickup < $franchise["PickupDifferenceHour"] && empty($response["errors"])) {
                $response["message"] = "Order Pickup difference should be at least " . $franchise["PickupDifferenceHour"] . " hours , please contact our customer support";
                //$response["error"] = true;
            } else {
                $data['PickupDate'] = $post["pickup_date"];
                $data['PickupTime'] = $post["pickup_time"];
            }

            if ($diffrenceTMP < $franchise["DeliveryDifferenceHour"]) {
                $response["error"] = true;
                $response["message"] = "Order delivery difference should be at least " . $franchise["DeliveryDifferenceHour"] . " hours , please contact our customer support.";
            }
        }
        $response['now'] = $now;
        $response['DiffrencePickup'] = $diffrencePickup;
        $response['DiffrenceDelivery'] = $diffrenceTMP;

        if ($response["error"] == false) {

            $member_id = $this->GetCurrentMemberID();
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));

            $header = 200;

            $data['DeliveryDate'] = $post["delivery_date"];
            $data['DeliveryTime'] = $post["delivery_time"];
            $data['OrderNotes'] = !empty($post["notes"]) ? $post["notes"] : "";
            $data['ID'] = $id;

            $response['message'] = "Collection and delivery time has been updated.";
            $this->model_database->UpdateRecord($this->tbl_invoices, $data, 'PKInvoiceID');

            /*
            if ($this->config->item("onfleet_enabled") == true) {

                $invoice["PickupDate"] = $data['PickupDate'];
                $invoice["PickupTime"] = $data['PickupTime'];
                $invoice["DeliveryDate"] = $data['DeliveryDate'];
                $invoice["DeliveryTime"] = $data['DeliveryTime'];
                $invoice["OrderNotes"] = $data['OrderNotes'];
                $updateResponse = $this->country->updateTaskTimings($record['invoice'], $member_record);
            } else {
            }*/


            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
            $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $id), false, false, false, "PKInvoiceServiceID", "asc");

            $record["invoice"] = $invoice_record;
            $record["services"] = $invoice_service_records;
            $record["member"] = $member_record;

            $this->saveToTookaan($record, "update");

            $record = $this->SendInvoiceEmail($record, "U");
        }
        echo jsonencode($response, 200);
    }

    function can_change_timings($franchise_id)
    {
        $post = $this->input->get(NULL, TRUE);


        $franchise = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $franchise_id));

        $response["error"] = false;
        $response["errors"] = [];
        //$response["franchise"] = $franchise;
        $i = 0;
        $header = 200;
        $type = $post["type"];
        $dateTime = $post["time"];
        if (!empty($post["time"]) && !empty($post["time"])) {

            list($date, $time) = explode(' ', $post["time"]);

            if (isset($date) && $time != '') {
                $timeArray = explode('-', $time);
                $timeClean = isset($timeArray[0]) ? $timeArray[0] . ':00' : '';
            } else {
                $timeClean = '';
            }

            $dateTimeTMP = $date . ' ' . $timeClean;
            if ($type == "pickup") {
                $now = date("Y-m-d H:i:s");
                $difference = $this->diffTime($now, $dateTimeTMP);
                $franchiseHour = $franchise["PickupDifferenceHour"];
                $message = "Dear Customer, You cannot change pickup time. Please contact our customer support";
            } else {
                //$diffrence = $this->diffTime($now, $dateTimeTMP);
            }

            if ($difference < $franchiseHour) {
                $response["message"] = $message;
                $response["error"] = true;
            } else {
                $response["error"] = false;
            }
        }
        echo jsonencode($response, 200);
    }

    function diffTime($d1, $d2)
    {

        $datetime1 = date_create($d1);
        $datetime2 = date_create($d2);
        $interval = date_diff($datetime1, $datetime2);
        $days = $interval->format('%a');
        $hour = (int) ((int) $interval->format('%h') + (int) ($days * 24));
        if ($interval->invert == 0) {
            return $hour;
        } else {
            return $hour * -1;
        }
    }

    function post_invoice_re_order_copy()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        //$post_data=$this->input->get(NULL, TRUE);;
        $log = http_build_query($post_data);
        file_put_contents('./application/logs/re_order_invoice_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $type = isset($post_data['type']) ? $post_data['type'] : '';
            if (!empty($id) && !empty($member_id) && !empty($device_id) && !empty($type)) {

                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'InvoicePendingVersion,InvoiceCancelVersion', array('PKMemberID' => $member_id));

                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,TookanResponse", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                    $invoice_number = $invoice_record['InvoiceNumber'];


                    if ($invoice_record != false) {
                        $invoice_data = array(
                            'ID' => $id,
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        if ($type == "Cancel") {
                            $invoice_data['PaymentStatus'] = "Cancel";
                            $invoice_data['OrderStatus'] = "Cancel";
                        } else if ($type == "Re Order") {
                            $invoice_data['PaymentStatus'] = "Pending";
                            $invoice_data['OrderStatus'] = "Pending";
                        }

                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                        $response_array["result"] = "Success";
                        $update_member = array(
                            'ID' => $member_id,
                            'InvoicePendingVersion' => (intval($member_record["InvoicePendingVersion"]) + 1),
                            'InvoiceCancelVersion' => (intval($member_record["InvoiceCancelVersion"]) + 1),
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                        $response_array["member_data"] = $this->_member_data_record($member_id);
                        $response_array['pending_data'] = $this->_member_invoice_records($member_id, "Pending");
                        $response_array['cancel_data'] = $this->_member_invoice_records($member_id, "Cancel");
                        if ($type == "Cancel") {


                            $update_invoice_record = array(
                                'ID' => $invoice_record['PKInvoiceID'],
                                'TookanResponse' => null,
                                'OnfleetResponse' => null,
                            );

                            if ($this->config->item("onfleet_enabled") == true) {
                                $update_invoice_record["OnfleetResponse"] = $this->country->cancelTask($invoice_record);
                            } else {
                            }

                            $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                            if (isset($tooken_response_array['data']['job_id'])) {
                                $tookan_array = array(
                                    'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                    'job_id' => $tooken_response_array['data']['job_id']
                                );
                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                $update_invoice_record["TookanUpdateResponse"] = $tookan_response_result;
                                $this->deleteDeliveryTask($tooken_response_array);
                            }

                            $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");


                            $this->SendInvoiceEmail($invoice_number, "X");
                            $response_array["message"] = "Invoice #" . $invoice_record['InvoiceNumber'] . " moved on cancel invoices. Please email us at info@love2laundry.com for us to process your refund. Without an email, refund will not be processed.";
                        } else if ($type == "Re Order") {
                            $response_array["message"] = "Invoice #" . $invoice_record['InvoiceNumber'] . " moved on pending invoices";
                        }
                    } else {
                        $response_array["message"] = "Love2Laundry : Invoice not found";
                    }
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }


    public function send_invoice_notification()
    {


        $response_array = array();
        $response_array["error"] = false;
        $response_array["message"] = "";
        $post = $this->input->get(NULL, TRUE);

        $number = isset($post['number']) ? $post['number'] : '';
        $type = isset($post['type']) ? $post['type'] : '';
        $id = isset($post['id']) ? $post['id'] : '';

        $response_array["post"] = $post;

        //d($response_array,1);


        if ($type == "insert") {
            $emailType = "C";
        } elseif ($type == "update") {
            $emailType = "U";
        } elseif ($type == "cancel") {
            $emailType = "X";
        }

        if ($id) {

            $invoice_id = $id;

            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));
            $member_id = $invoice_record['FKMemberID'];
            //$invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_id), false, false, false, "PKInvoiceServiceID", "asc");
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $invoice_record['FKMemberID']));




            $sql = "SELECT services.*,categories.Title as category FROM `" . $this->tbl_invoice_services . "` as services join
      ws_categories as categories on categories.PKCategoryID=services.FKCategoryID where services.FKInvoiceID=" . $invoice_id . "";
            $query = $this->db->query($sql);
            $invoice_service_records = $query->result_array();



            $record["invoice"] = $invoice_record;
            $record["services"] = $invoice_service_records;
            $record["member"] = $member_record;

            $this->saveToTookaan($record, $type);
            $this->track($record);
            $this->SendInvoiceEmail($record, $emailType);

            //$this->saveToTookaan($record, $member_record, $type);

            /*
            if ($this->config->item("onfleet_enabled") == true) {

                $response = $this->country->createTask($record, $member_record);
                $update_invoice_record["OnfleetResponse"] = $response;
                $update_invoice_record["ID"] = $id;
                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
            } else {
            }
            */


            if ($emailType == "C") {
                $member_id = $record["FKMemberID"];
                $device_id = $record["FKDeviceId"];




                $this->_update_device_information($device_id, $member_id);


                $member_detail = $member_record;
                $invoice_record = $record['invoice'];
                $pick_date = $invoice_record["PickupDate"];
                $pick_time = $invoice_record["PickupTime"];
                $PaymentStatus = ucfirst($invoice_record['PaymentStatus']);

                if ($PaymentStatus == 'Completed') {
                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));

                    $sms_field_array = array(
                        'Invoice Number' => $number,
                        'Loyalty Points' => $member_detail['TotalLoyaltyPoints'] - $member_detail['UsedLoyaltyPoints'],
                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                        'Currency' => $this->config->item("currency_symbol"),
                        'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                        'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                        'Pick Date' => date('d M Y', strtotime($pick_date)),
                        'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                    );
                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                    $this->SendMessage($sms_responder_record['PKResponderID'], $number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_detail['Phone'], 0), $msg_content);
                }
            } else {
                $this->SendInvoiceEmail($record, $emailType);
            }
            $response_array["message"] = "Success";
            $response_array["error"] = false;
        }
        echo json_encode($response_array);
    }

    public function track($data)
    {

        $this->load->library('tracker');
        $analyticId = $this->config->item("google_analytics_id");
        //
        //UA-54717435-1
        $tracker = new Tracker();
        $tracker->setup(/* web property id */$analyticId, /* client id */ 'l2l', /* user id */ null);

        $transaction_id = $data["invoice"]["InvoiceNumber"];
        $grandTotal = $data["invoice"]["GrandTotal"];
        $appVersion = $data["invoice"]["AppVersion"];

        

        $tracker->send(/* hit type */'event', /* hit properties */ array(
            'eventCategory' => 'Mobile Order ' . $appVersion,
            'eventAction' => 'transaction',
            'eventLabel' => 'Order # ' . $transaction_id
        ));
        $grandTotal =  (float) numberformat($grandTotal);
        $transactionData = array(
            'transactionId' => $transaction_id,
            'transactionAffiliation' => "Love2Laundry",
            'appVersion' => "Mobile",
            'transactionRevenue' => (float) $grandTotal, // not including tax or shipping
            // 'transactionShipping' => =,
            // 'transactionTax' => $total_tax
        );
        // d($transactionData,1);

        $tracker->send('transaction', $transactionData);

        // Send an item record related to the preceding transaction
        //d($data["services"],1);
        if (!empty($data["services"])) {
            foreach ($data["services"] as $service) {
                // d($service,1);
                $tracker->send('item', array(
                    'transactionId' => $transaction_id,
                    'itemName' => $service["Title"],
                    'itemCode' => $service["FKServiceID"],
                    'itemCategory' => $service["category"],
                    'itemPrice' => $service["Price"], // unit price
                    'itemQuantity' => $service["Quantity"]
                ));
            }
        }

        // d($tracker->print(), 1);
    }

    public function post_validate_credit_card()
    {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $response_array["messageCode"] = "0";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $number = isset($post_data['number']) ? $post_data['number'] : '';
            $year = isset($post_data['year']) ? $post_data['year'] : '';
            $month = isset($post_data['month']) ? $post_data['month'] : '';
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($number) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $is_valid = true;
                $card_token_stripe_response = $this->post_stripe_curl_request("https://api.stripe.com/v1/tokens", array('card' => array("number" => $number, "exp_month" => $month, "exp_year" => $year, "cvc" => $code)), $this->is_test);
                if (!empty($card_token_stripe_response)) {
                    $card_token_stripe_response_object = json_decode($card_token_stripe_response, true);
                    if (isset($card_token_stripe_response_object['error'])) {
                        $response_array["message"] = isset($card_token_stripe_response_object['error']['message']) ? $card_token_stripe_response_object['error']['message'] : 'card information not found';
                        $is_valid = false;
                    } else {
                        $response_array["message"] = "Card Information is valid";
                        $response_array["messageCode"] = "1";
                        $is_valid = true;
                    }
                } else {
                    $response_array["message"] = "Invalid Card Information";
                    $response_array["messageCode"] = "0";
                    $is_valid = false;
                }
            } else {
                $response_array["message"] = "Card and device id are required fields";
                $response_array["messageCode"] = "0";
                $is_valid = false;
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    private function pickup_date_records($id, $from = "")
    {
        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);

        $from = ($from != "") ? date("Y-m-d H:i:s", strtotime($from)) : date("Y-m-d H:i:s");

        $franchise_id = $id;
        if ($franchise_record != false) {
            $franchise_date_array['pick'] = array();
            $pick_array_count = 0;

            $closeTimings[] = (date("Y-m-d H:i:s", strtotime($franchise_record["ClosingTime"])));
            $current = $from;

            $current = strtotime($current) + ($franchise_record["PickupDifferenceHour"]) * 3600;



            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $id), false, false, false, false, 'asc');

            foreach ($franchise_timings_record as $row) {
                $closeTimings[] = (date("Y-m-d H:i:s", strtotime($row["ClosingTime"])));
            }

            $lastClose = max($closeTimings);

            if ($current > strtotime($lastClose)) { // start with next day
                $i = 1;
            } else {
                $i = 0;
            }

            for (; $i <= 100; $i++) {
                if ($pick_array_count <= 7) {

                    $date = date($this->config->item('query_date_format'), ($current) + ($i * 86400));
                    $date_class = "Disable";

                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Pickup", $id) == false) {
                            $date_class = "Available";
                            $current_date = date('Y-m-d');
                            if ($current_date == $date) {
                                $current_date_after_add_3_hour = date('Y-m-d', strtotime(date('Y-m-d H:i:s', strtotime('+' . $franchise_record["PickupDifferenceHour"] . ' hours'))));
                                if ($current_date != $current_date_after_add_3_hour) {
                                    $date_class = "Disable";
                                }
                            }
                        }
                    }



                    $day = date("l", strtotime($date));
                    if ($day == "Sunday" || $day == "Saturday") {
                        if ($franchise_record["DeliveryOption"] == "Saturday Sunday Both Pickup Delivery To Monday After 3") {
                            // $date_class = "Disable";
                        } elseif ($franchise_record["DeliveryOption"] == "Saturday Pickup Delivery To Monday After 3") {
                            // $date_class = "Disable";
                        }
                    }

                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['pick'][] = $date_array;
                    $pick_array_count += 1;
                }
                if ($pick_array_count == 7) {
                    break;
                }
            }
        }


        /*
         * if (isset($_GET["debug"])) {
          echo date("Y-m-d H:i:s");
          echo "--";
          echo $lastClose;
          die;
          }
         */

        $franchise_date_array["franchise"] = $franchise_record;

        return ($franchise_date_array);
    }

    public function get_pickup_date_records($id)
    {
        $franchise_date_array = array();
        $get = $this->input->get(NULL, TRUE);
        $franchise_date_array = $this->pickup_date_records($id, $get["from"]);


        echo json_encode($franchise_date_array);
    }

    public function get_collection_schedule($id)
    {
        $franchise_date_array = array();
        $get = $this->input->get(NULL, TRUE);
        $franchise_date_array = $this->pickup_date_records($id, $get["from"]);

        if (empty($get["from"])) {
            $get["from"] = date("Y-m-d");
        }
        ///$hours = $this->time_records($franchise_date_array["franchise"], $get["from"], $search_type = "Pickup", $get["from"]);

        if ($get["debug"] == 1) {
            // d($franchise_date_array, 1);
        }


        $data = array();
        $i = 0;
        foreach ($franchise_date_array["pick"] as $row) {
            $date = array();
            if ($row["Date_Class"] == "Available") {
                $date["date"] = $row["Date_Number"];
                $date["day"] = $row["Date_List"];
                $date["date_display"] = $row["Date_Selected"];
                $date["hours"] = $this->time_records($franchise_date_array["franchise"], $date["date"], $search_type = "Pickup", $date["date"]);

                if (!empty($date["hours"]["data"])) {
                    $data[$i] = $date;
                    $i++;
                }
            } else {
                continue;
            }
        }
        echo jsonencode($data, 200);
    }

    public function get_disable_timings($id)
    {
        $get = $this->input->get(NULL, TRUE);
        $date = $get["date"];
        $hours = explode(",", $get["hours"]);
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;
        $search_with = isset($get["type"]) ? ucfirst($get["type"]) : "Pickup";

        if ($search_with == "Collection") {
            $search_with = "Pickup";
        }


        $response_array = array();
        $response_array["result"] = "Success";

        foreach ($hours as $hour) {

            $time_class = "Available";
            list($from, $to) = explode("-", $hour);
            $search_time = $hour;

            if ($time_class == "Available") {

                $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_id . " and Date='" . $date . "' and Time='" . trim($search_time) . "' and (Type='Both' or Type='" . $search_with . "')");

                if ($disable_slot_record != false) {
                    $time_class = "Disable";
                }
            }
            if ($time_class == "Available") {
                if ($franchise_record['AllowSameTime'] == "No") {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $date . "' and PickupTime='" . $search_time . "') or (DeliveryDate='" . $date . "' and DeliveryTime='" . $search_time . "')");
                    if ($invoice_record_count > 0) {
                        $time_class = "Disable";
                    }
                } else {
                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_id . " and " . $search_with . "Date='" . $date . "' and " . $search_with . "Time='" . $search_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                    if ($invoice_record_count > 0) {
                        if ($search_with == "Pickup") {
                            if ($invoice_record_count >= $franchise_record['PickupLimit']) {
                                $time_class = "Disable";
                            }
                        } else {
                            if ($invoice_record_count >= $franchise_record['DeliveryLimit']) {
                                $time_class = "Disable";
                            }
                        }
                    }
                }
            }
            $time_array[] = array('Class' => $time_class, 'Time' => $search_time, 'Hour' => date('H', strtotime($from)));
        }
        $response_array["hours"]["result"] = "Success";
        $response_array["hours"]["data"] = $time_array;
        echo jsonencode($response_array, 200);
    }

    public function get_delivery_date_records($id)
    {

        $pick_date = isset($_REQUEST['pick_date']) ? $_REQUEST['pick_date'] : '';
        $pick_time_hour = isset($_REQUEST['pick_time_hour']) ? $_REQUEST['pick_time_hour'] : '';
        $franchise_date_array = array();
        $franchise_date_array = $this->delivery_date_records($id, $pick_date, $pick_time_hour);
        echo json_encode($franchise_date_array);
    }

    public function get_delivery_schedule($id)
    {
        $pick_date = isset($_REQUEST['pick_date']) ? $_REQUEST['pick_date'] : '';
        $pick_time_hour = isset($_REQUEST['pick_time_hour']) ? $_REQUEST['pick_time_hour'] : '';
        $franchise_date_array = array();
        $search_type = "Delivery";


        if (is_numeric($pick_time_hour)) {
            $h = $pick_time_hour;
        } else {
            $hours = explode("-", $pick_time_hour);
            $h = date("H", strtotime($hours[0]));
        }

        $franchise_date_array = $this->delivery_date_records($id, $pick_date, $h, 10);
        //d($franchise_date_array,1);
        if (isset($franchise_date_array["delivery"][0]["Date_Number"])) {
            $pickup_date = $franchise_date_array["delivery"][0]["Date_Number"];
            $delivery_date = $franchise_date_array["delivery"][0]["Date_Number"];
        } else {
        }

        $data = array();
        $i = 0;
        foreach ($franchise_date_array["delivery"] as $row) {
            $date = array();
            if ($row["Date_Class"] == "Available") {
                $hours = $this->time_records($franchise_date_array["franchise"], $row["Date_Number"], $search_type, $pick_date, $h);

                if ($hours["hours_list"] != "") {
                    $date["date"] = $row["Date_Number"];
                    $date["day"] = $row["Date_List"];
                    $date["date_display"] = $row["Date_Selected"];
                    $date["hours"] = $hours;

                    $data[$i] = $date;
                    $i++;
                }
            } else {
                continue;
            }
        }
        echo jsonencode($data, 200);
    }

    public function delivery_date_records($id, $pick_date, $pick_time_hour, $limit = 20)
    {

        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;


        if ($pick_date == '') {
            echo 'pickup date is required';
            die;
        } elseif ($pick_date != '') {

            $today_dt = strtotime(date('d-m-Y'));
            $expire_dt = strtotime(date('d-m-Y', strtotime($pick_date)));

            if ($expire_dt < $today_dt) {
                //echo 'pickup date should be greater than ' . date('d-m-Y');
                //die;
            }
        }

        if ($pick_time_hour == 0) {
            $ptime = " 23:59:59";
        } else {
            $ptime = " $pick_time_hour:00:00";
        }
        //$ptime = " $pick_time_hour:00:00";

        $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ ' . $franchise_record['DeliveryDifferenceHour'] . ' hours', strtotime($pick_date . $ptime))); // $now + 3 hours

        $pick_date = date($this->config->item('query_date_format'), strtotime($pick_date));
        $openingTime = $franchise_record["OpeningTime"];
        $closingTime = date("H", strtotime($franchise_record["ClosingTime"]));


        if ($franchise_record != false) {

            $franchise_date_array['delivery'] = array();
            $delivery_array_count = 0;

            $closeTimings[] = (date("Y-m-d H:i:s", strtotime($franchise_record["ClosingTime"])));

            $current = date("Y-m-d H:i:s");
            $current = strtotime($current) + ($franchise_record["DeliveryDifferenceHour"]) * 3600;

            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $id), false, false, false, false, 'asc');

            foreach ($franchise_timings_record as $row) {

                $closeTimings[] = (date("Y-m-d H:i:s", strtotime($row["ClosingTime"])));
            }
            // d($closeTimings,1);
            $lastClose = max($closeTimings);

            if ($current > strtotime($lastClose)) { // start with next day
                $i = 1;
            } else {
                $i = 0;
            }

            if ($i == 1) { // check next date close timeing 
                $h = date("H", strtotime($available_delivery_date));
                $closeTimeHour = date("H", strtotime($lastClose));

                if ($h >= $closeTimeHour) {
                    $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ 1 days', strtotime($available_delivery_date)));
                }
            }



            for (; $i <= 100; $i++) {

                if ($delivery_array_count <= 20) {
                    $date = date($this->config->item('query_date_format'), strtotime($available_delivery_date));
                    $h = date("H", strtotime($available_delivery_date));
                    $date_class = "Disable";



                    if ($pick_date == $date) {

                        $closingTime = date("H", strtotime($lastClose));

                        if ($h >= $closingTime) {
                            $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ 1 days', strtotime($available_delivery_date)));
                            continue;
                        }
                    }

                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Delivery", $id) == false) {
                            $date_class = "Available";
                        }
                    }

                    $day = date("l", strtotime($date));
                    //$day == "Sunday" ||
                    /*
                      if ($day == "Saturday") {
                      if ($franchise_record["DeliveryOption"] == "Saturday Sunday Both Pickup Delivery To Monday After 3") {
                      $date_class = "Disable";
                      } elseif ($franchise_record["DeliveryOption"] == "Saturday Pickup Delivery To Monday After 3") {
                      $date_class = "Disable";
                      }
                      } */

                    if ($date_class == "Available") {
                        if ($pick_date == $date) {
                            if ($franchise_record["AllowSameTime"] == "No") {
                                $date_class = "Disable";
                            }
                        }
                    }


                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );

                    if ($date_class == "Available") {
                        $franchise_date_array['delivery'][] = $date_array;
                    }
                    $delivery_array_count += 1;
                    $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ 1 days', strtotime($available_delivery_date)));
                }
                if ($delivery_array_count == $limit) {
                    break;
                }
            }
        }

        $franchise_date_array["franchise"] = $franchise_record;

        return $franchise_date_array;
    }

    function deleteDeliveryTask($tookanArray)
    {

        $apiKey = '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94';
        $tookan_array = array(
            'api_key' => $apiKey,
            'job_id' => $tookanArray['data']['delivery_job_id']
        );

        $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
    }

    function get_addresses()
    {

        //$this->load->library('loqate');
        //$loqate = new CI_Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));
        //$post_code = $_GET["post_code"];
        //$loqate->getAddresses($post_code);
    }

    function get_referral_amount()
    {

        $row = $this->model_database->GetRecord($this->tbl_settings, 'Content', array('Title' => "Referral Code Amount"));
        $response_array['error'] = false;
        if (isset($row["Content"])) {
            $response_array['amount'] = $row["Content"];
        } else {
            $response_array['amount'] = 5;
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function test_l2l_data()
    {
        $invoice_id = "27967";
        // $member_record = $this->_member_data_record("7591");
        $action = "insert";
        // $this->saveToTookaan($invoice_id, $member_record, $action);
    }
}
