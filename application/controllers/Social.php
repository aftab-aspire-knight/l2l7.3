<?php

//$sql = "ALTER TABLE `ws_members`  ADD `FacebookID` VARCHAR(255) NULL DEFAULT NULL  AFTER `StripeCustomerID`,  ADD `GoogleID` VARCHAR(255) NULL DEFAULT NULL  AFTER `FacebookID`";

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Social extends Base_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('user_agent');
        $this->load->helper('common');
        $this->load->model('cart');
        $this->load->model('cartservices');
        $this->load->database();
        $this->load->library('stripe');
    }

    function login() {

        $i = 0;
        $header = 400;
        $post = $this->input->post(NULL, TRUE);



        if (!empty($post)) {


            if (empty($post["email"])) {
                $response["error"] = true;
                $response["errors"]["email"] = "Your account should have an email address.";
            } else {
                $response = $this->manageLogin($post);
                $response["error"] = false;
                $header = 200;
            }
        } else {
            $response["error"] = true;
            $response["errors"]["method"] = "Invalid Method.";
        }

        echo jsonencode($response, $header);
        //die("sss");
    }

    public function manageLogin($post) {



        $id = $post["id"];
        $email = $post["email"];
        $firstName = $post["first_name"];
        $lastName = $post["last_name"];
        $type = $post["type"];
        $device_id = $post["device_id"];

        $response["email"] = $email;
        $response["first_name"] = $firstName;
        $response["last_name"] = $lastName;
        $response["type"] = $type;
        $response["full_name"] = $response["first_name"] . " " . $response["last_name"];


        if ($type == "facebook") {
            $column = 'FacebookID';
        } else {
            $column = 'GoogleID';
        }

        $member = $this->model_database->GetRecord($this->tbl_members, false, array($column => $id));

        $response["result"] = "login";
        $stripeKey = $this->GetStripeAPIKey();
        $stripe = new Stripe();

        if ($member == false) {
            /// if social id not exist check email 
            $member = $this->model_database->GetRecord($this->tbl_members, false, array('EmailAddress' => $email));

            if ($member == false) {
                $response["result"] = "register";
            }
        }

        if ($response["result"] == "login") {


            $response["id"] = $member['PKMemberID'];
            $response["member_id"] = $member['PKMemberID'];


            $phone = ltrim($member['Phone'], 0);
            $response["phone"] = $phone;
            $response["country_phone_code"] = $this->config->item("country_phone_code");
            $response["country_code"] = $member['CountryCode'];
            $response["referral_code"] = $member['ReferralCode'];


            $update_member = array(
                'ID' => $member['PKMemberID'],
                'FirstName' => $firstName,
                'LastName' => $lastName,
                $column => $id
            );

            if (empty($member["StripeCustomerID"])) {
                $customer = $stripe->saveCustomer($stripeKey, ["email" => $member['EmailAddress'], "name" => $firstName . " " . $lastName]);
                $update_member["StripeCustomerID"] = $customer->id;
            }
//aspire2021knight
            $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');

            if ($device_id == "") {
                $sql = "select PKCardID as id,Title as title,Name as name,Number as number,Year as year,Month as month,Code as code from " . $this->tbl_member_cards . " where FKMemberID=" . $response["member_id"] . " and Year >=" . date("Y");
                $cards = $this->model_database->query($sql);

                $response["cards"] = $cards;
                $cart_id = $this->GetSessionItem("cart_id");
                if (!empty($cart_id)) {
                    $this->cart->update(array("member_id" => $member['PKMemberID']), array("id" => $cart_id));
                }
                $this->_CreateMemberSession($member['PKMemberID']);
            } else {

                $update_device = array(
                    'ID' => $device_id,
                    'FKMemberID' => $member['PKMemberID'],
                    'ActiveDateTime' => date('Y-m-d h:i:s')
                );

                $this->model_database->UpdateRecord($this->tbl_devices_new, $update_device, 'PKDeviceID');
            }


            $response["error"] = false;
            $response["message"] = "Login Successfully...";
        } else {


            $post_code = isset($post['post_code']) ? $post['post_code'] : '';
            $address1 = isset($post['address']) ? $post['address'] : '';
            $address2 = isset($post['address2']) ? $post['address2'] : '';
            $town = isset($post['town']) ? $post['town'] : '';

            if ($device_id == "") {
                $from = "Desktop";

                $this->AddSessionItem("social_id", $id);
                $this->AddSessionItem("type", $type);
                $this->AddSessionItem("reg_email", $email);
                $this->AddSessionItem("reg_first_name", $firstName);
                $this->AddSessionItem("reg_last_name", $lastName);
            } else {
                $from = "Mobile";
            }

            $customer = $stripe->saveCustomer($stripeKey, ["email" => $email, "name" => $firstName . " " . $lastName]);
            $referralCode = $this->random_str($firstName);

            $insert = array(
                'FirstName' => $firstName,
                'LastName' => $lastName,
                'EmailAddress' => $email,
                $column => $id,
                'StripeCustomerId' => $customer->id,
                'PostalCode' => $post_code,
                'BuildingName' => $address1,
                'StreetName' => $address2,
                'Town' => $town,
                'Status' => 'Enabled',
                'RegisterFrom' => $from,
                'ReferralCode' => $referralCode,
                'PopShow' => 'No',
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );

            $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert);


            if ($device_id == "") {
                $response["cards"] = array();
                $cart_id = $this->GetSessionItem("cart_id");
                if (!empty($cart_id)) {
                    $this->cart->update(array("member_id" => $member_id), array("id" => $cart_id));
                }
                $this->_CreateMemberSession($member_id);
            } else {

                $update_device = array(
                    'ID' => $device_id,
                    'FKMemberID' => $member_id,
                    'ActiveDateTime' => date('Y-m-d h:i:s')
                );
                $this->model_database->UpdateRecord($this->tbl_devices_new, $update_device, 'PKDeviceID');
            }


            $response["id"] = $member_id;
            $response["member_id"] = $member_id;
            $response["type"] = $type;
            $response["phone"] = "";
            $response["country_phone_code"] = $this->config->item("country_phone_code");
            $response["country_code"] = $this->config->item("country_phone_code");
            $response["referral_code"] = $referralCode;
            $response["message"] = "Register Successfully...";
            $response["error"] = false;
        }

        return $response;
    }

}
