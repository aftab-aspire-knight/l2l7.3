<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders extends Base_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('user_agent');
        $this->load->helper('common');
    }

    function listing() {
        $get = $this->input->get(NULL, TRUE);
        $data["q"] = $get["q"];
        $data["p"] = $get["p"];

        $member_id = $this->GetCurrentMemberID();
        $data['order_type'] = ucfirst($data["q"]);
        $order_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('FKMemberID' => $member_id, 'OrderStatus' => $data['order_type']));
        if ($order_count > 0) {
            $page_no = 0;
            if (isset($get['p']) && is_numeric($get['p'])) {
                $page_no = $get['p'];
            }

            $page_no = ($page_no * 5);
            if ($page_no >= $order_count) {
                $data['has_more'] = "no";
            } else {
                $data['has_more'] = "yes";
            }
            $data['order_records'] = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,Currency,InvoiceNumber,GrandTotal,PaymentStatus,OrderStatus,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,CreatedDateTime", array('FKMemberID' => $member_id, 'OrderStatus' => $data['order_type']), false, 5, $page_no, "PKInvoiceID");
            foreach ($data['order_records'] as $key => $value) {
                $data['order_records'][$key]['CreatedDate'] = date($this->config->item('q'), strtotime($value['CreatedDateTime']));
                $data['order_records'][$key]['CreatedTime'] = date('h:i a', strtotime($value['CreatedDateTime']));
                $data['order_records'][$key]['PickupDate'] = $value['PickupDate'];
                $data['order_records'][$key]['PickupTime'] = $value['PickupTime'];

                $data['order_records'][$key]['DeliveryDate'] = $value['DeliveryDate'];
                $data['order_records'][$key]['DeliveryTime'] = $value['DeliveryTime'];

                $data['order_records'][$key]['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "Title", array('FKInvoiceID' => $value['PKInvoiceID']));
            }
        }
        $response = $data;
        $response["list"] = $this->load->view('order/listing', $data, true);
        echo jsonencode($response, 200);
    }

    function savetimings() {
        $post = $this->input->post(NULL, TRUE);
        $id = $post["id"];
        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
        $franchise = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $invoice["FKFranchiseID"]));


        $response["error"] = false;
        $response["errors"] = [];
        $i = 0;
        $header = 400;

        if (empty($post["pickup_date"])) {
            $response["error"] = true;
            $response["errors"]["pickup_date"] = "Pickup date required.";
        }

        if (empty($post["pickup_time"])) {
            $response["error"] = true;
            $response["errors"]["pickup_time"] = "Pickup time required.";
        }

        if (empty($post["delivery_date"])) {
            $response["error"] = true;
            $response["errors"]["delivery_date"] = "Pickup date required.";
        }

        if (empty($post["delivery_time"])) {
            $response["error"] = true;
            $response["errors"]["delivery_time"] = "Pickup time required.";
        }

        if ($post['pickup_date'] && $post['delivery_date']) {
            $pick_dateTMP = $post['pickup_date'];
            $pick_timeTMP = $post['pickup_time'];
            if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                $pick_timeArray = explode('-', $pick_timeTMP);
                $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
            } else {
                $pick_timeClean = '';
            }
            $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

            $now = date("Y-m-d H:i:s");
            $diffrencePickup = $this->dateTime_difference($pickupDateTimeTMP, $now);

            $delivery_dateTMP = $post['delivery_date'];
            $delivery_timeTMP = $post['delivery_time'];

            if (isset($delivery_timeTMP) && $delivery_timeTMP != '') {
                $delivery_timeArray = explode('-', $delivery_timeTMP);
                $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
            } else {
                $delivery_timeClean = '';
            }

            $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

            $diffrenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);


            if ($diffrencePickup < $franchise["PickupDifferenceHour"] && empty($response["errors"])) {
                $response["errors"][] = "Dear Customer,<br/>Order Pickup difference should be at least " . $franchise["PickupDifferenceHour"] . " hours , please contact our customer support";
            } elseif ($diffrenceTMP < $franchise["DeliveryDifferenceHour"]) {
                $response["error"] = true;
                $response["errors"]["delivery_time"] = "Dear Customer,<br/>Order delivery difference should be at least " . $franchise["DeliveryDifferenceHour"] . " hours , please contact our customer support.";
            }
        }


        if ($response["error"] == false) {

            $member_id = $this->GetCurrentMemberID();
            $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $member_id));

            $header = 200;
            $invoice["PickupDate"] = $data['PickupDate'] = $post["pickup_date"];
            $invoice["PickupTime"] = $data['PickupTime'] = $post["pickup_time"];
            $invoice["DeliveryDate"] = $data['DeliveryDate'] = $post["delivery_date"];
            $invoice["DeliveryTime"] = $data['DeliveryTime'] = $post["delivery_time"];


            $data['ID'] = $id;

            $response['message'] = "Collection and delivery time has been updated.";

            $this->model_database->UpdateRecord($this->tbl_invoices, $data, 'PKInvoiceID');

            if ($this->config->item("onfleet_enabled")==true) {
                // $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
                // $updateResponse = $this->country->updateTaskTimings($invoice, $member_record);
                // $data["OnfleetResponse"] = $updateResponse;
            } else {
                
            }
            /*
              $this->saveToTookaan($id, $member_record, "update");
             */



            $this->AddSessionItem("invoice_id", $id);
            $this->AddSessionItem("invoice_number", $invoice["InvoiceNumber"]);
            $this->AddSessionItem("OrderAction", "update");
        }
        echo jsonencode($response, $header);
    }

    function items() {

        $this->_URLChecker();

        $respose["error"] = false;
        $franchise_id = $_GET["fid"];
        $invoice_id = $_GET["invoice_id"];

        $service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Quantity,Price,Total,MobileImageName", array('FKInvoiceID' => $invoice_id), false, false, false, "PKInvoiceServiceID");

        foreach ($service_records as $c) {
            $services[$c["FKServiceID"]] = $c["Quantity"];
        }

        $data['services'] = $services;
        $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName,PopupMessage', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');

        if (sizeof($data['category_records']) > 0) {
            foreach ($data['category_records'] as $key => $value) {
                $service_records = $this->model_database->FranchiseCustomServicesSelectItems($franchise_id, $value['PKCategoryID']);
                if (sizeof($service_records) > 0) {
                    $data['category_records'][$key]['service_records'] = $service_records;
                } else {
                    unset($data['category_records'][$key]);
                }
            }
        }

        if (sizeof($data['category_records']) == 0) {
            $respose["error"] = true;
        } else {
            $this->load->view('order/items', $data);
        }
    }

    public function addremoveitems() {

        $get = $this->input->get(NULL, TRUE);
        $response["error"] = true;
        $response["errors"] = [];
        $i = 0;
        $header = 400;

        $franchise_id = $get["franchise_id"];
        $service_id = $get["service_id"];
        $category = $get["category"];
        $category_id = $get["category_id"];
        $preference = $get["preference"];
        $package = $get["package"];
        $qty = $get["qty"];
        $invoice_id = $get["invoice_id"];
        $desktop_image = $get["desktop_image"];
        $mobile_image = $get["mobile_image"];
        $title = $get["title"];


        $member_id = $this->GetCurrentMemberID();
        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id, 'FKMemberId' => $member_id));


        $services = $this->GetSessionItem("services");
        $franchiseService = $this->model_database->GetRecord($this->tbl_franchise_services, "PKFranchiseServiceID,Title,FKServiceID,Title,Price,DiscountPercentage,FKFranchiseID", array('FKServiceID' => $service_id, 'FKFranchiseID' => $franchise_id));

        if (!empty($franchiseService["DiscountPercentage"]) && $franchiseService["DiscountPercentage"] > 0) {
            $discount = $franchiseService["DiscountPercentage"];
            $discountAmount = ($franchiseService["Price"] * $discount) / 100;
            $price = (float) $franchiseService["Price"] - (float) $discountAmount;
        } else {
            $discount = 0;
            $price = $franchiseService["Price"];
        }

        if ($get["type"] == "add") {

            $services[$service_id]["title"] = $title;
            $services[$service_id]["category"] = $category;
            $services[$service_id]["category_id"] = $category_id;
            $services[$service_id]["preference"] = $preference;
            $services[$service_id]["package"] = $package;
            $services[$service_id]["desktop_image"] = $desktop_image;
            $services[$service_id]["mobile_image"] = $mobile_image;
            $services[$service_id]["discount"] = $discount;

            if (key_exists($service_id, $services)) {
                $services[$service_id]["quantity"] = $services[$service_id]["quantity"] + 1;
                $services[$service_id]["price"] = $price;
                $services[$service_id]["total"] = $services[$service_id]["quantity"] * $price;
            } else {

                $services[$service_id]["quantity"] = 1;
                $services[$service_id]["price"] = $price;
                $services[$service_id]["total"] = $price;
            }
            $quantity = $services[$service_id]["quantity"];
            $response["error"] = false;
            $header = 200;
        } else {

            if (key_exists($service_id, $services)) {

                $invoiceService = $this->model_database->GetRecord($this->tbl_invoice_services, "PKInvoiceServiceID,FKCategoryID,Title,FKServiceID,Title,Price,PreferencesShow,Quantity,Total,IsPackage", array('FKServiceID' => $service_id, 'FKInvoiceID' => $invoice_id));
                $quantity = 0;
                if ($invoiceService == false) {
                    if ($qty > 0) {
                        $qty--;
                        if ($qty == 0) {
                            unset($services[$service_id]);
                            $quantity = 0;
                        } else {
                            $services[$service_id]["quantity"] = $qty;
                            $services[$service_id]["price"] = $price;
                            $services[$service_id]["total"] = $services[$service_id]["quantity"] * $price;
                            $quantity = $services[$service_id]["quantity"];
                        }
                        $response["error"] = false;
                        $header = 200;
                    }
                } else {

                    if ($qty > $invoiceService["Quantity"]) {
                        $services[$service_id]["quantity"] = $qty - 1;
                        $services[$service_id]["price"] = $price;
                        $services[$service_id]["total"] = $services[$service_id]["quantity"] * $price;
                        $response["error"] = false;
                        $header = 200;
                        $quantity = $services[$service_id]["quantity"];
                    }
                }
            } else {
                $quantity = 0;
                $services[$service_id]["quantity"] = $quantity;
            }
        }
        $hasPreferences = false;
        $servicesTotal = 0;

        $discountAmount = 0.00;

        foreach ($services as $service) {
            //d($service, 1);
            $servicesTotal += $service["total"];

            if ($service["preference"] == "Yes" && $hasPreferences == false) {
                $hasPreferences = true;
            }

            if ($service["package"] == "No") {
                $discountAmount += $service["total"];
            }
        }

        $discount = 0.00;
        $sum = $servicesTotal;

        if ($invoice["DiscountType"] != "None") {
            $worth = $invoice["DiscountWorth"];
            if ($discountAmount > 0) {
                if ($invoice["DType"] == "Percentage") {
                    $discount = ($discountAmount / 100) * $worth;
                    $sum = ($sum - $discount);
                } else {
                    $discount = $worth;
                }
            }
        }

        $this->AddSessionItem("services", $services);
        $response["discount"] = $discount;
        $response["discount_type"] = $invoice["DType"];
        $response["after_discount"] = $servicesTotal - $discount;
        $response["discount_amount"] = numberformat($discount);
        $response["grand_total"] = numberformat($response["after_discount"] + $invoice["PreferenceTotal"]);
        $response["services_total"] = numberformat($servicesTotal);
        $response["services"] = $services;
        $response["total_services"] = count($services);
        $response["service_id"] = $service_id;
        $response["quantity"] = $quantity;
        $response["has_preferences"] = $hasPreferences;
        $response["currency"] = $this->config->item('currency_symbol');

        echo jsonencode($response, 200);
    }

    public function calculateDiscount($type, $worth) {

        $services = $services = $this->GetSessionItem("services");

        $discountAmount = 0.00;

        foreach ($services as $service) {
            $sum += $service["total"];
            if ($service["package"] == "No") {
                $discountAmount += $service["total"];
            }
        }
        $discount = 0.00;
        if ($discountAmount > 0) {
            if ($type == "Percentage") {
                $discount = ($discountAmount / 100) * $worth;
                $sum = $sum - $discount;
            } else {
                $discount = $worth;
            }
        }
        return numberformat($discount);
    }

    function update() {

        //$this->db->trans_start();
        $post = $this->input->post(NULL, TRUE);
        $id = $post["id"];
        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
        $franchise = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $invoice["FKFranchiseID"]));

        $response["error"] = false;
        $response["errors"] = [];
        $i = 0;
        $header = 400;
        $this->load->model('preferences');
        $services = $this->GetSessionItem("services");

        $stripeKey = $this->GetStripeAPIKey();
        $invoice_id = $id;
        $total = 0;

        $preferencesTotal = 0;
        $servicesTotal = 0;
        $subTotal = 0;


        $hasPreferences = false;
        foreach ($services as $service) {
            $total += $service["total"];

            if ($hasPreferences == false) {
                if ($service["preference"] == "Yes") {
                    $hasPreferences = true;
                }
            }
        }

        $servicesTotal = $total;
        $subTotal = $total;

        $preferences = $post["preferences_list"];

        if ($servicesTotal < $franchise["MinimumOrderAmount"]) {
            $response["error"] = true;
            $response["errors"]["minimum_orders"] = "Minimum order allowed is " . $this->config->item("currency_symbol") . numberformat($franchise["MinimumOrderAmount"]) . ". Your order total is " . $this->config->item("currency_symbol") . numberformat($servicesTotal) . ".";
        } else {
            if (!empty($preferences)) {

                $preferences = $this->preferences->where_in("PKPreferenceID", $preferences);

                foreach ($preferences as $preference) {

                    if ($preference["PriceForPackage"] == "Yes") {
                        $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                    }
                }

                $subTotal += $preferencesTotal;
            }


            $discount_id = $invoice["FKDiscountID"];
            $discount_type = $invoice["DiscountType"]; // ref or Discount
            $dType = $invoice["DType"];
            $worth = $invoice["DiscountWorth"];
            $code = $invoice["DiscountCode"];
            $paymentMethod = $invoice["PaymentMethod"];
            $paymentReference = $invoice["PaymentReference"];
            $paymentToken = $invoice["PaymentToken"];


            $discountTotal = 0.00;


            if (!empty($discount_id)) {

                $discountTotal = $this->calculateDiscount($dType, $worth);
                $total -= $discountTotal;
                $discountType = ucfirst($discount_type);
                $discountCode = $code;
                $discountWorth = $worth;
            } else {
                $discount_id = NULL;
                $discountType = "None";
                $discountCode = NULL;
                $discountWorth = NULL;
                $dType = "None";
            }

            $member_id = $this->GetCurrentMemberID();
            $member = $this->GetCurrentMemberDetail();
            $total += $preferencesTotal;
            $paymetError = false;
            if ($total > $invoice["GrandTotal"]) {
                $this->load->library('stripe');
                $chargeable = $total - $invoice["GrandTotal"];
                $stripeCustomerID = $member["StripeCustomerID"];

                if ($paymentMethod == "Cash") {
                    $paymetError = false;

                    $member_card['Code'] = null;
                    $intentId = "Cash";
                    $credit_card_id = null;
                    $orderStatus = "Processed";
                    $paymentStatus = "Pending";
                } else {
                    $stripe_grand_total = numberformat($chargeable);

                    $stripe = new Stripe();
                    $amount = (INT) ($stripe_grand_total * 100);



                    if ($paymentMethod == "Stripe") {
                        $member_card = $this->model_database->GetRecord($this->tbl_member_cards, false, array('PKCardID' => $invoice['FKCardID'], "FKMemberID" => $member_id));
                        $stripeCode = $member_card["Code"];
                    } else {

                        $stripeCode = $paymentReference;
                    }

                    $invoice_number = $invoice["InvoiceNumber"];
                    $data = [
                        'payment_method' => $stripeCode,
                        'amount' => $amount,
                        'customer' => $stripeCustomerID,
                        'payment_method_types' => ['card'],
                        'currency' => $this->config->item("currency_code"),
                        'confirmation_method' => 'manual',
                        'confirm' => true,
                        'setup_future_usage' => 'off_session',
                        'description' => "Customer #" . $member_id . " Updated Invoice #" . $invoice_number
                    ];




                    try {
                        $intent = $stripe->purchase($stripeKey, $data);

                        $intentId = $intent->id;


                        if (isset($intent["error"])) {
                            $paymetError = true;
                            $response["errors"][] = $intent["message"];
                            throw new Exception();
                        }
                    } catch (Exception $ex) {
                        $paymetError = true;
                        $response["error"] = true;
                        $header = 400;
                        $response["errors"][] = $ex->getMessage();
                    }
                }
            }

            if ($paymetError == false) {

                $invoice_data = array(
                    'SubTotal' => $subTotal,
                    'ServicesTotal' => $servicesTotal,
                    'DiscountTotal' => $discountTotal,
                    'GrandTotal' => $total,
                    'PreferenceTotal' => $preferencesTotal,
                    'IPAddress' => $this->input->ip_address(),
                    'ID' => $invoice_id,
                );



                $insert_invoice_payment_information = array(
                    'FKInvoiceID' => $invoice_id,
                    'FKCardID' => $invoice['FKCardID'],
                    'Amount' => $stripe_grand_total,
                    'PaymentReference' => $stripeCode,
                    'PaymentToken' => $intentId,
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );

                $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);


                foreach ($services as $key => $service) {
                    $invoiceService = $this->model_database->GetRecord($this->tbl_invoice_services, false, array('FKInvoiceID' => $invoice_id, 'FKServiceID' => $key));

                    $invoice_service = array(
                        'FKInvoiceID' => $invoice_id,
                        'FKServiceID' => $key,
                        'FKCategoryID' => $service["category_id"],
                        'Title' => $service["title"],
                        'DesktopImageName' => urldecode($service["desktop_image"]),
                        'MobileImageName' => urldecode($service["mobile_image"]),
                        'IsPackage' => $service["package"],
                        'PreferencesShow' => $service["preference"],
                        'Quantity' => $service["quantity"],
                        'Price' => $service["price"],
                        'Total' => $service["total"]
                    );

                    if ($invoiceService == false) {
                        $this->model_database->InsertRecord($this->tbl_invoice_services, $invoice_service);
                    } else {
                        $data = array();
                        $data['ID'] = $invoiceService["PKInvoiceServiceID"];
                        $data['Quantity'] = $service["quantity"];
                        $data['Price'] = $service["price"];
                        $data['Total'] = $service["total"];
                        $this->model_database->UpdateRecord($this->tbl_invoice_services, $data, 'PKInvoiceServiceID');
                    }
                }


                if ($hasPreferences) {
                    $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $invoice_id, "FKInvoiceID");

                    foreach ($preferences as $preference) {

                        $insert_invoice_preference["FKInvoiceID"] = $invoice_id;
                        $insert_invoice_preference["FKPreferenceID"] = $preference["PKPreferenceID"];
                        $insert_invoice_preference["Title"] = $preference["Title"];
                        $insert_invoice_preference["Price"] = $preference["Price"];
                        $insert_invoice_preference["Total"] = $preference["Price"];
                        if ($preference["ParentPreferenceID"] != 0) {
                            $p = $this->model_database->GetRecord($this->tbl_preferences, 'Title', array('PKPreferenceID' => $preference["ParentPreferenceID"]));
                            $insert_invoice_preference["ParentTitle"] = $p["Title"];
                        }
                        $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                    }
                }


                if ($this->config->item("onfleet_enabled")==true) {
                    //$invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
                    $updateResponse = $this->country->updateTaskServices($services, $invoice, $member);
                    //$invoice_data["OnfleetResponse"] = $updateResponse;
                    //$this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');
                } else {
                    
                }
                /*
                  $this->saveToTookaan($invoice_id, $member, "update");
                 */

                $this->AddSessionItem("invoice_id", $invoice_id);
                $this->AddSessionItem("invoice_number", $invoice_number);
                $this->AddSessionItem("OrderAction", "update");
                $response["error"] = false;
                $response['message'] = "your order has been updated.";
                $header = 200;
            } else {
                
            }
        }
        echo jsonencode($response, $header);
    }

    public function storepreferences() {
        $post = $this->input->post(NULL, TRUE);

        $header = 200;
        $preferencesTotal = 0;
        $response["error"] = true;

        if (count($post["preferences_list"]) > 0) {
            $this->load->model('preferences');
            $preferences = $this->preferences->where_in("PKPreferenceID", $post["preferences_list"]);

            foreach ($preferences as $preference) {

                if ($preference["PriceForPackage"] == "Yes") {
                    $preferencesTotal += !empty($preference["Price"]) ? $preference["Price"] : 0;
                }
            }
        }
        $response["error"] = false;
        $response["total"] = numberformat($preferencesTotal);
        echo jsonencode($response, $header);
    }

    function edit($id) {

        $current_url = isset($this->uri->segments[1]) ? strtolower($this->uri->segments[1]) : '';
        $member_id = $this->GetCurrentMemberID();
        $data['order'] = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id, 'FKMemberId' => $member_id));
        //d($data['order'],1);
        $franchise_id = $data['order']["FKFranchiseID"];

        $timeArray = explode("-", $data['order']['PickupTime']);
        $strPickupDate = strtotime($data['order']['PickupDate'] . ' ' . $timeArray[0] . ':00');

        $strNow = time();
        $hour = ($strPickupDate - $strNow) / (60 * 60);

        $data["edit_pickup"] = true;
        $data["edit_delivery"] = true;

        if ($hour < 4) {
            $data["edit_pickup"] = false;
        }

        $timeArray = explode("-", $data['order']['DeliveryTime']);
        $strDeliverypDate = strtotime($data['order']['DeliveryDate'] . ' ' . $timeArray[0] . ':00');
        $strNow = time();

        $hour = ($strDeliverypDate - $strNow) / (60 * 60);

        if ($hour < 4) {
            $data["edit_delivery"] = false;
        }


        $service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Quantity,Price,Total,DesktopImageName,MobileImageName,PreferencesShow,IsPackage,FKCategoryID", array('FKInvoiceID' => $id), false, false, false, "PKInvoiceServiceID");

        $services = array();
        foreach ($service_records as $c) {
            $franchiseService = $this->model_database->GetRecord($this->tbl_franchise_services, "PKFranchiseServiceID,Title,FKServiceID,Title,Price,DiscountPercentage,FKFranchiseID", array('FKServiceID' => $c["FKServiceID"], 'FKFranchiseID' => $data['order']["FKFranchiseID"]));
            $category = $this->model_database->GetRecord($this->tbl_categories, false, array('PKCategoryID' => $c["FKCategoryID"]));

            if (!empty($franchiseService["DiscountPercentage"]) && $franchiseService["DiscountPercentage"] > 0) {
                $discount = $franchiseService["DiscountPercentage"];
                $discountAmount = ($franchiseService["Price"] * $discount) / 100;
                $price = (float) $franchiseService["Price"] - (float) $discountAmount;
            } else {
                $discount = 0;
                $price = $franchiseService["Price"];
            }

            $member_preference_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID", array('FKInvoiceID' => $id));

            $preferences = array();
            foreach ($member_preference_records as $p) {
                $preferences[] = $p["FKPreferenceID"];
            }

            $services[$c["FKServiceID"]]["quantity"] = $c["Quantity"];
            $services[$c["FKServiceID"]]["price"] = $c["Price"];
            $services[$c["FKServiceID"]]["total"] = $c["Total"];
            $services[$c["FKServiceID"]]["preference"] = $c["PreferencesShow"];
            $services[$c["FKServiceID"]]["package"] = $c["IsPackage"];
            $services[$c["FKServiceID"]]["title"] = $c["Title"];
            $services[$c["FKServiceID"]]["desktop_image"] = $c["DesktopImageName"];
            $services[$c["FKServiceID"]]["mobile_image"] = $c["MobileImageName"];
            $services[$c["FKServiceID"]]["discount"] = $discount;
            $services[$c["FKServiceID"]]["category_id"] = $c["FKCategoryID"];
            $services[$c["FKServiceID"]]["category"] = $category["Title"];
        }


        $this->AddSessionItem("services", $services);

        $data["services"] = $services;
        $data["preferences"] = $preferences;
        $data['preference_records'] = $this->_GetPreferences();
        $data['member'] = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $data['order']['FKMemberID']));
        $data['member_detail'] = $data['member'];

        $data['current_url'] = $current_url;
        $data['id'] = $id;
        $data['franchise_id'] = $data['order']["FKFranchiseID"];

        $data["referrer"] = base_url("order-detail/") . $data['order']["InvoiceNumber"];
        $data["franchise"] = $this->_GetFranchiseRecord($franchise_id);


        if (!$this->agent->is_referral()) {
            $data["referrer"] = $this->agent->referrer();
        }

        $this->show_view_with_menu('order/edit', $data);
    }

}
