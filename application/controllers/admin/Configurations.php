<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Configurations extends Base_Admin_Controller{	
	function __construct(){
		parent::__construct();	
		$this->IsAdminLoginRedirect();
	}
	
	function index(){
        $this->show_view_with_menu("admin/configurations");
	}	
    
    function clear(){
        $banner_file_array = array();
        $banner_files = get_filenames('uploads/banners/');
        if(sizeof($banner_files)){
            for($i=0;$i<sizeof($banner_files);$i++){
                $file_name = $banner_files[$i];
                $banner_record = $this->model_database->GetRecord($this->tbl_banners,"PKBannerID",array('ImageName'=>$file_name));
                if($banner_record == false){
                    $page_record = $this->model_database->GetRecord($this->tbl_pages,"PKPageID",array('HeaderImageName'=>$file_name));
                    if($page_record == false){
                        $banner_file_array[] = $file_name;
                    }
                }
            }
            if(sizeof($banner_file_array) > 0){
                $banner_main_dir = $this->config->item('dir_upload_banner');
                $banner_thumb_dir = $this->config->item('dir_upload_banner_thumb');
                for($i=0;$i<sizeof($banner_file_array);$i++){
                    $file_name = $banner_file_array[$i];
                    if(file_exists($banner_main_dir . $file_name)){
                        unlink($banner_main_dir . $file_name);
                    }
                    if(file_exists($banner_thumb_dir . $file_name)){
                        unlink($banner_thumb_dir . $file_name);
                    }
                }
            }
        }
        $widget_file_array = array();
        $widget_files = get_filenames('uploads/widgets/');
        if(sizeof($widget_files)){
            for($i=0;$i<sizeof($widget_files);$i++){
                $file_name = $widget_files[$i];
                $widget_record = $this->model_database->GetRecord($this->tbl_widgets,"PKWidgetID",array('ImageName'=>$file_name));
                if($widget_record == false){
                    $widget_file_array[] = $file_name;
                }
            }
            if(sizeof($widget_file_array) > 0){
                $widget_main_dir = $this->config->item('dir_upload_widget');
                $widget_thumb_dir = $this->config->item('dir_upload_widget_thumb');
                for($i=0;$i<sizeof($widget_file_array);$i++){
                    $file_name = $widget_file_array[$i];
                    if(file_exists($widget_main_dir . $file_name)){
                        unlink($widget_main_dir . $file_name);
                    }
                    if(file_exists($widget_thumb_dir . $file_name)){
                        unlink($widget_thumb_dir . $file_name);
                    }
                }
            }
        }
        $category_file_array = array();
        $category_files = get_filenames('uploads/categories/');
        if(sizeof($category_files)){
            for($i=0;$i<sizeof($category_files);$i++){
                $file_name = $category_files[$i];
                $category_mobile_record = $this->model_database->GetRecord($this->tbl_categories,"PKCategoryID",array('MobileImageName'=>$file_name));
                if($category_mobile_record == false) {
                    $category_mobile_record = $this->model_database->GetRecord($this->tbl_categories,"PKCategoryID",array('MobileIcon'=>$file_name));
                    if($category_mobile_record == false) {
                        $category_file_array[] = $file_name;
                    }
                }
            }
            if(sizeof($category_file_array) > 0){
                $category_main_dir = $this->config->item('dir_upload_category');
                $category_thumb_dir = $this->config->item('dir_upload_category_thumb');
                for($i=0;$i<sizeof($category_file_array);$i++){
                    $file_name = $category_file_array[$i];
                    if(file_exists($category_main_dir . $file_name)){
                        unlink($category_main_dir . $file_name);
                    }
                    if(file_exists($category_thumb_dir . $file_name)){
                        unlink($category_thumb_dir . $file_name);
                    }
                }
            }
        }
        $service_file_array = array();
        $service_files = get_filenames('uploads/services/');
        if(sizeof($service_files)){
            for($i=0;$i<sizeof($service_files);$i++){
                $file_name = $service_files[$i];
                $service_desktop_record = $this->model_database->GetRecord($this->tbl_services,"PKServiceID",array('DesktopImageName'=>$file_name));
                if($service_desktop_record == false){
                    $service_mobile_record = $this->model_database->GetRecord($this->tbl_services,"PKServiceID",array('MobileImageName'=>$file_name));
                    if($service_mobile_record == false) {
                        $service_file_array[] = $file_name;
                    }
                }
            }
            if(sizeof($service_file_array) > 0){
                $service_main_dir = $this->config->item('dir_upload_service');
                $service_thumb_dir = $this->config->item('dir_upload_service_thumb');
                for($i=0;$i<sizeof($service_file_array);$i++){
                    $file_name = $service_file_array[$i];
                    if(file_exists($service_main_dir . $file_name)){
                        unlink($service_main_dir . $file_name);
                    }
                    if(file_exists($service_thumb_dir . $file_name)){
                        unlink($service_thumb_dir . $file_name);
                    }
                }
            }
        }
        $this->carabiner->empty_cache();
        $this->AddSessionItem("AdminMessage","All cache is cleared");
        redirect(site_url('admin/configurations'));
    }
}