<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class UnavailableTimes extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $access_check = $this->model_database->GetRecord($this->tbl_user_menus, "PKAdminMenuID", array('FKMenuID' => 25, 'FKUserID' => $this->GetCurrentAdminID()));
        if ($access_check == false) {
            redirect(site_url('admin/error/unauthorized'));
        }
        //$this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index() {
        $is_not_found = true;
        if (isset($_REQUEST['f']) && $_REQUEST['f'] != "") {
            $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID", array("PKFranchiseID" => $_REQUEST['f']));
            if ($franchise_record != false) {
                $is_not_found = false;
                $this->AddSessionItem("franchise_id_record", $franchise_record['PKFranchiseID']);
                $data['franchise_id'] = $franchise_record['PKFranchiseID'];
                $this->show_view_with_menu("admin/unavailable_times", $data);
            }
        }
        if ($is_not_found) {
            redirect(site_url('admin/franchises'));
        }
    }

    function listener() {
        echo $this->model_database->GenerateTable($this->tbl_disable_date_slots, 'PKDateID as ID,DisableDateFrom,DisableDateTo,Type', 'unavailabletimes', 'Unavailable Time', $this->GetSessionItem("franchise_id_record"));
    }

    function add() {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        if ($this->GetSessionItem("franchise_id_record") != null) {

            $data['type'] = "Add";
            $data['franchise_id'] = $this->GetSessionItem("franchise_id_record");
            $data['form_action'] = site_url('admin/unavailabletimes/insert');

            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $data['franchise_id']), false, false, false, false, 'asc');

            $timings = array();
            foreach ($franchise_timings_record as $f) {
                //d($f["ClosingTime"],1);
                $timings[$f["GapTime"]]["OpeningTime"] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $f["OpeningTime"]));
                $timings[$f["GapTime"]]["ClosingTime"] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $f["ClosingTime"]));
            }

            $data['timings'] = $timings;
            $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, false, array("PKFranchiseID" => $data['franchise_id']));
            $data['franchise'] = $franchise_record;
            $this->show_view_with_menu("admin/unavailable_times", $data);
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function insert() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $date_from = date('Y-m-d', strtotime($postData['disable_date_from']));
            $date_to = date('Y-m-d', strtotime($postData['disable_date_from']));
            $insert_date_slot = array(
                'FKFranchiseID' => $this->GetSessionItem("franchise_id_record"),
                'DisableDateFrom' => $date_from,
                'Type' => $postData['disable_type'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            if (!empty($postData['disable_date_to'])) {
                $date_to = date('Y-m-d', strtotime($postData['disable_date_to']));
                $insert_date_slot['DisableDateTo'] = $date_to;
            }
            $date_from_unix = strtotime($date_from); // Convert date to a UNIX timestamp
            $date_to_unix = strtotime($date_to); // Convert date to a UNIX timestamp
            $date_array = array();
            // Loop from the start date to end date and output all dates inbetween
            for ($i = $date_from_unix; $i <= $date_to_unix; $i += 86400) {
                $date_array[] = date("Y-m-d", $i);
            }
            $date_id = $this->model_database->InsertRecord($this->tbl_disable_date_slots, $insert_date_slot);
            $time_array = json_decode($postData['assigned_output'], true);
            if ($time_array !== null && sizeof($time_array) > 0) {
                foreach ($time_array as $rec) {
                    for ($i = 0; $i < sizeof($date_array); $i++) {
                        $insert_time_slot = array(
                            'FKFranchiseID' => $this->GetSessionItem("franchise_id_record"),
                            'FKDateID' => $date_id,
                            'Date' => $date_array[$i],
                            'Time' => $rec['text'],
                            'Type' => $postData['disable_type']
                        );
                        $this->model_database->InsertRecord($this->tbl_disable_time_slots, $insert_time_slot);
                    }
                }
            }
            $this->AddSessionItem("AdminMessage", "Unavailable Time Add Successfully");
            echo "success||t||" . site_url('admin/unavailabletimes') . '?f=' . $this->GetSessionItem("franchise_id_record");
        } else {
            redirect(site_url('admin/unavailabletimes'));
        }
    }

    function edit($id) {
        if ($this->GetSessionItem("franchise_id_record") != null) {
            if ($id != "" && $id != null) {
                $data['record'] = $this->model_database->GetRecord($this->tbl_disable_date_slots, false, array('PKDateID' => $id, 'FKFranchiseID' => $this->GetSessionItem("franchise_id_record")));
                if ($data['record'] !== false) {
                    $data['type'] = "Edit";
                    $data['record']['Navigation'] = array();
                    $time_records = $this->model_database->GetDistinctRecords($this->tbl_disable_time_slots, "R", "Time", array('FKDateID' => $id), false, false, false, "PKTimeID");
                    if (sizeof($time_records) > 0) {
                        foreach ($time_records as $value) {
                            $data['record']['Navigation'][]['Time'] = $value['Time'];
                        }
                    }
                    $data['franchise_id'] = $data['record']['FKFranchiseID'];

                    $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $data['franchise_id']), false, false, false, false, 'asc');

                    $timings = array();
                    foreach ($franchise_timings_record as $f) {
                        //d($f["ClosingTime"],1);
                        $timings[$f["GapTime"]]["OpeningTime"] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $f["OpeningTime"]));
                        $timings[$f["GapTime"]]["ClosingTime"] = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $f["ClosingTime"]));
                    }

                    $data['timings'] = $timings;
                    $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, false, array("PKFranchiseID" => $data['franchise_id']));
                    $data['franchise'] = $franchise_record;
                    $data['form_action'] = site_url('admin/unavailabletimes/update');
                    $this->show_view_with_menu("admin/unavailable_times", $data);
                } else {
                    redirect(site_url('admin/unavailabletimes/add'));
                }
            } else {
                redirect(site_url('admin/unavailabletimes'));
            }
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function update() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $date_id = $postData['date_id'];
            $date_from = date('Y-m-d', strtotime($postData['disable_date_from']));
            $date_to = date('Y-m-d', strtotime($postData['disable_date_from']));
            $update_date_slot = array(
                'FKFranchiseID' => $this->GetSessionItem("franchise_id_record"),
                'DisableDateFrom' => $date_from,
                'Type' => $postData['disable_type'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $date_id
            );
            if (!empty($postData['disable_date_to'])) {
                $date_to = date('Y-m-d', strtotime($postData['disable_date_to']));
                $update_date_slot['DisableDateTo'] = $date_to;
            } else {
                $update_date_slot['DisableDateTo'] = null;
            }
            $date_from_unix = strtotime($date_from); // Convert date to a UNIX timestamp
            $date_to_unix = strtotime($date_to); // Convert date to a UNIX timestamp
            $date_array = array();
            // Loop from the start date to end date and output all dates inbetween
            for ($i = $date_from_unix; $i <= $date_to_unix; $i += 86400) {
                $date_array[] = date("Y-m-d", $i);
            }
            $this->model_database->UpdateRecord($this->tbl_disable_date_slots, $update_date_slot, "PKDateID");
            $this->model_database->RemoveRecord($this->tbl_disable_time_slots, $date_id, "FKDateID");
            $time_array = json_decode($postData['assigned_output'], true);
            if ($time_array !== null && sizeof($time_array) > 0) {
                foreach ($time_array as $rec) {
                    for ($i = 0; $i < sizeof($date_array); $i++) {
                        $insert_time_slot = array(
                            'FKFranchiseID' => $this->GetSessionItem("franchise_id_record"),
                            'FKDateID' => $date_id,
                            'Date' => $date_array[$i],
                            'Time' => $rec['text'],
                            'Type' => $postData['disable_type']
                        );
                        $this->model_database->InsertRecord($this->tbl_disable_time_slots, $insert_time_slot);
                    }
                }
            }
            $this->AddSessionItem("AdminMessage", "Unavailable Time Updated Successfully");
            echo "success||t||" . site_url('admin/unavailabletimes') . '?f=' . $this->GetSessionItem("franchise_id_record");
        } else {
            redirect(site_url('admin/unavailabletimes'));
        }
    }

    function view($id) {
        if ($this->GetSessionItem("franchise_id_record") != null) {
            if ($id != "" && $id != null) {
                $data['record'] = $this->model_database->GetRecord($this->tbl_disable_date_slots, false, array('PKDateID' => $id, 'FKFranchiseID' => $this->GetSessionItem("franchise_id_record")));
                if ($data['record'] !== false) {
                    $data['type'] = "View";
                    $data['record']['Navigation'] = array();
                    $time_records = $this->model_database->GetDistinctRecords($this->tbl_disable_time_slots, "R", "Time", array('FKDateID' => $id), false, false, false, "PKTimeID");
                    if (sizeof($time_records) > 0) {
                        foreach ($time_records as $value) {
                            $data['record']['Navigation'][]['Time'] = $value['Time'];
                        }
                    }
                    $data['franchise_id'] = $data['record']['FKFranchiseID'];
                    $this->show_view_with_menu("admin/unavailable_times", $data);
                } else {
                    redirect(site_url('admin/unavailabletimes/add'));
                }
            } else {
                redirect(site_url('admin/unavailabletimes'));
            }
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function delete($id) {
        if ($id != "" && $id != null) {
            $this->model_database->RemoveRecord($this->tbl_disable_time_slots, $id, "FKDateID");
            $this->model_database->RemoveRecord($this->tbl_disable_date_slots, $id, "PKDateID");
            echo 'success';
        } else {
            redirect(site_url('admin/unavailabletimes'));
        }
    }

}
