<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Profile extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
    }

    function index(){
        $data['record'] = $this->GetCurrentAdminDetail();
        $this->show_view_with_menu("admin/profile",$data);
    }
    
    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $admin_id = $this->GetCurrentAdminID();
            $admin_exist = $this->model_database->GetRecord($this->tbl_users,"PKUserID",array('PKUserID !='=>$admin_id,'EmailAddress'=>$postData['email_address'],'FKFranchiseID'=>0));
            if($admin_exist !== false){
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            $update_administrator =  array(
                'FirstName' => $postData['first_name'],
                'LastName' => $postData['last_name'],
                'EmailAddress' => $postData['email_address'],
                'UpdatedBy' => $admin_id,
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $admin_id
            );

            if(!empty($postData['u_password'])){
                $update_administrator['Password'] = encrypt_decrypt('encrypt',$postData['u_password']);
            }
            $this->model_database->UpdateRecord($this->tbl_users,$update_administrator,"PKUserID");
            $admin_updated_record = $this->model_database->GetRecord($this->tbl_users,false,array('PKUserID' => $admin_id));
            if($admin_updated_record != false){
                $this->AddSessionItem('admin_love_2_laundry_detail',$admin_updated_record);
            }
            $this->AddSessionItem("AdminMessage","Profile Updated Successfully");
            echo "success||t||" . site_url('admin/profile');
        }else{
            redirect(site_url('admin/profile'));
        }
    }
}