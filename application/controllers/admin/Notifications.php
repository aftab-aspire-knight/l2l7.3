<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Notifications extends Base_Admin_Controller {

    private $topic = "";

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
        $country = substr($this->config->item('iso_code'), 0, 2);
        $this->topic = "/topics/l2l";
        //$this->topic = "/topics/l2l_category_5";
    }

    function index() {
        $topic = $this->topic;
        $this->show_view_with_menu("admin/notifications");
    }

    function listener() {
        echo $this->model_database->GenerateTable($this->tbl_notifications, 'PKNotificationID as ID,Title,Content,PublishDateTime,LastPublishDateTime,PublishCount,Status', 'notifications', 'Notification');
    }

    function add() {
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/notifications/insert');
        $this->show_view_with_menu("admin/notifications", $data);
    }

    function insert() {
        $postData = $this->input->post(NULL, TRUE);

        if ($postData) {
            /* $notification_exist = $this->model_database->GetRecord($this->tbl_notifications,"PKNotificationID",array('Title'=>$postData['title']));
              if($notification_exist !== false){
              echo "error||a||title||Title already exist in our records";
              return;
              } */
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';

            $topic = $this->topic;
            $insert_notification = array(
                'Title' => $postData['title'],
                'Content' => $postData['content'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );


            if ($postData["btn_submit"] == "Save") {
                if (!empty($postData["publish_date"])) {
                    $insert_notification["PublishDateTime"] = $postData["publish_date"] . " " . $postData["time"] . ":00";
                }
            } else if ($postData["btn_submit"] == "Publish") {
                $insert_notification["LastPublishDateTime"] = date("Y-m-d H:i:s");
                $insert_notification["PublishCount"] = 1;
                $response = $this->publish($topic, $postData['title'], $postData['content']);
            }
            //d($insert_notification, 1);

            $notification_id = $this->model_database->InsertRecord($this->tbl_notifications, $insert_notification);
            $this->AddSessionItem("AdminMessage", "Notification Add Successfully");
            echo "success||t||" . site_url('admin/notifications');
        } else {
            redirect(site_url('admin/notifications'));
        }
    }

    function edit($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_notifications, false, array('PKNotificationID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/notifications/update');

                $this->show_view_with_menu("admin/notifications", $data);
            } else {
                redirect(site_url('admin/notifications/add'));
            }
        } else {
            redirect(site_url('admin/notifications'));
        }
    }

    function publish($topic = "/topics/l2l", $title = "Love2laundry", $message = "") {

        $this->load->library('Firebasemessaging');
        $n = new Firebasemessaging();
        return $response = $n->send($topic, $title, $message);
    }

    function view($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_notifications, false, array('PKNotificationID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "View";
                $this->show_view_with_menu("admin/notifications", $data);
            } else {
                redirect(site_url('admin/notifications/add'));
            }
        } else {
            redirect(site_url('admin/notifications'));
        }
    }

    function update() {
        $topic = $this->topic;
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $notification_id = $postData['notification_id'];
            $notification = $this->model_database->GetRecord($this->tbl_notifications, false, array('PKNotificationID' => $notification_id));
            /* $notification_exist = $this->model_database->GetRecord($this->tbl_notifications,"PKNotificationID",array('Title'=>$postData['title'],'PKNotificationID !='=>$notification_id));
              if($notification_exist !== false){
              echo "error||a||title||Title already exist in our records";
              return;
              } */
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            $update_notification = array(
                'Title' => $postData['title'],
                'Content' => $postData['content'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $notification_id
            );

            if ($postData["btn_submit"] == "Save") {
                if (!empty($postData["publish_date"])) {
                    $update_notification["PublishDateTime"] = $postData["publish_date"] . " " . $postData["time"] . ":00";
                } else {
                    $update_notification["PublishDateTime"] = null;
                }
            } else if ($postData["btn_submit"] == "Publish") {
                $update_notification["LastPublishDateTime"] = date("Y-m-d H:i:s");
                $update_notification["PublishCount"] = $notification["PublishCount"] + 1;
                $response = $this->publish($topic, $postData['title'], $postData['content']);
            }

            $this->model_database->UpdateRecord($this->tbl_notifications, $update_notification, "PKNotificationID");

            $this->AddSessionItem("AdminMessage", "Notification Updated Successfully");
            echo "success||t||" . site_url('admin/notifications');
        } else {
            redirect(site_url('admin/notifications'));
        }
    }

    function delete($id) {
        if ($id != "" && $id != null) {
            $this->model_database->RemoveRecord($this->tbl_notifications, $id, "PKNotificationID");
            echo 'success';
        } else {
            redirect(site_url('admin/notifications'));
        }
    }

}
