<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Discounts extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/discounts");
    }

    function members(){
        //$members = $this->model_database->GetRecords($this->tbl_members,"R","PKMemberID,EmailAddress",false,false,false,false,"EmailAddress","asc");
        $searchTerm = $_GET["searchTerm"];
        $sql = "SELECT PKMemberID,EmailAddress FROM `ws_members` where EmailAddress LIKE '%$searchTerm%' and Status='Enabled' order by PKMemberID desc limit 100";
        $query = $this->db->query($sql);
        $members = $query->result_array();

        foreach($members as $member){
            $json[] = ['id'=>$member['PKMemberID'], 'text'=>$member['EmailAddress']];
        }

        echo json_encode($json);
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_discounts,'ws_discounts.PKDiscountID as ID,ws_members.EmailAddress as EmailAddress,ws_discounts.Code,ws_discounts.Worth,ws_discounts.DType as Type,ws_discounts.DiscountFor,ws_discounts.CodeUsed,ws_discounts.Status','discounts','Discount');
    }
    function add(){
        $data['type'] = "Add";
        $data['member_records'] = $this->model_database->GetRecords($this->tbl_members,"R","PKMemberID,EmailAddress",false,false,false,false,"EmailAddress","asc");
        $data['form_action'] = site_url('admin/discounts/insert');
        $this->show_view_with_menu("admin/discounts",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){

            $code_exist = $this->model_database->GetRecord($this->tbl_discounts,'PKDiscountID',array('Code'=>$postData['code']));
            if($code_exist !== false){
                echo "error||a||code||Code already exist in our records";
                return;
            }
            $insert_discount =  array(
                'FKMemberID' =>$postData['member'],
                'Code' =>$postData['code'],
                'Worth' => $postData['worth'],
                'DType' => $postData['d_type'],
                'DiscountFor' => $postData['discount_for'],
                'CodeUsed' => $postData['code_used'],
                'MinimumOrderAmount' => $postData['minimum_order_amount'],
                'Status' => $postData['status'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            if(!empty($postData['start_date'])){
                $insert_discount['StartDate'] = date('Y-m-d',strtotime($postData['start_date']));
            }
            if(!empty($postData['expire_date'])){
                $insert_discount['ExpireDate'] = date('Y-m-d',strtotime($postData['expire_date']));
            }
            $discount_id = $this->model_database->InsertRecord($this->tbl_discounts,$insert_discount);
            $this->AddSessionItem("AdminMessage","Discount Code Add Successfully");
            echo "success||t||" . site_url('admin/discounts');
        }else{
            redirect(site_url('admin/discounts'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_discounts,false,array('PKDiscountID'=>$id));
            

            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/discounts/update');
                if(empty($data['record']['StartDate'])){
                    unset($data['record']['StartDate']);
                }
                if(empty($data['record']['ExpireDate'])){
                    unset($data['record']['ExpireDate']);
                }
                $data['member_records']=[];
                if(!empty($data['record']['FKMemberID'])){
                    $data['member_records'] = $this->model_database->GetRecord($this->tbl_members,"PKMemberID,EmailAddress",array('PKMemberID' => $data['record']['FKMemberID']));
                }

                //d($data['member_records'],1);
                $this->show_view_with_menu("admin/discounts",$data);
            }else{
                redirect(site_url('admin/discounts/add'));
            }
        }else{
            redirect(site_url('admin/discounts'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $discount_id = $postData['discount_id'];
            $update_discount =  array(
                'FKMemberID' =>$postData['member'],
                'Code' =>$postData['code'],
                'Worth' => $postData['worth'],
                'DType' => $postData['d_type'],
                'DiscountFor' => $postData['discount_for'],
                'CodeUsed' => $postData['code_used'],
                'MinimumOrderAmount' => $postData['minimum_order_amount'],
                'Status' => $postData['status'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $discount_id
            );
            if(!empty($postData['start_date'])){
                $update_discount['StartDate'] = date('Y-m-d',strtotime($postData['start_date']));
            }else{
                $update_discount['StartDate'] = null;
            }
            if(!empty($postData['expire_date'])){
                $update_discount['ExpireDate'] = date('Y-m-d',strtotime($postData['expire_date']));
            }else{
                $update_discount['ExpireDate'] = null;
            }
            $this->model_database->UpdateRecord($this->tbl_discounts,$update_discount,"PKDiscountID");
            
            $this->AddSessionItem("AdminMessage","Discount Code Updated Successfully");
            echo "success||t||" . site_url('admin/discounts');
        }else{
            redirect(site_url('admin/discounts'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_discounts,false,array('PKDiscountID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                if(empty($data['record']['StartDate'])){
                    unset($data['record']['StartDate']);
                }
                if(empty($data['record']['ExpireDate'])){
                    unset($data['record']['ExpireDate']);
                }
                $data['member_records'] = $this->model_database->GetRecords($this->tbl_members,"R","PKMemberID,EmailAddress",false,false,false,false,"EmailAddress","asc");
                $this->show_view_with_menu("admin/discounts",$data);
            }else{
                redirect(site_url('admin/discounts/add'));
            }
        }else{
            redirect(site_url('admin/discounts'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $invoice = $this->model_database->GetRecord($this->tbl_invoices,'FKDiscountID',array('FKDiscountID' => $id,'DiscountType'=>'Discount'));
            if($invoice !== false){
                echo "you can not delete this discount because it is used in some invoices" ;
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_discounts,$id,"PKDiscountID");
            echo 'success';
        }else{
            redirect(site_url('admin/discounts'));
        }
    }
}