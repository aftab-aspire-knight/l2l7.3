<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Franchises extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index() {
        $this->show_view_with_menu("admin/franchises");
    }

    function listener() {
        echo $this->model_database->GenerateTable($this->tbl_franchises, 'PKFranchiseID as ID,Title,Name,EmailAddress,PickupLimit,DeliveryLimit,OpeningTime,ClosingTime,AllowSameTime,DeliveryDifferenceHour,Status', 'franchises', 'Franchise');
    }

    function add() {
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/franchises/insert');
        $this->show_view_with_menu("admin/franchises", $data);
    }

    function insert() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $franchise_exist = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID", array('Title' => $postData['title']));
            if ($franchise_exist !== false) {
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $delivery_difference_hour = intval($postData['delivery_difference_hour']);
            $pickup_difference_hour = intval($postData['pickup_difference_hour']);
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            /* Hassan OffDay  13-10-2018 */
            $off_days = "";
            if (isset($postData['off_days'])) {
                foreach ($postData['off_days'] as $rec) {
                    $off_days .= $rec . ',';
                }
                if (!empty($off_days)) {
                    $off_days = substr_replace($off_days, "", -1);
                }
            }
            $insert_franchise = array(
                'Title' => $postData['title'],
                'Name' => $postData['name'],
                'EmailAddress' => $postData['email_address'],
                'Mobile' => $postData['mobile_no'],
                'Telephone' => $postData['telephone_no'],
                'Address' => $postData['address'],
                'AllowSameTime' => $postData['allow_same_time'],
                'DeliveryOption' => $postData['delivery_option'],
                'PickupDifferenceHour' => $pickup_difference_hour,
                'DeliveryDifferenceHour' => $delivery_difference_hour,
                'MinimumOrderAmount' => $postData['minimum_order_amount'],
                'PickupLimit' => $postData['pickup_limit'],
                'DeliveryLimit' => $postData['delivery_limit'],
                'OpeningTime' => $postData['opening_time'],
                'ClosingTime' => $postData['closing_time'],
                'GapTime' => $postData['GapTime'],
                'OffDays' => $off_days,
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $franchise_id = $this->model_database->InsertRecord($this->tbl_franchises, $insert_franchise);

            /* hassan changes for franchise 23-01-2018 */
            $more_pickup_limit = isset($postData['more_pickup_limit']) ? $postData['more_pickup_limit'] : array();
            $more_delivery_limit = isset($postData['more_delivery_limit']) ? $postData['more_delivery_limit'] : array();
            $more_opening_time = isset($postData['more_opening_time']) ? $postData['more_opening_time'] : array();
            $more_closing_time = isset($postData['more_closing_time']) ? $postData['more_closing_time'] : array();
            $gap_time = isset($postData['GapTime']) ? $postData['GapTime'] : array();
            if (isset($more_pickup_limit) && !empty($more_pickup_limit)) {
                foreach ($more_pickup_limit as $key => $val) {

                    if (!isset($gap_time[$key]) || $gap_time[$key] == NULL || $gap_time[$key] <= 0) {
                        continue;
                    }


                    $insert_franchise_timmings = array(
                        'FKFranchiseID' => $franchise_id,
                        'PickupLimit' => $val,
                        'DeliveryLimit' => isset($more_delivery_limit[$key]) ? $more_delivery_limit[$key] : NULL,
                        'OpeningTime' => isset($more_opening_time[$key]) ? $more_opening_time[$key] : NULL,
                        'ClosingTime' => isset($more_closing_time[$key]) ? $more_closing_time[$key] : NULL,
                        'GapTime' => isset($gap_time[$key]) ? $gap_time[$key] : NULL,
                    );
                    $franchise_timmings_id = $this->model_database->InsertRecord($this->tbl_franchise_timings, $insert_franchise_timmings);
                }
            }
            /* hassan changes for franchise 23-01-2018 ends */


            $post_code_array = json_decode($postData['assigned_output'], true);
            if ($post_code_array !== null && sizeof($post_code_array) > 0) {
                foreach ($post_code_array as $rec) {
                    $insert_post_code = array(
                        'FKFranchiseID' => $franchise_id,
                        'Code' => $rec['code']
                    );
                    $this->model_database->InsertRecord($this->tbl_franchise_post_codes, $insert_post_code);
                }
            }
            $this->AddSessionItem("AdminMessage", "Franchise Add Successfully");
            echo "success||t||" . site_url('admin/franchises');
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function edit($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/franchises/update');
                /* Franchise services action urls */
                $data['add_services_action'] = site_url('admin/franchises/bind_services');
                $data['update_services_price'] = site_url('admin/franchises/update_franchise_services');
                $data['remove_services'] = site_url('admin/franchises/remove_franchise_services');
                /* Franchise services action urls ends */

                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_franchise_post_codes, "R", 'Code', array('FKFranchiseID' => $id));
                /* Hassan changes 23-01-2018 */
                $data['record']['timings'] = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array('FKFranchiseID' => $id));
                /* ends */
                /* Franchise services by id */
                $data['record']['Services'] = $this->model_database->FranchiseCustomServices($id);
                /* Franchise services by id ends */

                $this->show_view_with_menu("admin/franchises", $data);
            } else {
                redirect(site_url('admin/franchises/add'));
            }
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function view($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "View";
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_franchise_post_codes, "R", 'Code', array('FKFranchiseID' => $id));
                $this->show_view_with_menu("admin/franchises", $data);
            } else {
                redirect(site_url('admin/franchises/add'));
            }
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function update() {
        $postData = $this->input->post(NULL, TRUE);


        if ($postData) {
            $franchise_id = $postData['franchise_id'];
            $franchise_exist = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID", array('Title' => $postData['title'], 'PKFranchiseID !=' => $franchise_id));
            if ($franchise_exist !== false) {
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            $delivery_difference_hour = intval($postData['delivery_difference_hour']);
            $pickup_difference_hour = intval($postData['pickup_difference_hour']);
            /* Hassan OffDay  13-10-2018 */
            $off_days = "";
            if (isset($postData['off_days'])) {
                foreach ($postData['off_days'] as $rec) {
                    $off_days .= $rec . ',';
                }
                if (!empty($off_days)) {
                    $off_days = substr_replace($off_days, "", -1);
                }
            }

            $update_franchise = array(
                'Title' => $postData['title'],
                'Name' => $postData['name'],
                'EmailAddress' => $postData['email_address'],
                'Mobile' => $postData['mobile_no'],
                'Telephone' => $postData['telephone_no'],
                'Address' => $postData['address'],
                'AllowSameTime' => $postData['allow_same_time'],
                'DeliveryOption' => $postData['delivery_option'],
                'PickupLimit' => $postData['pickup_limit'],
                'DeliveryLimit' => $postData['delivery_limit'],
                'PickupDifferenceHour' => $pickup_difference_hour,
                'DeliveryDifferenceHour' => $delivery_difference_hour,
                'MinimumOrderAmount' => $postData['minimum_order_amount'],
                'OpeningTime' => $postData['opening_time'],
                'ClosingTime' => $postData['closing_time'],
                'GapTime' => $postData['GapTime'],
                'OffDays' => $off_days,
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $franchise_id
            );
            $this->model_database->UpdateRecord($this->tbl_franchises, $update_franchise, "PKFranchiseID");
            $this->model_database->RemoveRecord($this->tbl_franchise_post_codes, $franchise_id, "FKFranchiseID");
            $post_code_array = json_decode($postData['assigned_output'], true);
            if ($post_code_array !== null && sizeof($post_code_array) > 0) {
                foreach ($post_code_array as $rec) {
                    $insert_post_code = array(
                        'FKFranchiseID' => $franchise_id,
                        'Code' => $rec['code']
                    );
                    $this->model_database->InsertRecord($this->tbl_franchise_post_codes, $insert_post_code);
                }
            }

            /* hassan changes for franchise 23-01-2018 */
            $more_pickup_limit = isset($postData['more_pickup_limit']) ? $postData['more_pickup_limit'] : array();
            $more_delivery_limit = isset($postData['more_delivery_limit']) ? $postData['more_delivery_limit'] : array();
            $more_opening_time = isset($postData['more_opening_time']) ? $postData['more_opening_time'] : array();
            $more_closing_time = isset($postData['more_closing_time']) ? $postData['more_closing_time'] : array();
            $gap_time = isset($postData['GapTimes']) ? $postData['GapTimes'] : array();
            $this->model_database->RemoveRecord($this->tbl_franchise_timings, $franchise_id, "FKFranchiseID");
            if (isset($more_pickup_limit) && !empty($more_pickup_limit)) {

                foreach ($more_pickup_limit as $key => $val) {
                    //d($gap_time,1);
                    $insert_franchise_timmings = array(
                        'FKFranchiseID' => $franchise_id,
                        'PickupLimit' => $val,
                        'DeliveryLimit' => isset($more_delivery_limit[$key]) ? $more_delivery_limit[$key] : NULL,
                        'OpeningTime' => isset($more_opening_time[$key]) ? $more_opening_time[$key] : NULL,
                        'ClosingTime' => isset($more_closing_time[$key]) ? $more_closing_time[$key] : NULL,
                        'GapTime' => isset($gap_time[$key]) ? $gap_time[$key] : NULL,
                    );
                    $this->model_database->InsertRecord($this->tbl_franchise_timings, $insert_franchise_timmings);
                }
            }
            /* hassan changes for franchise 23-01-2018 ends */

            $this->AddSessionItem("AdminMessage", "Franchise Updated Successfully");
            echo "success||t||" . site_url('admin/franchises');
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    function delete($id) {
        if ($id != "" && $id != null) {
            $user_record = $this->model_database->GetRecord($this->tbl_users, 'PKUserID', array('FKFranchiseID' => $id));
            if ($user_record !== false) {
                echo "you can not delete this franchise because it have some users in our record";
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_franchises, $id, "PKFranchiseID");
            $this->model_database->RemoveRecord($this->tbl_franchise_post_codes, $id, "FKFranchiseID");
            echo 'success';
        } else {
            redirect(site_url('admin/franchises'));
        }
    }

    /* Franchise services functions starts */

    function bind_services() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData['franchise_id'] != "" && $postData['franchise_id'] != null) {
            $franchise_id = $postData['franchise_id'];
            $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, 'PKFranchiseID', array('PKFranchiseID' => $franchise_id));
            if ($franchise_record !== false) {

                $services = $this->model_database->GetRecords($this->tbl_services, "R", 'PKServiceID,Price,Title', array('Status' => 'Enabled'));
                if (isset($services) && !empty($services)) {
                    foreach ($services as $rec) {
                        $PKServiceID = $rec['PKServiceID'];
                        $PKServicePrice = $rec['Price'];
                        $PKServiceTitle = $rec['Title'];
                        $franchise_service_record = $this->model_database->GetRecord($this->tbl_franchise_services, 'FKFranchiseID', array('FKServiceID' => $PKServiceID, 'FKFranchiseID' => $franchise_id));
                        if (empty($franchise_service_record)) {
                            $insert_post_code = array(
                                'FKFranchiseID' => $franchise_id,
                                'FKServiceID' => $PKServiceID,
                                'Price' => $PKServicePrice,
                                'Title' => $PKServiceTitle
                            );
                            $this->model_database->InsertRecord($this->tbl_franchise_services, $insert_post_code);
                        } else {
                            continue;
                        }
                    }
                    echo 'success';
                } else {
                    echo "you can not bind services  because services are not  in our record";
                    return;
                }
            } else {
                echo "you can not bind services  because franchise is not  in our record";
                return;
            }
        } else {
            echo "Franchise id is required";
            return;
        }
    }

    function update_franchise_services() {
        $postData = $this->input->post(NULL, TRUE);

        if ($postData['id'] != "" && $postData['id'] != null) {
            $PKFranchiseServiceID = $postData['id'];
            $franchise_services_record = $this->model_database->GetRecord($this->tbl_franchise_services, 'PKFranchiseServiceID', array('PKFranchiseServiceID' => $PKFranchiseServiceID));
            if ($franchise_services_record !== false) {
                $dataTmp["ID"] = $PKFranchiseServiceID;
                $dataTmp["Price"] = $postData['price'];
                $dataTmp["Title"] = $postData['title'];
                $dataTmp["DiscountPercentage"] = $postData['discount'];
                $this->model_database->UpdateRecord($this->tbl_franchise_services, $dataTmp, 'PKFranchiseServiceID');
                echo "success";
            } else {
                echo "Franchise Service does not exist";
                return;
            }
        } else {
            echo "Franchise Service id is required";
            return;
        }
    }

    function remove_franchise_services() {
        $postData = $this->input->post(NULL, TRUE);

        if ($postData['id'] != "" && $postData['id'] != null) {
            $PKFranchiseServiceID = $postData['id'];
            $franchise_services_record = $this->model_database->GetRecord($this->tbl_franchise_services, 'PKFranchiseServiceID', array('PKFranchiseServiceID' => $PKFranchiseServiceID));
            if ($franchise_services_record !== false) {

                $this->model_database->RemoveRecord($this->tbl_franchise_services, $PKFranchiseServiceID, "PKFranchiseServiceID");

                echo "success";
            } else {
                echo "Franchise Service does not exist";
                return;
            }
        } else {
            echo "Franchise Service id is required";
            return;
        }
    }

    /* Franchise services functions ends */
}
