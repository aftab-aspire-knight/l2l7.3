<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Subscribers extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/subscribers");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_subscribers,'PKSubscriberID as ID,EmailAddress,Source,Status','subscribers','Subscriber');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/subscribers/insert');
        $this->show_view_with_menu("admin/subscribers",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $subscriber_exist = $this->model_database->GetRecord($this->tbl_subscribers,"PKSubscriberID",array('EmailAddress'=>$postData['email_address']));
            if($subscriber_exist !== false){
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_subscriber =  array(
                'EmailAddress' =>$postData['email_address'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            
            $subscriber_id = $this->model_database->InsertRecord($this->tbl_subscribers,$insert_subscriber);
            $this->AddSessionItem("AdminMessage","Subscriber Add Successfully");
            echo "success||t||" . site_url('admin/subscribers');
        }else{
            redirect(site_url('admin/subscribers'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_subscribers,false,array('PKSubscriberID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/subscribers/update');
                $this->show_view_with_menu("admin/subscribers",$data);
            }else{
                redirect(site_url('admin/subscribers/add'));
            }
        }else{
            redirect(site_url('admin/subscribers'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $subscriber_id = $postData['subscriber_id'];
            $subscriber_exist = $this->model_database->GetRecord($this->tbl_subscribers,"PKSubscriberID",array('PKSubscriberID !='=>$subscriber_id,'EmailAddress'=>$postData['email_address']));
            if($subscriber_exist !== false){
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_subscriber =  array(
                'EmailAddress' =>$postData['email_address'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $subscriber_id
            );
            
            $this->model_database->UpdateRecord($this->tbl_subscribers,$update_subscriber,"PKSubscriberID");
            
            $this->AddSessionItem("AdminMessage","Subscriber Updated Successfully");
            echo "success||t||" . site_url('admin/subscribers');
        }else{
            redirect(site_url('admin/subscribers'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_subscribers,false,array('PKSubscriberID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $this->show_view_with_menu("admin/subscribers",$data);
            }else{
                redirect(site_url('admin/subscribers/add'));
            }
        }else{
            redirect(site_url('admin/subscribers'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_subscribers,$id,"PKSubscriberID");
            echo 'success';
        }else{
            redirect(site_url('admin/subscribers'));
        }
    }
}