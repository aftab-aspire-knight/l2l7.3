<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class InvoiceFilters extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }
    function _Listing(){
        $data['franchise_records'] = $this->model_database->GetRecords($this->tbl_franchises,"R",'PKFranchiseID as ID,Title',false,false,false,false,"Title","asc");
        $data['member_records'] = $this->model_database->GetRecords($this->tbl_members,"R",'PKMemberID as ID,EmailAddress',false,false,false,false,"EmailAddress","asc");
        return $data;
    }
    function index(){
        $data = $this->_Listing();
        $this->show_view_with_menu("admin/invoice_filters",$data);
    }
    function search(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $data = array_merge($this->_Listing(),$postData);
            $data['is_post'] = true;
            $where_clause = "PKInvoiceID != 0";
            if(!empty($postData['franchise'])){
                $where_clause .= " and FKFranchiseID=" . $postData['franchise'];
            }
            if(!empty($postData['member'])){
                $where_clause .= " and FKMemberID=" . $postData['member'];
            }
            if(!empty($postData['pick_date'])){
                $where_clause .= " and PickupDate='" . date('Y-m-d',strtotime($postData['pick_date'])) . "'";
            }
            if(!empty($postData['pick_time'])){
                $where_clause .= " and PickupTime='" . $postData['pick_time'] . "'";
            }
            if(!empty($postData['delivery_date'])){
                $where_clause .= " and DeliveryDate='" . date('Y-m-d',strtotime($postData['delivery_date'])) . "'";
            }
            if(!empty($postData['delivery_time'])){
                $where_clause .= " and DeliveryTime='" . $postData['delivery_time'] . "'";
            }
            if(!empty($postData['order_post_from'])){
                $where_clause .= " and OrderPostFrom='" . $postData['order_post_from'] . "'";
            }
            if(!empty($postData['payment_status'])){
                $where_clause .= " and PaymentStatus='" . $postData['payment_status'] . "'";
            }
            if(!empty($postData['order_status'])){
                $where_clause .= " and OrderStatus='" . $postData['order_status'] . "'";
            }
            if(!empty($postData['created_date_time'])){
                $where_clause .= " and CreatedDateTime between '" . date('Y-m-d',strtotime($postData['created_date_time'])) . " 00:00:00.000' and '" . date('Y-m-d',strtotime($postData['created_date_time'])) . " 23:59:59.997'";
            }
            $data['invoice_records'] = $this->model_database->GetRecords($this->tbl_invoices,"R","PKInvoiceID,FKFranchiseID,FKMemberID,InvoiceNumber,PaymentStatus,Location,Locker,BuildingName,StreetName,PostalCode,Town,PickupDate,PickupTime,DeliveryDate,DeliveryTime",$where_clause,false,false,false,"PKInvoiceID");
            if(sizeof($data['invoice_records']) > 0){
                foreach($data['invoice_records'] as $key=>$value){
                    $member_record = $this->model_database->GetRecord($this->tbl_members,"FirstName,LastName,Phone",array('PKMemberID' => $value['FKMemberID']));
                    if($member_record != false){
                        $data['invoice_records'][$key]['Phone'] = $member_record['Phone'];
                        $data['invoice_records'][$key]['MemberName'] = $member_record["FirstName"] . " " . $member_record["LastName"];
                    }else{
                        $data['invoice_records'][$key]['Phone'] = "Unknown";
                        $data['invoice_records'][$key]['MemberName'] = "Unknown";
                    }
                    $franchise_record = $this->model_database->GetRecord($this->tbl_franchises,"Title",array('PKFranchiseID' => $value['FKFranchiseID']));
                    if($franchise_record != false){
                        $data['invoice_records'][$key]['FranchiseTitle'] = $franchise_record['Title'];
                    }else{
                        $data['invoice_records'][$key]['FranchiseTitle'] = "Unknown";
                    }
                }
            }
            $this->show_view_with_menu("admin/invoice_filters",$data);
        }else{
            redirect(site_url('admin/invoicefilters'));
        }
    }
    function export(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $where_clause = "PKInvoiceID != 0";
            if(!empty($postData['franchise'])){
                $where_clause .= " and FKFranchiseID=" . $postData['franchise'];
            }
            if(!empty($postData['member'])){
                $where_clause .= " and FKMemberID=" . $postData['member'];
            }
            if(!empty($postData['pick_date'])){
                $where_clause .= " and PickupDate='" . date('Y-m-d',strtotime($postData['pick_date'])) . "'";
            }
            if(!empty($postData['pick_time'])){
                $where_clause .= " and PickupTime='" . $postData['pick_time'] . "'";
            }
            if(!empty($postData['delivery_date'])){
                $where_clause .= " and DeliveryDate='" . date('Y-m-d',strtotime($postData['delivery_date'])) . "'";
            }
            if(!empty($postData['delivery_time'])){
                $where_clause .= " and DeliveryTime='" . $postData['delivery_time'] . "'";
            }
            if(!empty($postData['order_post_from'])){
                $where_clause .= " and OrderPostFrom='" . $postData['order_post_from'] . "'";
            }
            if(!empty($postData['payment_status'])){
                $where_clause .= " and PaymentStatus='" . $postData['payment_status'] . "'";
            }
            if(!empty($postData['order_status'])){
                $where_clause .= " and OrderStatus='" . $postData['order_status'] . "'";
            }
            if(!empty($postData['created_date_time'])){
                $where_clause .= " and CreatedDateTime between '" . date('Y-m-d',strtotime($postData['created_date_time'])) . " 00:00:00.000' and '" . date('Y-m-d',strtotime($postData['created_date_time'])) . " 23:59:59.997'";
            }
            $data['invoice_records'] = $this->model_database->GetRecords($this->tbl_invoices,"R","PKInvoiceID,FKMemberID,InvoiceNumber,PaymentStatus,Location,Locker,BuildingName,StreetName,PostalCode,Town,PickupDate,PickupTime,DeliveryDate,DeliveryTime,OrderNotes",$where_clause,false,false,false,"PKInvoiceID");
            if(sizeof($data['invoice_records']) > 0){
                ini_set('memory_limit', '-1');
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename="invoices-' . date('Y-m-d') . '.csv"');
                header("Pragma: no-cache");
                header("Expires: 0");

                //echo "\xEF\xBB\xBF";
                $handle = fopen('php://output', 'w');
                fputs( $handle , "\xEF\xBB\xBF" );
                fputcsv($handle, array(
                    'Task Description','Customer Email','Customer Name','Street Level Address','City','Zipcode/ Pincode','Country','Customer Phone Number',
                    'Delivery Date and Time (MM/DD/YYYY) (HH:MM:SS)','Agent ID (Settings > Agents)','has_pickup (1=yes)','Merchant Email','Merchant Name',
                    'Street Level Address','City','Zipcode/ Pincode','Country','Phone Number','Pickup Date and Time (MM/DD/YYYY) (HH:MM:SS)'
                ));

                foreach($data['invoice_records'] as $key=>$value){
                    $member_full_name = "Unknown";
                    $member_email_address = "Unknown";
                    $member_phone_number = "Unknown";
                    $member_record = $this->model_database->GetRecord($this->tbl_members,"FirstName,LastName,EmailAddress,Phone",array('PKMemberID' => $value['FKMemberID']));
                    if($member_record != false){
                        $member_full_name = $member_record["FirstName"] . " " . $member_record["LastName"];
                        $member_email_address = $member_record['EmailAddress'];
                        $member_phone_number = $member_record['Phone'];
                    }
                    $address_html = '';
                    if($value['Location'] != "Add" && $value['Location'] != "0" && $value['Location'] != null){
                        $address_html = $value['Location'] . ',' . $value['Locker'];
                    }else{
                        $address_html = $value['BuildingName'] . ', ' . $value['StreetName'];
                    }
                    $delivery_time_array = explode("-",$value['DeliveryTime']);
                    $pick_time_array = explode("-",$value['PickupTime']);
                    fputcsv($handle, array(
                        $value['OrderNotes'],$member_email_address,$member_full_name,$address_html,"London",$value['PostalCode'],"UNITED KINGDOM",$member_phone_number,
                        date('m/d/Y',strtotime($value['DeliveryDate'])) . ' ' . $delivery_time_array[0] . ':00',"","1","info@love2laundry.com","Love2Laundry",
                        "42 Albion Street","London","SE16 7JQ","UNITED KINGDOM","02036000296",date('m/d/Y',strtotime($value['PickupDate'])) . ' ' . $pick_time_array[0] . ':00'
                    ));
                }
                fclose($handle);
                exit;
                // Old Code Change on 16-06-2017
//                if(!empty($postData['pick_date']) && !empty($postData['delivery_date'])){
//                    fputcsv($handle, array(
//                        'Invoice Number','Member','Phone','Address','Pickup Time','Delivery Time','Payment Status'
//                    ));
//                }else if(!empty($postData['pick_date'])){
//                    fputcsv($handle, array(
//                        'Invoice Number','Member','Phone','Address','Pickup Time','Delivery Time','Payment Status'
//                    ));
//                }else{
//                    fputcsv($handle, array(
//                        'Invoice Number','Member','Phone','Address','Time','Payment Status'
//                    ));
//                }
//
//                foreach($data['invoice_records'] as $key=>$value){
//                    $phone = "";
//                    $member_name = "";
//                    $member_record = $this->model_database->GetRecord($this->tbl_members,"FirstName,LastName,Phone",array('PKMemberID' => $value['FKMemberID']));
//                    if($member_record != false){
//                        $phone = $member_record['Phone'];
//                        $member_name = $member_record["FirstName"] . " " . $member_record["LastName"];
//                    }else{
//                        $phone = "Unknown";
//                        $member_name = $phone;
//                    }
//                    $address_html = '';
//                    if($value['Location'] != "Add" && $value['Location'] != "0" && $value['Location'] != null){
//                        $address_html = $value['Location'] . ',' . $value['Locker'];
//                    }else{
//                        $address_html = $value['BuildingName'] . ', ' . $value['StreetName'] . ', ' . $value['PostalCode'] . ', ' . $value['Town'];
//                    }
//                    if(!empty($postData['pick_date']) && !empty($postData['delivery_date'])){
//                        fputcsv($handle, array(
//                            $value['InvoiceNumber'],$member_name,$phone,$address_html,$value['PickupDate'] . ' ' . $value['PickupTime'],$value['DeliveryDate'] . ' ' . $value['DeliveryTime'],$value['PaymentStatus']
//                        ));
//                    }else if(!empty($postData['pick_date'])){
//                        fputcsv($handle, array(
//                            $value['InvoiceNumber'],$member_name,$phone,$address_html,$value['PickupDate'] . ' ' . $value['PickupTime'],$value['DeliveryDate'] . ' ' . $value['DeliveryTime'],$value['PaymentStatus']
//                        ));
//					}else {
//						fputcsv($handle, array(
//							$value['InvoiceNumber'],$member_name,$phone,$address_html,$value['DeliveryDate'] . ' ' . $value['DeliveryTime'],$value['PaymentStatus']
//						));
//					}
//                }
//                fclose($handle);
//                exit;
            }
        }else{
            redirect(site_url('admin/invoicefilters'));
        }
    }
}