<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Error extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
    }

    function index(){
        redirect(site_url('admin/dashboard'));
    }

    function unauthorized(){
        $this->show_view_with_menu("admin/unauthorized");
    }

    function pagenotfound(){
        $this->show_view_with_menu("admin/page_not_found");
    }
}
