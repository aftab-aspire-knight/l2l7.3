<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Pages extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/pages");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_pages,'PKPageID as ID,Template,Title,HeaderImageName as Image,AccessURL as URL,Status','pages','Page');
    }
    function rewrite(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData) {
            $original_url = "";
            if(isset($postData['title'])){
                $postData['title'] = trim($postData['title']);
                $page_record_count = $this->model_database->GetRecords($this->tbl_pages,"C","PKPageID",array('Title'=>$postData['title']));
                if($page_record_count > 0){
                    $original_url = CreateAccessURL($postData['title']) . '-' . $page_record_count;
                }else{
                    $original_url = CreateAccessURL($postData['title']);
                }
            }else if(isset($postData['url'])){
                $page_record_count = $this->model_database->GetRecords($this->tbl_pages,"C","PKPageID",array('PKPageID !=' => $postData['id'],'AccessURL'=>$postData['url']));
                if($page_record_count > 0){
                    $original_url = CreateAccessURL($postData['url']) . '-' . $page_record_count;
                }else{
                    $original_url = CreateAccessURL($postData['url']);
                }
            }
            echo $original_url;
        }else{
            redirect(site_url('admin/pages'));
        }
    }
    function add(){
        $data['type'] = "Add";
        $data['area_detail_page_records'] = $this->model_database->GetRecords($this->tbl_pages,"R","PKPageID as ID,Title","Template='Area' or Template='Area Detail'");
        $data['form_action'] = site_url('admin/pages/insert');
        $this->show_view_with_menu("admin/pages",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            if($postData['template'] == "None"){
                if(isset($_FILES['header_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('header_image_file')) {
                        echo "error||a||header_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['header_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
            }else{
                if(isset($_FILES['section_1_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('section_1_image_file')) {
                        echo "error||a||section_1_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['section_1_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
                if(isset($_FILES['section_2_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('section_2_image_file')) {
                        echo "error||a||section_2_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['section_2_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
            }
            $original_url = $postData['access_url'];
            $page_record_count = $this->model_database->GetRecords($this->tbl_pages,"C","PKPageID",array('PKPageID !=' => $postData['page_id'],'AccessURL'=>$original_url));
            if($page_record_count > 0){
                $original_url = CreateAccessURL($postData['access_url']) . '-' . $page_record_count;
            }else{
                $original_url = CreateAccessURL($postData['access_url']);
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_page =  array(
                'Template' => $postData['template'],
                'Title' =>$postData['title'],
                'MetaTitle' => $postData['meta_title'],
                'MetaKeyword' => $postData['meta_keyword'],
                'MetaDescription' => $postData['meta_description'],
                'NoIndexFollowTag' => $postData['no_index_follow_tag'],
                'AccessURL' => $original_url,
                'Status' => $status,
                'IsPermanentRedirect' => $postData['is_permanent_redirect'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            if($postData['template'] == "None"){
                $insert_page['Content'] = $postData['content'];
                $insert_page['HeaderImageName'] = $postData['header_pic_path'];
            }else{
                $insert_page['Latitude'] = $postData['latitude'];
                $insert_page['Longitude'] = $postData['longitude'];
                $insert_page['HeaderTitle'] = $postData['header_title'];
            }
            if($postData['is_permanent_redirect'] == "Yes"){
                $insert_page['PermanentRedirectURL'] = $postData['permanent_redirect_url'];
            }
            $page_id = $this->model_database->InsertRecord($this->tbl_pages,$insert_page);
            if($postData['template'] != "None"){
                $insert_section_1_data_array = array(
                    'FKPageID' => $page_id,
                    'Title' => $postData['section_1_title'],
                    'Content' => $postData['section_1_content'],
                    'ImageName' => $postData['section_1_pic_path']
                );
                $this->model_database->InsertRecord($this->tbl_page_area_sections,$insert_section_1_data_array);
                $insert_section_2_data_array = array(
                    'FKPageID' => $page_id,
                    'Title' => $postData['section_2_title'],
                    'Content' => $postData['section_2_content'],
                    'ImageName' => $postData['section_2_pic_path']
                );
                $this->model_database->InsertRecord($this->tbl_page_area_sections,$insert_section_2_data_array);
                if($postData['template'] == "Area"){
                    if(isset($postData['area_detail_pages'])){
                        for($i=0;$i<sizeof($postData['area_detail_pages']);$i++){
                            $insert_area_page = array(
                                'FKPageID' => $page_id,
                                'FKAreaDetailID' => $postData['area_detail_pages'][$i]
                            );
                            $this->model_database->InsertRecord($this->tbl_page_areas,$insert_area_page);
                        }
                    }
                }
            }
            $this->AddSessionItem("AdminMessage","Page Add Successfully");
            echo "success||t||" . site_url('admin/pages');
        }else{
            redirect(site_url('admin/pages'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_pages,false,array('PKPageID' => $id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['area_detail_page_records'] = $this->model_database->GetRecords($this->tbl_pages,"R","PKPageID as ID,Title","Template='Area' or Template='Area Detail'");
                $data['form_action'] = site_url('admin/pages/update');
                if($data['record']['Template'] != "None"){
                    $page_section_records = $this->model_database->GetRecords($this->tbl_page_area_sections,"R","Title,Content,ImageName",array("FKPageID" => $id),false,false,false,"PKSectionID","asc");
                    if(sizeof($page_section_records) > 0){
                        $count = 1;
                        foreach($page_section_records as $rec){
                            $data['record']["Section_" . $count . "_Title"] = $rec['Title'];
                            $data['record']["Section_" . $count . "_Content"] = $rec['Content'];
                            $data['record']["Section_" . $count . "_Image_Name"] = $rec['ImageName'];
                            $count += 1;
                        }
                    }
                    if($data['record']['Template'] == "Area"){
                        $area_page_array = array();
                        $area_page_records = $this->model_database->GetRecords($this->tbl_page_areas,"R","FKAreaDetailID",array('FKPageID' => $id));
                        if(sizeof($area_page_records) > 0){
                            foreach($area_page_records as $rec){
                                $area_page_array[] = $rec['FKAreaDetailID'];
                            }
                        }
                        $data['record']['area_page_array'] = $area_page_array;
                    }
                }
                $this->show_view_with_menu("admin/pages",$data);
            }else{
                redirect(site_url('admin/pages/add'));
            }
        }else{
            redirect(site_url('admin/pages'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            if($postData['template'] == "None"){
                if(isset($_FILES['header_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('header_image_file')) {
                        echo "error||a||header_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['header_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
            }else{
                if(isset($_FILES['section_1_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('section_1_image_file')) {
                        echo "error||a||section_1_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['section_1_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
                if(isset($_FILES['section_2_image_file'])){
                    $image_config = array(
                        'upload_path' => $this->config->item('dir_upload_banner'),
                        'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                    );
                    $this->load->library('upload', $image_config);
                    if (!$this->upload->do_upload('section_2_image_file')) {
                        echo "error||a||section_2_image_file||" . $this->upload->display_errors();
                        return;
                    } else {
                        $result = array('upload_data' => $this->upload->data());
                        $postData['section_2_pic_path'] = $result['upload_data']['file_name'];
                        $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                    }
                }
            }
            $page_id = $postData['page_id'];
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $original_url = $postData['access_url'];
            $page_record_count = $this->model_database->GetRecords($this->tbl_pages,"C","PKPageID",array('PKPageID !=' => $page_id,'AccessURL'=>$original_url));
            if($page_record_count > 0){
                $original_url = CreateAccessURL($postData['access_url']) . '-' . $page_record_count;
            }else{
                $original_url = CreateAccessURL($postData['access_url']);
            }
            $update_page =  array(
                'Template' => $postData['template'],
                'Title' => $postData['title'],
                'MetaTitle' => $postData['meta_title'],
                'MetaKeyword' => $postData['meta_keyword'],
                'MetaDescription' => $postData['meta_description'],
                'NoIndexFollowTag' => $postData['no_index_follow_tag'],
                'AccessURL' => $original_url,
                'Status' => $status,
                'IsPermanentRedirect' => $postData['is_permanent_redirect'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $page_id
            );
            if($postData['template'] == "None"){
                $update_page['Content'] = $postData['content'];
                $update_page['HeaderImageName'] = $postData['header_pic_path'];
                $update_page['Latitude'] = null;
                $update_page['Longitude'] = null;
                $update_page['HeaderTitle'] = null;
            }else{
                $update_page['HeaderTitle'] = $postData['header_title'];
                $update_page['Latitude'] = $postData['latitude'];
                $update_page['Longitude'] = $postData['longitude'];
                $update_page['Content'] = null;
                $update_page['HeaderImageName'] = null;
            }
            if($postData['is_permanent_redirect'] == "Yes"){
                $update_page['PermanentRedirectURL'] = $postData['permanent_redirect_url'];
            }else{
                $update_page['PermanentRedirectURL'] = "";
            }
            $this->model_database->UpdateRecord($this->tbl_pages,$update_page,"PKPageID");
            $this->model_database->RemoveRecord($this->tbl_page_areas,$page_id,"FKPageID");
            $this->model_database->RemoveRecord($this->tbl_page_area_sections,$page_id,"FKPageID");
            if($postData['template'] != "None"){
                $insert_section_1_data_array = array(
                    'FKPageID' => $page_id,
                    'Title' => $postData['section_1_title'],
                    'Content' => $postData['section_1_content'],
                    'ImageName' => $postData['section_1_pic_path']
                );
                $this->model_database->InsertRecord($this->tbl_page_area_sections,$insert_section_1_data_array);
                $insert_section_2_data_array = array(
                    'FKPageID' => $page_id,
                    'Title' => $postData['section_2_title'],
                    'Content' => $postData['section_2_content'],
                    'ImageName' => $postData['section_2_pic_path']
                );
                $this->model_database->InsertRecord($this->tbl_page_area_sections,$insert_section_2_data_array);
                if($postData['template'] == "Area"){
                    if(isset($postData['area_detail_pages'])){
                        for($i=0;$i<sizeof($postData['area_detail_pages']);$i++){
                            $insert_area_page = array(
                                'FKPageID' => $page_id,
                                'FKAreaDetailID' => $postData['area_detail_pages'][$i]
                            );
                            $this->model_database->InsertRecord($this->tbl_page_areas,$insert_area_page);
                        }
                    }
                }
            }
            $navigation_records= $this->model_database->GetRecords($this->tbl_navigations,"R","PKNavigationID",array('FKPageID'=>$page_id));
            if(sizeof($navigation_records) > 0){
                foreach($navigation_records as $value){
                    $update_navigation = array(
                        'ID' => $value['PKNavigationID'],
                        'LinkWithUrl' => $original_url
                    );
                    $this->model_database->UpdateRecord($this->tbl_navigations,$update_navigation,"PKNavigationID");
                }
            }
            $this->AddSessionItem("AdminMessage","Page Updated Successfully");
            echo "success||t||" . site_url('admin/pages');
        }else{
            redirect(site_url('admin/pages'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_pages,false,array('PKPageID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['area_detail_page_records'] = $this->model_database->GetRecords($this->tbl_pages,"R","PKPageID as ID,Title","Template='Area' or Template='Area Detail'");
                if($data['record']['Template'] != "None"){
                    $page_section_records = $this->model_database->GetRecords($this->tbl_page_area_sections,"R","Title,Content,ImageName",array("FKPageID" => $id),false,false,false,"PKSectionID","asc");
                    if(sizeof($page_section_records) > 0){
                        $count = 1;
                        foreach($page_section_records as $rec){
                            $data['record']["Section_" . $count . "_Title"] = $rec['Title'];
                            $data['record']["Section_" . $count . "_Content"] = $rec['Content'];
                            $data['record']["Section_" . $count . "_Image_Name"] = $rec['ImageName'];
                            $count += 1;
                        }
                    }
                    if($data['record']['Template'] == "Area"){
                        $area_page_array = array();
                        $area_page_records = $this->model_database->GetRecords($this->tbl_page_areas,"R","FKAreaDetailID",array('FKPageID' => $id));
                        if(sizeof($area_page_records) > 0){
                            foreach($area_page_records as $rec){
                                $area_page_array[] = $rec['FKAreaDetailID'];
                            }
                        }
                        $data['record']['area_page_array'] = $area_page_array;
                    }
                }
                $this->show_view_with_menu("admin/pages",$data);
            }else{
                redirect(site_url('admin/pages/add'));
            }
        }else{
            redirect(site_url('admin/pages'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $navigation_record = $this->model_database->GetRecord($this->tbl_navigations,'Name',array('FKPageID' => $id));
            if($navigation_record !== false){
                echo "you can not delete this page because it is link to Menu (" . $navigation_record['Name'] . ')' ;
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_page_areas,$id,"FKPageID");
            $this->model_database->RemoveRecord($this->tbl_page_area_sections,$id,"FKPageID");
            $this->model_database->RemoveRecord($this->tbl_pages,$id,"PKPageID");
            echo 'success';
        }else{
            redirect(site_url('admin/pages'));
        }
    }

}