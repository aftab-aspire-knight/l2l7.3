<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Dashboard extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
    }

    function index() {
        $data['is_dummy'] = true;
        $admin_detail = $this->GetCurrentAdminDetail();
        /* Hassan changes 23-01-2018 */
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        $data['FKFranchiseID'] = $this->FKFranchiseID;
        if ($admin_detail['Role'] != "Driver") {
            /* Hassan changes 23-01-2018 */
            if ($this->FKFranchiseID != "0") {
                $q = 'FKFranchiseID in (' . $this->FKFranchiseID . ')';
                $data['invoice_total_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", $q);

                $data['invoice_pending_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", $q . " and OrderStatus='Pending'");
                $data['invoice_completed_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", $q . " and OrderStatus='Completed'");
            } else {
                $data['member_count'] = $this->model_database->GetRecords($this->tbl_members, "C", "PKMemberID");
                $data['invoice_total_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", false);
                $data['invoice_pending_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Pending'));
                $data['invoice_completed_count'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Completed'));
            }
        }
//        $invoice_count_array = array();
//        $invoice_counter = 0;
//        for($i=1;$i<23;$i++){
//            $search_time = sprintf("%02d",$i) . ':00-' . sprintf("%02d",($i + 1)) . ':00';
//            $invoice_count_array[$invoice_counter]['PickUp'] = $this->model_database->GetRecords($this->tbl_invoices,"C","PKInvoiceID",array('PaymentStatus'=>'Completed','PickupTime' => $search_time));
//            $invoice_count_array[$invoice_counter]['Delivery'] = $this->model_database->GetRecords($this->tbl_invoices,"C","PKInvoiceID",array('PaymentStatus'=>'Completed','DeliveryTime' => $search_time));
//            $invoice_counter += 1;
//        }
//        $data['invoice_count_array'] = $invoice_count_array;
        $this->show_view_with_menu("admin/dashboard", $data);
    }

    function logout() {
        $admin = array(
            'admin_love_2_laundry_detail',
            'admin_love_2_laundry_id',
            'is_love_2_laundry_admin_logged_in'
        );
        $this->RemoveSessionItems($admin);
        $this->RemoveSessionItems("Last_Admin_Visited_Page_URL");
        redirect(site_url('admin/login'));
    }

    function customerneverplaceedorderlistener() { /* Hassan changes 23-01-2018 */
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        if ($this->FKFranchiseID > 0) {
            return false;
        } /* ends */
        echo $this->model_database->GenerateTable($this->tbl_members, 'ws_members.EmailAddress,ws_members.Phone', 'Dashboard', 'Customer1');
    }

    function customerneverplaceedorderexport() { /* Hassan changes 23-01-2018 */
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        if ($this->FKFranchiseID > 0) {
            return false;
        } /* ends */
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="customerneverplaceedorder-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array(
            'First Name', 'Last Name', 'Email Address', 'Phone No'
        ));
        foreach ($this->model_database->DashboardCustomer1Export() as $record) {
            fputcsv($handle, array(
                $record['FirstName'], $record['LastName'], $record['EmailAddress'], $record['Phone']
            ));
        }
        fclose($handle);
        exit;
    }

    function customerneverplaceedordermonthlistener() { /* Hassan changes 23-01-2018 */
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        if ($this->FKFranchiseID > 0) {
            return false;
        } /* ends */
        echo $this->model_database->GenerateTable($this->tbl_members, 'EmailAddress,Phone', 'Dashboard', 'Customer2');
    }

    function customerneverplaceedordermonthexport() { /* Hassan changes 23-01-2018 */
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        if ($this->FKFranchiseID > 0) {
            return false;
        } /* ends */
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="customerneverplaceedordermonth-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array(
            'First Name', 'Last Name', 'Email Address', 'Phone No'
        ));
        foreach ($this->model_database->DashboardCustomer2Export() as $record) {
            fputcsv($handle, array(
                $record['FirstName'], $record['LastName'], $record['EmailAddress'], $record['Phone']
            ));
        }
        fclose($handle);
        exit;
    }

    function loadstats() {
        $postData = $this->input->post(NULL, TRUE);
        $this->FKFranchiseID = $this->GetCurrentFranchiseID();
        if ($postData) {
            $search_date_time = date('Y-m-d');
            if ($postData["period"] == "0") {
                $search_date_time = date('Y-m-d', strtotime("-7 days"));
            } else {
                $search_date_time = date('Y-m-d', strtotime("-30 days"));
            }
            if ($this->FKFranchiseID && $this->FKFranchiseID != "0") {
                $FKFranchiseIDStr = ' AND FKFranchiseID  in (' . $this->FKFranchiseID . ")";
            } else {
                $FKFranchiseIDStr = '';
            }



            $member_records = $this->model_database->GetRecords($this->tbl_members, "R", "PKMemberID", "CreatedDateTime between '" . $search_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'" . $FKFranchiseIDStr);
            $invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,FKMemberID,GrandTotal", "((PaymentMethod='Stripe' and PaymentStatus='Completed') or (PaymentMethod='Cash' and (OrderStatus='Completed' or OrderStatus='Processed' ))) and CreatedDateTime between '" . $search_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'" . $FKFranchiseIDStr);
            $invoice_amount = 0;
            $member_invoice_array = array();
            foreach ($invoice_records as $key => $value) {
                $invoice_amount += $value["GrandTotal"];
                $member_invoice_array[] = $value["FKMemberID"];
            }
            $member_array = array_count_values($member_invoice_array);
            $repeat_customer_count = 0;
            foreach ($member_array as $key => $value) {
                if ($value > 1) {
                    $repeat_customer_count += 1;
                }
            }
            $member_invoice_count = 0;
            $member_invoice_amount = 0;
            foreach ($member_records as $key => $value) {
                $invoice_member_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID,GrandTotal", array("FKMemberID" => $value["PKMemberID"], "PaymentStatus" => "Completed"));
                foreach ($invoice_member_records as $key1 => $value1) {
                    $member_invoice_count += 1;
                    $member_invoice_amount += $value1["GrandTotal"];
                }
            }
            echo sizeof($member_records) . "||" . sizeof($invoice_records) . "||" . $invoice_amount . "||" . $member_invoice_count . "||" . $member_invoice_amount . "||" . $repeat_customer_count;
        }
    }

    function exportpickdeliverytimereport() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="export-pickup-delivery-time-report-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array(
            'Time', 'PickUp Last 7 Days Total', 'PickUp Last 30 Days Total', 'PickUp Total', 'Delivery Last 7 Days Total', 'Delivery Last 30 Days Total', 'Delivery Total'
        ));
        $invoice_counter = 0;
        $search_week_date_time = date('Y-m-d', strtotime("-7 days"));
        $search_month_date_time = date('Y-m-d', strtotime("-30 days"));
        for ($i = 1; $i < 23; $i++) {
            $search_time = sprintf("%02d", $i) . ':00-' . sprintf("%02d", ($i + 1)) . ':00';
            $pickup_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('PaymentStatus' => 'Completed', 'PickupTime' => $search_time));
            $pickup_week_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "PaymentStatus='Completed' and PickupTime='" . $search_time . "' and CreatedDateTime between '" . $search_week_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'");
            $pickup_month_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "PaymentStatus='Completed' and PickupTime='" . $search_time . "' and CreatedDateTime between '" . $search_month_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'");
            $delivery_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('PaymentStatus' => 'Completed', 'DeliveryTime' => $search_time));
            $delivery_week_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "PaymentStatus='Completed' and DeliveryTime='" . $search_time . "' and CreatedDateTime between '" . $search_week_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'");
            $delivery_month_total = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "PaymentStatus='Completed' and DeliveryTime='" . $search_time . "' and CreatedDateTime between '" . $search_month_date_time . " 00:00:00.000' and '" . date('Y-m-d') . " 23:59:59.997'");
            fputcsv($handle, array(
                $search_time, $pickup_week_total, $pickup_month_total, $pickup_total, $delivery_week_total, $delivery_month_total, $delivery_total
            ));
            $invoice_counter += 1;
        }
        fclose($handle);
        exit;
    }

}
