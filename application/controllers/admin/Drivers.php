<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Drivers extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }
//    function index(){
//        $data['is_dummy'] = true;
//        $data['invoice_records'] = $this->model_database->GetRecords($this->tbl_invoices,"R","PKInvoiceID,FKFranchiseID,FKMemberID,InvoiceNumber,Location,Locker,BuildingName,StreetName,PostalCode,Town,PickupDate,PickupTime,DeliveryDate,DeliveryTime,GoogleDirectionInformation","FKMemberID != 1 and PaymentStatus='Completed' and OrderStatus='Processed' and (PickupDate='" . date('Y-m-d') . "' or DeliveryDate='" . date('Y-m-d') . "')",false,false,false,"PKInvoiceID");
//        if(sizeof($data['invoice_records']) > 0){
//            foreach($data['invoice_records'] as $key=>$value){
//                $is_valid = false;
//                if(!empty($value['GoogleDirectionInformation'])){
//                    $response_result_json_data = json_decode($value['GoogleDirectionInformation'], true);
//                    if(isset($response_result_json_data['status'])){
//                        if(strtolower($response_result_json_data['status']) == "ok"){
//                            if(isset($response_result_json_data['routes'][0]['legs'][0]['distance'])){
//                                $is_valid = true;
//                                $data['invoice_records'][$key]['DistanceText'] = $response_result_json_data['routes'][0]['legs'][0]['distance']['text'];
//                                $data['invoice_records'][$key]['DistanceValue'] = (int)$response_result_json_data['routes'][0]['legs'][0]['distance']['value'];
//                                $member_record = $this->model_database->GetRecord($this->tbl_members,"EmailAddress,Phone",array('PKMemberID' => $value['FKMemberID']));
//                                if($member_record != false){
//                                    $data['invoice_records'][$key]['MemberEmailAddress'] = $member_record['EmailAddress'];
//                                    $data['invoice_records'][$key]['MemberPhone'] = $member_record['Phone'];
//                                }else{
//                                    $data['invoice_records'][$key]['MemberEmailAddress'] = "Unknown";
//                                    $data['invoice_records'][$key]['MemberPhone'] = "Unknown";
//                                }
//                            }
//                        }
//                    }
//                }
//                if(!$is_valid){
//                    unset($data['invoice_records'][$key]);
//                }else{
//                    $location_name = trim($value['Location']) . " " . trim($value['PostalCode']) . "<br/>Locker : " . $value['Locker'];
//                    if ($value['Location'] == "" || $value['Location'] == "Add" || $value['Location'] == "0" || $value['Location'] == 0 || $value['Location'] == null) {
//                        $location_name = trim($value['BuildingName']) . " " . trim($value['StreetName']) . " " . trim($value['Town']) . " " . trim($value['PostalCode']);
//                    }
//                    $data['invoice_records'][$key]['Address'] = $location_name;
//                    unset($data['invoice_records'][$key]['Location']);
//                    unset($data['invoice_records'][$key]['Locker']);
//                    unset($data['invoice_records'][$key]['BuildingName']);
//                    unset($data['invoice_records'][$key]['StreetName']);
//                    unset($data['invoice_records'][$key]['Town']);
//                    unset($data['invoice_records'][$key]['PostalCode']);
//                    unset($data['invoice_records'][$key]['GoogleDirectionInformation']);
//                }
//            }
//        }
//        usort($data['invoice_records'], function($a, $b) {
//            return $a['DistanceValue'] - $b['DistanceValue'];
//        });
//        $this->show_view_with_menu("admin/drivers",$data);
//    }
    function index(){
        $data['is_dummy'] = true;
        $today_date = date('Y-m-d');
        $data['invoice_records'] = $this->model_database->GetRecords($this->tbl_invoices,"R","PKInvoiceID,InvoiceNumber,PickupDate,PickupTime,DeliveryDate,DeliveryTime,PickupDriverConfirmed,DeliveryDriverConfirmed","FKMemberID != 1 and PaymentStatus='Completed' and OrderStatus='Processed' and (PickupDate='" . $today_date . "' or DeliveryDate='" . $today_date . "')",false,false,false,"PKInvoiceID");
        if(sizeof($data['invoice_records']) > 0){
            foreach($data['invoice_records'] as $key=>$value){
                if($value['PickupDate'] == $today_date){
                    if($value['PickupDriverConfirmed'] == "Yes"){
                        unset($data['invoice_records'][$key]);
                    }else{
                        $data['invoice_records'][$key]['OrderType'] = "Pick Up";
                        $data['invoice_records'][$key]['OrderTime'] = strtotime($value['PickupTime']);
                    }
                }else{
                    if($value['DeliveryDriverConfirmed'] == "Yes"){
                        unset($data['invoice_records'][$key]);
                    }else{
                        $data['invoice_records'][$key]['OrderType'] = "Delivery";
                        $data['invoice_records'][$key]['OrderTime'] = strtotime($value['DeliveryTime']);
                    }
                }
            }
        }
        if(sizeof($data['invoice_records']) > 0){
            usort($data['invoice_records'], function($a, $b) {
                return $a['OrderTime'] - $b['OrderTime'];
            });
        }
        $this->show_view_with_menu("admin/drivers",$data);
    }
    function load(){
        $response_array = array();
        $response_array["type"] = "Success";
        $today_date = date('Y-m-d');
        $invoice_records = $this->model_database->GetRecords($this->tbl_invoices,"R","PKInvoiceID,InvoiceNumber,PickupDate,PickupTime,DeliveryDate,DeliveryTime,PickupDriverConfirmed,DeliveryDriverConfirmed","FKMemberID != 1 and PaymentStatus='Completed' and OrderStatus='Processed' and (PickupDate='" . $today_date . "' or DeliveryDate='" . $today_date . "')",false,false,false,"PKInvoiceID");
        if(sizeof($invoice_records) > 0){
            foreach($invoice_records as $key=>$value){
                if($value['PickupDate'] == $today_date){
                    if($value['PickupDriverConfirmed'] == "Yes"){
                        unset($invoice_records[$key]);
                    }else{
                        $invoice_records[$key]['OrderType'] = "Pick Up";
                        $invoice_records[$key]['OrderTime'] = strtotime($value['PickupTime']);
                    }
                }else{
                    if($value['DeliveryDriverConfirmed'] == "Yes"){
                        unset($invoice_records[$key]);
                    }else{
                        $invoice_records[$key]['OrderType'] = "Delivery";
                        $invoice_records[$key]['OrderTime'] = strtotime($value['DeliveryTime']);
                    }
                }
            }
        }
        if(sizeof($invoice_records) > 0) {
            usort($invoice_records, function ($a, $b) {
                return $a['OrderTime'] - $b['OrderTime'];
            });
        }
        $response_array['data'] = $invoice_records;
        header('Content-Type: application/json');
        echo json_encode($response_array);
    }
    function confirm(){
        if ($this->input->is_ajax_request() == true) {
            $postData = $this->input->post(NULL, TRUE);
            $response_array = array();
            $response_array["type"] = "Error";
            if ($postData) {
                $record = $this->model_database->GetRecord($this->tbl_invoices,"PKInvoiceID",array('PKInvoiceID'=>$postData['id']));
                if($record !== false){
                     $update_invoice_information = array(
                        'ID' => $record["PKInvoiceID"]
                    );
                    if($postData['type'] == "Delivery"){
                        $update_invoice_information['DeliveryDriverConfirmed'] = "Yes";
                    }else{
                        $update_invoice_information['PickupDriverConfirmed'] = "Yes";
                    }
                    $this->model_database->UpdateRecord($this->tbl_invoices,$update_invoice_information,'PKInvoiceID');
                    $response_array["type"] = "Success";
                    $response_array["message"] = "Order Confirmed";
                }else{
                    $response_array["message"] = "No Record Found";
                }
            }else{
                $response_array["message"] = "No Post Data";
            }
            header('Content-Type: application/json');
            echo json_encode($response_array);
        }else{
            redirect(site_url('admin/drivers'));
        }
    }
    function view(){
        if($this->input->is_ajax_request() == true){
            $postData = $this->input->post(NULL,TRUE);
            $response_array = array();
            $response_array["type"] = "Error";
            if($postData){
                $record = $this->model_database->GetRecord($this->tbl_invoices,"PKInvoiceID,InvoiceNumber,FKMemberID,Location,Locker,BuildingName,StreetName,PostalCode,Town,PickupDate,PickupTime,DeliveryDate,DeliveryTime",array('PKInvoiceID'=>$postData['id']));
                if($record !== false){
                    $member_record = $this->model_database->GetRecord($this->tbl_members,"FirstName,LastName,EmailAddress,Phone",array('PKMemberID' => $record['FKMemberID']));
                    if($member_record != false){
                        $record['MemberFullName'] = $member_record['FirstName'] . ' ' . $member_record['LastName'];
                        $record['MemberEmailAddress'] = $member_record['EmailAddress'];
                        $record['MemberPhone'] = $member_record['Phone'];
                    }else{
                        $record['MemberFullName'] = "Unknown";
                        $record['MemberEmailAddress'] = "Unknown";
                        $record['MemberPhone'] = "Unknown";
                    }
                    $location_name = "";
                    if ($record['Location'] == "" || $record['Location'] == "Add" || $record['Location'] == "0" || $record['Location'] == 0 || $record['Location'] == null) {
                        $location_name = trim($record['BuildingName']) . " " . trim($record['StreetName']) . " " . trim($record['Town']) . " " . trim($record['PostalCode']);
                    }else{
                        $location_array = explode("||",$record['Location']);
                        if(sizeof($location_array) > 1){
                            $location_name = $location_array[1];
                        }else{
                            $location_name = $location_array[0];
                        }
                        $location_name .= ' ' . trim($record['PostalCode']) . "<br/>Locker : " . $record['Locker'];
                    }
                    $record['Address'] = $location_name;
                    unset($record['FKMemberID']);
                    unset($record['Location']);
                    unset($record['Locker']);
                    unset($record['BuildingName']);
                    unset($record['StreetName']);
                    unset($record['Town']);
                    unset($record['PostalCode']);
                    $response_array["type"] = "Success";
                    $response_array["data"] = $record;
                }else{
                    $response_array["message"] = "No Record Found";
                }
            }else{
                $response_array["message"] = "No Post Data";
            }
            header('Content-Type: application/json');
            echo json_encode($response_array);
        }else{
            redirect(site_url('admin/drivers'));
        }
    }
    function map($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_invoices,"PKInvoiceID,InvoiceNumber,Location,PostalCode,Locker,BuildingName,StreetName,Town",array('PKInvoiceID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Map";
                $location_name = "";
                if ($data['record']['Location'] == "" || $data['record']['Location'] == "Add" || $data['record']['Location'] == "0" || $data['record']['Location'] == 0 || $data['record']['Location'] == null) {
                    $location_name = trim($data['record']['BuildingName']) . " " . trim($data['record']['StreetName']) . " " . trim($data['record']['Town']) . " " . trim($data['record']['PostalCode']);
                }else{
                    $location_array = explode("||",$data['record']['Location']);
                    if(sizeof($location_array) > 1){
                        $location_name = $location_array[1];
                    }else{
                        $location_name = $location_array[0];
                    }
                    $location_name .= ' ' . trim($data['record']['PostalCode']);
                }
                $data['record']['Address'] = $location_name;
                unset($data['record']['Location']);
                unset($data['record']['Locker']);
                unset($data['record']['BuildingName']);
                unset($data['record']['StreetName']);
                unset($data['record']['Town']);
                unset($data['record']['PostalCode']);
                $this->show_view_with_menu("admin/drivers",$data);
            }else{
                redirect(site_url('admin/drivers'));
            }
        }else{
            redirect(site_url('admin/drivers'));
        }
    }
}