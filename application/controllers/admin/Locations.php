<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Locations extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/locations");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_locations,'PKLocationID as ID,Title,PostalCode,Status','locations','Location');
    }

    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/locations/insert');
        $this->show_view_with_menu("admin/locations",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            /*$location_exist = $this->model_database->GetRecord($this->tbl_locations,"PKLocationID",array('Title'=>$postData['title']));
            if($location_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }*/
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_location =  array(
                'Title' => $postData['title'],
                'PostalCode' => preg_replace('/[\s]+/','',$postData['postal_code']),
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $location_id = $this->model_database->InsertRecord($this->tbl_locations,$insert_location);
            $locker_array = json_decode($postData['assigned_output'], true);
            if($locker_array !== null && sizeof($locker_array) > 0){
                $position = 0;
                foreach ($locker_array as $rec){
                    $insert_locker =  array(
                        'FKLocationID' => $location_id,
                        'Title' => $rec['name'],
                        'Position' => $position
                    );
                    $this->model_database->InsertRecord($this->tbl_lockers,$insert_locker);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Location Add Successfully");
            echo "success||t||" . site_url('admin/locations');
        }else{
            redirect(site_url('admin/locations'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_locations,false,array('PKLocationID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/locations/update');
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_lockers,"R",'Title',array('FKLocationID'=>$id),false,false,false,"Position","asc");
                $this->show_view_with_menu("admin/locations",$data);
            }else{
                redirect(site_url('admin/locations/add'));
            }
        }else{
            redirect(site_url('admin/locations'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_locations,false,array('PKLocationID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_lockers,"R",'Title',array('FKLocationID'=>$id),false,false,false,"Position","asc");
                $this->show_view_with_menu("admin/locations",$data);
            }else{
                redirect(site_url('admin/locations/add'));
            }
        }else{
            redirect(site_url('admin/locations'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $location_id = $postData['location_id'];
            /*$location_exist = $this->model_database->GetRecord($this->tbl_locations,"PKLocationID",array('Title'=>$postData['title'],'PKLocationID !='=>$location_id));
            if($location_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }*/
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_location =  array(
                'Title' => $postData['title'],
                'PostalCode' => preg_replace('/[\s]+/','',$postData['postal_code']),
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $location_id
            );
            $this->model_database->UpdateRecord($this->tbl_locations,$update_location,"PKLocationID");
            $this->model_database->RemoveRecord($this->tbl_lockers,$location_id,"FKLocationID");
            $locker_array = json_decode($postData['assigned_output'], true);
            if($locker_array !== null && sizeof($locker_array) > 0){
                $position = 0;
                foreach ($locker_array as $rec){
                    $insert_locker =  array(
                        'FKLocationID' => $location_id,
                        'Title' => $rec['name'],
                        'Position' => $position
                    );
                    $this->model_database->InsertRecord($this->tbl_lockers,$insert_locker);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Location Updated Successfully");
            echo "success||t||" . site_url('admin/locations');
        }else{
            redirect(site_url('admin/locations'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_lockers,$id,"FKLocationID");
            $this->model_database->RemoveRecord($this->tbl_locations,$id,"PKLocationID");
            echo 'success';
        }else{
            redirect(site_url('admin/locations'));
        }
    }
}
