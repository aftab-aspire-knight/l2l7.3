<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Navigations extends Base_Admin_Controller{
    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/navigations");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_navigation_sections,'PKSectionID as ID,Title','navigations','Navigation');
    }

    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/navigations/insert');
        $data['page_records'] = $this->model_database->GetRecords($this->tbl_pages,"R",'PKPageID as ID,Title,AccessURL as URL',false,false,false,false,'Title','asc');
        $this->show_view_with_menu("admin/navigations",$data);
    }
    
    private function _InsertNavigation($menu,$section_id,$position){
        $page_id = $menu['id'];
        $url = $menu['url'];
        $link_with = "Page";
        if($page_id == 0){
            $link_with = "Url";
        }
        $insert_navigation =  array(
            'FKSectionID' => $section_id,
            'Name' => $menu['name'],
            'FKPageID' => $page_id,
            'LinkWith' => $link_with,
            'LinkWithURL' => $url,
            'Position' => $position
        );
        return $this->model_database->InsertRecord($this->tbl_navigations,$insert_navigation);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $section_exist = $this->model_database->GetRecord($this->tbl_navigation_sections,"PKSectionID",array('Title'=>$postData['n_title']));
            if($section_exist !== false){
                echo "error||a||n_title||Title already exist in our records";
                return;
            }
            $insert_section =  array(
                'Title' => $postData['n_title'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $section_id = $this->model_database->InsertRecord($this->tbl_navigation_sections,$insert_section);
            $navigation_array = json_decode($postData['assigned_output'], true);
            if($navigation_array !== null && sizeof($navigation_array) > 0){
                $position = 0;
                foreach ($navigation_array as $rec){
                    $this->_InsertNavigation($rec,$section_id,$position);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Navigation Add Successfully");
            echo "success||t||" . site_url('admin/navigations');
        }else{
            redirect(site_url('admin/navigations'));
        }
    }
    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_navigation_sections,false,array('PKSectionID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/navigations/update');
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_navigations,"R",'PKNavigationID,FKPageID,Name,LinkWith,LinkWithURL',array('FKSectionID'=>$id),false,false,false,"Position","asc");
                if(sizeof($data['record']['Navigation']) > 0){
                    foreach ($data['record']['Navigation'] as $key => $value) {
                        if($value['FKPageID'] != 0){
                            $page_record = $this->model_database->GetRecord($this->tbl_pages,'Title',array('PKPageID'=>$value['FKPageID']));
                            if($page_record !== false){
                                $data['record']['Navigation'][$key]['PageName'] = $page_record['Title'];
                            }else{
                                $data['record']['Navigation'][$key]['PageName'] = "Unknown";
                            }
                            $data['record']['Navigation'][$key]['Type'] = "Page";
                        }else{
                            $data['record']['Navigation'][$key]['Type'] = "Link";
                            $data['record']['Navigation'][$key]['PageName'] = $value['Name'];
                        }
                    }
                }
                $data['page_records'] = $this->model_database->GetRecords($this->tbl_pages,"R",'PKPageID as ID,Title,AccessURL as URL',false,false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/navigations",$data);
            }else{
                redirect(site_url('admin/navigations/add'));
            }
        }else{
            redirect(site_url('admin/navigations'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_navigation_sections,false,array('PKSectionID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_navigations,"R",'PKNavigationID,FKPageID,Name,LinkWith,LinkWithURL',array('FKSectionID'=>$id),false,false,false,"Position","asc");
                if(sizeof($data['record']['Navigation']) > 0){
                    foreach ($data['record']['Navigation'] as $key => $value) {
                        if($value['FKPageID'] != 0){
                            $page_record = $this->model_database->GetRecord($this->tbl_pages,'Title',array('PKPageID'=>$value['FKPageID']));
                            if($page_record !== false){
                                $data['record']['Navigation'][$key]['PageName'] = $page_record['Title'];
                            }else{
                                $data['record']['Navigation'][$key]['PageName'] = "Unknown";
                            }
                            $data['record']['Navigation'][$key]['Type'] = "Page";
                        }else{
                            $data['record']['Navigation'][$key]['Type'] = "Link";
                            $data['record']['Navigation'][$key]['PageName'] = $value['Name'];
                        }
                    }
                }
                $this->show_view_with_menu("admin/navigations",$data);
            }else{
                redirect(site_url('admin/navigations/add'));
            }
        }else{
            redirect(site_url('admin/navigations'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $section_id = $postData['section_id'];

            $section_exist = $this->model_database->GetRecord($this->tbl_navigation_sections,"PKSectionID",array('PKSectionID !='=>$section_id,'Title'=>$postData['n_title']));
            if($section_exist !== false){
                echo "error||a||n_title||Title already exist in our records";
                return;
            }
            
            $update_section =  array(
                'Title' => $postData['n_title'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $section_id
            );
            $this->model_database->UpdateRecord($this->tbl_navigation_sections,$update_section,"PKSectionID");
            $this->model_database->RemoveRecord($this->tbl_navigations,$section_id,"FKSectionID");
            $navigation_array = json_decode($postData['assigned_output'], true);
            if($navigation_array !== null && sizeof($navigation_array) > 0){
                $position = 0;
                foreach ($navigation_array as $rec){
                    $this->_InsertNavigation($rec,$section_id,$position);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Navigation Updated Successfully");
            echo "success||t||" . site_url('admin/navigations');
        }else{
            redirect(site_url('admin/navigations'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_navigations,$id,"FKSectionID");
            $this->model_database->RemoveRecord($this->tbl_navigation_sections,$id,"PKSectionID");
            echo 'success';
        }else{
            redirect(site_url('admin/navigations'));
        }
    }
}
