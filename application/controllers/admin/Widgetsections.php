<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class WidgetSections extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/widget_sections");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_widget_sections,'PKSectionID as ID,Title','widgetsections','Widget Section');
    }

    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/widgetsections/insert');
        $data['widget_records'] = $this->model_database->GetRecords($this->tbl_widgets,"R",'PKWidgetID as ID,Title',false,false,false,false,'Title','asc');
        $this->show_view_with_menu("admin/widget_sections",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $section_exist = $this->model_database->GetRecord($this->tbl_widget_sections,"PKSectionID",array('Title'=>$postData['title']));
            if($section_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $insert_section =  array(
                'Title' => $postData['title'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $section_id = $this->model_database->InsertRecord($this->tbl_widget_sections,$insert_section);
            $widget_array = json_decode($postData['assigned_output'], true);
            if($widget_array !== null && sizeof($widget_array) > 0){
                $position = 0;
                foreach ($widget_array as $rec){
                    $insert_navigation =  array(
                        'FKSectionID' => $section_id,
                        'FKWidgetID' => $rec['id'],
                        'Position' => $position
                    );
                    $this->model_database->InsertRecord($this->tbl_widget_navigations,$insert_navigation);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Widget Section Add Successfully");
            echo "success||t||" . site_url('admin/widgetsections');
        }else{
            redirect(site_url('admin/widgetsections'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_widget_sections,false,array('PKSectionID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/widgetsections/update');
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_widget_navigations,"R",'PKNavigationID,FKWidgetID',array('FKSectionID'=>$id),false,false,false,"Position","asc");
                if(sizeof($data['record']['Navigation']) > 0){
                    foreach ($data['record']['Navigation'] as $key => $value) {
                        $data['record']['Navigation'][$key]['WidgetID'] = $value['FKWidgetID'];
                        $widget = $this->model_database->GetRecord($this->tbl_widgets,'Title',array('PKWidgetID'=>$value['FKWidgetID']));
                        if($widget !== false){
                            $data['record']['Navigation'][$key]['Title'] = $widget['Title'];
                        }else{
                            $data['record']['Navigation'][$key]['Title'] = "Unknown";
                        }
                    }
                }
                $data['widget_records'] = $this->model_database->GetRecords($this->tbl_widgets,"R",'PKWidgetID as ID,Title',false,false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/widget_sections",$data);
            }else{
                redirect(site_url('admin/widgetsections/add'));
            }
        }else{
            redirect(site_url('admin/widgetsections'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_widget_sections,false,array('PKSectionID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['record']['Navigation'] = $this->model_database->GetRecords($this->tbl_widget_navigations,"R",'PKNavigationID,FKWidgetID',array('FKSectionID'=>$id),false,false,false,"Position","asc");
                if(sizeof($data['record']['Navigation']) > 0){
                    foreach ($data['record']['Navigation'] as $key => $value) {
                        $data['record']['Navigation'][$key]['WidgetID'] = $value['FKWidgetID'];
                        $widget = $this->model_database->GetRecord($this->tbl_widgets,'Title',array('PKWidgetID'=>$value['FKWidgetID']));
                        if($widget !== false){
                            $data['record']['Navigation'][$key]['Title'] = $widget['Title'];
                        }else{
                            $data['record']['Navigation'][$key]['Title'] = "Unknown";
                        }
                    }
                }
                $this->show_view_with_menu("admin/widget_sections",$data);
            }else{
                redirect(site_url('admin/widgetsections/add'));
            }
        }else{
            redirect(site_url('admin/widgetsections'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $section_id = $postData['section_id'];
            $section_exist = $this->model_database->GetRecord($this->tbl_widget_sections,"PKSectionID",array('Title'=>$postData['title'],'PKSectionID !='=>$section_id));
            if($section_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $update_section =  array(
                'Title' => $postData['title'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $section_id
            );
            $this->model_database->UpdateRecord($this->tbl_widget_sections,$update_section,"PKSectionID");
            $this->model_database->RemoveRecord($this->tbl_widget_navigations,$section_id,"FKSectionID");
            $widget_array = json_decode($postData['assigned_output'], true);
            if($widget_array !== null && sizeof($widget_array) > 0){
                $position = 0;
                foreach ($widget_array as $rec){
                    $insert_navigation =  array(
                        'FKSectionID' => $section_id,
                        'FKWidgetID' => $rec['id'],
                        'Position' => $position
                    );
                    $this->model_database->InsertRecord($this->tbl_widget_navigations,$insert_navigation);
                    $position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage","Widget Section Updated Successfully");
            echo "success||t||" . site_url('admin/widgetsections');
        }else{
            redirect(site_url('admin/widgetsections'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_widget_navigations,$id,"FKSectionID");
            $this->model_database->RemoveRecord($this->tbl_widget_sections,$id,"PKSectionID");
            echo 'success';
        }else{
            redirect(site_url('admin/widgetsections'));
        }
    }
}
