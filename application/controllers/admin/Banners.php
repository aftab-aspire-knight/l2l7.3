<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Banners extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/banners");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_banners,'PKBannerID as ID,Title,ImageName as Image,Position,Status','banners','Banner');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/banners/insert');
        $this->show_view_with_menu("admin/banners",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            if(isset($_FILES['image_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_banner'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('image_file')) {
                    echo "error||a||image_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                }
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_banner =  array(
                'Title' =>$postData['title'],
                'Link' => $postData['link'],
                'Position' => $postData['position'],
                'Content' => $postData['content'],
                'ImageName' => $postData['pic_path'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $banner_id = $this->model_database->InsertRecord($this->tbl_banners,$insert_banner);
            $this->AddSessionItem("AdminMessage","Banner Add Successfully");
            echo "success||t||" . site_url('admin/banners');
        }else{
            redirect(site_url('admin/banners'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_banners,false,array('PKBannerID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/banners/update');
                $this->show_view_with_menu("admin/banners",$data);
            }else{
                redirect(site_url('admin/banners/add'));
            }
        }else{
            redirect(site_url('admin/banners'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            if(isset($_FILES['image_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_banner'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('image_file')) {
                    echo "error||a||image_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_banner_thumb'),150,120);
                }
            }
            $banner_id = $postData['banner_id'];
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_banner =  array(
                'Title' =>$postData['title'],
                'Link' => $postData['link'],
                'Position' => $postData['position'],
                'Content' => $postData['content'],
                'ImageName' => $postData['pic_path'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $banner_id
            );

            $this->model_database->UpdateRecord($this->tbl_banners,$update_banner,"PKBannerID");
            $this->AddSessionItem("AdminMessage","Banner Updated Successfully");
            echo "success||t||" . site_url('admin/banners');
        }else{
            redirect(site_url('admin/banners'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_banners,false,array('PKBannerID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $this->show_view_with_menu("admin/banners",$data);
            }else{
                redirect(site_url('admin/banners/add'));
            }
        }else{
            redirect(site_url('admin/banners'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_banners,$id,"PKBannerID");
            echo 'success';
        }else{
            redirect(site_url('admin/banners'));
        }
    }
}