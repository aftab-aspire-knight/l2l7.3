<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Tags extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/tags");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_tags,'PKTagID as ID,Title,Tag','tags','Tag');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/tags/insert');
        $this->show_view_with_menu("admin/tags",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $title_field_exist = $this->model_database->GetRecord($this->tbl_tags,"PKTagID",array('Title'=>$postData['title']));
            if($title_field_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $tag_exist = $this->model_database->GetRecord($this->tbl_tags,"PKTagID",array('Tag'=>$postData['tag']));
            if($tag_exist !== false){
                echo "error||a||tag||Tag already exist in our records";
                return;
            }

            $insert_tag =  array(
                'Title' => $postData['title'],
                'Tag' =>$postData['tag'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $tag_id = $this->model_database->InsertRecord($this->tbl_tags,$insert_tag);
            $this->AddSessionItem("AdminMessage","Tag Add Successfully");
            echo "success||t||" . site_url('admin/tags');
        }else{
            redirect(site_url('admin/tags'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_tags,false,array('PKTagID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/tags/update');
                $this->show_view_with_menu("admin/tags",$data);
            }else{
                redirect(site_url('admin/tags/add'));
            }
        }else{
            redirect(site_url('admin/tags'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $tag_id = $postData['tag_id'];

            $title_field_exist = $this->model_database->GetRecord($this->tbl_tags,"PKTagID",array('PKTagID !='=>$tag_id,'Title'=>$postData['title']));
            if($title_field_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }

            $tag_exist = $this->model_database->GetRecord($this->tbl_tags,"PKTagID",array('PKTagID !='=>$tag_id,'Tag'=>$postData['tag']));
            if($tag_exist !== false){
                echo "error||a||tag||Tag already exist in our records";
                return;
            }


            $update_tag =  array(
                'Title' => $postData['title'],
                'Tag' =>$postData['tag'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $tag_id
            );

            $this->model_database->UpdateRecord($this->tbl_tags,$update_tag,"PKTagID");
            $this->AddSessionItem("AdminMessage","Tag Updated Successfully");
            echo "success||t||" . site_url('admin/tags');
        }else{
            redirect(site_url('admin/tags'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_tags,false,array('PKTagID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $this->show_view_with_menu("admin/tags",$data);
            }else{
                redirect(site_url('admin/tags/add'));
            }
        }else{
            redirect(site_url('admin/tags'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_tags,$id,"PKTagID");
            echo 'success';
        }else{
            redirect(site_url('admin/tags'));
        }
    }

}