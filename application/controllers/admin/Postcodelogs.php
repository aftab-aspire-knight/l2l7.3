<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class PostCodeLogs extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/post_code_logs");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_search_post_codes,'ws_search_post_codes.PKSearchID as ID,ws_franchises.Title as FranchiseTitle,ws_search_post_codes.Code,ws_search_post_codes.IPAddress,ws_search_post_codes.CreatedDateTime','postcodelogs','Post Code Logs');
    }

}