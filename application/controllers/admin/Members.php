<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Members extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index() {
        $this->show_view_with_menu("admin/members");
    }

    function listener() {
        echo $this->model_database->GenerateTable($this->tbl_members, 'PKMemberID as ID,RegisterFrom,ReferralCode,FirstName,LastName,EmailAddress,Phone,TotalLoyaltyPoints,UsedLoyaltyPoints,Status,CreatedDateTime', 'members', 'Member');
    }

    function _PreferencesList() {
        $preference_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title', array('ParentPreferenceID' => 0, 'Status' => 'Enabled'), false, false, false, 'Position', 'asc');
        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {
                $preference_records[$key]['children'] = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title', array('ParentPreferenceID' => $value['ID'], 'Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                if (sizeof($preference_records[$key]['children']) == 0) {
                    unset($preference_records[$key]);
                }
            }
        }
        return $preference_records;
    }

    function add() {
        $data['type'] = "Add";
        $data['preference_records'] = $this->_PreferencesList();
        $data['form_action'] = site_url('admin/members/insert');
        $this->show_view_with_menu("admin/members", $data);
    }

    function insert() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $member_exist = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('EmailAddress' => $postData['email_address']));
            if ($member_exist !== false) {
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            if (!empty($post_data['referral_code'])) {
                $member_exist = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('ReferralCode' => $postData['referral_code']));
                if ($member_exist !== false) {
                    echo "error||a||referral_code||Referral Code already exist in our records";
                    return;
                }
            }
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            if (isset($postData['TotalLoyaltyPoints']) && $postData['TotalLoyaltyPoints'] > 0) {
                $insert_member = array(
                    'RegisterFrom' => $postData['register_from'],
                    'ReferralCode' => $postData['referral_code'],
                    'FirstName' => $postData['first_name'],
                    'LastName' => $postData['last_name'],
                    'Phone' => $postData['phone'],
                    'EmailAddress' => $postData['email_address'],
                    'Password' => encrypt_decrypt('encrypt', $postData['u_password']),
                    'PostalCode' => $postData['postal_code'],
                    'BuildingName' => $postData['building_name'],
                    'StreetName' => $postData['street_name'],
                    'Town' => $postData['town'],
                    'TotalLoyaltyPoints' => $postData['TotalLoyaltyPoints'],
                    'Status' => $status,
                    'CreatedBy' => $this->GetCurrentAdminID(),
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
            } else {
                $insert_member = array(
                    'RegisterFrom' => $postData['register_from'],
                    'ReferralCode' => $postData['referral_code'],
                    'FirstName' => $postData['first_name'],
                    'LastName' => $postData['last_name'],
                    'Phone' => $postData['phone'],
                    'EmailAddress' => $postData['email_address'],
                    'Password' => encrypt_decrypt('encrypt', $postData['u_password']),
                    'PostalCode' => $postData['postal_code'],
                    'BuildingName' => $postData['building_name'],
                    'StreetName' => $postData['street_name'],
                    'Town' => $postData['town'],
                    'Status' => $status,
                    'CreatedBy' => $this->GetCurrentAdminID(),
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
            }

            if (!empty($postData['account_notes'])) {
                $insert_member['AccountNotes'] = $postData['account_notes'];
            }
            $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);

            if (isset($postData['preference_list'])) {
                foreach ($postData['preference_list'] as $rec) {
                    $insert_member_preference = array(
                        'FKMemberID' => $member_id,
                        'FKPreferenceID' => $rec
                    );
                    $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                }
            }
            $this->AddSessionItem("AdminMessage", "Member Add Successfully");
            echo "success||t||" . site_url('admin/members');
        } else {
            redirect(site_url('admin/members'));
        }
    }

    function edit($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "Edit";
                $data['preference_records'] = $this->_PreferencesList();
                $member_preference_array = array();
                $member_preference_records = $this->model_database->GetRecords($this->tbl_member_preferences, "R", 'FKPreferenceID', array('FKMemberID' => $id), false, false, false, 'PKMemberPreferenceID');
                if (sizeof($member_preference_records) > 0) {
                    foreach ($member_preference_records as $key => $value) {
                        $member_preference_array[] = $value['FKPreferenceID'];
                    }
                }
                $data['record']['member_preferences_array'] = $member_preference_array;
                $data['form_action'] = site_url('admin/members/update');
                unset($data['record']['Password']);
                $this->show_view_with_menu("admin/members", $data);
            } else {
                redirect(site_url('admin/members/add'));
            }
        } else {
            redirect(site_url('admin/members'));
        }
    }

    function update() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $member_id = $postData['member_id'];

            $member__exist = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('PKMemberID !=' => $member_id, 'EmailAddress' => $postData['email_address']));
            if ($member__exist !== false) {
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            if (!empty($post_data['referral_code'])) {
                $member_exist = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", array('PKMemberID !=' => $member_id, 'ReferralCode' => $postData['referral_code']));
                if ($member_exist !== false) {
                    echo "error||a||referral_code||Referral Code already exist in our records";
                    return;
                }
            }
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            if (isset($postData['TotalLoyaltyPoints']) && $postData['TotalLoyaltyPoints'] > 0) {
                $update_member = array(
                    'ReferralCode' => $postData['referral_code'],
                    'FirstName' => $postData['first_name'],
                    'LastName' => $postData['last_name'],
                    'Phone' => $postData['phone'],
                    'EmailAddress' => $postData['email_address'],
                    'Status' => $status,
                    'PostalCode' => $postData['postal_code'],
                    'BuildingName' => $postData['building_name'],
                    'StreetName' => $postData['street_name'],
                    'Town' => $postData['town'],
                    'TotalLoyaltyPoints' => $postData['TotalLoyaltyPoints'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => $member_id
                );
            } else {
                $update_member = array(
                    'ReferralCode' => $postData['referral_code'],
                    'FirstName' => $postData['first_name'],
                    'LastName' => $postData['last_name'],
                    'Phone' => $postData['phone'],
                    'EmailAddress' => $postData['email_address'],
                    'Status' => $status,
                    'PostalCode' => $postData['postal_code'],
                    'BuildingName' => $postData['building_name'],
                    'StreetName' => $postData['street_name'],
                    'Town' => $postData['town'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => $member_id
                );
            }

            if (!empty($postData['account_notes'])) {
                $update_member['AccountNotes'] = $postData['account_notes'];
            } else {
                $update_member['AccountNotes'] = null;
            }
            if (!empty($postData['u_password'])) {
                $update_member['Password'] = encrypt_decrypt('encrypt', $postData['u_password']);
            }
            $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
            $this->model_database->RemoveRecord($this->tbl_member_preferences, $member_id, "FKMemberID");
            if (isset($postData['preference_list'])) {
                foreach ($postData['preference_list'] as $rec) {
                    $insert_member_preference = array(
                        'FKMemberID' => $member_id,
                        'FKPreferenceID' => $rec
                    );
                    $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                }
            }
            $this->AddSessionItem("AdminMessage", "Member Updated Successfully");
            echo "success||t||" . site_url('admin/members');
        } else {
            redirect(site_url('admin/members'));
        }
    }

    function view($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $id));
            if ($data['record'] !== false) {
                $data['type'] = "View";
                $data['preference_records'] = $this->_PreferencesList();
                $member_preference_array = array();
                $member_preference_records = $this->model_database->GetRecords($this->tbl_member_preferences, "R", 'FKPreferenceID', array('FKMemberID' => $id), false, false, false, 'PKMemberPreferenceID');
                if (sizeof($member_preference_records) > 0) {
                    foreach ($member_preference_records as $key => $value) {
                        $member_preference_array[] = $value['FKPreferenceID'];
                    }
                }
                $data['record']['member_preferences_array'] = $member_preference_array;
                $data['record']['Password'] = encrypt_decrypt('decrypt', $data['record']['Password']);
                $this->show_view_with_menu("admin/members", $data);
            } else {
                redirect(site_url('admin/members/add'));
            }
        } else {
            redirect(site_url('admin/members'));
        }
    }

    function delete($id) {
        if ($id != "" && $id != null) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'FKMemberID', array('FKMemberID' => $id));
            if ($invoice_record !== false) {
                echo "you can not delete this member because it have some invoices in our record";
                return;
            }
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'FKMemberID', array('FKDiscountID' => $id, 'DiscountType' => 'Referral'));
            if ($invoice_record !== false) {
                echo "you can not delete this member because it have some invoice referrals in our record";
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_member_preferences, $id, "FKMemberID");
            $this->model_database->RemoveRecord($this->tbl_members, $id, "PKMemberID");
            echo 'success';
        } else {
            redirect(site_url('admin/members'));
        }
    }

    function export() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="members-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array("Member ID", 'First Name', 'Last Name', 'Email Address', "Password", 'Phone Number', 'Address', 'Postcode', 'News Letter'));
        $member_records = $this->model_database->GetRecords($this->tbl_members, "R", "PKMemberID,FirstName,LastName,EmailAddress,Password,Phone,CONCAT_WS(',', BuildingName, StreetName, Town) Address,PostalCode,NewsLetter");

        foreach ($member_records as $record) {
            fputcsv($handle, array($record['PKMemberID'], $record['FirstName'], $record['LastName'], $record['EmailAddress'], encrypt_decrypt('decrypt', $record['Password']), $record['Phone'], $record['Address'], $record['PostalCode'], (isset($record['NewsLetter']) && $record['NewsLetter'] == 1) ? 'Yes' : 'No'
            ));
        }
        fclose($handle);
        exit;
    }

}
