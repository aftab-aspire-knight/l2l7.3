<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH."../application/core/Base_Admin_Controller.php";

class Login extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if ($this->IsAdminLogin() === true) {
            redirect(site_url('admin/dashboard'));
        } else {
            $data['header_title'] = $this->GetHeaderTitle();
            $data['website_name'] = $this->GetWebsiteName();
            $this->load->view('admin/login', $data);
        }
    }

    function postlogin() {
        if ($this->input->is_ajax_request() == true) {
            $postData = $this->input->post(NULL, TRUE);
            if ($postData) {
                $admin_record = $this->model_database->GetRecord($this->tbl_users, false, array('EmailAddress' => $postData['email_address'], 'Password' => encrypt_decrypt('encrypt', $postData['password']), 'Status' => 'Enabled'));
                if ($admin_record !== false) {
                    $admin_session_array = array(
                        'admin_love_2_laundry_detail' => $admin_record,
                        'admin_love_2_laundry_id' => $admin_record["PKUserID"],
                        'is_love_2_laundry_admin_logged_in' => true
                    );
                    $this->AddSessionItems($admin_session_array);
                    $redirect_url = site_url('admin/invoices');
                    if ($this->IsSessionItem("Last_Admin_Visited_Page_URL") == true) {
                        $redirect_url = $this->GetSessionItem("Last_Admin_Visited_Page_URL");
                    }
                    echo "success||t||" . $redirect_url;
                } else {
                    echo "error||p||password||Email Address/Password Incorrect";
                }
            }
        } else {
            redirect(site_url('admin/login'));
        }
    }

    function forgot() {
        if ($this->IsAdminLogin() === true) {
            redirect(site_url('admin/dashboard'));
        } else {
            $data['header_title'] = $this->GetHeaderTitle();
            $data['website_name'] = $this->GetWebsiteName();
            $this->load->view('admin/forgot_password', $data);
        }
    }

    function postforgot() {
        if ($this->input->is_ajax_request() == true) {
            $postData = $this->input->post(NULL, TRUE);
            if ($postData) {
                $admin_record = $this->model_database->GetRecord($this->tbl_users, 'PKUserID', array('EmailAddress' => $postData['email_address'], 'Status' => 'Enabled'));
                if ($admin_record !== false) {
                    $random_password = $this->RandomPassword();
                    $update_admin = array(
                        'ID' => $admin_record['PKUserID'],
                        'Password' => encrypt_decrypt("encrypt", $random_password)
                    );
                    $this->model_database->UpdateRecord($this->tbl_users, $update_admin, 'PKUserID');
                    $this->emailsender->send_email($this->GetWebsiteEmailSender(), $postData['email_address'], 'Administrator Account Password Recovery from ' . $this->GetWebsiteName(), 'Your Password is ' . $random_password, false);
                    echo "success||s||Password send to your desired email address";
                } else {
                    echo "error||p||email_address||Email address not found in our records";
                }
            }
        } else {
            redirect(site_url('admin/login/forgot'));
        }
    }

}
