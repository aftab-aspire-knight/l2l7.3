<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Settings extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $data['dummy'] = true;
        $setting_records = $this->model_database->GetRecords($this->tbl_settings,"R",'PKSettingID as ID,Title,Content');
        if(sizeof($setting_records) > 0){
            foreach($setting_records as $key=>$value){
                 if($value['Title'] == "Front Header Title"){
                     $data['record']['header_content'] = $value['Content'];
                 }else if($value['Title'] == "Front Footer Title"){
                    $data['record']['footer_content'] = $value['Content'];
                 }else if($value['Title'] == "Google Play Store Link URL"){
                     $data['record']['google_play_content'] = $value['Content'];
                 }else if($value['Title'] == "Apple Store Link URL"){
                     $data['record']['apple_store_content'] = $value['Content'];
                 }else if($value['Title'] == "Linkedin Link URL"){
                     $data['record']['linkedin_content'] = $value['Content'];
                 }else if($value['Title'] == "Pinterest Link URL"){
                     $data['record']['pinterest_content'] = $value['Content'];
                 }else if($value['Title'] == "Facebook Link URL"){
                     $data['record']['facebook_content'] = $value['Content'];
                 }else if($value['Title'] == "Twitter Link URL"){
                     $data['record']['twitter_content'] = $value['Content'];
                 }else if($value['Title'] == "Google Plus Link URL"){
                     $data['record']['google_plus_content'] = $value['Content'];
                 }else if($value['Title'] == "Instagram Link URL"){
                     $data['record']['instagram_content'] = $value['Content'];
                 }else if($value['Title'] == "You Tube Link URL"){
                     $data['record']['you_tube_content'] = $value['Content'];
                 }else if($value['Title'] == "Minimum Order Amount Later"){
                     $data['record']['minimum_order_amount_later_content'] = $value['Content'];
                 }else if($value['Title'] == "Referral Code Amount"){
                     $data['record']['referral_code_amount_content'] = $value['Content'];
                 }else if($value['Title'] == "Loyalty Code Amount"){
                     $data['record']['loyalty_code_amount_content'] = $value['Content'];
                 }else if($value['Title'] == "Minimum Loyalty Points"){
                     $data['record']['minimum_loyalty_points_content'] = $value['Content'];
                 }else if($value['Title'] == "Front Telephone Number"){
                     $data['record']['telephone_content'] = $value['Content'];
                 }else if($value['Title'] == "Front Email Address"){
                     $data['record']['email_content'] = $value['Content'];
                 }else if($value['Title'] == "Email Footer Title"){
                     $data['record']['email_footer_title_content'] = $value['Content'];
                 }else if($value['Title'] == "Website Status"){
                     $data['record']['website_status_content'] = $value['Content'];
                 }else if($value['Title'] == "Website Email Sender"){
                     $data['record']['website_email_sender_content'] = $value['Content'];
                 }else if($value['Title'] == "Website Email Receivers"){
                     $data['record']['website_email_receivers_content'] = $value['Content'];
                 }else if($value['Title'] == "Website Name"){
                     $data['record']['website_name_content'] = $value['Content'];
                 }else if($value['Title'] == "Website URL Address"){
                     $data['record']['website_url_address_content'] = $value['Content'];
                 }else if($value['Title'] == "Google Analytics ID"){
                     $data['record']['google_analytics_content'] = $value['Content'];
                 }else if($value['Title'] == "Stripe Payment Mode"){
                     $data['record']['stripe_payment_mode_content'] = $value['Content'];
                 }else if($value['Title'] == "Stripe JQuery Test Key"){
                     $data['record']['stripe_jquery_test_key_content'] = $value['Content'];
                 }else if($value['Title'] == "Stripe JQuery Live Key"){
                     $data['record']['stripe_jquery_live_key_content'] = $value['Content'];
                 }else if($value['Title'] == "Stripe API Test Key"){
                     $data['record']['stripe_api_test_key_content'] = $value['Content'];
                 }else if($value['Title'] == "Stripe API Live Key"){
                     $data['record']['stripe_api_live_key_content'] = $value['Content'];
                 }else if($value['Title'] == "Minimum Order Amount"){
                     $data['record']['minimum_order_amount_content'] = $value['Content'];
                 }else if($value['Title'] == "Android App Version"){
                     $data['record']['android_app_version_content'] = $value['Content'];
                 }else if($value['Title'] == "iOS App Version"){
                     $data['record']['ios_app_version_content'] = $value['Content'];
                 }
            }
        }
        $this->show_view_with_menu("admin/settings",$data);
    }
    
    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $setting_record = $this->GetSettingRecord("App Settings Version");
            if($setting_record != false && $setting_record["Content"] != "#"){
                $setting_version = intval($setting_record["Content"]);
                $update_setting =  array(
                    'Content' => ($setting_version + 1),
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 37
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }
            if($postData['active_tab'] == "#tab_website"){
                $update_setting =  array(
                    'Content' => $postData['website_name'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 17
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['website_url_address'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 27
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['header_title'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 2
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['footer_title'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 3
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['google_play_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 24
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['apple_store_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 25
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['google_analytics_id'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 18
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['telephone_no'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 12
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['email_address'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 13
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['android_app_version'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d h:i:s'),
                    'ID' => 33
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['ios_app_version'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d h:i:s'),
                    'ID' => 34
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['website_status'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 14
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }else if($postData['active_tab'] == "#tab_email"){
                $update_setting =  array(
                    'Content' => $postData['website_email_sender'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 15
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['website_email_receivers'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d h:i:s'),
                    'ID' => 16
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['email_footer_title'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d h:i:s'),
                    'ID' => 28
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }else if ($postData['active_tab'] == "#tab_order"){
                $update_setting =  array(
                    'Content' => $postData['minimum_order_amount_later'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 11
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['minimum_order_amount'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 31
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['referral_code_amount'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 26
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['loyalty_code_amount'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 29
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['minimum_loyalty_points'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 30
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }else if($postData['active_tab'] == "#tab_social"){
                $update_setting =  array(
                    'Content' => $postData['facebook_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 6
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['twitter_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 7
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['google_plus_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 8
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['instagram_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 9
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['you_tube_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 10
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['pinterest_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 5
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['linkedin_url'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 4
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }else if($postData['active_tab'] == "#tab_stripe"){
                $update_setting =  array(
                    'Content' => $postData['stripe_payment_mode'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 19
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['stripe_jquery_test_key'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 20
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['stripe_api_test_key'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 22
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['stripe_jquery_live_key'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 21
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
                $update_setting =  array(
                    'Content' => $postData['stripe_api_live_key'],
                    'UpdatedBy' => $this->GetCurrentAdminID(),
                    'UpdatedDateTime' => date('Y-m-d H:i:s'),
                    'ID' => 23
                );
                $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
                unset($update_setting);
            }
            $this->AddSessionItem("AdminMessage","Settings Updated Successfully");
            echo "success||t||" . site_url('admin/settings');
        }else{
            redirect(site_url('admin/settings'));
        }
    }

}
