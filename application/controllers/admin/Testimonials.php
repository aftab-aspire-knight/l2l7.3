<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Testimonials extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/testimonials");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_testimonials,'PKTestimonialID as ID,Title,Name,EmailAddress,Rating,TestimonialDate,Status','testimonials','Testimonial');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/testimonials/insert');
        $this->show_view_with_menu("admin/testimonials",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_testimonial =  array(
                'Title' => $postData['title'],
                'Name' =>$postData['name'],
                'EmailAddress' => $postData['email_address'],
                'Content' => $postData['content'],
                'Rating' => $postData['rating'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            if(!empty($postData['testimonial_date'])){
                $insert_testimonial['TestimonialDate'] = date('Y-m-d',strtotime($postData['testimonial_date']));
            }
            
            $testimonial_id = $this->model_database->InsertRecord($this->tbl_testimonials,$insert_testimonial);
            $this->AddSessionItem("AdminMessage","Testimonial Add Successfully");
            echo "success||t||" . site_url('admin/testimonials');
        }else{
            redirect(site_url('admin/testimonials'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_testimonials,false,array('PKTestimonialID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/testimonials/update');
                if(empty($data['record']['TestimonialDate'])){
                    unset($data['record']['TestimonialDate']);
                }
                $this->show_view_with_menu("admin/testimonials",$data);
            }else{
                redirect(site_url('admin/testimonials/add'));
            }
        }else{
            redirect(site_url('admin/testimonials'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $testimonial_id = $postData['testimonial_id'];
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_testimonial =  array(
                'Title' => $postData['title'],
                'Name' =>$postData['name'],
                'EmailAddress' => $postData['email_address'],
                'Content' => $postData['content'],
                'Rating' => $postData['rating'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $testimonial_id
            );
           
            if(!empty($postData['testimonial_date'])){
                $update_testimonial['TestimonialDate'] = date('Y-m-d',strtotime($postData['testimonial_date']));
            }else{
                $update_testimonial['TestimonialDate'] = null;
            }
            $this->model_database->UpdateRecord($this->tbl_testimonials,$update_testimonial,"PKTestimonialID");
            
            $this->AddSessionItem("AdminMessage","Testimonial Updated Successfully");
            echo "success||t||" . site_url('admin/testimonials');
        }else{
            redirect(site_url('admin/testimonials'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_testimonials,false,array('PKTestimonialID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                if(empty($data['record']['TestimonialDate'])){
                    unset($data['record']['TestimonialDate']);
                }
                $this->show_view_with_menu("admin/testimonials",$data);
            }else{
                redirect(site_url('admin/testimonials/add'));
            }
        }else{
            redirect(site_url('admin/testimonials'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_testimonials,$id,"PKTestimonialID");
            echo 'success';
        }else{
            redirect(site_url('admin/testimonials'));
        }
    }
}