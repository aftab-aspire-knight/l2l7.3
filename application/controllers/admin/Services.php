<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Services extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }
    function _update_setting_record(){
        $setting_record = $this->GetSettingRecord("App Categories Services Version");
        if($setting_record != false && $setting_record["Content"] != "#"){
            $setting_version = intval($setting_record["Content"]);
            $update_setting =  array(
                'Content' => ($setting_version + 1),
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => 35
            );
            $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
            unset($update_setting);
        }
    }
    function index(){
        $this->show_view_with_menu("admin/services");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_services,'ws_services.PKServiceID as ID,ws_categories.Title as CategoryTitle,ws_services.Title,ws_services.DesktopImageName,ws_services.MobileImageName,ws_services.Price,ws_services.PreferencesShow,ws_services.Position,ws_services.Status','services','Service');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/services/insert');
        $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories,"R",'PKCategoryID as ID,Title',false,false,false,false,'Title','asc');
        $this->show_view_with_menu("admin/services",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            //$service_exist = $this->model_database->GetRecord($this->tbl_services,"PKServiceID",array('FKCategoryID'=>$postData['category'],'Title'=>$postData['title']));
            //if($service_exist !== false){
               // echo "error||a||title||Title already exist in our records";
               // return;
            //}
            if(isset($_FILES['desktop_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_service'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('desktop_file')) {
                    echo "error||a||desktop_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['desktop_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_service_thumb'),150,120);
                }
            }
            if(isset($_FILES['mobile_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_service'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('mobile_file')) {
                    echo "error||a||mobile_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['mobile_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_service_thumb'),150,120);
                }
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $price = floatval(str_replace(",","",$postData['price']));
            $insert_service =  array(
                'FKCategoryID' => $postData['category'],
                'Title' =>$postData['title'],
                'Position' => $postData['position'],
                'DesktopImageName' => $postData['desktop_pic_path'],
                'MobileImageName' => $postData['mobile_pic_path'],
                'Price' => $price,
                'Content' => $postData['content'],
                'IsPackage' => $postData['is_package'],
                'PreferencesShow' => $postData['preference_show'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $service_id = $this->model_database->InsertRecord($this->tbl_services,$insert_service);
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Service Add Successfully");
            echo "success||t||" . site_url('admin/services');
        }else{
            redirect(site_url('admin/services'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_services,false,array('PKServiceID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/services/update');
                $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories,"R",'PKCategoryID as ID,Title',false,false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/services",$data);
            }else{
                redirect(site_url('admin/services/add'));
            }
        }else{
            redirect(site_url('admin/services'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $service_id = $postData['service_id'];
           // $service_exist = $this->model_database->GetRecord($this->tbl_services,"PKServiceID",array('FKCategoryID'=>$postData['category'],'PKServiceID !='=>$service_id,'Title'=>$postData['title']));
            //if($service_exist !== false){
              //  echo "error||a||title||Title already exist in our records";
               // return;
            //}
            if(isset($_FILES['desktop_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_service'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('desktop_file')) {
                    echo "error||a||desktop_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['desktop_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_service_thumb'),150,120);
                }
            }
            if(isset($_FILES['mobile_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_service'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('mobile_file')) {
                    echo "error||a||mobile_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['mobile_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_service_thumb'),150,120);
                }
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $price = floatval(str_replace(",","",$postData['price']));
            $update_service =  array(
                'FKCategoryID' => $postData['category'],
                'Title' =>$postData['title'],
                'Position' => $postData['position'],
                'DesktopImageName' => $postData['desktop_pic_path'],
                'MobileImageName' => $postData['mobile_pic_path'],
                'Price' => $price,
                'Content' => $postData['content'],
                'IsPackage' => $postData['is_package'],
                'PreferencesShow' => $postData['preference_show'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $service_id
            );

            $this->model_database->UpdateRecord($this->tbl_services,$update_service,"PKServiceID");
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Service Updated Successfully");
            echo "success||t||" . site_url('admin/services');
        }else{
            redirect(site_url('admin/services'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_services,false,array('PKServiceID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories,"R",'PKCategoryID as ID,Title',false,false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/services",$data);
            }else{
                redirect(site_url('admin/services/add'));
            }
        }else{
            redirect(site_url('admin/services'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoice_services,'FKServiceID',array('FKServiceID' => $id));
            if($invoice_record !== false){
                echo "you can not delete this service because it have some invoices in our record" ;
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_services,$id,"PKServiceID");
            $this->_update_setting_record();
            echo 'success';
        }else{
            redirect(site_url('admin/services'));
        }
    }
}