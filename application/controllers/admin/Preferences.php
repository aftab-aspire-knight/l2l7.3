<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Preferences extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }
    function _update_setting_record(){
        $setting_record = $this->GetSettingRecord("App Preferences Version");
        if($setting_record != false && $setting_record["Content"] != "#"){
            $setting_version = intval($setting_record["Content"]);
            $update_setting =  array(
                'Content' => ($setting_version + 1),
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => 36
            );
            $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
            unset($update_setting);
        }
    }
    function index(){
        $this->show_view_with_menu("admin/preferences");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_preferences,'a.PKPreferenceID as ID,a.Title,b.Title as ParentTitle,a.Price,a.Position,a.Status','preferences','Preference');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/preferences/insert');
        $data['preference_records'] = $this->model_database->GetRecords($this->tbl_preferences,"R",'PKPreferenceID as ID,Title',array('ParentPreferenceID'=>0),false,false,false,'Title','asc');
        $this->show_view_with_menu("admin/preferences",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $preference_exist = $this->model_database->GetRecord($this->tbl_preferences,"PKPreferenceID",array('ParentPreferenceID !='=>$postData['parent_preference'],'Title'=>$postData['title']));
            if($preference_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_preference =  array(
                'ParentPreferenceID' => $postData['parent_preference'],
                'Title' =>$postData['title'],
                'Position' => $postData['position'],
                'Price' => $postData['price'],
                'PriceForPackage' => $postData['price_for_package'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $preference_id = $this->model_database->InsertRecord($this->tbl_preferences,$insert_preference);
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Preference Add Successfully");
            echo "success||t||" . site_url('admin/preferences');
        }else{
            redirect(site_url('admin/preferences'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_preferences,false,array('PKPreferenceID'=>$id));
            if($data['record'] !== false){
                if($data['record']['ParentPreferenceID'] == 0){
                    $child_record = $this->model_database->GetRecord($this->tbl_preferences,"PKPreferenceID",array('ParentPreferenceID'=>$id));
                    if($child_record != false){
                        $data['record']['is_parent'] = true;
                    }
                }
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/preferences/update');
                $data['preference_records'] = $this->model_database->GetRecords($this->tbl_preferences,"R",'PKPreferenceID as ID,Title',array('ParentPreferenceID'=>0),false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/preferences",$data);
            }else{
                redirect(site_url('admin/preferences/add'));
            }
        }else{
            redirect(site_url('admin/preferences'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $preference_id = $postData['preference_id'];
            $parent_preference = isset($postData['parent_preference'])?$postData['parent_preference']:0;
            $preference_exist = $this->model_database->GetRecord($this->tbl_preferences,"PKPreferenceID",array('PKPreferenceID !='=>$preference_id,'ParentPreferenceID !='=>$parent_preference,'Title'=>$postData['title']));
            if($preference_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
           
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_preference =  array(
                'ParentPreferenceID' => $parent_preference,
                'Title' =>$postData['title'],
                'Position' => $postData['position'],
                'Price' => (isset($postData['price']) && strlen(trim($postData['price']))>0)?$postData['price']:NULL,
                'PriceForPackage' => $postData['price_for_package'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $preference_id
            );

            $this->model_database->UpdateRecord($this->tbl_preferences,$update_preference,"PKPreferenceID");
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Preference Updated Successfully");
            echo "success||t||" . site_url('admin/preferences');
        }else{
            redirect(site_url('admin/preferences'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_preferences,false,array('PKPreferenceID'=>$id));
            if($data['record'] !== false){
                if($data['record']['ParentPreferenceID'] == 0){
                    $child_record = $this->model_database->GetRecord($this->tbl_preferences,"PKPreferenceID",array('ParentPreferenceID'=>$id));
                    if($child_record != false){
                        $data['record']['is_parent'] = true;
                    }
                }
                $data['type'] = "View";
                $data['preference_records'] = $this->model_database->GetRecords($this->tbl_preferences,"R",'PKPreferenceID as ID,Title',array('ParentPreferenceID'=>0),false,false,false,'Title','asc');
                $this->show_view_with_menu("admin/preferences",$data);
            }else{
                redirect(site_url('admin/preferences/add'));
            }
        }else{
            redirect(site_url('admin/preferences'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $is_valid = true;
            $error_msg = "";
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoice_preferences,'FKPreferenceID',array('PKPreferenceID' => $id));
            if($invoice_record !== false){
                $is_valid = false;
                $error_msg =  "you can not delete this preference because it have some invoices in our record" ;
            }
            if($is_valid){
                $parent_preference_record = $this->model_database->GetRecord($this->tbl_preferences,'PKPreferenceID',array('ParentPreferenceID' => $id));
                if($parent_preference_record !== false){
                    $is_valid = false;
                    $error_msg = "you can not delete this preference because it have some child preferences" ;
                }
                if($is_valid){
                    $member__record = $this->model_database->GetRecord($this->tbl_member_preferences,'FKPreferenceID',array('PKPreferenceID' => $id));
                    if($member__record !== false){
                        $is_valid = false;
                        $error_msg =  "you can not delete this preference because it have some members preferences in our record" ;
                    }
                }
            }
            if($is_valid == true){
                $this->model_database->RemoveRecord($this->tbl_preferences,$id,"PKPreferenceID");
                $this->_update_setting_record();
                echo 'success';  
            }else{
                echo $error_msg;
            }
        }else{
            redirect(site_url('admin/preferences'));
        }
    }
}