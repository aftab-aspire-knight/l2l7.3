<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Administrators extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index() {
        $this->show_view_with_menu("admin/administrators");
    }

    function listener() {
        echo $this->model_database->GenerateTable($this->tbl_users, 'PKUserID as ID,Role,FirstName,LastName,EmailAddress,Status', 'administrators', 'Administrator', $this->GetCurrentAdminID());
    }

    function add() {
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/administrators/insert');
        $data['menus'] = $this->model_database->GetRecords($this->tbl_menus, "R", 'PKMenuID as MenuID,Name', false, false, false, false, 'Name', 'asc');
        $this->show_view_with_menu("admin/administrators", $data);
    }

    function insert() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $admin_exist = $this->model_database->GetRecord($this->tbl_users, "PKUserID", array('EmailAddress' => $postData['email_address'], 'FKFranchiseID' => 0));
            if ($admin_exist !== false) {
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';

            $insert_administrator = array(
                'Role' => $postData['role'],
                'FirstName' => $postData['first_name'],
                'LastName' => $postData['last_name'],
                'EmailAddress' => $postData['email_address'],
                'Password' => encrypt_decrypt('encrypt', $postData['u_password']),
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );

            $admin_id = $this->model_database->InsertRecord($this->tbl_users, $insert_administrator);
            $menus = json_decode($postData['assigned_output'], true);
            if (sizeof($menus) > 0) {
                $p_position = 0;
                foreach ($menus as $key => $value) {
                    $insert_parent_menu = array(
                        'ParentMenuID' => 0,
                        'FKUserID' => $admin_id,
                        'FKMenuID' => $value['id'],
                        'Position' => $p_position
                    );
                    $p_menu_id = $this->model_database->InsertRecord($this->tbl_user_menus, $insert_parent_menu);
                    if (isset($value['children']) && sizeof($value['children']) > 0) {
                        $c_position = 0;
                        foreach ($value['children'] as $key1 => $value1) {
                            $insert_child_menu = array(
                                'ParentMenuID' => $p_menu_id,
                                'FKUserID' => $admin_id,
                                'FKMenuID' => $value1['id'],
                                'Position' => $c_position
                            );
                            $this->model_database->InsertRecord($this->tbl_user_menus, $insert_child_menu);
                            $c_position += 1;
                        }
                    }
                    $p_position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage", "Administrator Add Successfully");
            echo "success||t||" . site_url('admin/administrators');
        } else {
            redirect(site_url('admin/administrators'));
        }
    }

    function edit($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_users, false, array('PKUserID' => $id, 'FKFranchiseID' => 0));
            if ($data['record'] !== false) {
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/administrators/update');
                $data['admin_assigned_menus'] = $this->model_database->GetAdminMenus($id);
                $data['menus'] = $this->model_database->GetUnassignedMenus($id);
                unset($data['record']['Password']);
                $this->show_view_with_menu("admin/administrators", $data);
            } else {
                redirect(site_url('admin/administrators/add'));
            }
        } else {
            redirect(site_url('admin/administrators'));
        }
    }

    function update() {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {
            $admin_id = $postData['admin_id'];

            $admin_exist = $this->model_database->GetRecord($this->tbl_users, "PKUserID", array('PKUserID !=' => $admin_id, 'EmailAddress' => $postData['email_address'], 'FKFranchiseID' => 0));
            if ($admin_exist !== false) {
                echo "error||a||email_address||Email Address already exist in our records";
                return;
            }
            $status = isset($postData['status']) ? 'Enabled' : 'Disabled';
            $update_administrator = array(
                'Role' => $postData['role'],
                'FirstName' => $postData['first_name'],
                'LastName' => $postData['last_name'],
                'EmailAddress' => $postData['email_address'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $admin_id
            );

            if (!empty($postData['u_password'])) {
                $update_administrator['Password'] = encrypt_decrypt('encrypt', $postData['u_password']);
            }
            $this->model_database->UpdateRecord($this->tbl_users, $update_administrator, "PKUserID");
            $this->model_database->RemoveRecord($this->tbl_user_menus, $admin_id, "FKUserID");
            $menus = json_decode($postData['assigned_output'], true);
            if (sizeof($menus) > 0) {
                $p_position = 0;
                foreach ($menus as $key => $value) {
                    $insert_parent_menu = array(
                        'ParentMenuID' => 0,
                        'FKUserID' => $admin_id,
                        'FKMenuID' => $value['id'],
                        'Position' => $p_position
                    );
                    $p_menu_id = $this->model_database->InsertRecord($this->tbl_user_menus, $insert_parent_menu);
                    if (isset($value['children']) && sizeof($value['children']) > 0) {
                        $c_position = 0;
                        foreach ($value['children'] as $key1 => $value1) {
                            $insert_child_menu = array(
                                'ParentMenuID' => $p_menu_id,
                                'FKUserID' => $admin_id,
                                'FKMenuID' => $value1['id'],
                                'Position' => $c_position
                            );
                            $this->model_database->InsertRecord($this->tbl_user_menus, $insert_child_menu);
                            $c_position += 1;
                        }
                    }
                    $p_position += 1;
                }
            }
            $this->AddSessionItem("AdminMessage", "Administrator Updated Successfully");
            echo "success||t||" . site_url('admin/administrators');
        } else {
            redirect(site_url('admin/administrators'));
        }
    }

    function view($id) {
        if ($id != "" && $id != null) {
            $data['record'] = $this->model_database->GetRecord($this->tbl_users, false, array('PKUserID' => $id, 'FKFranchiseID' => 0));
            if ($data['record'] !== false) {
                $data['type'] = "View";
                $data['admin_assigned_menus'] = $this->model_database->GetAdminMenus($id);
                $data['record']['Password'] = encrypt_decrypt('decrypt', $data['record']['Password']);
                $this->show_view_with_menu("admin/administrators", $data);
            } else {
                redirect(site_url('admin/administrators/add'));
            }
        } else {
            redirect(site_url('admin/administrators'));
        }
    }

    function delete($id) {
        if ($id != "" && $id != null) {
            if ($id == $this->GetCurrentAdminID()) {
                echo "You will not be able to delete yourself";
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_user_menus, $id, "FKUserID");
            $this->model_database->RemoveRecord($this->tbl_users, $id, "PKUserID");
            echo 'success';
        } else {
            redirect(site_url('admin/administrators'));
        }
    }

    function export() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="users-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array("User ID", 'First Name', 'Last Name', 'Email Address', 'Password'));
        $member_records = $this->model_database->GetRecords($this->tbl_users, "R", "PKUserID,FirstName,LastName,EmailAddress,Password");

        foreach ($member_records as $record) {
            fputcsv($handle, array($record['PKUserID'], $record['FirstName'], $record['LastName'], $record['EmailAddress'], encrypt_decrypt('decrypt', $record['Password'])));
        }
        fclose($handle);
        exit;
    }

}
