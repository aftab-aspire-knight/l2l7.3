<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";


class Categories extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }
    function _update_setting_record(){
        $setting_record = $this->GetSettingRecord("App Categories Services Version");
        if($setting_record != false && $setting_record["Content"] != "#"){
            $setting_version = intval($setting_record["Content"]);
            $update_setting =  array(
                'Content' => ($setting_version + 1),
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => 35
            );
            $this->model_database->UpdateRecord($this->tbl_settings,$update_setting,"PKSettingID");
            unset($update_setting);
        }
    }
    function index(){
        $this->show_view_with_menu("admin/categories");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_categories,'PKCategoryID as ID,Title,MobileTitle,DesktopIconClassName,MobileImageName,MobileIcon,Position,Status','categories','Category');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/categories/insert');
        $this->show_view_with_menu("admin/categories",$data);
    }
    
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $category_exist = $this->model_database->GetRecord($this->tbl_categories,"PKCategoryID",array('Title'=>$postData['title']));
            if($category_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }

            if(isset($_FILES['mobile_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_category'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('mobile_file')) {
                    echo "error||a||mobile_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['mobile_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_category_thumb'),150,120);
                }
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_category =  array(
                'Title' =>$postData['title'],
                'MobileTitle' => $postData['mobile_title'],
                'Position' => $postData['position'],
                'DesktopIconClassName' => $postData['desktop_icon_class_name'],
                'MobileImageName' => $postData['mobile_pic_path'],
                'MobileIcon' => $postData['mobile_icon_name'],
                'PopupMessage' => $postData['popup_message'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $category_id = $this->model_database->InsertRecord($this->tbl_categories,$insert_category);
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Category Add Successfully");
            echo "success||t||" . site_url('admin/categories');
        }else{
            redirect(site_url('admin/categories'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_categories,false,array('PKCategoryID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/categories/update');
                $this->show_view_with_menu("admin/categories",$data);
            }else{
                redirect(site_url('admin/categories/add'));
            }
        }else{
            redirect(site_url('admin/categories'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $category_id = $postData['category_id'];
            $category_exist = $this->model_database->GetRecord($this->tbl_categories,"PKCategoryID",array('PKCategoryID !='=>$category_id,'Title'=>$postData['title']));
            if($category_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            if(isset($_FILES['mobile_file'])){
                $image_config =array(
                    'upload_path' => $this->config->item('dir_upload_category'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('mobile_file')) {
                    echo "error||a||mobile_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['mobile_pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_category_thumb'),150,120);
                }
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_category =  array(
                'Title' =>$postData['title'],
                'MobileTitle' => $postData['mobile_title'],
                'Position' => $postData['position'],
                'DesktopIconClassName' => $postData['desktop_icon_class_name'],
                'MobileImageName' => $postData['mobile_pic_path'],
                'MobileIcon' => $postData['mobile_icon_name'],
                'PopupMessage' => $postData['popup_message'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $category_id
            );

            $this->model_database->UpdateRecord($this->tbl_categories,$update_category,"PKCategoryID");
            $this->_update_setting_record();
            $this->AddSessionItem("AdminMessage","Category Updated Successfully");
            echo "success||t||" . site_url('admin/categories');
        }else{
            redirect(site_url('admin/categories'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_categories,false,array('PKCategoryID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $this->show_view_with_menu("admin/categories",$data);
            }else{
                redirect(site_url('admin/categories/add'));
            }
        }else{
            redirect(site_url('admin/categories'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $product_record = $this->model_database->GetRecord($this->tbl_services,'Title',array('FKCategoryID' => $id));
            if($product_record !== false){
                echo "you can not delete this category because it is link to the service (" . $product_record['Title'] . ')' ;
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_categories,$id,"PKCategoryID");
            $this->_update_setting_record();
            echo 'success';
        }else{
            redirect(site_url('admin/categories'));
        }
    }
}