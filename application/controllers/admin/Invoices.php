<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Invoices extends Base_Admin_Controller
{

    private $FKFranchiseID; /* Hassan 23-1-2018 */

    function __construct()
    {
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
        $this->FKFranchiseID = $this->GetCurrentFranchiseID(); /* Hassan 23-1-2018 */
    }

    function index()
    {
        $data['FKFranchiseID'] = $this->FKFranchiseID; /* Hassan 23-1-2018 */
        $this->show_view_with_menu("admin/invoices", $data);
    }

    function listener()
    {
        /* Hassan changes 23-01-2018 */
        $refer_url = $_SERVER['HTTP_REFERER'];
        $where_clause = false;

        if (strpos($refer_url, "?") !== false) {
            $refer_array = explode("?", $refer_url);
            if (sizeof($refer_array) > 1) {
                $refer_array_2 = explode("&", $refer_array[1]);
                if (sizeof($refer_array_2) > 1) {
                    $type = strtolower(str_replace("t=", "", $refer_array_2[0]));
                    $query = strtolower(str_replace("q=", "", $refer_array_2[1]));
                    if ($type == "order" && ($query == "pending" || $query == "completed")) {
                        $where_clause = "OrderStatus='" . ucfirst($query) . "'";
                    }
                }
            }
        }

        if ($this->FKFranchiseID != 0 && !$where_clause) {
            //AND OrderStatus!="completed"
            $where_clause .= ' ws_invoices.FKFranchiseID in (' . $this->FKFranchiseID . ') ';
        } elseif ($this->FKFranchiseID != 0 && $where_clause) {
            $where_clause .= ' AND ws_invoices.FKFranchiseID in (' . $this->FKFranchiseID . ")";
        }
        if ($this->FKFranchiseID != "0") {
            $columns_str = 'ws_invoices.PKInvoiceID as ID,ws_invoices.PKInvoiceID as CheckID,CONCAT(ws_members.FirstName, " ", ws_members.LastName) as Name,ws_invoices.InvoiceNumber,ws_franchises.Title,ws_invoices.GrandTotal,ws_invoices.PickupDate,ws_invoices.PickupTime,ws_invoices.DeliveryDate,ws_invoices.DeliveryTime';
        } else {
            $columns_str = 'ws_invoices.PKInvoiceID as ID,ws_invoices.PKInvoiceID as CheckID,ws_invoices.OrderPostFrom,ws_invoices.InvoiceNumber,ws_franchises.Title,ws_members.EmailAddress,ws_invoices.InvoiceType,Regularly ,ws_invoices.PaymentMethod,ws_invoices.PaymentStatus,ws_invoices.OrderStatus,ws_invoices.GrandTotal,ws_invoices.PickupDate,ws_invoices.PickupTime,ws_invoices.PickupNotification,ws_invoices.DeliveryDate,ws_invoices.DeliveryTime,ws_invoices.DeliveryNotification,ws_invoices.CreatedDateTime,ws_invoices.UpdatedDateTime';
        }



        echo $this->model_database->GenerateTable($this->tbl_invoices, $columns_str, 'invoices', 'Invoice', $where_clause, $this->FKFranchiseID);
        /* ends */
    }

    function add()
    {
        /* Hassan changes 23-01-2018 */
        if (isset($this->FKFranchiseID) && $this->FKFranchiseID > 0) {
            redirect(site_url('admin/invoices'));
        }
        /* ends */
        $data['type'] = "Add";
        $data['member_records'] = $this->model_database->GetRecords($this->tbl_members, "R", 'PKMemberID as ID,EmailAddress,PostalCode', false, false, false, false, "EmailAddress", "asc");
        $data['FKFranchiseID'] = $this->FKFranchiseID; /* Hassan changes 23-01-2018 */
        $this->show_view_with_menu("admin/invoices", $data);
    }

    function edit($id)
    {
        $data['FKFranchiseID'] = $this->FKFranchiseID; /* Hassan changes 23-01-2018 */
        if ($id != "" && $id != null) {
            $MemberID = 0;
            $data['record'] = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $id));
            $data['REGULARITY'] = array(0 => 'Just once', 1 => 'Weekly', 2 => 'Every two weeks');
            if ($data['record'] !== false) {
                $data['type'] = "Edit";
                $FKFranchiseID = (isset($data['record']['FKFranchiseID']) && $data['record']['FKFranchiseID'] > 0) ? $data['record']['FKFranchiseID'] : $this->FKFranchiseID;
                $data['record']['services'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $id));
                $i = 0;
                foreach ($data['record']['services'] as $s) {
                    $fservice = $this->model_database->GetRecord($this->tbl_franchise_services, false, array('FKServiceID' => $s['FKServiceID'], "FKFranchiseID" => $FKFranchiseID));
                    $data['record']['services'][$i]["Discount"] = empty($fservice["DiscountPercentage"]) ? 0 : $fservice["DiscountPercentage"];
                    $i++;
                }



                $data['record']['preferences'] = array();
                $member_preference_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID", array('FKInvoiceID' => $id));
                foreach ($member_preference_records as $key => $value) {
                    $data['record']['preferences'][] = $value['FKPreferenceID'];
                }
                $data['record']['member'] = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $data['record']['FKMemberID']));
                $data['record']['member_card_records'] = $this->model_database->GetRecords($this->tbl_member_cards, "R", false, array('FKMemberID' => $data['record']['FKMemberID']));
                $MemberID = $data['record']['FKMemberID'];
                if (sizeof($data['record']['member_card_records']) > 0) {
                    foreach ($data['record']['member_card_records'] as $key => $value) {
                        $data['record']['member_card_records'][$key]['Number'] = $value['Number'];
                        $data['record']['member_card_records'][$key]['NumberMasked'] = $data['record']['member_card_records'][$key]['Number'];
                        $data['record']['member_card_records'][$key]['Code'] = $value['Code'];
                        $data['record']['member_card_records'][$key]['CodeMasked'] = $data['record']['member_card_records'][$key]['Code'];
                    }
                }
                $data['location_records'] = $this->model_database->GetRecords($this->tbl_locations, "R", "PKLocationID,Title,PostalCode", false);
                if (sizeof($data['location_records']) > 0) {
                    foreach ($data['location_records'] as $key => $value) {
                        $locker_records = $this->model_database->GetRecords($this->tbl_lockers, "R", "Title", array("FKLocationID" => $value['PKLocationID']));
                        if (sizeof($locker_records) > 0) {
                            $data['location_records'][$key]['lockers'] = $locker_records;
                        } else {
                            unset($data['location_records'][$key]);
                        }
                    }
                }
                $data['discount_records'] = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID as ID,DiscountFor,Code,Worth,DType", "FKMemberID=" . $data['record']['FKMemberID'] . " or FKMemberID=0");
                $data['referral_records'] = $this->model_database->GetRecords($this->tbl_members, "R", "PKMemberID as ID,ReferralCode", "PKMemberID !=" . $data['record']['FKMemberID'] . " and ReferralCode != '' and ReferralCode IS NOT NULL");
                $data['franchise_records'] = $this->model_database->GetRecords($this->tbl_franchises, "R", "PKFranchiseID as ID,Title,MinimumOrderAmount,MinimumOrderAmountLater");
                //array('PKFranchiseID'=>$FKFranchiseID)
                $data['category_records'] = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID,Title,DesktopIconClassName', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');
                if (sizeof($data['category_records']) > 0) {
                    foreach ($data['category_records'] as $key => $value) {
                        //$service_records = $this->model_database->GetRecords($this->tbl_franchise_services,"R",'PKServiceID,Title,Content,Price,DesktopImageName,MobileImageName,IsPackage as Package,PreferencesShow',array('Status'=>'Enabled','FKCategoryID'=>$value['PKCategoryID']),false,false,false,'Position','asc');
                        $service_records = $this->model_database->FranchiseCustomServicesSelectItemsWithPercentage($FKFranchiseID, $value['PKCategoryID']);
                        //$service_records = $this->model_database->GetRecords($this->tbl_services,"R",'PKServiceID,Title,Content,Price,DesktopImageName,MobileImageName,IsPackage as Package,PreferencesShow',array('Status'=>'Enabled','FKCategoryID'=>$value['PKCategoryID']),false,false,false,'Position','asc');
                        if (sizeof($service_records) > 0) {
                            $data['category_records'][$key]['service_records'] = $service_records;
                        } else {
                            unset($data['category_records'][$key]);
                        }
                    }
                }
                $data['preference_records'] = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title', array('Status' => 'Enabled', 'ParentPreferenceID' => 0), false, false, false, 'Position', 'asc');
                if (sizeof($data['preference_records']) > 0) {
                    foreach ($data['preference_records'] as $key => $value) {
                        $preference_child_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID,Title,Price,PriceForPackage as PricePackage', array('Status' => 'Enabled', 'ParentPreferenceID' => $value['PKPreferenceID']), false, false, false, 'Price', 'asc');
                        if (sizeof($preference_child_records) > 0) {
                            $data['preference_records'][$key]['child_records'] = $preference_child_records;
                        } else {
                            unset($data['preference_records'][$key]);
                        }
                    }
                }
                $data['referral_code_amount'] = $this->GetReferralCodeAmount();

                $data['invoice_franchise'] = $this->model_database->GetRecords($this->tbl_franchises, "R", "PKFranchiseID as ID,Title,MinimumOrderAmount,MinimumOrderAmountLater", array("PKFranchiseID" => $FKFranchiseID));

                if ($data['invoice_franchise'] != false) {
                    $data["order_minimum_amount"] = $data['invoice_franchise'][0]['MinimumOrderAmount'];
                    $data["order_minimum_later_amount"] = $data['invoice_franchise'][0]['MinimumOrderAmountLater'];
                } else {
                    $data["order_minimum_amount"] = $this->GetOrderAmount();
                    $data["order_minimum_later_amount"] = $this->GetOrderAmountLater();
                }

                $data["admin_detail"] = $this->GetCurrentAdminDetail();
                //d($data["admin_detail"],);


                $data["stripe_jquery_api_key"] = $this->GetStripeJQueryKey();


                $this->show_view_with_menu("admin/invoices", $data);
            } else {
                redirect(site_url('admin/invoices/add'));
            }
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function pdf($id)
    {

        $invoice = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber", array("PKInvoiceID" => $id));
        $invoiceNumber = $invoice["InvoiceNumber"];

        $PDFfilename = FCPATH . "uploads/emailattachments/invoice-$invoiceNumber.pdf";
        $pdf = file_get_contents($PDFfilename);
        header('Content-Type: application/pdf');
        header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Length: ' . strlen($pdf));
        header('Content-Disposition: inline; filename="' . basename($PDFfilename) . '";');
        ob_clean();
        flush();
        echo $pdf;

        /*
          $url = base_url("uploads/emailattachments/invoice-").$invoiceNumber.".pdf";
          redirect($url, 'refresh');
          die;
         *          */
    }

    function availability()
    {
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $is_found = true;
            $post_latitude_longitude = $this->GetLatitudeLongitude($post_data['postal_code']);
            if ($post_latitude_longitude == "") {
                $is_found = false;
            } else {
                if (strlen($post_latitude_longitude) < 5) {
                    $is_found = false;
                } else {
                    $post_code = $post_data['postal_code'];
                    $franchise_id = 0;
                    //                    $search_franchise_post_code_record = $this->model_database->GetRecord($this->tbl_search_post_codes, "FKFranchiseID", array('Code' => $post_code));
                    //                    if ($search_franchise_post_code_record != false) {
                    //                        $franchise_id = $search_franchise_post_code_record['FKFranchiseID'];
                    //                    }
                    if ($franchise_id == 0) {
                        $short_post_code_array = $this->ValidatePostalCode($post_code);
                        if ($short_post_code_array['validate'] == false) {
                            $is_found = false;
                        } else {
                            $short_post_code = $short_post_code_array['prefix'];
                            $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                            if ($franchise_post_code_record != false) {
                                $franchise_id = $franchise_post_code_record['FKFranchiseID'];
                            }
                        }
                    }
                    $insert_search_post_code = array(
                        'FKFranchiseID' => $franchise_id,
                        'Code' => $post_code,
                        'IPAddress' => $this->input->ip_address(),
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
                    if ($franchise_id != 0) {
                        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, false, array('PKFranchiseID' => $franchise_id));
                        if ($franchise_record != false) {
                            $this->AddSessionItem($this->session_post_code, $post_code);
                            $this->AddSessionItem($this->session_franchise_record, $franchise_record);
                        } else {
                            $is_found = false;
                        }
                    } else {
                        $is_found = false;
                    }
                }
            }
            if ($is_found) {
                $member_array = explode("||", $post_data['member']);
                echo "success||i||" . site_url($this->config->item('front_select_item_page_url')) . '?t=add&m=' . EncodeString($member_array[0]);
            } else {
                echo "error||a||postal_code||Postal code not found in franchise records";
            }
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function updateorderstatus()
    {
        $post_data = $this->input->post(NULL, TRUE);

        if ($post_data) {
            $invoice_id_array = json_decode($post_data['ids'], true);
            foreach ($invoice_id_array as $id) {
                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array("PKInvoiceID" => $id));
                if ($invoice_record != false) {
                    $update_invoice = array(
                        'OrderStatus' => $post_data['type'],
                        'ID' => $id,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");

                    if ($post_data['type'] == "Cancel") {


                        $update_invoice_record = array(
                            'ID' => $invoice_record['PKInvoiceID'],
                            'TookanResponse' => null,
                            'TookanUpdateResponse' => null,
                            'OnfleetResponse' => null,
                        );

                        if ($this->config->item("onfleet_enabled") == true) {
                            $updateResponse = $this->country->cancelTask($invoice_record);
                            $updateResponse = "";
                            $tookan_response_result = "";
                        } else {
                        }
                        ////
                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                        if (isset($tooken_response_array['data']['job_id'])) {
                            //$this->SendInvoiceEmail($invoice_record['InvoiceNumber'], "X");
                            $apiKey = '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94';

                            $tookan_array = array(
                                'api_key' => $apiKey,
                                'job_id' => $tooken_response_array['data']['job_id'],
                            );
                            $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));

                            $this->deleteDeliveryTask($tooken_response_array);
                            $updateResponse = null;
                        }
                        ///

                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                    }
                }
            }
            $this->AddSessionItem("AdminMessage", "Invoices Updated Successfully");
            echo 'success';
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function updatenotification()
    {
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $invoice_records = json_decode($post_data['ids'], true);
            foreach ($invoice_records as $value) {
                $update_invoice = array(
                    'ID' => $value,
                    'UpdatedDateTime' => date('Y-m-d H:i:s')
                );
                if ($post_data["type"] == "Pick") {
                    $update_invoice['PickupNotification'] = "Yes";
                } else if ($post_data["type"] == "Delivery") {
                    $update_invoice['DeliveryNotification'] = "Yes";
                }
                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
            }
            $this->AddSessionItem("AdminMessage", "Invoices Updated Successfully");
            echo 'success';
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function update()
    {
        $postData = $this->input->post(NULL, TRUE);
        if ($postData) {


            $invoice_id = $postData['invoice_id'];
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,FKMemberID,FKFranchiseID,InvoiceNumber,PickupDate,PickupTime,DeliveryDate,DeliveryTime,PaymentStatus,GrandTotal,PaymentMethod,PaymentReference,PaymentToken", array('PKInvoiceID' => $invoice_id));
            $invoice_record_old = $invoice_record;
            $MemberID = 0;
            if ($invoice_record != false) {
                $this->load->helper('language');
                $this->lang->load('merchant', 'english');

                $MemberID = $invoice_record['FKMemberID'];
                $order_from = $postData["order_from"];
                $ip_address = $postData["ip_address"];
                $franchise = $postData["franchise"];
                $member_card = $postData["member_card"];

                $discount = $postData["discount"];
                $payment_method = $postData["payment_method"];
                $payment_status = $postData["payment_status"];
                $order_status = $postData["order_status"];
                $account_notes = $postData["account_notes"];
                $order_notes = $postData["order_notes"];
                $additional_instructions = $postData["additional_instructions"];
                $location = $postData["location"];
                $locker = $postData["locker"];
                $postal_code = $postData["postal_code"];
                $building_name = $postData["building_name"];
                $street_name = $postData["street_name"];
                $town = $postData["town"];
                $regularly = $postData["regularly"];
                $pick_date = date('Y-m-d', strtotime($postData['pick_date']));
                $delivery_date = date('Y-m-d', strtotime($postData['delivery_date']));



                $session_cart_sub_total = floatval($postData['session_cart_sub_total']);
                $session_cart_preference_total = floatval($postData['session_cart_preference_total']);
                $session_cart_discount_total = floatval($postData['session_cart_discount_total']);
                $session_cart_grand_total = floatval($postData['session_cart_grand_total']);
                $invoice_token = $postData["invoice_token"];
                $session_cart = $postData["session_cart"];
                $invoice_type = "After";
                $stripe_grand_total = 0;
                $invoice_payment_records = $this->model_database->GetRecords($this->tbl_invoice_payment_informations, "R", "Amount", array('FKInvoiceID' => $invoice_record["PKInvoiceID"]));

                if (sizeof($invoice_payment_records) > 0) {
                    $inv_total = 0;
                    foreach ($invoice_payment_records as $inv_payment_record) {
                        $inv_total += $inv_payment_record["Amount"];
                    }
                    $stripe_grand_total = number_format((float) $session_cart_grand_total, 2, '.', '') - number_format((float) $inv_total, 2, '.', '');
                } else {
                    $stripe_grand_total = $session_cart_grand_total;
                }

                if (!empty($session_cart) && $session_cart != "null") {
                    $invoice_type = "Items";
                }



                if ($invoice_record["PaymentMethod"] == "Stripe") {

                    $credit_card_cart_array = explode("||", $member_card);
                    $card_id = $credit_card_cart_array[0];
                    $paymentReference = $credit_card_cart_array[4];
                } else {
                    $card_id = null;
                    $paymentReference = $invoice_record["PaymentReference"];
                }

                $pick_time = $postData['pick_time_from'] . "-" . $postData['pick_time_to'];
                $delivery_time = $postData['delivery_time_from'] . "-" . $postData['delivery_time_to'];

                if ($franchise && $franchise > 0) {
                    $invoice_data = array(
                        'ID' => $invoice_id,
                        'FKCardID' => $card_id,
                        'FKFranchiseID' => $franchise,
                        'InvoiceType' => $invoice_type,
                        'PaymentMethod' => $payment_method,
                        'OrderNotes' => $order_notes,
                        'SubTotal' => $session_cart_sub_total,
                        'GrandTotal' => $session_cart_grand_total,
                        'PreferenceTotal' => $session_cart_preference_total,
                        'DiscountTotal' => $session_cart_discount_total,
                        'AdditionalInstructions' => $additional_instructions,
                        'AccountNotes' => $account_notes,
                        'IPAddress' => $ip_address,
                        'PickupDate' => $pick_date,
                        'PickupTime' => $pick_time,
                        'DeliveryDate' => $delivery_date,
                        'DeliveryTime' => $delivery_time,
                        'Regularly' => $regularly,
                        'PaymentStatus' => $payment_status,
                        'OrderStatus' => $order_status,
                        'OrderPostFrom' => $order_from,
                        'Location' => $location,
                        'PostalCode' => $postal_code,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                } else {
                    $invoice_data = array(
                        'ID' => $invoice_id,
                        'FKCardID' => $card_id,
                        'InvoiceType' => $invoice_type,
                        'PaymentMethod' => $payment_method,
                        'OrderNotes' => $order_notes,
                        'SubTotal' => $session_cart_sub_total,
                        'GrandTotal' => $session_cart_grand_total,
                        'PreferenceTotal' => $session_cart_preference_total,
                        'DiscountTotal' => $session_cart_discount_total,
                        'AdditionalInstructions' => $additional_instructions,
                        'AccountNotes' => $account_notes,
                        'IPAddress' => $ip_address,
                        'PickupDate' => $pick_date,
                        'PickupTime' => $pick_time,
                        'DeliveryDate' => $delivery_date,
                        'DeliveryTime' => $delivery_time,
                        'Regularly' => $regularly,
                        'PaymentStatus' => $payment_status,
                        'OrderStatus' => $order_status,
                        'OrderPostFrom' => $order_from,
                        //'Location' => $location,
                        'PostalCode' => $postal_code,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                }

                /*
                  if (empty($location)) {
                  $invoice_data['Locker'] = null;
                  if ($franchise && $franchise > 0) {
                  $invoice_data['BuildingName'] = $building_name;
                  $invoice_data['StreetName'] = $street_name;
                  $invoice_data['Town'] = $town;
                  }
                  } else {
                  $invoice_data['BuildingName'] = null;
                  $invoice_data['StreetName'] = null;
                  $invoice_data['Town'] = null;
                  if ($franchise && $franchise > 0) {
                  $invoice_data['Locker'] = $locker;
                  }
                  } */

                $invoice_data['BuildingName'] = $building_name;
                $invoice_data['StreetName'] = $street_name;
                $invoice_data['Town'] = $town;
                $change = false;

                if ($invoice_record_old['BuildingName'] != $building_name || $invoice_record_old['PostalCode'] != $postal_code) {

                    $change = true;

                    if ($invoice_record['HasAddress'] == "0") {
                        $location = $this->country->GetLatitudeLongitude($postal_code);
                    } else {
                        $address = $invoice_record_old['BuildingName'];
                        $location = $this->country->GetLatitudeLongitude($address . " " . $postal_code);
                    }
                    $invoice_data['Location'] = $location;
                }


                if (!empty($discount)) {
                    $discount_cart_array = explode("||", $discount);
                    if ($discount_cart_array[1] == "Discount") {
                        $invoice_data['DiscountType'] = "Discount";
                        $invoice_data['FKDiscountID'] = $discount_cart_array[0];
                    } else {
                        $invoice_data['DiscountType'] = "Referral";
                        $invoice_data['ReferralID'] = $discount_cart_array[0];
                    }
                    $invoice_data['DiscountWorth'] = $discount_cart_array[3];
                    $invoice_data['DType'] = $discount_cart_array[4];
                    $invoice_data['DiscountCode'] = $discount_cart_array[2];
                } else {
                    $invoice_data['DiscountType'] = "None";
                }


                $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $invoice_id, "FKInvoiceID");
                $this->model_database->RemoveRecord($this->tbl_invoice_services, $invoice_id, "FKInvoiceID");
                $description_tookan = "";
                if ($invoice_type == "Items") {
                    $add_preferences = false;
                    $package_count = 0;
                    $session_cart_array = json_decode($session_cart, true);
                    $k = 0;

                    $services = [];

                    foreach ($session_cart_array as $cart_data) {
                        $values = json_decode($cart_data, true);
                        $total = floatval(intval($values['qty']) * floatval($values['price']));
                        $insert_invoice_service = array(
                            'FKInvoiceID' => $invoice_id,
                            'FKServiceID' => $values['id'],
                            'Title' => urldecode($values['title']),
                            'DesktopImageName' => urldecode($values['desktop_img']),
                            'MobileImageName' => urldecode($values['mobile_img']),
                            'FKCategoryID' => $values['category_id'],
                            'IsPackage' => $values['package'],
                            'PreferencesShow' => $values['preference_show'],
                            'Quantity' => $values['qty'],
                            'Price' => $values['price'],
                            'Total' => $total
                        );

                        $description_tookan .= '
									' . ($k + 1) . ' # Service : ' . urldecode($values['title']) . '
									';
                        $description_tookan .= '# Quantity : ' . $values['qty'] . '
									';
                        $description_tookan .= '# Price : ' . $values['price'] . '
									';
                        $description_tookan .= '# Total : ' . $total . '
									
									';


                        $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                        if ($values['preference_show'] == "Yes") {
                            $add_preferences = true;
                        }
                        if ($values['package'] == "Yes") {
                            $package_count += intval($values['qty']);
                        }

                        $services[$k]["title"] = urldecode($values['title']);
                        $services[$k]["quantity"] = $values['qty'];
                        $services[$k]["price"] = $values['price'];
                        $services[$k]["total"] = $total;
                        $services[$k]["category_id"] = $values['category_id'];
                        $services[$k]["package"] = $values['package'];
                        $services[$k]["service_id"] = $values['id'];

                        $k++;
                    }
                    if ($add_preferences == true) {
                        foreach ($postData['preferences_list'] as $preference) {
                            $current_preference_array = explode("||", $preference);
                            if (sizeof($current_preference_array) > 1) {
                                $insert_invoice_preference = array(
                                    'FKInvoiceID' => $invoice_id,
                                    'FKPreferenceID' => $current_preference_array[0],
                                    'ParentTitle' => $current_preference_array[1],
                                    'Title' => $current_preference_array[2]
                                );
                                if (isset($current_preference_array[3]) && !empty($current_preference_array[3])) {
                                    $preference_price = $current_preference_array[3];
                                    $preference_total_price = $preference_price;
                                    if ($current_preference_array[4] == "Yes") {
                                        if ($package_count > 0) {
                                            $preference_total_price = ($preference_total_price * $package_count);
                                        }
                                    }
                                    $insert_invoice_preference["Price"] = $preference_price;
                                    $insert_invoice_preference["Total"] = $preference_total_price;
                                }
                                $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                            }
                        }
                    }
                }

                // $member_detail = $this->model_database->GetRecord($this->tbl_members, "Phone,StripeCustomerID", array("PKMemberID" => $invoice_record["FKMemberID"]));
                $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $invoice_record['FKMemberID']));
                $stripe_customer_id = $member_record["StripeCustomerID"];

                if ($stripe_grand_total > 0) {


                    if ($payment_method != "Cash") {



                        $this->load->library('stripe');
                        $stripeKey = $this->GetStripeAPIKey();
                        $data = [
                            'payment_method' => $paymentReference,
                            'amount' => $stripe_grand_total * 100,
                            'customer' => $stripe_customer_id,
                            'payment_method_types' => ['card'],
                            'currency' => $this->config->item("currency_code"),
                            'confirmation_method' => 'manual',
                            'confirm' => true,
                            'setup_future_usage' => 'off_session',
                            'description' => "Admin #" . $this->GetCurrentAdminID() . " Updated Invoice #" . $invoice_record["InvoiceNumber"]
                        ];

                        $stripe = new Stripe();
                        $intent = $stripe->purchase($stripeKey, $data);
                        $stripe_information = array(
                            'InvoiceNumber' => $invoice_record["InvoiceNumber"],
                            'Content' => json_encode($intent),
                            'CreatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->InsertRecord($this->tbl_payment_informations, $stripe_information);

                        if (isset($intent->id)) {
                            // mark order as complete
                            $insert_invoice_payment_information = array(
                                'FKInvoiceID' => $invoice_id,
                                'FKCardID' => $card_id,
                                'Amount' => $stripe_grand_total,
                                'PaymentReference' => $paymentReference,
                                'PaymentToken' => $intent->id,
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                            $this->AddSessionItem("AdminMessage", "Invoice Updated Successfully");

                            //echo "success||t||" . site_url('admin/invoices');
                            //die;
                        } else {
                            echo "error||msg||something wrong with card||";
                            die;
                        }

                        $invoice_update = array(
                            'OrderStatus' => $order_status,
                            'PaymentStatus' => $payment_status,
                            'PaymentMethod' => $payment_method,
                            'ID' => $invoice_id
                        );
                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_update, 'PKInvoiceID');
                    }
                } else if ($stripe_grand_total < 0) {

                    if ($payment_method != "Cash") {
                        $this->model_database->RemoveRecord($this->tbl_invoice_payment_informations, $invoice_id, "FKInvoiceID");
                        $insert_invoice_payment_information = array(
                            'FKInvoiceID' => $invoice_id,
                            'FKCardID' => $card_id,
                            'Amount' => $session_cart_grand_total,
                            'CreatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                    }
                    $this->AddSessionItem("AdminMessage", "Invoice Updated Successfully");
                    echo "success||t||stripe_grand_total < 0" . site_url('admin/invoices');
                } else {
                    $this->AddSessionItem("AdminMessage", "Invoice Updated Successfully");
                }
                if ($invoice_record['PickupDate'] != $pick_date || $invoice_record['PickupTime'] != $pick_time) {
                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 7, 'Status' => 'Enabled'));
                    if ($sms_responder_record != false) {
                        $sms_field_array = array(
                            'Invoice Number' => $invoice_record["InvoiceNumber"],
                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                            'Currency' => $this->config->item("currency_symbol"),
                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                            'Pick Date' => date('d M Y', strtotime($pick_date)),
                            'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                        );
                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                        $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_record["InvoiceNumber"], $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                    }
                }
                if ($invoice_record['DeliveryDate'] != $delivery_date || $invoice_record['DeliveryTime'] != $delivery_time) {
                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 8, 'Status' => 'Enabled'));
                    if ($sms_responder_record != false) {
                        $sms_field_array = array(
                            'Invoice Number' => $invoice_record["InvoiceNumber"],
                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                            'Currency' => $this->config->item("currency_symbol"),
                            'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                            'Delivery Date' => date('d M Y', strtotime($delivery_date)),
                            'Delivery Time' => str_replace(" pm", "", str_replace(" am", "", $delivery_time))
                        );
                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                        $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_record["InvoiceNumber"], $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                    }
                }
                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_record['InvoiceNumber']));
                $invoice_service_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_id), false, false, false, "PKInvoiceServiceID", "asc");

                $record = [];
                $record["invoice"] = $invoice_record;
                $record["services"] = $invoice_service_records;
                $record["member"] = $member_record;


                if ($invoice_record != false) {



                    if ($invoice_record_old["GrandTotal"] != $invoice_record['GrandTotal'] || $invoice_record_old['DeliveryDate'] != $delivery_date || $invoice_record_old['DeliveryTime'] != $delivery_time || $invoice_record_old['PickupDate'] != $pick_date || $invoice_record_old['PickupTime'] != $pick_time) {

                        if ($invoice_record_old['FKFranchiseID'] == $invoice_record['FKFranchiseID']) {
                            $recordEmail = $this->SendInvoiceEmail($record, "U");
                            $change = true;
                        }
                    } elseif (isset($postData["order_status"]) && trim($postData["order_status"]) == 'Cancel') {
                        $record = $this->SendInvoiceEmail($record, "X");
                        $change = true;
                    }
                    //&& !empty($invoice_record["TookanResponse"])
                    if ($change == true) {

                        $invoice_update = array(
                            'ID' => $invoice_id
                        );

                        /*if ($this->config->item("onfleet_enabled") == true) {

                            if ($invoice_record["OrderStatus"] == "Cancel") {
                                $updateResponse = $this->country->cancelTask($invoice_record);
                            } else {
                                $updateResponse = $this->country->updateAdminTask($services, $invoice_record, $member_record);
                            }

                            //$invoice_update["OnfleetResponse"] = $updateResponse;
                        } else {
                        }*/

                        $updateResponse = $this->updateTookanTask($invoice_record_old, $invoice_record, $member_record, $description_tookan);
                        //$invoice_update["TookanResponse"] = $updateResponse;
                        //$this->model_database->UpdateRecord($this->tbl_invoices, $invoice_update, 'PKInvoiceID');
                        // }
                    }
                }
                echo "success||t||" . site_url('admin/invoices');
            } else {
                echo "error||a||invoice_number||Invoice not found in our records";
            }
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function updateTookanTask($invoice_record_old, $invoice_record, $member_record, $description_tookan)
    {


        $member_address = '';


        $postal_code_type = $this->config->item("postal_code_type");

        if ($postal_code_type["title"] == "Area") {
            $member_address = $invoice_record['BuildingName'] . ', ' . $invoice_record['StreetName'] . ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];
            $complete_address = $invoice_record['BuildingName'] . ', ' . $invoice_record['StreetName'] . ', ' . ', ' . $invoice_record['Town'];
        } else {
            $member_address = $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];
            $complete_address = $invoice_record['BuildingName'] . ', ' . $invoice_record['StreetName'] . ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];
        }



        $pick_time_array = explode("-", $invoice_record['PickupTime']);
        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);


        /* ends */

        $apiKey = "0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94";

        $jobDescription = $complete_address . "  - Notes:  " . $invoice_record['OrderNotes'] . $description_tookan;

        if ($invoice_record_old['DeliveryDate'] != $invoice_record['DeliveryDate'] || $invoice_record_old['DeliveryTime'] != $invoice_record['DeliveryTime'] || $invoice_record_old['PickupDate'] != $invoice_record['PickupDate'] || $invoice_record_old['PickupTime'] != $invoice_record['PickupTime']) {

            $tookan_array = array(
                'api_key' => $apiKey,
                'order_id' => $invoice_record['InvoiceNumber'],
                'team_id' => "21339",
                'auto_assignment' => 0,
                'job_description' => $jobDescription,
                'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                'job_pickup_email' => $member_record['EmailAddress'],
                'job_pickup_address' => $member_address,
                'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                'customer_email' => $member_record['EmailAddress'],
                'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                'customer_address' => $member_address,
                'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                'has_pickup' => "1",
                'has_delivery' => "1",
                'layout_type' => "0",
                'timezone' => $this->config->item('tookan_timezone'),
                'custom_field_template' => "",
                'meta_data' => array(),
                'tracking_link' => "1",
                'geofence' => 1
            );
        } else {
            $tookan_array = array(
                'api_key' => $apiKey,
                'order_id' => $invoice_record['InvoiceNumber'],
                'team_id' => "21339",
                'auto_assignment' => 0,
                'job_description' => $jobDescription,
                'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                'job_pickup_email' => $member_record['EmailAddress'],
                'job_pickup_address' => $member_address,
                'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                'customer_email' => $member_record['EmailAddress'],
                'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                'customer_address' => $member_address,
                'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                'has_pickup' => "1",
                'has_delivery' => "1",
                'layout_type' => "0",
                'timezone' => $this->config->item('tookan_timezone'),
                'custom_field_template' => "",
                'meta_data' => array(),
                'tracking_link' => "1",
                'geofence' => 1
            );
        }

        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
        $update_invoice_record = array();
        $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
        $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];

        if (isset($tooken_response_array['data']['job_id'])) {

            /*
              if (isset($tookan_array['team_id'])) {
              //unset($tookan_array['team_id']);
              }
              if (isset($tookan_array['auto_assignment'])) {
              //  unset($tookan_array['auto_assignment']);
              }
             */

            $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];

            if (!empty($invoice_record["Location"])) {
                $location = explode(",", $invoice_record["Location"]);
                $tookan_array['latitude'] = $location[0];
                $tookan_array['longitude'] = $location[1];
            }


            $tookan_response_result = "";
            /* Hassan 11-10-18 */
            if ($invoice_record["OrderStatus"] == 'Cancel') {

                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                $this->deleteDeliveryTask($tooken_response_array);
                $update_invoice_record['TookanResponse'] = null;
            } elseif ($invoice_record['PaymentStatus'] == "Completed") {

                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                if (isset($tooken_response_array['data']['delivery_job_id'])) {
                    $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                    $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                }
            } else {
                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                if (isset($tooken_response_array['data']['delivery_job_id'])) {
                    $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                    $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                }
                // $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                // $this->deleteDeliveryTask($tooken_response_array);
                //$update_invoice_record['TookanResponse'] = null;
            }
            $update_invoice_record['TookanUpdateResponse'] = $tookan_response_result;
        } else {
            $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
            if (isset($tooken_response_array['data']['delivery_job_id'])) {
                $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
            }
        }

        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
    }

    function delete($id)
    {
        if ($this->FKFranchiseID > 0) {
            return false;
        }
        if ($id != "" && $id != null) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array("PKInvoiceID" => $id));
            if ($invoice_record != false) {



                if ($this->config->item("onfleet_enabled") == true) {
                    $updateResponse = $this->country->cancelTask($invoice_record);
                    $tookan_response_result = "";
                } else {
                    $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                    if (isset($tooken_response_array['data']['job_id'])) {
                        $tookan_array = array(
                            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                            'job_id' => $tooken_response_array['data']['job_id']
                        );
                        $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                        $this->deleteDeliveryTask($tooken_response_array);
                    }
                }
            }
            $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $id, "FKInvoiceID");
            $this->model_database->RemoveRecord($this->tbl_invoice_services, $id, "FKInvoiceID");
            $this->model_database->RemoveRecord($this->tbl_invoices, $id, "PKInvoiceID");
            echo 'success';
        } else {
            redirect(site_url('admin/invoices'));
        }
    }

    function reinstate($invoice_id)
    {

        $invoice = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));
        $member_record = $this->model_database->GetRecord($this->tbl_members, false, array('PKMemberID' => $invoice['FKMemberID']));


        //$this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Price,Quantity,Total", array('FKInvoiceID' => $invoice_id));

        $response["status"] = 201;
        $response["message"] = "";
        $response["data"] = array();

        if ($invoice["OrderStarus"] != "Cancel" && $invoice["PaymentStatus"] != "Cancel") {


            if (empty($invoice["TookanResponse"])) {

                if ($this->config->item("onfleet_enabled") == true) {
                    $response = $this->saveToOnFleet($invoice, $member_record);
                } else {
                }
                $response = $this->saveToTookaan($invoice, $member_record, "insert");
            } else {
                $response["message"] = "Already in tookan see extra tab.";
            }
        } else {
            $response["message"] = "Order and Payment Statuses must not be canceled.";
        }


        $response = json_encode($response);

        echo $response;
    }

    function saveToOnFleet($invoice, $member)
    {

        $invoice_id = $invoice["PKInvoiceID"];
        $invoice_services = $this->model_database->GetRecords($this->tbl_invoice_services, "R", false, array('FKInvoiceID' => $invoice_id));
        $params["invoice"] = $invoice;
        $params["invoice_services"] = $invoice_services;
        $updateResponse = $this->country->createTask($params, $member);
        $result = json_decode($updateResponse);

        $invoice_data["OnfleetResponse"] = $updateResponse;
        $invoice_data["ID"] = $invoice_id;
        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");

        $response_array["status"] = 200;
        $response_array["message"] = "";
        return $response_array;
    }

    function saveToTookaan($invoice_record, $member_record, $action)
    {

        $invoice_id = $invoice_record["PKInvoiceID"];
        $invoice_number = $invoice_record["InvoiceNumber"];

        //$invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('PKInvoiceID' => $invoice_id));

        $tookan_array['team_id'] = "21339";

        $tookan_array = array(
            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
            'order_id' => $invoice_number,
            'auto_assignment' => 0,
            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $member_record['CountryCode']),
            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
            'job_pickup_email' => $member_record['EmailAddress'],
            'customer_email' => $member_record['EmailAddress'],
            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $member_record['CountryCode']),
            'has_pickup' => "1",
            'has_delivery' => "1",
            'layout_type' => "0",
            'timezone' => $this->config->item('tookan_timezone'),
            'custom_field_template' => "",
            'meta_data' => array(),
            'tracking_link' => "1",
            'geofence' => 1
        );

        if ($action == "insert") {

            $tookan_url = "https://api.tookanapp.com/v2/create_task";
        } else {
            $tookan_url = "https://api.tookanapp.com/v2/edit_task";
            $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);

            if (!empty($invoice_record['TookanResponse'])) {
                if (isset($tookan_array['team_id'])) {
                    unset($tookan_array['team_id']);
                }

                if (isset($tookan_array['auto_assignment'])) {
                    // unset($tookan_array['auto_assignment']);
                }

                $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];
                $tookan_response_result = "";
            }
        }

        $addresses = $invoice_record['BuildingName'] . " " . $invoice_record['StreetName'] . " " . $invoice_record['PostalCode'] . " " . $invoice_record['Town'];

        $tookan_array["job_pickup_address"] = $addresses;
        $tookan_array["customer_address"] = $addresses;

        $pick_time_array = explode("-", $invoice_record['PickupTime']);
        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);

        $description_tookan = $addresses;
        $description_tookan .= '
							';

        if ($invoice_record['InvoiceType'] == "Items") {
            $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Price,Quantity,Total", array('FKInvoiceID' => $invoice_id));
        }

        if (!empty($invoice_record['service_records'])) {

            $k = 0;
            foreach ($invoice_record["service_records"] as $valueis) {
                $description_tookan .= '
									' . ($k + 1) . ' # Service : ' . $valueis['Title'] . '
									';
                $description_tookan .= '# Quantity : ' . $valueis['Quantity'] . '
									';
                $description_tookan .= '# Price : ' . $valueis['Price'] . '
									';
                $description_tookan .= '# Total : ' . $valueis['Total'] . '
									
									';
                $k++;
            }
        }



        $update_invoice_record = array();
        $tookan_array["job_description"] = $invoice_record['OrderNotes'] . $description_tookan;

        $tookan_array["job_pickup_datetime"] = date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00';
        $tookan_array["job_delivery_datetime"] = date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00';

        $tookan_response_result = $this->tookan_api_post_request($tookan_url, json_encode($tookan_array));
        $response_array = json_decode($tookan_response_result, true);

        if ($response_array["status"] == 200) {


            if (!empty($response_array["data"])) {
                $update_invoice_record['TookanResponse'] = $tookan_response_result;
                $update_invoice_record['TookanUpdateResponse'] = "";
                $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
                $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
            }

            if (isset($tooken_response_array['data']['delivery_job_id'])) {
                $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                $tookan_response_result = $this->tookan_api_post_request($tookan_url, json_encode($tookan_array));
                $update_invoice_record['TookanResponse'] = $tookan_response_result;
                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
            }
        }

        return $tookan_response_result;
    }

    function refund()
    {

        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $this->load->helper('language');
            //$this->lang->load('merchant', 'english');
            //$this->load->library('merchant');
            //$this->merchant->load('stripe');

            $this->load->library('stripe');
            $stripeKey = $this->GetStripeAPIKey();


            $refund_service_price = $post_data['refund_service_price'];
            $id = $post_data['id'];
            /* if($id!=16095)
              {
              echo "error||msg||" . "Test error";die;
              } */

            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array("PKInvoiceID" => $id));
            if ($invoice_record != false) {
                $InvoiceNumber = $invoice_record['InvoiceNumber'];
                $payment_informations = $this->model_database->GetRecord($this->tbl_payment_informations, false, array("InvoiceNumber" => $InvoiceNumber));
                //echo $InvoiceNumber;
                //d($payment_informations['Content'],1);
                if ($payment_informations != false) {
                    //echo $payment_informations['Content'];
                    //die;
                    $Content = json_decode($payment_informations['Content'], true);
                    if (isset($Content["id"])) {

                        $charge_id = $Content["id"];

                        $stripe = new Stripe();
                        $response = $stripe->refund($stripeKey, $charge_id, $refund_service_price);

                        if (!isset($response['error'])) {
                            $stripe_information = array(
                                'InvoiceNumber' => $invoice_record["InvoiceNumber"],
                                'Content' => json_encode($intent),
                                'CreatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->InsertRecord($this->tbl_payment_refund_informations, $stripe_information);
                            echo "success||msg||Invoice Refunded Successfully";
                            die;
                        } else {
                            echo "error||msg||" . $response["message"];
                            die;
                        }
                    } else {
                        echo "error||msg||2. Payment Data Content not found in Franchise Payment record";
                        die;
                    }
                } else {
                    echo "error||msg||1. Payment Data not found in Franchise Payment record";
                    die;
                }
            } else {
                echo "error||msg||Data not found in Franchise record";
                die;
            }
        } else {
            echo "error||msg||Post Data not found in request";
            die;
        }
    }

    function getrefund()
    {
        $post_data = $this->input->post(NULL, TRUE);
        //$post_data = $_GET;

        if ($post_data) {
            $id = $post_data['id'];


            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array("PKInvoiceID" => $id));

            if ($invoice_record != false) {
                $InvoiceNumber = $invoice_record['InvoiceNumber'];
                $PaymentToken = $invoice_record['PaymentToken'];

                $payment_refund_informations = $this->model_database->GetRecords($this->tbl_payment_refund_informations, "R", false, array("InvoiceNumber" => $InvoiceNumber), 1, false, false, "PKRefundID", "desc");

                if ($payment_refund_informations != false) {
                    $Content = isset($payment_refund_informations[0]['Content']) ? json_decode($payment_refund_informations[0]['Content']) : false;
                    if ($Content != false) {

                        $error = true;
                        if ($Content->object == "refund") {

                            $this->load->library('stripe');
                            $stripeKey = $this->GetStripeAPIKey();
                            $stripe = new Stripe();
                            $response = $stripe->getRefunds($stripeKey, $PaymentToken);


                            if (count($response->charges->data[0]["refunds"]["data"]) > 0) {
                                $error = false;

                                $refundHtml = '<table class="table table-striped">
                                                <thead class="cf">
                                                    <tr>
                                                    <th><strong>Title</strong></th>
                                                    <th><strong>Amount</strong></th>
                                                    <th><strong>Status</strong></th>
                                                    <th><strong>Date Time</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                                $refunds = $response->charges->data[0]["refunds"]["data"];

                                $amount_refunded = 0.0;
                                foreach ($refunds as $refund) {
                                    $amount = $refund->amount;
                                    $amount_status = $refund->status;
                                    $amount_created = $refund->created;

                                    $amount = $amount / 100;



                                    $refundHtml .= '<tr>'
                                        . '<td><strong>' . ucfirst("Amount") . '</strong></td>'
                                        . '<td>' . ($this->config->item('currency_sign') . $amount) . '</td>'
                                        . '<td>' . ucfirst($amount_status) . '</td>'
                                        . '<td>' . date('m-d-Y H:i:s', ($amount_created)) . '</td>'
                                        . '</tr>';
                                    $amount_refunded = $amount_refunded + $amount;
                                }
                                $refundHtml .= '<tr>'
                                    . '<td><strong>' . ucfirst("Total Refunded") . '</strong></td>'
                                    . '<td>' . ($amount_refunded) . '</td>
                                            <td>--</td>
                                            <td>--</td>
                                            </tr>';

                                $refundHtml .= '</tbody>'
                                    . '</table>';
                                // d($refunds);
                            }
                        } else {
                            $amount_charged = isset($Content->amount) ? $this->config->item('currency_sign') . ($Content->amount / 100) : 'N/A';
                            $amount_refunded = isset($Content->amount_refunded) ? $this->config->item('currency_sign') . ($Content->amount_refunded / 100) : 'N/A';
                            $amount_created = isset($Content->created) ? $Content->created : 'N/A';
                            $amount_status = isset($Content->status) ? $Content->status : 'N/A';
                            $refunds = isset($Content->refunds->data) ? $Content->refunds->data : false;

                            if ($refunds != false) {
                                $error = false;
                                $refundHtml = '<table class="table table-striped">
										<thead class="cf">
											<tr>
												<th><strong>Title</strong></th>
												<th><strong>Amount</strong></th>
												<th><strong>Status</strong></th>
												<th><strong>Date Time</strong></th>
											</tr>
										</thead>
										<tbody>';


                                $refundHtml .= '<tr>
										<td><strong>' . ucfirst("Total Amount") . '</strong></td>
										<td>' . ($amount_charged) . '</td>
										<td>' . ucfirst($amount_status) . '</td>
										<td>' . date('m-d-Y H:i:s', ($amount_created)) . '</td>
										
									</tr>';

                                foreach ($refunds as $key => $val) {
                                    $refundHtml .= '<tr>
												<td>' . ucfirst($val->object) . '</td>
												<td>' . $this->config->item('currency_sign') . ($val->amount / 100) . '</td>
												<td>' . ucfirst($val->status) . '</td>
												<td>' . date('m-d-Y H:i:s', ($val->created)) . '</td>
												
											</tr>';
                                }

                                $refundHtml .= '<tr>
												<td><strong>' . ucfirst("Total Refunded") . '</strong></td>
												<td>' . ($amount_refunded) . '</td>
												<td>--</td>
												<td>--</td>
												
											</tr>';

                                $refundHtml .= '</tbody>
									</table>';
                            }
                        }

                        if ($error == false) {

                            echo "success||msg||" . $refundHtml;
                            die;
                        } else {
                            echo "error||msg||Refund Data Content not found in Franchise Payment record";
                            die;
                        }
                    } else {
                        echo "error||msg||Payment Refund Data Content not found in Franchise Payment record";
                        die;
                    }
                } else {
                    echo "error||msg||Payment Refund Data not found in Franchise Payment record";
                    die;
                }
            } else {
                echo "error||msg||Payment Invoice Data not found in Franchise  records";
                die;
            }
        } else {
            echo "error||msg||Post Data not found in request";
            die;
        }
    }

    function deleteDeliveryTask($tookanArray)
    {

        $apiKey = '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94';
        $tookan_array = array(
            'api_key' => $apiKey,
            'job_id' => $tookanArray['data']['delivery_job_id']
        );

        $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
    }
}
