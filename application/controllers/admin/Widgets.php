<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Widgets extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $this->show_view_with_menu("admin/widgets");
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_widgets,'PKWidgetID as ID,Title,ImageName as Image','widgets','Widget');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/widgets/insert');
        $this->show_view_with_menu("admin/widgets",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            if(isset($_FILES['image_file'])){
                $image_config = array(
                    'upload_path' => $this->config->item('dir_upload_widget'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('image_file')) {
                    echo "error||a||image_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_widget_thumb'),150,120);
                }
            }

            $insert_widget =  array(
                'Title' =>$postData['title'],
                'Link' => $postData['link'],
                'Content' => $postData['content'],
                'ImageName' => $postData['pic_path'],
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $widget_id = $this->model_database->InsertRecord($this->tbl_widgets,$insert_widget);
            $this->AddSessionItem("AdminMessage","Widget Add Successfully");
            echo "success||t||" . site_url('admin/widgets');
        }else{
            redirect(site_url('admin/widgets'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_widgets,false,array('PKWidgetID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/widgets/update');
                $this->show_view_with_menu("admin/widgets",$data);
            }else{
                redirect(site_url('admin/widgets/add'));
            }
        }else{
            redirect(site_url('admin/widgets'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $widget_id = $postData['widget_id'];
            if(isset($_FILES['image_file'])){
                $image_config = array(
                    'upload_path' => $this->config->item('dir_upload_widget'),
                    'allowed_types' => 'gif|GIF|jpg|JPG||png|PNG|bmp|BMP|jpeg|JPEG'
                );
                $this->load->library('upload', $image_config);
                if (!$this->upload->do_upload('image_file')) {
                    echo "error||a||image_file||" . $this->upload->display_errors();
                    return;
                } else {
                    $result = array('upload_data' => $this->upload->data());
                    $postData['pic_path'] = $result['upload_data']['file_name'];
                    $this->create_image_thumb($result,$this->config->item('dir_upload_widget_thumb'),150,120);
                }
            }
            $update_widget =  array(
                'Title' =>$postData['title'],
                'Link' => $postData['link'],
                'Content' => $postData['content'],
                'ImageName' => $postData['pic_path'],
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $widget_id
            );

            $this->model_database->UpdateRecord($this->tbl_widgets,$update_widget,"PKWidgetID");
            $this->AddSessionItem("AdminMessage","Widget Updated Successfully");
            echo "success||t||" . site_url('admin/widgets');
        }else{
            redirect(site_url('admin/widgets'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_widgets,false,array('PKWidgetID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $this->show_view_with_menu("admin/widgets",$data);
            }else{
                redirect(site_url('admin/widgets/add'));
            }
        }else{
            redirect(site_url('admin/widgets'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $widget_exist = $this->model_database->GetRecord($this->tbl_widget_navigations,'PKNavigationID',array('FKWidgetID' => $id));
            if($widget_exist !== false){
                echo "you can not delete this widget because it is link to section" ;
                return;
            }
            $this->model_database->RemoveRecord($this->tbl_widgets,$id,"PKWidgetID");
            echo 'success';
        }else{
            redirect(site_url('admin/widgets'));
        }
    }
}