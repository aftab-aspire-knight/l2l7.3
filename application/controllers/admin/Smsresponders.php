<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class SmsResponders extends Base_Admin_Controller{

    function __construct(){
        parent::__construct();
        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index(){
        $data['email_tag_records'] = $this->model_database->GetRecords($this->tbl_tags,"R","Title,Tag",false,false,false,false,"Title",'asc');
        $this->show_view_with_menu("admin/sms_responders",$data);
    }

    function listener(){
        echo $this->model_database->GenerateTable($this->tbl_sms_responders,'PKResponderID as ID,Title,Status','smsresponders','Sms Responder');
    }
    function add(){
        $data['type'] = "Add";
        $data['form_action'] = site_url('admin/smsresponders/insert');
        $data['email_tag_records'] = $this->model_database->GetRecords($this->tbl_tags,"R","Title,Tag",false,false,false,false,"Title",'asc');
        $this->show_view_with_menu("admin/sms_responders",$data);
    }
    function insert(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            
            $title_exist = $this->model_database->GetRecord($this->tbl_sms_responders,"PKResponderID",array('Title'=>$postData['title']));
            if($title_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }
            $status = isset($postData['status'])?'Enabled':'Disabled';
            $insert_sms_responder =  array(
                'Title' =>$postData['title'],
                'Content' => $postData['content'],
                'Status' => $status,
                'CreatedBy' => $this->GetCurrentAdminID(),
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $responder_id = $this->model_database->InsertRecord($this->tbl_sms_responders,$insert_sms_responder);
            $this->AddSessionItem("AdminMessage","Sms Responder Add Successfully");
            echo "success||t||" . site_url('admin/smsresponders');
        }else{
            redirect(site_url('admin/smsresponders'));
        }
    }

    function edit($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_sms_responders,false,array('PKResponderID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "Edit";
                $data['form_action'] = site_url('admin/smsresponders/update');
                $data['email_tag_records'] = $this->model_database->GetRecords($this->tbl_tags,"R","Title,Tag",false,false,false,false,"Title",'asc');
                $this->show_view_with_menu("admin/sms_responders",$data);
            }else{
                redirect(site_url('admin/smsresponders/add'));
            }
        }else{
            redirect(site_url('admin/smsresponders'));
        }
    }

    function update(){
        $postData = $this->input->post(NULL,TRUE);
        if($postData){
            $responder_id = $postData['responder_id'];
            
            $title_exist = $this->model_database->GetRecord($this->tbl_sms_responders,"PKResponderID",array('Title'=>$postData['title'],'PKResponderID !=' => $responder_id));
            if($title_exist !== false){
                echo "error||a||title||Title already exist in our records";
                return;
            }

            $status = isset($postData['status'])?'Enabled':'Disabled';
            $update_sms_responder =  array(
                'Title' =>$postData['title'],
                'Content' => $postData['content'],
                'Status' => $status,
                'UpdatedBy' => $this->GetCurrentAdminID(),
                'UpdatedDateTime' => date('Y-m-d H:i:s'),
                'ID' => $responder_id
            );

            $this->model_database->UpdateRecord($this->tbl_sms_responders,$update_sms_responder,"PKResponderID");
            $this->AddSessionItem("AdminMessage","Sms Responder Updated Successfully");
            echo "success||t||" . site_url('admin/smsresponders');
        }else{
            redirect(site_url('admin/smsresponders'));
        }
    }

    function view($id){
        if($id != "" && $id != null){
            $data['record'] = $this->model_database->GetRecord($this->tbl_sms_responders,false,array('PKResponderID'=>$id));
            if($data['record'] !== false){
                $data['type'] = "View";
                $data['email_tag_records'] = $this->model_database->GetRecords($this->tbl_tags,"R","Title,Tag",false,false,false,false,"Title",'asc');
                $this->show_view_with_menu("admin/sms_responders",$data);
            }else{
                redirect(site_url('admin/smsresponders/add'));
            }
        }else{
            redirect(site_url('admin/smsresponders'));
        }
    }

    function delete($id){
        if($id != "" && $id != null){
            $this->model_database->RemoveRecord($this->tbl_sms_responders,$id,"PKResponderID");
            echo 'success';
        }else{
            redirect(site_url('admin/smsresponders'));
        }
    }
}