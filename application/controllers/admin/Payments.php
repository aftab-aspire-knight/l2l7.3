<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once BASEPATH . "../application/core/Base_Admin_Controller.php";

class Payments extends Base_Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->IsAdminLoginRedirect();
        $this->IsCurrentAdminAccess($this->uri->segments[2]);
    }

    function index() {
        $data['franchise_records'] = $this->model_database->GetRecords($this->tbl_franchises, "R", "PKFranchiseID as ID,Title");
        $data['type'] = 'Add';
        $this->show_view_with_menu("admin/owner_payments", $data);
    }

    function listener() {

        $refer_url = $_SERVER['HTTP_REFERER'];
        $where_clause = false;

        if (strpos($refer_url, "?") !== false) {
            $refer_array = explode("?", $refer_url);
            if (sizeof($refer_array) > 1) {
                $refer_array_2 = explode("&", $refer_array[1]);
                if (sizeof($refer_array_2) > 1) {
                    $type = strtolower(str_replace("t=", "", $refer_array_2[0]));
                    $query = strtolower(str_replace("q=", "", $refer_array_2[1]));
                    if ($type == "order" && ($query == "pending" || $query == "completed")) {
                        $where_clause = "OrderStatus='" . ucfirst($query) . "'";
                    }
                }
            }
        }
        $post_data = $this->input->post(NULL, TRUE);
        $FKFranchiseID = isset($post_data['franchise_id']) ? $post_data['franchise_id'] : 0;
        $start_date = isset($post_data['start_date']) ? $post_data['start_date'] : '';
        $end_date = isset($post_data['end_date']) ? $post_data['end_date'] : '';
        $franchise_paid = isset($post_data['franchise_paid']) ? $post_data['franchise_paid'] : 0;
        $franchise_not_paid = isset($post_data['franchise_not_paid']) ? $post_data['franchise_not_paid'] : 0;

        $date_type = isset($post_data['date_type']) ? $post_data['date_type'] : 'CreatedDateTime';
        //d($post_data,1);

        $str_paid = '';
        if ($franchise_paid > 0) {
            $str_paid = 'AND ws_invoices.OwnerPaymentStatus = "Yes" ';
        }
        if ($franchise_not_paid > 0) {
            $str_paid = 'AND ws_invoices.OwnerPaymentStatus = "No" ';
        }
        if ($franchise_not_paid > 0 && $franchise_paid > 0) {
            $str_paid = 'AND (ws_invoices.OwnerPaymentStatus = "No" OR ws_invoices.OwnerPaymentStatus = "Yes" ) ';
        }
        $str_date = '';
        if ($start_date != '') {
            $str_date = ' AND ws_invoices.' . $date_type . ' >= STR_TO_DATE(' . $start_date . ') ';
        }
        if ($end_date != '') {
            $str_date = ' AND ws_invoices.' . $date_type . ' <= STR_TO_DATE(' . $end_date . ') ';
        }

        if ($start_date != '' && $end_date != '') {
            $str_date = ' AND ws_invoices.' . $date_type . ' BETWEEN STR_TO_DATE("' . $start_date . '", "%Y-%m-%d") AND STR_TO_DATE("' . $end_date . '", "%Y-%m-%d") ';
        }


        if ($FKFranchiseID > 0 && !$where_clause) {
            $where_clause .= ' ws_invoices.FKFranchiseID=' . $FKFranchiseID . ' ' . $str_paid . ' ' . $str_date;
        } else {
            echo json_encode('');
            die;
        }
        //,ws_invoices.CreatedDateTime,ws_invoices.UpdatedDateTime
        if ($FKFranchiseID > 0) {
            $columns_str = 'ws_invoices.PKInvoiceID as CheckID,ws_invoices.InvoiceNumber,CONCAT(ws_members.FirstName,ws_members.LastName) AS EmailAddress,ws_invoices.OwnerPaymentStatus,ws_invoices.PaymentStatus,ws_invoices.OrderStatus,ws_invoices.GrandTotal,ws_invoices.PickupDate,ws_invoices.PickupTime';
            //$columns_str='ws_invoices.PKInvoiceID as ID,ws_invoices.PKInvoiceID as CheckID,ws_invoices.OrderPostFrom,ws_invoices.InvoiceNumber,ws_franchises.Title,CONCAT(ws_members.FirstName,ws_members.LastName) AS EmailAddress,ws_invoices.OwnerPaymentStatus,ws_invoices.PaymentMethod,ws_invoices.PaymentStatus,ws_invoices.OrderStatus,ws_invoices.GrandTotal,ws_invoices.PickupDate,ws_invoices.PickupTime,ws_invoices.PickupNotification,ws_invoices.DeliveryDate,ws_invoices.DeliveryTime,ws_invoices.DeliveryNotification,ws_invoices.CreatedDateTime,ws_invoices.UpdatedDateTime';
        } else {
            $columns_str = 'ws_invoices.PKInvoiceID as CheckID,ws_invoices.OrderPostFrom,ws_invoices.InvoiceNumber,ws_franchises.Title,ws_members.EmailAddress,ws_invoices.InvoiceType,regularly ,ws_invoices.PaymentMethod,ws_invoices.PaymentStatus,ws_invoices.OrderStatus,ws_invoices.GrandTotal,ws_invoices.PickupDate,ws_invoices.PickupTime,ws_invoices.PickupNotification,ws_invoices.DeliveryDate,ws_invoices.DeliveryTime,ws_invoices.DeliveryNotification';
        }

        // echo $where_clause;


        echo $this->model_database->GenerateTable($this->tbl_invoices, $columns_str, 'invoices', 'PaymentInvoice', $where_clause, $this->FKFranchiseID);
    }

    function invoices() {
        //  d($_POST, 1);
    }

    function updateownerpaymentstatus() {
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $invoice_id_array = json_decode($post_data['ids'], true);
            foreach ($invoice_id_array as $id) {
                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,TookanResponse", array("PKInvoiceID" => $id));
                if ($invoice_record != false) {
                    $update_invoice = array(
                        'OwnerPaymentStatus' => $post_data['type'],
                        'ID' => $id,
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice, "PKInvoiceID");
                }
            }
            echo 'success';
        }
    }

    function exportfranchiseinvoicesreport() {


        $where_clause = '';
        $FKFranchiseID = isset($_REQUEST['franchise_id']) ? $_REQUEST['franchise_id'] : 0;
        $start_date = isset($_REQUEST['start_date']) ? $_REQUEST['start_date'] : '';
        $end_date = isset($_REQUEST['end_date']) ? $_REQUEST['end_date'] : '';
        $franchise_paid = isset($_REQUEST['franchise_paid']) ? $_REQUEST['franchise_paid'] : 0;
        $franchise_not_paid = isset($_REQUEST['franchise_not_paid']) ? $_REQUEST['franchise_not_paid'] : 0;
        $date_type = isset($_REQUEST['date_type']) ? $_REQUEST['date_type'] : 'CreatedDateTime';

        $str_paid = '';
        $str_namepaid = '';
        if ($franchise_paid > 0) {
            $str_paid = 'AND i.OwnerPaymentStatus = "Yes" ';
            $str_namepaid = '-paid';
        }
        if ($franchise_not_paid > 0) {
            $str_paid = 'AND i.OwnerPaymentStatus = "No" ';
            $str_namepaid = '-non-paid';
        }
        if ($franchise_not_paid > 0 && $franchise_paid > 0) {
            $str_paid = 'AND (i.OwnerPaymentStatus = "No" OR i.OwnerPaymentStatus = "Yes" ) ';
            $str_namepaid = '-all';
        }


        $str_date = '';
        if ($start_date != '') {
            $str_date = ' AND i.' . $date_type . ' >= STR_TO_DATE(' . $start_date . ') ';
        }
        if ($end_date != '') {
            $str_date = ' AND i.' . $date_type . ' <= STR_TO_DATE(' . $end_date . ') ';
        }

        if ($start_date != '' && $end_date != '') {
            $str_date = ' AND i.' . $date_type . ' BETWEEN STR_TO_DATE("' . $start_date . '", "%Y-%m-%d") AND STR_TO_DATE("' . $end_date . '", "%Y-%m-%d")  ';
        }
//and DiscountCode is not null

        if ($FKFranchiseID > 0) {
            $where_clause .= ' i.FKFranchiseID=' . $FKFranchiseID . ' ' . $str_paid . ' ' . $str_date;
        } else {
            echo json_encode('');
            die;
        }



        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, false, array("PKFranchiseID" => $FKFranchiseID));
        if (isset($franchise_record) && $franchise_record['Title'] && $franchise_record['Title'] != '') {
            $title_franchise = $franchise_record['Title'];
        } else {
            $title_franchise = '';
        }
        $invoices = $this->model_database->GetRecordsInvoices($this->tbl_invoices, $where_clause);
        
        
      //  d($invoices,1);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="export-franchise-invoices' . $str_namepaid . '-report-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array(
            '       ', '          ', $title_franchise
        ));
        fputcsv($handle, array(
            'InvoiceNumber', 'Customer Name', 'Payment Status', 'Order Status', 'Pickup Date Time', 'Gross Amount', 'Discount Code', 'Amount'
        ));

        if (isset($invoices) && !empty($invoices)) {
            foreach ($invoices as $key => $val) {
                fputcsv($handle, array(
                    $val['InvoiceNumber'], $val['EmailAddress'], $val['PaymentStatus'], $val['OrderStatus'], $val['PickupDateTime'], $val['GrossTotal'], $val['DiscountCode'], $val['GrandTotal']
                ));
            }
        }



        fclose($handle);
        exit;
    }

}
