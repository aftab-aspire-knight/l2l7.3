<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apiv7 extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->is_test = false;
        $this->load->library('stripe');
        $this->load->library('loqate');
    }

    function index() {
        redirect(site_url());
    }

    private function _update_device_information($device_id, $member_id = false) {
        $update_device = array(
            'ID' => $device_id,
            'ActiveDateTime' => date('Y-m-d h:i:s')
        );
        if ($member_id) {
            $update_device["FKMemberID"] = $member_id;
        }
        $this->model_database->UpdateRecord($this->tbl_devices_new, $update_device, 'PKDeviceID');
    }

    private function _franchise_record($id) {
        $franchise_record = $this->model_database->GetRecord($this->tbl_franchises, "PKFranchiseID as ID,PickupLimit,DeliveryLimit,OpeningTime,ClosingTime,AllowSameTime,DeliveryOption,MinimumOrderAmount,MinimumOrderAmountLater,DeliveryDifferenceHour,PickupDifferenceHour,GapTime", array("PKFranchiseID" => $id, 'Status' => 'Enabled'));
        return $franchise_record;
    }

    private function _member_invoice_records($member_id, $order_status) {
        if ($order_status == "All") {
            $response_array = array();
            $invoice_pending_array = array();
            $invoice_processed_array = array();
            $invoice_canceled_array = array();
            $invoice_completed_array = array();
            $invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,GrandTotal,OrderStatus,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('FKMemberID' => $member_id), false, false, false, "PKInvoiceID");
        } else {
            $invoice_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,GrandTotal,OrderStatus,PickupDate,PickupTime,DeliveryDate,DeliveryTime", array('FKMemberID' => $member_id, 'OrderStatus' => $order_status), false, false, false, "PKInvoiceID");
        }
        if (sizeof($invoice_records) > 0) {
            foreach ($invoice_records as $key => $value) {
                $invoice_records[$key]['CurrencyAmount'] = $this->config->item("currency_symbol") . $value['GrandTotal'];
                unset($invoice_records[$key]['GrandTotal']);
                if (!empty($value['PickupDate']) && !empty($value['PickupDate'])) {
                    $invoice_records[$key]['PickupDateTimeFormatted'] = date('d-M, Y', strtotime($value['PickupDate'])) . ' - ' . $value['PickupTime'];
                } else {
                    $invoice_records[$key]['PickupDateTimeFormatted'] = "-";
                }
                unset($invoice_records[$key]['PickupDate']);
                unset($invoice_records[$key]['PickupTime']);
                if (!empty($value['DeliveryDate']) && !empty($value['DeliveryTime'])) {
                    $invoice_records[$key]['DeliveryDateTimeFormatted'] = date('d-M, Y', strtotime($value['DeliveryDate'])) . ' - ' . $value['DeliveryTime'];
                } else {
                    $invoice_records[$key]['DeliveryDateTimeFormatted'] = "-";
                }
                unset($invoice_records[$key]['DeliveryDate']);
                unset($invoice_records[$key]['DeliveryTime']);
                if ($order_status == "All") {
                    if ($value["OrderStatus"] == "Pending") {
                        $invoice_pending_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Processed") {
                        $invoice_processed_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Cancel") {
                        $invoice_canceled_array[] = $invoice_records[$key];
                    } else if ($value["OrderStatus"] == "Completed") {
                        $invoice_completed_array[] = $invoice_records[$key];
                    }
                }
            }
        }
        if ($order_status == "All") {
            $response_array['pending_invoices'] = $invoice_pending_array;
            $response_array['processed_invoices'] = $invoice_processed_array;
            $response_array['canceled_invoices'] = $invoice_canceled_array;
            $response_array['completed_invoices'] = $invoice_completed_array;
            return $response_array;
        } else {
            return $invoice_records;
        }
    }

    private function _member_loyalty_records($member_id) {
        $response_array = array();
        $loyalty_active_records = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID as ID,Code,Worth,StartDate,ExpireDate,DType", "Status='Active' and FKMemberID=" . $member_id . " and DiscountFor='Loyalty'", false, false, false, "ID");
        if (sizeof($loyalty_active_records) > 0) {
            foreach ($loyalty_active_records as $key => $value) {
                $is_valid = true;
                if (!empty($value['StartDate'])) {
                    if (strtotime($value['StartDate']) < strtotime(date('Y-m-d'))) {
                        $is_valid = false;
                    }
                }
                if (!empty($value['ExpireDate'])) {
                    if (strtotime($value['ExpireDate']) > strtotime(date('Y-m-d'))) {
                        $update_discount = array(
                            'ID' => $value['ID'],
                            'Status' => 'Expire',
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                        $is_valid = false;
                    }
                }
                if ($is_valid) {
                    if ($value['DType'] == "Percentage") {
                        $loyalty_active_records[$key]['WorthValue'] = $value['Worth'] . '%';
                    } else {
                        $loyalty_active_records[$key]['WorthValue'] = '£' . $value['Worth'];
                    }
                    unset($loyalty_active_records[$key]['Worth']);
                    unset($loyalty_active_records[$key]['StartDate']);
                    unset($loyalty_active_records[$key]['ExpireDate']);
                    unset($loyalty_active_records[$key]['DType']);
                } else {
                    unset($loyalty_active_records[$key]);
                }
            }
        }
        $response_array["loyalty_active"] = $loyalty_active_records;
        $loyalty_history_records = $this->model_database->GetRecords($this->tbl_loyalty_points, "R", "PKLoyaltyID as ID,InvoiceNumber,GrandTotal,Points,CreatedDateTime", array('FKMemberID' => $member_id), false, false, false, "ID");
        if (sizeof($loyalty_history_records) > 0) {
            foreach ($loyalty_history_records as $key => $value) {
                $loyalty_history_records[$key]['CurrencyAmount'] = $this->config->item("currency_symbol") . $value['GrandTotal'];
                unset($loyalty_history_records[$key]['GrandTotal']);
                $loyalty_history_records[$key]['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($value['CreatedDateTime'])) . ' ' . date('H:i', strtotime($value['CreatedDateTime']));
                unset($loyalty_history_records[$key]['CreatedDateTime']);
            }
        }
        $response_array["loyalty_history"] = $loyalty_history_records;
        return $response_array;
    }

    private function _member_discount_records($member_id) {
        $response_array = array();
        $member_discount_active_records = $this->model_database->GetRecords($this->tbl_discounts, "R", "PKDiscountID as ID,Code,Worth,StartDate,ExpireDate,DType", "FKMemberID=" . $member_id . " and Status='Active' and (DiscountFor='Discount' or DiscountFor='Referral')", false, false, false, "ID");
        if (sizeof($member_discount_active_records) > 0) {
            foreach ($member_discount_active_records as $key => $value) {
                $is_valid = true;
                if (!empty($value['StartDate'])) {
                    if (strtotime($value['StartDate']) < strtotime(date('Y-m-d'))) {
                        $is_valid = false;
                    }
                }
                if (!empty($value['ExpireDate'])) {
                    if (strtotime($value['ExpireDate']) > strtotime(date('Y-m-d'))) {
                        $update_discount = array(
                            'ID' => $value['ID'],
                            'Status' => 'Expire',
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                        $is_valid = false;
                    }
                }
                if ($value['Worth'] == "") {
                    $is_valid = false;
                }
                if ($is_valid == true) {
                    if ($value['DType'] == "Percentage") {
                        $member_discount_active_records[$key]['WorthValue'] = $value['Worth'] . '%';
                    } else {
                        $member_discount_active_records[$key]['WorthValue'] = '£' . $value['Worth'];
                    }
                    unset($member_discount_active_records[$key]['Worth']);
                    unset($member_discount_active_records[$key]['StartDate']);
                    unset($member_discount_active_records[$key]['ExpireDate']);
                    unset($member_discount_active_records[$key]['DType']);
                } else {
                    unset($member_discount_active_records[$key]);
                }
            }
        }
        $response_array["discount_active"] = $member_discount_active_records;
        $member_discount_history_records = $this->model_database->GetRecords($this->tbl_invoices, "R", "PKInvoiceID as ID,InvoiceNumber,DiscountCode,DiscountWorth,DType,GrandTotal,CreatedDateTime", "FKMemberID=" . $member_id . " and FKDiscountID != '' and FKDiscountID !='0' and FKDiscountID is not null", false, false, false, "PKInvoiceID");
        if (sizeof($member_discount_history_records) > 0) {
            foreach ($member_discount_history_records as $key => $value) {
                $member_discount_history_records[$key]['CurrencyAmount'] = $this->config->item("currency_symbol") . $value['GrandTotal'];
                unset($member_discount_history_records[$key]['GrandTotal']);
                $member_discount_history_records[$key]['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($value['CreatedDateTime'])) . ' ' . date('H:i', strtotime($value['CreatedDateTime']));
                unset($member_discount_history_records[$key]['CreatedDateTime']);
                if ($value['DType'] == "Percentage") {
                    $member_discount_history_records[$key]['DiscountWorthValue'] = $value['DiscountWorth'] . '%';
                } else {
                    $member_discount_history_records[$key]['DiscountWorthValue'] = '£' . $value['DiscountWorth'];
                }
                unset($member_discount_history_records[$key]['DiscountWorth']);
                unset($member_discount_history_records[$key]['DType']);
            }
        }
        $response_array["discount_history"] = $member_discount_history_records;
        return $response_array;
    }

    private function _member_card_records($member_id) {
        $card_records = $this->model_database->GetRecords($this->tbl_member_cards, "R", "PKCardID as ID,Title,Name,Number,Year as ExpireYear,Month as ExpireMonth,Code", array('FKMemberID' => $member_id), false, false, false, "PKCardID");
        if (sizeof($card_records) > 0) {
            $i = 0;
            $cards = array();
            foreach ($card_records as $key => $value) {

                if (strlen($value['Number']) > 4) {
                    unset($card_records[$key]);
                } else {

                    $cards[$i]["ID"] = $value['ID'];
                    $cards[$i]["Title"] = $value['Title'];
                    $cards[$i]["Name"] = $value['Name'];
                    $cards[$i]["ExpireYear"] = $value['ExpireYear'];
                    $cards[$i]["ExpireMonth"] = $value['ExpireMonth'];
                    //$cards[$i]["Number"]=$value['Number'];
                    $cards[$i]["MaskedNumber"] = $value['Number'];
                    $cards[$i]["MaskedCode"] = $value['Code'];
                    $i++;
                }
            }
        }
        return $cards;
    }

    public function get_member_preferences($member_id) {

        $sql = "SELECT PKPreferenceID as ID,Title,Price,ParentPreferenceID,PriceForPackage FROM `ws_member_preferences` as wmp join
      ws_preferences as wp on wmp.FKPreferenceID=wp.PKPreferenceID where wmp.FKMemberID=" . $member_id . " and wp.Status='Enabled' order by wp.Position";
        $query = $this->db->query($sql);
        return $data = $query->result_array();
        //echo json_encode($data);
    }

    private function _member_preference_records($member_id) {
        return $this->model_database->GetRecords($this->tbl_member_preferences, "R", "FKPreferenceID as ID", array('FKMemberID' => $member_id));
    }

    private function _member_data_record($member_id) {
        $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID as ID,FKWebsiteID as WebsiteID,FKFranchiseID as FranchiseID,ReferralCode,FirstName,LastName,Phone,EmailAddress,BuildingName,StreetName,PostalCode,Town,AccountNotes,RegisterFrom,TotalLoyaltyPoints,UsedLoyaltyPoints,PopShow,InvoicePendingVersion,InvoiceProcessedVersion,InvoiceCancelVersion,InvoiceCompletedVersion,LoyaltyActiveVersion,LoyaltyHistoryVersion,DiscountActiveVersion,DiscountHistoryVersion,MemberCardVersion,MemberPreferenceVersion,MemberInformationVersion", array('PKMemberID' => $member_id));
        if ($member_record != false) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_record['ID']));
            if ($invoice_record != false) {
                $member_record['ReferralCodeUsed'] = "Yes";
            } else {
                $member_record['ReferralCodeUsed'] = "No";
            }
            $member_record['RemainingLoyaltyPoints'] = intval($member_record['TotalLoyaltyPoints']) - intval($member_record['UsedLoyaltyPoints']);
        }
        return $member_record;
    }

    private function _member_login_data_record($member_id, $device_id) {
        $response_array = array();
        $member_record = $this->_member_data_record($member_id);
        if ($member_record != false) {
            $response_array["result"] = "Success";
            $response_array = array_merge($response_array, $this->_member_invoice_records($member_record["ID"], "All"), $this->_member_loyalty_records($member_record["ID"]), $this->_member_discount_records($member_record["ID"]));
            $response_array['member'] = $member_record;
            $response_array['preferences'] = $this->_member_preference_records($member_record["ID"]);
            $response_array['cards'] = $this->_member_card_records($member_record["ID"]);
            $this->_update_device_information($device_id, $member_id);
        }
        return $response_array;
    }

    private function _category_records($FKFranchiseID = 0) {
        $category_records = $this->model_database->GetRecords($this->tbl_categories, "R", 'PKCategoryID as ID,MobileTitle as MTitle,MobileIcon', array('Status' => 'Enabled'), false, false, false, 'Position', 'asc');

        /* check if franchise id is available */
        $franchise_id = 0;
        if (isset($FKFranchiseID) && ($FKFranchiseID) > 0) {
            $franchise_id = $FKFranchiseID;
        }
        $categories = array();
        if (sizeof($category_records) > 0) {

            $i = 0;
            foreach ($category_records as $key => $value) {
                //$categories[$i]=array();


                $categories[$i]['ID'] = $value['ID'];
                $categories[$i]['MTitle'] = strtoupper($value['MTitle']);
                $categories[$i]['MobileIcon'] = $value['MobileIcon'];
                $categories[$i]['service_records'] = array();
                $service_records = $this->model_database->FranchiseCustomServicesSelectItemsApi($franchise_id, $value['ID']);

                if (sizeof($service_records) > 0) {
                    foreach ($service_records as $key1 => $value1) {
                        /* Calculate discount percentage */
                        if (isset($value1['DiscountPercentage']) && $value1['DiscountPercentage'] > 0) {
                            $actualPrice = $value1['Price'];
                            $DiscountPercentage = $value1['DiscountPercentage'];
                            $tmpPrice = ($DiscountPercentage / 100) * $actualPrice;
                            $finalPrice = $actualPrice - $tmpPrice;
                        } else {
                            $finalPrice = $value1['Price'];
                        }

                        $service_records[$key1]['Content'] = str_replace("\n", "", RemoveHtmlTags($value1['Content']));
                        $service_records[$key1]['TitleUpper'] = strtoupper($value1['Title']);
                        $service_records[$key1]['CurrencyAmount'] = $this->config->item("currency_symbol") . $value1['Price'];
                        /* send OfferPrice and DiscountPercentage two new values */
                        $service_records[$key1]['OfferPrice'] = $this->config->item("currency_symbol") . number_format($finalPrice, 2);
                        $service_records[$key1]['DiscountPercentage'] = (isset($value1['DiscountPercentage']) && $value1['DiscountPercentage'] > 0) ? $value1['DiscountPercentage'] : 0;
                        $desktop_image_name = "";
                        $desktop_image_name_path = "";
                        $mobile_image_name = "";
                        $mobile_image_name_path = "";

                        $base_url = base_url();
                        $base_url = str_replace("lovetolaundrydev/", "", $base_url);
                        if (!empty($value1['DesktopImage'])) {
                            if (file_exists($this->config->item("dir_upload_service") . $value1['DesktopImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value1['DesktopImage'])) {
                                $desktop_image_name = $value1['DesktopImage'];
                                $desktop_image_name_path = $base_url . $this->config->item("front_service_folder_url") . $value1['DesktopImage'];
                            }
                        }
                        if (!empty($value1['MobileImage'])) {
                            if (file_exists($this->config->item("dir_upload_service") . $value1['MobileImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value1['MobileImage'])) {
                                $mobile_image_name = $value1['MobileImage'];
                                $mobile_image_name_path = $base_url . $this->config->item("front_service_folder_url") . $value1['MobileImage'];
                            }
                        }
                        $service_records[$key1]['DesktopImage'] = $desktop_image_name;
                        $service_records[$key1]['MobileImage'] = $mobile_image_name;
                        $service_records[$key1]['DesktopImagePath'] = $desktop_image_name_path;
                        $service_records[$key1]['MobileImagePath'] = $mobile_image_name_path;
                    }
                    $categories[$i]['service_records'] = $service_records;
                    //$category_records[$key]['service_records'] = $service_records;
                } else {
                    unset($category_records[$key]);
                }
                $i++;
            }
        }
        return $categories;
    }

    private function _preferences_records() {
        $preference_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title', array('Status' => 'Enabled', 'ParentPreferenceID' => 0), false, false, false, 'Position', 'asc');
        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {
                $preference_child_records = $this->model_database->GetRecords($this->tbl_preferences, "R", 'PKPreferenceID as ID,Title,Price,PriceForPackage', array('Status' => 'Enabled', 'ParentPreferenceID' => $value['ID']), false, false, false, 'Price', 'asc');
                if (sizeof($preference_child_records) > 0) {
                    $preference_records[$key]['child_records'] = $preference_child_records;
                } else {
                    unset($preference_records[$key]);
                }
            }
        }
        return $preference_records;
    }

    private function _location_records() {
        $location_records = $this->model_database->GetRecords($this->tbl_locations, "R", "PKLocationID as ID,Title,PostalCode", array("Status" => 'Enabled'));
        if (sizeof($location_records) > 0) {
            foreach ($location_records as $key => $value) {
                $locker_records = $this->model_database->GetRecords($this->tbl_lockers, "R", "Title", array("FKLocationID" => $value['ID']));
                if (sizeof($locker_records) > 0) {
                    $location_records[$key]['lockers'] = $locker_records;
                } else {
                    unset($location_records[$key]);
                }
            }
        }
        return $location_records;
    }

    private function _setting_records($MinimumOrderAmount = 15) {
        // Minimum order amount with franchise
        $setting_records = array();
        $setting_records[] = array(
            'ID' => 11,
            'Title' => 'Minimum Order Amount Later',
            'Content' => $MinimumOrderAmount
                //'Content' => $this->GetOrderAmountLater()
        );
        $setting_records[] = array(
            'ID' => 16,
            'Title' => 'Website Email Receivers',
            'Content' => $this->GetWebsiteEmailReceivers()
        );
        $setting_records[] = array(
            'ID' => 17,
            'Title' => 'Website Name',
            'Content' => $this->GetWebsiteName()
        );
        $setting_records[] = array(
            'ID' => 24,
            'Title' => 'Google Play Store Link URL',
            'Content' => $this->GetGooglePlayLink()
        );
        $setting_records[] = array(
            'ID' => 25,
            'Title' => 'Apple Store Link URL',
            'Content' => $this->GetAppleStoreLink()
        );
        $setting_records[] = array(
            'ID' => 26,
            'Title' => 'Referral Code Amount',
            'Content' => $this->GetReferralCodeAmount()
        );
        $setting_records[] = array(
            'ID' => 29,
            'Title' => 'Loyalty Code Amount',
            'Content' => $this->GetLoyaltyCodeAmount()
        );
        $setting_records[] = array(
            'ID' => 30,
            'Title' => 'Minimum Loyalty Points',
            'Content' => $this->GetMinimumLoyaltyPoints()
        );
        $setting_records[] = array(
            'ID' => 31,
            'Title' => 'Minimum Order Amount',
            'Content' => $MinimumOrderAmount
                //'Content' => $this->GetOrderAmount()
        );
        $setting_records[] = array(
            'ID' => 38,
            'Title' => 'App Faq Page Link',
            'Content' => $this->GetSettingRecord("App Faq Page Link")['Content']
        );
        $setting_records[] = array(
            'ID' => 39,
            'Title' => 'App Term Condition Page Link',
            'Content' => $this->GetSettingRecord("App Term Condition Page Link")['Content']
        );
        return $setting_records;
    }

    public function get_credit_cards($member_id) {

        $response_array['cards'] = $this->_member_card_records($member_id);
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function get_d($id) {
        echo json_encode($this->_date_records($id)); // 200 being the HTTP response code
    }

    public function dashboard($member_id) {

        $response_array = array();
        $response_array["result"] = "Success";

        $member_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID as ID,FKWebsiteID as WebsiteID,FKFranchiseID as FranchiseID,FirstName,LastName,Phone,EmailAddress,BuildingName,StreetName,PostalCode,Town,TotalLoyaltyPoints,UsedLoyaltyPoints", array('PKMemberID' => $member_id));
        if ($member_record != false) {
            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_record['ID']));
            if ($invoice_record != false) {
                $member_record['ReferralCodeUsed'] = "Yes";
            } else {
                $member_record['ReferralCodeUsed'] = "No";
            }
            $member_record['RemainingLoyaltyPoints'] = intval($member_record['TotalLoyaltyPoints']) - intval($member_record['UsedLoyaltyPoints']);

            $member_record['Pending'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Pending', 'FKMemberID' => $member_record['ID']));
            ;
            $member_record['Processed'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Processed', 'FKMemberID' => $member_record['ID']));
            $member_record['Cancel'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Cancel', 'FKMemberID' => $member_record['ID']));
            $member_record['Completed'] = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", array('OrderStatus' => 'Completed', 'FKMemberID' => $member_record['ID']));
            $response_array["data"] = $member_record;
        }
        $response_array = array_merge($response_array);
        echo json_encode($response_array);
    }

    public function get_discounts($member_id) {

        $response_array = array();
        $response_array["result"] = "Success";
        $response_array['data'] = $this->_member_discount_records($member_id);
        echo json_encode($response_array);
    }

    public function get_loyalty($member_id) {

        $response_array = array();
        $response_array["result"] = "Success";
        $response_array['data'] = $this->_member_loyalty_records($member_id);
        echo json_encode($response_array);
    }

    public function get_invoices($member_id) {

        $response_array = array();
        $response_array["result"] = "Success";
        $type = isset($_GET["type"]) ? $_GET["type"] : "Pending";
        $response_array['data'] = $this->_member_invoice_records($member_id, $type);
        echo json_encode($response_array);
    }

    private function _date_records($id) {
        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;
        if ($franchise_record != false) {
            $franchise_date_array['pick'] = array();
            $franchise_date_array['delivery'] = array();
            $pick_array_count = 0;
            $delivery_array_count = 0;
            for ($i = 0; $i <= 100; $i++) {
                if ($pick_array_count <= 14) {
                    $date = date($this->config->item('query_date_format'), strtotime("+" . $i . ' days'));
                    $date_class = "Disable";
                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Pickup", $id) == false) {
                            $date_class = "Available";
                            $current_date = date('Y-m-d');
                            if ($current_date == $date) {
                                $current_date_after_add_3_hour = date('Y-m-d', strtotime(date('Y-m-d H:i:s', strtotime('+3 hours'))));
                                if ($current_date != $current_date_after_add_3_hour) {
                                    $date_class = "Disable";
                                }
                            }
                        }
                    }
                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('d-F', strtotime($date)) . ' ' . $this->GetFullDayName(date('w', strtotime($date))),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['pick'][] = $date_array;
                    $pick_array_count += 1;
                }
                if ($delivery_array_count <= 20 && $pick_array_count > 0) {
                    $date = date($this->config->item('query_date_format'), strtotime("+" . ($i + 1) . ' days'));
                    $date_class = "Disable";
                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Delivery", $id) == false) {
                            $date_class = "Available";
                        }
                    }
                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['delivery'][] = $date_array;
                    $delivery_array_count += 1;
                }
                if ($pick_array_count == 14 && $delivery_array_count == 20) {
                    break;
                }
            }
        }

        return $franchise_date_array;
    }

    public function get_time_records() {

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";


        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : '';
            $search_type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $pick_date = isset($_REQUEST['pick_date']) ? $_REQUEST['pick_date'] : '';
            $pick_time_hour = isset($_REQUEST['pick_time_hour']) ? $_REQUEST['pick_time_hour'] : '';

            $delivery_after_hour = 0;
            $franchise_record = $this->_franchise_record($id);

            if ($franchise_record != false && !empty($date) && !empty($device_id) && !empty($search_type) && ($search_type == "Pickup" || $search_type == "Delivery")) {

                $is_valid = true;
                $opening_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['OpeningTime']));
                $closing_franchise_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . " " . $franchise_record['ClosingTime']));
                $total_hours = ((strtotime($closing_franchise_time) - strtotime($opening_franchise_time)) / ( 60 * 60 ));
                $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $id), false, false, false, false, 'asc');
                //d($franchise_timings_record,1);
                //d($franchise_record);


                if ($search_type == "Delivery") {
                    $differenceHour = $franchise_record['DeliveryDifferenceHour'];
                } else {
                    $differenceHour = $franchise_record['PickupDifferenceHour'];
                }

                $return_response = $this->GetFranchiseDateTimeCollectionUpdated($date, $total_hours, $search_type, $franchise_record, $opening_franchise_time, $franchise_timings_record);





                if ($search_type == "Delivery") {
                    //d($return_response,1);
                    $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ ' . $differenceHour . ' hours', strtotime($pick_date . " $pick_time_hour:00"))); // $now + 3 hours

                    $p_date_hour = date($this->config->item('query_date_format'), strtotime($pick_date)) . " " . "$pick_time_hour:00:00";

                    $d_date_hour = date($this->config->item('query_date_format'), strtotime($date)) . " " . "$pick_time_hour:00:00";

                    $diff = strtotime($d_date_hour) - strtotime($p_date_hour);
                    $h = $diff / (60 * 60);

                    $pickupDay = date("l", strtotime($available_delivery_date));
                    $deliveryDay = date("l", strtotime($date));
                    if ($deliveryDay == "Monday") {

                        if ($franchise_record["DeliveryOption"] != "None") {

                            $i = 0;
                            foreach ($return_response["Times"] as $time) {

                                if ($time["Hour"] < 15) {
                                    $return_response["Times"][$i]["Class"] = "Disable";
                                }
                                $i++;
                            }
                        }
                    } else {


                        $p = strtotime($pick_date . " " . "$pick_time_hour:00:00");

                        $i = 0;
                        foreach ($return_response["Times"] as $time) {

                            $d_date_hour = strtotime($date . " " . $time["Hour"] . ":00:00");

                            $diff = $d_date_hour - $p;
                            $h = $diff / (60 * 60);
                            //$h--;
                            if ($h < $franchise_record['DeliveryDifferenceHour']) {
                                $return_response["Times"][$i]["Class"] = "Disable";
                            }

                            $i++;
                        }
                    }
                }
                //d($response_array,1);
                unset($response_array["message"]);
                $response_array["result"] = "Success";
                $response_array["data"] = $return_response["Times"];
            }
        }

        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public static function d($arr, $die = 0) {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        if ($die == 1) {
            die("end");
        }
    }

    public function get_preferences_records() {


        $response_array["preferences"] = array("version" => $this->GetAppPreferenceVersion(), "data" => $this->_preferences_records());
        $response_array["preferences"]["member_preferences"] = array();
        $total = 0.00;
        $selected = array();
        if (isset($_GET["member_id"])) {
            //$response_array["member_preferences"]=$this->get_member_preferences($_GET["member_id"]);

            $member_preferences = $this->get_member_preferences($_GET["member_id"]);


            //self::d($member_preferences,1);
            foreach ($member_preferences as $p) {

                if (is_numeric($p["Price"]) && $p["PriceForPackage"] == "Yes") {
                    $total = $total + $p["Price"];
                }
                $total = number_format($total, 2);


                $selected[$p["ParentPreferenceID"]] = $p;
            }
        }

        $response_array["preferences"]["member_preferences"] = $selected;
        $response_array["preferences"]["total"] = $total;
        echo json_encode($response_array);
    }

    public function get_load_data() {

        $base_url = str_replace("lovetolaundrydev", "", base_url());

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";

        if ($_REQUEST) {
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $versions = isset($_REQUEST['versions']) ? $_REQUEST['versions'] : '';
            /* get post code for franchise items and more info check */
            $post_code = isset($_REQUEST['post_code']) ? $_REQUEST['post_code'] : '';
            $more_info = isset($_REQUEST['more_info']) ? $_REQUEST['more_info'] : '';
            /* changes ends */
            if (!empty($type) && !empty($device_id)) {
                $this->_update_device_information($device_id);

                ////////////////postcode/////
                $MinimumOrderAmount = 15;
                $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);



                if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) >= 5) {
                    $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                    if ($short_post_code_array['validate'] != false) {
                        $short_post_code = $short_post_code_array['prefix'];
                        $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));

                        if ($franchise_post_code_record != false) {
                            $FKFranchiseID = $franchise_post_code_record['FKFranchiseID'];
                            $franchise_record = $this->_franchise_record($franchise_post_code_record['FKFranchiseID']);

                            if ($franchise_record != false) {
                                $MinimumOrderAmount = isset($franchise_record['MinimumOrderAmount']) ? $franchise_record['MinimumOrderAmount'] : 15;
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                if (!empty($more_info)) {
                                    $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", "OpeningTime,ClosingTime", array("FKFranchiseID" => $franchise_post_code_record['FKFranchiseID']), false, false, false, false, 'asc');
                                    if (isset($franchise_timings_record) && !empty($franchise_timings_record)) {
                                        $franchise_record['OtherSlots'] = $franchise_timings_record;
                                    } else {
                                        $franchise_record['OtherSlots'] = array();
                                    }
                                    $response_array["franchise_detail"] = $franchise_record;
                                    $date_records = $this->_date_records($franchise_post_code_record['FKFranchiseID']);
                                    if (sizeof($date_records) > 0) {
                                        $response_array["pick_date_data"] = $date_records["pick"];
                                        $response_array["delivery_date_data"] = $date_records["delivery"];
                                    }
                                }
                            } else {
                                $response_array["message"] = "Sorry !!! We are not in your area yet";
                            }
                        } else {
                            $response_array["message"] = "Sorry !!! We are not in your area yet";
                        }
                    } else {
                        $response_array["message"] = "Please enter valid post code";
                    }
                } else {
                    $response_array["message"] = "Please enter valid post code";
                }

                ////////////////////////
                /* if franchise exist */
                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                    if ($type == "load") {
                        $category_records = $this->_category_records($FKFranchiseID);
                        if (sizeof($category_records) > 0) {
                            unset($response_array["message"]);
                            $response_array["result"] = "Success";
                            $response_array["categories"] = array("version" => $this->GetAppCategoryServiceVersion(), "data" => $category_records);
                            $response_array["preferences"] = array("version" => $this->GetAppPreferenceVersion(), "data" => $this->_preferences_records());
                            $response_array["settings"] = array("version" => $this->GetAppSettingVersion(), "data" => $this->_setting_records($MinimumOrderAmount));
                        } else {
                            $response_array["message"] = "Love 2 Laundry data is not available. Please try again later";
                        }
                    } else if ($type == "fetch" && !empty($versions)) {
                        $version_array = explode("|", $versions);
                        if (sizeof($version_array) == 3) {
                            $category_records = $this->_category_records($FKFranchiseID);
                            if (sizeof($category_records) > 0) {
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                $category_service_version = $this->GetAppCategoryServiceVersion();
                                if ($category_service_version != $version_array[0]) {
                                    $response_array["categories"] = array("version" => $category_service_version, "data" => $category_records);
                                }
                                $preference_version = $this->GetAppPreferenceVersion();
                                if ($preference_version != $version_array[1]) {
                                    $response_array["preferences"] = array("version" => $preference_version, "data" => $this->_preferences_records());
                                }
                                $setting_version = $this->GetAppSettingVersion();
                                if ($setting_version != $version_array[2]) {
                                    $response_array["settings"] = array("version" => $setting_version, "data" => $this->_setting_records($MinimumOrderAmount));
                                }
                            } else {
                                $response_array["message"] = "Love 2 Laundry data is not available. Please try again later";
                            }
                        }
                    }
                } else {
                    $response_array["message"] = "Sorry !!! We are not in your area yet Franchise is not available";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function get_services_data() {

        $base_url = str_replace("l2llondon", "", base_url());
        $base_url = str_replace("l2l", "", base_url());
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            $versions = isset($_REQUEST['versions']) ? $_REQUEST['versions'] : '';
            /* get post code for franchise items and more info check */
            $post_code = isset($_REQUEST['post_code']) ? $_REQUEST['post_code'] : '';
            $more_info = isset($_REQUEST['more_info']) ? $_REQUEST['more_info'] : '';
            $services_ids = isset($_REQUEST['services_ids']) ? $_REQUEST['services_ids'] : '';
            /* changes ends */
            if (!empty($type) && !empty($device_id)) {
                ////////////////postcode/////
                $MinimumOrderAmount = 15;
                $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);
                if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) >= 5) {
                    $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                    if ($short_post_code_array['validate'] != false) {
                        $short_post_code = $short_post_code_array['prefix'];
                        $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                        if ($franchise_post_code_record != false) {
                            $FKFranchiseID = $franchise_post_code_record['FKFranchiseID'];
                            $franchise_record = $this->_franchise_record($franchise_post_code_record['FKFranchiseID']);

                            if ($franchise_record != false) {
                                $MinimumOrderAmount = isset($franchise_record['MinimumOrderAmount']) ? $franchise_record['MinimumOrderAmount'] : 15;
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                $response_array["franchise_detail"] = $franchise_record;
                            } else {
                                $response_array["message"] = "Sorry !!! We are not in your area yet";
                            }
                        } else {
                            $response_array["message"] = "Sorry !!! We are not in your area yet";
                        }
                    } else {
                        $response_array["message"] = "Please enter valid post code";
                    }
                } else {
                    $response_array["message"] = "Please enter valid post code";
                }

                ////////////////////////
                /* if franchise exist */
                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {

                    //$category_records = $this->_category_records($FKFranchiseID);
                    $response_array["result"] = "Success";
                    $response_array["services"] = array();
                    if ($services_ids != "") {
                        $sql = "select s.PKServiceID,wfs.`FKFranchiseID`,s.Price as ServicePrice,wfs.`FKFranchiseID`,wfs.Price as FranchisePrice,wfs.DiscountPercentage from " . $this->tbl_services . " as s left join ws_franchise_services as wfs on s.PKServiceID = wfs.FKServiceID  where s.PKServiceID IN (" . $services_ids . ") and wfs.`FKFranchiseID` = $FKFranchiseID";
                        $query = $this->db->query($sql);
                        if ($query->num_rows() > 0) {
                            $result = $query->result_array();

                            $i = 0;
                            $services = array();
                            foreach ($result as $row) {
                                $services[$i]["ServiceID"] = $row["PKServiceID"];

                                if (!empty($row["FranchisePrice"])) {
                                    $services[$i]["Price"] = $row["FranchisePrice"];
                                } else {
                                    $services[$i]["Price"] = $row["ServicePrice"];
                                }

                                $services[$i]["Discount"] = number_format(($services[$i]["Price"] * $row["DiscountPercentage"] ) / 100, 2);
                                $services[$i]["OfferPrice"] = number_format($services[$i]["Price"], 2);
                                $services[$i]["CurrencyAmount"] = $this->config->item("currency_symbol") . $services[$i]["Price"];
                                if ($row["DiscountPercentage"] > 0) {

                                    $offerPrice = $services[$i]["Price"] - $services[$i]["Discount"];
                                    $services[$i]["OfferPrice"] = number_format($offerPrice, 2);
                                    /// $services[$i]["OfferPrice"] = 55.41;
                                }

                                $i++;
                            }
                            $response_array["services"] = $services;
                        }
                    }
                } else {
                    $response_array["message"] = "Sorry !!! We are not in your area yet Franchise is not available";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function get_member_data() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            if (!empty($id) && !empty($device_id)) {
                unset($response_array["message"]);
                unset($response_array["result"]);
                $response_array = $this->_member_login_data_record($id, $device_id);
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function get_invoice_detail() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : GET";
        if ($_REQUEST) {
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
            $member_id = isset($_REQUEST['member_id']) ? $_REQUEST['member_id'] : '';
            $device_id = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : '';
            if (!empty($id) && !empty($member_id) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID as ID,FKMemberID as MemberID,InvoiceNumber,DiscountType,DiscountCode,DiscountWorth,DType,InvoiceType,Location,Locker,BuildingName,StreetName,PostalCode,Town,OrderStatus,ServicesTotal,PreferenceTotal,SubTotal,DiscountTotal,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,Regularly,CreatedDateTime,AccountNotes,AdditionalInstructions", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                //d($invoice_record,1);

                if ($invoice_record != false) {
                    $response_array["result"] = "Success";
                    if (!empty($invoice_record['BuildingName'])) {
                        $invoice_record['AddressShow'] = "Building";
                    } else {
                        $invoice_record['AddressShow'] = "Location";
                    }
                    if (empty($invoice_record['PreferenceTotal'])) {
                        $invoice_record['PreferenceTotal'] = "0.00";
                    }
                    if (empty($invoice_record['DiscountTotal'])) {
                        $invoice_record['DiscountTotal'] = "0.00";
                    }
                    $invoice_record['CurrencyAmount'] = $this->config->item("currency_symbol") . $invoice_record['GrandTotal'];
                    unset($invoice_record['GrandTotal']);
                    $invoice_record['CreatedDateTimeFormatted'] = date('d-M, Y', strtotime($invoice_record['CreatedDateTime'])) . ' ' . date('h:i a', strtotime($invoice_record['CreatedDateTime']));
                    unset($invoice_record['CreatedDateTime']);
                    if (!empty($invoice_record['PickupDate']) && !empty($invoice_record['PickupDate'])) {
                        $invoice_record['PickUpDateTimeFormatted'] = date('d M, Y', strtotime($invoice_record['PickupDate'])) . ' - ' . $invoice_record['PickupTime'];
                    } else {
                        $invoice_record['PickUpDateTimeFormatted'] = "-";
                    }

                    $pickupTime = explode("-", $invoice_record["PickupTime"]);
                    $pickupTime = $pickupTime[0];
                    $pickupDateTime = strtotime($invoice_record['PickupDate'] . " " . $pickupTime . ":00");
                    $invoice_record['PickupDate'] . " " . $pickupTime . ":00";
                    $currentDateTime = time();
                    $diff = (int) $pickupDateTime - (int) $currentDateTime;
                    $diff = $diff / 3600;

                    $invoice_record['editable'] = false;

                    if ($diff > 4) {
                        $invoice_record['editable'] = true;
                    }


                    $pd = $invoice_record['PickupDate'];
                    $pt = $invoice_record['PickupTime'];
                    unset($invoice_record['PickupDate']);
                    unset($invoice_record['PickupTime']);

                    if (!empty($invoice_record['DeliveryDate']) && !empty($invoice_record['DeliveryTime'])) {
                        $invoice_record['DeliveryDateTimeFormatted'] = date('d M, Y', strtotime($invoice_record['DeliveryDate'])) . ' - ' . $invoice_record['DeliveryTime'];
                    } else {
                        $invoice_record['DeliveryDateTimeFormatted'] = "-";
                    }

                    $dd = $invoice_record['DeliveryDate'];
                    $dt = $invoice_record['DeliveryTime'];

                    unset($invoice_record['DeliveryDate']);
                    unset($invoice_record['DeliveryTime']);

                    $invoice_record['preference_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "ParentTitle,Title", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoicePreferenceID");

                    $invoice_record['service_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID,Title,Quantity,Price,Total,MobileImageName", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoiceServiceID");
                    $response_array["data"] = $invoice_record;
                    $response_array["data"]["PickupDate"] = $pd;
                    $response_array["data"]["PickupTime"] = $pt;
                    $response_array["data"]["DeliveryDate"] = $dd;
                    $response_array["data"]["DeliveryTime"] = $pt;

                    $short_post_code_array = $this->country->ValidatePostalCode($invoice_record['PostalCode']);
                    $short_post_code = $short_post_code_array['prefix'];
                    $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                    $franchise_id = $franchise_post_code_record["FKFranchiseID"];
                    $franchise_record = $this->_franchise_record($franchise_id);
                    $response_array["data"]["franchise"] = $franchise_record;
                    $response_array["data"]["franchise_id"] = $franchise_id;
                    unset($response_array["message"]);
                } else {
                    $response_array["message"] = "Love2Laundry : Invoice not found";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_validate_app_version() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $version = isset($post_data['version']) ? $post_data['version'] : '';
            $platform = isset($post_data['platform']) ? $post_data['platform'] : '';
            if (!empty($version) && !empty($platform)) {
                /* $setting_record = $this->model_database->GetRecord($this->tbl_settings,"PKSettingID",array("Title" => $platform . " App Version","Content" => $version));
                  if($setting_record != false){
                  $response_array["result"] = "Success";
                  $response_array["message"] = "Latest Version";
                  }else{
                  $response_array["message"] = "Please upgrade to the new version";
                  } */
                $response_array["result"] = "Success";
                $response_array["message"] = "Latest Version";
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_register_device() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);

        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $model = isset($post_data['model']) ? $post_data['model'] : '';
            $platform = isset($post_data['platform']) ? $post_data['platform'] : '';
            $version = isset($post_data['version']) ? $post_data['version'] : '';
            $device_data = array(
                'ID' => $id,
                'Model' => $model,
                'Platform' => $platform,
                'Version' => $version
            );

            $device_record = $this->model_database->GetRecord($this->tbl_devices_new, "PKDeviceID", array("ID" => $id, "Model" => $model, "Platform" => $platform, "Version" => $version));
            if ($device_record != false) {
                $device_data['PKDeviceID'] = $device_record['PKDeviceID'];
                $this->_update_device_information($device_record['PKDeviceID']);
            } else {
                $device_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                $device_data['ActiveDateTime'] = date('Y-m-d h:i:s');
                $device_data['PKDeviceID'] = $this->model_database->InsertRecord($this->tbl_devices_new, $device_data);
                unset($device_data['CreatedDateTime']);
                unset($device_data['ActiveDateTime']);
            }
            unset($response_array["message"]);
            $response_array["result"] = "Success";
            $response_array["device_detail"] = $device_data;
            $response_array["settings"] = array("version" => $this->GetAppSettingVersion(), "data" => $this->_setting_records(20));
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_verify_post_code() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $more_info = isset($post_data['more_info']) ? $post_data['more_info'] : '';
            if (!empty($post_code) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $insert_search_post_code = array(
                    'FKDeviceID' => $device_id,
                    'Code' => $post_code,
                    'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
                $post_latitude_longitude = $this->country->GetLatitudeLongitude($post_code);
                if (!empty($post_latitude_longitude) && strlen($post_latitude_longitude) >= 5) {
                    $short_post_code_array = $this->country->ValidatePostalCode($post_code);
                    if ($short_post_code_array['validate'] != false) {
                        $short_post_code = $short_post_code_array['prefix'];
                        $franchise_post_code_record = $this->model_database->GetRecord($this->tbl_franchise_post_codes, "FKFranchiseID", array('Code' => $short_post_code));
                        if ($franchise_post_code_record != false) {
                            $insert_search_post_code['FKFranchiseID'] = $franchise_post_code_record['FKFranchiseID'];
                            $franchise_record = $this->_franchise_record($franchise_post_code_record['FKFranchiseID']);
                            if ($franchise_record != false) {
                                unset($response_array["message"]);
                                $response_array["result"] = "Success";
                                if (!empty($more_info)) {
                                    $response_array["franchise_detail"] = $franchise_record;
                                    $date_records = $this->_date_records($franchise_post_code_record['FKFranchiseID']);
                                    if (sizeof($date_records) > 0) {
                                        $response_array["pick_date_data"] = $date_records["pick"];
                                        $response_array["delivery_date_data"] = $date_records["delivery"];
                                    }
                                }
                            } else {
                                $response_array["message"] = "Sorry !!! We are not in your area yet";
                            }
                        } else {
                            $response_array["message"] = "Sorry !!! We are not in your area yet";
                        }
                    } else {
                        $response_array["message"] = "Please enter valid post code";
                    }
                } else {
                    $response_array["message"] = "Please enter valid post code";
                }
                $this->model_database->InsertRecord($this->tbl_search_post_codes, $insert_search_post_code);
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_login() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);

        if ($post_data) {

            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($email_address) && !empty($password) && !empty($device_id)) {
                $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,StripeCustomerID,PKMemberID", array('EmailAddress' => $email_address, 'Password' => encrypt_decrypt('encrypt', $password), 'Status' => 'Enabled'));

                if ($member_record != false) {
                    unset($response_array["message"]);
                    unset($response_array["result"]);

                    if (empty($member_record["StripeCustomerID"])) {
                        $stripeKey = $this->GetStripeAPIKey();
                        $stripe = new Stripe();
                        $customer = $stripe->saveCustomer($stripeKey, ["email" => $member_record['EmailAddress'], "name" => $member_record['FirstName'] . " " . $member_record['LastName']]);

                        $update_member = array(
                            'ID' => $member_record['PKMemberID'],
                            'StripeCustomerID' => $customer->id
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                    }
                    $response_array = $this->_member_login_data_record($member_record['PKMemberID'], $device_id);
                } else {
                    $response_array["message"] = "Incorrect Username/Password";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_forgot_password() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($email_address) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID,FirstName,LastName,EmailAddress,Phone', array('EmailAddress' => $email_address, 'Status' => 'Enabled'));
                if ($member_record != false) {
                    $random_password = $this->RandomPassword();
                    $update_member = array(
                        'ID' => $member_record['PKMemberID'],
                        'Password' => encrypt_decrypt("encrypt", $random_password)
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                        'Password' => $random_password,
                        'Email Address' => $member_record['EmailAddress'],
                        'Phone Number' => $member_record['Phone']
                    );
                    $this->emailsender->send_email_responder(3, $email_field_array);
                    $this->emailsender->send_email_responder(8, $email_field_array);
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Password send to your desired email address please check your inbox";
                } else {
                    $response_array["message"] = "Email Address not found in our records";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_register() {

        //$loqate = new Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));

        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);


        if ($post_data) {
            $first_name = isset($post_data['first_name']) ? $post_data['first_name'] : '';
            $last_name = isset($post_data['last_name']) ? $post_data['last_name'] : '';
            $phone = isset($post_data['phone']) ? $post_data['phone'] : '';
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $address1 = isset($post_data['address']) ? $post_data['address'] : '';
            $address2 = isset($post_data['address2']) ? $post_data['address2'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $is_valid = true;

            if (empty($first_name)) {
                $response_array["message"] = "Please enter your first name.";
                $is_valid = false;
            } elseif (empty($last_name)) {
                $response_array["message"] = "Please enter your last name.";
                $is_valid = false;
            } elseif (empty($email_address)) {
                $response_array["message"] = "Please enter your email address.";
                $is_valid = false;
            } elseif (empty($password)) {
                $response_array["message"] = "Please enter your password.";
                $is_valid = false;
            } elseif (empty($phone)) {
                $response_array["message"] = "Please enter your phone number.";
                $is_valid = false;
            } elseif (empty($address1)) {
                $response_array["message"] = "Please enter your address.";
                $is_valid = false;
            } elseif (!$this->validateEmail($post_data['email_address'])) {
                $response_array["message"] = "Please enter correct email address.";
                $is_valid = false;
            } elseif (empty($post_code)) {
                $response_array["message"] = "Please enter your postal code.";
                $is_valid = false;
            } elseif (empty($town)) {
                $response_array["message"] = "Please enter your town.";
                $is_valid = false;
            }

            if ($is_valid == true) {


                $postal_code_type = $this->config->item("postal_code_type");

                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('EmailAddress' => $email_address));

                if ($member_record != false) {
                    $response_array["message"] = "Email Address already exist in our records";
                    $is_valid = false;
                }

                $member_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('Phone' => $phone));
                if ($member_record != false) {
                    $response_array["message"] = "Phone Number already exist in our records";
                    $is_valid = false;
                }

                $short_post_code_array = $this->country->ValidatePostalCode($post_code);

                if ($short_post_code_array['validate'] == false) {
                    $response_array["message"] = $postal_code_type["validation_message"];
                    $is_valid = false;
                }


                if ($is_valid == true) {



                    $stripeKey = $this->GetStripeAPIKey();
                    $stripe = new Stripe();
                    $customer = $stripe->saveCustomer($stripeKey, ["email" => $email_address, "name" => $first_name . " " . $last_name]);

                    //$phone = ltrim($phone,"+");
                    //$phone = ltrim($phone,$this->config->item('country_phone_code'));
                    //$phone = ltrim($phone,0);


                    $insert_member = array(
                        'FirstName' => $first_name,
                        'LastName' => $last_name,
                        'EmailAddress' => $email_address,
                        'StripeCustomerId' => $customer->id,
                        'Password' => encrypt_decrypt('encrypt', $password),
                        'Phone' => $phone,
                        'PostalCode' => $post_code,
                        'BuildingName' => $address1,
                        'StreetName' => $address2,
                        'Town' => $town,
                        'Status' => 'Enabled',
                        'RegisterFrom' => 'Mobile',
                        'ReferralCode' => $this->random_str($first_name),
                        'PopShow' => 'No',
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $member_id = $this->model_database->InsertRecord($this->tbl_members, $insert_member);
                    unset($response_array["message"]);
                    $response_array["result"] = "Success";
                    $this->_update_device_information($device_id, $member_id);
                    $response_array['data'] = $this->_member_data_record($member_id);
                    $response_array["MemberID"] = $member_id;
                    $member_record = $response_array['data'];
                    if (isset($member_id)) {
                        /* default member preferences 9/10/2018 */
                        $MemberPreferences = $this->_member_preference_records($member_id);
                        if (empty($MemberPreferences)) {
                            $Preferences = $this->_GetPreferencesRegistration();

                            if (isset($Preferences) && $Preferences !== false) {
                                foreach ($Preferences as $key => $value) {
                                    $child_records = (isset($value['child_records']) && !empty($value['child_records'])) ? $value['child_records'] : array();
                                    if (isset($child_records[0]['PKPreferenceID']) && $child_records[0]['PKPreferenceID'] > 0) {
                                        $insert_member_preference = array(
                                            'FKMemberID' => $member_id,
                                            'FKPreferenceID' => $child_records[0]['PKPreferenceID']
                                        );
                                        $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                                    } else {
                                        continue;
                                    }
                                }
                            }
                        }

                        /* default member preferences 9/10/2018 */
                    }
                    $response_array['preferences'] = $this->_member_preference_records($member_id);

                    $email_field_array = array(
                        'Email Sender' => $this->GetWebsiteEmailSender(),
                        'Email Receivers' => $this->GetWebsiteEmailReceivers(),
                        'Website Name' => $this->GetWebsiteName(),
                        'Website URL' => $this->GetWebsiteURLAddress(),
                        'Website Telephone Number' => $this->GetWebsiteTelephoneNo(),
                        'Website Email Address' => $this->GetWebsiteEmailAddress(),
                        'Header Menus' => $this->GetEmailHeaderMenus(),
                        'Footer Menus' => $this->GetEmailFooterMenus(),
                        'Footer Title' => $this->GetEmailFooterTitle(),
                        'Full Name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                        'First Name' => $member_record['FirstName'],
                        'Last Name' => $member_record['LastName'],
                        'Email Address' => $member_record['EmailAddress'],
                        'Phone Number' => $member_record['Phone'],
                        'Postal Code' => $member_record['PostalCode'],
                        'Building Name' => $member_record['BuildingName'],
                        'Street Name' => $member_record['StreetName'],
                        'Town' => $member_record['Town'],
                        'Register From' => $member_record['RegisterFrom']
                    );
                    $this->emailsender->send_email_responder(1, $email_field_array);
                    $this->emailsender->send_email_responder(6, $email_field_array);

                    $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 11, 'Status' => 'Enabled'));
                    if ($sms_responder_record != false) {
                        $sms_field_array = array(
                            'Store Link' => $this->config->item('store_link_short_url'),
                            'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                            'Email Address' => $member_record['EmailAddress']
                        );
                        $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                        $this->SendMessage($sms_responder_record['PKResponderID'], 0, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_record['Phone'], 0), $msg_content);
                    }
                }
            }
        }
        echo json_encode($response_array);
    }

    public function post_account_update() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $first_name = isset($post_data['first_name']) ? $post_data['first_name'] : '';
            $last_name = isset($post_data['last_name']) ? $post_data['last_name'] : '';
            $phone = isset($post_data['phone']) ? $post_data['phone'] : '';
            $post_code = isset($post_data['post_code']) ? trim($post_data['post_code']) : '';
            $address = isset($post_data['building_name']) ? $post_data['building_name'] : '';
            $address2 = isset($post_data['street_name']) ? $post_data['street_name'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $email_address = isset($post_data['email_address']) ? $post_data['email_address'] : '';
            $password = isset($post_data['password']) ? $post_data['password'] : '';
            $referral_code = isset($post_data['referral_code']) ? $post_data['referral_code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $is_valid = true;


            if (!empty($id) && !empty($first_name) && !empty($last_name) && !empty($phone) && !empty($post_code) && !empty($address) && !empty($town) && !empty($email_address) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'MemberInformationVersion', array('PKMemberID' => $id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                    $is_valid = false;
                }

                /*
                  if ($this->verifyPhoneNumber("", $phone) == false) {
                  $response_array["message"] = "Please enter correct phone number e.g. 4478666xxxxx.";
                  $is_valid = false;
                  }
                 */

                $member_email_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $id, 'EmailAddress' => $email_address));
                if ($member_email_record != false) {
                    $response_array["message"] = "Email address already exist in our records";
                    $is_valid = false;
                }

                $member_phone_record = $this->model_database->GetRecord($this->tbl_members, 'PKMemberID', array('PKMemberID !=' => $id, 'Phone' => $phone));
                if ($member_phone_record != false) {
                    $response_array["message"] = "Phone number already exist in our records";
                    $is_valid = false;
                }

                if ($is_valid == true) {
                    $update_member = array(
                        // 'ReferralCode' => $referral_code,
                        'FirstName' => $first_name,
                        'LastName' => $last_name,
                        'EmailAddress' => $email_address,
                        'Phone' => $phone,
                        'PostalCode' => $post_code,
                        'BuildingName' => $address,
                        'StreetName' => $address2,
                        'Town' => $town,
                        'ID' => $id,
                        'MemberInformationVersion' => (intval($member_record["MemberInformationVersion"]) + 1),
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                    if (!empty($password)) {
                        $update_member['Password'] = encrypt_decrypt('encrypt', $password);
                    }
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Your Account Setting Updated Successfully";
                    $response_array["data"] = $this->_member_data_record($id);
                }
            } else {
                $response_array["message"] = "Fields Missing";
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_preferences_update() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $preference_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';



            if (!empty($member_id) && !empty($preference_data) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'MemberPreferenceVersion', array('PKMemberID' => $member_id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $this->model_database->RemoveRecord($this->tbl_member_preferences, $member_id, "FKMemberID");
                    $preference_array = explode("||", $preference_data);
                    if (sizeof($preference_array) > 0) {
                        $count = 0;
                        foreach ($preference_array as $p_data) {
                            if ($count == 0) {
                                $update_member = array(
                                    'AccountNotes' => $p_data,
                                    'ID' => $member_id
                                );
                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                            } else {
                                $insert_member_preference = array(
                                    'FKMemberID' => $member_id,
                                    'FKPreferenceID' => $p_data
                                );
                                $this->model_database->InsertRecord($this->tbl_member_preferences, $insert_member_preference);
                            }
                            $count += 1;
                        }
                    }
                    $response_array["result"] = "Success";
                    $response_array["message"] = "Your Laundry Settings Updated Successfully";
                    $update_member = array(
                        'ID' => $member_id,
                        'MemberPreferenceVersion' => (intval($member_record["MemberPreferenceVersion"]) + 1),
                        'UpdatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                    $response_array["member_data"] = $this->_member_data_record($member_id);
                    $response_array["data"] = $this->_member_preference_records($member_id);
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_credit_card() {
        error_reporting(1);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        //$post_data = $_GET;

        $stripeKey = $this->GetStripeAPIKey();
        if (!empty($post_data)) {

            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $type = isset($post_data['type']) ? $post_data['type'] : '';
            $title = isset($post_data['title']) ? $post_data['title'] : '';
            $name = isset($post_data['name']) ? $post_data['name'] : '';
            $number = isset($post_data['number']) ? $post_data['number'] : '';

            $last4 = substr($number, 12);
            $title = "************" . $last4;
            $name = $title;



            $year = isset($post_data['year']) ? $post_data['year'] : '';
            $month = isset($post_data['month']) ? $post_data['month'] : '';
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';

            if (!empty($member_id) && !empty($type) && !empty($device_id)) {


                $this->_update_device_information($device_id);
                $c_id = intval($id);
                $is_valid = true;
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'StripeCustomerID,MemberCardVersion', array('PKMemberID' => $member_id));


                $stripe_customer_id = $member_record["StripeCustomerID"];

                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                    $is_valid = false;
                }
                if ($type == "remove") {
                    if ($c_id != 0) {
                        $card_record = $this->model_database->GetRecord($this->tbl_member_cards, "PKCardID", array('PKCardID' => $c_id, 'FKMemberID' => $member_id));
                        if ($card_record != false) {
                            $this->model_database->RemoveRecord($this->tbl_member_cards, $c_id, "PKCardID");
                            $response_array["result"] = "Success";
                            $update_member = array(
                                'ID' => $member_id,
                                'MemberCardVersion' => (intval($member_record["MemberCardVersion"]) + 1),
                                'UpdatedDateTime' => date('Y-m-d H:i:s')
                            );
                            $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                            $response_array["member_data"] = $this->_member_data_record($member_id);
                            $response_array["data"] = $this->_member_card_records($member_id);
                            $response_array["message"] = "Card Deleted Successfully";
                        } else {
                            $response_array["message"] = "You are not authorized to delete this card information";
                        }
                    }
                } else {

                    if (strlen($year) < 4) {
                        $year = "20" . $year;
                    }


                    if ($is_valid) {
                        $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, 'PKCardID', array('PKCardID !=' => $c_id, 'FKMemberID' => $member_id, 'number' => $last4, 'Year' => $year, 'Month' => $month));

                        if ($member_card_record != false) {
                            $response_array["message"] = "Name already exist in your records";
                            $is_valid = false;
                        } else {

                            try {
                                $stripe = new Stripe();
                                $data = ['number' => $number, 'exp_month' => $month, 'exp_year' => $year, 'cvc' => $code];


                                $payment_method = $stripe->createCard($stripeKey, $data);



                                if (!isset($payment_method['error'])) {
                                    $data = array(
                                        'payment_method_types' => ['card'],
                                        "payment_method" => $payment_method->id,
                                        "customer" => $stripe_customer_id,
                                        "confirm" => true
                                    );
                                    $stripe->saveSetupIntent($stripeKey, $data);
                                    $payment_method->attach(['customer' => $stripe_customer_id]);
                                    $is_valid = true;
                                } else {
                                    $response_array["message"] = $payment_method["message"];
                                    $response_array["messageCode"] = "0";
                                    $is_valid = false;
                                }
                            } catch (Stripe\Exception\CardException $e) {
                                $response_array['exception'] = "CardException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\ApiErrorException $e) {
                                $response_array['exception'] = "ApiErrorException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\RateLimitException $e) {
                                $response_array['exception'] = "RateLimitException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\ApiConnectionException $e) {
                                $response_array['exception'] = "ApiConnectionException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\InvalidRequestException $e) {
                                $response_array['exception'] = "InvalidRequestException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\UnknownApiErrorException $e) {
                                $response_array['exception'] = "UnknownApiErrorException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception $e) {
                                $response_array['exception'] = "Exception";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\PermissionException $e) {
                                $response_array['exception'] = "PermissionException";
                                $response_array['message'] = $e->getMessage();
                            } catch (Stripe\Exception\UnexpectedValueException $e) {
                                $response_array['exception'] = "UnexpectedValueException";
                                $response_array['message'] = $e->getMessage();
                            }
                        }


                        if ($is_valid) {
                            $member_card = array(
                                'FKMemberID' => $member_id,
                                'Title' => $title,
                                'Name' => $name,
                                'Month' => $month,
                                'Year' => $year
                            );
                            $response_array["name"] = $name;
                            $response_array["title"] = $title;
                            $response_array["number"] = $last4;
                            $response_array["month"] = $month;
                            $response_array["year"] = $year;
                            $response_array["code"] = $payment_method->id;

                            if (!empty($number)) {
                                $member_card['Number'] = $last4;
                            }
                            if (!empty($code)) {
                                $member_card['Code'] = $payment_method->id;
                            }
                            if ($c_id == 0) {
                                $member_card['CreatedDateTime'] = date('Y-m-d H:i:s');
                                $c_id = $this->model_database->InsertRecord($this->tbl_member_cards, $member_card);
                                $response_array["card_id"] = $c_id;
                                $response_array["message"] = "Card Added Successfully";
                            } else {
                                $member_card['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                $member_card['ID'] = $c_id;
                                $this->model_database->UpdateRecord($this->tbl_member_cards, $member_card, 'PKCardID');
                                $response_array["message"] = "Card Updated Successfully";
                            }
                            $response_array["result"] = "Success";


                            //$this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                            $response_array["member_data"] = $this->_member_data_record($member_id);
                            $response_array["data"] = $this->_member_card_records($member_id);
                        }
                    }
                    $response_array["card_id"] = $c_id;
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_discount_code() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $is_bundle = isset($post_data['is_bunddle']) ? $post_data['is_bunddle'] : 0;
            if (!empty($code) && !empty($member_id) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $is_found = true;
                $discount_record = $this->model_database->GetRecord($this->tbl_discounts, false, array('Code' => $code, 'Status' => 'Active'));
                if ($discount_record != false) {
                    $current_date = date('Y-m-d');
                    if ($discount_record['FKMemberID'] != 0) {
                        if ($discount_record['FKMemberID'] != $member_id) {
                            $response_array["message"] = "We did not remember the discount code you entered";
                            $is_found = false;
                        }
                    }

                    /* Hassan changes 26-5-2018 */

                    if (isset($is_bundle) && $is_bundle > 0) {

                        $response_array["message"] = "Discount code is not valid on laundry packages";
                        $is_found = false;
                    }
                    /* Hassan changes 26-5-2018 */

                    if ($is_found) {
                        if (!empty($discount_record['StartDate'])) {
                            $start_date = date('Y-m-d', strtotime($discount_record['StartDate']));
                            if ($current_date < $start_date) {
                                $is_found = false;
                                $response_array["message"] = "We did not remember the discount code you entered";
                            }
                        }
                    }
                    if ($is_found) {
                        if (!empty($discount_record['ExpireDate'])) {
                            $expire_date = date('Y-m-d', strtotime($discount_record['ExpireDate']));
                            if ($current_date > $expire_date) {
                                $update_discount = array(
                                    'Status' => 'Expire',
                                    'ID' => $discount_record['PKDiscountID'],
                                    'UpdatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->UpdateRecord($this->tbl_discounts, $update_discount, "PKDiscountID");
                                $is_found = false;
                                $response_array["message"] = "We did not remember the discount code you entered";
                            }
                        }
                    }
                    if ($is_found) {
                        if ($discount_record['CodeUsed'] == "One Time") {
                            $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, 'PKInvoiceID', array('FKDiscountID' => $discount_record['PKDiscountID'], 'FKMemberID' => $member_id));
                            if ($invoice_record != false) {
                                $is_found = false;
                                $response_array["message"] = "You have already used this discount code";
                            }
                        }
                    }
                    if ($is_found) {
                        unset($response_array["message"]);
                        $response_array["result"] = "Success";
                        $response_array["data"] = "Discount|" . $discount_record['PKDiscountID'] . '|' . $discount_record['Code'] . '|' . $discount_record['Worth'] . '|' . $discount_record['DType'] . '|' . $discount_record['MinimumOrderAmount'];
                    }
                } else {
                    $response_array["message"] = "We did not remember the discount code you entered";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_referral_code() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $member_data = isset($post_data['member_data']) ? $post_data['member_data'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($code) && !empty($member_data) && !empty($device_id)) {

                $this->_update_device_information($device_id);
                $member_data_array = json_decode($member_data, true);

                $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID", array('DiscountType' => 'Referral', 'FKMemberID' => $member_data_array['ID']));

                if ($invoice_record == false) {
                    $member_referral_record = $this->model_database->GetRecord($this->tbl_members, "PKMemberID", "ReferralCode='" . $post_data['code'] . "' and Status='Enabled' and Phone != '" . $member_data_array['Phone'] . "' and BuildingName !='" . $member_data_array['BuildingName'] . "' and PKMemberID !=" . $member_data_array['ID']);

                    if ($member_referral_record != false) {
                        unset($response_array["message"]);
                        $response_array["result"] = "Success";
                        $referral_amount = $this->GetReferralCodeAmount();
                        $response_array["data"] = "Referral|" . $member_referral_record['PKMemberID'] . '|' . $post_data['code'] . '|' . $referral_amount . '|Price|' . ($referral_amount + 5);
                    } else {
                        $response_array["result"] = "referral";
                        $response_array["message"] = "We did not remember the referral code you entered";
                    }
                } else {
                    $response_array["result"] = "referral";
                    $response_array["message"] = "You can only use referral code once.";
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_invoice_copy() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $log = http_build_query($post_data);
        file_put_contents('./application/logs/invoice_copy_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($id) && !empty($member_id) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'InvoicePendingVersion', array('PKMemberID' => $member_id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID as ID,FKWebsiteID,FKFranchiseID,FKMemberID,InvoiceType,Location,Locker,BuildingName,StreetName,PostalCode,Town,CustomerNotes,OrderNotes,AdditionalInstructions", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                    if ($invoice_record != false) {
                        $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");
                        $invoice_data = array(
                            'FKWebsiteID' => $invoice_record['FKWebsiteID'],
                            'FKFranchiseID' => $invoice_record['FKFranchiseID'],
                            'FKMemberID' => $invoice_record['FKMemberID'],
                            'InvoiceType' => $invoice_record['InvoiceType'],
                            'InvoiceNumber' => $invoice_number,
                            'Location' => $invoice_record['Location'],
                            'Locker' => $invoice_record['Locker'],
                            'BuildingName' => $invoice_record['BuildingName'],
                            'StreetName' => $invoice_record['StreetName'],
                            'PostalCode' => $invoice_record['PostalCode'],
                            'Town' => $invoice_record['Town'],
                            'CustomerNotes' => $invoice_record['CustomerNotes'],
                            'OrderNotes' => $invoice_record['OrderNotes'],
                            'AccountNotes' => $invoice_record['AccountNotes'],
                            'AdditionalInstructions' => $invoice_record['AdditionalInstructions'],
                            'PaymentMethod' => "Stripe",
                            'PaymentStatus' => "Pending",
                            'OrderStatus' => "Pending",
                            'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : null,
                            'OrderPostFrom' => "Mobile"
                        );
                        $franchise_record = $this->_franchise_record($invoice_record['FKFranchiseID']);
                        if ($invoice_record['InvoiceType'] == "After") {
                            $minimum_order_amount_later = $this->GetOrderAmountLater();
                            if (intval($franchise_record['MinimumOrderAmountLater']) > 0) {
                                $minimum_order_amount_later = intval($franchise_record['MinimumOrderAmountLater']);
                            }
                            $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                            $invoice_data['GrandTotal'] = floatval($minimum_order_amount_later);
                            $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                        } else {
                            $invoice_id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                            $preferences_records = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoicePreferenceID");
                            $services_records = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKCategoryID,FKServiceID,Quantity", array('FKInvoiceID' => $invoice_record['ID']), false, false, false, "PKInvoiceServiceID");
                            $services_total = floatval(0);
                            $preferences_total = floatval(0);
                            $is_preferences = false;
                            $package_count = intval(0);
                            if (sizeof($services_records) > 0) {
                                foreach ($services_records as $rec) {
                                    $service_record = $this->model_database->model_database->GetRecord($this->tbl_services, "Title,DesktopImageName,MobileImageName,Price,IsPackage,PreferencesShow", array('PKServiceID' => $rec['FKServiceID']));
                                    if ($service_record != false) {
                                        $service_price = floatval($service_record['Price']);
                                        $service_total_price = floatval(intval($rec['Quantity']) * $service_price);
                                        $invoice_service_data = array(
                                            'FKInvoiceID' => $invoice_id,
                                            'FKCategoryID' => $rec['FKCategoryID'],
                                            'FKServiceID' => $rec['FKServiceID'],
                                            'Title' => $service_record['Title'],
                                            'ImageURL' => '/uploads/services/' . $service_record['DesktopImageName'],
                                            'DesktopImageName' => $service_record['DesktopImageName'],
                                            'MobileImageName' => $service_record['MobileImageName'],
                                            'IsPackage' => $service_record['IsPackage'],
                                            'PreferencesShow' => $service_record['PreferencesShow'],
                                            'Quantity' => $rec['Quantity'],
                                            'Price' => $service_price,
                                            'Total' => $service_total_price
                                        );
                                        $invoice_service_id = $this->model_database->InsertRecord($this->tbl_invoice_services, $invoice_service_data);
                                        if ($invoice_service_id != 0) {
                                            $services_total += $service_total_price;
                                            if ($invoice_service_data['PreferencesShow'] == "Yes") {
                                                $is_preferences = true;
                                            }
                                            if ($invoice_service_data['IsPackage'] == "Yes") {
                                                $package_count += intval($invoice_service_data['Quantity']);
                                            }
                                        }
                                    }
                                }
                            }
                            if ($is_preferences && sizeof($preferences_records) > 0) {
                                foreach ($preferences_records as $rec) {
                                    $preference_record = $this->model_database->GetRecord($this->tbl_preferences, "ParentPreferenceID,Title,Price,PriceForPackage", array('PKPreferenceID' => $rec['FKPreferenceID']));
                                    if ($preference_record != false) {
                                        $parent_preference_record = $this->model_database->GetRecord($this->tbl_preferences, "Title", array('PKPreferenceID' => $preference_record['ParentPreferenceID']));
                                        if ($parent_preference_record != false) {
                                            $invoice_preference_data = array(
                                                'FKInvoiceID' => $invoice_id,
                                                'FKPreferenceID' => $rec['FKPreferenceID'],
                                                'ParentTitle' => $parent_preference_record['Title'],
                                                'Title' => $preference_record['Title']
                                            );
                                            $preference_price = floatval($preference_record['Price']);
                                            $preference_total_price = $preference_price;
                                            if ($preference_price > 0) {
                                                if ($preference_record['PriceForPackage'] == "Yes" && $package_count > 0) {
                                                    $preference_total_price = floatval($preference_price * $package_count);
                                                }
                                                $invoice_preference_data["Price"] = $preference_price;
                                                $invoice_preference_data["Total"] = $preference_total_price;
                                            }
                                            $invoice_preference_id = $this->model_database->InsertRecord($this->tbl_invoice_preferences, $invoice_preference_data);
                                            if ($invoice_preference_id != 0) {
                                                $preferences_total += $preference_total_price;
                                            }
                                        }
                                    }
                                }
                            }
                            $grand_total = floatval($services_total + $preferences_total);
                            $invoice_data['ServicesTotal'] = $services_total;
                            if ($preferences_total > 0) {
                                $invoice_data['PreferenceTotal'] = $preferences_total;
                                $invoice_data['SubTotal'] = $grand_total;
                            }
                            $invoice_data['GrandTotal'] = $grand_total;
                            $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                            $invoice_data['ID'] = $invoice_id;
                            $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');
                        }
                        $response_array["result"] = "Success";
                        $update_member = array(
                            'ID' => $member_id,
                            'InvoicePendingVersion' => (intval($member_record["InvoicePendingVersion"]) + 1),
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                        $response_array["member_data"] = $this->_member_data_record($member_id);
                        $response_array['data'] = $this->_member_invoice_records($member_id, "Pending");
                        $response_array["message"] = "Invoice #" . $invoice_number . " has been created on pending invoices";
                    } else {
                        $response_array["message"] = "Love2Laundry : Invoice not found";
                    }
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_invoice_re_order_copy() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        //$post_data=$_GET;
        $log = http_build_query($post_data);
        file_put_contents('./application/logs/re_order_invoice_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $type = isset($post_data['type']) ? $post_data['type'] : '';
            if (!empty($id) && !empty($member_id) && !empty($device_id) && !empty($type)) {

                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'InvoicePendingVersion,InvoiceCancelVersion', array('PKMemberID' => $member_id));

                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "PKInvoiceID,InvoiceNumber,TookanResponse", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                    $invoice_number = $invoice_record['InvoiceNumber'];


                    if ($invoice_record != false) {
                        $invoice_data = array(
                            'ID' => $id,
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        if ($type == "Cancel") {
                            $invoice_data['PaymentStatus'] = "Cancel";
                            $invoice_data['OrderStatus'] = "Cancel";
                        } else if ($type == "Re Order") {
                            $invoice_data['PaymentStatus'] = "Pending";
                            $invoice_data['OrderStatus'] = "Pending";
                        }
                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                        $response_array["result"] = "Success";
                        $update_member = array(
                            'ID' => $member_id,
                            'InvoicePendingVersion' => (intval($member_record["InvoicePendingVersion"]) + 1),
                            'InvoiceCancelVersion' => (intval($member_record["InvoiceCancelVersion"]) + 1),
                            'UpdatedDateTime' => date('Y-m-d H:i:s')
                        );
                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, "PKMemberID");
                        $response_array["member_data"] = $this->_member_data_record($member_id);
                        $response_array['pending_data'] = $this->_member_invoice_records($member_id, "Pending");
                        $response_array['cancel_data'] = $this->_member_invoice_records($member_id, "Cancel");
                        if ($type == "Cancel") {
                            $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                            if (isset($tooken_response_array['data']['job_id'])) {
                                $tookan_array = array(
                                    'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                    'job_id' => $tooken_response_array['data']['job_id']
                                );
                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                $update_invoice_record = array(
                                    'ID' => $invoice_record['PKInvoiceID'],
                                    'TookanResponse' => null,
                                    'TookanUpdateResponse' => $tookan_response_result
                                );
                                $this->deleteDeliveryTask($tooken_response_array);
                                $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                            }

                            $this->SendInvoiceEmail($invoice_number, "X");
                            $response_array["message"] = "Invoice #" . $invoice_record['InvoiceNumber'] . " moved on cancel invoices";
                        } else if ($type == "Re Order") {
                            $response_array["message"] = "Invoice #" . $invoice_record['InvoiceNumber'] . " moved on pending invoices";
                        }
                    } else {
                        $response_array["message"] = "Love2Laundry : Invoice not found";
                    }
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_invoice_edit() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        $log = http_build_query($post_data);
        file_put_contents('./application/logs/edit_invoice_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {
            $id = isset($post_data['id']) ? $post_data['id'] : '';
            $member_id = isset($post_data['member_id']) ? $post_data['member_id'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';

            if (!empty($id) && !empty($member_id) && !empty($device_id)) {

                $this->_update_device_information($device_id);
                $member_record = $this->model_database->GetRecord($this->tbl_members, 'BuildingName,StreetName,PostalCode,Town', array('PKMemberID' => $member_id));
                if ($member_record == false) {
                    $response_array["message"] = "Member Information not found";
                } else {
                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "FKFranchiseID,FKCardID,FKDiscountID,ReferralID,DiscountType,DiscountCode,DiscountWorth,DType,PaymentMethod,InvoiceType,Location,Locker,BuildingName,StreetName,PostalCode,Town,AccountNotes,AdditionalInstructions,ServicesTotal,PreferenceTotal,SubTotal,DiscountTotal,GrandTotal,PickupDate,PickupTime,DeliveryDate,DeliveryTime,Regularly", array('FKMemberID' => $member_id, 'PKInvoiceID' => $id));
                    if ($invoice_record != false) {
                        $invoice_record['PickupDateSelected'] = date('d-F-Y', strtotime($invoice_record['PickupDate']));
                        $invoice_record['DeliveryDateSelected'] = date('d-F-Y', strtotime($invoice_record['DeliveryDate']));
                        $is_address_change = false;
                        if ($member_record['BuildingName'] != $invoice_record['BuildingName']) {
                            $is_address_change = true;
                        }
                        if ($member_record['StreetName'] != $invoice_record['StreetName']) {
                            $is_address_change = true;
                        }
                        if ($member_record['PostalCode'] != $invoice_record['PostalCode']) {
                            $is_address_change = true;
                        }
                        if ($member_record['Town'] != $invoice_record['Town']) {
                            $is_address_change = true;
                        }
                        if ($is_address_change) {
                            $invoice_record['PickerAddressSelectedIndex'] = "1";
                        } else {
                            $invoice_record['PickerAddressSelectedIndex'] = "0";
                        }
                        if ($invoice_record['DiscountType'] == "Discount") {
                            $discount_minimum_amount = "";
                            $discount_record = $this->model_database->GetRecord($this->tbl_discounts, "MinimumOrderAmount", array("PKDiscountID" => $invoice_record['FKDiscountID']));
                            if ($discount_record != false) {
                                $discount_minimum_amount = $discount_record['MinimumOrderAmount'];
                            }
                            $invoice_record['DiscountMinimumAmount'] = $discount_minimum_amount;
                        } else if ($invoice_record['DiscountType'] == "Referral") {
                            $invoice_record['ReferralMinimumAmount'] = ($this->GetReferralCodeAmount() + 5);
                        }
                        $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Name", array('PKCardID' => $invoice_record['FKCardID']));
                        if ($member_card_record != false) {
                            $invoice_record['CardName'] = $member_card_record['Name'];
                        }
                        $invoice_record['franchise_detail'] = $this->_franchise_record($invoice_record['FKFranchiseID']);
                        if ($invoice_record['InvoiceType'] == "Items") {
                            $invoice_record['preferences_records'] = $this->model_database->GetRecords($this->tbl_invoice_preferences, "R", "FKPreferenceID,ParentTitle,Title,Price,Total", array('FKInvoiceID' => $id), false, false, false, "PKInvoicePreferenceID");
                            $invoice_record['services_records'] = $this->model_database->GetRecords($this->tbl_invoice_services, "R", "FKServiceID as ID,FKCategoryID as CategoryID,Title,DesktopImageName as DesktopImage,MobileImageName as MobileImage,IsPackage,PreferencesShow,Quantity,Price,Total", array('FKInvoiceID' => $id), false, false, false, "PKInvoiceServiceID");
                            foreach ($invoice_record['services_records'] as $key => $value) {
                                $service_record = $this->model_database->GetRecord($this->tbl_services, "Content", array("PKServiceID" => $value['ID']));
                                if ($service_record != false) {
                                    $invoice_record['services_records'][$key]['Content'] = str_replace("\n", "", RemoveHtmlTags($service_record['Content']));
                                    $invoice_record['services_records'][$key]['TitleUpper'] = strtoupper($value['Title']);
                                    $invoice_record['services_records'][$key]['CurrencyAmount'] = $this->config->item("currency_symbol") . $value['Price'];
                                    $desktop_image_name_path = "";
                                    $mobile_image_name_path = "";
                                    if (!empty($value['DesktopImage'])) {

                                        if (file_exists($this->config->item("dir_upload_service") . $value['DesktopImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value['DesktopImage'])) {
                                            $desktop_image_name_path = str_replace("lovetolaundrydev", "", base_url()) . $this->config->item("front_service_folder_url") . $value['DesktopImage'];
                                        }
                                    }
                                    if (!empty($value['MobileImage'])) {
                                        if (file_exists($this->config->item("dir_upload_service") . $value['MobileImage']) && file_exists($this->config->item("dir_upload_service_thumb") . $value['MobileImage'])) {
                                            $mobile_image_name_path = str_replace("lovetolaundrydev", "", base_url()) . $this->config->item("front_service_folder_url") . $value['MobileImage'];
                                        }
                                    }
                                    $invoice_record['services_records'][$key]['DesktopImagePath'] = $desktop_image_name_path;
                                    $invoice_record['services_records'][$key]['MobileImagePath'] = $mobile_image_name_path;
                                } else {
                                    unset($invoice_record['services_records'][$key]);
                                }
                            }
                        }
                        $response_array["result"] = "Success";
                        $response_array["data"] = $invoice_record;
                    } else {
                        $response_array["message"] = "Love2Laundry : Invoice not found";
                    }
                }
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function order_test() {
        // var_dump(json_decode($this->post_stripe_curl_request("https://api.stripe.com/v1/tokens",array('card' => array("number" => "4242424242424242", "exp_month" => "12", "exp_year" => "2018", "cvc" => "123"))),true));
        //var_dump(json_decode($this->post_stripe_curl_request("https://api.stripe.com/v1/charges",array("amount" => round(12 * 100), "currency" => "gbp", "source" => "tok_1AEyVaKMOOEVGfcKlvvYdrMv", "description" => "invoice #asd")),true));
    }

    public function post_invoice_checkout() {
        error_reporting(1);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["type"] = "Message";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);

        $log = http_build_query($post_data);

        file_put_contents('./application/logs/checkout_' . date("j.n.Y") . '_' . time() . '.log', $log, FILE_APPEND);
        if ($post_data) {
            $grand_total = floatval(isset($post_data['grand_total']) ? $post_data['grand_total'] : '');
            $card_id = intval(isset($post_data['card_id']) ? $post_data['card_id'] : '');
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $address = isset($post_data['building_name']) ? $post_data['building_name'] : '';
            $street_name = isset($post_data['street_name']) ? $post_data['street_name'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $pick_date = isset($post_data['pick_date']) ? date('Y-m-d', strtotime($post_data['pick_date'])) : '';
            $pick_time = isset($post_data['pick_time']) ? $post_data['pick_time'] : '';
            $delivery_date = isset($post_data['delivery_date']) ? date('Y-m-d', strtotime($post_data['delivery_date'])) : '';
            $delivery_time = isset($post_data['delivery_time']) ? $post_data['delivery_time'] : '';
            $member_data = isset($post_data['member_data']) ? $post_data['member_data'] : '';
            $franchise_data = isset($post_data['franchise_data']) ? $post_data['franchise_data'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $regularly_cart = isset($post_data['regularly_cart']) ? $post_data['regularly_cart'] : 0;
            $appVersion = isset($post_data['version']) ? $post_data['version'] : "";
            $paymentMethod = isset($post_data['payment_method']) ? $post_data['payment_method'] : "Stripe";
            //$franchise_data = trim(preg_replace('/\s\s+/', ' ', $franchise_data));
            //$franchise_data = trim(preg_replace('/\s\s+/', ' ', $franchise_data));
            $franchise_data = $this->clean_json($franchise_data);

            if ($grand_total != 0 && !empty($pick_date) && !empty($pick_time) && !empty($delivery_date) && !empty($delivery_time) && !empty($post_code) && !empty($address) && !empty($town) && !empty($member_data) && !empty($franchise_data) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_data = $this->clean_json($member_data);
                $member_data_array = json_decode($member_data, true);
                $franchise_data_array = json_decode($franchise_data, true);

                if (!empty($member_data_array) && !empty($franchise_data_array)) {

                    $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone,StripeCustomerID", array('PKMemberID' => $member_data_array["ID"]));

                    $stripe_customer_id = $member_record["StripeCustomerID"];

                    $id = intval(isset($post_data['id']) ? $post_data['id'] : '');
                    $discount_referral_data = isset($post_data['discount_referral_data']) ? $post_data['discount_referral_data'] : '';
                    $preferences_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
                    $service_total = floatval(isset($post_data['service_total']) ? $post_data['service_total'] : '0.00');
                    $preference_total = floatval(isset($post_data['preference_total']) ? $post_data['preference_total'] : '0.00');
                    $sub_total = floatval(isset($post_data['sub_total']) ? $post_data['sub_total'] : '');
                    $discount_total = floatval(isset($post_data['discount_total']) ? $post_data['discount_total'] : '0.00');
                    $service_data = isset($post_data['services_data']) ? $post_data['services_data'] : '';
                    $is_valid = true;
                    if ($id == 0) {
                        $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and Date='" . $pick_date . "' and Time='" . $pick_time . "' and (Type='Both' or Type='Pickup')");
                        if ($disable_slot_record != false) {
                            $is_valid = false;
                            $response_array["type"] = "Date Time";
                            $response_array["message"] = "PickUp Date Time not available";
                        } else {

                            if ($franchise_data_array['AllowSameTime'] == "No") {
                                $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $pick_date . "' and PickupTime='" . $pick_time . "') or (DeliveryDate='" . $pick_date . "' and DeliveryTime='" . $pick_time . "')");
                                if ($invoice_record_count > 0) {
                                    $is_valid = false;
                                    $response_array["type"] = "Date Time";
                                    $response_array["message"] = "PickUp Date Time not available";
                                }
                            } else {
                                $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and PickupDate='" . $pick_date . "' and PickupTime='" . $pick_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                                if ($invoice_record_count > 0) {
                                    if ($invoice_record_count >= intval($franchise_data_array['PickupLimit'])) {
                                        $is_valid = false;
                                        $response_array["type"] = "Date Time";
                                        $response_array["message"] = "PickUp Date Time not available";
                                    }
                                }
                            }
                        }
                        if ($is_valid) {
                            $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and Date='" . $delivery_date . "' and Time='" . $delivery_time . "' and (Type='Both' or Type='Delivery')");
                            if ($disable_slot_record != false) {
                                $is_valid = false;
                                $response_array["type"] = "Date Time";
                                $response_array["message"] = "Delivery Date Time not available";
                            } else {
                                if ($franchise_data_array['AllowSameTime'] == "No") {
                                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $delivery_date . "' and PickupTime='" . $delivery_time . "') or (DeliveryDate='" . $delivery_date . "' and DeliveryTime='" . $delivery_time . "')");
                                    if ($invoice_record_count > 0) {
                                        $is_valid = false;
                                        $response_array["type"] = "Date Time";
                                        $response_array["message"] = "Delivery Date Time not available";
                                    }
                                } else {
                                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and DeliveryDate='" . $delivery_date . "' and DeliveryTime='" . $delivery_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                                    if ($invoice_record_count > 0) {
                                        if ($invoice_record_count >= intval($franchise_data_array['DeliveryLimit'])) {
                                            $is_valid = false;
                                            $response_array["type"] = "Date Time";
                                            $response_array["message"] = "Delivery Date Time not available";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    /* Hassan changes 1-4-2018 */
                    if ($post_data['pick_date'] && $post_data['delivery_date']) {
                        $pick_dateTMP = $post_data['pick_date'];
                        $pick_timeTMP = $post_data['pick_time'];
                        if (isset($pick_timeTMP) && $pick_timeTMP != '') {
                            $pick_timeArray = explode('-', $pick_timeTMP);
                            $pick_timeClean = isset($pick_timeArray[0]) ? $pick_timeArray[0] . ':00' : '';
                        } else {
                            $pick_timeClean = '';
                        }
                        $pickupDateTimeTMP = $pick_dateTMP . ' ' . $pick_timeClean;

                        $delivery_dateTMP = $post_data['delivery_date'];
                        $delivery_timeTMP = $post_data['delivery_time'];

                        if (isset($delivery_timeTMP) && $delivery_timeTMP != '') {
                            $delivery_timeArray = explode('-', $delivery_timeTMP);
                            $delivery_timeClean = isset($delivery_timeArray[0]) ? $delivery_timeArray[0] . ':00' : '';
                        } else {
                            $delivery_timeClean = '';
                        }

                        $deliveryDateTimeTMP = $delivery_dateTMP . ' ' . $delivery_timeClean;

                        $diffrenceTMP = $this->dateTime_difference($pickupDateTimeTMP, $deliveryDateTimeTMP);

                        if ($diffrenceTMP < 35) {
                            $is_valid = false;
                            $response_array["type"] = "Date Time";
                            $response_array["message"] = "Difference between Pickup and Delivery Date Time should be greater than 35 hours";
                        }
                    }

                    /* Hassan difference of 3 hours pickup 25-5-2018 */
                    if (isset($id)) {

                        $invoice_recordTmp = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,PaymentStatus,GrandTotal,OrderStatus,PickupTime,PickupDate", array('PKInvoiceID' => $id));

                        if (isset($invoice_recordTmp['OrderStatus']) && $invoice_recordTmp['OrderStatus'] == "Pending" || $invoice_recordTmp['OrderStatus'] == "Processed") {

                            $is_edit_button_show = true;
                            $pickup_time_array = explode("-", $invoice_recordTmp['PickupTime']);

                            $date1 = new DateTime(date('Y-m-d', strtotime($invoice_recordTmp['PickupDate'])) . 'T' . $pickup_time_array[0] . ':00');

                            $date2 = new DateTime(date('Y-m-d') . 'T' . date('H:i:s'));

                            // The diff-methods returns a new DateInterval-object...

                            $interval = $date2->diff($date1);

                            $invert = $interval->invert;

                            $h = $interval->format('%h');
                            $days = $interval->format('%d');
                            $hour = ($days * 24) + $h;

                            if ($invert > 0) {

                                $is_edit_button_show = false;
                            } else {

                                if ($hour <= 3) {

                                    $is_edit_button_show = false;
                                }
                            }
                            if (!$is_edit_button_show) {


                                $is_valid = false;
                                $response_array["message"] = "Dear Customer, You cannot update order, please contact our customer support";
                            }
                        }
                    }
                    /* Hassan difference of 3 hours pickup ends 25-5-2018 */


                    if ($is_valid) {
                        $invoice_number = "";
                        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,PaymentStatus,GrandTotal", array('PKInvoiceID' => $id));
                        if ($invoice_record != false) {
                            $invoice_number = $invoice_record['InvoiceNumber'];
                            $response_array['InvoiceNumber'] = $invoice_number;
                            if ($invoice_record["PaymentStatus"] == "Completed") {
                                if (floatval($invoice_record["GrandTotal"]) > $grand_total) {
                                    $is_valid = false;
                                    $response_array["message"] = "Dear Customer, You cannot reduce your order amount from the cart, please contact our customer support";
                                }
                            }
                        }
                        if ($is_valid) {
                            $stripe_grand_total = floatval(0);
                            if ($invoice_record != false) {
                                $invoice_payment_records = $this->model_database->GetRecords($this->tbl_invoice_payment_informations, "R", "Amount", array('FKInvoiceID' => $id));
                                if (sizeof($invoice_payment_records) > 0) {
                                    $payment_information_amount = floatval(0);
                                    foreach ($invoice_payment_records as $rec) {
                                        $payment_information_amount += floatval($rec["Amount"]);
                                    }
                                    $stripe_grand_total = $grand_total - $payment_information_amount;
                                } else {
                                    $stripe_grand_total = $grand_total;
                                }
                            } else {
                                $stripe_grand_total = $grand_total;
                            }
                            $is_test = false;
                            $card_token = "";

                            if ($stripe_grand_total > 0 && $paymentMethod == "Stripe") {
                                $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Number,Year,Month,Code", array("PKCardID" => $card_id));
                                if ($member_card_record == false) {

                                    $response_array["type"] = "Card";
                                    $response_array["message"] = "Card Information not found";
                                    //$response_array['card_data'] = $this->_member_card_records($member_data_array["ID"]);
                                    $is_valid = false;
                                } else {
                                    $orderStatus = "Processed";
                                    $paymentStatus = "Processed";
                                }
                            } else {
                                $orderStatus = "Processed";
                                $paymentStatus = "Pending";
                            }

                            if ($is_valid) {
                                if (empty($invoice_number)) {
                                    $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");
                                    $response_array['InvoiceNumber'] = $invoice_number;
                                }
                                $invoice_data = array(
                                    'BuildingName' => $address,
                                    'StreetName' => $street_name,
                                    'Town' => $town,
                                    'PostalCode' => $post_code,
                                    'FKDeviceID' => $device_id,
                                    'FKCardID' => $card_id,
                                    'FKMemberID' => $member_data_array["ID"],
                                    'FKFranchiseID' => $franchise_data_array["ID"],
                                    'InvoiceNumber' => $invoice_number,
                                    'PaymentMethod' => $paymentMethod,
                                    'OrderNotes' => isset($post_data['account_notes']) ? $post_data['account_notes'] : '',
                                    'AccountNotes' => isset($post_data['account_notes']) ? $post_data['account_notes'] : '',
                                    'AdditionalInstructions' => isset($post_data['additional_instruction']) ? $post_data['additional_instruction'] : '',
                                    'GrandTotal' => $grand_total,
                                    'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                                    'PickupDate' => $pick_date,
                                    'PickupTime' => $pick_time,
                                    'OrderPostFrom' => 'Mobile',
                                    'DeliveryDate' => $delivery_date,
                                    'DeliveryTime' => $delivery_time,
                                    'Regularly' => $regularly_cart,
                                    'AppVersion' => $appVersion
                                );
                                if ($service_total > 0) {
                                    $invoice_data['ServicesTotal'] = $service_total;
                                }
                                if ($preference_total > 0) {
                                    $invoice_data['PreferenceTotal'] = $preference_total;
                                }
                                if ($sub_total > 0) {
                                    $invoice_data['SubTotal'] = $sub_total;
                                }
                                if ($discount_total > 0) {
                                    $invoice_data['DiscountTotal'] = $discount_total;
                                }
                                $is_discount_found = false;
                                if (!empty($discount_referral_data)) {
                                    $discount_referral_data_array = explode('|', $discount_referral_data);
                                    if (sizeof($discount_referral_data_array) > 0) {
                                        if ($discount_referral_data_array[0] == "Discount") {
                                            $invoice_data['DiscountType'] = "Discount";
                                            $invoice_data['FKDiscountID'] = $discount_referral_data_array[1];
                                        } else {
                                            $invoice_data['DiscountType'] = "Referral";
                                            $invoice_data['ReferralID'] = $discount_referral_data_array[1];
                                        }
                                        $invoice_data['DiscountCode'] = $discount_referral_data_array[2];
                                        $invoice_data['DiscountWorth'] = $discount_referral_data_array[3];
                                        $invoice_data['DType'] = $discount_referral_data_array[4];
                                        $is_discount_found = true;
                                    }
                                }
                                if (!$is_discount_found) {
                                    $invoice_data['DiscountType'] = "None";
                                    $invoice_data['FKDiscountID'] = null;
                                    $invoice_data['ReferralID'] = null;
                                    $invoice_data['DiscountCode'] = null;
                                    $invoice_data['DiscountWorth'] = null;
                                    $invoice_data['DType'] = "None";
                                }
                                if ($invoice_record == false) {
                                    $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                    $invoice_data['PaymentStatus'] = $paymentStatus;
                                    $invoice_data['OrderStatus'] = $orderStatus;
                                    $id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                                } else {
                                    $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $id, "FKInvoiceID");
                                    $this->model_database->RemoveRecord($this->tbl_invoice_services, $id, "FKInvoiceID");
                                }
                                $invoice_data['ID'] = $id;
                                $response_array["InvoiceID"] = $id;
                                $is_service_found = false;
                                $tookan_invoice_service = array();
                                if (strlen($service_data) > 10) {
                                    $service_data_array = json_decode($service_data, true);
                                    if (sizeof($service_data_array) > 0) {
                                        foreach ($service_data_array as $cart_data) {
                                            $total = floatval(intval($cart_data['Quantity']) * floatval($cart_data['Price'])); /* Cart services calculation hassan 1-13-2019 */

                                            $insert_invoice_service = array(
                                                'FKInvoiceID' => $id,
                                                'FKServiceID' => $cart_data['ID'],
                                                'FKCategoryID' => $cart_data['CategoryID'],
                                                'Title' => $cart_data['Title'],
                                                'DesktopImageName' => $cart_data['DesktopImage'],
                                                'MobileImageName' => $cart_data['MobileImage'],
                                                'IsPackage' => $cart_data['IsPackage'],
                                                'PreferencesShow' => $cart_data['PreferencesShow'],
                                                'Quantity' => $cart_data['Quantity'],
                                                'Price' => $cart_data['Price'],
                                                'Total' => $total
                                            );
                                            $tookan_invoice_service[] = $insert_invoice_service;
                                            $invoice_service_id = $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                            if ($invoice_service_id != 0) {
                                                $is_service_found = true;
                                            }
                                        }
                                    }
                                }
                                if ($is_service_found) {
                                    $invoice_data['InvoiceType'] = "Items";
                                } else {
                                    $invoice_data['InvoiceType'] = "After";
                                }
                                if (strlen($preferences_data) > 10) {
                                    $preferences_data_array = explode("|", $preferences_data);
                                    if (sizeof($preferences_data_array) > 0) {
                                        foreach ($preferences_data_array as $rec) {
                                            $preference_array = explode(",", $rec);
                                            if (sizeof($preference_array) > 0) {
                                                $insert_invoice_preference = array(
                                                    'FKInvoiceID' => $id,
                                                    'FKPreferenceID' => $preference_array[0],
                                                    'ParentTitle' => $preference_array[1],
                                                    'Title' => $preference_array[2]
                                                );
                                                $preference_price = floatval($preference_array[3]);
                                                if ($preference_price > 0) {
                                                    $insert_invoice_preference["Price"] = $preference_price;
                                                    $insert_invoice_preference["Total"] = floatval($preference_array[4]);
                                                }

                                                $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                            }
                                        }
                                    }
                                }
                                if ($invoice_record == false) {
                                    $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                } else {
                                    $invoice_data['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                }
                                $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                                $is_success = true;

                                if ($stripe_grand_total > 0) {

                                    if ($paymentMethod == "Stripe") {
                                        $convertAmount = $this->country->convertAmount($stripe_grand_total);
                                        $amount = (INT) ($convertAmount * 100);

                                        $stripe = new Stripe();
                                        $data = [
                                            'payment_method' => $member_card_record['Code'],
                                            'amount' => $amount,
                                            'customer' => $stripe_customer_id,
                                            'payment_method_types' => ['card'],
                                            'currency' => $this->config->item("currency_to"),
                                            'confirmation_method' => 'manual',
                                            'confirm' => true,
                                            'off_session' => true,
                                            'description' => "Invoice #" . $invoice_number
                                        ];


                                        $stripeKey = $this->GetStripeAPIKey();
                                        $intent = $stripe->purchase($stripeKey, $data);

                                        $intentResponse = json_encode($intent);
                                        $intentId = $intentResponse;
                                        $orderStatus = "Processed";
                                        $paymentStatus = "Processed";
                                    } else {


                                        $member_card_record['Code'] = null;
                                        $intentResponse = "Cash";
                                        $intentId = "Cash";
                                        $credit_card_id = null;
                                        $orderStatus = "Processed";
                                        $paymentStatus = "Pending";
                                    }

                                    if (isset($intent->id)) {
                                        $payment_information_data = array(
                                            'InvoiceNumber' => $invoice_number,
                                            'Content' => $intentId,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );

                                        $this->model_database->InsertRecord($this->tbl_payment_informations, $payment_information_data);


                                        $invoice_data['PaymentToken'] = $intentId;
                                        $invoice_data['PaymentReference'] = $member_card_record['Code'];
                                        $invoice_data['OrderStatus'] = $orderStatus;
                                        $invoice_data['PaymentStatus'] = $paymentStatus;
                                        $invoice_data['PaymentMethod'] = $paymentMethod;
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, 'PKInvoiceID');

                                        $insert_invoice_payment_information = array(
                                            'FKInvoiceID' => $id,
                                            'FKCardID' => $card_id,
                                            'Amount' => $stripe_grand_total,
                                            'PaymentReference' => $member_card_record['Code'],
                                            'PaymentToken' => $intentId,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);

                                        if ($invoice_data['InvoiceType'] == "Items") {
                                            if (!$is_discount_found) {
                                                $loyalty_points = intval($grand_total);
                                                $minus_loyalty_points = 0;
                                                $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_data['ID']));
                                                $loyalty_data = array(
                                                    'FKMemberID' => $member_data_array['ID'],
                                                    'FKInvoiceID' => $id,
                                                    'InvoiceNumber' => $invoice_number,
                                                    'GrandTotal' => $grand_total,
                                                    'Points' => $loyalty_points
                                                );
                                                if ($invoice_loyalty_record != false) {
                                                    $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                                    $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                                    $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                                    $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                                } else {
                                                    $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                                    $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                                }
                                                $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                                $total_loyalty_points = intval($member_data_array['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                                $used_loyalty_points = intval($member_data_array['UsedLoyaltyPoints']);
                                                $total_loyalty_points += $loyalty_points;
                                                $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                                if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                                    $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                                    for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                                        $loyalty_code = $this->RandomPassword(6);
                                                        $insert_discount = array(
                                                            'FKMemberID' => $member_data_array['ID'],
                                                            'Code' => $loyalty_code,
                                                            'Worth' => $this->GetLoyaltyCodeAmount(),
                                                            'DType' => "Price",
                                                            'DiscountFor' => "Loyalty",
                                                            'Status' => "Active",
                                                            'CodeUsed' => "One Time",
                                                            'CreatedBy' => 0,
                                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                                        );
                                                        $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                        $used_loyalty_points += $minimum_loyalty_points;
                                                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                                        if ($sms_responder_record != false) {
                                                            $sms_field_array = array(
                                                                'Loyalty Points' => $remain_loyalty_points,
                                                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                                'Currency' => $this->config->item("currency_symbol"),
                                                                'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                                'Discount Code' => $loyalty_code
                                                            );
                                                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                                            $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data['Phone'], 0), $msg_content);
                                                        }
                                                    }
                                                }
                                                $update_member = array(
                                                    'TotalLoyaltyPoints' => $total_loyalty_points,
                                                    'UsedLoyaltyPoints' => $used_loyalty_points,
                                                    'ID' => $member_data['ID']
                                                );
                                                $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                            } else {
                                                if ($is_discount_found) {
                                                    $discount_referral_data_array = explode('|', $discount_referral_data);
                                                    if ($discount_referral_data_array[0] != "Discount") {
                                                        $insert_discount = array(
                                                            'FKMemberID' => $discount_referral_data_array[1],
                                                            'Code' => $this->RandomPassword(6),
                                                            'Worth' => $discount_referral_data_array[3],
                                                            'DType' => $discount_referral_data_array[4],
                                                            'DiscountFor' => "Referral",
                                                            'Status' => "Active",
                                                            'CodeUsed' => "One Time",
                                                            'CreatedBy' => 0,
                                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                                        );
                                                        $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                    }
                                                }
                                            }
                                        }
                                        //$this->SendInvoiceEmail($invoice_number,"C");
                                        $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));
                                        if ($sms_responder_record != false) {
                                            $sms_field_array = array(
                                                'Invoice Number' => $invoice_number,
                                                'Loyalty Points' => $member_data_array['TotalLoyaltyPoints'] - $member_data_array['UsedLoyaltyPoints'],
                                                'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                'Currency' => $this->config->item("currency_symbol"),
                                                'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                                'Pick Date' => date('d M Y', strtotime($pick_date)),
                                                'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                                            );
                                            $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                            $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data_array['Phone'], 0), $msg_content);
                                        }
                                    }
                                }
                                $response_array["result"] = "Success";
                                if ($is_success) {

                                    $description_tookan = '
									';
                                    if (isset($tookan_invoice_service) && !empty($tookan_invoice_service)) {

                                        foreach ($tookan_invoice_service as $keyis => $valueis) {
                                            $description_tookan .= '
											' . ($keyis + 1) . ' # Service : ' . $valueis['Title'] . '
											';
                                            $description_tookan .= '# Quantity : ' . $valueis['Quantity'] . '
											';
                                            $description_tookan .= '# Price : ' . $valueis['Price'] . '
											';
                                            $description_tookan .= '# Total : ' . $valueis['Total'] . '

											';
                                        }
                                    }

                                    if ($invoice_record != false) {
                                        $this->SendInvoiceEmail($invoice_number, "U");
                                        $response_array["message"] = "Invoice Updated Successfully #" . $invoice_number;
                                    } else {
                                        $this->SendInvoiceEmail($invoice_number, "C");
                                        $response_array["message"] = "Invoice Created Successfully #" . $invoice_number;
                                    }
                                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
                                    if ($invoice_record != false) {
                                        $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone", array('PKMemberID' => $invoice_record['FKMemberID']));
                                        $member_address = '';


                                        $member_address = $invoice_record['BuildingName'];

                                        if (!empty($invoice_record['StreetName'])) {
                                            $member_address .= ', ' . $invoice_record['StreetName'];
                                        }


                                        $member_address .= ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];

                                        $pick_time_array = explode("-", $invoice_record['PickupTime']);
                                        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);
                                        $tookan_array = array(
                                            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                            'order_id' => $invoice_record['InvoiceNumber'],
                                            'team_id' => "21339",
                                            'auto_assignment' => 0,
                                            'job_description' => $invoice_record['OrderNotes'] . $description_tookan,
                                            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'job_pickup_email' => $member_record['EmailAddress'],
                                            'job_pickup_address' => $member_address,
                                            'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                                            'customer_email' => $member_record['EmailAddress'],
                                            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'customer_address' => $member_address,
                                            'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                                            'has_pickup' => "1",
                                            'has_delivery' => "1",
                                            'layout_type' => "0",
                                            'timezone' => $this->config->item('tookan_timezone'),
                                            'custom_field_template' => "",
                                            'meta_data' => array(),
                                            'tracking_link' => "1",
                                            //'notify' => "1",
                                            'geofence' => 1
                                        );
                                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                                        $update_invoice_record = array();
                                        $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
                                        $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
                                        if (isset($tooken_response_array['data']['job_id'])) {
                                            $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];
                                            $tookan_response_result = "";
                                            if ($invoice_record['PaymentStatus'] != "Cancel") {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                /* Hassan changes 24-5-2018 */
                                                if (isset($tooken_response_array['data']['delivery_job_id'])) {
                                                    $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                                                    $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                }
                                                /* Hassan changes ends 24-5-2018 */
                                            } else {

                                                /*
                                                  $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                                  $this->deleteDeliveryTask($tooken_response_array);
                                                  $update_invoice_record['TookanResponse'] = null;
                                                 */
                                            }
                                            $update_invoice_record['TookanUpdateResponse'] = $tookan_response_result;
                                        } else {
                                            //if ($invoice_record['PaymentStatus'] == "Completed") {
                                            $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/create_task", json_encode($tookan_array));
                                            $update_invoice_record['TookanResponse'] = $tookan_response_result;
                                            //}
                                        }
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                                    }
                                }
                                //$response_array['member_data'] = $this->_member_data_record($member_data_array['ID']);
                                //$response_array['pending_data'] = $this->_member_invoice_records($member_data_array['ID'], "Pending");
                                //$response_array['processed_data'] = $this->_member_invoice_records($member_data_array['ID'], "Processed");
                            }
                        }
                    } else {
                        $date_records = $this->_date_records($franchise_data_array['ID']);
                        if (sizeof($date_records) > 0) {
                            $response_array["pick_date_data"] = $date_records["pick"];
                            $response_array["delivery_date_data"] = $date_records["delivery"];
                        }
                    }
                }
            }
        }
        $response_array["InvoiceID"] = $id;
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function post_invoice_checkout_paymentStatus() {
        error_reporting(0);
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["type"] = "Message";
        $response_array["message"] = "Required Action : POST";
        $post_data = $this->input->post(NULL, TRUE);
        //header('Content-Type: application/json');
        $log = http_build_query($post_data);
        //file_put_contents('/home/graspdea/public_html/lovetolaundrydev/application/controllers/logs/checkout_'.date("j.n.Y").'_'.time().'.log', $log, FILE_APPEND);

        if ($post_data) {
            $grand_total = floatval(isset($post_data['grand_total']) ? $post_data['grand_total'] : '');
            $card_id = intval(isset($post_data['card_id']) ? $post_data['card_id'] : '');
            $post_code = isset($post_data['post_code']) ? $post_data['post_code'] : '';
            $building_name = isset($post_data['building_name']) ? $post_data['building_name'] : '';
            $street_name = isset($post_data['street_name']) ? $post_data['street_name'] : '';
            $town = isset($post_data['town']) ? $post_data['town'] : '';
            $pick_date = isset($post_data['pick_date']) ? date('Y-m-d', strtotime($post_data['pick_date'])) : '';
            $pick_time = isset($post_data['pick_time']) ? $post_data['pick_time'] : '';
            $delivery_date = isset($post_data['delivery_date']) ? date('Y-m-d', strtotime($post_data['delivery_date'])) : '';
            $delivery_time = isset($post_data['delivery_time']) ? $post_data['delivery_time'] : '';
            $member_data = isset($post_data['member_data']) ? $post_data['member_data'] : '';
            $franchise_data = isset($post_data['franchise_data']) ? $post_data['franchise_data'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            $regularly_cart = isset($post_data['regularly_cart']) ? $post_data['regularly_cart'] : 0;

            if ($grand_total != 0 && $card_id != 0 && !empty($pick_date) && !empty($pick_time) && !empty($delivery_date) && !empty($delivery_time) && !empty($post_code) && !empty($building_name) && !empty($street_name) && !empty($town) && !empty($member_data) && !empty($franchise_data) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $member_data_array = json_decode($member_data, true);
                $franchise_data_array = json_decode($franchise_data, true);
                if ($member_data_array != null && sizeof($member_data_array) > 0 && $franchise_data_array != null && sizeof($franchise_data_array) > 0) {
                    $id = intval(isset($post_data['id']) ? $post_data['id'] : '');
                    $discount_referral_data = isset($post_data['discount_referral_data']) ? $post_data['discount_referral_data'] : '';
                    $preferences_data = isset($post_data['preferences_data']) ? $post_data['preferences_data'] : '';
                    $service_total = floatval(isset($post_data['service_total']) ? $post_data['service_total'] : '');
                    $preference_total = floatval(isset($post_data['preference_total']) ? $post_data['preference_total'] : '');
                    $sub_total = floatval(isset($post_data['sub_total']) ? $post_data['sub_total'] : '');
                    $discount_total = floatval(isset($post_data['discount_total']) ? $post_data['discount_total'] : '');
                    $service_data = isset($post_data['services_data']) ? $post_data['services_data'] : '';
                    $is_valid = true;
                    if ($id == 0) {

                        $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and Date='" . $pick_date . "' and Time='" . $pick_time . "' and (Type='Both' or Type='Pickup')");
                        if ($disable_slot_record != false) {
                            $is_valid = false;
                            $response_array["type"] = "Date Time";
                            $response_array["message"] = "PickUp Date Time not available";
                        } else {
                            if ($franchise_data_array['AllowSameTime'] == "No") {
                                $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $pick_date . "' and PickupTime='" . $pick_time . "') or (DeliveryDate='" . $pick_date . "' and DeliveryTime='" . $pick_time . "')");
                                if ($invoice_record_count > 0) {
                                    $is_valid = false;
                                    $response_array["type"] = "Date Time";
                                    $response_array["message"] = "PickUp Date Time not available";
                                }
                            } else {
                                $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and PickupDate='" . $pick_date . "' and PickupTime='" . $pick_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                                if ($invoice_record_count > 0) {
                                    if ($invoice_record_count >= intval($franchise_data_array['PickupLimit'])) {
                                        $is_valid = false;
                                        $response_array["type"] = "Date Time";
                                        $response_array["message"] = "PickUp Date Time not available";
                                    }
                                }
                            }
                        }
                        if ($is_valid) {
                            $disable_slot_record = $this->model_database->GetRecord($this->tbl_disable_time_slots, "PKTimeID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and Date='" . $delivery_date . "' and Time='" . $delivery_time . "' and (Type='Both' or Type='Delivery')");
                            if ($disable_slot_record != false) {
                                $is_valid = false;
                                $response_array["type"] = "Date Time";
                                $response_array["message"] = "Delivery Date Time not available";
                            } else {
                                if ($franchise_data_array['AllowSameTime'] == "No") {
                                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and IsTestOrder='No' and PaymentStatus='Completed' and (PickupDate='" . $delivery_date . "' and PickupTime='" . $delivery_time . "') or (DeliveryDate='" . $delivery_date . "' and DeliveryTime='" . $delivery_time . "')");
                                    if ($invoice_record_count > 0) {
                                        $is_valid = false;
                                        $response_array["type"] = "Date Time";
                                        $response_array["message"] = "Delivery Date Time not available";
                                    }
                                } else {
                                    $invoice_record_count = $this->model_database->GetRecords($this->tbl_invoices, "C", "PKInvoiceID", "FKFranchiseID=" . $franchise_data_array["ID"] . " and DeliveryDate='" . $delivery_date . "' and DeliveryTime='" . $delivery_time . "' and IsTestOrder='No' and PaymentStatus='Completed'");
                                    if ($invoice_record_count > 0) {
                                        if ($invoice_record_count >= intval($franchise_data_array['DeliveryLimit'])) {
                                            $is_valid = false;
                                            $response_array["type"] = "Date Time";
                                            $response_array["message"] = "Delivery Date Time not available";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    /* Hassan difference of 3 hours pickup 25-5-2018 */
                    if (isset($id)) {

                        $invoice_recordTmp = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,PaymentStatus,GrandTotal,OrderStatus,PickupTime,PickupDate", array('PKInvoiceID' => $id));

                        if (isset($invoice_recordTmp['OrderStatus']) && $invoice_recordTmp['OrderStatus'] == "Pending" || $invoice_recordTmp['OrderStatus'] == "Processed") {

                            $is_edit_button_show = true;
                            $pickup_time_array = explode("-", $invoice_recordTmp['PickupTime']);

                            $date1 = new DateTime(date('Y-m-d', strtotime($invoice_recordTmp['PickupDate'])) . 'T' . $pickup_time_array[0] . ':00');

                            $date2 = new DateTime(date('Y-m-d') . 'T' . date('H:i:s'));

                            // The diff-methods returns a new DateInterval-object...

                            $interval = $date2->diff($date1);

                            $invert = $interval->invert;

                            $hour = $interval->format('%h');

                            if ($invert > 0) {

                                $is_edit_button_show = false;
                            } else {

                                if ($hour <= 3) {

                                    $is_edit_button_show = false;
                                }
                            }
                            if (!$is_edit_button_show) {


                                $is_valid = false;
                                $response_array["message"] = "Dear Customer, You cannot update order, please contact our customer support";
                            }
                        }
                    }
                    /* Hassan difference of 3 hours pickup ends 25-5-2018 */

                    if ($is_valid) {

                        $invoice_number = "";
                        $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, "InvoiceNumber,PaymentStatus,GrandTotal", array('PKInvoiceID' => $id));

                        if ($invoice_record != false) {
                            $invoice_number = $invoice_record['InvoiceNumber'];
                            $response_array['InvoiceNumber'] = $invoice_number;
                            if ($invoice_record["PaymentStatus"] == "Completed") {
                                if (floatval($invoice_record["GrandTotal"]) > $grand_total) {
                                    $is_valid = false;
                                    $response_array["message"] = "Dear Customer, You cannot reduce your order amount from the cart, please contact our customer support";
                                }
                            }
                        }
                        if ($is_valid) {
                            $stripe_grand_total = floatval(0);
                            if ($invoice_record != false) {
                                $invoice_payment_records = $this->model_database->GetRecords($this->tbl_invoice_payment_informations, "R", "Amount", array('FKInvoiceID' => $id));
                                if (sizeof($invoice_payment_records) > 0) {
                                    $payment_information_amount = floatval(0);
                                    foreach ($invoice_payment_records as $rec) {
                                        $payment_information_amount += floatval($rec["Amount"]);
                                    }
                                    $stripe_grand_total = $grand_total - $payment_information_amount;
                                } else {
                                    $stripe_grand_total = $grand_total;
                                }
                            } else {
                                $stripe_grand_total = $grand_total;
                            }
                            $is_test = false;
                            $card_token = "";

                            $invoice_data = array();

                            if ($stripe_grand_total > 0) {



                                /* Stripe payment is successful DO UPDATE INVOICE */

                                $is_test = false;
                                $member_card_record = $this->model_database->GetRecord($this->tbl_member_cards, "Number,Year,Month,Code", array("PKCardID" => $card_id));

                                if ($member_card_record != false) {
                                    if ($member_data_array["ID"] == 1) {
                                        $is_test = true;
                                    }
                                    $card_token_stripe_response = $this->post_stripe_curl_request("https://api.stripe.com/v1/tokens", array('card' => array("number" => encrypt_decrypt("decrypt", $member_card_record['Number']), "exp_month" => $member_card_record['Month'], "exp_year" => $member_card_record['Year'], "cvc" => encrypt_decrypt("decrypt", $member_card_record['Code']))), $this->is_test);
                                    if (!empty($card_token_stripe_response)) {
                                        $card_token_stripe_response_object = json_decode($card_token_stripe_response, true);
                                        if (isset($card_token_stripe_response_object['error'])) {
                                            $response_array["message"] = isset($card_token_stripe_response_object['error']['message']) ? $card_token_stripe_response_object['error']['message'] : 'card information not found';
                                            $is_valid = false;
                                        } else {
                                            $card_token = $card_token_stripe_response_object['id'];
                                        }
                                    } else {
                                        $response_array["message"] = "Invalid Card Information";
                                        $is_valid = false;
                                    }
                                } else {
                                    $response_array["type"] = "Card";
                                    $response_array["message"] = "Card Information not found";
                                    $response_array['card_data'] = $this->_member_card_records($member_data_array["ID"]);
                                    $is_valid = false;
                                }
                                if ($is_valid) {
                                    if (!isset($invoice_number)) {
                                        $invoice_number = "";
                                    }
                                    $card_charge_stripe_response = $this->post_stripe_curl_request("https://api.stripe.com/v1/charges", array("amount" => round($stripe_grand_total * 100), "currency" => $this->config->item("currency_code"), "source" => $card_token, "description" => "Invoice #" . $invoice_number), $this->is_test);
                                    if (!empty($card_charge_stripe_response)) {
                                        $payment_information_data = array(
                                            'InvoiceNumber' => $invoice_number,
                                            'Content' => $card_charge_stripe_response,
                                            'CreatedDateTime' => date('Y-m-d H:i:s')
                                        );
                                        $this->model_database->InsertRecord($this->tbl_payment_informations, $payment_information_data);
                                        $card_charge_stripe_response_object = json_decode($card_charge_stripe_response, true);
                                        if (isset($card_charge_stripe_response_object['error'])) {
                                            $response_array["message"] = isset($card_charge_stripe_response_object['error']['message']) ? $card_charge_stripe_response_object['error']['message'] : 'card information not found';
                                            $response_array["InvoiceID"] = isset($id) ? $id : '';
                                            $is_success = false;
                                        } else {
                                            $invoice_data['OrderStatus'] = "Processed";
                                            $invoice_data['PaymentStatus'] = "Completed";
                                            $invoice_data['PaymentMethod'] = "Stripe";
                                        }
                                    }
                                }
                            }
                            if ($is_valid && isset($invoice_data['PaymentStatus']) && $invoice_data['PaymentStatus'] == "Completed") {
                                if (empty($invoice_number)) {
                                    $invoice_number = $this->model_database->GetInvoiceNumber($this->tbl_invoices, "InvoiceNumber");
                                    $response_array['InvoiceNumber'] = $invoice_number;
                                }
                                $invoice_data = array(
                                    'BuildingName' => $building_name,
                                    'StreetName' => $street_name,
                                    'Town' => $town,
                                    'PostalCode' => $post_code,
                                    'FKDeviceID' => $device_id,
                                    'FKCardID' => $card_id,
                                    'FKMemberID' => $member_data_array["ID"],
                                    'FKFranchiseID' => $franchise_data_array["ID"],
                                    'InvoiceNumber' => $invoice_number,
                                    'PaymentMethod' => 'Stripe',
                                    'AccountNotes' => isset($post_data['account_notes']) ? $post_data['account_notes'] : '',
                                    'AdditionalInstructions' => isset($post_data['additional_instruction']) ? $post_data['additional_instruction'] : '',
                                    'GrandTotal' => $grand_total,
                                    'IPAddress' => isset($post_data['ip_address']) ? $post_data['ip_address'] : '',
                                    'PickupDate' => $pick_date,
                                    'PickupTime' => $pick_time,
                                    'OrderPostFrom' => 'Mobile',
                                    'DeliveryDate' => $delivery_date,
                                    'DeliveryTime' => $delivery_time,
                                    'PaymentStatus' => "Completed",
                                    'OrderStatus' => "Processed",
                                    'PaymentMethod' => "Stripe",
                                    'Regularly' => $regularly_cart
                                );
                                if ($service_total > 0) {
                                    $invoice_data['ServicesTotal'] = $service_total;
                                }
                                if ($preference_total > 0) {
                                    $invoice_data['PreferenceTotal'] = $preference_total;
                                }
                                if ($sub_total > 0) {
                                    $invoice_data['SubTotal'] = $service_total + $preference_total;
                                }
                                if ($discount_total > 0) {
                                    $invoice_data['DiscountTotal'] = $discount_total;
                                }
                                $is_discount_found = false;
                                if (!empty($discount_referral_data)) {
                                    $discount_referral_data_array = explode('|', $discount_referral_data);
                                    if (sizeof($discount_referral_data_array) > 0) {
                                        if ($discount_referral_data_array[0] == "Discount") {
                                            $invoice_data['DiscountType'] = "Discount";
                                            $invoice_data['FKDiscountID'] = $discount_referral_data_array[1];
                                        } else {
                                            $invoice_data['DiscountType'] = "Referral";
                                            $invoice_data['ReferralID'] = $discount_referral_data_array[1];
                                        }
                                        $invoice_data['DiscountCode'] = $discount_referral_data_array[2];
                                        $invoice_data['DiscountWorth'] = $discount_referral_data_array[3];
                                        $invoice_data['DType'] = $discount_referral_data_array[4];
                                        $is_discount_found = true;
                                    }
                                }
                                if (!$is_discount_found) {
                                    $invoice_data['DiscountType'] = "None";
                                    $invoice_data['FKDiscountID'] = null;
                                    $invoice_data['ReferralID'] = null;
                                    $invoice_data['DiscountCode'] = null;
                                    $invoice_data['DiscountWorth'] = null;
                                    $invoice_data['DType'] = "None";
                                }
                                if ($invoice_record == false) {
                                    $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                    $invoice_data['PaymentStatus'] = "Completed";
                                    $invoice_data['OrderStatus'] = "Processed";
                                    $invoice_data['PaymentMethod'] = "Stripe";

                                    $id = $this->model_database->InsertRecord($this->tbl_invoices, $invoice_data);
                                } else {
                                    $this->model_database->RemoveRecord($this->tbl_invoice_preferences, $id, "FKInvoiceID");
                                    $this->model_database->RemoveRecord($this->tbl_invoice_services, $id, "FKInvoiceID");
                                }
                                $invoice_data['ID'] = $id;
                                $response_array["InvoiceID"] = $id;
                                $is_service_found = false;
                                $tookan_invoice_service = array();
                                if (strlen($service_data) > 10) {
                                    $service_data_array = json_decode($service_data, true);
                                    if (sizeof($service_data_array) > 0) {
                                        foreach ($service_data_array as $cart_data) {
                                            $insert_invoice_service = array(
                                                'FKInvoiceID' => $id,
                                                'FKServiceID' => $cart_data['ID'],
                                                'FKCategoryID' => $cart_data['CategoryID'],
                                                'Title' => $cart_data['Title'],
                                                'DesktopImageName' => $cart_data['DesktopImage'],
                                                'MobileImageName' => $cart_data['MobileImage'],
                                                'IsPackage' => $cart_data['IsPackage'],
                                                'PreferencesShow' => $cart_data['PreferencesShow'],
                                                'Quantity' => $cart_data['Quantity'],
                                                'Price' => $cart_data['Price'],
                                                'Total' => $cart_data['Total']
                                            );
                                            $tookan_invoice_service[] = $insert_invoice_service;
                                            $invoice_service_id = $this->model_database->InsertRecord($this->tbl_invoice_services, $insert_invoice_service);
                                            if ($invoice_service_id != 0) {
                                                $is_service_found = true;
                                            }
                                        }
                                    }
                                }
                                if ($is_service_found) {
                                    $invoice_data['InvoiceType'] = "Items";
                                } else {
                                    $invoice_data['InvoiceType'] = "After";
                                }
                                if (strlen($preferences_data) > 10) {
                                    $preferences_data_array = explode("|", $preferences_data);
                                    if (sizeof($preferences_data_array) > 0) {
                                        foreach ($preferences_data_array as $rec) {
                                            $preference_array = explode(",", $rec);
                                            if (sizeof($preference_array) > 0) {
                                                $insert_invoice_preference = array(
                                                    'FKInvoiceID' => $id,
                                                    'FKPreferenceID' => $preference_array[0],
                                                    'ParentTitle' => $preference_array[1],
                                                    'Title' => $preference_array[2]
                                                );
                                                $preference_price = floatval($preference_array[3]);
                                                if ($preference_price > 0) {
                                                    $insert_invoice_preference["Price"] = $preference_price;
                                                    $insert_invoice_preference["Total"] = floatval($preference_array[4]);
                                                }
                                                $this->model_database->InsertRecord($this->tbl_invoice_preferences, $insert_invoice_preference);
                                            }
                                        }
                                    }
                                }
                                if ($invoice_record == false) {
                                    $invoice_data['CreatedDateTime'] = date('Y-m-d H:i:s');
                                } else {
                                    $invoice_data['UpdatedDateTime'] = date('Y-m-d H:i:s');
                                }
                                $this->model_database->UpdateRecord($this->tbl_invoices, $invoice_data, "PKInvoiceID");
                                $is_success = true;

                                $insert_invoice_payment_information = array(
                                    'FKInvoiceID' => $id,
                                    'FKCardID' => $card_id,
                                    'Amount' => $stripe_grand_total,
                                    'PaymentReference' => $card_charge_stripe_response_object['id'],
                                    'PaymentToken' => $card_token,
                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                );
                                $this->model_database->InsertRecord($this->tbl_invoice_payment_informations, $insert_invoice_payment_information);
                                if ($invoice_data['InvoiceType'] == "Items") {
                                    if (!$is_discount_found) {
                                        $loyalty_points = intval($grand_total);
                                        $minus_loyalty_points = 0;
                                        $invoice_loyalty_record = $this->model_database->GetRecord($this->tbl_loyalty_points, "PKLoyaltyID,Points", array('FKInvoiceID' => $invoice_data['ID']));
                                        $loyalty_data = array(
                                            'FKMemberID' => $member_data_array['ID'],
                                            'FKInvoiceID' => $id,
                                            'InvoiceNumber' => $invoice_number,
                                            'GrandTotal' => $grand_total,
                                            'Points' => $loyalty_points
                                        );
                                        if ($invoice_loyalty_record != false) {
                                            $minus_loyalty_points = $invoice_loyalty_record['Points'];
                                            $loyalty_data['UpdatedDateTime'] = date('Y-m-d h:i:s');
                                            $loyalty_data['ID'] = $invoice_loyalty_record['PKLoyaltyID'];
                                            $this->model_database->UpdateRecord($this->tbl_loyalty_points, $loyalty_data, "PKLoyaltyID");
                                        } else {
                                            $loyalty_data['CreatedDateTime'] = date('Y-m-d h:i:s');
                                            $this->model_database->InsertRecord($this->tbl_loyalty_points, $loyalty_data);
                                        }
                                        $minimum_loyalty_points = intval($this->GetMinimumLoyaltyPoints());
                                        $total_loyalty_points = intval($member_data_array['TotalLoyaltyPoints'] - $minus_loyalty_points);
                                        $used_loyalty_points = intval($member_data_array['UsedLoyaltyPoints']);
                                        $total_loyalty_points += $loyalty_points;
                                        $remain_loyalty_points = $total_loyalty_points - $used_loyalty_points;
                                        if ($remain_loyalty_points >= $minimum_loyalty_points) {
                                            $loop_loyalty_points = floor($remain_loyalty_points / $minimum_loyalty_points);
                                            for ($i = 0; $i < $loop_loyalty_points; $i++) {
                                                $loyalty_code = $this->RandomPassword(6);
                                                $insert_discount = array(
                                                    'FKMemberID' => $member_data_array['ID'],
                                                    'Code' => $loyalty_code,
                                                    'Worth' => $this->GetLoyaltyCodeAmount(),
                                                    'DType' => "Price",
                                                    'DiscountFor' => "Loyalty",
                                                    'Status' => "Active",
                                                    'CodeUsed' => "One Time",
                                                    'CreatedBy' => 0,
                                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                                );
                                                $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                                $used_loyalty_points += $minimum_loyalty_points;
                                                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 5, 'Status' => 'Enabled'));
                                                if ($sms_responder_record != false) {
                                                    $sms_field_array = array(
                                                        'Loyalty Points' => $remain_loyalty_points,
                                                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                                        'Currency' => $this->config->item("currency_symbol"),
                                                        'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                                        'Discount Code' => $loyalty_code
                                                    );
                                                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);

                                                    //echo "<pre>";
                                                    //print_r($member_data);

                                                    $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data_array['Phone'], 0), $msg_content);
                                                }
                                            }
                                        }
                                        $update_member = array(
                                            'TotalLoyaltyPoints' => $total_loyalty_points,
                                            'UsedLoyaltyPoints' => $used_loyalty_points,
                                            'ID' => $member_data_array['ID']
                                        );
                                        $this->model_database->UpdateRecord($this->tbl_members, $update_member, 'PKMemberID');
                                    } else {
                                        if ($is_discount_found) {
                                            $discount_referral_data_array = explode('|', $discount_referral_data);
                                            if ($discount_referral_data_array[0] != "Discount") {
                                                $insert_discount = array(
                                                    'FKMemberID' => $discount_referral_data_array[1],
                                                    'Code' => $this->RandomPassword(6),
                                                    'Worth' => $discount_referral_data_array[3],
                                                    'DType' => $discount_referral_data_array[4],
                                                    'DiscountFor' => "Referral",
                                                    'Status' => "Active",
                                                    'CodeUsed' => "One Time",
                                                    'CreatedBy' => 0,
                                                    'CreatedDateTime' => date('Y-m-d H:i:s')
                                                );
                                                $this->model_database->InsertRecord($this->tbl_discounts, $insert_discount);
                                            }
                                        }
                                    }
                                }
                                //$this->SendInvoiceEmail($invoice_number,"C");
                                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('PKResponderID' => 1, 'Status' => 'Enabled'));
                                if ($sms_responder_record != false) {
                                    $sms_field_array = array(
                                        'Invoice Number' => $invoice_number,
                                        'Loyalty Points' => $member_data_array['TotalLoyaltyPoints'] - $member_data_array['UsedLoyaltyPoints'],
                                        'Website Name' => str_replace(" ", "", $this->GetWebsiteName()),
                                        'Currency' => $this->config->item("currency_symbol"),
                                        'Loyalty Points Amount' => $this->GetLoyaltyCodeAmount(),
                                        'Full Day Name' => $this->GetFullDayName(date('w', strtotime($pick_date))),
                                        'Pick Date' => date('d M Y', strtotime($pick_date)),
                                        'Pick Time' => str_replace(" pm", "", str_replace(" am", "", $pick_time))
                                    );
                                    $msg_content = $this->ReplaceTags($sms_responder_record['Content'], $sms_field_array);
                                    $this->SendMessage($sms_responder_record['PKResponderID'], $invoice_number, $sms_responder_record['Title'], $this->config->item('country_phone_code') . ltrim($member_data_array['Phone'], 0), $msg_content);
                                }


                                $response_array["result"] = "Success";
                                if ($is_success) {

                                    $description_tookan = '
										';
                                    if (isset($tookan_invoice_service) && !empty($tookan_invoice_service)) {

                                        foreach ($tookan_invoice_service as $keyis => $valueis) {
                                            $description_tookan .= '
												' . ($keyis + 1) . ' # Service : ' . $valueis['Title'] . '
												';
                                            $description_tookan .= '# Quantity : ' . $valueis['Quantity'] . '
												';
                                            $description_tookan .= '# Price : ' . $valueis['Price'] . '
												';
                                            $description_tookan .= '# Total : ' . $valueis['Total'] . '

												';
                                        }
                                    }

                                    if ($invoice_record != false) {
                                        $this->SendInvoiceEmail($invoice_number, "U");
                                        $response_array["message"] = "Invoice Updated Successfully #" . $invoice_number;
                                    } else {
                                        $this->SendInvoiceEmail($invoice_number, "C");
                                        $response_array["message"] = "Invoice Created Successfully #" . $invoice_number;
                                    }
                                    $invoice_record = $this->model_database->GetRecord($this->tbl_invoices, false, array('InvoiceNumber' => $invoice_number));
                                    if ($invoice_record != false) {
                                        $member_record = $this->model_database->GetRecord($this->tbl_members, "FirstName,LastName,EmailAddress,Phone", array('PKMemberID' => $invoice_record['FKMemberID']));
                                        $member_address = '';
                                        if ($invoice_record['Location'] != "Add" && $invoice_record['Location'] != "0" && $invoice_record['Location'] != null) {
                                            $member_address = $invoice_record['Location'] . ',' . $invoice_record['Locker'];
                                        } else {
                                            $member_address = $invoice_record['BuildingName'] . ', ' . $invoice_record['StreetName'] . ', ' . $invoice_record['PostalCode'] . ', ' . $invoice_record['Town'];
                                        }
                                        $pick_time_array = explode("-", $invoice_record['PickupTime']);
                                        $delivery_time_array = explode("-", $invoice_record['DeliveryTime']);
                                        $tookan_array = array(
                                            'api_key' => '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94',
                                            'order_id' => $invoice_record['InvoiceNumber'],
                                            'team_id' => "21339",
                                            'auto_assignment' => 0,
                                            'job_description' => $invoice_record['OrderNotes'] . $description_tookan,
                                            'job_pickup_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'job_pickup_name' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'job_pickup_email' => $member_record['EmailAddress'],
                                            'job_pickup_address' => $member_address,
                                            'job_pickup_datetime' => date('Y-m-d', strtotime($invoice_record['PickupDate'])) . ' ' . $pick_time_array[1] . ':00',
                                            'customer_email' => $member_record['EmailAddress'],
                                            'customer_username' => $member_record['FirstName'] . ' ' . $member_record['LastName'],
                                            'customer_phone' => tookanPhoneNumber($member_record['Phone'], $this->config->item("country_phone_code")),
                                            'customer_address' => $member_address,
                                            'job_delivery_datetime' => date('Y-m-d', strtotime($invoice_record['DeliveryDate'])) . ' ' . $delivery_time_array[1] . ':00',
                                            'has_pickup' => "1",
                                            'has_delivery' => "1",
                                            'layout_type' => "0",
                                            'timezone' => $this->config->item('tookan_timezone'),
                                            'custom_field_template' => "",
                                            'meta_data' => array(),
                                            'tracking_link' => "1",
                                            //'notify' => "1",
                                            'geofence' => 1
                                        );
                                        $tooken_response_array = json_decode($invoice_record['TookanResponse'], true);
                                        $update_invoice_record = array();
                                        $update_invoice_record['ID'] = $invoice_record['PKInvoiceID'];
                                        $update_invoice_record['InvoiceNumber'] = $invoice_record['InvoiceNumber'];
                                        if (isset($tooken_response_array['data']['job_id'])) {
                                            $tookan_array['job_id'] = $tooken_response_array['data']['job_id'];
                                            $tookan_response_result = "";
                                            if ($invoice_record['PaymentStatus'] == "Completed") {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                /* Hassan changes 24-5-2018 */
                                                if (isset($tooken_response_array['data']['delivery_job_id'])) {
                                                    $tookan_array['job_id'] = $tooken_response_array['data']['delivery_job_id'];
                                                    $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/edit_task", json_encode($tookan_array));
                                                }
                                                /* Hassan changes ends 24-5-2018 */
                                            } else {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
                                                $update_invoice_record['TookanResponse'] = null;
                                                $this->deleteDeliveryTask($tooken_response_array);
                                            }
                                            $update_invoice_record['TookanUpdateResponse'] = $tookan_response_result;
                                        } else {
                                            if ($invoice_record['PaymentStatus'] == "Completed") {
                                                $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/create_task", json_encode($tookan_array));
                                                $update_invoice_record['TookanResponse'] = $tookan_response_result;
                                            }
                                        }
                                        $this->model_database->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
                                    }
                                }
                                $response_array['member_data'] = $this->_member_data_record($member_data_array['ID']);
                                $response_array['pending_data'] = $this->_member_invoice_records($member_data_array['ID'], "Pending");
                                $response_array['processed_data'] = $this->_member_invoice_records($member_data_array['ID'], "Processed");
                            }
                        }
                    } else {
                        $date_records = $this->_date_records($franchise_data_array['ID']);
                        if (sizeof($date_records) > 0) {
                            $response_array["pick_date_data"] = $date_records["pick"];
                            $response_array["delivery_date_data"] = $date_records["delivery"];
                        }
                    }
                }
            }
        }
        $response_array['InvoiceNumber'] = $invoice_number;
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    function post_validate_credit_card() {
        $response_array = array();
        $response_array["result"] = "Error";
        $response_array["message"] = "Required Action : POST";
        $response_array["messageCode"] = "0";
        $post_data = $this->input->post(NULL, TRUE);
        if ($post_data) {
            $number = isset($post_data['number']) ? $post_data['number'] : '';
            $year = isset($post_data['year']) ? $post_data['year'] : '';
            $month = isset($post_data['month']) ? $post_data['month'] : '';
            $code = isset($post_data['code']) ? $post_data['code'] : '';
            $device_id = isset($post_data['device_id']) ? $post_data['device_id'] : '';
            if (!empty($number) && !empty($device_id)) {
                $this->_update_device_information($device_id);
                $is_valid = true;
                $card_token_stripe_response = $this->post_stripe_curl_request("https://api.stripe.com/v1/tokens", array('card' => array("number" => $number, "exp_month" => $month, "exp_year" => $year, "cvc" => $code)), $this->is_test);
                if (!empty($card_token_stripe_response)) {
                    $card_token_stripe_response_object = json_decode($card_token_stripe_response, true);
                    if (isset($card_token_stripe_response_object['error'])) {
                        $response_array["message"] = isset($card_token_stripe_response_object['error']['message']) ? $card_token_stripe_response_object['error']['message'] : 'card information not found';
                        $is_valid = false;
                    } else {
                        $response_array["message"] = "Card Information is valid";
                        $response_array["messageCode"] = "1";
                        $is_valid = true;
                    }
                } else {
                    $response_array["message"] = "Invalid Card Information";
                    $response_array["messageCode"] = "0";
                    $is_valid = false;
                }
            } else {
                $response_array["message"] = "Card and device id are required fields";
                $response_array["messageCode"] = "0";
                $is_valid = false;
            }
        }
        echo json_encode($response_array); // 200 being the HTTP response code
    }

    public function get_pickup_date_records($id) {
        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;
        if ($franchise_record != false) {
            $franchise_date_array['pick'] = array();
            $pick_array_count = 0;

            $closeTimings[] = (date("Y-m-d H:i:s", strtotime($franchise_record["ClosingTime"])));


            //echo $current = strtotime(date("Y-m-d 23:20:30")); 
            $current = date("Y-m-d H:i:s");
            //$current = date("Y-m-d 14:45:00");


            $current = strtotime($current) + ($franchise_record["PickupDifferenceHour"]) * 3600;

            //echo date("Y-m-d H:00:00",$current);
            //die;

            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $id), false, false, false, false, 'asc');

            foreach ($franchise_timings_record as $row) {
                $closeTimings[] = (date("Y-m-d H:i:s", strtotime($row["ClosingTime"])));
            }

            $lastClose = max($closeTimings);


            if ($current > strtotime($lastClose)) { // start with next day
                $i = 1;
            } else {
                $i = 0;
            }

            for (; $i <= 100; $i++) {
                if ($pick_array_count <= 14) {
                    $date = date($this->config->item('query_date_format'), strtotime("+" . $i . ' days'));
                    $date_class = "Disable";
                    /* Hassan IsOffDay check 13-10-2018 */

                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Pickup", $id) == false) {
                            $date_class = "Available";
                            $current_date = date('Y-m-d');
                            if ($current_date == $date) {
                                $current_date_after_add_3_hour = date('Y-m-d', strtotime(date('Y-m-d H:i:s', strtotime('+' . $franchise_record["PickupDifferenceHour"] . ' hours'))));
                                if ($current_date != $current_date_after_add_3_hour) {
                                    $date_class = "Disable";
                                }
                            }
                        }
                    }



                    $day = date("l", strtotime($date));
                    /*
                      if ($day == "Sunday" || $day == "Saturday") {
                      if ($franchise_record["DeliveryOption"] == "Saturday Sunday Both Pickup Delivery To Monday After 3") {
                      // $date_class = "Disable";
                      } elseif ($franchise_record["DeliveryOption"] == "Saturday Pickup Delivery To Monday After 3") {
                      // $date_class = "Disable";
                      }
                      } */

                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );
                    $franchise_date_array['pick'][] = $date_array;
                    $pick_array_count += 1;
                }
                if ($pick_array_count == 14) {
                    break;
                }
            }
        }

        echo json_encode($franchise_date_array);
    }

    public function get_delivery_date_records($id) {


        $franchise_date_array = array();
        $franchise_record = $this->_franchise_record($id);
        $franchise_id = $id;
        $pick_date = isset($_REQUEST['pick_date']) ? $_REQUEST['pick_date'] : '';
        $pick_time_hour = isset($_REQUEST['pick_time_hour']) ? $_REQUEST['pick_time_hour'] : '';

        if ($pick_date == '') {
            echo 'pickup date is required';
            die;
        } elseif ($pick_date != '') {

            $today_dt = strtotime(date('d-m-Y'));
            $expire_dt = strtotime(date('d-m-Y', strtotime($pick_date)));

            if ($expire_dt < $today_dt) {
                echo 'pickup date should be greater than ' . date('d-m-Y');
                die;
            }
        }



        if ($pick_time_hour == 0) {
            $ptime = " 23:59:59";
        } else {
            $ptime = " $pick_time_hour:00:00";
        }
        //$ptime = " $pick_time_hour:00:00";

        $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ ' . $franchise_record['DeliveryDifferenceHour'] . ' hours', strtotime($pick_date . $ptime))); // $now + 3 hours

        $pick_date = date($this->config->item('query_date_format'), strtotime($pick_date));
        $openingTime = $franchise_record["OpeningTime"];
        $closingTime = date("H", strtotime($franchise_record["ClosingTime"]));


        if ($franchise_record != false) {

            $franchise_date_array['delivery'] = array();
            $delivery_array_count = 0;

            $closeTimings[] = (date("Y-m-d H:i:s", strtotime($franchise_record["ClosingTime"])));

            $current = date("Y-m-d H:i:s");
            $current = strtotime($current) + ($franchise_record["DeliveryDifferenceHour"]) * 3600;

            $franchise_timings_record = $this->model_database->GetRecords($this->tbl_franchise_timings, "R", false, array("FKFranchiseID" => $id), false, false, false, false, 'asc');

            foreach ($franchise_timings_record as $row) {
                $closeTimings[] = (date("Y-m-d H:i:s", strtotime($row["ClosingTime"])));
            }

            $lastClose = max($closeTimings);

            if ($current > strtotime($lastClose)) { // start with next day
                $i = 1;
            } else {
                $i = 0;
            }
            //d($i,1);
            for (; $i <= 100; $i++) {

                if ($delivery_array_count <= 20) {
                    $date = date($this->config->item('query_date_format'), strtotime($available_delivery_date));
                    $h = date("H", strtotime($available_delivery_date));
                    $date_class = "Disable";


                    if ($pick_date == $date) {

                        //d($pick_date,1);

                        $closingTime = date("H", strtotime($lastClose));
                        //d($closingTime); 
                        //d($closingTime,1);

                        if ($h >= $closingTime) {
                            $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ 1 days', strtotime($available_delivery_date)));
                            continue;
                        }
                    }

                    /* Hassan IsOffDay check 13-10-2018 */
                    if ($this->IsOffDay($date, $franchise_id) == false) {
                        if ($this->IsDisableDateSlot($date, "Delivery", $id) == false) {
                            $date_class = "Available";
                        }
                    }

                    $day = date("l", strtotime($date));
                    //$day == "Sunday" ||
                    if ($day == "Saturday") {
                        if ($franchise_record["DeliveryOption"] == "Saturday Sunday Both Pickup Delivery To Monday After 3") {
                            $date_class = "Disable";
                        } elseif ($franchise_record["DeliveryOption"] == "Saturday Pickup Delivery To Monday After 3") {
                            $date_class = "Disable";
                        }
                    }

                    if ($date_class == "Available") {
                        if ($pick_date == $date) {
                            if ($franchise_record["AllowSameTime"] == "No") {
                                $date_class = "Disable";
                            }
                        }
                    }


                    $date_array = array(
                        'Date_Class' => $date_class,
                        'Date_Number' => $date,
                        'Date_List' => date('D, d M', strtotime($date)),
                        'Date_Selected' => date('d-F-Y', strtotime($date))
                    );

                    if ($date_class == "Available") {
                        $franchise_date_array['delivery'][] = $date_array;
                    }
                    $delivery_array_count += 1;
                    $available_delivery_date = date("Y-m-d H:i:s", strtotime('+ 1 days', strtotime($available_delivery_date)));
                }
                if ($delivery_array_count == 20) {
                    break;
                }
            }
        }
        echo json_encode($franchise_date_array);
        die;
    }

    function deleteDeliveryTask($tookanArray) {

        $apiKey = '0f6013ecef72377204f75b500944af8e2ddac66e1a359451a44662e5e3304e94';
        $tookan_array = array(
            'api_key' => $apiKey,
            'job_id' => $tookanArray['data']['delivery_job_id']
        );

        $tookan_response_result = $this->tookan_api_post_request("https://api.tookanapp.com/v2/delete_task", json_encode($tookan_array));
    }

    function get_addresses() {

        $this->load->library('loqate');
        $loqate = new CI_Loqate($this->config->item("loqate_key"), $this->config->item("iso_country_code"));
        $post_code = $_GET["post_code"];

        $loqate->getAddresses($post_code);
    }

}
