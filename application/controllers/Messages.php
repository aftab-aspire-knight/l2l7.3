<?php

//$sql = "ALTER TABLE `ws_members`  ADD `FacebookID` VARCHAR(255) NULL DEFAULT NULL  AFTER `StripeCustomerID`,  ADD `GoogleID` VARCHAR(255) NULL DEFAULT NULL  AFTER `FacebookID`";

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages extends Base_Controller {

    private $notification;

    function __construct() {
        parent::__construct();

        $this->load->helper('common');
        $this->load->database();
        $this->load->library('firebasemessaging');
        $this->notification = new Firebasemessaging();
    }

    function abandon_cart() {

        $i = 0;
        $header = 400;
        $post = $this->input->get(NULL, TRUE);
        $hours = 0;
        $till = date("Y-m-d H:i:s", strtotime("-$hours hours"));
        $from = date("Y-m-d H:i:s", strtotime("-30 days"));
        $encrypt = encrypt_decrypt("encrypt", $this->config->item("website_name"));
        $decrypt = encrypt_decrypt("decrypt", $post["key"]);
        if ($decrypt == $this->config->item("website_name")) {

            $sql = "select c.id,device_id,member_id,postal_code,franchise_id,FirstName,LastName,EmailAddress,Platform,PushID from carts c join ws_devices_new d on d.PKDeviceID =c.device_id left join ws_members m on m.PKMemberID =c.member_id where c.device_id is not null and c.created_at between '$from' and '$till' and PushID is not null;";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $ids = array();
            foreach ($data as $d) {
                $ids[] = $d["PushID"];
            }
            
           
            if (!empty($ids)) {

                $sms_responder_record = $this->model_database->GetRecord($this->tbl_sms_responders, "PKResponderID,Title,Content", array('Title' => "Abandoned Cart", 'Status' => 'Enabled'));

                $n = new Firebasemessaging();
                $response = $n->send($ids, $this->config->item("website_name"), $sms_responder_record["Content"]);
            }
        } else {
            $response["error"] = true;
            $response["errors"]["method"] = "Invalid Method.";
        }

        echo jsonencode($response, $header);
        //die("sss");
    }

}
