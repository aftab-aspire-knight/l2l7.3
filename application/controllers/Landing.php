<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing extends Base_Controller {

    function __construct() {
        parent::__construct();
    }

    private function pageData() {
        $current_url = isset($this->uri->segments[1]) ? $this->uri->segments[1] : '';
        $current_second_url = isset($this->uri->segments[2]) ? $this->uri->segments[2] : false;
        $current_third_url = isset($this->uri->segments[3]) ? $this->uri->segments[3] : false;
        $current_url = strtolower($current_url);
        $current_page_url = $current_url;
        if ($current_second_url) {
            $current_second_url = strtolower($current_second_url);
        }
        if ($current_third_url) {
            $current_third_url = strtolower($current_third_url);
        }
        if ($current_second_url) {
            $current_page_url .= '/' . $current_second_url;
            if ($current_third_url) {
                $current_page_url .= '/' . $current_third_url;
            }
        }
        $data = $this->GetPageRecord("AccessURL", $current_page_url);
        return $data;
    }

    public function july10() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['discount_offer'] = "10%";
        $data['new_customer_offer_text'] = "";
        $data['uk_link'] = "https://www.love2laundry.com/offers/july10/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/july10/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/july10/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'JULY10');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function tryme25() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['discount_offer'] = "25%";
        $data['new_customer_offer_text'] = "FOR NEW CUSTOMERS";
        $data['uk_link'] = "https://www.love2laundry.com/offers/tryme25/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'TRYME25');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function apptryme25() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['uk_link'] = "https://www.love2laundry.com/offers/tryme25/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'APP TRYME25');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function appBirminghamtryme25() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['uk_link'] = "https://www.love2laundry.com/offers/tryme25/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'BIRMINGHAM APP TRYME25');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function appManchestertryme25() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['uk_link'] = "https://www.love2laundry.com/offers/tryme25/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'MANCHESTER APP TRYME25');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function scape() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['scape_logo'] = "/assets/images/front/scape-logo.png";
        $data['uk_link'] = "https://www.love2laundry.com/offers/tryme25/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("Title", 'Scape');
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function save10() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['template'] = "offer_template";
        $data['save10_image'] = "/assets/images/front/save10_offers.jpg";
        $data['uk_link'] = "https://www.love2laundry.com/offers/save10/";
        $data['nl_link'] = "https://www.love2laundry.nl/offers/tryme25/";
        $data['ae_link'] = "https://www.love2laundry.ae/offers/tryme25/";

        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 30);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function dryCleaning() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 19);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function laundry() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 25);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function ironing() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 24);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function shoeRepair() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 26);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function shirt() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 27);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function alteration() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 28);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }

    public function stayalert20() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_ppc_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 29);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        $widget_section_record = $this->GetWidgetSectionRecord("PKSectionID", 4);
        if ($widget_section_record != false) {
            $widget_navigation_records = $this->GetWidgetNavigationRecords($widget_section_record['ID'], 3);
            if (sizeof($widget_navigation_records) > 0) {
                $data['widget_work_section_record'] = $widget_section_record;
                $data['widget_work_section_navigation_records'] = $widget_navigation_records;
            }
        }
        $data['ppc_faq_widget'] = $this->GetWidgetRecord("PKWidgetID", 18);
        if ($data['ppc_faq_widget'] == false) {
            unset($data['ppc_faq_widget']);
        }
        $widget_price_section_record = $this->GetWidgetSectionRecord("PKSectionID", 5);
        if ($widget_price_section_record != false) {
            $widget_price_navigation_records = $this->GetWidgetNavigationRecords($widget_price_section_record['ID'], 4);
            if (sizeof($widget_price_navigation_records) > 0) {
                $data['widget_price_section_navigation_records'] = $widget_price_navigation_records;
            }
        }
        $this->show_view_with_menu('pages/ppc', $data);
    }
    public function membership() {
        $this->_URLChecker();
        $data['page_data'] = $this->pageData();
        $data["is_home_page"] = true;
        $data['ppc_header_widget'] = $this->GetWidgetRecord("PKWidgetID", 25);
        if ($data['ppc_header_widget'] == false) {
            unset($data['ppc_header_widget']);
        }
        
        $this->show_view_with_menu('pages/membership', $data);
    }

}
