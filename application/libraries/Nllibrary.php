<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Nllibrary {

    private $ci;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('model_database', 'dm');
        $this->ci->load->config('params');
    }

    public function ValidatePostalCode($postal_code) {
        /*
         * * This function takes in a string, cleans it up, and returns an array with the following information:
         * * ["validate"] => TRUE/FALSE
         * * ["prefix"] => first part of postcode
         * * ["suffix"] => second part of postcode
         */
        $post_code = str_replace(' ', '', $postal_code); // remove any spaces;
        $post_code = strtoupper($post_code); // force to uppercase;
        $valid_postcode_exp = "/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i";
        // set default output results (assuming invalid postcode):
        $output = array();
        $output['validate'] = false;
        $output['prefix'] = '';
        $output['suffix'] = '';
        if (preg_match($valid_postcode_exp, strtoupper($post_code))) {
            $output['validate'] = true;
            $post_code = preg_replace('/\s+/', '', $post_code);
            $suffix = substr($post_code, -2);
            $prefix = str_replace($suffix, '', $post_code);
            $output['prefix'] = preg_replace('/\s+/', '', $prefix);
            $output['suffix'] = preg_replace('/\s+/', '', $suffix);
            $output['franchise'] = $this->ci->dm->GetRecord("ws_franchise_post_codes", "FKFranchiseID", array('Code' => $output['prefix']));
        }

        return $output;
    }

    public function GetLatitudeLongitude($postal_code) {
        return '25.276987,55.296249';
    }
    
    public function getAddresses($postal_code) {
        
        
        $url = $this->ci->config->item("loqate_api_url");
        $key = $this->ci->config->item("loqate_key");
        $origin = $this->ci->config->item("loqate_origin");
        $postal_code_type = $this->ci->config->item("postal_code_type");
        $country=$this->ci->config->item("iso_country_code");;
        $valid = $this->ValidatePostalCode($postal_code);
        $addresses = array();
        
        if($valid["validate"]){
            $url1 = $url . "Capture/Interactive/Find/v1.10/json3.ws?Key=". $key ."&Text=". $postal_code . "&IsMiddleware=false&Countries=".$country."&Limit=10&Language=en&origin=".$origin;
            $res = $this->curl($url1,array(),"", "GET");
            
            foreach($res["Items"] as $item){
                if($item["Type"]=="Postcode"){
                    $url2=$url1."&Container=".$item["Id"];
                    $responseAddress = $this->curl($url2,array(),"", "GET");
                    
                    if(count($responseAddress)>0){
                        foreach($responseAddress["Items"] as $v){
                            $city = trim(str_replace($postal_code,"",$v["Description"]));
                            $addresses[]["address"] = trim($v["Text"]).", ". $postal_code .", ".$city;
                        }
                        
                    }
                }else{
                    
                }
                $response["addresses"] = array_values($addresses);
                $response["city"] = $city;
                $response["error"] = false;
            }
            
            
            
        }else{
            $response["error"] = true;
            $response["message"] = $postal_code_type["validation_message"];
        }
        return $response;
    }

    public function curl($url, $headers, $post, $type = "POST") {
        $curl = curl_init();


        $data = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => $headers,
        );
        //d($data, 1);
        if ($type == "POST") {

            $data[CURLOPT_POST] = 1;
            $data[CURLOPT_POSTFIELDS] = urlencode($post);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, "postvar1=value1&postvar2=value2&postvar3=value3");
        } else {
            $data[CURLOPT_CUSTOMREQUEST] = $type;
        }
        curl_setopt_array($curl, $data);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
            
        if ($err) {
            $response["error"] = true;
        } else {
            $response = json_decode($response, true);
            
        }
        return $response;
    }
    
    public function convertAmount($convert_amount) {
        return $convert_amount;
    }
    
    public function tookaanAddress($address1,$address2,$postalCode,$town) {
        $response = array();
        
        $response["address_tookan"]= $address1. ", ".$address2.", ". ($postalCode) . ", ". ucfirst($town);
        $response["address_description"]= $response["address_tookan"] .$address2;
        return $response;
    }
    
    public function SendMessage($responder_id, $invoice_number, $title, $number, $message, $ip, $originator) {

        try {
            $msg_array = array(
                'USER_NAME' => $this->ci->config->item("sms_api_username"),
                'PASSWORD' => $this->ci->config->item("sms_api_password"),
                'ORIGINATOR' => $originator,
                'RECIPIENT' => $number,
                'ROUTE' => "mglobal",
                'MESSAGE_TEXT' => $message
            );
            $url = "http://app.mobivatebulksms.com/gateway/api/simple/MT?" . http_build_query($msg_array, '', "&");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            $insert_sms_log = array(
                'PKSMSID' => $responder_id,
                'InvoiceNumber' => $invoice_number,
                'Title' => $title,
                'Content' => $message,
                'Phone' => $number,
                'Response' => $res,
                'IPAddress' => $ip,
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $this->ci->dm->InsertRecord("ws_sms_logs", $insert_sms_log);
            $xml = new SimpleXMLElement($res);
            return $xml;
        } catch (Exception $e) {
            return null;
        }
    }
    
    

}

?>