<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Qalibrary {

    private $ci;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('model_database', 'dm');
        //$this->config->load('params');
        //$this->ci->config->item("default_postal_code");
    }

    public function ValidatePostalCode($postal_code) {
        $p = $this->ci->config->item("default_postal_code");
        $output['franchise'] = $this->ci->dm->GetRecord("ws_franchise_post_codes", "FKFranchiseID", array('FKFranchiseID' => $this->ci->config->item("default_franchise_id")));
        $output['validate'] = true;
        $output['prefix'] = $p;
        $output['suffix'] = $p;
        return $output;
    }

    public function GetLatitudeLongitude($postal_code) {
         return '25.3548,51.1839';
    }

    public function convertAmount($convert_amount) {
        return $convert_amount;
    }

    public function tookaanAddress($address1, $address2, $postalCode, $town) {
        $response = array();

        $response["address_tookan"] = $address1 . ", " . $address2 . ", " . ($postalCode) . ", " . ucfirst($town);
        $response["address_description"] = $response["address_tookan"] . $address2;
        return $response;
    }

    public function SendMessage($responder_id, $invoice_number, $title, $number, $message, $ip, $originator) {

        try {
            $msg_array = array(
                'action' => 'sendsms',
                'user' => $this->ci->config->item("sms_api_username"),
                'password' => $this->ci->config->item("sms_api_password"),
                'from' => $originator,
                'to' => $number,
                'text' => $message,
                'scheduledatetime' => date("Y-m-d H:i:s")
            );
            echo $url = "https://api.smsglobal.com/http-api.php?" . http_build_query($msg_array, '', "&");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            $insert_sms_log = array(
                'PKSMSID' => $responder_id,
                'InvoiceNumber' => $invoice_number,
                'Title' => $title,
                'Content' => $message,
                'Phone' => $number,
                'Response' => $res,
                'IPAddress' => $ip,
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            //d($insert_sms_log,1);
            $this->ci->dm->InsertRecord("ws_sms_logs", $insert_sms_log);
            //$xml = new SimpleXMLElement($res);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

}

?>