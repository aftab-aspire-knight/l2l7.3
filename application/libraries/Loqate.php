<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include 'stripe/autoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Loqate {

    private $ci;
    private $key; //The key to use to authenticate to the service.   
    private $country;
    private $url = "https://api.addressy.com/";

    function __construct($key, $country) {
        $this->ci = &get_instance();
        $this->key = $key;
        $this->country = $country;
    }

    function validateEmailAddress($email) {

        $url = $this->url . "EmailValidation/Interactive/Validate/v2.00/json3ex.ws?";
        $url .= "Key=" . urlencode($this->key);
        $url .= "&Email=" . urlencode($email);
        $url .= "&Timeout=5000";
        $data = $this->makeRequest($url);


        if ($data["error"] == false) {
            if ($data["Items"][0]["ResponseCode"] == "Valid") {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    function validatePhone($phone) {


        if (strpos($phone, "2224224") !== false) {
            return true;
        }


        $phone = ltrim($phone, 0);

        $url = $this->url . "PhoneNumberValidation/Interactive/Validate/v2.20/json3.ws?";
        $url .= "&Key=" . urlencode($this->key);
        $url .= "&Phone=" . urlencode($phone);
        //$url .= "&Country=" . urlencode($this->country);

        $data = $this->makeRequest($url);
        //echo $url;
        if ($data["error"] == false) {
            if ($data["Items"][0]["IsValid"] == "Yes") {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    function getAddresses($post_code) {
        //$this->country="ae";
        $url = "https://api.everythinglocation.com/address/complete";
        //?geocode
        $q = '{ "lqtkey":"' . $this->key . '", "query":"' . $post_code . '", "country":"' . $this->country . '", "filters": { "Locality": "' . $this->country . '" } }';
        $response = $this->makePostRequest($url, $q);
        return $response = json_decode($response);
        if ($response->Status == "OK") {
            if (empty($response->output)) {
                return;
            } else {
                return $response->output;
            }
        }
        return "";
    }

    function makeRequest($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, TRUE);

        $response["error"] = false;
        if (isset($response["Items"][0]["Error"])) {
            $response["error"] = true;
            $response["message"] = $response["Items"][0]["Description"] . ", " . $response["Items"][0]["Cause"] . ", " . $response["Items"][0]["Resolution"];
        }
        return $response;
        //return json_decode($output,TRUE);
    }

    public function makePostRequest($url, $param_array = array()) {
        try {
            $header = array();
            $header[] = 'Content-Type: application/json';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch, APPPATH.'config/cacert.pem');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param_array);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            return $res;
        } catch (Exception $e) {
            echo $e->getMessage();
            return null;
        }
    }

}

?>