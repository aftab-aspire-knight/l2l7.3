<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmailSender {

    var $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('model_database');
    }

    public function send_email($from, $to, $subject, $message, $files = false) {
        $this->CI->load->library('email');
        $this->CI->email->set_newline("\r\n");
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['charset'] = 'utf-8';
        $this->CI->email->initialize($config);
        //Sent Email
        $this->CI->email->from($from);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $this->CI->email->message($message);

        if ($files) {
            foreach ($files as $file) {
                $path = './uploads/emailattachments/' . $file['FileName'];
                $this->CI->email->attach($path);
            }
        }
        return $this->CI->email->send();
    }

    public function send_email_responder($responderID, $data, $files = false, $invoice_number = false) { /* 1. Customer ko order invoice ki mail jati hai woh order@love2laundry se jati hai usko no-reply@love2laundry.com karna hai sirf or sirf customer k liya not for admin and franchise 5-5-2018 */ /* User - Contact Us = 2  , User - Invoice 5 and 15, User - Register = 1,User - Business Enquiry=12, */
        if (isset($responderID) && ($responderID == 5 || $responderID == 15 || $responderID == 1)) {
            if (isset($data['Email Sender']) && $data['Email Sender'] != '') {
                $data['Email Sender'] = 'no-reply@love2laundry.com';
            }
        } elseif (isset($responderID) && ($responderID == 2 || $responderID == 12)) {
            if (isset($data['Email Sender']) && $data['Email Sender'] != '') {
                $data['Email Sender'] = 'info@love2laundry.com';
            }
        } /* ends */
        $email_record = $this->CI->model_database->GetRecord("cms_email_responders", 'Title,FromEmail,ToEmail,Subject,Content', array('PKResponderID' => $responderID, 'Status' => 'Enabled'));
        if ($email_record !== false) {
            $email_tags = $this->CI->model_database->GetEmailTags($data);
            $FromEmail = $email_record["FromEmail"];
            $ToEmail = $email_record["ToEmail"];
            $Subject = $email_record["Subject"];
            $Description = $email_record["Content"];
            foreach ($email_tags as $v) {
                $Tag = $v["Tag"];
                $MapField = $v["Title"];
                $FromEmail = str_replace($Tag, $data[$MapField], $FromEmail);
                $ToEmail = str_replace($Tag, $data[$MapField], $ToEmail);
                $Subject = str_replace($Tag, $data[$MapField], $Subject);
                $Description = str_replace($Tag, $data[$MapField], $Description);
                $Description = str_replace(strtoupper($Tag), $data[$MapField], $Description);
                $Description = str_replace(strtolower($Tag), $data[$MapField], $Description);
            }
            $this->CI->load->library('email');
            $this->CI->email->set_newline("\r\n");
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $config['charset'] = 'utf-8';
            $this->CI->email->initialize($config);
            //Sent Email
            $this->CI->email->from($FromEmail);
            $this->CI->email->to($ToEmail);
            $this->CI->email->subject($Subject);
            $this->CI->email->message($Description);

            if ($invoice_number) {
                $this->CI->load->library('pdf');
                $pdf = $this->CI->pdf->load();
                $pdf->WriteHTML($Description); // write the HTML into the PDF
                $pdf->Output(FCPATH . 'uploads/emailattachments/invoice-' . $invoice_number . '.pdf', 'F'); // save to file because we can
                $path = './uploads/emailattachments/invoice-' . $invoice_number . '.pdf';
                $this->CI->email->attach($path);
            } else {
                if ($files) {
                    foreach ($files as $file) {
                        $path = './uploads/emailattachments/' . $file['FileName'];
                        $this->CI->email->attach($path);
                    }
                }
            }
            if ($responderID == 5) {
                $this->CI->email->bcc('efc1b355f5@invite.trustpilot.com');
            }
            $this->CI->email->send();
            $this->CI->email->clear(TRUE);
        }
    }

}
