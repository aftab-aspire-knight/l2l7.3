<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Comlibrary {

    private $ci;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('model_database', 'dm');
        $this->ci->load->config('params');
    }

    public function ValidatePostalCode($postal_code) {

        $postal_code = str_replace(' ', '', $postal_code); // remove any spaces;
        $postal_code = strtoupper($postal_code); // force to uppercase;
        $valid_postcode_exp = "/^(([A-PR-UW-Z]{1}[A-IK-Y]?)([0-9]?[A-HJKS-UW]?[ABEHMNPRVWXY]?|[0-9]?[0-9]?))\s?([0-9]{1}[ABD-HJLNP-UW-Z]{2})$/i";
        // set default output results (assuming invalid postcode):
        $output = array();
        $output['validate'] = false;
        $output['prefix'] = '';
        $output['suffix'] = '';

        if (preg_match($valid_postcode_exp, strtoupper($postal_code))) {
            $output['validate'] = true;
            $postal_code = preg_replace('/\s+/', '', $postal_code);
            $suffix = substr($postal_code, -3);
            $prefix = str_replace($suffix, '', $postal_code);
            $output['prefix'] = preg_replace('/\s+/', '', $prefix);
            $output['suffix'] = preg_replace('/\s+/', '', $suffix);

            $output['franchise'] = $this->ci->dm->GetRecord("ws_franchise_post_codes", "FKFranchiseID", array('Code' => $output['prefix']));
        }

        return $output;
    }

    public function GetLatitudeLongitude($postal_code) {
        $google_map_api_key = $this->ci->config->item("google_map_api_key");

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($postal_code) . "&key=" . $google_map_api_key . "&sensor=false";


        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $suffix = substr($postal_code, -3);

        if (strtolower($result['status']) == "ok") {
            $result1[] = $result['results'][0];
            $result2[] = $result1[0]['geometry'];
            $result3[] = $result2[0]['location'];

            return $result3[0]['lat'] . ',' . $result3[0]['lng'];
        } elseif ($suffix) {
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($suffix) . "&key=" . $google_map_api_key . "&sensor=false";
            $result_string = file_get_contents($url);
            $result = json_decode($result_string, true);
            if (strtolower($result['status']) == "ok") {
                $result1[] = $result['results'][0];
                $result2[] = $result1[0]['geometry'];
                $result3[] = $result2[0]['location'];
                return $result3[0]['lat'] . ',' . $result3[0]['lng'];
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    /*
      public function getAddresses($postal_code) {


      $url = $this->ci->config->item("loqate_api_url");
      $key = $this->ci->config->item("loqate_key");
      $origin = $this->ci->config->item("loqate_origin");
      $postal_code_type = $this->ci->config->item("postal_code_type");
      $country = $this->ci->config->item("iso_country_code");
      $valid = $this->ValidatePostalCode($postal_code);
      $addresses = array();

      if ($valid["validate"]) {
      $url1 = $url . "Capture/Interactive/Find/v1.10/json3.ws?Key=" . $key . "&Text=" . urlencode($postal_code) . "&IsMiddleware=false&Countries=" . $country . "&Limit=10&Language=en&origin=" . $origin;
      $res = $this->curl($url1, array(), "", "GET");

      foreach ($res["Items"] as $item) {
      if ($item["Type"] == "Postcode") {
      $url2 = $url1 . "&Container=" . $item["Id"];
      $responseAddress = $this->curl($url2, array(), "", "GET");


      if (count($responseAddress) > 0) {
      foreach ($responseAddress["Items"] as $v) {
      $city = trim(str_replace($postal_code, "", $v["Description"]));
      $city = trim(str_replace(",", "", $city));
      $addresses[]["address"] = trim($v["Text"]) . ", " . $postal_code . ", " . $city;
      }
      }
      } else {

      }
      $response["addresses"] = array_values($addresses);
      $response["city"] = $city;
      $response["error"] = false;
      }
      } else {
      $response["error"] = true;
      $response["message"] = $postal_code_type["validation_message"];
      }
      return $response;
      } */

    public function getAddresses($postal_code) {

        $addressian_api_key = $this->ci->config->item("addressian_api_key");
        $postal_code_type = $this->ci->config->item("postal_code_type");
        $postal_code = str_replace(" ", "", $postal_code);
        $addressian_api_url = "https://api-full.addressian.co.uk/address/" . $postal_code;
        $headers = array("x-api-key: $addressian_api_key", "Content-Type:application/json");
        $result = $this->curl($addressian_api_url, $headers, array(), "GET");

        $addresses = array();


        $short_post_code_array = $this->ValidatePostalCode($postal_code);
        if ($short_post_code_array['validate'] != false) {


            if ($result["error"] != true && !empty($result)) {

                $i = 0;

                foreach ($result as $r) {
                    $city = $r["citytown"];
                    $addresses[]["address"] = implode(", ", $r["address"]) . ", " . $city;

                    $i++;
                }

                $response["addresses"] = array_values($addresses);
                $response["city"] = $city;
                $response["error"] = false;
            } else {
                $response["error"] = true;
                $response["message"] = $postal_code_type["validation_message"];
            }
        } else {
            $response["error"] = true;
            $response["message"] = $postal_code_type["validation_message"];
        }



        return $response;
    }

    public function curl($url, $headers, $post, $type = "POST") {
        $curl = curl_init();


        $data = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => $headers,
        );
        //d($data, 1);
        if ($type == "POST") {

            $data[CURLOPT_POST] = 1;
            $data[CURLOPT_POSTFIELDS] = urlencode($post);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, "postvar1=value1&postvar2=value2&postvar3=value3");
        } else {
            $data[CURLOPT_CUSTOMREQUEST] = $type;
        }
        curl_setopt_array($curl, $data);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $response["error"] = true;
        } else {
            $response = json_decode($response, true);
        }
        return $response;
    }

    public function convertAmount($convert_amount) {
        return $convert_amount;
    }

    public function tookaanAddress($address1, $address2, $postalCode, $town) {
        $response = array();

        $postalCode = strtolower(str_replace(" ", "", $postalCode));
        $town = strtolower(str_replace(" ", "", $town));

        $splitAddresses = explode(",", $address1);

        $address = "";
        if (isset($splitAddresses[2])) {
            $address .= $splitAddresses[2];
        } else {
            $address .= $splitAddresses[1];
        }

        if (strpos(strtolower(str_replace(" ", "", $address1)), $postalCode) !== false) {
            $member_address = $address1;
        } else {
            $member_address = $address1 . ', ' . $postalCode;
        }

        if (strpos(strtolower(str_replace(" ", "", $address1)), $postalCode) !== false) {
            $member_address = $member_address;
        } else {
            $member_address = $member_address . ', ' . $town;
        }
        if (trim($address2) != "") {
            $address2 = " - " . $address2;
        }

        $response["address_tookan"] = $address . ", " . strtoupper($postalCode) . ", " . ucfirst($town);
        $response["address_description"] = $member_address . $address2;

        return $response;
    }

    public function SendMessage($responder_id, $invoice_number, $title, $number, $message, $ip, $originator) {

        try {
            $msg_array = array(
                'USER_NAME' => $this->ci->config->item("sms_api_username"),
                'PASSWORD' => $this->ci->config->item("sms_api_password"),
                'ORIGINATOR' => $originator,
                'RECIPIENT' => $number,
                'ROUTE' => "mglobal",
                'MESSAGE_TEXT' => $message
            );
            $url = "http://app.mobivatebulksms.com/gateway/api/simple/MT?" . http_build_query($msg_array, '', "&");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            $insert_sms_log = array(
                'PKSMSID' => $responder_id,
                'InvoiceNumber' => $invoice_number,
                'Title' => $title,
                'Content' => $message,
                'Phone' => $number,
                'Response' => $res,
                'IPAddress' => $ip,
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            $this->ci->dm->InsertRecord("ws_sms_logs", $insert_sms_log);
            $xml = new SimpleXMLElement($res);
            return $xml;
        } catch (Exception $e) {
            return null;
        }
    }

    function createTask($invoice, $member) {
        $this->ci->load->library('onfleet');
        $onfleet = new Onfleet();
        $params = $invoice;
        $params["member"] = $member;
        $response = $onfleet->task_create($params);

        $arr = json_decode($response, true);

        $pickup["id"] = $arr["tasks"][0]['id'];
        $delivery["id"] = $arr["tasks"][1]['id'];

        $params = [];
        $params["dependencies"] = [$pickup["id"]];
        $params["id"] = $delivery["id"];
        $onfleet->task_update($params);


        /*
          $update_invoice_record["TookanResponse"] = $response;
          $update_invoice_record["ID"] = $invoice_id;
          $this->ci->dm->UpdateRecord($this->tbl_invoices, $update_invoice_record, "PKInvoiceID");
         */

        $insertData["onfleet_pickup_id"] = $pickup["id"];
        $insertData["onfleet_delivery_id"] = $delivery["id"];
        $insertData["response"] = $response;
        $insertData["invoice_id"] = $invoice["invoice"]["PKInvoiceID"];
        $this->insert($insertData);

        return $response;
    }

    function updateTaskTimings($invoice, $member) {
        $this->ci->load->library('onfleet');
        $onfleet = new Onfleet();
        //$params["invoice"] = $invoice;
        //$params["member"] = $member;
        $country = $this->ci->config->item("onfleet_country");
        $tookanResponse = json_decode($invoice["OnfleetResponse"], true);
        $tasks = $tookanResponse["tasks"];

        if ($invoice["HasAddress"] == 1) {
            $address["unparsed"] = $invoice["BuildingName"];
        } else {
            $address["unparsed"] = $invoice["BuildingName"] . " , " . $invoice["StreetName"] . " , " . $invoice["PostalCode"] . " , " . $invoice["Town"];
        }

        $address["name"] = $invoice["BuildingName"];
        $address["city"] = $invoice["Town"];
        $address["street"] = $invoice["StreetName"];
        $address["postalCode"] = $invoice["PostalCode"];
        $address["country"] = $country;

        $destination["notes"] = (!empty($invoice["OrderNotes"])) ? $invoice["OrderNotes"] : "";
        $destination["address"] = $address;

        if (!empty($invoice["Location"])) {
            $location = explode(",", $invoice["Location"]);
            $destination["location"] = [(float) $location[1], (float) $location[0]];
        }
        //d($tasks,1);
        // d($tasks, 1);

        $pickupDate = $invoice["PickupDate"];
        $deliveryDate = $invoice["DeliveryDate"];

        //$pickupDate = "2020-03-12";
        $pickupTimes = explode("-", $invoice["PickupTime"]);
        $deliveryTimes = explode("-", $invoice["DeliveryTime"]);

        $date = new DateTime($pickupDate . " " . $pickupTimes[0]);
        $current = new DateTime();

        // d($opening_date);
        $params["id"] = $tasks[0]['id'];
        if ($date > $current) {
            $completeAfter = strtotime($pickupDate . " " . $pickupTimes[0] . ":00");
            $completeBefore = strtotime($pickupDate . " " . $pickupTimes[1] . ":00");
            //d($completeBefore);
            $params["completeAfter"] = $completeAfter * 1000;
            $params["completeBefore"] = $completeBefore * 1000;


            //d($tasks[0]);
        }
        $params["destination"] = $destination;
        $response=[];
        //$response = json_decode($onfleet->task_update($params), true);
        $tasks[0] = $response;


        $params = [];
        $date = new DateTime($deliveryDate . " " . $deliveryTimes[0]);


        if ($date > $current) {
            $completeDeliveryAfter = $deliveryDate . " " . $deliveryTimes[0] . ":00";
            $completeDeliveryBefore = $deliveryDate . " " . $deliveryTimes[1] . ":00";

            $completeAfter = strtotime($completeDeliveryAfter);
            $completeBefore = strtotime($completeDeliveryBefore);

            $params["destination"] = $destination;
            $params["completeAfter"] = $completeAfter * 1000;
            $params["completeBefore"] = $completeBefore * 1000;
            $params["id"] = $tasks[1]['id'];

            $response = json_decode($onfleet->task_update($params), true);
            $tasks[1] = $response;
        }

        $return["tasks"] = $tasks;

        $updateResponse = json_encode($return);

        $invoice_data = array();

        $invoice_data["ID"] = $invoice["PKInvoiceID"];

        if (isset($tasks[0]['id']) && isset($tasks[1]['id'])) {
            $invoice_data["OnfleetResponse"] = $updateResponse;
            $invoice_data["OnfleetUpdateResponse"] = "Updated Task Timings";
        } else {
            $invoice_data["OnfleetUpdateResponse"] = "Fail Task Timings :::-> " . $updateResponse;
        }
        $this->update($invoice_data);
        return $updateResponse;
    }

    function cancelTask($invoice) {
        $this->ci->load->library('onfleet');

        $onfleet = new Onfleet();

        $tookanResponse = json_decode($invoice["OnfleetResponse"], true);
        $tasks = $tookanResponse["tasks"];
        $pickup["id"] = $tasks[0]['id'];
        $onfleet->task_delete($pickup);

        $delivery["id"] = $tasks[1]['id'];
        $onfleet->task_delete($delivery);

        $this->ci->dm->RemoveRecord("onfleet", $invoice["PKInvoiceID"], "invoice_id");

        return "";
        //task_delete
    }

    function updateAdminTask($services, $invoice, $member) {




        $currency = $this->ci->config->item("currency_symbol");
        $tookanResponse = json_decode($invoice["OnfleetResponse"], true);
        $tasks = $tookanResponse["tasks"];

        $pickupDate = $invoice["PickupDate"];
        $deliveryDate = $invoice["DeliveryDate"];

        $pickupTimes = explode("-", $invoice["PickupTime"]);
        $deliveryTimes = explode("-", $invoice["DeliveryTime"]);


        $date = new DateTime($pickupDate . " " . $pickupTimes[0]);
        $current = new DateTime();

        $params["id"] = $tasks[0]['id'];

        if ($date > $current) {
            $completeAfter = strtotime($pickupDate . " " . $pickupTimes[0] . ":00");
            $completeBefore = strtotime($pickupDate . " " . $pickupTimes[1] . ":00");
            //d($completeBefore);
            $params["completeAfter"] = $completeAfter * 1000;
            $params["completeBefore"] = $completeBefore * 1000;
            //$tasks[0] = $response;
            //d($tasks[0]);
        }

        if ($invoice["HasAddress"] == 1) {
            $address["unparsed"] = $invoice["BuildingName"];
        } else {
            $address["unparsed"] = $invoice["BuildingName"] . " , " . $invoice["StreetName"] . " , " . $invoice["PostalCode"] . " , " . $invoice["Town"];
        }

        $country = $this->ci->config->item("onfleet_country");
        $address["name"] = $invoice["BuildingName"];
        $address["city"] = $invoice["Town"];
        $address["street"] = $invoice["StreetName"];
        $address["postalCode"] = $invoice["PostalCode"];
        $address["country"] = $country;

        $destination["notes"] = (!empty($invoice["OrderNotes"])) ? $invoice["OrderNotes"] : "";
        $destination["address"] = $address;

        $recipients["name"] = $member["FirstName"] . " " . $member["LastName"];
        $recipients["email"] = $member["EmailAddress"];
        $recipients["phone"] = tookanPhoneNumber($member['Phone'], $this->phone_code);
        $recipients["skipPhoneNumberValidation"] = true;

        if (!empty($invoice["Location"])) {
            $location = explode(",", $invoice["Location"]);
            $destination["location"] = [(float) number_format($location[1], 7), (float) number_format($location[0], 7)];
        }

        //d($destination, 1);

        $params["destination"] = $destination;
        $params["recipients"][0] = $recipients;
        $params["pickupTask"] = true;
        //$params["autoAssign"]["state"] = 0;

        $quantity = count($services);
        //d($services,1);
        $description = $this->getServiceString($services);

        $params["notes"] = $description;
        $params["quantity"] = $quantity;

        $tasks[0] = $this->updateRequest($params);
        $request["pickup"] = $params;
        $request["pickup"]["response"] = $tasks[0];
        $params = [];
        $params["destination"] = $destination;
        $params["recipients"][0] = $recipients;
        $params["pickupTask"] = false;
        $params["notes"] = $description;
        $params["quantity"] = $quantity;

        $date = new DateTime($deliveryDate . " " . $deliveryTimes[0]);
        $current = new DateTime();
        $params["id"] = $tasks[1]['id'];


        if ($date > $current) {
            $completeAfter = strtotime($deliveryDate . " " . $deliveryTimes[0] . ":00");
            $completeBefore = strtotime($deliveryDate . " " . $deliveryTimes[1] . ":00");
            $params["completeAfter"] = $completeAfter * 1000;
            $params["completeBefore"] = $completeBefore * 1000;
        }

        $tasks[1] = $this->updateRequest($params);

        $return["tasks"] = $tasks;
        $updateResponse = json_encode($return);
        $request["delivery"] = $params;
        $request["delivery"]["response"] = $updateResponse;



        $invoice_data = array();

        $invoice_data["ID"] = $invoice["PKInvoiceID"];

        if (isset($tasks[0]['id']) && isset($tasks[1]['id'])) {
            $invoice_data["OnfleetResponse"] = $updateResponse;
            $invoice_data["OnfleetUpdateResponse"] = "Updated Admin ";
        } else {
            $invoice_data["OnfleetUpdateResponse"] = "Fail Admin :::-> " . $updateResponse . " :::-> " . json_encode($request);
        }

        $this->update($invoice_data);

        return $updateResponse;
    }

    private function updateRequest($params) {

        $this->ci->load->library('onfleet');
        $onfleet = new Onfleet();
        return $response = json_decode($onfleet->task_update($params), true);
    }

    private function getServiceString($services) {

        $currency = $this->ci->config->item("currency_symbol");
        $description = "";
        //d($services, 1);
        if (!empty($services)) {
            $k = 0;
            foreach ($services as $valueis) {
                $description .= '' . ($k + 1) . '. Service : ' . $valueis['title'] . '
									 ';
                $description .= 'Quantity : ' . $valueis['quantity'] . '
									 ';
                $description .= 'Price : ' . $currency . numberformat($valueis['price']) . '
									 ';
                $description .= 'Total : ' . $currency . numberformat($valueis['total']) . '
									
									 ';
                $k++;
            }
        }
        return $description;
    }

    function updateTaskServices($services, $invoice, $member) {

        $this->ci->load->library('onfleet');
        $currency = $this->ci->config->item("currency_symbol");

        $onfleet = new Onfleet();
        //$params["invoice"] = $invoice;
        //$params["member"] = $member;

        $invoice_id = $invoice["PKInvoiceID"];
        $invoice_number = $invoice["InvoiceNumber"];

        $tookanResponse = json_decode($invoice["OnfleetResponse"], true);
        $tasks = $tookanResponse["tasks"];

        $quantity = 0;
        $description = "";
        //d($services, 1);
        if (!empty($services)) {
            $quantity = count($services);
            $k = 0;
            foreach ($services as $valueis) {
                $description .= '' . ($k + 1) . '. Service : ' . $valueis['title'] . '
									 ';
                $description .= 'Quantity : ' . $valueis['quantity'] . '
									 ';
                $description .= 'Price : ' . $currency . numberformat($valueis['price']) . '
									 ';
                $description .= 'Total : ' . $currency . numberformat($valueis['total']) . '
									
									 ';
                $k++;
            }
        }

        $params["id"] = $tasks[0]['id'];
        $params["notes"] = $description;
        $params["quantity"] = $quantity;

        $response = json_decode($onfleet->task_update($params), true);
        $tasks[0] = $response;

        $params["id"] = $tasks[1]['id'];
        $params["notes"] = $description;
        $params["quantity"] = $quantity;
        $response = json_decode($onfleet->task_update($params), true);
        $tasks[1] = $response;
        $return["tasks"] = $tasks;
        $updateResponse = json_encode($return);

        $invoice_data["ID"] = $invoice_id;

        if (isset($tasks[0]['id']) && isset($tasks[1]['id'])) {
            $invoice_data["OnfleetResponse"] = $updateResponse;
            $invoice_data["OnfleetUpdateResponse"] = "Updated sdes Services";
        } else {
            $invoice_data["OnfleetUpdateResponse"] = "Fail services :::-> " . $updateResponse;
        }

        $this->update($invoice_data);

        return $updateResponse;
    }

    function insert($data) {

        $data['created_at'] = date('Y-m-d H:i:s');
        $this->ci->dm->InsertRecord("onfleet", $data);
    }

    function update($data) {
        $this->ci->dm->UpdateRecord("ws_invoices", $data, "PKInvoiceID");
    }

}

?>