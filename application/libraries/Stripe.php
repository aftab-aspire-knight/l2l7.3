<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
include 'stripe/autoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Stripe {

    function __construct() {
        $this->ci = & get_instance();
    }

    function saveCustomer($key, $customer) {

        \Stripe\Stripe::setApiKey($key);
        $customer = \Stripe\Customer::create($customer);
        return $customer;
    }

    function saveSetupIntent($key, $data) {
        \Stripe\Stripe::setApiKey($key);

        try {
            $paymentMethod = \Stripe\SetupIntent::create($data);
            $response = $paymentMethod;
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\UnknownApiErrorException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function createCard($key, $data) {

        $response = array();
        $response['error'] = true;
        \Stripe\Stripe::setApiKey($key);
        try {
            $paymentMethod = \Stripe\PaymentMethod::create([
                        'type' => 'card',
                        'card' => $data
            ]);
            $response = $paymentMethod;
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function purchase($key, $data) {

        $response = array();
        $response['error'] = true;
        \Stripe\Stripe::setApiKey($key);

        try {

            $intent = \Stripe\PaymentIntent::create($data);
            $response = $intent;
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function updatePurchase($key, $id, $data) {

        $response = array();
        $response['error'] = true;
        \Stripe\Stripe::setApiKey($key);

        try {

            $intent = \Stripe\PaymentIntent::update($id, $data);
            $response = $intent;
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function refund($key, $id, $amount) {

        $response = array();
        $response['error'] = true;
        \Stripe\Stripe::setApiKey($key);

        $amount = $amount + 0;
        $amount = $amount * 100;
        $amount = str_replace(".", "", $amount);
        $amount = (int) $amount;

        try {

            $intent = \Stripe\PaymentIntent::retrieve($id);
            $response = \Stripe\Refund::create([
                        'charge' => $intent->charges->data[0]->id,
                        'amount' => $amount,
            ]);
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\UnexpectedValueException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function getRefunds($key, $id) {

        $response = array();
        $response['error'] = true;
        \Stripe\Stripe::setApiKey($key);

        try {
            $intent = \Stripe\PaymentIntent::retrieve($id);
            $response = $intent;
        } catch (Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function getPaymentMethod($key, $id) {

        \Stripe\Stripe::setApiKey($key);
        $paymentMethod = \Stripe\PaymentMethod::retrieve($id);
        if (empty($paymentMethod)) {
            return false;
        } else {
            return $paymentMethod;
        }
    }

    function retrieve($customer_id) {

        $customer = \Stripe\Customer::retrieve($customer_id);
        if (empty($customer)) {
            return false;
        } else {
            return $customer;
        }
    }

    public static function generatePaymentResponse($intent) {
        if ($intent->status == 'requires_source_action' &&
                $intent->next_action->type == 'use_stripe_sdk') {
            # Tell the client to handle the action
            echo json_encode([
                'requires_action' => true,
                'payment_intent_client_secret' => $intent->client_secret
            ]);
        } else if ($intent->status == 'succeeded') {
            # The payment didn’t need any additional actions and completed!
            # Handle post-payment fulfillment
            echo json_encode([
                'success' => true
            ]);
        } else {
            # Invalid status
            http_response_code(500);
            echo json_encode(['error' => 'Invalid PaymentIntent status']);
        }
    }

}

?>