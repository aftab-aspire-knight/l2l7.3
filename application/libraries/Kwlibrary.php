<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Kwlibrary {

    private $ci;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('model_database', 'dm');
        //$this->config->load('params');
        //$this->ci->config->item("default_postal_code");
    }

    public function ValidatePostalCode($postal_code) {
        $p = $this->ci->config->item("default_postal_code");
        $output['franchise'] = $this->ci->dm->GetRecord("ws_franchise_post_codes", "FKFranchiseID", array('FKFranchiseID' => $this->ci->config->item("default_franchise_id")));
        $output['validate'] = true;
        $output['prefix'] = $p;
        $output['suffix'] = $p;
        return $output;
    }

    public function GetLatitudeLongitude($postal_code) {
        return '25.276987,55.296249';
    }
    
    public function convertAmount($convert_amount) {
        
        $currency_code = $this->ci->config->item("currency_code");
        $currency_to = $this->ci->config->item("currency_to");
        
        $currency_from = urlencode($currency_code);
	$currency_to = urlencode($currency_to);	
	$currencyUrl = "https://www.google.com/search?q=".$currency_code."+to+".$currency_to;
        $currencyDetails = file_get_contents($currencyUrl);
        
	$currencyData = preg_split('/\D\s(.*?)\s=\s/',$currencyDetails);
        $conversion_rate = (float) substr($currencyData[1],0,7);	
	$total_converted_currency_amount = ($convert_amount) * ($conversion_rate);
	$currencyJsonData = array( 'rate' => $conversion_rate, 'total_converted_currency_amount' =>$total_converted_currency_amount, 'currency_from' => strtoupper($currency_from), 'currency_to' => strtoupper($currency_to));
        return $total_converted_currency_amount-.02;
    }
    
    
    public function tookaanAddress($address1,$address2,$postalCode,$town) {
        $response = array();
        
        $response["address_tookan"]= $address1. ", ".$address2.", ". ($postalCode) . ", ". ucfirst($town);
        $response["address_description"]= $response["address_tookan"] .$address2;
        return $response;
    }
    
    
    
}

?>