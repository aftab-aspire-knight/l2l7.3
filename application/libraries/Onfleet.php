<?php

/*
 * Onfleet API wrapper (CodeIgniter Library)
 *
 * Onfleet's application programming interface (API) provides the communication link
 * between your application and Onfleet API.
 *
 * @author   Billy Bateman <billy@codekush.com>
 * @license  http://www.opensource.org/licenses/bsd-license.php New BSD license
 * @link     https://github.com/billybateman/Onfleet-PHP
 * @date     2015-07-07
 */

class Onfleet {
    /*
     * API URL
     */

    const http_api_url = 'https://onfleet.com/api/v2/';

    /**
     * Base settings
     */
    private $_api_key = '';
    private $_api_name = '';
    protected $params = array();
    protected $url;
    protected $country;
    protected $currency;
    protected $phone_code;

    function __construct() {
        $this->ci = &get_instance();

        $key = $this->ci->config->item("onfleet_api_key");
        $name = $this->ci->config->item("onfleet_app_name");
        $country = $this->ci->config->item("onfleet_country");
        $currency = $this->ci->config->item("currency_symbol");
        $country_phone_code = $this->ci->config->item("country_phone_code");

        $this->_api_key = $key;
        $this->_api_name = $name;
        $this->country = $country;
        $this->currency = $currency;
        $this->phone_code = $country_phone_code;
    }

    /**
     * Create task
     *
     * $params array
     *
     * @return object
     */
    public function task_create($params = array()) {

        $sample = ["tasks" => [
                ["destination" => ["address" => ["unparsed" => "2829 Vallejo St, SF, CA, USA"], "notes" => "Small green door by garage door has pin pad, enter *4821*"], "recipients" => [["name" => "Blas Silkovich", "phone" => "650-555-4481", "notes" => "Knows Neiman, VIP status."]], "notes" => "Order 332 =>   24oz Stumptown Finca El Puente, 10 x Aji de Gallina Empanadas, 13-inch Lelenitas Tres Leches"],
                ["destination" => ["address" => ["number" => "1264", "street" => "W. Augusta Blvd. ", "apartment" => "", "city" => "Chicago", "state" => "IL", "country" => "USA"]], "notes" => "12 x 2016 Getariako Txakolina (Rosé)", "pickupTask" => true, "metadata" => [["name" => "caseId", "type" => "number", "value" => 33162]], "recipients" => []],
                ["destination" => ["address" => ["number" => "420", "street" => "Infinite Loop", "city" => "Redmond", "state" => "WA", "country" => "USA"]], "recipients" => [["name" => "S.N.", "phone" => "206-341-8826"]]]]];

        //d($sample);

        $invoice = $params["invoice"];

        $metaData = [["name" => "Number", "type" => "number", "visibility" => ["api", "dashboard", "worker"], "value" => (int) $invoice["InvoiceNumber"]], ["name" => "id", "type" => "number", "visibility" => ["api", "dashboard", "worker"], "value" => (int) $invoice["PKInvoiceID"]]];


        $tasks = [];



        $services = !empty($params["invoice_services"]) ? $params["invoice_services"] : [];

        $member = $params["member"];
        $quantity = 0;
        $description = "";

        if (!empty($services)) {
            $quantity = count($services);
            $k = 0;
            foreach ($services as $valueis) {
                $description .= '' . ($k + 1) . '. Service : ' . $valueis['Title'] . '
									 ';
                $description .= 'Quantity : ' . $valueis['Quantity'] . '
									 ';
                $description .= 'Price : ' . $this->currency . $valueis['Price'] . '
									 ';
                $description .= 'Total : ' . $this->currency . $valueis['Total'] . '
									
									 ';
                $k++;
            }
        }

        if ($invoice["HasAddress"] == 1) {
            $address["unparsed"] = $invoice["BuildingName"];
        } else {
            $address["unparsed"] = $invoice["BuildingName"] . " , " . $invoice["StreetName"] . " , " . $invoice["PostalCode"] . " , " . $invoice["Town"];
        }




        $address["name"] = $invoice["BuildingName"];
        $address["city"] = $invoice["Town"];
        $address["street"] = $invoice["StreetName"];
        $address["postalCode"] = $invoice["PostalCode"];
        $address["country"] = $this->country;

        $destination["notes"] = (!empty($invoice["OrderNotes"])) ? $invoice["OrderNotes"] : "";
        $destination["address"] = $address;
        $recipients["name"] = $member["FirstName"] . " " . $member["LastName"];
        $recipients["email"] = $member["EmailAddress"];
        $recipients["phone"] = tookanPhoneNumber($member['Phone'], $this->phone_code);

        $recipients["skipPhoneNumberValidation"] = true;

        $location = [];

        if (!empty($invoice["Location"])) {
            $location = explode(",", $invoice["Location"]);
        }

        $destination["location"] = [(float) $location[1], (float) $location[0]];
        $tasks[0]["destination"] = $destination;
        $tasks[0]["recipients"][0] = $recipients;
        $tasks[0]["pickupTask"] = true;
        $tasks[0]["autoAssign"]["state"] = 0;
        $tasks[0]["notes"] = $description;
        $tasks[0]["quantity"] = $quantity;
        $tasks[0]["metadata"] = $metaData;

        $pickupDate = $invoice["PickupDate"];
        $deliveryDate = $invoice["DeliveryDate"];
        //$pickupTime = $invoice["PickupTime"];

        $pickupTimes = explode("-", $invoice["PickupTime"]);
        $deliveryTimes = explode("-", $invoice["DeliveryTime"]);

        $tasks[0]["completeAfter"] = strtotime($pickupDate . " " . $pickupTimes[0]) * 1000;
        $tasks[0]["completeBefore"] = strtotime($pickupDate . " " . $pickupTimes[1]) * 1000;

        $tasks[1]["destination"] = $destination;
        $tasks[1]["recipients"][0] = $recipients;
        $tasks[1]["completeAfter"] = strtotime($deliveryDate . " " . $deliveryTimes[0]) * 1000;
        $tasks[1]["completeBefore"] = strtotime($deliveryDate . " " . $deliveryTimes[1]) * 1000;
        $tasks[1]["pickupTask"] = false;
        $tasks[1]["autoAssign"]["state"] = 0;
        $tasks[1]["notes"] = $description;
        $tasks[1]["quantity"] = $quantity;
        $tasks[1]["metadata"] = $metaData;

        $post["tasks"] = $tasks;

        return $this->request('post', self::http_api_url . 'tasks/batch', $this->_api_key, $post);
    }

    public function tasks($params = array()) {
        return $this->request('get', self::http_api_url . 'tasks', $this->_api_key, $params);
    }

    /**
     * Task details
     *
     * $params array
     *
     * @return object
     */
    public function task_details($params = array()) {
        $id = $params['id'];

        return $this->request('get', self::http_api_url . 'tasks/' . $id, $this->_api_key);
    }

    /**
     * Update task
     *
     * $params array
     *
     * @return object
     */
    public function task_update($params = array()) {
        $id = $params['id'];
        //d($params,1);
        return $this->request('put', self::http_api_url . 'tasks/' . $id, $this->_api_key, $params);
    }

    /**
     * Delete task
     *
     * $params array
     *
     * @return object
     */
    public function task_delete($params = array()) {
        $id = $params['id'];

        return $this->request('delete', self::http_api_url . 'tasks/' . $id, $this->_api_key);
    }

    /**
     * List webhooks
     *
     * @return object
     */
    public function webhooks() {
        return $this->request('get', self::http_api_url . 'webhooks', $this->_api_key);
    }

    /**
     * Create webhook
     *
     * $params array
     * $params[url'] string
     * $params['trigger'] integer
     *
     * @return object
     */
    public function webhook_create($params = array()) {
        return $this->request('post', self::http_api_url . 'webhooks', $this->_api_key, $params);
    }

    /**
     * Delete webhook
     *
     * $params array
     *
     * @return object
     */
    public function webhook_delete($params = array()) {
        $id = $params['id'];

        return $this->request('delete', self::http_api_url . 'webhooks/' . $id, $this->_api_key);
    }

    /**
     * request data
     * Connect to API URL
     *
     * @param array
     * return string
     */
    protected function request($method, $url, $api_key, $params = array()) {
        $params = json_encode($params);
        //print_r($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        if ($method == 'put') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        if ($method == "delete") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$api_key:");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return $output;
    }

}
