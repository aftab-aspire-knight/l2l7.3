<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Firebasemessaging {

    private $ci;
    private $key;
    private $url = 'https://fcm.googleapis.com/fcm/send';

    function __construct() {
        $this->ci = & get_instance();
        $this->ci->load->config('params');
        $this->key = $this->ci->config->item("firbase_server_key");
        //$this->ci->load->database();
    }

    function send($to, $title = "Love2Laundry", $message) {

        $result = array();
        $apiKey = $this->key;

        // Compile headers in one variable
        $headers = array(
            'Authorization:key=' . $apiKey,
            'Content-Type:application/json'
        );


        $fields = array(
            'notification' => array(
                'title' => $title,
                'body' => $message,
                'sound' => 'default',
                "icon" => "ic_launcher",
                "dry_run" => true,
                'click_action' => "FCM_PLUGIN_ACTIVITY"
            )
        );

        if (!empty($to) && is_array($to)) {
            $fields["to"] = $to;
        } else {
           $fields["to"] = $to;
        }
        //d($fields,1);

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
