<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * $this->load->library('loqate');
  $loqate = new CI_Loqate($this->config->item("loqate_key"),$this->config->item("iso_country_code"));
  $data=$loqate->validatePhone($this->config->item("country_phone_code")."7528471411");
 */

class Bhlibrary {

    private $ci;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('model_database', 'dm');
        //$this->config->load('params');
        //$this->ci->config->item("default_postal_code");
    }

    public function ValidatePostalCode($postal_code) {
        $p = $this->ci->config->item("default_postal_code");
        $output['franchise'] = $this->ci->dm->GetRecord("ws_franchise_post_codes", "FKFranchiseID", array('FKFranchiseID' => $this->ci->config->item("default_franchise_id")));
        $output['validate'] = true;
        $output['prefix'] = $p;
        $output['suffix'] = $p;
        return $output;
    }

    public function GetLatitudeLongitude($postal_code) {
        return '25.276987,55.296249';
    }

    public function convertAmount($convert_amount) {

        $currency_code = $this->ci->config->item("currency_code");
        $currency_to = $this->ci->config->item("currency_to");

        $currency_from = urlencode($currency_code);
        $currency_to = urlencode($currency_to);
        $currencyUrl = "https://www.google.com/search?q=" . $currency_code . "+to+" . $currency_to;
        $currencyDetails = file_get_contents($currencyUrl);

        $currencyData = preg_split('/\D\s(.*?)\s=\s/', $currencyDetails);
        $conversion_rate = (float) substr($currencyData[1], 0, 7);
        $total_converted_currency_amount = ($convert_amount) * ($conversion_rate);
        $currencyJsonData = array('rate' => $conversion_rate, 'total_converted_currency_amount' => $total_converted_currency_amount, 'currency_from' => strtoupper($currency_from), 'currency_to' => strtoupper($currency_to));
        return $total_converted_currency_amount - .02;
    }

    public function tookaanAddress($address1, $address2, $postalCode, $town) {
        $response = array();

        $response["address_tookan"] = $address1 . ", " . $address2 . ", " . ($postalCode) . ", " . ucfirst($town);
        $response["address_description"] = $response["address_tookan"] . $address2;
        return $response;
    }

    public function SendMessage($responder_id, $invoice_number, $title, $number, $message, $ip, $originator) {

        try {
            $msg_array = array(
                'action' => 'sendsms',
                'user' => $this->ci->config->item("sms_api_username"),
                'password' => $this->ci->config->item("sms_api_password"),
                'from' => $originator,
                'to' => $number,
                'text' => $message,
                'scheduledatetime' => date("Y-m-d H:i:s")
            );
            echo $url = "https://api.smsglobal.com/http-api.php?" . http_build_query($msg_array, '', "&");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);
            $error = curl_error($ch);
            if (!empty($error)) {
                $res = $error;
            }
            curl_close($ch);
            $insert_sms_log = array(
                'PKSMSID' => $responder_id,
                'InvoiceNumber' => $invoice_number,
                'Title' => $title,
                'Content' => $message,
                'Phone' => $number,
                'Response' => $res,
                'IPAddress' => $ip,
                'CreatedDateTime' => date('Y-m-d H:i:s')
            );
            //d($insert_sms_log,1);
            $this->ci->dm->InsertRecord("ws_sms_logs", $insert_sms_log);
            //$xml = new SimpleXMLElement($res);
            return $res;
        } catch (Exception $e) {
            return null;
        }
    }

}

?>