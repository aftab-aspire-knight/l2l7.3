<div class="content">
    <?php echo show_admin_bread_crumbs('Settings', base_url('admin/dashboard.html'), isset($parent_page_name)?$parent_page_name:'Configurations')?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Configurations</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <div class="col-md-12 col-xs-12 col-lg-12">
                            <form class="form-no-horizontal-spacing" id="frm_configuration" action="<?php echo site_url('admin/configurations/clear')?>" method="post">
                                <div class="form-group">
                                    <label class="form-label">Clear Cache </label>
                                    <div class="controls">
                                        <button class="btn btn-primary btn-cons" type="button" id="btn_empty">Clear</button>
                                    </div>
                                </div>
                                <br clear="all" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
        if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>