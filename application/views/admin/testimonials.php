<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/testimonials'), isset($parent_page_name)?$parent_page_name:'Testimonials');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Testimonials');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Testimonials</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="20%">Title</th>
                                <th width="15%" data-hide="phone">Name</th>
                                <th width="20%" data-hide="phone">Email</th>
                                <th width="5%" data-hide="phone">Rating</th>
								<th width="10%" data-hide="phone">On</th>
                                <th width="10%" data-hide="phone">Status</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_testimonial" name="frm_testimonial" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="testimonial_id" id="testimonial_id"<?php echo isset($record['PKTestimonialID'])?' value="' . $record['PKTestimonialID'] . '"':''?> />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Name <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="name" name="name" class="form-control"<?php echo $disabled_attribute;echo isset($record['Name'])?' value="' . $record['Name'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Email Address <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="email_address" name="email_address" class="form-control"<?php echo $disabled_attribute;echo isset($record['EmailAddress'])?' value="' . $record['EmailAddress'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Content <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <textarea id="content" name="content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Content'])?$record['Content']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Testimonial Date <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="testimonial_date" name="testimonial_date" class="form-control date"<?php echo $disabled_attribute;echo isset($record['TestimonialDate'])?' value="' . $record['TestimonialDate'] . '"':''?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Rating</label>
                                        <div class="controls">
                                            <?php
                                            if($type == "View"){
                                                ?>
                                                <input type="text" id="rating" name="rating" class="col-md-3 only-number not-zero"<?php echo $disabled_attribute;echo isset($record['Rating'])?' value="' . $record['Rating'] . '"':' value="1"'?> />
                                            <?php }else{?>
                                                <input type="text" id="rating" name="rating" class="col-md-3 rating-spin only-number not-zero"<?php echo isset($record['Rating'])?' value="' . $record['Rating'] . '"':' value="1"'?> />
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/testimonials')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/testimonials/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,
                null,null,null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/testimonials/add')?>">Add</a>');

    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>