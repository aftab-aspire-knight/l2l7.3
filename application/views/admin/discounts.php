<div class="content">
    <?php
    //d($member_records, 1);
    if (isset($type)) {
        echo show_admin_bread_crumbs($type, site_url('admin/discounts'), isset($parent_page_name) ? $parent_page_name : 'Discounts');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name) ? $parent_page_name : 'Discounts');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Discounts</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if (!isset($type)) {
                        ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th width="20%">Member</th>
                                        <th width="10%">Code</th>
                                        <th width="10%" data-hide="phone">Worth</th>
                                        <th width="10%" data-hide="phone">Type</th>
                                        <th width="10%" data-hide="phone">For</th>
                                        <th width="10%" data-hide="phone">Code Used</th>
                                        <th width="10%" data-hide="phone">Status</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                            </table>
                        <?php } else {
                            $disabled_attribute = '';
                            if ($type == "View") {
                                $disabled_attribute = ' disabled="disabled"';
                            }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_discount" name="frm_discount" action="<?php echo isset($form_action) ? $form_action : '#' ?>" method="post">
                                <input type="hidden" name="discount_id" id="discount_id" <?php echo isset($record['PKDiscountID']) ? ' value="' . $record['PKDiscountID'] . '"' : '' ?> />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Code <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="code" name="code" class="form-control" <?php echo $disabled_attribute;
                                                                                                            echo isset($record['Code']) ? ' value="' . $record['Code'] . '"' : '' ?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Worth <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="worth" name="worth" class="form-control only-number" <?php echo $disabled_attribute;
                                                                                                                        echo isset($record['Worth']) ? ' value="' . $record['Worth'] . '"' : '' ?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Type <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="d_type" name="d_type" class="form-control" <?php echo $disabled_attribute; ?>>
                                                <option value="">Select</option>
                                                <option value="Percentage" <?php echo isset($record['DType']) ? ($record['DType'] == "Percentage") ? ' selected="selected"' : '' : '' ?>>Percentage</option>
                                                <option value="Price" <?php echo isset($record['DType']) ? ($record['DType'] == "Price") ? ' selected="selected"' : '' : '' ?>>Price</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Status <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status" class="form-control" <?php echo $disabled_attribute; ?>>
                                                <option value="">Select</option>
                                                <option value="Active" <?php echo isset($record['Status']) ? ($record['Status'] == "Active") ? ' selected="selected"' : '' : '' ?>>Active</option>
                                                <option value="Expire" <?php echo isset($record['Status']) ? ($record['Status'] == "Expire") ? ' selected="selected"' : '' : '' ?>>Expire</option>
                                                <option value="Used" <?php echo isset($record['Status']) ? ($record['Status'] == "Used") ? ' selected="selected"' : '' : '' ?>>Used</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Start Date</label>
                                        <div class="controls">
                                            <input type="text" id="start_date" name="start_date" class="form-control date" <?php echo $disabled_attribute;
                                                                                                                            echo isset($record['StartDate']) ? ' value="' . $record['StartDate'] . '"' : '' ?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Expire Date</label>
                                        <div class="controls">
                                            <input type="text" id="expire_date" name="expire_date" class="form-control date" <?php echo $disabled_attribute;
                                                                                                                                echo isset($record['ExpireDate']) ? ' value="' . $record['ExpireDate'] . '"' : '' ?>>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Code Used <span class="red-color">*</span></label><span class="help">one member can used this code</span>
                                        <div class="controls">
                                            <select id="code_used" name="code_used" class="form-control" <?php echo $disabled_attribute; ?>>
                                                <option value="One Time" <?php echo isset($record['CodeUsed']) ? ($record['CodeUsed'] == "One Time") ? ' selected="selected"' : '' : '' ?>>One Time</option>
                                                <option value="Multiple Time" <?php echo isset($record['CodeUsed']) ? ($record['CodeUsed'] == "Multiple Time") ? ' selected="selected"' : '' : '' ?>>Multiple Time</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php /* ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Member <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="member" name="member" class="form-control select2"<?php echo $disabled_attribute;?>>
                                                <option value="0"<?php echo isset($record['FKMemberID'])?($record['FKMemberID']=="None")?' selected="selected"':'':''?>>None</option>
                                                <?php
                                                if(isset($member_records) && sizeof($member_records) > 0){
                                                    foreach($member_records as $rec){
                                                        if(!empty($rec['EmailAddress'])){
                                                            $member_selected = "";
                                                            if(isset($record['FKMemberID'])){
                                                                if($record['FKMemberID'] == $rec['PKMemberID']){
                                                                    $member_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $rec['PKMemberID'] . '"' . $member_selected . '>' . $rec['EmailAddress'] . '</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div><? */ ?>

                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Member <span class="red-color">*</span></label>
                                        <div class="controls">

                                            <select id="member" class="member form-control" <?php echo $disabled_attribute; ?> name="member">

                                                <?php if (isset($member_records['PKMemberID'])) { ?>
                                                    <option value="<?php echo $member_records['PKMemberID'] ?>"><?php echo $member_records['EmailAddress'] ?></option>

                                                <?php } ?>



                                            </select>


                                        </div>
                                    </div>
                                </div>

                                <br clear="all" />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Discount For <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="discount_for" name="discount_for" class="form-control" <?php echo $disabled_attribute; ?>>
                                                <option value="">Select</option>
                                                <option value="Discount" <?php echo isset($record['DiscountFor']) ? ($record['DiscountFor'] == "Discount") ? ' selected="selected"' : '' : '' ?>>Discount</option>
                                                <option value="Referral" <?php echo isset($record['DiscountFor']) ? ($record['DiscountFor'] == "Referral") ? ' selected="selected"' : '' : '' ?>>Referral</option>
                                                <option value="Loyalty" <?php echo isset($record['DiscountFor']) ? ($record['DiscountFor'] == "Loyalty") ? ' selected="selected"' : '' : '' ?>>Loyalty</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Minimum Order Amount</label>
                                        <div class="controls">
                                            <input type="text" id="minimum_order_amount" name="minimum_order_amount" data-a-sep="," data-a-dec="." class="form-control auto" <?php echo $disabled_attribute;
                                                                                                                                                                                echo isset($record['MinimumOrderAmount']) ? ' value="' . $record['MinimumOrderAmount'] . '"' : '' ?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if ($type != "View") {
                                            $button_text = "Cancel";
                                        ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php } ?>
                                        <a href="<?php echo site_url('admin/discounts') ?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text ?></a>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable({
        "sPaginationType": "bootstrap",
        "aaSorting": [
            [0, "desc"]
        ],
        'bProcessing': true,
        'bServerSide': true,
        "aLengthMenu": [
            [20, 50, 100, 500, 1000, -1],
            [20, 50, 100, 500, 1000, "All"]
        ],
        "iDisplayLength": 20,
        "cache": false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth: false,
        fnPreDrawCallback: function() {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax': {
            url: '<?php echo site_url('admin/discounts/listener'); ?>',
            type: 'POST'
        },
        'aoColumns': [{
                'bSearchable': true,
                'bVisible': true
            },
            null, null, null, null, null, null,
            null, {
                "bSortable": false,
                "bSearchable": false
            }

        ],
        fnRowCallback: function(nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback: function(oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/discounts/add') ?>">Add</a>');

    $(document).ready(function() {
        <?php
        if (isset($admin_message)) {
        ?>
            MessageBoxSuccess("#msg_box", "<?php echo $admin_message ?>", 1000, "");
            scrollToPosition($("#msg_box"));
        <?php
        }
        ?>

        $('.member').select2({
            placeholder: 'Select an item',
            ajax: {
                url: '<?php echo base_url("admin/discounts/members") ?>',
                dataType: 'json',
                delay: 250,
                data: function(data) {
                    return {
                        searchTerm: data.term // search term
                    };
                },
                processResults: function(response) {
                    return {
                        results: response
                    };
                },
                formatSelection: function(element) {
                    
                    return '<?php echo isset($member_records['EmailAddress']) ? $member_records['EmailAddress'] : null ?>' + '(<?php echo isset($member_records['PKMemberID']) ? $member_records['PKMemberID'] : null ?>)';
                },
                cache: true
            }
        });


    });
</script>