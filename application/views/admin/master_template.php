<!DOCTYPE html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo isset($header_title) ? $header_title : ''; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/x-icon"
          href="<?php echo base_url('assets/images/front/l2l-fav/favicon.ico') ?>"/>
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-57x57.png') ?>"/>
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-72x72.png') ?>"/>
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-114x114.png') ?>"/>
    <!-- START CSS -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700"/>
    <?php
    echo $this->carabiner->display('Admin_Data_Table_Css');
    if ($current_page_url == "administrators") {
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_Nestable_Css');
    } else if ($current_page_url == "discounts" || $current_page_url == "invoicefilters") {
        echo $this->carabiner->display('Admin_Select2_Css');
        echo $this->carabiner->display('Admin_DatePicker_Css');
    } else if ($current_page_url == "navigations" || $current_page_url == "widgetsections") {
        echo $this->carabiner->display('Admin_Select2_Css');
        echo $this->carabiner->display('Admin_Nestable_Css');
    } else if ($current_page_url == "banners" || $current_page_url == "smsresponders" || $current_page_url == "categories" || $current_page_url == "subscribers" || $current_page_url == "emailresponders") {
        echo $this->carabiner->display('Admin_Switch_Css');
    } else if ($current_page_url == "unavailabletimes") {
        echo $this->carabiner->display('Admin_DatePicker_Css');
        echo $this->carabiner->display('Admin_Nestable_Css');
        echo $this->carabiner->display('Admin_Select2_Css');
    } else if ($current_page_url == "testimonials") {
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_DatePicker_Css');
    } else if ($current_page_url == "services" || $current_page_url == "preferences" || $current_page_url == "members" || $current_page_url == "pages") {
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_Select2_Css');
    } else if ($current_page_url == "locations" || $current_page_url == "franchises") {
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_Nestable_Css');
        echo $this->carabiner->display('Admin_Select2_Css');
    } else if ($current_page_url == "users") {
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_Nestable_Css');
        echo $this->carabiner->display('Admin_Select2_Css');
    } else if ($current_page_url == "invoices") {
        echo $this->carabiner->display('Admin_Select2_Css');
        echo $this->carabiner->display('Admin_DatePicker_Css');
    } else if ($current_page_url == "payments") {
        echo $this->carabiner->display('Admin_Select2_Css');
        echo $this->carabiner->display('Admin_DatePicker_Css');
        echo $this->carabiner->display('Admin_Data_Table_Css');
    } else if ($current_page_url == "notifications") {
        echo $this->carabiner->display('Admin_DatePicker_Css');
        echo $this->carabiner->display('Admin_Switch_Css');
        echo $this->carabiner->display('Admin_Select2_Css');
    }
    echo $this->carabiner->display('Admin_Core_Css');
    echo $this->carabiner->display('Admin_Main_Css');
    echo $this->carabiner->display('Admin_Custom_Css');
    ?>
    <!-- END CSS -->
</head>
<!-- BEGIN BODY -->
<body class="">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('admin/includes/header'); ?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
        <?php $this->load->view('admin/includes/sidebar'); ?>
        <!-- END SIDEBAR -->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
            <div class="clearfix"></div>
            <?php $this->load->view($main_content); ?>
            <!-- END PAGE -->
        </div>
    </div>
    <!-- END CONTAINER -->
</body>
<!-- END BODY -->