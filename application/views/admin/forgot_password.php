<!DOCTYPE html>
<meta
    http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8" />
    <title><?php echo isset($title)?$title:isset($website_name)?$website_name:'Love 2 Laundry' . ' | Administrator Area';?></title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/x-icon"
          href="<?php echo base_url('assets/images/front/l2l-fav/favicon.ico') ?>"/>
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-57x57.png') ?>"/>
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-72x72.png') ?>"/>
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-114x114.png') ?>"/>
    <!-- START CSS -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700"/>
    <?php
    echo $this->carabiner->display('Admin_Core_Css');
    echo $this->carabiner->display('Admin_Main_Css');
    echo $this->carabiner->display('Admin_Custom_Css');
    ?>
    <!-- END CSS -->
</head>
<!-- BEGIN BODY -->
<body
    class="">
<!-- BEGIN CONTAINER -->
<div class="">
    <div class="content" style="text-align:center;">
        <div class="admin-login">
            <form id="frm_forgot" name="frm_forgot" method="post" autocomplete="off" action="<?php echo site_url('admin/login/postforgot')?>">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Love 2 <span class="semi-bold">Laundry</span></h4>
                        <h3>Forgot <span class="semi-bold">Password</span></h3>
                    </div>
                    <div class="grid-body no-border">
                        <div class="row">
                            <label class="form-label float-left">Email Address <span class="red-color">*</span></label><br clear="all" />
                            <div class="input-group">
                                <input type="text" id="email_address" name="email_address" class="form-control" placeholder="someone@example.com" />
                            <span class="input-group-addon primary">				  
                              <span class="arrow"></span>
                                <i class="fa fa-align-justify"></i>
                              </span>
                            </div>
                            <br />
                            <a href="<?php echo site_url('admin/login')?>" id="btn_reset" class="btn btn-white btn-cons">Back</a>
                            <button type="submit" class="btn btn-primary btn-cons" id="btn_forgot">
                                <i class="fa fa-unlock"></i>
                                &nbsp;&nbsp;Send
                            </button>
                            <br clear="all" />
                            <div id="msg_box" class="alert hide-area">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTAINER -->
<!-- START JAVASCRIPT -->
<?php
echo $this->carabiner->display('JQuery');
echo $this->carabiner->display('Admin_Core_JQuery');
echo $this->carabiner->display('Validation_JQuery');
echo $this->carabiner->display('Admin_Main_JQuery');
?>
<!-- END JAVASCRIPT -->
</body>
<!-- END BODY -->