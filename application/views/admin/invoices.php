<div class="content">
    <?php
    if (isset($type)) {
        echo show_admin_bread_crumbs($type, site_url('admin/invoices'), isset($parent_page_name) ? $parent_page_name : 'Invoices');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name) ? $parent_page_name : 'Invoices');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Invoices</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if (!isset($type)) {
                        ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                    <tr>
                                        <th width="3%">ID</th>
                                        <th width="2%">#</th>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="5%">From</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '<th width="5%">Name</th>';
                                        } else {
                                        ?><?php } ?>
                                        <th width="5%">Invoice</th>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '<th width="8%">Franchise</th>';
                                        } else {
                                        ?><th width="8%">Franchise</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?>
                                            <th width="7%">Member</th>
                                        <?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="4%">Type</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="3%">Regularity</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="5%">Method</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="5%">P-Status</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="5%">O-Status</th><?php } ?>
                                        <th width="5%">Total(<?php echo $this->config->item('currency_sign') ?>)</th>
                                        <th width="7%">P-Date</th>
                                        <th width="7%">P-Time</th>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="3%">P-N</th><?php } ?>
                                        <th width="7%">D-Date</th>
                                        <th width="7%">D-Time</th>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="3%">D-N</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="4%">C-Time</th><?php } ?>
                                        <?php
                                        if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                            echo '';
                                        } else {
                                        ?><th width="4%">U-Time</th><?php } ?>
                                        <th width="7%">Options</th>
                                    </tr>
                                </thead>
                            </table>
                            <?php
                        } else {
                            if ($type == "Add") {
                            ?>
                                <form class="form-no-horizontal-spacing" id="frm_invoice_add" name="frm_invoice_add" action="<?php echo site_url('admin/invoices/availability') ?>" method="post">
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Member <span class="red-color">*</span></label>

                                            <div class="controls">
                                                <select id="member" name="member" class="form-control select2">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (isset($member_records) && sizeof($member_records) > 0) {
                                                        foreach ($member_records as $rec) {
                                                            $member_selected = "";
                                                            if (isset($record['FKMemberID'])) {
                                                                if ($record['FKMemberID'] == $rec['ID']) {
                                                                    $member_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $rec['ID'] . '||' . $rec['PostalCode'] . '"' . $member_selected . '>' . $rec['EmailAddress'] . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Post Code <span class="red-color">*</span></label>

                                            <div class="controls">
                                                <input type="text" id="postal_code" name="postal_code" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />

                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="float-right">
                                            <?php
                                            $button_text = "Back";
                                            if ($type != "View") {
                                                $button_text = "Cancel";
                                            ?>
                                                <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">
                                                    Check Availability
                                                </button>
                                            <?php } ?>
                                            <a href="<?php echo site_url('admin/invoices') ?>" class="btn btn-danger btn-cons" id="btn_option">Back</a>
                                        </div>
                                    </div>
                                </form>
                            <?php
                            } else {
                            ?>



                                <div class="col-md-8 col-xs-8 col-lg-8">


                                    <form class="form-no-horizontal-spacing" id="frm_invoice_edit" name="frm_invoice_edit" action="<?php echo site_url('admin/invoices/update') ?>" method="post">
                                        <input type="hidden" name="stripe_jquery_api_key" id="stripe_jquery_api_key" <?php echo isset($stripe_jquery_api_key) ? ' value="' . $stripe_jquery_api_key . '"' : '' ?> />
                                        <input type="hidden" name="order_minimum_amount" id="order_minimum_amount" <?php echo isset($order_minimum_amount) ? ' value="' . $order_minimum_amount . '"' : '' ?> />
                                        <input type="hidden" name="order_minimum_later_amount" id="order_minimum_later_amount" <?php echo isset($order_minimum_amount) ? ' value="' . $order_minimum_amount . '"' : '' ?> />
                                        <input type="hidden" name="old_session_cart_grand_total" id="old_session_cart_grand_total" <?php echo isset($record['GrandTotal']) ? ' value="' . $record['GrandTotal'] . '"' : '' ?> />
                                        <input type="hidden" name="session_cart_sub_total" id="session_cart_sub_total" <?php echo isset($record['SubTotal']) ? ' value="' . $record['SubTotal'] . '"' : '' ?> />
                                        <input type="hidden" name="session_cart_preference_total" id="session_cart_preference_total" <?php echo isset($record['PreferenceTotal']) ? ' value="' . $record['PreferenceTotal'] . '"' : '' ?> />
                                        <input type="hidden" name="session_cart_discount_total" id="session_cart_discount_total" <?php echo isset($record['DiscountTotal']) ? ' value="' . $record['DiscountTotal'] . '"' : '' ?> />
                                        <input type="hidden" name="session_cart_grand_total" id="session_cart_grand_total" <?php echo isset($record['GrandTotal']) ? ' value="' . $record['GrandTotal'] . '"' : '' ?> />
                                        <input type="hidden" name="invoice_id" id="invoice_id" <?php echo isset($record['PKInvoiceID']) ? ' value="' . $record['PKInvoiceID'] . '"' : '' ?> />
                                        <input type="hidden" name="invoice_token" id="invoice_token" />
                                        <input type="hidden" name="session_cart" id="session_cart" />
                                        <input type="hidden" name="refund_price_url" id="refund_price_url" value="<?php echo site_url('admin/invoices/refund') ?>" />
                                        <input type="hidden" name="get_refunds_url" id="get_refunds_url" value="<?php echo site_url('admin/invoices/getrefund') ?>" />

                                        <ul class="nav nav-tabs" id="maincontent" role="tablist">

                                            <?php
                                            if ($admin_detail["FKFranchiseID"] == "0") {
                                            ?>
                                                <li class="active">
                                                    <a href="#home" role="tab" data-toggle="tab">Order</a>
                                                </li>
                                                <li>
                                                    <a href="#timings" role="tab" data-toggle="tab">Pickup/Delivery</a>
                                                </li>
                                                <li>
                                                    <a href="#preferences" role="tab" data-toggle="tab">Preferences</a>
                                                </li>
                                                <li>
                                                    <a href="#cart" role="tab" data-toggle="tab">Cart Information</a>
                                                </li>
                                                <li>
                                                    <a href="#notes" role="tab" data-toggle="tab">Notes</a>
                                                </li>

                                                <li>
                                                    <a href="#extra" role="tab" data-toggle="tab">Extra</a>
                                                </li>

                                                <li>
                                                    <a href="<?php echo base_url("uploads/emailattachments"); ?>/invoice-<?php echo $record['InvoiceNumber'] ?>.pdf" target="_blank">Download Invoice</a>
                                                </li>
                                            <?php } else { ?>
                                                <li>
                                                    <a href="#cart" role="tab" data-toggle="tab">Cart Information</a>
                                                </li>

                                            <?php } ?>




                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane in <?php if ($admin_detail["FKFranchiseID"] == "0") { ?>  active <?php } ?>" id="home">
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Order From <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <select id="order_from" name="order_from" class="form-control">
                                                                <option value="Desktop" <?php echo isset($record['OrderPostFrom']) ? ($record['OrderPostFrom'] == "Desktop") ? ' selected="selected"' : '' : '' ?>>Desktop</option>
                                                                <option value="Mobile" <?php echo isset($record['OrderPostFrom']) ? ($record['OrderPostFrom'] == "Mobile") ? ' selected="selected"' : '' : '' ?>>Mobile</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Invoice Number</label>
                                                        <div class="controls">
                                                            <input type="text" readonly="readonly" id="invoice_number" class="form-control only-number" <?php echo isset($record['InvoiceNumber']) ? ' value="' . $record['InvoiceNumber'] . '"' : ' value="' . $invoice_number . '"' ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">IP Address <span class="red-color">*</span></label>

                                                        <div class="controls">
                                                            <input type="text" id="ip_address" name="ip_address" class="form-control" <?php echo isset($record['IPAddress']) ? ' value="' . $record['IPAddress'] . '"' : '' ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br clear="all" />
                                                <?php if (isset($FKFranchiseID) && $FKFranchiseID > 0) { ?>
                                                <?php } else { ?>
                                                    <div class="col-md-3 col-xs-12 col-lg-3">
                                                        <div class="form-group">
                                                            <label class="form-label">Franchise <span class="red-color">*</span></label>
                                                            <div class="controls">
                                                                <select id="franchise" name="franchise" class="form-control select2">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                    if (isset($franchise_records) && sizeof($franchise_records) > 0) {
                                                                        foreach ($franchise_records as $rec) {
                                                                            $franchise_selected = "";
                                                                            if (isset($record['FKFranchiseID'])) {
                                                                                if ($record['FKFranchiseID'] == $rec['ID']) {
                                                                                    $franchise_selected = ' selected="selected"';
                                                                                }
                                                                            }
                                                                            echo '<option value="' . $rec['ID'] . '"' . $franchise_selected . '>' . $rec['Title'] . '</option>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-xs-12 col-lg-3">
                                                        <div class="form-group">
                                                            <label class="form-label">Member</label>
                                                            <div class="controls">
                                                                <input type="text" readonly class="form-control" <?php echo isset($record['member']['EmailAddress']) ? ' value="' . $record['member']['EmailAddress'] . '"' : '' ?> />
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-md-3 col-xs-12 col-lg-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Credit Card <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <select id="member_card" name="member_card" class="form-control select2">
                                                                <option value="">Select</option>
                                                                <?php
                                                                if (isset($record['member_card_records']) && sizeof($record['member_card_records']) > 0) {
                                                                    foreach ($record['member_card_records'] as $rec) {
                                                                        $card_selected = "";
                                                                        if (isset($record['FKCardID'])) {
                                                                            if ($record['FKCardID'] == $rec['PKCardID']) {
                                                                                $card_selected = ' selected="selected"';
                                                                            }
                                                                        }
                                                                        echo '<option value="' . $rec['PKCardID'] . '||' . $rec['Number'] . '||' . $rec['Month'] . '||' . $rec['Year'] . '||' . $rec['Code'] . '"' . $card_selected . '>' . $rec['Title'] . '</option>';
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-xs-12 col-lg-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Discount</label>
                                                        <div class="controls">
                                                            <select id="discount" name="discount" class="form-control select2">
                                                                <option value="">None</option>
                                                                <?php
                                                                if (isset($discount_records) && sizeof($discount_records) > 0) {
                                                                    foreach ($discount_records as $rec) {
                                                                        $discount_selected = "";
                                                                        if (isset($record['FKDiscountID'])) {
                                                                            if ($record['FKDiscountID'] == $rec['ID'] && $record['DiscountType'] == "Discount") {
                                                                                $discount_selected = ' selected="selected"';
                                                                            }
                                                                        }
                                                                        $d_value = "";
                                                                        if (!empty($discount_selected)) {
                                                                            if ($record['DType'] == "Percentage") {
                                                                                $d_value = $record['DiscountCode'] . ' - (' . $record['DiscountWorth'] . '%)';
                                                                            } else if ($record['DType'] == "Price") {
                                                                                $d_value = $record['DiscountCode'] . ' - (' . $this->config->item('currency_symbol') . $record['DiscountWorth'] . ')';
                                                                            }
                                                                            echo '<option value="' . $rec['ID'] . '||Discount||' . $record['DiscountCode'] . '||' . $record['DiscountWorth'] . '||' . $record['DType'] . '"' . $discount_selected . '>T=' . $record['DiscountType'] . ':C=' . $d_value . '</option>';
                                                                        } else {
                                                                            if ($rec['DType'] == "Percentage") {
                                                                                $d_value = $rec['Code'] . ' - (' . $rec['Worth'] . '%)';
                                                                            } else if ($rec['DType'] == "Price") {
                                                                                $d_value = $rec['Code'] . ' - (' . $this->config->item('currency_symbol') . $rec['Worth'] . ')';
                                                                            }
                                                                            echo '<option value="' . $rec['ID'] . '||Discount||' . $rec['Code'] . '||' . $rec['Worth'] . '||' . $rec['DType'] . '"' . $discount_selected . '>T=' . $rec['DiscountFor'] . ':C=' . $d_value . '</option>';
                                                                        }
                                                                    }
                                                                }
                                                                if (isset($referral_records) && sizeof($referral_records) > 0) {
                                                                    foreach ($referral_records as $rec) {
                                                                        $referral_selected = "";
                                                                        if (isset($record['ReferralID'])) {
                                                                            if ($record['ReferralID'] == $rec['ID'] && $record['DiscountType'] == "Referral") {
                                                                                $referral_selected = ' selected="selected"';
                                                                            }
                                                                        }
                                                                        $d_value = "";
                                                                        if (!empty($referral_selected)) {
                                                                            $d_value = $record['DiscountCode'] . ' - (' . $this->config->item('currency_symbol') . $record['DiscountWorth'] . ')';
                                                                            echo '<option value="' . $rec['ID'] . '||' . $record['DiscountType'] . '||' . $record['DiscountCode'] . '||' . $record['DiscountWorth'] . '||' . $record['DType'] . '"' . $referral_selected . '>T=Referral:C=' . $d_value . '</option>';
                                                                        } else {
                                                                            $d_value = $rec['ReferralCode'] . ' - (' . $this->config->item('currency_symbol') . $referral_code_amount . ')';
                                                                            echo '<option value="' . $rec['ID'] . '||Referral||' . $rec['ReferralCode'] . '||' . $referral_code_amount . '||Price"' . $referral_selected . '>T=Referral:C=' . $d_value . '</option>';
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br clear="all" />
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Payment Method <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <select id="payment_method" name="payment_method" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="Cash" <?php echo isset($record['PaymentMethod']) ? ($record['PaymentMethod'] == "Cash") ? ' selected="selected"' : '' : '' ?>>Cash</option>
                                                                <option value="Stripe" <?php echo isset($record['PaymentMethod']) ? ($record['PaymentMethod'] == "Stripe") ? ' selected="selected"' : '' : '' ?>>Stripe</option>
                                                                <option value="Googlepay" <?php echo isset($record['PaymentMethod']) ? ($record['PaymentMethod'] == "Googlepay") ? ' selected="selected"' : '' : '' ?>>Google Pay</option>
                                                                <option value="Applepay" <?php echo isset($record['PaymentMethod']) ? ($record['PaymentMethod'] == "Applepay") ? ' selected="selected"' : '' : '' ?>>Apple Pay</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Payment Status <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <select id="payment_status" name="payment_status" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="Pending" <?php echo isset($record['PaymentStatus']) ? ($record['PaymentStatus'] == "Pending") ? ' selected="selected"' : '' : '' ?>>Pending</option>
                                                                <option value="Processed" <?php echo isset($record['PaymentStatus']) ? ($record['PaymentStatus'] == "Processed") ? ' selected="selected"' : '' : '' ?>>Processed</option>
                                                                <option value="Cancel" <?php echo isset($record['PaymentStatus']) ? ($record['PaymentStatus'] == "Cancel") ? ' selected="selected"' : '' : '' ?>>Cancel</option>
                                                                <option value="Completed" <?php echo isset($record['PaymentStatus']) ? ($record['PaymentStatus'] == "Completed") ? ' selected="selected"' : '' : '' ?>>Completed</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Order Status <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <select id="order_status" name="order_status" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="Pending" <?php echo isset($record['OrderStatus']) ? ($record['OrderStatus'] == "Pending") ? ' selected="selected"' : '' : '' ?>>Pending</option>
                                                                <option value="Processed" <?php echo isset($record['OrderStatus']) ? ($record['OrderStatus'] == "Processed") ? ' selected="selected"' : '' : '' ?>>Processed</option>
                                                                <option value="Cancel" <?php echo isset($record['OrderStatus']) ? ($record['OrderStatus'] == "Cancel") ? ' selected="selected"' : '' : '' ?>>Cancel</option>
                                                                <option value="Completed" <?php echo isset($record['OrderStatus']) ? ($record['OrderStatus'] == "Completed") ? ' selected="selected"' : '' : '' ?>>Completed</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br clear="all" />

                                                <?php
                                                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                                    echo '';
                                                } else {
                                                ?>
                                                    <br clear="all" />
                                                    <div class="col-md-4 col-xs-12 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Address <span class="red-color">*</span></label>
                                                            <div class="controls">
                                                                <select id="location" name="location" class="form-control select2">
                                                                    <option value="">Your Address</option>
                                                                    <?php
                                                                    if (isset($location_records) && sizeof($location_records) > 0) {
                                                                        foreach ($location_records as $rec) {
                                                                            $location_selected = "";
                                                                            if (isset($record['Location'])) {
                                                                                if ($record['Location'] == $rec['Title']) {
                                                                                    $location_selected = ' selected="selected"';
                                                                                }
                                                                            }
                                                                            $location_value = "";
                                                                            foreach ($rec['lockers'] as $rec_locker) {
                                                                                $location_value .= $rec_locker['Title'] . '|';
                                                                            }
                                                                            var_dump($location_value);
                                                                            $location_value = RemoveCharacter($location_value);
                                                                            echo '<option value="' . $rec['Title'] . '||' . $location_value . '"' . $location_selected . '>' . $rec['Title'] . '</option>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Address Type</label>
                                                            <div class="controls">
                                                            <input type="text" disabled class="form-control" <?php echo isset($record['AddressType']) ? ' value="' . ucfirst($record['AddressType']) . '"' : '' ?> />

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="col-md-4 col-xs-12 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="form-label">Locker <span class="red-color">*</span></label>
                                                            <div class="controls">
                                                                <select id="locker" name="locker" class="form-control select2">
                                                                    <option value="">None</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                <?php } ?>
                                                <div style="display:<?php echo (isset($FKFranchiseID) && $FKFranchiseID > 0) ? 'none' : ''; ?>" class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Post Code <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="postal_code" name="postal_code" class="form-control" <?php echo isset($record['PostalCode']) ? ' value="' . $record['PostalCode'] . '"' : '' ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br clear="all" />
                                                <div style="display:<?php echo (isset($FKFranchiseID) && $FKFranchiseID > 0) ? 'none' : ''; ?>" class="col-md-4 col-xs-12 col-lg-4 address-area">
                                                    <div class="form-group">
                                                        <label class="form-label">Address<span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="building_name" name="building_name" class="form-control" <?php echo isset($record['BuildingName']) ? ' value="' . $record['BuildingName'] . '"' : '' ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="display:<?php echo (isset($FKFranchiseID) && $FKFranchiseID > 0) ? 'none' : ''; ?>" class="col-md-4 col-xs-12 col-lg-4 address-area">
                                                    <div class="form-group">
                                                        <label class="form-label">Address Line 2 </label>
                                                        <div class="controls">
                                                            <input type="text" id="street_name" name="street_name" class="form-control" <?php echo isset($record['StreetName']) ? ' value="' . $record['StreetName'] . '"' : '' ?> />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="display:<?php echo (isset($FKFranchiseID) && $FKFranchiseID > 0) ? 'none' : ''; ?>" class="col-md-4 col-xs-12 col-lg-4 address-area">
                                                    <div class="form-group">
                                                        <label class="form-label">Town<span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="town" name="town" class="form-control" <?php echo isset($record['Town']) ? ' value="' . $record['Town'] . '"' : '' ?> />
                                                        </div>
                                                    </div>
                                                </div> <?php if (isset($REGULARITY) && !empty($REGULARITY)) { ?> <div class="col-md-4 col-xs-12 col-lg-4">
                                                        <div class="form-group"> <label class="form-label">How regularly should we come? <span class="red-color">*</span></label>
                                                            <div class="controls"> <select id="regularly" name="regularly" class="form-control">
                                                                    <option value="">Select</option> <?php foreach ($REGULARITY as $key => $val) { ?> <option value="<?php echo $key; ?>" <?php echo isset($record['Regularly']) ? ($record['Regularly'] == $key) ? ' selected="selected"' : '' : '' ?>><?php echo $val; ?></option> <?php } ?>
                                                                </select> </div>
                                                        </div>
                                                    </div> <br clear="all" /> <?php } ?>
                                                <br clear="all" class="address-area" />

                                                <br clear="all" />
                                            </div>
                                            <div class="tab-pane fade" id="timings">
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Pickup Date <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <!--
                                                                                                                      <select onChange="getPickupTimes()" id="pick_date" name="pick_date" class="form-control">
                                                                                                                          <option selected="selected" value="<?php echo $record["PickupDate"]; ?>"><?php echo $record["PickupDate"]; ?></option>
                                                          
                                                            <?php foreach ($pickup_date_slots as $date) { ?>
                                                                                                                                                                                                                    <option value="<?php echo $date; ?>"><?php echo $date; ?></option>
                                                            <?php } ?>
                                                          
                                                                                                                      </select>
                                                            -->

                                                            <input name="pick_date" id="pick_date" type="text" readonly="readonly" class="form-control i-date" <?php echo isset($record['PickupDate']) ? ' value="' . $record['PickupDate'] . '"' : '' ?> />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-xs-12 col-lg-8">

                                                    <label class="form-label">Pickup Time <span class="red-color">*</span></label>


                                                    <?php
                                                    list($from, $to) = explode("-", $record['PickupTime']);
                                                    //$from = sprintf("%02d", $from). ":00";
                                                    //$to = sprintf("%02d", $to). ":00";
                                                    ?>

                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <select name="pick_time_from" id="pick_time_from" class="form-control">

                                                            <?php
                                                            for ($i = 0; $i < 24; $i++) {
                                                                $hour = (int) $i . ":00";
                                                            ?>
                                                                <option value="<?php echo $hour ?>" <?php if ($hour == trim($from)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option>
                                                            <?php } ?>


                                                        </select>

                                                    </div>
                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <select name="pick_time_to" id="pick_time_to" class="form-control">

                                                            <?php
                                                            for ($i = 0; $i < 24; $i++) {
                                                                $hour = (int) $i . ":00";
                                                            ?>
                                                                <option value="<?php echo $hour ?>" <?php if ($hour == trim($to)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option>
                                                            <?php } ?>
                                                        </select>

                                                    </div>

                                                </div>
                                                <br clear="all" />
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Delivery Date <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <!--
                                                            <select onchange="getDeliveryTimes()" id="delivery_date" name="delivery_date" class="form-control">
                                                            <?php echo $record["DeliveryDate"]; ?>
                                                                <option value="<?php echo $record["DeliveryDate"]; ?>"><?php echo $record["DeliveryDate"]; ?></option>

                                                            </select>   
                                                            -->

                                                            <input name="delivery_date" id="delivery_date" readonly="readonly" type="text" class="form-control i-date" <?php echo isset($record['DeliveryDate']) ? ' value="' . $record['DeliveryDate'] . '"' : '' ?> />

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-8 col-xs-12 col-lg-8">

                                                    <label class="form-label">Delivery Time <span class="red-color">*</span></label>
                                                    <?php
                                                    list($from, $to) = explode("-", $record['DeliveryTime']);
                                                    //$from = sprintf("%02d", $from). ":00";
                                                    //$to = sprintf("%02d", $to). ":00";
                                                    // echo $to;
                                                    ?>

                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <select name="delivery_time_from" id="delivery_time_from" class="form-control">

                                                            <?php
                                                            for ($i = 0; $i < 24; $i++) {
                                                                $hour = (int) $i . ":00";
                                                            ?>
                                                                <option value="<?php echo $hour ?>" <?php if ($hour == trim($from)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option>
                                                            <?php } ?>


                                                        </select>

                                                    </div>
                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <select name="delivery_time_to" id="delivery_time_to" class="form-control">

                                                            <?php
                                                            for ($i = 0; $i < 24; $i++) {
                                                                //$hour = sprintf("%02d", $i) . ":00";
                                                                $hour = (int) $i . ":00";
                                                            ?>
                                                                <option value="<?php echo $hour ?>" <?php if ($hour == trim($to)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option>
                                                            <?php } ?>


                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="preferences">
                                                <?php
                                                echo '<div id="preference_area">';
                                                if (isset($preference_records) && sizeof($preference_records) > 0) {
                                                    echo '<h3>Wash <strong>Preferences</strong></h3><hr/>';
                                                    $p_counter = 0;
                                                    foreach ($preference_records as $p_rec) {
                                                        if ($p_counter == 2) {
                                                            echo '<br clear="all" />';
                                                            $p_counter = 0;
                                                        }
                                                        echo '<div class="col-md-6 col-xs-12 col-lg-6">';
                                                        echo '<div class="form-group">';
                                                        echo '<label class="control-label">' . $p_rec['Title'] . ' <span class="red-color">*</span></label>';
                                                        echo '<select class="form-control preferences" id="cmb_' . str_replace("-", "_", CreateAccessURL($p_rec['Title'])) . '" name="preferences_list[]">';
                                                        foreach ($p_rec['child_records'] as $rec) {
                                                            $selected_preference = "";
                                                            if (in_array($rec['PKPreferenceID'], $record['preferences'])) {
                                                                $selected_preference = " selected='selected'";
                                                            }
                                                            echo '<option value="' . $rec['PKPreferenceID'] . '||' . $p_rec['Title'] . "||" . $rec['Title'] . '||' . $rec['Price'] . '||' . $rec["PricePackage"] . '"' . $selected_preference . '>' . $rec['Title'] . " - " . (isset($rec['Price']) ? $rec['Price'] : 0.00) . '</option>';
                                                        }
                                                        echo '</select>';
                                                        echo '</div>';
                                                        echo '</div>';
                                                        $p_counter += 1;
                                                    }
                                                    if ($p_counter != 0) {
                                                        echo '<br clear="all" />';
                                                    }
                                                }
                                                echo '</div>';
                                                ?>
                                            </div>


                                            <div class="tab-pane fade" id="extra">

                                                <div class="col-md-12 col-xs-12 col-lg-12">
                                                    <h3><span class="semi-bold">Extra</span></h3>
                                                    <hr />
                                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Tookaan Response</label>
                                                            <div class="controls">
                                                                <textarea disabled="disabled" rows="5" cols="15" class="form-control"><?php echo isset($record['TookanResponse']) ? $record['TookanResponse'] : '' ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Tookan Update Response</label>
                                                            <div class="controls">
                                                                <textarea disabled="disabled" rows="1" cols="15" class="form-control"><?php echo isset($record['TookanUpdateResponse']) ? $record['TookanUpdateResponse'] : '' ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Onfleet Response</label>
                                                            <div class="controls">
                                                                <textarea disabled="disabled" rows="5" cols="15" class="form-control"><?php echo isset($record['OnfleetResponse']) ? $record['OnfleetResponse'] : '' ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label">Onfleet Update Response</label>
                                                            <div class="controls">
                                                                <textarea disabled="disabled" rows="1" cols="15" class="form-control"><?php echo isset($record['OnfleetUpdateResponse']) ? $record['OnfleetUpdateResponse'] : '' ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>






                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label">Created At</label>
                                                            <div class="controls">
                                                                <input type="text" disabled="disabled" value="<?php echo isset($record['CreatedDateTime']) ? $record['CreatedDateTime'] : '' ?>" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label">Updated At</label>
                                                            <div class="controls">
                                                                <input type="text" disabled="disabled" value="<?php echo isset($record['UpdatedDateTime']) ? $record['UpdatedDateTime'] : '' ?>" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label">App Version</label>
                                                            <div class="controls">
                                                                <input type="text" disabled="disabled" value="<?php echo isset($record['AppVersion']) ? $record['AppVersion'] : '' ?>" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label">Device ID</label>
                                                            <div class="controls">
                                                                <input type="text" disabled="disabled" value="<?php echo isset($record['FKDeviceID']) ? $record['FKDeviceID'] : '' ?>" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>




                                            </div>


                                            <div class="tab-pane <?php if ($admin_detail["FKFranchiseID"] != "0") { ?>  active <?php } else { ?> fade <?php } ?>" id="cart">
                                                <div class="col-md-12 col-xs-12 col-lg-12">
                                                    <h3>Add Custom Service</h3>
                                                    <label class="form-label">Title <span class="red-color">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" name="custom_service_title" id="custom_service_title" class="form-control" />
                                                    </div>
                                                    <br clear="all" />
                                                    <label class="form-label">Price <span class="red-color">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" name="custom_service_price" id="custom_service_price" class="form-control auto" data-a-sep="," data-a-dec="." />
                                                    </div>
                                                    <br clear="all" />
                                                    <button type="button" class="btn btn-small btn-primary" id="add_custom_service">Add</button>
                                                    <br clear="all" />

                                                    <?php
                                                    if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                                        echo '';
                                                    } else {
                                                    ?>
                                                        <br clear="all" />
                                                        <label class="form-label">Refund Price <span class="red-color">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" name="refund_service_price" id="refund_service_price" class="form-control" />
                                                        </div>
                                                        <br clear="all" />
                                                        <button type="button" class="btn btn-small btn-primary" id="refund_service_price_btn">Refund</button>
                                                    <?php } ?>

                                                </div>


                                                <h3>Cart <span class="semi-bold">Information</span></h3>
                                                <hr />
                                                <div class="col-md-12 col-xs-12 col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label">Service <span class="red-color">*</span></label>

                                                        <table id="table_services" class="table table-striped dataTable">
                                                            <thead>
                                                                <tr role="row">
                                                                    <th rowspan="1" colspan="1">Name</th>
                                                                    <th rowspan="1" colspan="1">Category</th>
                                                                    <th rowspan="1" colspan="1">Price</th>

                                                                    <th rowspan="1" colspan="1"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <?php
                                                                if (isset($category_records) && sizeof($category_records) > 0) {
                                                                    foreach ($category_records as $category) {
                                                                        if (isset($category['service_records']) && sizeof($category['service_records']) > 0) {
                                                                            foreach ($category['service_records'] as $service_record) {
                                                                                $desktop_image_name = $this->config->item('front_dummy_image_name');
                                                                                $mobile_image_name = $this->config->item('front_dummy_mobile_service_image_name');
                                                                                if (!empty($rec['DesktopImageName'])) {
                                                                                    if (file_exists($this->config->item("dir_upload_service") . $rec['DesktopImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['DesktopImageName'])) {
                                                                                        $desktop_image_name = $rec['DesktopImageName'];
                                                                                    }
                                                                                }
                                                                                if (!empty($rec['MobileImageName'])) {
                                                                                    if (file_exists($this->config->item("dir_upload_service") . $rec['MobileImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['MobileImageName'])) {
                                                                                        $mobile_image_name = $rec['MobileImageName'];
                                                                                    }
                                                                                }
                                                                ?>

                                                                                <tr role="row" class="odd">
                                                                                    <td><?php echo $service_record['Title']; ?></td>
                                                                                    <td><?php echo $category['Title']; ?></td>
                                                                                    <td>

                                                                                        <?php echo $this->config->item("currency_sign"); ?>
                                                                                        <?php echo $service_record['OfferPrice']; ?></td>
                                                                                    <td><button type="button" id="add_cart" data-service="<?php echo $service_record['PKServiceID'] . '||' . $service_record['Title'] . '||' . $service_record['OfferPrice'] . '||' . $desktop_image_name . '||' . $mobile_image_name . '||' . $category["PKCategoryID"] . '||' . $service_record["Package"] . '||' . $service_record["PreferencesShow"] ?>" class="btn btn-success">ADD</button></td>

                                                                                </tr>
                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>

                                                        <div class="controls">
                                                            <!--
                                                            <select id="service" name="service" class="form-control select2">
                                                                <option value="">Select</option>
                                                            <?php
                                                            if (isset($category_records) && sizeof($category_records) > 0) {
                                                                foreach ($category_records as $category) {
                                                                    if (isset($category['service_records']) && sizeof($category['service_records']) > 0) {
                                                                        foreach ($category['service_records'] as $service_record) {
                                                                            $desktop_image_name = $this->config->item('front_dummy_image_name');
                                                                            $mobile_image_name = $this->config->item('front_dummy_mobile_service_image_name');
                                                                            if (!empty($rec['DesktopImageName'])) {
                                                                                if (file_exists($this->config->item("dir_upload_service") . $rec['DesktopImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['DesktopImageName'])) {
                                                                                    $desktop_image_name = $rec['DesktopImageName'];
                                                                                }
                                                                            }
                                                                            if (!empty($rec['MobileImageName'])) {
                                                                                if (file_exists($this->config->item("dir_upload_service") . $rec['MobileImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['MobileImageName'])) {
                                                                                    $mobile_image_name = $rec['MobileImageName'];
                                                                                }
                                                                            }
                                                                            echo '<option value="' . $service_record['PKServiceID'] . '||' . $service_record['Title'] . '||' . $service_record['OfferPrice'] . '||' . $desktop_image_name . '||' . $mobile_image_name . '||' . $category["PKCategoryID"] . '||' . $service_record["Package"] . '||' . $service_record["PreferencesShow"] . '">(' . $category['Title'] . ') ' . $service_record['Title'] . '</option>';
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                            </select>
                                                            <br clear="all" />
                                                            <button type="button" class="btn btn-small btn-primary" id="add_cart">Add</button>
                                                            <br clear="all" />
                                                            -->



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="notes">
                                                <h3>Order <span class="semi-bold">Notes</span></h3>
                                                <hr />
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Account Notes</label>
                                                        <div class="controls">
                                                            <textarea id="account_notes" name="account_notes" rows="5" class="form-control"><?php echo isset($record['AccountNotes']) ? $record['AccountNotes'] : '' ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Order Notes</label>
                                                        <div class="controls">
                                                            <textarea id="order_notes" name="order_notes" rows="5" class="form-control"><?php echo isset($record['OrderNotes']) ? $record['OrderNotes'] : '' ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-label">Additional Instructions</label>
                                                        <div class="controls">
                                                            <textarea id="additional_instructions" name="additional_instructions" rows="5" class="form-control"><?php echo isset($record['AdditionalInstructions']) ? $record['AdditionalInstructions'] : '' ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 col-lg-12">
                                                <div class="float-right">


                                                    <?php
                                                    if (($record["OrderStarus"] != "Cancel" && $record["PaymentStatus"] != "Cancel")) {

                                                        //&& $record['OnfleetResponse'] == ""
                                                        if ($record['TookanResponse'] == "") {
                                                    ?>
                                                            <a href="javascript:void(0)" class="btn btn-success btn-cons" data-id="<?php echo $record['PKInvoiceID']; ?>" id="btn_reinstate">Reinstate Tookan</a>

                                                    <?php
                                                        }
                                                    }
                                                    ?>

                                                    <?php
                                                    $button_text = "Back";
                                                    if ($type != "View") {
                                                        $button_text = "Cancel";
                                                    ?>
                                                        <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">
                                                            Save
                                                        </button>
                                                    <?php } ?>

                                                    <a href="<?php echo site_url('admin/invoices') ?>" class="btn btn-danger btn-cons" id="btn_option">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-4 col-xs-4 col-lg-4">
                                    <br clear="all" /><br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12" id="service_cart_area">
                                    </div>
                                    <br clear="all" />

                                    <div class="col-md-12 col-xs-12 col-lg-12" id="service_refund_area">

                                    </div>
                                    <br clear="all" />

                                </div>


                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>

<script>
    var pickupDate = "<?php echo $record["PickupDate"]; ?>";
    var pickupTime = "<?php echo $record["PickupTime"]; ?>";
    var deliveryDate = "<?php echo $record["DeliveryDate"]; ?>";
    var deliveryTime = "<?php echo $record["DeliveryTime"]; ?>";
    $(document).ready(function() {
        //$("#pick_date").val(pickupDate);
        // $("#pick_time").val(pickupTime);
        // $("#delivery_date").val(deliveryDate);
        // $("#delivery_time").val(deliveryTime);
        //alert(pickupDate);
        //getPickupTimes();
    });
    $(document).on("click", "#btn_reinstate", function() {

        var id = $(this).data("id");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/invoices/reinstate/") ?>" + id,
            cache: false,
            async: false,
            data: '',
            dataType: "json",
            success: function(data) {
                console.log(data);
                console.log(data["status"]);
                if (data["status"] == 200) {
                    window.location.reload();
                } else {
                    MessageBoxError("#msg_box", data["message"], 2000, "");
                }
            },
            error: function(data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
        return false;
    });

    function getPickupTimes() {
        pickupDate = $("#pick_date").val();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url("admin/invoices/get_pickup_times/?franchise_id=" . $FranchiseID . "&pickup_date=") ?>" + pickupDate,
            data: "",
            cache: false,
            async: false,
            success: function(response) {
                $("#pick_time").html(response);
                // $("#pick_time").val(pickupTime);
                getDeliveryDates()
            },
            error: function(data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
    }


    function getDeliveryDates() {
        pickupTime = $("#pick_time").val();
        pickupDate = $("#pick_date").val();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url("admin/invoices/get_delivery_dates/?franchise_id=" . $FranchiseID . "&pickup_date=") ?>" + pickupDate + "&pickup_time=" + pickupTime,
            data: "",
            cache: false,
            async: false,
            success: function(response) {
                $("#delivery_date").html(response);
                //  $("#delivery_date").val(deliveryDate);
                getDeliveryTimes()
            },
            error: function(data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
    }

    function getDeliveryTimes() {
        pickupTime = $("#pick_time").val();
        pickupDate = $("#pick_date").val();
        deliveryDate = $("#delivery_date").val();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url("admin/invoices/get_delivery_times/?franchise_id=" . $FranchiseID . "&pickup_date=") ?>" + pickupDate + "&pickup_time=" + pickupTime + "&delivery_date=" + deliveryDate,
            data: "",
            cache: false,
            async: false,
            success: function(response) {
                $("#delivery_time").html(response);
                //  $("#delivery_time").val(deliveryTime);

            },
            error: function(data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
    }
</script>

<script type="text/javascript">
    var row_selected = [];
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableElement = $('.recordTable');
    tableElement.dataTable({
        "sPaginationType": "bootstrap",
        "aaSorting": [
            [0, "desc"]
        ],
        'bProcessing': true,
        'bServerSide': true,
        "aLengthMenu": [
            [20, 50, 100, 500, 1000, -1],
            [20, 50, 100, 500, 1000, "All"]
        ],
        "iDisplayLength": 20,
        "cache": false,
        "sScrollX": <?php if (isset($FKFranchiseID) && $FKFranchiseID != "0") { ?> '99%'
    <?php } else { ?> '200%'
    <?php } ?>,
    "sScrollY": "500",
    "oLanguage": {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
    },
    bAutoWidth: false,
    fnPreDrawCallback: function() {
        // Initialize the responsive datatables helper once.
        if (!responsiveHelper) {
            responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
        }
    },
    'ajax': {
        url: '<?php echo site_url('admin/invoices/listener'); ?>',
        type: 'POST'
    },
    <?php if (isset($FKFranchiseID) && $FKFranchiseID > 0) { ?> "columnDefs": [{
            "defaultContent": "-",
            "targets": "_all"
        }],
    <?php } else { ?> 'aoColumns': [{
                'bSearchable': true,
                'bVisible': false
            },
            null, null, null, null, null, null, <?php if (isset($FKFranchiseID) && $FKFranchiseID > 0) { ?><?php } else { ?>null, <?php } ?>
        null, null, null, null, null, null, null, null, null, null, null, null, {
            "bSortable": false,
            "bSearchable": false
        }

        ],
    <?php } ?>



    fnRowCallback: function(nRow) {
        responsiveHelper.createExpandIcon(nRow);
    },
    fnDrawCallback: function(oSettings) {
        responsiveHelper.respond();
    },
    createdRow: function(row, data, index) {
        if ($.inArray(data[0], row_selected) !== -1) {
            $(row).find(".invoice-box").prop("checked", true);
        } else {
            $(row).find(".invoice-box").prop("checked", false);
        }
    }
    });
    <?php
    if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
        echo '';
    } else {
    ?>
        $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/invoices/add') ?>">Add</a>');
    <?php } ?>
    $(document).on('change', '.invoice-box', function() {
        var id = $(this).val();
        var index = $.inArray(id, row_selected);
        if (index === -1) {
            row_selected.push(id);
        } else {
            row_selected.splice(index, 1);
        }
        showButton();
    });
    $(document).on("change", "#location", function() {
        if ($("#location").val() != "") {
            $(".address-area").hide();
            var location_array = $("#location").val().split("||");
            var locker_array = location_array[1].split("|");
            if (locker_array.length > 0) {
                $("#locker").select2("destroy");
                var locker_html = "";
                for (var i = 0; i < locker_array.length; i++) {
                    locker_html += "<option value='" + locker_array[i] + "'>" + locker_array[i] + "</option>";
                }
                $("#locker").html(locker_html);
                $("#locker").select2();
            }
        } else {
            $(".address-area").show();
            $("#locker").select2("destroy");
            $("#locker").html("<option value=''>None</option>");
            $("#locker").select2();
        }
    });
    $(document).on("change", "#member", function() {
        if ($(this).val() != "") {
            var current_value_array = $(this).val().split("||");
            $("#postal_code").val(current_value_array[1]);
        } else {
            $("#postal_code").val("");
        }
    });

    function showButton() {
        $("#order_status_button_area").remove();
        if (row_selected.length > 0) {
            var order_status_button_area = $('<div>').attr("id", "order_status_button_area").appendTo($("#DataTables_Table_0_length"));
            $('<br>').attr("clear", "all").appendTo(order_status_button_area);
            $('<br>').appendTo(order_status_button_area);
            $('<label>').html('Actions : ').appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/invoices/updateorderstatus') ?>").attr("data-type", "Processed").html("Move To Processed").appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/invoices/updateorderstatus') ?>").attr("data-type", "Cancel").html("Move To Cancel").appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/invoices/updateorderstatus') ?>").attr("data-type", "Completed").html("Move To Completed").appendTo(order_status_button_area);
            $('<br>').attr("clear", "all").appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/invoices/updatenotification') ?>").attr("data-type", "Pick").html("Checked Pickup SMS").appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/invoices/updatenotification') ?>").attr("data-type", "Delivery").html("Checked Delivery SMS").appendTo(order_status_button_area);
        }
    }
    $(document).on("click", ".order-status", function() {
        var current_context = $(this);
        var current_text = current_context.attr("data-type");
        current_context.html("<i class='fa fa-spinner fa-spin'></i> " + current_context.html());
        $(".page-content").find("a").addClass("disabled");
        $.ajax({
            type: "POST",
            url: current_context.attr("href"),
            cache: false,
            async: false,
            data: 'type=' + current_text + "&ids=" + JSON.stringify(row_selected),
            success: function(data) {
                console.log(data);
                if (data) {
                    window.location.reload();
                }
            },
            error: function(data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
        return false;
    });
    var session_variable_name = "love2laundry_admin_session_cart_v1";
    var Clear_Storage_Record = function(name) {
        localStorage.removeItem(name);
    }
    var Add_Storage_Record = function(name, value) {
        localStorage.setItem(name, value);
    }
    var Get_Storage_Record = function(name) {
        if (localStorage.getItem(name) == null) {
            return null;
        } else {
            return localStorage.getItem(name);
        }
    }
    var Total_Cart_Price = function() {
        var cart_records = Get_Storage_Record(session_variable_name);
        if (cart_records == null) {
            return "0.00";
        } else {
            var json_array = [];
            var total_price = 0;
            json_array = JSON.parse(cart_records);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                total_price += parseFloat((parseInt(data.qty) * parseFloat(data.price)));
            }
            return total_price.toFixed(2);
        }
    }
    var Current_Item_Price = function(id) {
        var item_total_price = 0;
        var cart_records = Get_Storage_Record(session_variable_name);
        if (cart_records != null) {
            var json_array = [];
            json_array = JSON.parse(cart_records);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                if (data.id == id) {
                    item_total_price = parseFloat(data.price);
                    break;
                }
            }
        }
        return item_total_price;
    }
    var Total_Cart_Item = function() {
        var total_qty = 0;
        var cart_records = Get_Storage_Record(session_variable_name);
        if (cart_records != null) {
            var json_array = [];
            json_array = JSON.parse(cart_records);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                total_qty += parseInt(data.qty);
            }
        }
        return total_qty;
    }
    var Current_Item_Total = function(id) {
        var item_total_qty = 0;
        var cart_records = Get_Storage_Record(session_variable_name);
        if (cart_records != null) {
            var json_array = [];
            json_array = JSON.parse(cart_records);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                if (data.id == id) {
                    item_total_qty = parseInt(data.qty);
                    break;
                }
            }
        }
        return item_total_qty;
    }
    var Add_Cart_Item = function(id, title, price, desktop_img, mobile_img, qty, category_id, package, preference_show) {
        var json_array = [];
        var cart_records = Get_Storage_Record(session_variable_name);
        var current_data = {
            "id": id,
            "title": encodeURIComponent(title),
            "price": price,
            "desktop_img": encodeURIComponent(desktop_img),
            "mobile_img": encodeURIComponent(mobile_img),
            "qty": qty,
            "category_id": category_id,
            "package": package,
            "preference_show": preference_show
        };
        if (cart_records == null) {
            json_array.push(JSON.stringify(current_data));
            Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
        } else {
            json_array = JSON.parse(cart_records);
            var is_found = false;
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                if (data.id == id && data.title == encodeURIComponent(title)) {
                    current_data.qty = (parseInt(data.qty) + parseInt(qty));
                    json_array[i] = JSON.stringify(current_data);
                    is_found = true;
                    break;
                }
            }
            if (is_found == false) {
                json_array.push(JSON.stringify(current_data));
            }
            Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
        }
        Update_Cart_Section();
    }
    var Remove_Cart_Item = function(id, title) {
        var json_array = [];
        var cart_records = Get_Storage_Record(session_variable_name);
        if (cart_records != null) {
            json_array = JSON.parse(cart_records);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                if (data.id == id && data.title == title) {
                    data.qty -= 1;
                    if (data.qty == 0) {
                        json_array.splice(i, 1);
                    } else {
                        json_array[i] = JSON.stringify(data);
                    }
                    break;
                }
            }
            if (json_array.length > 0) {
                Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
            } else {
                Clear_Storage_Record(session_variable_name);
            }
            Update_Cart_Section();
        }
    }
    $(document).on("change", "#discount", function() {
        Update_Cart_Section();
    });
    $(document).on("change", ".preferences", function() {
        Update_Cart_Section();
    });
    var Update_Cart_Section = function() {
        var cart_ul = $("#service_cart_area");
        cart_ul.html("");
        var cart_table = $('<table>').addClass("table table-striped").appendTo(cart_ul);
        var cart_header = $('<thead>').addClass("cf").appendTo(cart_table);
        var cart_header_row = $('<tr>').appendTo(cart_header);
        var cart_header_column_1 = $('<th>').appendTo(cart_header_row);
        $('<strong>').html("Title").appendTo(cart_header_column_1);
        var cart_header_column_2 = $('<th>').appendTo(cart_header_row);
        $('<strong>').html("Qty").appendTo(cart_header_column_2);
        var cart_header_column_3 = $('<th>').appendTo(cart_header_row);
        $('<strong>').html("Price").appendTo(cart_header_column_3);
        if (Total_Cart_Item() != 0) {
            <?php if (isset($type) && $type != "View") { ?>
                var cart_header_column_4 = $('<th>').appendTo(cart_header_row);
                $('<strong>').html("#").appendTo(cart_header_column_4);
            <?php } ?>
            var cart_table_body = $('<tbody>').appendTo(cart_table);
            var json_array = [];
            json_array = JSON.parse(localStorage.getItem(session_variable_name));
            var add_preference = false;
            var package_count = 0;
            var discount_total = parseFloat(0);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                var cart_row = $('<tr>').appendTo(cart_table_body);
                $('<td>').html(decodeURIComponent(data.title)).appendTo(cart_row);
                $('<td>').html(data.qty).appendTo(cart_row);
                $('<td>').html('<?php echo $this->config->item('currency_symbol'); ?>' + parseFloat(data.qty * data.price).toFixed(2)).appendTo(cart_row);
                <?php if (isset($type) && $type != "View") { ?>
                    var remove_anchor = $('<td>').appendTo(cart_row);
                    $('<a>').attr("href", "#").attr("data-id", data.id).attr("data-title", data.title).addClass("remove_cart fa fa-remove").appendTo(remove_anchor);
                <?php } ?>
                if (data.preference_show == "Yes") {
                    add_preference = true;
                }
                if (data.package == "Yes") {
                    package_count += parseInt(data.qty);
                }

                if ($("#discount").val() != "") {
                    var isPackage = data.package;
                    if (isPackage == "No") {
                        discount_total += parseInt(data.qty) * parseFloat(data.price);
                    }

                }


            }
            var sub_total = parseFloat(Total_Cart_Price());
            var preference_total = parseFloat(0);
            if (add_preference == true) {
                $("#preference_area").show();
                if ($(".preferences").length > 0) {
                    $(".preferences").each(function() {
                        var current_preference_array = $(this).val().split("||");
                        if (current_preference_array[3] != "" && current_preference_array[3] != undefined && current_preference_array[3] != null) {
                            var pref_price = parseFloat(current_preference_array[3]);
                            if (current_preference_array[4] == "Yes") {
                                if (package_count > 0) {
                                    pref_price = (parseFloat(pref_price) * parseInt(package_count));
                                }
                            }
                            preference_total += parseFloat(pref_price);
                        }
                    });
                }
            } else {
                $("#preference_area").hide();
            }
            sub_total = sub_total.toFixed(2);
            preference_total = preference_total.toFixed(2);
            var grand_total = parseFloat(sub_total) + parseFloat(preference_total);
            var cart_sub_total_row = $('<tr>').appendTo(cart_table_body);
            var cart_sub_total_column = $('<td>').attr("colspan", "2").appendTo(cart_sub_total_row);
            $('<strong>').html("Sub Total").appendTo(cart_sub_total_column);
            var cart_sub_total_column_item = $('<td>').attr("colspan", "2").appendTo(cart_sub_total_row);
            $('<strong>').html('<?php echo $this->config->item('currency_symbol'); ?>' + sub_total).appendTo(cart_sub_total_column_item);
            var cart_preference_total_row = $('<tr>').appendTo(cart_table_body);
            var cart_preference_total_column = $('<td>').attr("colspan", "2").appendTo(cart_preference_total_row);
            $('<strong>').html("Preference Total").appendTo(cart_preference_total_column);
            var cart_preference_total_column_item = $('<td>').attr("colspan", "2").appendTo(cart_preference_total_row);
            $('<strong>').html('<?php echo $this->config->item('currency_symbol'); ?>' + preference_total).appendTo(cart_preference_total_column_item);
            var cart_discount_total_row = $('<tr>').appendTo(cart_table_body);
            var cart_discount_total_column = $('<td>').attr("colspan", "2").appendTo(cart_discount_total_row);
            if ($("#discount").val() != "") {
                var discount_array = $("#discount").val().split("||");
                if (discount_array[4] == "Percentage") {
                    discount_total = parseFloat((discount_total * discount_array[3]) / 100).toFixed(2);
                    $('<strong>').html(discount_array[1] + " Total (" + discount_array[3] + "%)<br/><small>Code : " + discount_array[2] + "</small>").appendTo(cart_discount_total_column);
                } else {
                    discount_total = parseFloat(discount_array[3]).toFixed(2);
                    $('<strong>').html(discount_array[1] + " Total (<?php echo $this->config->item('currency_symbol'); ?>" + discount_array[3] + ")<br/><small>Code : " + discount_array[2] + "</small>").appendTo(cart_discount_total_column);
                }
            } else {
                $('<strong>').html("Discount Total").appendTo(cart_discount_total_column);
                discount_total = discount_total.toFixed(2);
            }
            var cart_discount_total_column_item = $('<td>').attr("colspan", "2").appendTo(cart_discount_total_row);
            $('<strong>').html('(<?php echo $this->config->item('currency_symbol'); ?>' + discount_total + ')').appendTo(cart_discount_total_column_item);
            grand_total = parseFloat(grand_total - discount_total).toFixed(2);
            $('#refund_service_price').val(grand_total);
            var cart_grand_total_row = $('<tr>').appendTo(cart_table_body);
            var cart_grand_total_column = $('<td>').attr("colspan", "2").appendTo(cart_grand_total_row);
            $('<strong>').html("Grand Total").appendTo(cart_grand_total_column);
            var cart_grand_total_column_item = $('<td>').attr("colspan", "2").appendTo(cart_grand_total_row);
            $('<strong>').html('<?php echo $this->config->item('currency_symbol'); ?>' + grand_total).appendTo(cart_grand_total_column_item);
            $("#session_cart_sub_total").val(sub_total);
            $("#session_cart_preference_total").val(preference_total);
            $("#session_cart_discount_total").val(discount_total);
            $("#session_cart_grand_total").val(grand_total);
            $("#session_cart").val(Get_Storage_Record(session_variable_name));
        } else {
            var cart_table_body = $('<tbody>').appendTo(cart_table);
            var cart_row = $('<tr>').appendTo(cart_table_body);
            $('<td>').attr("colspan", "4").html("No items in cart.<br/>You can add items later and complete your order with minimum amount of <?php echo $this->config->item('currency_symbol'); ?>" + $("#order_minimum_later_amount").val()).appendTo(cart_row);
            $("#session_cart").val("");
            $("#session_cart_sub_total").val($("#order_minimum_later_amount").val());
            $("#session_cart_preference_total").val("");
            $("#session_cart_discount_total").val("");
            $("#session_cart_grand_total").val($("#order_minimum_later_amount").val());
        }
    }
    $(document).on("click", "#add_cart", function() {

        ;
        $("#service_error").remove();
        if ($(this).data("service") != "") {
            var product_array = $(this).data("service").split("||");
            Add_Cart_Item(product_array[0], product_array[1], product_array[2], product_array[3], product_array[4], 1, product_array[5], product_array[6], product_array[7]);
        } else {
            $("<span>").attr("id", "service_error").addClass("error").attr("for", "service").html("Please select service").insertAfter($("#service"));
        }
    });
    $(document).on("click", "#add_custom_service", function() {
        var is_valid = true;
        $("#custom_service_title_error").remove();
        $("#custom_service_price_error").remove();
        if ($("#custom_service_title").val() == "") {
            $("<span>").attr("id", "custom_service_title_error").addClass("error").attr("for", "service").html("Please enter title").insertAfter($("#custom_service_title"));
            is_valid = false;
        }
        if ($("#custom_service_price").val() == "") {
            $("<span>").attr("id", "custom_service_price_error").addClass("error").attr("for", "service").html("Please enter price").insertAfter($("#custom_service_price"));
            is_valid = false;
        }
        if (is_valid) {
            Add_Cart_Item(0, $("#custom_service_title").val(), $("#custom_service_price").val(), "<?php echo $this->config->item('front_dummy_image_name') ?>", "<?php echo $this->config->item('front_dummy_mobile_service_image_name') ?>", 1, 0, "No", "No");
            //$("#custom_service_title").val("");
            // $("#custom_service_price").val("");
        }
    });
    $(document).on("click", ".remove_cart", function() {
        var current_context = $(this);
        Remove_Cart_Item(current_context.attr("data-id"), current_context.attr("data-title"));
        return false;
    });
    $(document).ready(function() {
        Clear_Storage_Record(session_variable_name);
        Update_Cart_Section();
        if ($("#location").length > 0) {
            if ($("#location").val() != "") {
                $(".address-area").hide();
                var location_array = $("#location").val().split("||");
                var locker_array = location_array[1].split("|");
                if (locker_array.length > 0) {
                    $("#locker").select2("destroy");
                    var locker_html = "";
                    for (var i = 0; i < locker_array.length; i++) {
                        locker_html += "<option value='" + locker_array[i] + "'>" + locker_array[i] + "</option>";
                    }
                    $("#locker").html(locker_html);
                    $("#locker").select2();
                }
                <?php
                if (isset($record['Locker']) && !empty($record['Locker'])) {
                ?>
                    $("#locker").select2("val", "<?php echo $record['Locker']; ?>");
                <?php
                }
                ?>
            } else {
                $(".address-area").show();
                $("#locker").select2("destroy");
                $("#locker").html("<option value=''>None</option>");
                $("#locker").select2();
            }
        }
        <?php
        if (isset($admin_message)) {
        ?>
            MessageBoxSuccess("#msg_box", "<?php echo $admin_message ?>", 1000, "");
            scrollToPosition($("#msg_box"));
            <?php
        }
        if (isset($record['services']) && sizeof($record['services']) > 0) {
            foreach ($record['services'] as $rec) {
            ?>
                Add_Cart_Item("<?php echo $rec['FKServiceID'] ?>", "<?php echo $rec['Title'] ?>", "<?php echo $rec['Price'] ?>", "<?php echo $rec['DesktopImageName'] ?>", "<?php echo $rec['MobileImageName'] ?>", "<?php echo $rec['Quantity'] ?>", "<?php echo $rec['FKCategoryID'] ?>", "<?php echo $rec['IsPackage'] ?>", "<?php echo $rec['PreferencesShow'] ?>");
        <?php
            }
        }
        ?>
    });
    <?php
    if (isset($FKFranchiseID) && $FKFranchiseID != "0") {
        echo '';
    } else {
    ?>
        $(document).on("click", "#refund_service_price_btn", function() {
            var refund_service_price = $('#refund_service_price').val();
            $("#refund_service_price_error").remove();
            if (refund_service_price == "") {
                $("<span>").attr("id", "refund_service_price_error").addClass("error").attr("for", "service_price").html("Please enter price").insertAfter($("#refund_service_price"));
            } else {
                $.ajax({
                    type: "POST",
                    url: $('#refund_price_url').val(),
                    cache: false,
                    async: false,
                    data: 'refund_service_price=' + refund_service_price + "&id=" + $('#invoice_id').val(),
                    success: function(data) {

                        if (data) {
                            var data = data.split("||");
                            if (data[0] == "success") {
                                $("<span>").attr("id", "refund_service_price_error").addClass("success").attr("for", "service_price").html(data[2]).insertAfter($("#refund_service_price"));
                            } else if (data[0] == "error") {
                                $("<span>").attr("id", "refund_service_price_error").addClass("error").attr("for", "service_price").html(data[2]).insertAfter($("#refund_service_price"));
                            }
                            getRefunds();
                            return false;
                        }
                    },
                    error: function(data) {
                        $("<span>").attr("id", "refund_service_price_error").addClass("error").attr("for", "service_price").html(data).insertAfter($("#refund_service_price"));
                    }
                });
            }

        });

        function getRefunds() {
            var invoice_id = $('#invoice_id').val();
            if (invoice_id && invoice_id > 0) {
                $("#refund_service_error").remove();
                $("#service_refund_area").html('');
                $.ajax({
                    type: "POST",
                    url: $('#get_refunds_url').val(),
                    cache: false,
                    async: false,
                    data: "id=" + invoice_id,
                    success: function(data) {

                        if (data) {
                            var data = data.split("||");
                            if (data[0] == "success") {
                                $("#service_refund_area").html(data[2]);
                            } else if (data[0] == "error") {
                                $("<span>").attr("id", "refund_service_error").addClass("error").attr("for", "service_refunds").html(data[2]).insertAfter($("#service_refund_area"));
                            }
                            return false;
                        }
                    },
                    error: function(data) {
                        $("<span>").attr("id", "refund_service_error").addClass("error").attr("for", "service_refunds").html(data).insertAfter($("#service_refund_area"));
                    }
                });
            } else {
                return false;
            }
        }

        $(window).load(function() {
            if ($('#invoice_id').val() && $('#invoice_id').val() > 0) {
                getRefunds();
            }
        });
    <?php } ?>


    $(document).ready(function() {
        var table = $('#table_services').DataTable();
    });
    $(document).on('focus', '#table_services input', function() {

        $(this).unbind().bind('keyup', function(e) {

            if (e.keyCode === 13) {
                oTable.search(this.value).draw();
            }
        });
    });
</script>