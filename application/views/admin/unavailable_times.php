<?php
$opening_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $franchise["OpeningTime"]));
$closing_time = date($this->config->item('show_date_time_format'), strtotime(date($this->config->item('show_date_format')) . $franchise["ClosingTime"]));
$gapTime = $franchise["GapTime"];
?>


<div class="content">
    <?php
    if (isset($type)) {
        $add_query_string = "";
        if (isset($franchise_id)) {
            $add_query_string = "?f=" . $franchise_id;
        }
        echo show_admin_bread_crumbs($type, site_url('admin/unavailabletimes') . $add_query_string, isset($parent_page_name) ? $parent_page_name : 'Unavailable Times');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/franchises'), isset($parent_page_name) ? $parent_page_name : 'Unavailable Times');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Unavailable Times</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if (!isset($type)) {
                            ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th width="30%">Date From</th>
                                        <th width="30%">Date To</th>
                                        <th width="20%">Type</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                            </table>
                            <?php
                        } else {
                            $disabled_attribute = '';
                            if ($type == "View") {
                                $disabled_attribute = ' disabled="disabled"';
                            }
                            ?>
                            <form class="form-no-horizontal-spacing" id="frm_unavailable_time" name="frm_unavailable_time" action="<?php echo isset($form_action) ? $form_action : '#' ?>" method="post">
                                <input type="hidden" name="date_id" id="date_id"<?php echo isset($record['PKDateID']) ? ' value="' . $record['PKDateID'] . '"' : '' ?> />
                                <input type="hidden" id="assigned_output" name="assigned_output" />
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Date From<span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="disable_date_from" name="disable_date_from" class="form-control date"<?php
                                            echo $disabled_attribute;
                                            echo isset($record['DisableDateFrom']) ? ' value="' . $record['DisableDateFrom'] . '"' : ''
                                            ?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Date To</label>
                                        <div class="controls">
                                            <input type="text" id="disable_date_to" name="disable_date_to" class="form-control date"<?php
                                            echo $disabled_attribute;
                                            echo isset($record['DisableDateTo']) ? ' value="' . $record['DisableDateTo'] . '"' : ''
                                            ?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Type <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="disable_type" name="disable_type" class="form-control"<?php echo $disabled_attribute; ?>>
                                                <option value="Both"<?php echo isset($record['Type']) ? ($record['Type'] == "Both") ? ' selected="selected"' : '' : '' ?>>Both</option>
                                                <option value="Pickup"<?php echo isset($record['Type']) ? ($record['Type'] == "Pickup") ? ' selected="selected"' : '' : '' ?>>Pickup</option>
                                                <option value="Delivery"<?php echo isset($record['Type']) ? ($record['Type'] == "Delivery") ? ' selected="selected"' : '' : '' ?>>Delivery</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <?php
                                if ($type != "View") {
                                    ?>
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Time <span class="red-color">*</span></label>
                                            <div class="controls">

                                                <?php
                                                //echo $this->config->item('show_date_time_format');
                                                //        die;
                                                ?>

                                                <select id="disable_time" name="disable_time" class="form-control select2">
                                                    <option value="All Day">All Day</option>
                                                    <?php
                                                    $total_hours = ((strtotime($closing_time) - strtotime($opening_time)) / ( 60 * 60 ));
                                                    $i = 0;
                                                    while ($i < 24) {

                                                        $lft = strtotime('+' . $i . ' hours', strtotime($opening_time));
                                                        $start = date('G:i', $lft);

                                                        $search_time = $start . '-' . date('G:i', strtotime('+' . ($gapTime) . ' hours', strtotime($start)));
                                                        echo '<option value="' . $search_time . '">' . $search_time . '</option>';
                                                        $i = $i + $gapTime;
                                                    }
                                                    ?>

                                                    <?php
                                                    foreach ($timings as $gapTime => $t) {
                                                        $total_hours = ((strtotime($t["ClosingTime"]) - strtotime($t["OpeningTime"])) / ( 60 * 60 ));
                                                        $i = 0;
                                                        while ($i < 24) {
                                                            $lft = strtotime('+' . $i . ' hours', strtotime($t["OpeningTime"]));
                                                            $start = date('G:i', $lft);

                                                            $search_time = $start . '-' . date('G:i', strtotime('+' . ($gapTime) . ' hours', strtotime($start)));
                                                            echo '<option value="' . $search_time . '">' . $search_time . '</option>';
                                                            $i = $i + $gapTime;
                                                            ?>


                                                            <?php
                                                        }
                                                    }
                                                    ?>


                                                </select>
                                                <br clear="all" />
                                                <button type="button" class="btn btn-white btn-cons" id="btn_time_add">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div id="specific_timings">
                                         <div class="col-md-3 col-xs-12 col-lg-3">
                                             <div class="form-group">
                                                 <label class="form-label">From </label>
                                                 <select id="specific_from" name="specific_from" class="form-control">
                                    <?php for ($i = 1; $i <= 22; $i++) { ?>
                                                                                                                                                         <option value="<?php echo $i; ?>"><?php echo $i; ?>:00</option>
                                    <?php } ?>
                                                 </select>
                                             </div>
                                         </div>
                                         <div class="col-md-6 col-xs-12 col-lg-3">
                                             <div class="form-group">
                                                 <label class="form-label">Till </label>
                                                 <select id="specific_till" name="specific_till" class="form-control">
                                                     <option value="2">2:00</option>
                                                 </select>
                                             </div>
                                             <label class="form-label"> </label>
                                         </div>
                                     </div>-->
                                <?php } ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="dd" id="nestable_time">
                                        <ol class="dd-list" id="t_list">
                                            <?php
                                            if (isset($record['Navigation']) && sizeof($record['Navigation']) > 0) {
                                                $handle_class = 'dd-handle';
                                                if ($type == "View") {
                                                    $handle_class = "dd-nodrag";
                                                }
                                                foreach ($record['Navigation'] as $rec) {
                                                    $random = mt_rand();
                                                    echo '<li class="dd-item dd3-item" data-text="' . $rec['Time'] . '"><div class="' . $handle_class . ' dd3-handle"></div>';
                                                    echo '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
                                                    echo '<div class="accordion-heading"> <a href="#collapse' . $random . '" data-toggle="collapse" class="accordion-toggle collapsed">' . $rec['Time'] . '<i class="fa fa-plus"></i> </a> </div>';
                                                    echo '<div class="accordion-body collapse" id="collapse' . $random . '" style="height: 0px;"><div class="accordion-inner">';
                                                    if ($type != "View") {
                                                        echo '<div class="control-button"><a class="remove-nav">Remove</a></div>';
                                                    }
                                                    echo '</div></div></div></div></div></li>';
                                                }
                                            }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if ($type != "View") {
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php } ?>
                                        <a href="<?php echo site_url('admin/unavailabletimes') . '?f=' . $franchise_id ?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text ?></a>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">

    $(document).ready(function () {

        $("#specific_from").change(function () {

            var specific_from = parseInt($("#specific_from").val());
            var s = "";
            var option = 0;
            for (var i = specific_from; i <= 22; i++)
            {
                option = i + 1;
                s += "<option value=" + option + ">" + option + ":00</option>";
            }
            $("#specific_till").html(s);
        });

        $("#specific_from").trigger('change');


        //getPickupTimes();

    });

    var getPickupTimes = function () {

        var disable_date_from = "<?php echo date("Y-m-d") ?>";
        var franchise_id = "<?php echo $franchise_id ?>";

        $.ajax({
            type: "get",
            dataType: "json",
            url: "<?php echo base_url(); ?>/api/get_time_records/?id=" + franchise_id + "&date=" + disable_date_from + "&type=Pickup&device_id=1234",
            cache: false,
            success: function (response) {
                var s = "";


                s += "<option value='All Day'>All Day</option>";
                $.each(response.data, function (i, item) {
                    console.log(item)

                    s += "<option value='" + item.Time + "'>" + item.Time + "</option>";



                });
                $("#disable_time").html(s);
            },
            error: function (data) {
                // alert("Some went wrong.");
                console.log(data);
                //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
            }
        });
    }



    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable({
        "sPaginationType": "bootstrap",
        "aaSorting": [[0, "desc"]],
        'bProcessing': true,
        'bServerSide': true,
        "aLengthMenu": [[20, 50, 100, 500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength": 20,
        "cache": false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth: false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax': {
            url: '<?php echo site_url('admin/unavailabletimes/listener'); ?>',
            type: 'POST'
        },
        'aoColumns':
                [
                    {
                        'bSearchable': true,
                        'bVisible': false
                    },
                    null, null, null, {"bSortable": false, "bSearchable": false}

                ],
        fnRowCallback: function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback: function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/unavailabletimes/add') ?>">Add</a>');

    $(document).ready(function () {
<?php
if (isset($admin_message)) {
    ?>
            MessageBoxSuccess("#msg_box", "<?php echo $admin_message ?>", 1000, "");
            scrollToPosition($("#msg_box"));
    <?php
}
?>
    });
</script>