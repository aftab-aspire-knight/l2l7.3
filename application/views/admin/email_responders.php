<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/emailresponders'), isset($parent_page_name)?$parent_page_name:'Emails');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Emails');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Emails</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="15%">Title</th>
                                <th width="20%" data-hide="phone">Subject</th>
                                <th width="15%" data-hide="phone">From</th>
                                <th width="15%" data-hide="phone">To</th>
                                <th width="15%" data-hide="phone,tablet">Status</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_email_responder" name="frm_email_responder" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="responder_id" id="responder_id"<?php echo isset($record['PKResponderID'])?' value="' . $record['PKResponderID'] . '"':''?> />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Email From <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="from_email" name="from_email" class="form-control"<?php echo $disabled_attribute;echo isset($record['FromEmail'])?' value="' . $record['FromEmail'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Email To <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="to_email" name="to_email" class="form-control"<?php echo $disabled_attribute;echo isset($record['ToEmail'])?' value="' . $record['ToEmail'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Subject <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="subject" name="subject" class="form-control"<?php echo $disabled_attribute;echo isset($record['Subject'])?' value="' . $record['Subject'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Content <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <textarea id="content" name="content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Content'])?$record['Content']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-6 col-lg-6">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/emailresponders')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-6 col-lg-6">
                                    <table class="table table-bordered no-more-tables">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="40%">Title</th>
                                            <th class="text-center" width="60%">Tag</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(isset($email_tag_records) && sizeof($email_tag_records) > 0){
                                            foreach($email_tag_records as $record){
                                                echo '<tr>';
                                                echo '<td class="text-center">' . $record['Title'] . '</td>';
                                                echo '<td class="text-center">' . $record['Tag'] . '</td>';
                                                echo '</tr>';
                                            }
                                        }else{
                                            echo '<tr><td class="text-center" colspan="2">No Record Found</td></tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/emailresponders/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,null,null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/emailresponders/add')?>">Add</a>');
    
    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>