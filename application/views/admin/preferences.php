<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/preferences'), isset($parent_page_name)?$parent_page_name:'Preferences');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Preferences');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Preferences</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row" id="grid_panel">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="28%">Title</th>
                                    <th width="27%">Parent Title</th>
                                    <th width="10%">Price</th>
                                    <th width="5%">Position</th>
                                    <th width="10%">Status</th>
                                    <th width="15%">Options</th>
                                </tr>
                                </thead>
                            </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_preference" name="frm_preference" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="preference_id" id="preference_id"<?php echo isset($record['PKPreferenceID'])?' value="' . $record['PKPreferenceID'] . '"':''?> />
                                <?php
                                    if(!isset($record['is_parent'])){
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Parent Preference</label>
                                        <div class="controls">
                                            <select id="parent_preference" name="parent_preference" class="form-control select2"<?php echo $disabled_attribute;?>>
                                                <option value="0">None</option>
                                                <?php
                                                if(isset($preference_records) && sizeof($preference_records) > 0){
                                                    foreach($preference_records as $rec){
                                                        $output = true;
                                                        if(isset($record['PKPreferenceID'])){
                                                            if($record['PKPreferenceID'] == $rec['ID']){
                                                                $output = false;
                                                            }
                                                        }
                                                        if($output){
                                                            $preference_selected = "";
                                                            if(isset($record['ParentPreferenceID'])){
                                                                if($record['ParentPreferenceID'] == $rec['ID']){
                                                                    $preference_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $rec['ID'] . '"'.$preference_selected.'>' . $rec['Title'] . '</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Price <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="price" name="price"  data-a-sep="," data-a-dec="." class="form-control auto"<?php echo $disabled_attribute;echo isset($record['Price'])?' value="' . $record['Price'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-5 col-xs-12 col-lg-5">
                                    <div class="form-group">
                                        <label class="form-label">Price For Package</label>
                                        <div class="controls">
                                            <select id="price_for_package" name="price_for_package" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="No"<?php echo isset($record['PriceForPackage'])?($record['PriceForPackage'] == "No")?' selected="selected"':'':''?>>No</option>
                                                <option value="Yes"<?php echo isset($record['PriceForPackage'])?($record['PriceForPackage'] == "Yes")?' selected="selected"':'':''?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Position</label>
                                        <div class="controls">
                                            <?php
                                            if($type == "View"){
                                            ?>
                                            <input type="text" id="position" name="position" class="col-md-2 only-number"<?php echo $disabled_attribute;echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }else{?>
                                                <input type="text" id="position" name="position" class="col-md-2 spin only-number"<?php echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/preferences')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/preferences/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,
                null,null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/preferences/add')?>">Add</a>');
    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>