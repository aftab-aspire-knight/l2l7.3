<div class="content">
    <?php
    if (isset($type)) {
        echo show_admin_bread_crumbs($type, site_url('admin/notifications'), isset($parent_page_name) ? $parent_page_name : 'Notifications');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name) ? $parent_page_name : 'Notifications');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Notifications</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if (!isset($type)) {
                            ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th width="10%">Title</th>
                                        <th width="25%" data-hide="phone">Content</th>
                                        <th width="15%" data-hide="phone">Publish</th>
                                        <th width="15%" data-hide="phone">Last Publish</th>
                                        <th width="5%" data-hide="phone">Count</th>
                                        <th width="10%" data-hide="phone">Status</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                            </table>
                            <?php
                        } else {
                            $disabled_attribute = '';
                            if ($type == "View") {
                                $disabled_attribute = ' disabled="disabled"';
                            }
                            ?>
                            <form class="form-no-horizontal-spacing" id="frm_notification" name="frm_notification" action="<?php echo isset($form_action) ? $form_action : '#' ?>" method="post">
                                <input type="hidden" name="notification_id" id="notification_id"<?php echo isset($record['PKNotificationID']) ? ' value="' . $record['PKNotificationID'] . '"' : '' ?> />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php
                                            echo $disabled_attribute;
                                            echo isset($record['Title']) ? ' value="' . $record['Title'] . '"' : ' value="Love2laundry"'
                                            ?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Content</label>
                                        <div class="controls">
                                            <textarea maxlength="90"  id="content" name="content" rows="2" cols="20" style="width: 100%;height:50px;" class="form-control"><?php echo isset($record['Content']) ? $record['Content'] : "Your search for an SMS text messaging partner is over. We've worked with more than 165,000 industry-leading businesses and organizations." ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <label class="form-label">Publish Date </label>
                                    <input type="text" id="publish_date" name="publish_date" class="form-control date"<?php
                                    echo $disabled_attribute;
                                    echo isset($record['PublishDateTime']) ? ' value="' . date("Y-m-d", strtotime($record['PublishDateTime'])) . '"' : ''
                                    ?>>
                                </div>

                                <?php
                                if (isset($record['PublishDateTime'])) {
                                    $hour = date("H:i", strtotime($record['PublishDateTime']));
                                } else {
                                    $hour = date("H") . ":00";
                                }
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <label class="form-label">Publish Time </label>
                                    <select id="time" name="time" class="form-control" <?php echo $disabled_attribute; ?> >
                                        <?php
                                        $i = 0;
                                        $selected = "";
                                        while ($i < 24) {
                                            $num_padded = sprintf("%02d", $i) . ":00";

                                            if ($num_padded == $hour) {
                                                $selected = "selected='selected'";
                                            }

                                            echo '<option value="' . $num_padded . '" ' . $selected . ' >' . $num_padded . '</option>';
                                            $i++;
                                            $selected = "";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if ($type != "View") {
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status']) ? ' checked="checked"' : '' ?> />
                                        </div>
                                    <?php } else { ?>
                                        <p><strong><?php echo $record['Status'] ?></strong></p>
                                    <?php } ?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if ($type != "View") {
                                            $button_text = "Cancel";
                                            ?>
                                            <button value="Save" class="btn btn-primary btn-cons" type="submit" name="btn_submit" id="btn_submit">Save</button>
                                            <button value="Publish" class="btn btn-warning btn-cons" type="submit" name="btn_submit" id="btn_submit">Save & Publish</button>
                                        <?php } ?>
                                        <a href="<?php echo site_url('admin/notifications') ?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text ?></a>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">

    $(document).ready(function () {

    });

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable({
        "sPaginationType": "bootstrap",
        "aaSorting": [[0, "desc"]],
        'bProcessing': true,
        'bServerSide': true,
        "aLengthMenu": [[20, 50, 100, 500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength": 20,
        "cache": false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth: false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax': {
            url: '<?php echo site_url('admin/notifications/listener'); ?>',
            type: 'POST'
        },
        'aoColumns':
                [
                    {
                        'bSearchable': true,
                        'bVisible': true
                    },
                    null, null, null,null,null,
                    null, {"bSortable": false, "bSearchable": false}

                ],
        fnRowCallback: function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback: function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/notifications/add') ?>">Add</a>');

    $(document).ready(function () {
<?php
if (isset($record['Status'])) {
    ?>
            ChangeSwitch("#status", "<?php echo $record['Status'] ?>");
    <?php
} else {
    ?>
            ChangeSwitch("#status", "Enabled");
    <?php
}if (isset($admin_message)) {
    ?>
            MessageBoxSuccess("#msg_box", "<?php echo $admin_message ?>", 1000, "");
            scrollToPosition($("#msg_box"));
    <?php
}
?>
    });
</script>