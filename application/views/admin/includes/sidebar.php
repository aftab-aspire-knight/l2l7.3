<div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
    <div class="user-info-wrapper">
        <div class="user-info">
            <div class="greeting">Welcome</div>
            <div class="username"><?php echo $admin_detail['FirstName']?> <span class="semi-bold"><?php echo $admin_detail['LastName']?></span></div>
            <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
        </div>
    </div>
    <!-- END MINI-PROFILE -->

    <!-- BEGIN SIDEBAR MENU -->
    <p class="menu-title">BROWSE </p>
    <ul>
            <li <?php if($current_page_url == 'dashboard'){echo 'class="active"';}?>>
                <a href="<?php echo site_url('admin/dashboard')?>"><i class="icon-custom-home"></i>
                    <span class="title">Dashboard</span></a></li>
        <?php
        foreach ($admin_menus as $menu):
            echo '<li';
            if($current_page_url == $menu['AccessURL']){echo ' class="active"';}
            if(sizeof($menu['children'])>0) {
                if(in_array_r($current_page_url, $menu['children']) !== false){
                    echo ' class=" active open"';
                }
            }
            echo '>';
            if(sizeof($menu['children'])>0){echo '<a href="javascript:;">';}else{echo '<a href="'. site_url('admin/' . $menu['AccessURL'])  . '">';}
            echo '<i class="' . $menu['MenuIcon'] . '"></i><span class="title">' . $menu['Name'] . '</span>';
            if(sizeof($menu['children'])>0){
                $is_open = "";
                $ul_style = "";
                if(in_array_r($current_page_url, $menu['children']) !== false){
                    $is_open = ' open';
                    $ul_style = ' style="display:block;"';
                }
                echo '<span class="arrow ' . $is_open . '"></span></a>';
                echo '<ul class="sub-menu" ' . $ul_style . '>';
                foreach ($menu['children'] as $child_menu):
                    echo '<li';
                    if($current_page_url == $child_menu['AccessURL']){echo ' class="active"';}
                    echo '>';
                    echo '<a href="' . site_url('admin/' . $child_menu['AccessURL']) . '"><i class="' . $child_menu['MenuIcon'] . '"></i><span class="title">' . $child_menu['Name'] . '</span></a></li>';
                endforeach;
                echo '</ul></li>';
            }else{
                echo '</a></li>';
            }
            ?>
        <?php endforeach;
        ?>
        <li <?php if($current_page_url == 'profile'){echo 'class="active"';}?>>
            <a href="<?php echo site_url('admin/profile')?>"><i class="fa fa-pencil"></i>
                <span class="title">Profile</span></a></li>

        <li <?php if($current_page_url == 'dashboard/logout'){echo 'class="active"';}?>>
            <a href="<?php echo site_url('admin/dashboard/logout')?>"><i class="fa fa-sign-out"></i>
                <span class="title">Log Out</span></a></li>
    </ul>

    <a href="#" class="scrollup">Scroll</a>
    <div class="clearfix"></div>
    <!-- END SIDEBAR MENU -->
        </div>
</div>
<div class="footer-widget">
    <div class="pull-right">
        <a href="<?php echo site_url('admin/dashboard/logout')?>"><i class="icon-off"></i></a></div>
</div>