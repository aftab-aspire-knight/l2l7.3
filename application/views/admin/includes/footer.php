<!-- START JAVASCRIPT -->
<?php
echo $this->carabiner->display('JQuery');
echo $this->carabiner->display('Admin_Core_JQuery');
echo $this->carabiner->display('Admin_Func_JQuery');
if ($current_page_url != "") {
    if ($current_page_url == "dashboard") {
        echo $this->carabiner->display('Admin_Auto_Number_JQuery');
        echo $this->carabiner->display('Admin_Data_Table_JQuery');
    } else {
        echo $this->carabiner->display('Admin_Data_Table_JQuery');
        if ($current_page_url == "administrators") {
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Nestable_JQuery');
        } else if ($current_page_url == "discounts") {
            echo $this->carabiner->display('Admin_Auto_Numeric_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
        } else if ($current_page_url == "widgets") {
            ?>
            <script type="text/javascript" src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
            <?php
        } else if ($current_page_url == "navigations" || $current_page_url == "widgetsections") {
            echo $this->carabiner->display('Admin_Nestable_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_Nav_JQuery');
        } else if ($current_page_url == "banners" || $current_page_url == "emailresponders") {
            echo $this->carabiner->display('Admin_Switch_JQuery');
            ?>
            <script type="text/javascript" src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
            <?php
        } else if ($current_page_url == "pages") {
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            ?>
            <script type="text/javascript" src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
            <?php
        } else if ($current_page_url == "subscribers" || $current_page_url == "categories" || $current_page_url == "smsresponders") {
            echo $this->carabiner->display('Admin_Switch_JQuery');
        } else if ($current_page_url == "members") {
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
        } else if ($current_page_url == "unavailabletimes") {
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
            echo $this->carabiner->display('Admin_Nestable_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_Nav_JQuery');
        } else if ($current_page_url == "testimonials") {
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
            ?>
            <script type="text/javascript" src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
            <?php
        } else if ($current_page_url == "services") {
            echo $this->carabiner->display('Admin_Auto_Numeric_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            ?>
            <script type="text/javascript" src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
            <?php
        } else if ($current_page_url == "preferences") {
            echo $this->carabiner->display('Admin_Auto_Numeric_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
        } else if ($current_page_url == "locations" || $current_page_url == "franchises") {
            echo $this->carabiner->display('Admin_Nestable_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Nav_JQuery');
        } else if ($current_page_url == "users") {
            echo $this->carabiner->display('Admin_Nestable_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Nav_JQuery');
        } else if ($current_page_url == "invoices") {
            echo $this->carabiner->display('Admin_Auto_Numeric_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
            ?>
            <!--
                        <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
            -->
            <?php
        } else if ($current_page_url == "payments") {
            echo $this->carabiner->display('Admin_Auto_Numeric_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
            echo $this->carabiner->display('Admin_Data_Table_JQuery');
        } else if ($current_page_url == "invoicefilters") {
            echo $this->carabiner->display('Admin_Select2_JQuery');
            echo $this->carabiner->display('Admin_Auto_Number_JQuery');
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
        } else if ($current_page_url == "drivers") {
            echo '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPzCxWhMOqi3mvnSHcs85MlXZw-NEiERg"></script>';
        }else if ($current_page_url == "notifications") {
            echo $this->carabiner->display('Admin_DatePicker_JQuery');
            echo $this->carabiner->display('Admin_Switch_JQuery');
            echo $this->carabiner->display('Admin_Select2_JQuery');
        }
    }
}
echo $this->carabiner->display('Validation_JQuery');
echo $this->carabiner->display('Admin_Main_JQuery');
if ($current_page_url == "franchises") {
    echo $this->carabiner->display('Admin_Select2_JQuery');
}
?>
<!-- END JAVASCRIPT -->

