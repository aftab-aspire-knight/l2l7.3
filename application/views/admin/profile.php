<div class="content">
    <?php echo show_admin_bread_crumbs('Profile', base_url('admin/dashboard.html'), 'Profile')?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4 id="heading">Profile</h4>
                </div>
                <div class="grid-body no-border">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <form class="form-no-horizontal-spacing" id="frm_profile" name="frm_profile" action="<?php echo site_url('admin/profile/update')?>" method="post">
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">First Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="first_name" id="first_name" type="text" class="form-control" placeholder="First Name"<?php echo isset($record['FirstName'])?' value="' . $record['FirstName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Last Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Last Name"<?php echo isset($record['LastName'])?' value="' . $record['LastName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Email Address <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="email_address" id="email_address" type="text" class="form-control" placeholder="email@address.com"<?php echo isset($record['EmailAddress'])?' value="' . $record['EmailAddress'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <div class="controls">
                                        <input name="u_password" id="u_password" type="password" class="form-control" placeholder="Password" />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" /><br /><br />
                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <div class="float-right">
                                    <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
        if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
    ?>
    });
</script>