<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/drivers'), isset($parent_page_name)?$parent_page_name:'Driver Invoices');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Driver Invoices');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12" id="order_area">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Driver Invoices</span></h4>
                    <div class="tools">
                        <a href="javascript:;" class="reload" id="order_reload"></a>
                    </div>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php if(!isset($type)){ ?>
                            <br clear="all" />
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Invoice</th>
                                    <th width="30%">Type</th>
                                    <th width="40%">#</th>
                                </tr>
                                </thead>
                                <tbody id="order_table_body">
                                <?php
                                if(isset($invoice_records) && sizeof($invoice_records) > 0){
                                    $count = 1;
                                    foreach($invoice_records as $rec){
                                        echo '<tr>';
                                        echo '<td>' . $count . '</td>';
                                        echo '<td>' . $rec['InvoiceNumber'] . '</td>';
                                        echo '<td>' . $rec['OrderType'];
                                        if($rec['OrderType'] == "Delivery"){
                                            echo '<br/>' . $rec['DeliveryTime'] . '</td>';
                                        }else{
                                            echo '<br/>' . $rec['PickupTime'] . '</td>';
                                        }
                                        echo '<td><div class="row">';
                                        echo '<div class="form-group"><a class="label label-success view-order" title="view" href="#"  data-id="' . $rec['PKInvoiceID'] . '">View</a></div>';
                                        echo '<div class="form-group"><a class="label label-success" title="map" href="' . site_url('admin/drivers/map/' . $rec['PKInvoiceID']) . '">Map</a></div>';
                                        echo '<div class="form-group"><a class="label label-success confirm-order" title="confirm" href="#" data-order-type="' . $rec['OrderType'] . '" data-id="' . $rec['PKInvoiceID'] . '">Confirm</a></div>';
                                        echo '</div></td>';
                                        echo '</tr>';
                                        $count += 1;
                                    }
                                }else{
                                    echo '<tr><td colspan="4">No Record Found</td></tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                        }else{
                        ?>
                            <div id="map" ></div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<?php if(isset($type)){?>
    <style type="text/css">
        #map {
            min-height: 300px;
        }
    </style>
<?php }?>
<script type="text/javascript">
    <?php if(!isset($type)){?>
    var LoadOrders = function(){
        var btn_el = $("#order_reload");
        btn_el.addClass("fa-spin");
        var el = $("#order_area");
        blockUI(el);
        $.getJSON("<?php echo site_url('admin/drivers/load')?>", function (response) {
            if (response.type == "Success") {
                var table_body = $("#order_table_body");
                table_body.html("");
                var data = response.data;
                for (var i = 0; i < data.length; i++) {
                    var record = data[i];
                    var row = $("<tr>").appendTo(table_body);
                    $("<td>").html((i+1)).appendTo(row);
                    $("<td>").html(record.InvoiceNumber).appendTo(row);
                    if(record.OrderType == "Delivery"){
                        $("<td>").html(record.OrderType + "<br/>" + record.DeliveryTime).appendTo(row);
                    }else{
                        $("<td>").html(record.OrderType + "<br/>" + record.PickupTime).appendTo(row);
                    }
                    var column_html = '<div class="row">';
                    column_html += '<div class="form-group"><a class="label label-success view-order" title="view" href="#" data-id="' + record.PKInvoiceID + '">View</a></div>';
                    column_html += '<div class="form-group"><a class="label label-success" title="map" href="https://www.love2laundry.com/admin/drivers/map/' + record.PKInvoiceID + '.html">Map</a></div>';
                    column_html += '<div class="form-group"><a class="label label-success confirm-order" title="confirm" href="#" data-order-type="' + record.OrderType + '" data-id="' + record.PKInvoiceID + '">Confirm</a></div>';
                    column_html += '</div>';
                    $("<td>").html(column_html).appendTo(row);
                }
            }
            setTimeout(function () {
                btn_el.removeClass("fa-spin");
                unblockUI(el);
            }, 2000);
        });
    }
    $(document).on("click", "#order_reload", function () {
        LoadOrders();
        $(".view-order").each(function () {
            $(this).html("View");
        });
    });
    var stop_interval = false;
    setInterval(function () {
        if (stop_interval == false) {
            LoadOrders();
        }
    }, 120000);
    $(document).on("click", ".close-order", function () {
        $("#order_row_data").remove();
        $(this).removeClass("close-order").addClass("view-order");
        $(this).html("View");
        stop_interval = true;
        return false;
    });
    $(document).on("click", ".view-order", function () {
        $(".close-order").each(function () {
            $(this).removeClass("close-order").addClass("view-order");
            $(this).html("View");
        });
        $("#order_row_data").remove();
        stop_interval = true;
        var current_context = $(this);
        var row = current_context.parents("tr");
        var row_html = row.html();
        current_context.html("<i class='fa fa-spinner fa-spin'></i>");
        $.post("<?php echo site_url('admin/drivers/view')?>", { id: current_context.attr("data-id") }, function (response) {
                console.log(response);
                if (response.type == "Success") {
                    current_context.removeClass("view-order").addClass("close-order");
                    current_context.html("Close");
                    var data = response.data;
                    var new_row = $('<tr>').attr("id", "order_row_data").insertAfter(row);
                    var new_row_column = $('<td>').attr("colspan", "4").appendTo(new_row);
                    var new_row_area = $('<div>').addClass("row").attr("style","margin-left:5px;").appendTo(new_row_column);
                    var invoice_row_area = $('<div>').addClass("form-group").appendTo(new_row_area);
                    $('<label>').addClass("form-label").html("Invoice Number : ").appendTo(invoice_row_area);
                    $('<span>').addClass("help").html(data.InvoiceNumber).appendTo(invoice_row_area);
                    var name_row_area = $('<div>').addClass("form-group").appendTo(new_row_area);
                    $('<label>').addClass("form-label").html("Name : ").appendTo(name_row_area);
                    $('<span>').addClass("help").html(data.MemberFullName).appendTo(name_row_area);
                    var email_address_row_area = $('<div>').addClass("form-group").appendTo(new_row_area);
                    $('<label>').addClass("form-label").html("Email Address : ").appendTo(email_address_row_area);
                    $('<span>').addClass("help").html(data.MemberEmailAddress).appendTo(email_address_row_area);
                    var phone_row_area = $('<div>').addClass("form-group").appendTo(new_row_area);
                    $('<label>').addClass("form-label").html("Phone No : ").appendTo(phone_row_area);
                    $('<span>').addClass("help").html(data.MemberPhone).appendTo(phone_row_area);
                    var address_row_area = $('<div>').addClass("form-group").appendTo(new_row_area);
                    $('<label>').addClass("form-label").html("Address : ").appendTo(address_row_area);
                    $('<span>').addClass("help").html(data.Address).appendTo(address_row_area);
                } else {
                    row.html("<td colspan='4'><span style='color:red;'>" + response.message + "</span></td>").fadeOut("100", function () {
                        row.html(row_html);
                        setTimeout(function () {
                            stop_interval = false;
                        }, 2000);
                    });
                }
        });
        return false;
    });
    $(document).on("click", ".confirm-order", function () {
        var current_context = $(this);
        var result = confirm('Are you sure you want to confirm order?');
        if(result == true){
            $(".close-order").each(function () {
                $(this).removeClass("close-order").addClass("view-order");
                $(this).html("View");
            });
            $("#order_row_data").remove();
            stop_interval = true;
            var row = current_context.parents("tr");
            var row_html = row.html();
            current_context.html("<i class='fa fa-spinner fa-spin'></i>");
            $.post("<?php echo site_url('admin/drivers/confirm')?>", { id: current_context.attr("data-id"),type: current_context.attr("data-order-type")}, function (response) {
                if (response.type == "Success") {
                    if (row.next().attr("id") != undefined) {
                        row.next().remove();
                    }
                    row.html("<td colspan='4'><span style='color:green;'>" + response.message + "</span></td>").fadeOut("slow", function () {
                        row.remove();
                        setTimeout(function () {
                            stop_interval = false;
                        }, 2000);
                    });
                } else {
                    row.html("<td colspan='4'><span style='color:red;'>" + response.message + "</span></td>").fadeOut("slow", function () {
                        row.html(row_html);
                        setTimeout(function () {
                            stop_interval = false;
                        }, 2000);
                    });
                }
            });
        }
        return false;
    });
    <?php } else {
    if(isset($record['Address'])){
    ?>
    function initMap() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                var directionsService = new google.maps.DirectionsService;
                var directionsDisplay = new google.maps.DirectionsRenderer;
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: {lat: 51.4995576, lng: -0.0549809}
                });
                directionsDisplay.setMap(map);
                directionsService.route({
                    origin: latitude + "," + longitude,
                    destination: "<?php echo $record['Address']?>",
                    travelMode: 'DRIVING'
                }, function (response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                    } else {
                        alert('Directions request failed due to ' + status);
                    }
                });
            }, function () {
                console.log("error 1");
            });
        } else {
            console.log("error 2");
        }
    }
    initMap();
    <?php }
    }
    ?>
</script>
