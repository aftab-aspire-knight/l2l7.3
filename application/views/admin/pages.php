<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/pages'), isset($parent_page_name)?$parent_page_name:'Pages');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Pages');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Pages</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="15%">Template</th>
                                <th width="20%">Title</th>
                                <th width="15%" data-hide="phone">Image</th>
                                <th width="20%" data-hide="phone">URL</th>
                                <th width="10%" data-hide="phone">Status</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_page" name="frm_page" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="page_id" id="page_id"<?php echo isset($record['PKPageID'])?' value="' . $record['PKPageID'] . '"':''?> />
                                <input type="hidden" name="header_pic_path" id="header_pic_path"<?php echo isset($record['HeaderImageName'])?' value="' . $record['HeaderImageName'] . '"':''?> />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Meta Title</label>
                                        <div class="controls">
                                            <input type="text" id="meta_title" name="meta_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['MetaTitle'])?' value="' . $record['MetaTitle'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Meta Keyword</label>
                                        <div class="controls">
                                            <input type="text" id="meta_keyword" name="meta_keyword" class="form-control"<?php echo $disabled_attribute;echo isset($record['MetaKeyword'])?' value="' . $record['MetaKeyword'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Meta Description</label>
                                        <div class="controls">
                                            <textarea id="meta_description" name="meta_description" rows="5" class="form-control"<?php echo $disabled_attribute;?>><?php echo isset($record['MetaDescription'])?$record['MetaDescription']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Is Permanent Redirect?</label>
                                        <div class="controls">
                                            <select id="is_permanent_redirect" name="is_permanent_redirect" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="No"<?php echo isset($record['IsPermanentRedirect'])?($record['IsPermanentRedirect'] == "No")?' selected="selected"':'':''?>>No</option>
                                                <option value="Yes"<?php echo isset($record['IsPermanentRedirect'])?($record['IsPermanentRedirect'] == "Yes")?' selected="selected"':'':''?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-xs-12 col-lg-9">
                                    <div class="form-group">
                                        <label class="form-label">Permanent Redirect URL </label>
                                        <div class="controls">
                                            <input type="text" id="permanent_redirect_url" name="permanent_redirect_url" class="form-control"<?php echo $disabled_attribute;echo isset($record['PermanentRedirectURL'])?' value="' . $record['PermanentRedirectURL'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">No Index Follow Tag</label>
                                        <div class="controls">
                                            <select id="no_index_follow_tag" name="no_index_follow_tag" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="No"<?php echo isset($record['NoIndexFollowTag'])?($record['NoIndexFollowTag'] == "No")?' selected="selected"':'':''?>>No</option>
                                                <option value="Yes"<?php echo isset($record['NoIndexFollowTag'])?($record['NoIndexFollowTag'] == "Yes")?' selected="selected"':'':''?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Template <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="template" name="template" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="None"<?php echo isset($record['Template'])?($record['Template'] == "None")?' selected="selected"':'':''?>>None</option>
                                                <option value="Area"<?php echo isset($record['Template'])?($record['Template'] == "Area")?' selected="selected"':'':''?>>Area</option>
                                                <option value="Area Detail"<?php echo isset($record['Template'])?($record['Template'] == "Area Detail")?' selected="selected"':'':''?>>Area Detail</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">URL <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="access_url" name="access_url" class="form-control"<?php echo $disabled_attribute;echo isset($record['AccessURL'])?' value="' . $record['AccessURL'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="template-area" id="page_template">
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Content <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <textarea id="content" name="content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Content'])?$record['Content']:''?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <?php if($type != "View") { ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Header Image</label>
                                                <div class="controls">
                                                    <input type="file" name="header_image_file" id="header_image_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } if(isset($record['HeaderImageName']) && !empty($record['HeaderImageName'])){ ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6" id="header_image_view">
                                            <div class="form-group">
                                                <label class="form-label">Header Image Preview:</label>
                                                <div class="controls">
                                                    <img id="img_preview" src="<?php echo $this->config->item('path_upload_banner_thumb') . $record['HeaderImageName']?>" alt="Image" width="150" height="120" /><br />
                                                    <?php if($type != "View"){ ?>
                                                    <a href="#" id="header_remove_image" class="btn btn-danger btn-cons" style="display:block; width:120px; margin-top:20px">Remove</a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="template-area hide-area" id="area_template">
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Area Detail Pages</label>
                                            <div class="controls">
                                                <select id="area_detail_pages" name="area_detail_pages[]" multiple class="form-control select2"<?php echo $disabled_attribute;?>>
                                                    <?php
                                                    if(isset($area_detail_page_records) && sizeof($area_detail_page_records) > 0){
                                                        foreach($area_detail_page_records as $rec){
                                                            $area_detail_page_selected = "";
                                                            if(isset($record['area_page_array'])){
                                                                if(in_array($rec['ID'],$record['area_page_array'])){
                                                                    $area_detail_page_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $rec['ID'] . '"'.$area_detail_page_selected.'>' . $rec['Title'] . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                </div>
                                <br clear="all" />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Latitude <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="latitude" name="latitude" class="form-control"<?php echo $disabled_attribute;echo isset($record['Latitude'])?' value="' . $record['Latitude'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Longitude <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="longitude" name="longitude" class="form-control"<?php echo $disabled_attribute;echo isset($record['Longitude'])?' value="' . $record['Longitude'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="template-area hide-area" id="area_main_template">
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Header Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="header_title" name="header_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['HeaderTitle'])?' value="' . $record['HeaderTitle'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Section 1 Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="section_1_title" name="section_1_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Section_1_Title'])?' value="' . $record['Section_1_Title'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Section 1 Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="section_1_title" name="section_1_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Section_1_Title'])?' value="' . $record['Section_1_Title'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Section 1 Content <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <textarea id="section_1_content" name="section_1_content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Section_1_Content'])?$record['Section_1_Content']:''?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <input type="hidden" name="section_1_pic_path" id="section_1_pic_path"<?php echo isset($record['Section_1_Image_Name'])?' value="' . $record['Section_1_Image_Name'] . '"':''?> />
                                    <?php if($type != "View") { ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Image</label>
                                                <div class="controls">
                                                    <input type="file" name="section_1_image_file" id="section_1_image_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } if(isset($record['Section_1_Image_Name']) && !empty($record['Section_1_Image_Name'])){ ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6" id="section_1_image_view">
                                            <div class="form-group">
                                                <label class="form-label">Image Preview:</label>
                                                <div class="controls">
                                                    <img id="img_preview" src="<?php echo $this->config->item('path_upload_banner_thumb') . $record['Section_1_Image_Name']?>" alt="Image" width="150" height="120" /><br />
                                                    <?php if($type != "View"){ ?>
                                                        <a href="#" id="section_1_remove_image" class="btn btn-danger btn-cons" style="display:block; width:120px; margin-top:20px">Remove</a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <br clear="all" /><br />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Section 2 Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="section_2_title" name="section_2_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Section_2_Title'])?' value="' . $record['Section_2_Title'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <div class="col-md-12 col-xs-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-label">Section 2 Content <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <textarea id="section_2_content" name="section_2_content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Section_2_Content'])?$record['Section_2_Content']:''?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <br clear="all" />
                                    <input type="hidden" name="section_2_pic_path" id="section_2_pic_path"<?php echo isset($record['Section_2_Image_Name'])?' value="' . $record['Section_2_Image_Name'] . '"':''?> />
                                    <?php if($type != "View") { ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Image</label>
                                                <div class="controls">
                                                    <input type="file" name="section_2_image_file" id="section_2_image_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } if(isset($record['Section_2_Image_Name']) && !empty($record['Section_2_Image_Name'])){ ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6" id="section_2_image_view">
                                            <div class="form-group">
                                                <label class="form-label">Image Preview:</label>
                                                <div class="controls">
                                                    <img id="img_preview" src="<?php echo $this->config->item('path_upload_banner_thumb') . $record['Section_2_Image_Name']?>" alt="Image" width="150" height="120" /><br />
                                                    <?php if($type != "View"){ ?>
                                                        <a href="#" id="section_2_remove_image" class="btn btn-danger btn-cons" style="display:block; width:120px; margin-top:20px">Remove</a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <br clear="all" /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/pages')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');
    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/pages/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
        [
            {
                'bSearchable': true,
                'bVisible'   : true
            },
            null,null,null,null,null,{ "bSortable": false,"bSearchable":false }
        ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/pages/add')?>">Add</a>');
    $(document).on("click","#header_remove_image",function(){
        $("#header_pic_path").val("");
        $("#header_image_view").hide();
        return false;
    });
    $(document).on("click","#section_1_remove_image",function(){
        $("#section_1_pic_path").val("");
        $("#section_1_image_view").hide();
        return false;
    });
    $(document).on("click","#section_2_remove_image",function(){
        $("#section_2_pic_path").val("");
        $("#section_2_image_view").hide();
        return false;
    });
    $(document).on("change","#template",function(){
        $(".template-area").hide();
        if($(this).val() == "None"){
            $("#page_template").show();
        }else{
            $("#area_main_template").show();
            if($(this).val() == "Area"){
                $("#area_template").show();
            }
        }
    });
    $(document).ready(function(){
        <?php if(isset($record['Status'])){ ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php }else{ ?>
        ChangeSwitch("#status","Enabled");
        <?php } if(isset($record['Template'])){ ?>
            $("#template").trigger("change");
        <?php } if(isset($admin_message)){ ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php } ?>
    });
    $(document).on("keyup","#title",function(){
        var current_context = $(this);
        if($("#page_id").val() == ""){
            delay(function() {
                $.post("<?php echo site_url('admin/pages/rewrite')?>", {title: current_context.val()}, function (data) {
                    $("#access_url").val(data);
                });
            },1000);
        }
    });
    $(document).on("keyup","#access_url",function(e){
        var current_context = $(this);
        if(e.keyCode != 8){
            delay(function() {
                $.post("<?php echo site_url('admin/pages/rewrite')?>",{ id:$("#page_id").val(),url:current_context.val() },function(data){
                    $("#access_url").val(data);
                });
            },1000);
        }
    });
</script>