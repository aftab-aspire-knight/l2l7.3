<div class="content">
    <?php
    echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Invoice Filters');
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Invoice Filters</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        $search_action = site_url('admin/invoicefilters/search');
                        if(isset($invoice_records) && sizeof($invoice_records) > 0){
                            $search_action = site_url('admin/invoicefilters/export');
                        }
                        $field_area = "";
                        if(isset($is_post)){
                            $field_area = "hide-area";
                        }
                        ?>
                        <form class="form-no-horizontal-spacing" id="frm_invoice_filter" name="frm_invoice_filter" action="<?php echo $search_action?>" method="post">
                            <div class="<?php echo $field_area?>">
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Franchise</label>
                                    <div class="controls">
                                        <select id="franchise" name="franchise" class="form-control select2">
                                            <option value="">None</option>
                                            <?php
                                            if(isset($franchise_records) && sizeof($franchise_records) > 0){
                                                foreach($franchise_records as $rec){
                                                    $franchise_selected = "";
                                                    if(isset($franchise)){
                                                        if($franchise == $rec['ID']){
                                                            $franchise_selected = ' selected="selected"';
                                                        }
                                                    }
                                                    echo '<option value="' . $rec['ID'] . '"' . $franchise_selected . '>' . $rec['Title'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Member</label>
                                    <div class="controls">
                                        <select id="member" name="member" class="form-control select2">
                                            <option value="">None</option>
                                            <?php
                                            if(isset($member_records) && sizeof($member_records) > 0){
                                                foreach($member_records as $rec){
                                                    $member_selected = "";
                                                    if(isset($member)){
                                                        if($member == $rec['ID']){
                                                            $member_selected = ' selected="selected"';
                                                        }
                                                    }
                                                    echo '<option value="' . $rec['ID'] . '"' . $member_selected . '>' . $rec['EmailAddress'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                <br clear="all" />
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Pick Date</label>
                                        <div class="controls">
                                            <input name="pick_date" id="pick_date" type="text" class="form-control i-date"<?php echo isset($pick_date)?' value="' . $pick_date . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Pick Time</label>
                                        <div class="controls">
                                            <select id="pick_time" name="pick_time" class="form-control">
                                                <option value="">All Day</option>
                                                <?php
                                                $opening_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 05:00 am"));
                                                $closing_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 12:00 pm"));
                                                $total_hours = ((strtotime($closing_time) - strtotime($opening_time)) / ( 60 * 60 ));
                                                if($total_hours > 0) {
                                                    for($i=0;$i<$total_hours;$i++){
                                                        $search_time = date('h:i',strtotime('+' . $i . ' hours',strtotime($opening_time))) . '-' . date('h:i a',strtotime('+' . ($i + 1) . ' hours',strtotime($opening_time)));
                                                        $time_selected = "";
                                                        if(isset($pick_time)){
                                                            if($pick_time == $search_time){
                                                                $time_selected = ' selected="selected"';
                                                            }
                                                        }
                                                        echo '<option value="' . $search_time . '"'.$time_selected.'>' . $search_time . '</option>';
                                                    }
                                                }
                                                $opening_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 01:00 pm"));
                                                $closing_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 11:00 pm"));
                                                $total_hours = ((strtotime($closing_time) - strtotime($opening_time)) / ( 60 * 60 ));
                                                if($total_hours > 0) {
                                                    for($i=0;$i<$total_hours;$i++){
                                                        $search_time = date('h:i',strtotime('+' . $i . ' hours',strtotime($opening_time))) . '-' . date('h:i a',strtotime('+' . ($i + 1) . ' hours',strtotime($opening_time)));
                                                        $time_selected = "";
                                                        if(isset($pick_time)){
                                                            if($pick_time == $search_time){
                                                                $time_selected = ' selected="selected"';
                                                            }
                                                        }
                                                        echo '<option value="' . $search_time . '"'.$time_selected.'>' . $search_time . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Delivery Date</label>
                                        <div class="controls">
                                            <input name="delivery_date" id="delivery_date" type="text" class="form-control i-date"<?php echo isset($delivery_date)?' value="' . $delivery_date . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Delivery Time</label>
                                        <div class="controls">
                                            <select id="delivery_time" name="delivery_time" class="form-control">
                                                <option value="">All Day</option>
                                                <?php
                                                $opening_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 05:00 am"));
                                                $closing_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 12:00 pm"));
                                                $total_hours = ((strtotime($closing_time) - strtotime($opening_time)) / ( 60 * 60 ));
                                                if($total_hours > 0) {
                                                    for($i=0;$i<$total_hours;$i++){
                                                        $search_time = date('h:i',strtotime('+' . $i . ' hours',strtotime($opening_time))) . '-' . date('h:i a',strtotime('+' . ($i + 1) . ' hours',strtotime($opening_time)));
                                                        $time_selected = "";
                                                        if(isset($delivery_time)){
                                                            if($delivery_time == $search_time){
                                                                $time_selected = ' selected="selected"';
                                                            }
                                                        }
                                                        echo '<option value="' . $search_time . '"'.$time_selected.'>' . $search_time . '</option>';
                                                    }
                                                }
                                                $opening_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 01:00 pm"));
                                                $closing_time = date($this->config->item('show_date_time_format'),strtotime(date($this->config->item('show_date_format')) . " 11:00 pm"));
                                                $total_hours = ((strtotime($closing_time) - strtotime($opening_time)) / ( 60 * 60 ));
                                                if($total_hours > 0) {
                                                    for($i=0;$i<$total_hours;$i++){
                                                        $search_time = date('h:i',strtotime('+' . $i . ' hours',strtotime($opening_time))) . '-' . date('h:i a',strtotime('+' . ($i + 1) . ' hours',strtotime($opening_time)));
                                                        $time_selected = "";
                                                        if(isset($delivery_time)){
                                                            if($delivery_time == $search_time){
                                                                $time_selected = ' selected="selected"';
                                                            }
                                                        }
                                                        echo '<option value="' . $search_time . '"'.$time_selected.'>' . $search_time . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">Order From</label>
                                    <div class="controls">
                                        <select id="order_post_from" name="order_post_from" class="form-control">
                                            <option value=""<?php echo isset($order_post_from)?($order_post_from == "")?' selected="selected"':'':''?>>Both</option>
                                            <option value="Desktop"<?php echo isset($order_post_from)?($order_post_from == "Desktop")?' selected="selected"':'':''?>>Desktop</option>
                                            <option value="Mobile"<?php echo isset($order_post_from)?($order_post_from == "Mobile")?' selected="selected"':'':''?>>Mobile</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Payment Status</label>
                                        <div class="controls">
                                            <select id="payment_status" name="payment_status" class="form-control">
                                                <option value=""<?php echo isset($payment_status)?($payment_status == "")?' selected="selected"':'':''?>>All</option>
                                                <option value="Pending"<?php echo isset($payment_status)?($payment_status == "Pending")?' selected="selected"':'':''?>>Pending</option>
                                                <option value="Processed"<?php echo isset($payment_status)?($payment_status == "Processed")?' selected="selected"':'':''?>>Processed</option>
                                                <option value="Cancel"<?php echo isset($payment_status)?($payment_status == "Cancel")?' selected="selected"':'':''?>>Cancel</option>
                                                <option value="Completed"<?php echo isset($payment_status)?($payment_status == "Completed")?' selected="selected"':'':''?>>Completed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Order Status</label>
                                        <div class="controls">
                                            <select id="order_status" name="order_status" class="form-control">
                                                <option value=""<?php echo isset($order_status)?($order_status == "")?' selected="selected"':'':''?>>All</option>
                                                <option value="Pending"<?php echo isset($order_status)?($order_status == "Pending")?' selected="selected"':'':''?>>Pending</option>
                                                <option value="Processed"<?php echo isset($order_status)?($order_status == "Processed")?' selected="selected"':'':''?>>Processed</option>
                                                <option value="Cancel"<?php echo isset($order_status)?($order_status == "Cancel")?' selected="selected"':'':''?>>Cancel</option>
                                                <option value="Completed"<?php echo isset($order_status)?($order_status == "Completed")?' selected="selected"':'':''?>>Completed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Created Date</label>
                                        <div class="controls">
                                            <input name="created_date_time" id="created_date_time" type="text" class="form-control i-date"<?php echo isset($created_date_time)?' value="' . $created_date_time . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                            </div>
                            <?php
                            if(isset($is_post)){
                            ?>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">&nbsp;</label>
                                    <div class="controls">
                                        <a class="btn btn-primary btn-cons" href="<?php echo site_url('admin/invoicefilters')?>">Clear</a>
                                        <?php
                                        if(isset($invoice_records) && sizeof($invoice_records) > 0){
                                         ?>
                                            &nbsp;&nbsp;<button class="btn btn-primary btn-cons" type="submit">Export</button> 
                                        <?php
                                        } 
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php }else { ?>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">&nbsp;</label>
                                        <div class="controls">
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                               ?>
                        </form>
                        <?php
                            if(isset($is_post)){
                         ?>
                                <br clear="all" />
                                <div class="dataTables_scrollBody" style="overflow: auto; height: 500px; width: 100%;">
                                <table class="table table-bordered no-more-tables" style="width:1200px;">
                                    <thead>
                                    <tr>
                                        <th width="3%">Invoice</th>
                                        <th width="12%">Franchise</th>
                                        <th width="13%">Member</th>
                                        <th width="7%">Phone</th>
                                        <th width="8%">P-Status</th>
                                        <th width="15%">Address</th>
                                        <th width="10%">P-Date</th>
                                        <th width="10%">P-Time</th>
                                        <th width="10%">D-Date</th>
                                        <th width="10%">D-Time</th>
                                        <th width="2%" class="no-print">View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(isset($invoice_records) && sizeof($invoice_records) > 0){
                                                foreach($invoice_records as $rec){
                                                    echo '<tr>';
                                                    echo '<td>' . $rec['InvoiceNumber'] . '</td>';
                                                    echo '<td>' . $rec['FranchiseTitle'] . '</td>';
                                                    echo '<td>' . $rec['MemberName'] . '</td>';
                                                    echo '<td>' . $rec['Phone'] . '</td>';
                                                    echo '<td>' . $rec['PaymentStatus'] . '</td>';
                                                    $address_html = '';
                                                    if($rec['Location'] != "Add" && $rec['Location'] != "0" && $rec['Location'] != null){
                                                        $address_html = $rec['Location'] . '<br/>' . $rec['Locker'];
                                                    }else{
                                                        $address_html = $rec['BuildingName'] . '<br/>' . $rec['StreetName'] . '<br/>' . $rec['PostalCode'] . '<br/>' . $rec['Town'];
                                                    }
                                                    echo '<td>' . $address_html . '</td>';
                                                    echo '<td>' . date('d-m-Y',strtotime($rec['PickupDate'])) . '</td>';
                                                    echo '<td>' . $rec['PickupTime'] . '</td>';
                                                    echo '<td>' . date('d-m-Y',strtotime($rec['DeliveryDate'])) . '</td>';
                                                    echo '<td>' . $rec['DeliveryTime'] . '</td>';
                                                    echo '<td class="no-print"><div class="row"><div class="col-md-12 col-xs-12 col-lg-12 cursor text-center"><a target="_blank" title="view" href="' . site_url('order-detail/' . $rec['PKInvoiceID']) . '?t=v"><i class="fa fa-eye"></i></a></div></div></td>';
                                                    echo '</tr>';
                                                }
                                            }else{
                                                echo '<tr><td colspan="7">No Record Found</td></tr>';
                                            }
                                        ?>
                                    </tbody>
                                </table>
                                </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>