<div class="content">
    <?php echo show_admin_bread_crumbs('Settings', base_url('admin/dashboard.html'), isset($parent_page_name)?$parent_page_name:'Settings')?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Settings</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <div class="col-md-12 col-xs-12 col-lg-12">
                            <form class="form-no-horizontal-spacing" id="frm_setting" name="frm_setting" action="<?php echo site_url('admin/settings/update')?>" method="post">
                                <input type="hidden" id="active_tab" name="active_tab" value="#tab_website" />
                                <p>Please fill all the required fields with <span class="red-color">*</span> mark.<br/>Use <span class="red-color">#</span> for blank.</p>
                                <br clear="all" />
                                <ul class="nav nav-pills" id="setting_tab">
                                    <li class="active"><a id="btn_website" href="#tab_website">Website</a></li>
                                    <li><a id="btn_email" href="#tab_email">Email</a></li>
                                    <li><a id="btn_stripe" href="#tab_order">Order</a></li>
                                    <li><a id="btn_social" href="#tab_social">Social Links</a></li>
                                    <li><a id="btn_stripe" href="#tab_stripe">Stripe Payment</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_website">
                                        <div class="form-group">
                                            <label class="form-label">Website Name <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="website_name" name="website_name" class="form-control"<?php echo isset($record['website_name_content'])?' value="' . $record['website_name_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Website URL Address <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="website_url_address" name="website_url_address" class="form-control"<?php echo isset($record['website_url_address_content'])?' value="' . $record['website_url_address_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Header Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="header_title" name="header_title" class="form-control"<?php echo isset($record['header_content'])?' value="' . $record['header_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Footer Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="footer_title" name="footer_title" class="form-control"<?php echo isset($record['footer_content'])?' value="' . $record['footer_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Google Play Store Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="google_play_url" name="google_play_url" class="form-control"<?php echo isset($record['google_play_content'])?' value="' . $record['google_play_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Apple Store Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="apple_store_url" name="apple_store_url" class="form-control"<?php echo isset($record['apple_store_content'])?' value="' . $record['apple_store_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Google Analytics ID <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="google_analytics_id" name="google_analytics_id" class="form-control"<?php echo isset($record['google_analytics_content'])?' value="' . $record['google_analytics_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Telephone No <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="telephone_no" name="telephone_no" class="form-control"<?php echo isset($record['telephone_content'])?' value="' . $record['telephone_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Email Address <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="email_address" name="email_address" class="form-control"<?php echo isset($record['email_content'])?' value="' . $record['email_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Android App Version <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="android_app_version" name="android_app_version" class="form-control"<?php echo isset($record['android_app_version_content'])?' value="' . $record['android_app_version_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">IOS App Version <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="ios_app_version" name="ios_app_version" class="form-control"<?php echo isset($record['ios_app_version_content'])?' value="' . $record['ios_app_version_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Status <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <select id="website_status" name="website_status" class="form-control">
                                                    <option value="Online"<?php echo isset($record['website_status_content'])?($record['website_status_content'] == "Online")?' selected="selected"':'':''?>>Online</option>
                                                    <option value="Offline"<?php echo isset($record['website_status_content'])?($record['website_status_content'] == "Offline")?' selected="selected"':'':''?>>Offline</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_email">
                                        <div class="form-group">
                                            <label class="form-label">Email Sender <span class="red-color">*</span></label>
                                            <span class="help">only one email address</span>
                                            <div class="controls">
                                                <input type="text" id="website_email_sender" name="website_email_sender" class="form-control"<?php echo isset($record['website_email_sender_content'])?' value="' . $record['website_email_sender_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Email Receivers <span class="red-color">*</span></label>
                                            <span class="help">multiple email address separated with <span class="red-color">comma ,</span></span>
                                            <div class="controls">
                                                <input type="text" id="website_email_receivers" name="website_email_receivers" class="form-control"<?php echo isset($record['website_email_receivers_content'])?' value="' . $record['website_email_receivers_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Email Footer Title <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="email_footer_title" name="email_footer_title" class="form-control"<?php echo isset($record['email_footer_title_content'])?' value="' . $record['email_footer_title_content'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_order">
                                        <div class="form-group">
                                            <label class="form-label">Minimum Order Amount (Item Later) <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="minimum_order_amount_later" name="minimum_order_amount_later" class="form-control only-number"<?php echo isset($record['minimum_order_amount_later_content'])?' value="' . $record['minimum_order_amount_later_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Minimum Order Amount <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="minimum_order_amount" name="minimum_order_amount" class="form-control only-number"<?php echo isset($record['minimum_order_amount_content'])?' value="' . $record['minimum_order_amount_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Referral Code Amount <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="referral_code_amount" name="referral_code_amount" class="form-control only-number"<?php echo isset($record['referral_code_amount_content'])?' value="' . $record['referral_code_amount_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Loyalty Code Amount <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="loyalty_code_amount" name="loyalty_code_amount" class="form-control only-number"<?php echo isset($record['loyalty_code_amount_content'])?' value="' . $record['loyalty_code_amount_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Minimum Loyalty Points <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="minimum_loyalty_points" name="minimum_loyalty_points" class="form-control only-number"<?php echo isset($record['minimum_loyalty_points_content'])?' value="' . $record['minimum_loyalty_points_content'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_social">
                                        <div class="form-group">
                                            <label class="form-label">Facebook Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="facebook_url" name="facebook_url" class="form-control"<?php echo isset($record['facebook_content'])?' value="' . $record['facebook_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Twitter Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="twitter_url" name="twitter_url" class="form-control"<?php echo isset($record['twitter_content'])?' value="' . $record['twitter_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Google Plus Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="google_plus_url" name="google_plus_url" class="form-control"<?php echo isset($record['google_plus_content'])?' value="' . $record['google_plus_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Instagram Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="instagram_url" name="instagram_url" class="form-control"<?php echo isset($record['instagram_content'])?' value="' . $record['instagram_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">You Tube Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="you_tube_url" name="you_tube_url" class="form-control"<?php echo isset($record['you_tube_content'])?' value="' . $record['you_tube_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Pinterest Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="pinterest_url" name="pinterest_url" class="form-control"<?php echo isset($record['pinterest_content'])?' value="' . $record['pinterest_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">LinkedIn Page URL <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="linkedin_url" name="linkedin_url" class="form-control"<?php echo isset($record['linkedin_content'])?' value="' . $record['linkedin_content'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_stripe">
                                        <div class="form-group">
                                            <label class="form-label">Mode <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <select id="stripe_payment_mode" name="stripe_payment_mode" class="form-control">
                                                    <option value="Test"<?php echo isset($record['stripe_payment_mode_content'])?($record['stripe_payment_mode_content'] == "Test")?' selected="selected"':'':''?>>Test</option>
                                                    <option value="Live"<?php echo isset($record['stripe_payment_mode_content'])?($record['stripe_payment_mode_content'] == "Live")?' selected="selected"':'':''?>>Live</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">JQuery Test Key <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="stripe_jquery_test_key" name="stripe_jquery_test_key" class="form-control"<?php echo isset($record['stripe_jquery_test_key_content'])?' value="' . $record['stripe_jquery_test_key_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">API Test Key <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="stripe_api_test_key" name="stripe_api_test_key" class="form-control"<?php echo isset($record['stripe_api_test_key_content'])?' value="' . $record['stripe_api_test_key_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">JQuery Live Key <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="stripe_jquery_live_key" name="stripe_jquery_live_key" class="form-control"<?php echo isset($record['stripe_jquery_live_key_content'])?' value="' . $record['stripe_jquery_live_key_content'] . '"':''?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">API Live Key <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="text" id="stripe_api_live_key" name="stripe_api_live_key" class="form-control"<?php echo isset($record['stripe_api_live_key_content'])?' value="' . $record['stripe_api_live_key_content'] . '"':''?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
        if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>