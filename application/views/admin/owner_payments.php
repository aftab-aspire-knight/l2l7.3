<div class="content">
    <?php
    if (isset($type)) {
        echo show_admin_bread_crumbs($type, site_url('admin/invoices'), isset($parent_page_name) ? $parent_page_name : 'Invoices');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name) ? $parent_page_name : 'Invoices');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Invoices</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <!--Order ID
                        Customer Name
                        Pickup Date and Time
                        Order Date
                        Owner Payment Status
                        Amount-->
                        <table  class="table table-striped dataTable recordTable">
                            <thead>
                                <tr>


                                    <th width="10%">#</th>
                                    <!--<th width="5%">From</th>-->
                                    <th width="10%">Invoice</th>
                                    <!--<th width="8%">Franchise</th>-->
                                    <th width="10%">Customer Name</th>
                                    <th width="10%">Owner Payment Status</th>                                

<!--<th width="5%">Method</th>-->
                                    <th width="5%">P-Status</th>
                                    <th width="5%">O-Status</th>
                                    <th width="10%">Total(<?php echo $this->config->item('currency_sign') ?>)</th>
                                    <th width="10%">P-Date</th>
                                    <th width="10%">P-Time</th>
                                    <!--<th width="3%">P-N</th>-->
                                    <!--<th width="7%">D-Date</th>-->
                                    <!--<th width="7%">D-Time</th>-->
                                    <!--<th width="3%">D-N</th>-->
                                    <!--<th width="10%">C-Time</th>-->
                                    <!--<th width="4%">U-Time</th>-->
                                    <th width="10%">Options</th>
                                </tr>
                            </thead>
                        </table>

                        <div class="form-no-horizontal-spacing" id="frm_franchise_search" name="frm_franchise_search"
                             >
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Franchise <span class="red-color">*</span></label>

                                    <div class="controls">
                                        <select id="franchise_id" name="franchise_id" class="form-control select2">
                                            <option value="">Select</option>
                                            <?php
                                            if (isset($franchise_records) && sizeof($franchise_records) > 0) {
                                                foreach ($franchise_records as $rec) {
                                                    $franchise_selected = "";

                                                    echo '<option value="' . $rec['ID'] . '"' . $member_selected . '>' . $rec['Title'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">Start Date <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="start_date" id="start_date" type="text" value="" readonly="readonly" class="form-control i-date" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">End Date <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="end_date" id="end_date" type="text" value="" readonly="readonly" class="form-control i-date" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">Search By <span class="red-color">*</span></label>
                                    <div class="controls">

                                        <select name="date_type" id="date_type" class="form-select" aria-label="Default select example">
                                            <option value="CreatedDateTime">Ordered Date</option>
                                            <option selected="selected" value="PickupDate">Pickup Date</option>
                                            <option value="DeliveryDate">Delivery Date</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">Franchise <span class="red-color">*</span></label>

                                    <div class="controls">
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" name="franchise_paid" id="franchise_paid">
                                            <label for="franchise_paid">Paid <span class="red-color">*</span></label>
                                        </div>
                                        <div class="checkbox check-primary">
                                            <input type="checkbox" name="franchise_not_paid" id="franchise_not_paid">
                                            <label for="franchise_not_paid">Not Paid<span class="red-color">*</span></label>
                                        </div>

                                    </div>

                                </div>
                            </div>


                            <br clear="all"/>

                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <div class="col-md-6 col-lg-6 col-xs-12"><a class="btn btn-primary" onclick="getReport();" target="_blank" style="margin-left:12px;" href="javascript:void(0);">Export</a></div>
                                <div class="float-right">
                                    <button class="btn btn-primary btn-cons"  id="btn_submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">
    var row_selected = [];
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };


// A $( document ).ready() block.
    $(document).ready(function () {
        $('#DataTables_Table_0_wrapper').css('display', 'none');
        $.fn.dataTableExt.sErrMode = 'throw'
    });
    function loadTableData()
    {
        var franchise_id = $('#franchise_id').val();
        if (franchise_id == '')
        {
            alert('Please select franchise');
            return false;
        }

        $('#DataTables_Table_0_wrapper').css('display', 'block');
        $(".recordTable").DataTable().destroy();

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var date_type = $('#date_type').val();

        var franchise_paid = 0;
        if ($('#franchise_paid').is(':checked'))
        {
            franchise_paid = 1;
        } else
        {
            franchise_paid = 0;
        }
        var franchise_not_paid = 0;
        if ($('#franchise_not_paid').is(':checked'))
        {
            franchise_not_paid = 1;
        } else
        {
            franchise_not_paid = 0;
        }

        $('.recordTable').dataTable({
            "sPaginationType": "bootstrap",
            "aaSorting": [[0, "desc"]],
            'bProcessing': true,
            'bServerSide': true,

            "aLengthMenu": [[20, 50, 100, 500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
            "iDisplayLength": 20,
            "cache": false,
            "sScrollX": '200%',
            "sScrollY": "500",
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper($('.recordTable'), breakpointDefinition);
                }
            },
            'ajax': {
                url: '<?php echo site_url('admin/payments/listener'); ?>',
                type: 'POST',
                data: {franchise_id: franchise_id, start_date: start_date, end_date: end_date, date_type: date_type, franchise_paid: franchise_paid, franchise_not_paid: franchise_not_paid},
            },
            'aoColumnDef':
                    [
                        {"aTargets": ["sortme"], "bSortable": true},
                        {"aTargets": ['nosort'], "bSortable": false}

                    ],
            fnRowCallback: function (nRow) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            },
            createdRow: function (row, data, index) {
                if ($.inArray(data[0], row_selected) !== -1) {
                    $(row).find(".invoice-box").prop("checked", true);
                } else {
                    $(row).find(".invoice-box").prop("checked", false);
                }
            }
        });
    }





    var tableElement = $('.recordTable');
    $("#btn_submit").click(function () {

        loadTableData();
    });

    $('#recordTable').css('display', 'block');


    tableElement.dataTable({
        "sPaginationType": "bootstrap",
        "aaSorting": [[0, "desc"]],
        'bProcessing': true,
        'bServerSide': true,
        "aLengthMenu": [[20, 50, 100, 500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength": 20,
        "cache": false,
        "sScrollX": '200%',
        "sScrollY": "500",
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth: false,
        fnPreDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax': {
            url: '<?php //echo site_url('admin/payments/listener');            ?>',
            type: 'POST'
        },
        'aoColumnDef':
                [
                    {"aTargets": ["sortme"], "bSortable": true},
                    {"aTargets": ['nosort'], "bSortable": false}

                ],
        fnRowCallback: function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback: function (oSettings) {
            //responsiveHelper.respond();
        },
        createdRow: function (row, data, index) {
            if ($.inArray(data[0], row_selected) !== -1) {
                $(row).find(".invoice-box").prop("checked", true);
            } else {
                $(row).find(".invoice-box").prop("checked", false);
            }
        }
    });


<?php
if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
    echo '';
} else {
    ?>
        $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/invoices/add') ?>">Add</a>');
<?php } ?>
    $(document).on('change', '.invoice-box', function () {
        var id = $(this).val();
        var index = $.inArray(id, row_selected);
        if (index === -1) {
            row_selected.push(id);
        } else {
            row_selected.splice(index, 1);
        }
        showButton();
    });
    function showButton() {
        $("#order_status_button_area").remove();
        if (row_selected.length > 0) {
            var order_status_button_area = $('<div>').attr("id", "order_status_button_area").appendTo($("#DataTables_Table_0_length"));
            $('<br>').attr("clear", "all").appendTo(order_status_button_area);
            $('<br>').appendTo(order_status_button_area);
            $('<label>').html('Actions : ').appendTo(order_status_button_area);
            $('<a>').addClass("btn btn-primary btn-mini order-status").attr("style", "margin-left:12px;").attr("href", "<?php echo site_url('admin/payments/updateownerpaymentstatus') ?>").attr("data-type", "Yes").html("Move To Owner Payment Paid").appendTo(order_status_button_area);
        }
    }
    $(document).on("click", ".order-status", function () {
        var current_context = $(this);
        var current_text = current_context.attr("data-type");
        current_context.html("<i class='fa fa-spinner fa-spin'></i> " + current_context.html());
        $(".page-content").find("a").addClass("disabled");
        $.ajax({
            type: "POST",
            url: current_context.attr("href"),
            cache: false,
            async: false,
            data: 'type=' + current_text + "&ids=" + JSON.stringify(row_selected),
            success: function (data) {
                console.log(data);
                if (data) {
                    loadTableData();
                }
            },
            error: function (data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
        return false;
    });
    function getReport()
    {
        var franchise_id = $('#franchise_id').val();
        if (franchise_id == '')
        {
            alert('Please select franchise');
            return false;
        }

        $('#DataTables_Table_0_wrapper').css('display', 'block');
        $(".recordTable").DataTable().destroy();

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var date_type = $('#date_type').val();
        var franchise_paid = 0;
        if ($('#franchise_paid').is(':checked'))
        {
            franchise_paid = 1;
        } else
        {
            franchise_paid = 0;
        }
        var franchise_not_paid = 0;
        if ($('#franchise_not_paid').is(':checked'))
        {
            franchise_not_paid = 1;
        } else
        {
            franchise_not_paid = 0;
        }
        window.location = '<?php echo site_url('admin/payments/exportfranchiseinvoicesreport'); ?>?franchise_id=' + franchise_id + '&start_date=' + start_date + '&end_date=' + end_date + '&franchise_paid=' + franchise_paid + '&franchise_not_paid=' + franchise_not_paid + '&date_type=' + date_type;
        //window.open = ('<?php echo site_url('admin/payments/exportfranchiseinvoicesreport.html?test=1'); ?>', '_blank');

    }
</script>