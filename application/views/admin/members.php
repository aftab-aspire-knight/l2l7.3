<div class="content">
    <?php 
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/members'), isset($parent_page_name)?$parent_page_name:'Members');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Members');  
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Members</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="8%">Register From</th>
                                    <th width="8%">Referral Code</th>
                                    <th width="10%">First Name</th>
                                    <th width="10%">Last Name</th>
                                    <th width="10%">Email Address</th>
                                    <th width="5%">Phone</th>
                                    <th width="5%">Loyalty</th>
                                    <th width="5%">Used</th>
                                    <th width="10%">Status</th>
                                    <th width="14%">C-DateTime</th>
                                    <th width="15%">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <?php }else{
                            $disabled_attribute = '';
                            if($type == "View"){
                                $disabled_attribute = ' disabled="disabled"';
                            }
                            ?>
                        <form class="form-no-horizontal-spacing" id="frm_member" name="frm_member" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                            <input type="hidden" name="member_id" id="member_id"<?php echo isset($record['PKMemberID'])?' value="' . $record['PKMemberID'] . '"':''?> />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Register From <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <select id="register_from" name="register_from" class="form-control"<?php echo $disabled_attribute;?>>
                                            <option value="Desktop"<?php echo isset($record['RegisterFrom'])?($record['RegisterFrom'] == "Desktop")?' selected="selected"':'':''?>>Desktop</option>
                                            <option value="Mobile"<?php echo isset($record['RegisterFrom'])?($record['RegisterFrom'] == "Mobile")?' selected="selected"':'':''?>>Mobile</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Referral Code</label>
                                    <div class="controls">
                                        <input type="text" id="referral_code" maxlength="50" name="referral_code" class="form-control"<?php echo $disabled_attribute;echo isset($record['ReferralCode'])?' value="' . $record['ReferralCode'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">First Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="first_name" name="first_name" class="form-control"<?php echo $disabled_attribute;echo isset($record['FirstName'])?' value="' . $record['FirstName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Last Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="last_name" name="last_name" class="form-control"<?php echo $disabled_attribute;echo isset($record['LastName'])?' value="' . $record['LastName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Email Address <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="email_address" name="email_address" class="form-control"<?php echo $disabled_attribute;echo isset($record['EmailAddress'])?' value="' . $record['EmailAddress'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Password 
                                        <?php
                                            if($type == "Add"){
                                        ?>
                                        <span class="red-color">*</span>
                                        <?php }?>
                                    </label>
                                    <div class="controls">
                                        <?php
                                        if($type == "View" && isset($record['Password'])){
                                            ?>
                                            <input name="u_password" id="u_password" type="text" class="form-control" placeholder="Password" disabled="disabled" value="<?php echo $record['Password']?>" />
                                        <?php }else{?>
                                            <input name="u_password" id="u_password" type="password" class="form-control" />
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Phone No <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="phone" name="phone" class="form-control only-number"<?php echo $disabled_attribute;echo isset($record['Phone'])?' value="' . $record['Phone'] . '"':''?> />
                                    </div>
                                </div>
                            </div>

                            <br clear="all" />
                            <h3>Address Information</h3>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Post Code <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="postal_code" name="postal_code" class="form-control"<?php echo $disabled_attribute;echo isset($record['PostalCode'])?' value="' . $record['PostalCode'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Address  <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="building_name" name="building_name" class="form-control"<?php echo $disabled_attribute;echo isset($record['BuildingName'])?' value="' . $record['BuildingName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Address Line 2 <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="street_name" name="street_name" class="form-control"<?php echo $disabled_attribute;echo isset($record['StreetName'])?' value="' . $record['StreetName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Town <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="town" name="town" class="form-control"<?php echo $disabled_attribute;echo isset($record['Town'])?' value="' . $record['Town'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <?php
                                if(isset($preference_records) && sizeof($preference_records) > 0){
                                    echo '<h3>Wash Preferences</h3>';
                                    $c_p_count = 0;
                                    foreach($preference_records as $rec){
                                        echo '<div class="col-md-6 col-xs-12 col-lg-6">';
                                        echo '<div class="form-group">';
                                        echo '<label class="form-label">';
                                        echo $rec['Title'];
                                        echo ' <span class="red-color">*</span></label>';
                                        echo '<div class="controls">';
                                        echo '<select name="preference_list[]" class="form-control select2"' . $disabled_attribute . '>';
                                        if(isset($rec['children']) && sizeof($rec['children']) > 0){
                                            foreach($rec['children'] as $rec_child){
                                                $preference_selected = "";
                                                if(isset($record['member_preferences_array'])){
                                                    if(in_array($rec_child['ID'],$record['member_preferences_array'])){
                                                        $preference_selected = ' selected="selected"';
                                                    }
                                                }
                                                echo '<option value="' . $rec_child['ID'] . '"' . $preference_selected . '>' . $rec_child['Title'] . '</option>';
                                            }
                                        }
                                        echo '</select>';
                                        echo '</div>';
                                        echo '</div>';
                                        echo '</div>';
                                        $c_p_count += 1;
                                        if($c_p_count == 2){
                                            echo '<br clear="all" />';
                                            $c_p_count = 0;
                                        }
                                    }
                                    echo '<br clear="all" />';
                                }
                            ?>
                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Account Notes</label>
                                    <div class="controls">
                                        <textarea id="account_notes" name="account_notes" rows="5" class="form-control"<?php echo $disabled_attribute;?>><?php echo isset($record['AccountNotes'])?$record['AccountNotes']:''?></textarea>
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
							<div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Total Loyalty Points <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input type="text" id="TotalLoyaltyPoints" name="TotalLoyaltyPoints" class="form-control"<?php echo $disabled_attribute;echo isset($record['TotalLoyaltyPoints'])?' value="' . $record['TotalLoyaltyPoints'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <label class="form-label">Status</label>
                                <?php
                                if($type != "View"){
                                ?>
                                <div class="slide-primary">
                                    <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                </div>
                                <?php }else{?>
                                    <p><strong><?php echo $record['Status']?></strong></p>
                                <?php }?>
                            </div>
                            <br clear="all" />
                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <div class="float-right">
                                    <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                    ?>
                                    <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                    <?php }?>
                                    <a href="<?php echo site_url('admin/members')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                </div>
                            </div>
                        </form>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "sScrollX": '130%',
        "sScrollY": "500",
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/members/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,null,
                null,null,null,{ "bSortable": false,"bSearchable":false,'bVisible'   : false },null,null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/members/add')?>">Add</a>');
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/members/export')?>">Export</a>');
    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
    ?>
    });
</script>
