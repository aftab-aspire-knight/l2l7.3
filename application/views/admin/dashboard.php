<div class="content">
    <div class="page-title">
        <h3>Dashboard </h3>
    </div>
    <div id="container">
        <?php if($admin_detail['Role'] != "Driver"){?>
        <div class="row">
			<?php if(!isset($FKFranchiseID) || $FKFranchiseID==0) {?>
            <div class="col-md-3 col-lg-3 col-sm-6 m-b-10">
                <a href="<?php echo site_url('admin/members')?>">
                    <div class="tiles blue added-margin">
                        <div class="tiles-body">
                            <div class="tiles-title">
                                Members
                            </div>
                            <div class="heading">
                                <i class="icon-user"></i>
                                <span class="animate-number" data-value="<?php echo isset($member_count)?$member_count:'0'?>" data-animation-duration="1000">0</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
			<?php } ?>
            <div class="col-md-3 col-lg-3 col-sm-6 m-b-10">
                <a href="<?php echo site_url('admin/invoices')?>">
                    <div class="tiles red added-margin">
                        <div class="tiles-body">
                            <div class="tiles-title">
                                Invoices
                            </div>
                            <div class="heading">
                                <i class="icon-print"></i>
                                <span class="animate-number" data-value="<?php echo isset($invoice_total_count)?$invoice_total_count:'0'?>" data-animation-duration="1000">0</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 m-b-10">
                <a href="<?php echo site_url('admin/invoices')?>?t=order&q=pending">
                    <div class="tiles navy added-margin">
                        <div class="tiles-body">
                            <div class="tiles-title">
                                Pending Invoices
                            </div>
                            <div class="heading">
                                <i class="icon-print"></i>
                                <span class="animate-number" data-value="<?php echo isset($invoice_pending_count)?$invoice_pending_count:'0'?>" data-animation-duration="1000">0</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 m-b-10">
                <a href="<?php echo site_url('admin/invoices')?>?t=order&q=completed">
                    <div class="tiles purple added-margin">
                        <div class="tiles-body">
                            <div class="tiles-title">
                                Completed Invoices
                            </div>
                            <div class="heading">
                                <i class="icon-print"></i>
                                <span class="animate-number" data-value="<?php echo isset($invoice_completed_count)?$invoice_completed_count:'0'?>" data-animation-duration="1000">0</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
		<?php if(!isset($FKFranchiseID) || $FKFranchiseID==0) { ?>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="grid simple ">
                    <div class="grid-body ">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-lg-6 m-b-10">
                                <h4><span class="semi-bold">Customer never placed the order</span></h4>
                                <table class="table table-striped dataTable" id="customer_never_placed_order_table">
                                    <thead>
                                    <tr>
                                        <th width="75%">Email Address</th>
                                        <th width="25%">Phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6 m-b-10">
                                <h4><span class="semi-bold">Customers not placed the order over a month</span></h4>
                                <table class="table table-striped dataTable" id="customer_never_placed_order_month_table">
                                    <thead>
                                    <tr>
                                        <th width="75%">Email Address</th>
                                        <th width="25%">Phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="grid simple ">
                    <h4><span class="semi-bold">Statistics</span></h4>
                    <div class="grid-body ">
                        <div class="col-md-3 col-xs-12 col-lg-3 m-b-10">
                            <div class="form-group">
                                <div class="controls">
                                    <label>Period</label>
                                    <select id="period" name="period" class="form-control">
                                        <option value="0">Last 7 Days</option>
                                        <option value="1">Last 30 Days</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-lg-6 m-b-10">
                            <div class="form-group">
                                <div class="controls">
                                    <label>Export Pickup Delivery Time Report</label>
                                    <a href="<?php echo site_url('admin/dashboard/exportpickdeliverytimereport')?>" class="btn btn-primary">Export</a>
                                </div>
                            </div>

                        </div>
                        <br clear="all" />
                        <div id="listing">
                        </div>
                        <?php
                            /*if(isset($invoice_count_array)){
                                $invoice_counter = 0;
                                for($i=1;$i<23;$i++){
                                    $search_time = sprintf("%02d",$i) . ':00-' . sprintf("%02d",($i + 1)) . ':00';
                                    echo ' <div class="col-md-3 col-lg-3 col-sm-6 m-b-10">';
                                    echo ' <a href="#">';
                                    echo ' <div class="tiles blue added-margin">';
                                    echo ' <div class="tiles-body">';
                                    echo '<div class="tiles-title">';
                                    echo '<strong>Pick Up : </strong>' . $invoice_count_array[$invoice_counter]['PickUp'] . '<br/><strong>Delivery : </strong>' . $invoice_count_array[$invoice_counter]['Delivery'];
                                    echo ' </div>';
                                    echo '<strong>Time : ' . $search_time . '</strong>';
                                    echo '</div>';
                                    echo '</div>';
                                    echo '</a>';
                                    echo '</div>';
                                    $invoice_counter += 1;
                                }
                            }*/
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<?php if($admin_detail['Role'] != "Driver"){?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('#customer_never_placed_order_table');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 10,
        "bLengthChange": false,
        //"bFilter": false,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/dashboard/customerneverplaceedorderlistener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
               null,null
            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
    $("<a>").addClass("btn btn-primary").attr("target","_blank").attr("style","margin-left:12px;").attr("href","<?php echo site_url('admin/dashboard/customerneverplaceedorderexport')?>").html("Export").appendTo($("#customer_never_placed_order_table_filter").parent().prev());

    var tableElement1 = $('#customer_never_placed_order_month_table');
    tableElement1.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 10,
        "bLengthChange": false,
        //"bFilter": false,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement1, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/dashboard/customerneverplaceedordermonthlistener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                null,null
            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }
    });
    $("<a>").addClass("btn btn-primary").attr("target","_blank").attr("style","margin-left:12px;").attr("href","<?php echo site_url('admin/dashboard/customerneverplaceedordermonthexport')?>").html("Export").appendTo($("#customer_never_placed_order_month_table_filter").parent().prev());

    $(document).on("change","#period",function(){
        var current_context = $(this);
        current_context.attr("disabled","disabled");
        var listing = $("#listing");
        listing.html("");
        var loader_area = $("<div>").addClass("col-md-offset-6").appendTo(listing);
        $("<i>").addClass("fa fa-spinner fa-spin fa-4x").appendTo(loader_area);
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/dashboard/loadstats')?>",
            data: "period=" + current_context.val(),
            cache: false,
            async: false,
            success: function(response)
            {
                listing.html("");
                var data = response.split("||");
                var area_1 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_1 = $("<a>").attr("href","#").appendTo(area_1);
                var title_main_1 = $("<div>").addClass("tiles blue added-margin").appendTo(anchor_1);
                var title_body_1 = $("<div>").addClass("tiles-body").appendTo(title_main_1);
                $("<div>").addClass("tiles-title").html("No of New Customers").appendTo(title_body_1);
                var heading_1 = $("<div>").addClass("heading").appendTo(title_body_1);
                $("<i>").addClass("icon-print").appendTo(heading_1);
                $("<span>").addClass("animate-number").attr("data-value",data[0]).attr("data-animation-duration","1000").html("0").appendTo(heading_1);
                var area_2 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_2 = $("<a>").attr("href","#").appendTo(area_2);
                var title_main_2 = $("<div>").addClass("tiles red added-margin").appendTo(anchor_2);
                var title_body_2 = $("<div>").addClass("tiles-body").appendTo(title_main_2);
                $("<div>").addClass("tiles-title").html("No of Orders").appendTo(title_body_2);
                var heading_2 = $("<div>").addClass("heading").appendTo(title_body_2);
                $("<i>").addClass("icon-print").appendTo(heading_2);
                $("<span>").addClass("animate-number").attr("data-value",data[1]).attr("data-animation-duration","1000").html("0").appendTo(heading_2);
                var area_3 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_3 = $("<a>").attr("href","#").appendTo(area_3);
                var title_main_3 = $("<div>").addClass("tiles navy added-margin").appendTo(anchor_3);
                var title_body_3 = $("<div>").addClass("tiles-body").appendTo(title_main_3);
                $("<div>").addClass("tiles-title").html("Amount of Orders").appendTo(title_body_3);
                var heading_3 = $("<div>").addClass("heading").appendTo(title_body_3);
                $("<i>").addClass("icon-print").appendTo(heading_3);
                $("<span>").addClass("animate-number").attr("data-value",data[2]).attr("data-animation-duration","1000").html("0").appendTo(heading_3);
                var area_4 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_4 = $("<a>").attr("href","#").appendTo(area_4);
                var title_main_4 = $("<div>").addClass("tiles purple added-margin").appendTo(anchor_4);
                var title_body_4 = $("<div>").addClass("tiles-body").appendTo(title_main_4);
                $("<div>").addClass("tiles-title").html("No of Orders New Customers").appendTo(title_body_4);
                var heading_4 = $("<div>").addClass("heading").appendTo(title_body_4);
                $("<i>").addClass("icon-print").appendTo(heading_4);
                $("<span>").addClass("animate-number").attr("data-value",data[3]).attr("data-animation-duration","1000").html("0").appendTo(heading_4);
                var area_5 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_5 = $("<a>").attr("href","#").appendTo(area_5);
                var title_main_5 = $("<div>").addClass("tiles green added-margin").appendTo(anchor_5);
                var title_body_5 = $("<div>").addClass("tiles-body").appendTo(title_main_5);
                $("<div>").addClass("tiles-title").html("Amount of Orders New Customer").appendTo(title_body_5);
                var heading_5 = $("<div>").addClass("heading").appendTo(title_body_5);
                $("<i>").addClass("icon-print").appendTo(heading_5);
                $("<span>").addClass("animate-number").attr("data-value",data[4]).attr("data-animation-duration","1000").html("0").appendTo(heading_5);
                var area_6 = $("<div>").addClass("col-md-4 col-lg-4 col-sm-6 m-b-10").appendTo(listing);
                var anchor_6 = $("<a>").attr("href","#").appendTo(area_6);
                var title_main_6 = $("<div>").addClass("tiles black added-margin").appendTo(anchor_6);
                var title_body_6 = $("<div>").addClass("tiles-body").appendTo(title_main_6);
                $("<div>").addClass("tiles-title").html("No of Orders Repeat Customer").appendTo(title_body_6);
                var heading_6 = $("<div>").addClass("heading").appendTo(title_body_6);
                $("<i>").addClass("icon-print").appendTo(heading_6);
                $("<span>").addClass("animate-number").attr("data-value",data[5]).attr("data-animation-duration","1000").html("0").appendTo(heading_6);


                $('.animate-number').each(function () {
                    $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-animation-duration")));
                });
                current_context.removeAttr("disabled");
            },
            error:function(data){
                MessageBoxError("#msg_box",data,2000,"");
            }
        });
    });
    $(document).ready(function(){
        $("#period").trigger("change");
    });
</script>
<?php }?>
