<div class="content">
    <?php
    if (isset($type)) {
        echo show_admin_bread_crumbs($type, site_url('admin/franchises'), isset($parent_page_name) ? $parent_page_name : 'Franchises');
    } else {
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name) ? $parent_page_name : 'Franchises');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Franchises</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if (!isset($type)) {
                            ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th width="10%">Title</th>
                                        <th width="8%" data-hide="phone">Name</th>
                                        <th width="7%" data-hide="phone">Email Address</th>
                                        <th width="7%" data-hide="phone">Pickup Limit</th>
                                        <th width="8%" data-hide="phone">Delivery Limit</th>
                                        <th width="8%" data-hide="phone">Opening Time</th>
                                        <th width="7%" data-hide="phone">Closing Time</th>
                                        <th width="8%" data-hide="phone">Same Time</th>
                                        <th width="9%" data-hide="phone">Delivery Difference Hour</th>
                                        <th width="8%" data-hide="phone">Status</th>
                                        <th width="15%">Options</th>
                                    </tr>
                                </thead>
                            </table>
                            <?php
                        } else {
                            $disabled_attribute = '';
                            if ($type == "View") {
                                $disabled_attribute = ' disabled="disabled"';
                            }
                            ?>

                            <ul class="nav nav-tabs" id="maincontent" role="tablist">
                                <li class="active"><a href="#home" role="tab" data-toggle="tab">Franchises</a></li>
                                <li><a href="#services" role="tab" data-toggle="tab">Services</a></li>
                            </ul>
                            <form class="form-no-horizontal-spacing" id="frm_franchise" name="frm_franchise" action="<?php echo isset($form_action) ? $form_action : '#' ?>" method="post">
                                <input type="hidden" name="franchise_id" id="franchise_id"<?php echo isset($record['PKFranchiseID']) ? ' value="' . $record['PKFranchiseID'] . '"' : '' ?> />
                                <input type="hidden" id="assigned_output" name="assigned_output" />
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="home">

                                        <div class="col-md-12 col-xs-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="form-label">Title <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="title" name="title" class="form-control"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['Title']) ? ' value="' . $record['Title'] . '"' : ''
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <br clear="all" />
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Contact Person Name <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['Name']) ? ' value="' . $record['Name'] . '"' : ''
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Contact Person Email Address <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="email_address" name="email_address" class="form-control"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['EmailAddress']) ? ' value="' . $record['EmailAddress'] . '"' : ''
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <br clear="all" />
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Mobile No</label>
                                                <div class="controls">
                                                    <input type="text" id="mobile_no" name="mobile_no" class="form-control only-number"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['Mobile']) ? ' value="' . $record['Mobile'] . '"' : ''
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Telephone No</label>
                                                <div class="controls">
                                                    <input type="text" id="telephone_no" name="telephone_no" class="form-control only-number"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['Telephone']) ? ' value="' . $record['Telephone'] . '"' : ''
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <br clear="all" />
                                        <div class="col-md-3 col-xs-12 col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label">Pickup Limit <span class="red-color">*</span></label><span class="help">at one time</span>
                                                <div class="controls">
                                                    <input type="text" id="pickup_limit" name="pickup_limit" maxlength="3" class="form-control only-number not-zero"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['PickupLimit']) ? ' value="' . $record['PickupLimit'] . '"' : ' value="1"'
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label">Delivery Limit <span class="red-color">*</span></label><span class="help">at one time</span>
                                                <div class="controls">
                                                    <input type="text" id="delivery_limit" name="delivery_limit" maxlength="3" class="form-control only-number not-zero"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['DeliveryLimit']) ? ' value="' . $record['DeliveryLimit'] . '"' : ' value="1"'
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-lg-2">
                                            <div class="form-group">
                                                <label class="form-label">Opening Time <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <select id="opening_time" name="opening_time" class="form-control"<?php echo $disabled_attribute; ?>>
                                                        <?php
                                                        for ($i = 5; $i <= 12; $i++) {
                                                            $opening_time_selected = "";
                                                            if (strlen($i) == 1) {
                                                                $i = '0' . $i;
                                                            }
                                                            $opening_value = $i . ':00';
                                                            if ($i == 12) {
                                                                $opening_value .= ' pm';
                                                            } else {
                                                                $opening_value .= ' am';
                                                            }
                                                            if (isset($record['OpeningTime'])) {
                                                                if ($record['OpeningTime'] == $opening_value) {
                                                                    $opening_time_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
                                                        }
                                                        for ($i = 1; $i <= 11; $i++) {
                                                            $opening_time_selected = "";
                                                            if (strlen($i) == 1) {
                                                                $i = '0' . $i;
                                                            }
                                                            $opening_value = $i . ':00';
                                                            if ($i == 12) {
                                                                $opening_value .= ' am';
                                                            } else {
                                                                $opening_value .= ' pm';
                                                            }
                                                            if (isset($record['OpeningTime'])) {
                                                                if ($record['OpeningTime'] == $opening_value) {
                                                                    $opening_time_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-lg-2">
                                            <div class="form-group">
                                                <label class="form-label">Closing Time <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <select id="closing_time" name="closing_time" class="form-control"<?php echo $disabled_attribute; ?>>
                                                        <?php
                                                        for ($i = 5; $i <= 12; $i++) {
                                                            $closing_time_selected = "";
                                                            if (strlen($i) == 1) {
                                                                $i = '0' . $i;
                                                            }
                                                            $closing_value = $i . ':00';
                                                            if ($i == 12) {
                                                                $closing_value .= ' pm';
                                                            } else {
                                                                $closing_value .= ' am';
                                                            }
                                                            if (isset($record['ClosingTime'])) {
                                                                if ($record['ClosingTime'] == $closing_value) {
                                                                    $closing_time_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
                                                        }
                                                        for ($i = 1; $i <= 11; $i++) {
                                                            $closing_time_selected = "";
                                                            if (strlen($i) == 1) {
                                                                $i = '0' . $i;
                                                            }
                                                            $closing_value = $i . ':00';
                                                            if ($i == 12) {
                                                                $closing_value .= ' am';
                                                            } else {
                                                                $closing_value .= ' pm';
                                                            }
                                                            if (isset($record['ClosingTime'])) {
                                                                if ($record['ClosingTime'] == $closing_value) {
                                                                    $closing_time_selected = ' selected="selected"';
                                                                }
                                                            }
                                                            echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-xs-12 col-lg-2">
                                            <div class="form-group">
                                                <label class="form-label">Gap Time <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <?php // echo $record['GapTime']?>
                                                    <input type="text" id="GapTime" name="GapTime" class="form-control only-number not-zero"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['GapTime']) ? ' value="' . $record['GapTime'] . '"' : ' value="1"'
                                                    ?> />

                                                </div>
                                            </div>
                                        </div>
                                        <br clear="all" />


                                        <div id="franchise_timmings_div" style="border:1px solid;" >
                                            <button class="add_field_button btn btn-info">Add More Fields</button>
                                            <div class="input_fields_wrap">
                                                <?php
                                                if (isset($record['timings']) && sizeof($record['timings']) > 0) {
                                                    foreach ($record['timings'] as $key => $val) {
                                                        ?>
                                                        <div>
                                                            <br clear="all" />
                                                            <br clear="all" />
                                                            <div class="col-md-3 col-xs-12 col-lg-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Pickup Limit <span class="red-color">*</span></label><span class="help">at one time</span>
                                                                    <div class="controls">
                                                                        <input type="text" id="more_pickup_limit" name="more_pickup_limit[]" maxlength="3" class="form-control only-number not-zero"<?php
                                                                        echo $disabled_attribute;
                                                                        echo isset($val['PickupLimit']) ? ' value="' . $val['PickupLimit'] . '"' : ' value="1"'
                                                                        ?> />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-xs-12 col-lg-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Delivery Limit <span class="red-color">*</span></label><span class="help">at one time</span>
                                                                    <div class="controls">
                                                                        <input type="text" id="more_delivery_limit" name="more_delivery_limit[]" maxlength="3" class="form-control only-number not-zero"<?php
                                                                        echo $disabled_attribute;
                                                                        echo isset($val['DeliveryLimit']) ? ' value="' . $val['DeliveryLimit'] . '"' : ' value="1"'
                                                                        ?> />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-12 col-lg-2">
                                                                <div class="form-group">
                                                                    <label class="form-label">Opening Time <span class="red-color">*</span></label>
                                                                    <div class="controls">
                                                                        <select id="more_opening_time" name="more_opening_time[]" class="form-control"<?php echo $disabled_attribute; ?>>
                                                                            <?php
                                                                            for ($i = 5; $i <= 12; $i++) {
                                                                                $opening_time_selected = "";
                                                                                if (strlen($i) == 1) {
                                                                                    $i = '0' . $i;
                                                                                }
                                                                                $opening_value = $i . ':00';
                                                                                if ($i == 12) {
                                                                                    $opening_value .= ' pm';
                                                                                } else {
                                                                                    $opening_value .= ' am';
                                                                                }
                                                                                if (isset($val['OpeningTime'])) {
                                                                                    if ($val['OpeningTime'] == $opening_value) {
                                                                                        $opening_time_selected = ' selected="selected"';
                                                                                    }
                                                                                }
                                                                                echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
                                                                            }
                                                                            for ($i = 1; $i <= 11; $i++) {
                                                                                $opening_time_selected = "";
                                                                                if (strlen($i) == 1) {
                                                                                    $i = '0' . $i;
                                                                                }
                                                                                $opening_value = $i . ':00';
                                                                                if ($i == 12) {
                                                                                    $opening_value .= ' am';
                                                                                } else {
                                                                                    $opening_value .= ' pm';
                                                                                }
                                                                                if (isset($val['OpeningTime'])) {
                                                                                    if ($val['OpeningTime'] == $opening_value) {
                                                                                        $opening_time_selected = ' selected="selected"';
                                                                                    }
                                                                                }
                                                                                echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-12 col-lg-2">
                                                                <div class="form-group">
                                                                    <label class="form-label">Closing Time <span class="red-color">*</span></label>
                                                                    <div class="controls">
                                                                        <select id="more_closing_time" name="more_closing_time[]" class="form-control"<?php echo $disabled_attribute; ?>>
                                                                            <?php
                                                                            for ($i = 5; $i <= 12; $i++) {
                                                                                $closing_time_selected = "";
                                                                                if (strlen($i) == 1) {
                                                                                    $i = '0' . $i;
                                                                                }
                                                                                $closing_value = $i . ':00';
                                                                                if ($i == 12) {
                                                                                    $closing_value .= ' pm';
                                                                                } else {
                                                                                    $closing_value .= ' am';
                                                                                }
                                                                                if (isset($val['ClosingTime'])) {
                                                                                    if ($val['ClosingTime'] == $closing_value) {
                                                                                        $closing_time_selected = ' selected="selected"';
                                                                                    }
                                                                                }
                                                                                echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
                                                                            }
                                                                            for ($i = 1; $i <= 11; $i++) {
                                                                                $closing_time_selected = "";
                                                                                if (strlen($i) == 1) {
                                                                                    $i = '0' . $i;
                                                                                }
                                                                                $closing_value = $i . ':00';
                                                                                if ($i == 12) {
                                                                                    $closing_value .= ' am';
                                                                                } else {
                                                                                    $closing_value .= ' pm';
                                                                                }
                                                                                if (isset($val['ClosingTime'])) {
                                                                                    if ($val['ClosingTime'] == $closing_value) {
                                                                                        $closing_time_selected = ' selected="selected"';
                                                                                    }
                                                                                }
                                                                                echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-12 col-lg-2">
                                                                <div class="form-group">
                                                                    <label class="form-label">Gap Time <span class="red-color">*</span></label>
                                                                    <div class="controls">
                                                                        <?php // echo $record['GapTime']?>
                                                                        <input type="text" id="GapTimes[]" name="GapTimes[]" class="form-control only-number not-zero"<?php
                                                                        echo $disabled_attribute;
                                                                        echo isset($val['GapTime']) ? ' value="' . $val['GapTime'] . '"' : ' value="1"'
                                                                        ?> />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br clear="all" />
                                                            <a href="#" class="remove_field">Remove</a>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>


                                            </div>
                                        </div>


                                        <br clear="all" />
                                        <br clear="all" />



                                        <div class="col-md-4 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Address</label>
                                                <div class="controls">
                                                    <textarea id="address" name="address" rows="5" class="form-control"<?php echo $disabled_attribute; ?>><?php echo isset($record['Address']) ? $record['Address'] : '' ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Delivery Option</label>
                                                <div class="controls">
                                                    <select id="delivery_option" name="delivery_option" class="form-control"<?php echo $disabled_attribute; ?>>
                                                        <option value="None"<?php echo isset($record['DeliveryOption']) ? ($record['DeliveryOption'] == "None") ? ' selected="selected"' : '' : '' ?>>None</option>
                                                        <option value="Saturday Pickup Delivery To Monday After 3"<?php echo isset($record['DeliveryOption']) ? ($record['DeliveryOption'] == "Saturday Pickup Delivery To Monday After 3") ? ' selected="selected"' : '' : '' ?>>Saturday Pickup Delivery To Monday After 3</option>
                                                        <option value="Saturday Sunday Both Pickup Delivery To Monday After 3"<?php echo isset($record['DeliveryOption']) ? ($record['DeliveryOption'] == "Saturday Sunday Both Pickup Delivery To Monday After 3") ? ' selected="selected"' : '' : '' ?>>Saturday Sunday Both Pickup Delivery To Monday After 3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Pickup Difference Hour <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="pickup_difference_hour" name="pickup_difference_hour" maxlength="2" class="form-control only-number"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['PickupDifferenceHour']) ? ' value="' . $record['PickupDifferenceHour'] . '"' : ' value="3"'
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Delivery Difference Hour <span class="red-color">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="delivery_difference_hour" name="delivery_difference_hour" maxlength="2" class="form-control only-number"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['DeliveryDifferenceHour']) ? ' value="' . $record['DeliveryDifferenceHour'] . '"' : ' value="0"'
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-xs-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Minimum Order Amount <span class="red-color"></span></label>
                                                <div class="controls">
                                                    <input type="text" id="minimum_order_amount" name="minimum_order_amount" maxlength="10" class="form-control only-number"<?php
                                                    echo $disabled_attribute;
                                                    echo isset($record['MinimumOrderAmount']) ? ' value="' . $record['MinimumOrderAmount'] . '"' : ' value="0"'
                                                    ?> />
                                                </div>
                                            </div>
                                        </div>

                                        <br clear="all" />
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Allow Same Time</label>
                                                <div class="controls">
                                                    <select id="allow_same_time" name="allow_same_time" class="form-control"<?php echo $disabled_attribute; ?>>
                                                        <option value="Yes"<?php echo isset($record['AllowSameTime']) ? ($record['AllowSameTime'] == "Yes") ? ' selected="selected"' : '' : '' ?>>Yes</option>
                                                        <option value="No"<?php echo isset($record['AllowSameTime']) ? ($record['AllowSameTime'] == "No") ? ' selected="selected"' : '' : '' ?>>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <br clear="all" />
                                        <?php /* Hassan OffDay  13-10-2018 */ ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-label">Off Days</label>
                                                <div class="controls">
                                                    <select id="off_days" name="off_days[]" multiple class="form-control select2"<?php echo $disabled_attribute; ?>>
                                                        <?php
                                                        $day_array = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                                                        $off_day_array = array();
                                                        if (isset($record['OffDays'])) {
                                                            $off_day_array = explode(",", $record['OffDays']);
                                                        }
                                                        foreach ($day_array as $rec) {
                                                            $selected = '';
                                                            if (in_array($rec, $off_day_array)) {
                                                                $selected = ' selected="selected"';
                                                            }
                                                            echo '<option value="' . $rec . '"' . $selected . '>' . $rec . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <label class="form-label">Status</label>
                                            <?php
                                            if ($type != "View") {
                                                ?>
                                                <div class="slide-primary">
                                                    <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status']) ? ' checked="checked"' : '' ?> />
                                                </div>
                                            <?php } else { ?>
                                                <p><strong><?php echo $record['Status'] ?></strong></p>
                                            <?php } ?>
                                        </div>
                                        <br clear="all" /><br />
                                        <?php
                                        if ($type != "View") {
                                            ?>
                                            <div class="col-md-6 col-xs-12 col-lg-6">
                                                <div class="form-group">
                                                    <div class="control-group">
                                                        <label class="form-label">Post Code</label>
                                                        <div class="controls">
                                                            <input type="text" maxlength="100" id="post_code" name="post_code" class="form-control link">
                                                        </div>
                                                    </div><br clear="all" />
                                                    <button type="button" class="btn btn-white btn-cons" id="btn_franchise_post_code">Add</button>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div class="dd" id="nestable_franchise_post_code">
                                                <ol class="dd-list" id="l_list">
                                                    <?php
                                                    if (isset($record['Navigation']) && sizeof($record['Navigation']) > 0) {
                                                        $handle_class = 'dd-handle';
                                                        if ($type == "View") {
                                                            $handle_class = "dd-nodrag";
                                                        }
                                                        foreach ($record['Navigation'] as $rec) {
                                                            $random = mt_rand();
                                                            echo '<li class="dd-item dd3-item" data-code="' . $rec['Code'] . '"><div class="' . $handle_class . ' dd3-handle"></div>';
                                                            echo '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
                                                            echo '<div class="accordion-heading"> <a href="#collapse' . $random . '" data-toggle="collapse" class="accordion-toggle collapsed">Code : ' . $rec['Code'];
                                                            if ($type != "View") {
                                                                echo '<i class="fa fa-plus"></i>';
                                                            }
                                                            echo '</a> </div>';
                                                            echo '<div class="accordion-body collapse" id="collapse' . $random . '" style="height: 0px;"><div class="accordion-inner">';
                                                            if ($type != "View") {
                                                                echo '<div class="control-button"><a class="remove-nav">Remove</a></div>';
                                                            }
                                                            echo '</div></div></div></div></div></li>';
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                            </div>
                                        </div>

                                    </div><!--/.tab-pane -->
                                    <div class="tab-pane fade" id="services">

                                        <div class="col-md-6 col-xs-12 col-lg-6">
                                            <div >
                                                <input type="hidden" id="add_services_action" value="<?php echo isset($add_services_action) ? $add_services_action : ''; ?>" />
                                                <input type="hidden" id="update_services_price" value="<?php echo isset($update_services_price) ? $update_services_price : ''; ?>" />
                                                <input type="hidden" id="remove_services" value="<?php echo isset($remove_services) ? $remove_services : ''; ?>" />

                                                <?php
                                                //if (!isset($record['Services']) || sizeof($record['Services']) <= 0) {
                                                    ?>

                                                    <button type="button" class="btn btn-white btn-cons" id="btn_franchise_add_services">Add Services</button>
                                                <?php // } ?>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-xs-12 col-lg-12">
                                            <div class="table-responsive" id="nestable_franchise_services">
                                                <table id="table_services" class="table table-condensed display">
                                                    <thead>
                                                        <tr>
                                                            <th ></th>
                                                            <th scope="col">Service</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Discount(%)</th>
                                                            <th scope="col"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        if (isset($record['Services']) && sizeof($record['Services']) > 0) {
                                                            $handle_class = 'dd-handle';
                                                            if ($type == "View") {
                                                                $handle_class = "dd-nodrag";
                                                            }
                                                            $i = 0;
                                                            foreach ($record['Services'] as $rec) {


                                                                $random = mt_rand();
                                                                echo '<tr data-service="' . $rec['PKServiceID'] . '"><td scope="row">' . $rec['Title'] . '</td>';
                                                                echo '<td><input type="text" id="franchise_service_title' . $rec['PKServiceID'] . '" name="franchise_service_title"  class="form-control " value="' . $rec['Title'] . '" aria-invalid="false"></td>';
                                                                echo '<td><input type="text" id="franchise_service_price' . $rec['PKServiceID'] . '" name="franchise_service_price"  class="form-control " value="' . $rec['Price'] . '" aria-invalid="false"></td>';
                                                                echo '<td><input type="text" id="franchise_service_discount' . $rec['PKServiceID'] . '" name="franchise_service_price"  class="form-control " value="' . $rec['DiscountPercentage'] . '" aria-invalid="false"></td>';

                                                                echo '<td>';

                                                                echo '<a onclick="remove_franchise_services(' . $rec['PKFranchiseServiceID'] . ');" class="remove-nav btn btn-lg btn-danger">Remove</a>  ';
                                                                echo '<a onclick="update_franchise_price_services(' . $rec['PKFranchiseServiceID'] . ',$(\'#franchise_service_price' . $rec['PKServiceID'] . '\').val(),$(\'#franchise_service_title' . $rec['PKServiceID'] . '\').val(),$(\'#franchise_service_discount' . $rec['PKServiceID'] . '\').val());" style="cursor:pointer;" class="save-nav btn btn-lg  btn-info">Save</a>';

                                                                echo '</td></tr>';
                                                            }
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--/.tab-pane -->
                                </div>
                                <!--Franchise services ends-->

                                <br clear="all" /><br /><br />

                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if ($type != "View") {
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php } ?>
                                        <a href="<?php echo site_url('admin/franchises') ?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text ?></a>
                                    </div>
                                </div>
                            </form>




                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer'); ?>


<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
</script>
<?php if (!isset($type)) { ?>
    <script>
        var tableElement = $('.recordTable');

        tableElement.dataTable({
            "sPaginationType": "bootstrap",
            "aaSorting": [[0, "desc"]],
            'bProcessing': true,
            'bServerSide': true,
            "aLengthMenu": [[20, 50, 100, 500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
            "iDisplayLength": 20,
            "sScrollX": '110%',
            "sScrollY": "500",
            "cache": false,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
                }
            },
            'ajax': {
                url: '<?php echo site_url('admin/franchises/listener'); ?>',
                type: 'POST'
            },
            'aoColumns':
                    [
                        {
                            'bSearchable': true,
                            'bVisible': true
                        },
                        null, null, null, null, null, null, null, null, null, null, {"bSortable": false, "bSearchable": false}

                    ],
            fnRowCallback: function (nRow) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }

        });
        $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/franchises/add') ?>">Add</a>');
    </script>
<?php } ?>
<script>
    $(document).ready(function () {
<?php
if (isset($record['Status'])) {
    ?>
            ChangeSwitch("#status", "<?php echo $record['Status'] ?>");
    <?php
} else {
    ?>
            ChangeSwitch("#status", "Enabled");
    <?php
}if (isset($admin_message)) {
    ?>
            MessageBoxSuccess("#msg_box", "<?php echo $admin_message ?>", 1000, "");
            scrollToPosition($("#msg_box"));
    <?php
}
?>
    });




    $(document).ready(function () {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div>'
                        + '<br clear="all" />' +
                        '<div class="col-md-3 col-xs-12 col-lg-3">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Pickup Limit <span class="red-color">*</span></label><span class="help">at one time</span>' +
                        '<div class="controls">' +
                        '<input type="text" id="more_pickup_limit" name="more_pickup_limit[]" maxlength="3" class="form-control only-number not-zero"<?php
echo $disabled_attribute;
echo ' value=""'
?> />' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-3 col-xs-12 col-lg-3">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Delivery Limit <span class="red-color">*</span></label><span class="help">at one time</span>' +
                        '<div class="controls">' +
                        '<input type="text" id="more_delivery_limit" name="more_delivery_limit[]" maxlength="3" class="form-control only-number not-zero"<?php
echo $disabled_attribute;
echo ' value=""'
?> />' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-xs-12 col-lg-2">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Opening Time <span class="red-color">*</span></label>' +
                        '<div class="controls">' +
                        '<select id="more_opening_time" name="more_opening_time[]" class="form-control"<?php echo $disabled_attribute; ?>>' +
                        '<?php
for ($i = 5; $i <= 12; $i++) {
    $opening_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $opening_value = $i . ':00';
    if ($i == 12) {
        $opening_value .= ' pm';
    } else {
        $opening_value .= ' am';
    }
    /* if(isset($record['OpeningTime'])){
      if($record['OpeningTime'] == $opening_value){
      $opening_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
}
for ($i = 1; $i <= 11; $i++) {
    $opening_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $opening_value = $i . ':00';
    if ($i == 12) {
        $opening_value .= ' am';
    } else {
        $opening_value .= ' pm';
    }
    /* if(isset($record['OpeningTime'])){
      if($record['OpeningTime'] == $opening_value){
      $opening_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
}
?>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-xs-12 col-lg-2">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Closing Time <span class="red-color">*</span></label>' +
                        '<div class="controls">' +
                        '<select id="more_closing_time" name="more_closing_time[]" class="form-control"<?php echo $disabled_attribute; ?>>' +
                        '<?php
for ($i = 5; $i <= 12; $i++) {
    $closing_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $closing_value = $i . ':00';
    if ($i == 12) {
        $closing_value .= ' pm';
    } else {
        $closing_value .= ' am';
    }
    /* if(isset($record['ClosingTime'])){
      if($record['ClosingTime'] == $closing_value){
      $closing_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
}
for ($i = 1; $i <= 11; $i++) {
    $closing_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $closing_value = $i . ':00';
    if ($i == 12) {
        $closing_value .= ' am';
    } else {
        $closing_value .= ' pm';
    }
    /* if(isset($record['ClosingTime'])){
      if($record['ClosingTime'] == $closing_value){
      $closing_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
}
?>' +
                        '</select>' +
                        '</div>' +
                        '</div>'
                        + '</div>'
                        + '<div class="col-md-2 col-xs-12 col-lg-2"><label class="form-label">Gap Time <span class="red-color">*</span></label><div class="form-group"><input type="text" id="GapTimes[]" name="GapTimes[]" class="form-control only-number not-zero"/></div></div>'
                        + '<br clear="all" />'
                        + '<a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });


        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
</script>
<?php if (isset($type)) { ?>
    <script type="text/javascript">


        $(document).ready(function () {
            $('#table_services').dataTable({
                "paging": false,
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": true
                    }
                ]
            });
        });
    </script>
<?php } ?>