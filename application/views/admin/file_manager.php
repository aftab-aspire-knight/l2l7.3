<div class="content">
    <?php echo show_admin_bread_crumbs('File Manager', base_url('admin/dashboard.html'), isset($parent_page_name)?$parent_page_name:'File Manager')?>
    <div class="row-fluid" id="list">
        <div class="span12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">File Manager</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row-fluid">
                        <iframe width="100%" height="600" src="<?php echo base_url('assets/filemanager/dialog.php?type=0&editor=mce_0')?>" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>