<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/widgetsections'), isset($parent_page_name)?$parent_page_name:'Widget Sections');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Widget Sections');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Widget Sections</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="80%">Title</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_widget_section" name="frm_widget_section" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="section_id" id="section_id"<?php echo isset($record['PKSectionID'])?' value="' . $record['PKSectionID'] . '"':''?> />
                                <input type="hidden" id="assigned_output" name="assigned_output" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <?php
                                if($type != "View") {
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Widget</label>
                                        <div class="controls">
                                            <select id="widget" name="widget" class="form-control select2">
                                                <option value="">Select</option>
                                                <?php
                                                    if(isset($widget_records) && sizeof($widget_records) > 0){
                                                        foreach($widget_records as $rec){
                                                            echo '<option value="' . $rec['ID'] . '||' . $rec['Title'] . '">' . $rec['Title'] . '</option>';
                                                        }
                                                    }
                                                ?>
                                            </select><br clear="all" />
                                            <button type="button" class="btn btn-white btn-cons" id="btn_widget">Add</button>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="dd" id="nestable_widget">
                                        <ol class="dd-list" id="w_list">
                                            <?php
                                                if(isset($record['Navigation']) && sizeof($record['Navigation']) > 0){
                                                    $handle_class = 'dd-handle';
                                                    if($type == "View"){
                                                        $handle_class = "dd-nodrag";
                                                    }
                                                    foreach($record['Navigation'] as $rec){
                                                        $random = mt_rand();
                                                        echo '<li class="dd-item dd3-item" data-id="' . $rec['WidgetID'] . '"><div class="' . $handle_class . ' dd3-handle"></div>';
                                                        echo '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
                                                        echo '<div class="accordion-heading"> <a href="#collapse' . $random . '" data-toggle="collapse" class="accordion-toggle collapsed">' . $rec['Title'] . '<i class="fa fa-plus"></i> </a> </div>';
                                                        echo '<div class="accordion-body collapse" id="collapse' . $random . '" style="height: 0px;"><div class="accordion-inner">';
                                                        if($type != "View") {
                                                            echo '<div class="control-button"><a class="remove-nav">Remove</a></div>';
                                                        }
                                                        echo '</div></div></div></div></div></li>';
                                                    }
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/widgetsections')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/widgetsections/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/widgetsections/add')?>">Add</a>');

    $(document).ready(function(){
        <?php
        if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>