<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/banners'), isset($parent_page_name)?$parent_page_name:'Banners');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Banners');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Banners</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="30%">Title</th>
                                <th width="25%" data-hide="phone">Image</th>
                                <th width="10%" data-hide="phone">Position</th>
                                <th width="15%" data-hide="phone">Status</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_banner" name="frm_banner" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="banner_id" id="banner_id"<?php echo isset($record['PKBannerID'])?' value="' . $record['PKBannerID'] . '"':''?> />
                                <input type="hidden" name="pic_path" id="pic_path"<?php echo isset($record['ImageName'])?' value="' . $record['ImageName'] . '"':''?> />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Content</label>
                                        <div class="controls">
                                            <textarea id="content" name="content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Content'])?$record['Content']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Link</label>
                                        <div class="controls">
                                            <input type="text" id="link" name="link" class="form-control"<?php echo $disabled_attribute;echo isset($record['Link'])?' value="' . $record['Link'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <?php
                                if($type != "View") {
                                    ?>
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Image <span class="red-color">*</span></label>
                                            <div class="controls">
                                                <input type="file" name="image_file" id="image_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                if(isset($record['ImageName']) && !empty($record['ImageName'])){
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">Image Preview:</label>
                                        <div class="controls">
                                            <img src="<?php echo $this->config->item('path_upload_banner_thumb') . $record['ImageName']?>" alt="Image" width="150" height="120" />
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <br clear="all" /><br />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Position</label>
                                        <div class="controls">
                                            <?php
                                            if($type == "View"){
                                            ?>
                                            <input type="text" id="position" name="position" class="col-md-2 only-number"<?php echo $disabled_attribute;echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }else{?>
                                                <input type="text" id="position" name="position" class="col-md-2 spin only-number"<?php echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/banners')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/banners/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,
                null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/banners/add')?>">Add</a>');

    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>