<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/services'), isset($parent_page_name)?$parent_page_name:'Services');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Services');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Services</span></h4>
                </div>
                <div class="grid-body ">
                    <div class="row" id="grid_panel">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                            <table class="table table-striped dataTable recordTable">
                                <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="15%">Category</th>
                                    <th width="15%">Title</th>
                                    <th width="15%">Desktop Image</th>
                                    <th width="15%">Mobile Image</th>
                                    <th width="5%">Price</th>
                                    <th width="10%">Preference_Show</th>
                                    <th width="5%">Position</th>
                                    <th width="5%">Status</th>
                                    <th width="15%">Options</th>
                                </tr>
                                </thead>
                            </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_service" name="frm_service" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="service_id" id="service_id"<?php echo isset($record['PKServiceID'])?' value="' . $record['PKServiceID'] . '"':''?> />
                                <input type="hidden" name="desktop_pic_path" id="desktop_pic_path"<?php echo isset($record['DesktopImageName'])?' value="' . $record['DesktopImageName'] . '"':''?> />
                                <input type="hidden" name="mobile_pic_path" id="mobile_pic_path"<?php echo isset($record['MobileImageName'])?' value="' . $record['MobileImageName'] . '"':''?> />
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Category <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="category" name="category" class="form-control select2"<?php echo $disabled_attribute;?>>
                                                <option value="">Select</option>
                                                <?php
                                                if(isset($category_records) && sizeof($category_records) > 0){
                                                    foreach($category_records as $rec){
                                                        $category_selected = "";
                                                        if(isset($record['FKCategoryID'])){
                                                            if($record['FKCategoryID'] == $rec['ID']){
                                                                $category_selected = ' selected="selected"';
                                                            }
                                                        }
                                                        echo '<option value="' . $rec['ID'] . '"'.$category_selected.'>' . $rec['Title'] . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Price <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="price" name="price"  data-a-sep="," data-a-dec="." class="form-control auto"<?php echo $disabled_attribute;echo isset($record['Price'])?' value="' . $record['Price'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="title" name="title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Content</label>
                                        <div class="controls">
                                            <textarea id="content" name="content" rows="15" cols="80" style="width: 80%;height:400px;" class="tinymce"><?php echo isset($record['Content'])?$record['Content']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <?php
                                if($type != "View") {
                                    ?>
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Desktop Image</label>
                                            <div class="controls">
                                                <input type="file" name="desktop_file" id="desktop_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                if(isset($record['DesktopImageName']) && !empty($record['DesktopImageName'])){
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6" id="desktop_view">
                                    <div class="form-group">
                                        <label class="control-label">Desktop Image Preview:</label>
                                        <div class="controls">
                                            <img id="desktop_image_preview" src="<?php echo $this->config->item('path_upload_service_thumb') . $record['DesktopImageName']?>" alt="Image" width="150" height="120" />
                                            <?php
                                            if($type != "View"){
                                                ?>
                                                <a href="#" id="desktop_remove_image" class="btn btn-danger btn-cons" style="display:block; width:120px; margin-top:20px">Remove</a>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                <br clear="all" /><br />
                                <?php
                                if($type != "View") {
                                    ?>
                                    <div class="col-md-6 col-xs-12 col-lg-6">
                                        <div class="form-group">
                                            <label class="form-label">Mobile Image</label>
                                            <div class="controls">
                                                <input type="file" name="mobile_file" id="mobile_file" class="col-md-12 col-xs-12 col-lg-12"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                if(isset($record['MobileImageName']) && !empty($record['MobileImageName'])){
                                    ?>
                                    <div class="col-md-6 col-xs-12 col-lg-6" id="mobile_view">
                                        <div class="form-group">
                                            <label class="control-label">Mobile Image Preview:</label>
                                            <div class="controls">
                                                <img id="mobile_image_preview" src="<?php echo $this->config->item('path_upload_service_thumb') . $record['MobileImageName']?>" alt="Image" width="150" height="120" />
                                                <?php
                                                if($type != "View"){
                                                    ?>
                                                    <a href="#" id="mobile_remove_image" class="btn btn-danger btn-cons" style="display:block; width:120px; margin-top:20px">Remove</a>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                                <br clear="all" /><br />
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Is Package</label>
                                        <div class="controls">
                                            <select id="is_package" name="is_package" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="No"<?php echo isset($record['IsPackage'])?($record['IsPackage'] == "No")?' selected="selected"':'':''?>>No</option>
                                                <option value="Yes"<?php echo isset($record['IsPackage'])?($record['IsPackage'] == "Yes")?' selected="selected"':'':''?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-lg-3">
                                    <div class="form-group">
                                        <label class="form-label">Preferences Show <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <select id="preference_show" name="preference_show" class="form-control"<?php echo $disabled_attribute;?>>
                                                <option value="No"<?php echo isset($record['PreferencesShow'])?($record['PreferencesShow'] == "No")?' selected="selected"':'':''?>>No</option>
                                                <option value="Yes"<?php echo isset($record['PreferencesShow'])?($record['PreferencesShow'] == "Yes")?' selected="selected"':'':''?>>Yes</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label">Position</label>
                                        <div class="controls">
                                            <?php
                                            if($type == "View"){
                                            ?>
                                            <input type="text" id="position" name="position" class="col-md-2 only-number"<?php echo $disabled_attribute;echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }else{?>
                                                <input type="text" id="position" name="position" class="col-md-2 spin only-number"<?php echo isset($record['Position'])?' value="' . $record['Position'] . '"':' value="0"'?> />
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12 col-lg-2">
                                    <label class="form-label">Status</label>
                                    <?php
                                    if($type != "View"){
                                        ?>
                                        <div class="slide-primary">
                                            <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                        </div>
                                    <?php }else{?>
                                        <p><strong><?php echo $record['Status']?></strong></p>
                                    <?php }?>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/services')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "sScrollX": '130%',
        "sScrollY": "500",
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/services/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,
                null,null,null,null,null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/services/add')?>">Add</a>');
    $(document).on("click","#desktop_remove_image",function(){
        $("#desktop_pic_path").val("");
        $("#desktop_view").hide();
        $("#desktop_image_preview").removeAttr("src");
        return false;
    });
    $(document).on("click","#mobile_remove_image",function(){
        $("#mobile_pic_path").val("");
        $("#mobile_view").hide();
        $("#mobile_image_preview").removeAttr("src");
        return false;
    });
    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>