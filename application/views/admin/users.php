<div class="content">
    <?php 
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/users'), isset($parent_page_name)?$parent_page_name:'Users');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Users');  
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Users</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="15%" data-hide="phone,tablet">Franchise</th>
                                    <th width="15%" data-hide="phone,tablet">First Name</th>
                                    <th width="15%" data-hide="phone,tablet">Last Name</th>
                                    <th width="20%">Email Address</th>
                                    <th width="15%" data-hide="phone">Status</th>
                                    <th width="15%">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <?php }else{
                            $disabled_attribute = '';
                            if($type == "View"){
                                $disabled_attribute = ' disabled="disabled"';
                            }
                            ?>
                        <form class="form-no-horizontal-spacing" id="frm_user" name="frm_user" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                            <input type="hidden" name="admin_id" id="admin_id"<?php echo isset($record['PKUserID'])?' value="' . $record['PKUserID'] . '"':''?> />
                            <input type="hidden" id="assigned_output" name="assigned_output" />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">First Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="first_name" id="first_name" type="text" class="form-control" placeholder="First Name"<?php echo $disabled_attribute;echo isset($record['FirstName'])?' value="' . $record['FirstName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Last Name <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="last_name" id="last_name" type="text" class="form-control" placeholder="Last Name"<?php echo $disabled_attribute;echo isset($record['LastName'])?' value="' . $record['LastName'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <br clear="all" /><br />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Email Address <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <input name="email_address" id="email_address" type="text" class="form-control" placeholder="email@address.com"<?php echo $disabled_attribute;echo isset($record['EmailAddress'])?' value="' . $record['EmailAddress'] . '"':''?> />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                <label class="form-label">Password
                                    <?php
                                    if($type == "Add"){
                                        ?>
                                        <span class="red-color">*</span>
                                    <?php }?>
                                        </label>
                                    <div class="controls">
                                        <?php
                                        if($type == "View" && isset($record['Password'])){
                                        ?>
                                        <input name="u_password" id="u_password" type="text" class="form-control" placeholder="Password" disabled="disabled" value="<?php echo $record['Password']?>" />
                                        <?php }else{?>
                                            <input name="u_password" id="u_password" type="password" class="form-control" placeholder="Password" />
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                            <br clear="all" /><br />
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Franchise <span class="red-color">*</span></label>
                                    <div class="controls">
                                        <select id="franchise" name="franchise[]" multiple="multiple" class="form-control select2"<?php echo $disabled_attribute;?>>
                                            <option value="">Select</option>
                                            <?php
                                            if(isset($franchise_records) && sizeof($franchise_records) > 0){
                                                if(isset($record['FKFranchiseID'])){
                                                    $FKFranchiseID = explode(",",$record['FKFranchiseID']);
                                                }else{
                                                    $FKFranchiseID = array();
                                                }
                                                foreach($franchise_records as $rec){
                                                    $franchise_selected = "";
                                                        if(in_array($rec['ID'], $FKFranchiseID)){
                                                            $franchise_selected = ' selected="selected"';
                                                        }
                                                    
                                                    
                                                   echo '<option value="' . $rec['ID'] . '"'.$franchise_selected.'>' . $rec['Title'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6">
                                <label class="form-label">Status</label>
                                <?php
                                if($type != "View"){
                                ?>
                                <div class="slide-primary">
                                    <input type="checkbox" name="status" id="status" class="ios"<?php echo isset($record['Status'])?' checked="checked"':''?> />
                                </div>
                                <?php }else{?>
                                    <p><strong><?php echo $record['Status']?></strong></p>
                                <?php }?>
                            </div>
                            <br clear="all" /><br />
                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <h3>Administrator <span class="semi-bold">Menus</span></h3>
                                <?php
                                if($type != "View"){
                                ?>
                                <p>Left Side is user assigned menu list and the Right Side is user unassigned menu list</p>
                                <?php }?>
                                <br /><br />
                                <div class="cf nestable-lists">
                                    <div class="dd col-md-6 col-xs-12 col-lg-6" id="assigned_menus" style="margin-left:0px;height:300px !important;overflow-y: auto;">
                                       <?php
                                            if(isset($admin_assigned_menus) && sizeof($admin_assigned_menus) > 0){
                                                $handle_class = 'dd-handle';
                                                if($type == "View"){
                                                    $handle_class = "dd-nodrag";
                                                }
                                                echo '<ol class="dd-list">';
                                                 foreach($admin_assigned_menus as $rec){
                                                     echo '<li class="dd-item" data-id="' . $rec['MenuID'] .'">';
                                                     echo '<div class="' . $handle_class . '">' . $rec['Name'] . '</div>';
                                                     if(isset($rec['children']) && sizeof($rec['children']) > 0){
                                                         echo '<ol class="dd-list">';
                                                         foreach($rec['children'] as $child_rec){
                                                             echo '<li class="dd-item" data-id="' . $child_rec['MenuID'] . '"><div class="' . $handle_class . '">' . $child_rec['Name'] . '</div></li>';
                                                         }
                                                         echo '</ol>';
                                                     }
                                                     echo '</li>';
                                                  }
                                                echo '</ol>';
                                            }else{
                                                echo '<div class="dd-empty"></div>';
                                            }
                                       ?>
                                    </div>
                                    <?php
                                        if($type != "View"){
                                    ?>
                                    <div class="dd col-md-5 col-xs-12 col-lg-5" id="unassigned_menus" style="margin-left:20px;height:300px !important;overflow-y: auto;">
                                        <?php
                                            if(isset($menus) && sizeof($menus) > 0){
                                                echo '<ol class="dd-list dark">';
                                                foreach($menus as $rec){
                                                    echo '<li class="dd-item" data-id="' . $rec['MenuID'] . '"><div class="dd-handle">' . $rec['Name']  . '</div></li>';
                                                }
                                                echo '</ol>';
                                            }else{
                                                echo '<div class="dd-empty"></div>';
                                            }
                                        ?>
                                    </div>
                                    <?php }?>
                                    <br clear="all" />
                                </div>
                            </div>
                            <br clear="all" /><br /><br />
                            <div class="col-md-12 col-xs-12 col-lg-12">
                                <div class="float-right">
                                    <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                    ?>
                                    <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                    <?php }?>
                                    <a href="<?php echo site_url('admin/users')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                </div>
                            </div>
                        </form>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/users/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,null,null,null,
                null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/users/add')?>">Add</a>');
    $(document).ready(function(){
        <?php
            if(isset($record['Status'])){
        ?>
        ChangeSwitch("#status","<?php echo $record['Status']?>");
        <?php
        }else{
        ?>
        ChangeSwitch("#status","Enabled");
        <?php
        }if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
    ?>
    });
</script>
