<div class="content">
    <?php
    if(isset($type)){
        echo show_admin_bread_crumbs($type, site_url('admin/navigations'), isset($parent_page_name)?$parent_page_name:'Navigations');
    }else{
        echo show_admin_bread_crumbs('List', site_url('admin/dashboard'), isset($parent_page_name)?$parent_page_name:'Navigations');
    }
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4><span class="semi-bold">Navigations</span></h4>
                </div>
                <div class="grid-body">
                    <div class="row">
                        <div id="msg_box" class="alert hide-area">
                        </div>
                        <?php
                        if(!isset($type)){
                        ?>
                        <table class="table table-striped dataTable recordTable">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="80%">Title</th>
                                <th width="15%">Options</th>
                            </tr>
                            </thead>
                        </table>
                        <?php }else{
                        $disabled_attribute = '';
                        if($type == "View"){
                            $disabled_attribute = ' disabled="disabled"';
                        }
                        ?>
                            <form class="form-no-horizontal-spacing" id="frm_navigation" name="frm_navigation" action="<?php echo isset($form_action)?$form_action:'#'?>" method="post">
                                <input type="hidden" name="section_id" id="section_id"<?php echo isset($record['PKSectionID'])?' value="' . $record['PKSectionID'] . '"':''?> />
                                <input type="hidden" id="assigned_output" name="assigned_output" />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Title <span class="red-color">*</span></label>
                                        <div class="controls">
                                            <input type="text" id="n_title" name="n_title" class="form-control"<?php echo $disabled_attribute;echo isset($record['Title'])?' value="' . $record['Title'] . '"':''?> />
                                        </div>
                                    </div>
                                </div>
                                <br clear="all" />
                                <?php
                                if($type != "View") {
                                ?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Page</label>
                                        <div class="controls">
                                            <select id="page" name="page" class="form-control select2">
                                                <option value="">Select</option>
                                                <?php
                                                    if(isset($page_records) && sizeof($page_records) > 0){
                                                        foreach($page_records as $rec){
                                                            echo '<option value="' . $rec['ID'] . '||' . $rec['Title'] . '||' . $rec['URL'] . '">' . $rec['Title'] . ' (' . $rec['URL'] . ')</option>';
                                                        }
                                                    }
                                                ?>
                                            </select><br clear="all" />
                                            <button type="button" class="btn btn-white btn-cons" id="btn_page">Add</button>
                                        </div>
                                    </div>
                                    <h3>Add Custom Link</h3>
                                    <div class="form-group">
                                        <div class="control-group">
                                            <label class="form-label">Title</label>
                                            <div class="controls">
                                                <input type="text" id="title" name="title" class="form-control link">
                                            </div>
                                        </div><br clear="all" />
                                        <div class="control-group">
                                            <label class="form-label">URL</label>
                                            <div class="controls">
                                                <input type="text" id="url" name="url" class="form-control link">
                                            </div>
                                        </div><br clear="all" />
                                        <button type="button" class="btn btn-white btn-cons" id="btn_link">Add</button>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="col-md-6 col-xs-12 col-lg-6">
                                    <div class="dd" id="nestable_menus">
                                        <ol class="dd-list" id="p_list">
                                            <?php
                                                if(isset($record['Navigation']) && sizeof($record['Navigation']) > 0){
                                                    $handle_class = 'dd-handle';
                                                    if($type == "View"){
                                                        $handle_class = "dd-nodrag";
                                                    }
                                                    foreach($record['Navigation'] as $rec){
                                                        $random = mt_rand();
                                                        echo  '<li class="dd-item dd3-item" data-id="' . $rec['FKPageID'] . '" data-name="' . $rec['Name'] . '" data-url="' . $rec['LinkWithURL'] . '"><div class="' . $handle_class . ' dd3-handle"></div>';
                                                        echo '<input type="hidden" id="p_id" name="p_id[]" value="' . $rec['FKPageID'] . '" />';
                                                        echo '<input type="hidden" id="p_text" name="p_text[]" value="' . $rec['PageName'] . '" />';
                                                        echo '<input type="hidden" id="p_url" name="p_url[]" value="' . $rec['LinkWithURL'] . '" />';
                                                        echo '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
                                                        echo '<div class="accordion-heading"> <a href="#collapse' . $random . '" data-toggle="collapse" class="accordion-toggle collapsed">' . $rec['Name'] . ' (' . $rec['LinkWith'] . ')<i class="fa fa-plus"></i> </a> </div>';
                                                        echo '<div class="accordion-body collapse" id="collapse' . $random . '" style="height: 0px;"><div class="accordion-inner">';
                                                        echo '<div class="form-group"><label class="form-label">Title</label><div class="controls">';
                                                        echo '<input type="text" id="nav_label' . $random . '" name="nav_label[]" class="form-control nav-title" value="' . $rec['Name'] . '"></div></div>';
                                                        if ($rec['FKPageID'] != 0) {
                                                            echo '<div class="original-name"><p>Original : ' . $rec['PageName'] . '</p></div>';
                                                        } else {
                                                            echo '<div class="form-group"><label class="form-label">URL</label><div class="controls">';
                                                            echo '<input type="text" id="nav_url' . $random . '" name="nav_url[]" class="form-control nav-url" value="' . $rec['LinkWithURL'] . '"></div></div>';
                                                        }
                                                        if($type != "View") {
                                                            echo '<div class="control-button"><a class="remove-nav">Remove</a><a class="cancel-nav">Close</a></div>';
                                                        }
                                                        echo '</div></div></div></div></div></li>';
                                                    }
                                                }
                                            ?>
                                        </ol>
                                    </div>
                                </div>
                                <br clear="all" /><br /><br />
                                <div class="col-md-12 col-xs-12 col-lg-12">
                                    <div class="float-right">
                                        <?php
                                        $button_text = "Back";
                                        if($type != "View"){
                                            $button_text = "Cancel";
                                            ?>
                                            <button class="btn btn-primary btn-cons" type="submit" id="btn_submit">Save</button>
                                        <?php }?>
                                        <a href="<?php echo site_url('admin/navigations')?>" class="btn btn-danger btn-cons" id="btn_option"><?php echo $button_text?></a>
                                    </div>
                                </div>
                            </form>
                        <?php }?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/includes/footer');?>
<script type="text/javascript">
    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone : 480
    };
    var tableElement = $('.recordTable');

    tableElement.dataTable( {
        "sPaginationType": "bootstrap",
        "aaSorting": [[ 0, "desc" ]],
        'bProcessing'    : true,
        'bServerSide'    : true,
        "aLengthMenu": [[20, 50, 100 ,500, 1000, -1], [20, 50, 100, 500, 1000, "All"]],
        "iDisplayLength" : 20,
        "cache" : false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        },
        bAutoWidth     : false,
        fnPreDrawCallback: function () {
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper(tableElement, breakpointDefinition);
            }
        },
        'ajax'    : {
            url: '<?php echo site_url('admin/navigations/listener'); ?>',
            type:'POST'
        },
        'aoColumns'      :
            [
                {
                    'bSearchable': true,
                    'bVisible'   : true
                },
                null,{ "bSortable": false,"bSearchable":false }

            ],
        fnRowCallback  : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        fnDrawCallback : function (oSettings) {
            responsiveHelper.respond();
        }

    });
    $("#DataTables_Table_0_length").append('<a class="btn btn-primary" style="margin-left:12px" href="<?php echo site_url('admin/navigations/add')?>">Add</a>');

    $(document).ready(function(){
        <?php
        if(isset($admin_message)){
         ?>
        MessageBoxSuccess("#msg_box","<?php echo $admin_message?>",1000,"");
        scrollToPosition($("#msg_box"));
        <?php
         }
         ?>
    });
</script>