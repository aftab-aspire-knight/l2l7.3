<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
$pickups = $this->config->item("pickup_timings");
?>
<script>

    var vat = <?php echo numberformat($vat) ?>;
    var stripe_key = "<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>";
    var address1 = "<?php echo $address ?>";
    var address2 = "<?php echo $address2 ?>";
    var date = "<?php echo $date ?>";
    var name = "<?php echo $member["FirstName"] ?> <?php echo $member["LastName"] ?>";
        var email = "<?php echo $member["EmailAddress"] ?>";
</script>
<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="bg-white p-3 p-md-4 p-lg-5 rounded-xl">

                    <div class="cell example example2" id="example-2">

                        <form id="checkout-form" autocomplete="off" >
                            <div class="order-address steps" data-id= "step1_next_button" >
                                <h2 class="order-heading py-2 pb-md-3">Address</h2>
                                <input type="hidden" id="stripe_key" value="<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>" />
                                <input type="hidden" id="payment_method_id" name="payment_method_id" value="" >
                                <input type="hidden" id="payment_type" name="payment_type" value="" >
                                <input type="hidden" id="franchise_id" name="franchise_id" value="" >
                                <input type="hidden" id="pickup_date" name="pickup_date" value="<?php echo $date ?>" >
                                <input type="hidden" id="pickup_time" name="pickup_time" value="<?php echo $date ?>" >
                                <input type="hidden" id="client_secret" name="client_secret" value="<?php echo $client_secret ?>" >
                                <input type="hidden" id="setup_intent_id" name="setup_intent_id" value="<?php echo $setup_intent_id ?>" >

                                <div id='step1_errors'></div>
                                <div class="form-group mb-3 mb-md-4">
                                    <label><?php echo $postal_code_type["label"]; ?></label>
                                    <input class="form-control form-control-lg text-uppercase" type="text" placeholder="<?php echo $postal_code_type["placeholder"]; ?>"  id="postal_code" name="postal_code" value="<?php echo isset($postal_code) ? $postal_code : '' ?>" autocomplete="new-password"  />
                                    <div class="search-result" style="display: none;" id="google_addresses">

                                    </div>
                                </div>

                                <div class="form-group mb-3 mb-md-4 custom-select2" style="display: none;" id="div_addresses">
                                    <label>Select Your Address</label>
                                    <select id="addresses" name="addresses" class="custom-select custom-select-lg select2"></select>
                                </div>
                                <div class="form-group mb-3 mb-md-4" id="div_reg_address1" style="display: none;">
                                    <label>Enter your address <span class="red-color">*</span></label>
                                    <input id="address" name="address" type="text" placeholder="enter your address" class="form-control form-control-lg  address-building"  />
                                </div>
                                <div class="form-group mb-3 mb-md-4">
                                    <label>Address Line 2</label>
                                    <input type="text" placeholder="Please specify any extra address details" class="form-control form-control-lg  address-street" value="" id="address2" name="address2" />
                                    <input type="hidden" placeholder="Town" value="" class="form-control address-town" id="town" name="town" />
                                </div>
                            </div>

                            <div class="order-address steps step2 order-timeslots d-none" data-id= "order_schedule">

                                <div id='order_collection_errors'></div>
                                <h2 class="order-heading py-2 pb-md-3">When to pick up?</h2>
                                <div class="row align-items-center">
                                    <div class="col-lg-3 col-12 text-center">
                                        <img width="180" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'collection.svg' ?>" />
                                    </div>
                                    <div class="col-md col-12">
                                        <div class="form-group mb-1 custom-select2">
                                            <label>Date</label>
                                            <!--select2-->
                                            <select id="schedule" name="schedule" class="custom-select custom-select-lg">
                                                <option>Select Day</option>
                                                <?php
                                                foreach ($pickups as $k => $pickup) {
                                                    ?>
                                                    <option value="<?php echo $k ?>"><?php echo $pickup; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md col-12">
                                        <div class="form-group mb-1">

                                            <label>Time</label>
                                            <div id="pickup_time_div" class="select-dropdown">
                                                <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                <div class="dropdown-body" style="display: none;">
                                                    <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                    <span class="time-slot" data-time=""><i class="fa fa-leaf" aria-hidden="true"></i>
                                                    </span>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="order-address steps step2 d-none" data-id= "order_payments" >

                                <h2 class="order-heading py-2 pb-md-3">Payments Options </h2>


                                <div class="row stripe" >
                                    <div id='payment_errors'></div>

                                    <div class="col-md-12 col-12 cards-list" id="card-errors" ></div>
                                    <div class="col-md-12 col-12 cards-list" id="cards-list" 
                                    <?php if (empty($cards)) { ?>
                                             style="display: none;" 
                                         <?php } ?>
                                         >
                                        <div class="form-group col-12 offset-md-2 col-md-8 mb-3 mb-md-4">
                                            <button type="button" class="btn btn-block btn-outline-dark rounded-0  order-payment-btn payment-cards-a cards-list" data-show="card-form" data-hide="cards-list" ><i class="fas fa-plus mr-2"></i>Add Payment</button>
                                        </div>
                                        <div class="form-group mb-2 mb-md-4">
                                            <label>Select Card <span class="red-color">*</span></label>

                                            <?php
                                            $i = 0;
                                            foreach ($cards as $card) {
                                                ?>
                                                <div class="custom-control custom-radio btn btn-block btn-payment border shadow mb-3 py-3 rounded-0 <?php if ($i == 0) { ?> active <?php } ?>"  >
                                                    <input type="radio" value="<?php echo $card["id"] ?>" id="payments_cards_<?php echo $card["id"]; ?>" class="custom-control-input payments-cards" <?php if ($i == 0) { ?> checked="checked" <?php } ?> name="payments_cards">

                                                    <label class="custom-control-label form-row px-3 align-items-center" for="payments_cards_<?php echo $card["id"]; ?>">
                                                        <div class="col-7 text-left">
                                                            <p class="m-0">Card Number: <?php echo $card["title"] ?></p>
                                                        </div>
                                                        <div class="col-5 text-md-right">
                                                                <!-- <i class="fab fa-cc-visa fa-2x text-primary"></i> -->
                                                        </div>
                                                    </label>
                                                </div>

                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                    </div>


                                    <div class="cell example example2 card-form col-md-12 col-12" id="card-form" <?php if (!empty($cards)) { ?>
                                             style="display: none;" 
                                         <?php } ?> >
                                        <div class="row">
                                            <div class="form-group col-12 offset-md-2 col-md-8 mb-3 mb-md-4">
                                                <button type="button" <?php if (empty($cards)) { ?> style="display: none;"   <?php } ?>class="btn btn-block btn-outline-dark order-payment-btn mx-1 payment-cards-a" data-show="cards-list" data-hide="card-form" ><i class="far fa-edit mr-2"></i>Use Existing</button>
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">Card number</label>
                                                    <div id="card-number" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiry Date</label>
                                                    <div id="card-expiry" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">CVC</label>
                                                    <div id="card-cvc" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-12 mb-1 mb-md-3">
                                                <small>*Please note the final itemisation will done by the facility. The total invoice amount may vary.</small>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="form-group mb-3 mb-md-4 next-previous-buttons step2 d-none">
                                <div class="row justify-content-end">
                                    <div class="col-6 text-right">
                                        <button type="submit" class="btn btn-primary btn-order btn-lg btn-block rounded-pill next-rgt-confirm-button"  data-tid="elements_examples.form.pay_button">Submit Request</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>