<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
$pickups = $this->config->item("pickup_timings");
?>
<script>

    var stripe_key = "<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>";
    var date = "<?php echo $date ?>";

</script>
<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="bg-white p-3 p-md-4 p-lg-5 rounded-xl">
                    <div class="card-listing" id="card_listing">
                        <div class="mb-3 bg-white rounded-xl">
                            <div class="card-listing" id="card_listing">
                                <div class="d-md-flex align-items-center justify-content-between pb-3">
                                    <h3 class="h4 py-3">My Schedules</h3>
                                    <a class="btn btn-dark rounded-pill d-block d-md-inline-block" href="<?php echo base_url($this->config->item('front_member_schedule_order')) ?>"><i class="fas fa-plus"></i> </a>
                                </div>

                                <?php
                                if (!empty($schedules)) {
                                    echo '<div class="mb-3 bg-white rounded-xl">';

                                    foreach ($schedules as $schedule) {
                                        $title = $pickups[$schedule["schedule"]] . " between " . $schedule["pickup_time"];
                                        $address = $schedule["address"] . ", " . $schedule["postcode"] . ", " . $schedule["town"];

                                        echo '<ul class="list-order list-group list-group-horizontal-sm">';
                                        echo '<li class="col-8 col-md-9 list-group-item py-2 border-0 list-' . $schedule['id'] . '"><div class="h6 font-weight-bold text-dark">' . $title . '</div><div class="h5 mb-0">' . $address . '</div></li>';
                                        echo '<li class="col-4 col-md-3 list-group-item py-2 border-0 text-right"> <a href="#" data-id="' . $schedule['id'] . '" class="btn btn-primary btn-sm rounded-pill px-2 text-white remove-schedule" data-toggle="modal" ><i class="fas fa-trash-bin"></i> Delete</a></li>';
                                        echo '</ul>';
                                    }

                                    echo '</div>';
                                } else {
                                    echo '<div class="alert alert-danger">You havn\'t added any schedule.</div>';
                                }
                                ?>
                            </div>

                        </div>                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-schedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete schedule?</p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="schedule_id" name="schedule_id" value="" >
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary confirm-delete-schedule">Yes!</button>
            </div>
        </div>
    </div>
</div>