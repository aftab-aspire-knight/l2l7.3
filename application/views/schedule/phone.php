<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
$pickups = $this->config->item("pickup_timings");
?>
<script>
    var stripe_key = "";
</script>
<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="bg-white p-3 p-md-4 p-lg-5 rounded-xl">
                    <form class="form-horizontal custom-form" id="frm_phone_setting" name="frm_phone_setting" method="post" action="<?php echo base_url('schedule/updatephone') ?>">
                        <input type="hidden" value="<?php echo $this->config->item("country_phone_code"); ?>" readonly />
                        <div id="msg_box"></div>
                        <h3 class="h4 py-3 border-bottom"><span>Please enter your correct phone number</span></h3>
                        <div class="form-row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group mb-2 mb-md-3 ">
                                        <label>Phone</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend country-code"> 
                                                <span class="input-text-country">+<?php echo $this->config->item("country_phone_code") ?></span>
                                                <select class="custom-select custom-select-lg" id="countryCode" name="countryCode" >

                                                    <?php
                                                    foreach ($codes as $key => $country) {
                                                        ?>
                                                        <option value="<?php echo $key ?>" <?php if ($key == $this->config->item("country_phone_code")) { ?> selected <?php } ?>><?php echo $country ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <input type="tel" class="form-control form-control-lg only-number " id="phone_number" name="phone_number" value="<?php echo isset($member_detail['Phone']) ? $member_detail['Phone'] : '' ?>" />
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary btn-lg d-table mx-auto rounded-pill px-5 text-uppercase" id="btn_profile"><span class="px-5">Update</span></button>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>
</div>