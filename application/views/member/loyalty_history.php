<div class="py-3 py-md-4 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-xl-3">
				<?php $this->load->view('member/sidebar');?>
			</div>
			<div class="col-lg-8 col-xl-9">
				<div class="bg-white p-3 p-lg-5 rounded-xl">
					<h3 class="h4 py-3 border-bottom">Loyality Active Code</h3>
					<?php
                        if(isset($discount_records)){
							foreach($discount_records as $record){
								$active = ' badge-primary';
								if($record['Status'] != "Active"){
                                    echo ' badge-danger';
                                }
                                echo '<span class="badge badge-pill mx-1 px-3 py-2 mb-2' . $active . '">' . $record['Code'] . '</span>';
                                
                            }
                        }else{
                            echo '<p>No Loyalty Discount Codes</p>';
                        }
                    ?>
					<h3 class="h4 py-3">Loyality History</h3>
					<?php
					if(isset($loyalty_records)){
						echo '<div class="table-responsive">';
						echo '<table class="table table-striped table-fixed">';
						echo '<thead>';
							echo '<tr>';
							echo '<td width="20%" scope="col">Invoice ID</td>';
							echo '<td width="25%" class="text-center" scope="col">Invoice Amount</td>';
							echo '<td width="30%" class="text-center" scope="col">Created On</td>';
							echo '<td width="25%" class="text-center" scope="col">Loyality Points</td>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
						foreach($loyalty_records as $record){
							echo '<tr><td width="20%" class="invoice">' . $record['InvoiceNumber'] . '</td><td width="25%"  class="text-center" >' . $this->config->item('currency_sign') . $record['GrandTotal'] . '</td><td width="30%"  class="text-center" >' . date('d-M-Y',strtotime($record['CreatedDateTime'])) . ' | ' . date('H:i',strtotime($record['CreatedDateTime'])) . '</span><td width="25%" class="text-center">' . $record['Points'] . '</td></tr>';
						}
						echo '</tbody>';
						echo '<tfoot align="right">';
						
						echo '<tr class="loyality-total"><td colspan="4" class="total-points">Total Points you got <strong>' . $member_detail['TotalLoyaltyPoints'] . '</strong></td></tr>';
						echo '<tr class="loyality-total"><td colspan="4" class="total-points">Points Used <strong>' . $member_detail['UsedLoyaltyPoints']. '</strong></td></tr>';
						echo '<tr class="loyality-total"><td colspan="4" class="total-points">Balance Points <strong>' . $member_loyalty_points . '</strong></span></tr>';
						echo '</table>';
						echo '</table>';
						
						echo '</div>';
					}else{
						echo '<p>No Loyalty Points</p>';
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>