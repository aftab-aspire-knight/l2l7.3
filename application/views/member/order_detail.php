<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <?php if (!isset($is_admin_view)) { ?>
                <div class="col-lg-4 col-xl-3">
                    <?php $this->load->view('member/sidebar'); ?>
                </div>
            <?php } ?>
            <div class="<?php echo isset($is_admin_view) ? 'col-md-12' : 'col-lg-8 col-xl-9'; ?>">
                <div class="bg-white p-3 p-lg-5 rounded-xl">
                    <div class="row pb-3 border-bottom">
                        <div class="col-md-6">
                            <h2 class="h4 text-uppercase mb-0"><?php echo isset($member_detail) ? $member_detail['FirstName'] . ' ' . $member_detail['LastName'] : '' ?></h2>
                        </div>
                        <div class="col-md-6 text-right">
                            <h3 class="h4 mb-0">Invoice #<strong><?php echo isset($invoice_record) ? $invoice_record['InvoiceNumber'] : '' ?></strong></h3>
                        </div>
                    </div>
                    <div class="row align-items-center pt-3">
                        <div class="col-md-6">
                            <address>
                                <?php
                                if (isset($FKFranchiseID) && $FKFranchiseID > 0) {
                                    echo '';
                                } else {
                                    ?> 
                                    <strong>Billing Information</strong><br>
                                    <div class="py-1 d-flex"><i class="fas fa-phone fa-flip-horizontal mr-2"></i> <?php echo isset($member_detail) ? $member_detail['Phone'] : '' ?></div>
                                    <div class="py-1 d-flex"><i class="fas fa-envelope mr-2"></i> <?php echo isset($member_detail) ? $member_detail['EmailAddress'] : '' ?></div>			
                                    <div class="py-1 d-flex"><i class="fas fa-location-arrow mr-2"></i>
                                        <?php
                                        if (isset($invoice_record)) {
                                            if (!empty($invoice_record['BuildingName'])) {
                                                echo $invoice_record['BuildingName'] . ',';
                                            }
                                            if (!empty($invoice_record['StreetName'])) {
                                                echo $invoice_record['StreetName'] . '<br/>';
                                            }
                                            if (!empty($invoice_record['Town'])) {
                                                echo $invoice_record['Town'];
                                            }
                                            if (!empty($invoice_record['PostalCode'])) {
                                                echo ', ' . $invoice_record['PostalCode'];
                                            }
                                        }
                                    }
                                    echo '</div>';
                                    if ($invoice_record['Regularly']) {
                                        echo 'Regularly: ' . (isset($REGULARITY[$invoice_record['Regularly']]) ? $REGULARITY[$invoice_record['Regularly']] : 'Just once');
                                    }
                                    ?>
                            </address>
                        </div>
                        <div class="col-md-6">
                            <img width="100" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo-black.png' ?>" alt="" class="img-fluid float-right"> 
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="p-3 bg-primary text-center">
                                <div> Created Time </div>
                                <div><strong><?php echo isset($invoice_record) ? date('d-M-Y', strtotime($invoice_record['CreatedDateTime'])) : '' ?><br>
                                        <?php echo isset($invoice_record) ? date('h:i a', strtotime($invoice_record['CreatedDateTime'])) : '' ?></strong></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-3 bg-dark text-center text-white">
                                <div class="pull-left"> Pickup Time </div>
                                <div class="pull-right text-center"><strong><?php echo isset($invoice_record) ? date('d-M-Y', strtotime($invoice_record['PickupDate'])) : '' ?><br>
                                        <?php echo isset($invoice_record) ? $invoice_record['PickupTime'] : '' ?></strong></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="p-3 bg-success text-center text-white">
                                <div> Delivery Time </div>
                                <div><strong><strong><?php echo isset($invoice_record) ? date('d-M-Y', strtotime($invoice_record['DeliveryDate'])) : '' ?><br>
                                            <?php echo isset($invoice_record) ? $invoice_record['DeliveryTime'] : '' ?></strong></strong></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php
                    if (isset($invoice_record)) {
                        if (!empty($invoice_record['OrderNotes'])) {
                            echo '<div class="row">';

                            if (!empty($invoice_record['OrderNotes'])) {
                                echo '<div class="col-md-12">';
                                echo '<h4 class="h4">Delivery Notes</h4>';
                                echo '<p>' . $invoice_record['OrderNotes'] . '</p>';
                                echo '</div>';
                            }
                            echo '</div>';
                            echo '<hr>';
                        }
                    }
                    ?>
                    <?php
                    echo '<div class="table-responsive"><table class="table">';
                    if (isset($invoice_record['service_records'])) {

                        echo '<thead>';
                        echo '<tr>';
                        echo '<th class="text-left" width="55%">Title</th>';
                        echo '<th class="text-right" width="15%">Quantity</th>';
                        echo '<th class="text-right" width="15%">Price</th>';
                        echo '<th class="text-right" width="15%">Total</th>';
                        echo '</tr>';
                        echo '</thead>';
                        echo '<tbody>';
                        foreach ($invoice_record['service_records'] as $record) {
                            $total = floatval(intval($record['Quantity']) * ($record['Price']));
                            echo '<tr>';
                            echo '<td>' . $record['Title'] . '</td>';
                            echo '<td class="text-right">' . $record['Quantity'] . '</td>';
                            echo '<td class="text-right">' . $invoice_record['Currency'] . $record['Price'] . '</td>';
                            echo '<td class="text-right">' . $invoice_record['Currency'] . $total . '</td>';
                            echo '</tr>';
                        }
                        echo '<tr>';
                        echo '<td colspan="2"></td>';
                        echo '<td class="text-right"><strong>Services Total</strong></td>';
                        echo '<td class="text-right">' . $invoice_record['Currency'] . $invoice_record['ServicesTotal'] . '</td>';
                        echo '</tr>';
                    }
                    if (isset($invoice_record['preference_records'])) {
                        if (!isset($invoice_record['service_records'])) {
                            echo '<tbody>';
                        }
                        echo '<tr valign="middle">';
                        echo '<td colspan="4"><h2 class="h4 pb-3 border-bottom">Customer Preferences</h2>';
                        $up_area = "";
                        $down_area = "";
                        $count = 0;
                        foreach ($invoice_record['preference_records'] as $record) {
                            if ($record['ParentTitle'] == "Starch on Shirts") {
                                $down_area .= '<tr>';
                                $down_area .= '<td width="20%">' . $record['ParentTitle'] . '</td>';
                                $down_area .= '<td width="30%">' . $record['Title'] . '</td>';
                                $down_area .= '</tr>';
                            } else {
                                if ($count == 0) {
                                    $up_area .= '<tr>';
                                }
                                $up_area .= '<td width="20%">' . $record['ParentTitle'] . '</td>';
                                $up_area .= '<td width="30%">' . $record['Title'] . '</td>';
                                if ($count == 1) {
                                    $up_area .= '</tr>';
                                    $count = 0;
                                } else {
                                    $count = 1;
                                }
                            }
                        }
                        if ($count == 1) {
                            $up_area .= '</tr>';
                        }
                        echo '<table class="table table-striped table-bordered">';
                        echo '<tr>';
                        echo '<th colspan="4" class="text-center text-white bg-dark"> <h3 class="h5 mb-0 text-uppercase">' . $this->config->item('front_member_laundry_first_heading_text') . '</h3>';
                        echo '</th>';
                        echo '</tr>';
                        echo $up_area;
                        echo '</table>';
                        if (!empty($down_area)) {
                            echo '<table class="table table-striped table-bordered">';
                            echo '<tr>';
                            echo '<th colspan="2" class="text-center text-white bg-dark"> <h3 class="h5 mb-0 text-uppercase">' . $this->config->item('front_member_laundry_second_heading_text') . '</h3>';
                            echo '</th>';
                            echo '</tr>';
                            echo $down_area;
                            echo '</table>';
                        }
                        echo '</td>';
                        echo '</tr>';
                    }
                    if (!isset($invoice_record['service_records']) && !isset($invoice_record['preference_records'])) {
                        echo '<tbody>';
                    }
                    echo '<tr>
						<td width="50%" colspan="2">&nbsp;</td>
						<td width="25%" class="text-right no-border" style="vertical-align:middle"><strong>Preferences Total</strong></td>
						<td width="25%" class="text-right"  style="vertical-align:middle">' . $invoice_record['Currency'] . $invoice_record['PreferenceTotal'] . '</td>
						</tr>';
                    $discount_label = $invoice_record['DiscountType'];
                    if ($discount_label == "None") {
                        $discount_label = "Discount";
                    }
                    echo '<tr><td width="50%" colspan="2">&nbsp;</td>
						<td width="25%" class="text-right no-border" style="vertical-align:middle"><strong>' . $discount_label . ' Total';
                    if (!empty($invoice_record['DiscountCode'])) {
                        echo '<br/><small>Code:' . $invoice_record['DiscountCode'] . '</small>';
                        if ($invoice_record['DType'] == "Percentage") {
                            echo '<br/><small>Worth:' . $invoice_record['DiscountWorth'] . '%</small>';
                        } else if ($invoice_record['DType'] == "Price") {
                            echo '<br/><small>Worth:' . $this->config->item('currency_symbol') . $invoice_record['DiscountWorth'] . '</small>';
                        }
                    }
                    echo '</strong></td>';
                    if (empty($invoice_record['DiscountTotal'])) {
                        $invoice_record['DiscountTotal'] = "0.00";
                    }
                    echo '<td width="25%" class="text-right"  style="vertical-align:middle">(' . $invoice_record['Currency'] . $invoice_record['DiscountTotal'] . ')</td>
						</tr>';
                    echo '<tr class="well">
							<td width="50%" colspan="2">&nbsp;</td>
							<td width="25%" class="text-right no-border" style="vertical-align:middle"><strong>Grand Total</strong></td>
							<td width="25%" class="text-right"  style="vertical-align:middle"><strong>' . $invoice_record['Currency'] . $invoice_record['GrandTotal'] . '</strong></td>
						</tr>';
                    echo '</tbody>';
                    echo '</table></div>';
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>