<div class="py-3 py-md-4 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-xl-3">
				<?php $this->load->view('member/sidebar');?>
			</div>
			<div class="col-lg-8 col-xl-9">
				<div class="bg-white p-3 p-lg-5 rounded-xl">
					<?php
						if(isset($new_discount_records)){
							echo ' <h3 class="h4 pb-3 border-bottom">New Discount Codes</h3>';
							echo '<div class="available-code">';
							foreach($new_discount_records as $record){
								echo '<a href="#" class="btn btn-dark rounded-pill my-1 mr-2">';
								echo '<span class="code">' . $record['Code'] . '</span> <span class="badge badge-primary badge-pill mx-1">' . $record['WorthValue'] . '</span>';
								echo '<span class="small d-block">' . $record['Expiry'] . '</span>';
								echo '</a>';
							}
							echo '</div>';
						}
					?>
					
					<h3 class="h4 py-3 mt-3">Used Discount Codes</h3>
					<?php
					if(isset($discount_records)){
						echo '<div class="table-responsive">';
						echo '<table class="table table-striped table-fixed">';
						echo '<thead>';
							echo '<tr>';
							echo '<td width="20%" scope="col">Invoice ID</td>';
							echo '<td width="25%" class="text-center" scope="col">Discount Code</td>';
							echo '<td width="30%" class="text-center" scope="col">Invoice Amount</td>';
							echo '<td width="25%" class="text-center" scope="col">View</td>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
						foreach($discount_records as $record){
							echo '<tr><td width="20%" class="invoice">' . $record['InvoiceNumber'] . '</td><td width="30%"  class="text-center" >' . $record['Code'] . '</td><td width="25%" class="text-center" >' . $this->config->item('currency_sign') . $record['GrandTotal'] . '</td><td width="25%" class="text-center"><a href="' . base_url($this->config->item('front_member_order_detail_page_url') . '/' . $record['InvoiceNumber']) . '" class="btn btn-primary rounded-pill"><i class="fas fa-eye"></i></a></td></tr>';
						}
						echo '</tbody>';
						echo '</table>';
						
						echo '</div>';
					}else{
						echo '<div class="alert alert-danger">No Discount Codes Used</div>';
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>