<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="row">
                    <div class="col-6 col-md-6 col-lg-6 mb-4">
                        <a id="pending" href="<?php echo base_url($this->config->item('front_member_order_listing_page_url')) . '?q=pending' ?>" class="dashboard-button p-3 p-lg-5 rounded-xl text-center d-block">
                            <div class="h1 dashboard-count rounded-xl d-table mx-auto px-4 py-3 mb-4"><?php echo isset($invoice_pending_count) ? $invoice_pending_count : 0 ?></div>
                            <div class="h4 d-block mb-0">Pending Orders</div>
                            <span class="h6 text-uppercase"><span class="small">Click to View Listing</span></span>
                        </a>
                    </div>
                    <div class="col-6 col-md-6 col-lg-6 mb-4">
                        <a id="processed" href="<?php echo base_url($this->config->item('front_member_order_listing_page_url')) . '?q=processed' ?>" class="dashboard-button p-3 p-lg-5 rounded-xl text-center d-block">
                            <div class="h1 dashboard-count rounded-xl d-table mx-auto px-4 py-3 mb-4"><?php echo isset($invoice_processed_count) ? $invoice_processed_count : 0 ?></div>
                            <div class="h4 d-block mb-0">Processed Orders</div>
                            <span class="h6 text-uppercase"><span class="small">Click to View Listing</span></span>
                        </a>
                    </div>
                    <div class="col-6 col-md-6 col-lg-6 mb-4">
                        <a id="cancelled" href="<?php echo base_url($this->config->item('front_member_order_listing_page_url')) . '?q=cancel' ?>" class="dashboard-button p-3 p-lg-5 rounded-xl text-center d-block">
                            <div class="h1 dashboard-count rounded-xl d-table mx-auto px-4 py-3 mb-4"><?php echo isset($invoice_cancel_count) ? $invoice_cancel_count : 0 ?></div>
                            <div class="h4 d-block mb-0">Cancelled Orders</div>
                            <span class="h6 text-uppercase"><span class="small">Click to View Listing</span></span>
                        </a>
                    </div>
                    <div class="col-6 col-md-6 col-lg-6 mb-4">
                        <a id="completed" href="<?php echo base_url($this->config->item('front_member_order_listing_page_url')) . '?q=completed' ?>" class="dashboard-button p-3 p-lg-5 rounded-xl text-center d-block">
                            <div class="h1 dashboard-count rounded-xl d-table mx-auto px-4 py-3 mb-4"><?php echo isset($invoice_completed_count) ? $invoice_completed_count : 0 ?></div>
                            <div class="h4 d-block mb-0">Completed Orders</div>
                            <span class="h6 text-uppercase"><span class="small">Click to View Listing</span></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>