<?php
$has_schedule = $this->config->item("has_schedule");
$schedule_emails = $this->config->item("schedule_emails");
?>
<div class="d-flex align-items-center">
    <div class="bg-dark px-4 py-3 rounded-xl text-white mr-3"><i class="far fa-user fa-lg"></i></div>
    <div class="text-uppercase"><?php echo isset($member_detail['FirstName']) ? $member_detail['FirstName'] . '<br />' . $member_detail['LastName'] : '' ?></div>
    <a class="ml-auto py-2 d-block d-lg-none px-2" href="<?php echo base_url($this->config->item('front_logout_page_url')) ?>"><i class="fas fa-sign-out-alt mr-2"></i>Logout</a>
</div>
<div class="list-group list-group-flush list-group-horizontal-md flex-wrap py-3">
    <a href="<?php echo base_url($this->config->item('front_dashboard_page_url')) ?>" class="py-2 col-md-4 col-lg-12 active"><i class="fas fa-shopping-bag mr-2"></i> My Orders</a>
    <a href="<?php echo base_url($this->config->item('front_member_account_page_url')) ?>" class="py-2 col-md-4 col-lg-12"><i class="fas fa-cog mr-2"></i> Account settings</a>
    <a href="<?php echo base_url($this->config->item('front_member_payment_page_url')) ?>" class="py-2 col-md-4 col-lg-12"><i class="far fa-credit-card mr-2"></i> Payment Cards</a>
    <a href="<?php echo base_url($this->config->item('front_member_discount_page_url')) ?>" class="py-2 col-md-4 col-lg-12"><i class="fas fa-percent mr-2"></i> Discount History</a>
    <a href="<?php echo base_url($this->config->item('front_member_loyalty_page_url')) ?>" class="py-2 col-md-4 col-lg-12"><i class="fas fa-gift mr-2"></i> Loyalty History</a>

    <?php if ($has_schedule && in_array($member_detail['EmailAddress'], $schedule_emails)) { ?>
        <a href="<?php echo base_url("schedules") ?>" class="py-2 col-md-4 col-lg-12"><i class="fas fa-clock mr-2"></i> Schedule</a>
    <?php } ?>
</div>
<?php $this->load->view('includes/order_now/' . SERVER . '_box'); ?>
<div class="row no-gutters">
    <div class="col-12 col-md-6 col-lg-12 pr-md-3 pr-lg-0">
        <div class="bg-dark px-3 pt-4 pb-4 rounded-xl my-2 d-flex align-items-center justify-content-between">
            <span class="h5 font-weight-bold text-white text-uppercase pr-3 mb-0">Loyalty Points</span>
            <span class="border border-primary p-3 h4 mb-0 font-weight-bold text-primary rounded-xl"><?php echo isset($member_loyalty_points) ? $member_loyalty_points : '0' ?></span>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-12 pl-md-3 pl-lg-0">
        <div class="bg-dark px-3 pt-4 pb-4 rounded-xl my-2">
            <?php
            $ReferralCodeTMP = isset($member_detail['ReferralCode']) ? $member_detail['ReferralCode'] : '';
            if (isset($ReferralCodeTMP) && $ReferralCodeTMP != '') {
                echo '<span class="h5 font-weight-bold text-white text-uppercase pr-3 mb-0 d-block">Referral Code</span>';
                echo '<span class="h5 mb-0 font-weight-bold text-primary">' . $ReferralCodeTMP . '</span>';
            } else {
                echo '<a href="' . base_url($this->config->item('front_member_account_page_url')) . '" >Generate Your <br> Referral Code</a>';
            }
            ?>
        </div>
    </div>	
</div>
<a href="<?php echo base_url($this->config->item('front_logout_page_url')) ?>" class="py-3 d-none d-lg-block"><i class="fas fa-sign-out-alt mr-2"></i>Logout</a>