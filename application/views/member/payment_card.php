<script>
    var stripe_key = "<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>";
</script>

<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="bg-white p-3 p-lg-5  rounded-xl">
                        <form class="form-horizontal custom-forms" autocomplete="off" id="frm_payment_card" name="frm_payment_card" method="post" action="<?php echo base_url('payment-card') ?>">
                        <div class="card-listing" id="card_listing">
                            <div class="d-md-flex align-items-center justify-content-between pb-3">
                                <h3 class="h4 py-3">My Payment Methods</h3>
                                <a class="btn btn-dark rounded-pill d-block d-md-inline-block" id="add_card" href="#"><i class="fas fa-plus"></i> Add Payment Method</a>
                            </div>

                            <?php
                            if (isset($card_records)) {
                                echo '<div class="mb-3 bg-white rounded-xl">';
                                
                                foreach ($card_records as $record) {
                                    if (strlen($record["Number"]) != 4) {
                                        continue;
                                    }
									echo '<ul class="list-order list-group list-group-horizontal-sm">';
                                    echo '<li class="col-8 col-md-9 list-group-item py-2 border-0 list-' . $record['PKCardID'] . '"><div class="h6 font-weight-bold text-dark">Card Title</div><div class="h5 mb-0">' . $record['Title'] . '</div></li>';
                                    echo '<li class="col-4 col-md-3 list-group-item py-2 border-0 text-right"> <a href="#" data-id="' . $record['PKCardID'] . '" class="btn btn-primary btn-sm rounded-pill px-2 text-white remove-card" id="btn_order_detail"><i class="fas fa-trash-bin"></i> Delete</a></li>';
									echo '</ul>';
                                }
                                
                                echo '</div>';
                            } else {
                                echo '<div class="alert alert-danger"><p>No Card Records</p></div>';
                            }
                            ?>
                        </div>
                    </form>


                    <div class="card-listing hide-area col-sm-offset-1" id="card-form">
                        <h3>Add new card</h3>
                        <div class="cell example example2" style="width:100%;" id="example-2">
                            <form id="checkout-form">
                                <div id="payment_errors"></div>
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <div class="form-group mb-3 mb-md-4">
                                            <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">Card number</label>
                                            <div id="card-number" class="input empty form-control form-control-lg stripe-input"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mb-3 mb-md-4">
                                            <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiry Date</label>
                                            <div id="card-expiry" class="input empty form-control form-control-lg stripe-input"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group mb-3 mb-md-4">
                                            <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">cvc</label>
                                            <div id="card-cvc" class="input empty form-control form-control-lg stripe-input"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <small>*Please note the final itemisation will done by the facility. The total invoice amount may vary.</small>
                                    </div>
                                    <div class="offset-md-6 col-md-6 col-12 text-right pt-3">
                                        <button type="submit" class="btn btn-dark btn-order btn-lg px-4 rounded-pill
                                                next-rgt-confirm-button"  data-tid="elements_examples.form.pay_button">Save</button>
                                        <button type="button" class="btn btn-primary btn-order btn-lg rounded-pill
                                                next-rgt-confirm-button" id="btn_back">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo base_url('assets/js/front/payment.js') ?>"></script>
<script>
    var saveCard = function (payment_method_id, id) {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('payment-card') ?>",
            cache: false,
            data: "c_type=Add&payment_method_id=" + payment_method_id,

            success: function (response) {
                console.log(response);
                var data = response.split("||");
                if (data[0] == "success") {
                    ClearAllCache();
                    window.location = data[3];
                } else {
                    $("#payment_errors").show().html('<div class="alert alert-danger" role="alert">' + data[2] + '</div>');
                }
            },
            error: function (data) {
                // alert("Some went wrong.");
                console.log(data);
                //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
            }
        });

        /*
         $("#payment_errors").html("");
         alert(payment_method_id);
         
         var data = {payment_method_id: payment_method_id, card_id: id}
         var type = "POST";
         var dataType = "json";
         var url = web_url + "booking/savecard";
         var callBack = function (response) {
         //card_id = response.id;
         //confirmOrder();
         };
         var errorCallBack = function (jqXHR, error, errorThrown) {};
         ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
         */
    }
</script>