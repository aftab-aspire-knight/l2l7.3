<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
?>

<div class="py-3 py-md-4 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <?php $this->load->view('member/sidebar'); ?>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="bg-white p-3 p-md-4 p-lg-5 rounded-xl">
                    <form class="form-horizontal custom-form" id="frm_account_setting" name="frm_account_setting" method="post" action="<?php echo base_url('update-account') ?>">
                        <input type="hidden" value="<?php echo $this->config->item("country_phone_code"); ?>" readonly />
                        <div id="msg_box"></div>
                        <h3 class="h4 py-3 border-bottom"><span><?php echo $this->config->item('front_member_account_first_heading_text') ?></span></h3>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First Name <span class="red-color">*</span></label>
                                    <input type="text" class="form-control form-control-lg" placeholder="First Name" id="first_name" name="first_name" value="<?php echo isset($member_detail['FirstName']) ? $member_detail['FirstName'] : '' ?>" />

                                </div>
                                <div class="form-group">
                                    <label>Email Address <span class="red-color">*</span></label>
                                    <input type="text" class="form-control form-control-lg" placeholder="Email Address" id="email_address" name="email_address" value="<?php echo isset($member_detail['EmailAddress']) ? $member_detail['EmailAddress'] : '' ?>" />
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name <span class="red-color">*</span></label>
                                    <input type="text" class="form-control form-control-lg" placeholder="Last Name" id="last_name" name="last_name" value="<?php echo isset($member_detail['LastName']) ? $member_detail['LastName'] : '' ?>" />
                                </div>
                                <div class="form-group">
                                    <div class="form-group mb-2 mb-md-3 ">
                                        <label>Phone</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend country-code"> 
                                                <span class="input-text-country">+<?php echo $this->config->item("country_phone_code") ?></span>
                                                <select class="custom-select custom-select-lg" id="countryCode" name="countryCode" >

                                                    <?php
                                                    foreach ($codes as $key => $country) {
                                                        ?>
                                                        <option value="<?php echo $key ?>" <?php if ($key == $this->config->item("country_phone_code")) { ?> selected <?php } ?>><?php echo $country ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <input type="tel" class="form-control form-control-lg only-number " id="phone_number" name="phone_number" value="<?php echo isset($member_detail['Phone']) ? $member_detail['Phone'] : '' ?>" />
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Referral Code</label>
                                    <input type="text" class="form-control form-control-lg" maxlength="50" disabled placeholder="Referral Code" id="referral_code" name="referral_code" value="<?php echo isset($member_detail['ReferralCode']) ? $member_detail['ReferralCode'] : '' ?>" />								

                                </div>
                            </div>


                            <?php /* ?>
                              <h3 class="h4 py-3 border-bottom mt-3"><span><?php echo $this->config->item('front_member_account_second_heading_text') ?></span></h3>
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                              <label>Post Code <span class="red-color">*</span></label>
                              <div id="post_area">
                              <div class="input-group mb-3">
                              <input type="text" class="form-control form-control-lg valid" placeholder="Post Code" aria-label="Enter Postcode" id="postal_code" name="postal_code" value="<?php echo isset($member_detail['PostalCode']) ? $member_detail['PostalCode'] : '' ?>" aria-invalid="false" aria-describedby="btn_post_code">
                              <div class="input-group-append">
                              <button class="btn btn-dark find-address" type="button" id="btn_post_code">Find <i class="fas fa-location-arrow"></i></button>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                              <div class="col-md-6">
                              <div class="form-group"  >
                              <label>Address <span class="red-color">*</span></label>
                              <input type="text" placeholder="Please enter address" class="form-control form-control-lg address-building" name="building_name" id="building_name" value="<?php echo isset($member_detail['BuildingName']) ? $member_detail['BuildingName'] : '' ?>" />

                              </div>
                              <div class="form-group">
                              <label>Address Line 2</label>
                              <input type="text" placeholder="Please specify any extra address details" class="form-control form-control-lg address-street" name="street_name" id="street_name" value="<?php echo isset($member_detail['StreetName']) ? $member_detail['StreetName'] : '' ?>" />

                              </div>

                              <input type="hidden" class="form-control form-control-lg address-town" placeholder="Town" id="town" name="town" value="<?php echo isset($member_detail['Town']) ? $member_detail['Town'] : '' ?>" />
                              </div>
                              </div><?php */ ?>
                            <div class="col-md-12">
                                <h3 class="h4 py-3 border-bottom mt-3"><span><?php echo $this->config->item('front_member_account_third_heading_text') ?></span></h3>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <div class="input-group">
                                            <input placeholder="New Password" type="password" id="u_password" name="u_password" class="form-control form-control-lg" aria-label="password" />
                                            <div class="input-group-append" id="btn_password_show">
                                                <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Confirm New Password</label>
                                        <div class="input-group">
                                            <input placeholder="Re-enter your password" type="password" id="confirm_password" name="confirm_password" class="form-control form-control-lg" aria-label="password" />
                                            <div class="input-group-append" id="btn_password_show">
                                                <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg d-table mx-auto rounded-pill px-5 text-uppercase" id="btn_profile"><span class="px-5">Update</span></button>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>