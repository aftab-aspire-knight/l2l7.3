<section class="dashboard">
    <?php $this->load->view('member/sidebar');?>
    <div class="light-gray">
        <div class="container">
            <?php
                if(isset($preference_records)){
                    echo '<form class="form-horizontal custom-forms" id="frm_wash_preference" name="frm_wash_preference" method="post" action="' . base_url('update-preferences'). '">';
                    echo '<div id="msg_box"></div>';
                    echo '<h3 class="form-headings"><span>' . $this->config->item('front_member_laundry_first_heading_text') . '</span></h3>';
                        $left_area_html = "";
                        $right_area_html = "";
                        $down_area_html = "";
                        $counter = 0;
                        foreach($preference_records as $record){
                            $current_area = "";
                            $current_area .= '<div class="form-group">';
                            $current_area .= '<label class="col-sm-4 control-label">' . $record['Title'] . ' <span class="red-color">*</span></label>';
                            $current_area .= '<div class="col-sm-8">';
                            $current_area .= '<select class="form-control" name="preferences_list[]">';
                            foreach($record['child_records'] as $rec){
                                $selected_preference = "";
                                if(in_array($rec['PKPreferenceID'], $member_preference_records)){
                                    $selected_preference = " selected='selected'";
                                }
                                $current_area .= '<option value="' . $rec['PKPreferenceID'] . '"' . $selected_preference . '>' . $rec['Title'] . '</option>'; 
                            }
                            $current_area .= '</select>';
                            $current_area .= '</div>';
                            $current_area .= '</div>';
                            if($record['Title'] == "Starch on Shirts"){
                                $down_area_html .= $current_area;
                            }else{
                                if($counter == 0){
                                    $left_area_html .= $current_area;
                                    $counter = 1;
                                }else{
                                    $right_area_html .= $current_area;
                                    $counter = 0;
                                }
                            }
                        }
                        echo '<div class="row">';
                            echo '<div class="col-md-6">';
                            echo $left_area_html;
                            echo '</div>';
                            echo '<div class="col-md-6">';
                            echo $right_area_html;
                            echo '</div>';
                        echo '</div>';
                        echo '<h3 class="form-headings"><span>' . $this->config->item('front_member_laundry_second_heading_text') . '</span></h3>';
                        echo '<div class="row">';    
                            echo '<div class="col-md-6">';
                            echo $down_area_html;
                            echo '</div>';
                            echo '<div class="col-md-6">';
                            echo '<div class="form-group">';
                            echo '<label class="col-sm-4 control-label">Account Notes</label>';
                            echo '<div class="col-sm-8 icon-field">';
                            echo '<textarea class="form-control" id="account_notes" name="account_notes" rows="5">' . (isset($member_detail['AccountNotes'])?$member_detail['AccountNotes']:'') . '</textarea>';
                            echo '<i class="l2l-account-notes"></i>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        echo '</div>';
                    ?>
            <div class="row">
            <div class="col-md-12 margin-bottom-10">
                <div class="col-md-offset-10">
                    <button type="submit" class="btn btn-primary btn-lg btn-block text-uppercase" id="btn_setting">Update</button>
                </div>
            </div>
                </div>
            <?php
                    echo '</form>';
                }else{
                    echo '<div class="row">';
                    echo '<div class="alert alert-danger"><p>No Wash Preference Records</p></div>';
                    echo '</div>';
                }
            ?>
        </div>
    </div>
</section>