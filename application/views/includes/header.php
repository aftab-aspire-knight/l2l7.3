<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo isset($page_data['MetaTitle']) ? $page_data['MetaTitle'] : $header_title; ?></title>
        <meta name="google-site-verification" content="sMYxkh2_Z0VbfRvTZcB9yngAmMQaVmMR6nmeQZuhZeI" />
        <?php if (isset($page_data['MetaDescription'])) { ?>
            <meta name="description" content="<?php echo html_entity_decode($page_data['MetaDescription']); ?>">
        <?php }if ($current_url == "home") { ?>
            <meta name="description" content="<?php echo html_entity_decode('Order by App &#x2713; 25% Off First Order &#x2713; 24-Hour Turnaround &#x2713;  FREE Pickup & Delivery &#x2713; Affordable Prices &#x2713; Quality Cleaning &#x2713; 8.5 Trust Pilot Score &#x2713;'); ?>">
        <?php }if (isset($page_data['MetaKeyword'])) { ?>
            <meta name="keywords" content="<?php echo $page_data['MetaKeyword']; ?>">
            <?php
        }
        if (isset($page_data['NoIndexFollowTag'])) {
            if (($page_data['NoIndexFollowTag'] == "Yes")) {
                if ($current_url == "login" || $current_url == "register" || $current_url == "login" || $current_url == "offers-dry-cleaning" || $current_url == "offers-ironing" || $current_url == "offers-laundry" || $current_url == "offers-shoe-repair" || $current_url == "offers-shirt" || $current_url == "offers-alteration") {
                    ?>
                    <meta name="robots" content="noindex,follow" />
                <?php } else { ?>
                    <meta name="robots" content="noindex,nofollow" />
                    <?php
                }
            }
        }
        ?>
        <meta name="google-signin-scope" content="profile email">
        <meta name="google-signin-client_id" content="<?php echo $this->config->item('google_client_id') ?>">

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/front/l2l-fav/favicon.ico') ?>"/>
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-57x57.png') ?>"/>
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-72x72.png') ?>"/>
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-114x114.png') ?>"/>
        <?php
        if ($current_url == "home") {
            echo $this->carabiner->display('Front_Home_Template_Css');
        } else if ($current_url == $this->config->item('front_address_selection_page_url') || $current_url == $this->config->item('front_select_item_page_url') || $current_url == $this->config->item('front_check_out_page_url')) {
            echo $this->carabiner->display('Front_Dashboard_Template_Css');
        } else if ($current_url == "live-tracking") {
            echo $this->carabiner->display('Front_Tracking_Page_Template_Css');
        } else if ($current_url == "booking") {
            echo $this->carabiner->display('Front_Booking_Template_Css');
        } else if ($current_url == "pricing") {
            echo $this->carabiner->display('Front_Prices_Template_Css');
        } else if ($current_url == "orders") {
            echo $this->carabiner->display('Front_Order_Template_Css');
        } else if ($current_url == "thank-you") {
            echo $this->carabiner->display('Front_Thankyou_Template_Css');
        } else if ($current_url == "register" || $current_url == "login") {
            echo $this->carabiner->display('Front_Register_Template_Css');
        } else if ($current_url == "schedule") {
            echo $this->carabiner->display('Front_Schedule_Template_Css');
        } else {
            echo $this->carabiner->display('Front_Page_Template_Css');
        }
        ?>
        <!-- Anti-flicker snippet (recommended)  -->
        <style>.async-hide { opacity: 0 !important} </style>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        include 'head.php';
        ?>
    </head>
    <body id="body_area" <?php if (isset($is_order_page)) { ?> class="order-body" <?php } ?>>
        <?php
        include 'body/' . SERVER . '.php';
        ?>
        <input type="hidden" id="minimum_order_amount" name="minimum_order_amount" value="<?php echo isset($minimum_order_amount) ? $minimum_order_amount : '15' ?>"/>
        <input type="hidden" id="minimum_order_amount_later" name="minimum_order_amount_later" value="<?php echo isset($minimum_order_amount_later) ? $minimum_order_amount_later : '10' ?>"/>
        <input type="hidden" id="website_currency" name="website_currency" value="<?php echo $this->config->item('currency_sign') ?>"/>
        <input type="hidden" id="server_date" name="server_date" value="<?php echo date('d-m-Y') ?>"/>
        <input type="hidden" id="server_date_time" name="server_date_time" value="<?php echo date('d-m-Y H:i:s') ?>"/>
        <input type="hidden" id="website_telephone_number" name="website_telephone_number" value="<?php echo $website_telephone_no ?>"/>
        <?php
        if (!isset($is_order_page)) {
            if (isset($header_menus_records) && sizeof($header_menus_records) > 0) {
                $class_name = "header page-header bg-light";
                if (isset($is_home_page)) {
                    $class_name = "header";
                }
                if ($current_url == "pricing" || $current_url == "privacy-policy" || $current_url == "t-c") {
                    $class_name = "header page-header";
                }
                ?>
                <header class="<?php echo $class_name ?>">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-md-12 col-12" id="logo_area">
                                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon text-white"></span>
                                </button>
                                <a class="logo" href="<?php echo site_url() ?>" rel="home"><img width="100" height="100" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo-black.png' ?>" alt="Love2Laundry.com" /></a>
                                <a class="logo-fixed" href="<?php echo site_url() ?>" rel="home"><img width="140" height="69" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo.png' ?>" alt="Love2Laundry.com"></a>
                            </div>
                            <?php if (isset($header_menus_records) && sizeof($header_menus_records) > 0) { ?>
                                <div class="col-lg-10 col-md-12 col-12 text-right" id="main_menu">
                                    <nav class="navigation navbar navbar-expand-lg" id="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                                        <div class="collapse navbar-collapse" id="navbarResponsive">
                                            <ul class="main-menu">
                                                <?php
                                                foreach ($header_menus_records as $menu):
                                                    $current_class = '';
                                                    if ($current_url == $menu['PageURL']) {
                                                        $current_class = ' class="active"';
                                                    }
                                                    echo '<li' . $current_class . '><a href="' . CreateWebURL($menu['PageURL']) . '">' . $menu['PageName'] . '</a></li>';
                                                endforeach;
                                                if (isset($is_member_login)) {
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo base_url($this->config->item('front_dashboard_page_url')) ?>"><?php echo $this->config->item('front_header_dashboard_heading_text') ?></a>
                                                    </li>
                                                    <li class="sign-in">
                                                        <a class="logout" href="javascript:void(0)"><?php echo $this->config->item('front_header_log_out_heading_text') ?></a>
                                                    </li>
                                                <?php } else { ?>
                                                    <li class="sign-in">
                                                        <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>"><?php echo $this->config->item('front_header_sign_in_heading_text') ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </header>
                <?php
            }
        }
        ?>
    