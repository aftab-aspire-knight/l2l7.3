
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo isset($page_data['MetaTitle']) ? $page_data['MetaTitle'] : $header_title; ?></title>
        <?php if (isset($page_data['MetaDescription'])) { ?>
            <meta name="description" content="<?php echo html_entity_decode($page_data['MetaDescription']); ?>">
        <?php }if (isset($page_data['MetaKeyword'])) { ?>
            <meta name="keywords" content="<?php echo $page_data['MetaKeyword']; ?>">
            <?php
        }
        if (isset($page_data['NoIndexFollowTag'])) {
            if (($page_data['NoIndexFollowTag'] == "Yes")) {
                if ($current_url == "offers-dry-cleaning" || $current_url == "offers-ironing" || $current_url == "offers-laundry" || $current_url == "offers-shoe-repair" || $current_url == "offers-shirt" || $current_url == "offers-alteration") {
                    ?>
                    <meta name="robots" content="noindex,follow" />
                <?php } else { ?>
                    <meta name="robots" content="noindex,nofollow" />
                    <?php
                }
            }
        }
        echo '<link rel="canonical" href="' . base_url($this->uri->uri_string()) . '" />';
        ?>

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/front/l2l-fav/favicon.ico') ?>"/>
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-57x57.png') ?>"/>
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-72x72.png') ?>"/>
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-114x114.png') ?>"/>


        <script>
            var site_url = "<?php echo $this->config->item('base_url') ?>";
            var dir = "l2l";
            var iso_code = "<?php echo $this->config->item('iso_code') ?>";
            var currency = "<?php echo $this->config->item('currency') ?>";
            var currency_symbol = "<?php echo $this->config->item('currency_symbol') ?>";
            var currency_code = "<?php echo $this->config->item('currency_code') ?>";
            var currency_sign = "<?php echo $this->config->item('currency_sign') ?>";
            var intl_tel = "<?php echo $this->config->item('intl_tel') ?>";
            var api_key = "<?php echo $this->config->item('loqate_key') ?>";
            var loq_api_url = "<?php echo $this->config->item('loqate_api_url') ?>";
            var iso_country_code = "<?php echo $this->config->item('iso_country_code') ?>";
            var locality = "<?php echo $this->config->item('locality') ?>";
            var post_code_length = "<?php echo $this->config->item('post_code_length') ?>";
        </script>
        <?php
        if (isset($is_ppc_page)) {
            echo $this->carabiner->display('Offers_Template_Css');
        } else {
            echo $this->carabiner->display('Front_Page_Template_Css');
        }
        ?>
        <?php
        if (isset($google_analytics_id)) {
            ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', '<?php echo $google_analytics_id ?>', 'auto');
                ga('send', 'pageview');


    <?php
    if ($current_url == $this->config->item('front_thank_you_page_url')) {
        if (!empty($google_e_commerce_invoice_record)) {
            ?>
                        ga('require', 'ecommerce');
                        ga('ecommerce:addTransaction', {
                            'id': '<?php echo $google_e_commerce_invoice_record['InvoiceNumber'] ?>', // Transaction ID. Required.
                            'affiliation': 'Love2Laundry', // Affiliation or store name.
                            'revenue': '<?php echo $google_e_commerce_invoice_record['GrandTotal'] ?>'               // Grand Total.
                        });
            <?php
            if (!empty($google_e_commerce_invoice_record['service_records'])) {
                foreach ($google_e_commerce_invoice_record['service_records'] as $record) {
                    ?>
                                ga('ecommerce:addItem', {
                                    'id': '<?php echo $record['FKServiceID'] ?>', // Transaction ID. Required.
                                    'name': '<?php echo $record['Title'] ?>', // Product name. Required.
                                    'sku': '<?php echo $record['FKServiceID'] ?>', // SKU/code.
                                    'price': '<?php echo $record['Price'] ?>', // Unit price.
                                    'quantity': '<?php echo $record['Quantity'] ?>'                   // Quantity.
                                });
                    <?php
                }
            }
            echo "ga('ecommerce:send');";
            echo "ga('ecommerce:clear');";
        }
    }
    ?>
            </script>
        <?php } ?>
        <!-- Anti-flicker snippet (recommended)  -->
        <style>.async-hide { opacity: 0 !important} </style>
        <script>(function (a, s, y, n, c, h, i, d, e) {
                s.className += ' ' + y;
                h.start = 1 * new Date;
                h.end = i = function () {
                    s.className = s.className.replace(RegExp(' ?' + y), '')
                };
                (a[n] = a[n] || []).hide = h;
                setTimeout(function () {
                    i();
                    h.end = null
                }, c);
                h.timeout = c;
            })(window, document.documentElement, 'async-hide', 'dataLayer', 4000,
                    {'GTM-M3DTNTF': true});</script>
        <?php /* ?><!-- Facebook Conversion Code for LOVE2LAUNDRY -->
          <script>(function() {
          var _fbq = window._fbq || (window._fbq = []);
          if (!_fbq.loaded) {
          var fbds = document.createElement('script');
          fbds.async = true;
          fbds.src = '//connect.facebook.net/en_US/fbds.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(fbds, s);
          _fbq.loaded = true;
          }
          })();
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6018881968528',
          {'value':'0.00','currency':'GBP'}]);
          </script>
          <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6018881968528&amp;cd[value]=0.00&amp;cd[currency]=GBP&amp;noscript=1"/>
          </noscript><?php */ ?>
        <!-- Google Analytics Code for LOVE2LAUNDRY -->
        <script type="text/javascript">
            (function (a, e, c, f, g, b, d) {
                var
                        h = {ak: "965891363", cl: "pkDyCLH4q1wQo6rJzAM"};
                a[c] = a[c] ||
                        function () {
                            (a[c].q = a[c].q || []).push(arguments)
                        };
                a[f] ||
                        (a[f] = h.ak);
                b = e.createElement(g);
                b.async = 1;
                b.src = "//www.gstatic.com/wcm/loader.js";
                d = e.getElementsByTagName(g)[0];
                d.parentNode.insertBefore(b, d);
                a._googWcmGet = function (b, d, e) {
                    a[c](2, b, h, d, null, new
                            Date, e)
                }
            })(window, document, "_googWcmImpl", "_googWcmAk", "script");
        </script>
        <script>(function (w, d, t, r, u) {
                var f, n, i;
                w[u] = w[u] || [], f = function () {
                    var o = {ti: "5078868"};
                    o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
                }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
                    var s = this.readyState;
                    s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
                }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
            })(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5078868&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- TrustBox script --> <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js" async></script> <!-- End Trustbox script -->
    <!-- Facebook Pixel Code -->
    <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '397101313972207'); // Insert your pixel ID here.
            fbq('track', 'PageView');
<?php
if ($current_url == $this->config->item('front_address_selection_page_url')) {
    echo "fbq('track', 'AddPaymentInfo');";
} else if ($current_url == $this->config->item('front_check_out_page_url')) {
    echo "fbq('track', 'InitiateCheckout');";
} else if ($current_url == $this->config->item('front_thank_you_page_url')) {
    if (isset($invoice_fb_total)) {
        echo "fbq('track', 'CompleteRegistration', {value:" . $invoice_fb_total . ",currency: 'GBP'});";
    }
}
?>
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=397101313972207&ev=PageView&noscript=1"
                   /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
            window.addEventListener("load", function () {
                window.cookieconsent.initialise({
                    "palette": {
                        "popup": {
                            "background": "#000"
                        },
                        "button": {
                            "background": "#40bcdc",
                            "text": "#ffffff"
                        }
                    },
                    "position": "bottom-left",
                    "content": {
                        "message": "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies.",
                        "link": "Find out more here",
                        "href": "https://www.love2laundry.com/t-c.html#cookie"
                    }
                })
            });
    </script>

    <!--EMBED THE FRESHCHAT WIDGET CODE 5-16-2018 -->
    <script src="https://wchat.freshchat.com/js/widget.js"></script>
    <!--EMBED THE FRESHCHAT WIDGET CODE 5-16-2018 ends -->


    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-53FH3CM');</script>
    <!-- End Google Tag Manager -->

    <script>
        window['_fs_debug'] = false;
        window['_fs_host'] = 'fullstory.com';
        window['_fs_org'] = 'J2BKQ';
        window['_fs_namespace'] = 'FS';
        (function (m, n, e, t, l, o, g, y) {
            if (e in m) {
                if (m.console && m.console.log) {
                    m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');
                }
                return;
            }
            g = m[e] = function (a, b, s) {
                g.q ? g.q.push([a, b, s]) : g._api(a, b, s);
            };
            g.q = [];
            o = n.createElement(t);
            o.async = 1;
            o.src = 'https://' + _fs_host + '/s/fs.js';
            y = n.getElementsByTagName(t)[0];
            y.parentNode.insertBefore(o, y);
            g.identify = function (i, v, s) {
                g(l, {uid: i}, s);
                if (v)
                    g(l, v, s)
            };
            g.setUserVars = function (v, s) {
                g(l, v, s)
            };
            g.event = function (i, v, s) {
                g('event', {n: i, p: v}, s)
            };
            g.shutdown = function () {
                g("rec", !1)
            };
            g.restart = function () {
                g("rec", !0)
            };
            g.consent = function (a) {
                g("consent", !arguments.length || a)
            };
            g.identifyAccount = function (i, v) {
                o = 'account';
                v = v || {};
                v.acctId = i;
                g(o, v)
            };
            g.clearUserCookie = function () {};
        })(window, document, window['_fs_namespace'], 'script', 'user');
    </script>

    <!-- Hotjar Tracking Code for www.love2laundry.com -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 1876689, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>
<body id="body_area">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53FH3CM"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <input type="hidden" id="franchise_id" name="franchise_id" value="<?php echo isset($franchise_id) ? $franchise_id : '' ?>"/>

    <input type="hidden" id="invoice_id" name="invoice_id" value="<?php echo isset($PKInvoiceID) ? $PKInvoiceID : '' ?>"/>
    <input type="hidden" id="editdate" name="editdate" value="<?php echo isset($editdate) ? $editdate : true ?>"/>
    <input type="hidden" id="pdate" name="pdate" value="<?php echo isset($PickupDate) ? $PickupDate : '' ?>"/>
    <input type="hidden" id="ptime" name="ptime" value="<?php echo isset($PickupTime) ? $PickupTime : '' ?>"/>
    <input type="hidden" id="ddate" name="ddate" value="<?php echo isset($DeliveryDate) ? $DeliveryDate : '' ?>"/>
    <input type="hidden" id="dtime" name="dtime" value="<?php echo isset($DeliveryTime) ? $DeliveryTime : '' ?>"/>

    <input type="hidden" id="minimum_order_amount" name="minimum_order_amount" value="<?php echo isset($minimum_order_amount) ? $minimum_order_amount : '15' ?>"/>
    <input type="hidden" id="minimum_order_amount_later" name="minimum_order_amount_later" value="<?php echo isset($minimum_order_amount_later) ? $minimum_order_amount_later : '10' ?>"/>
    <input type="hidden" id="website_currency" name="website_currency" value="<?php echo $this->config->item('currency_sign') ?>"/>
    <input type="hidden" id="server_date" name="server_date" value="<?php echo date('d-m-Y') ?>"/>
    <input type="hidden" id="server_date_time" name="server_date_time" value="<?php echo date('d-m-Y H:i:s') ?>"/>
    <input type="hidden" id="website_telephone_number" name="website_telephone_number" value="<?php echo $website_telephone_no ?>"/>
    <header class="header ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-12 col-12">
                    <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon text-white"></span>
                    </button>
                    <a class="logo" href="<?php echo site_url() ?>" rel="home"><img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo-black.png' ?>" alt="Love2Laundry.com" /></a>
                    <a class="logo-fixed" href="<?php echo site_url() ?>" rel="home"><img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo.png' ?>" alt="Love2Laundry.com"></a>
                </div>
                <?php if (isset($header_menus_records) && sizeof($header_menus_records) > 0) { ?>
                    <div class="col-lg-10 col-md-12 col-12 text-right" id="main_menu">
                        <nav class="navigation navbar navbar-expand-lg" id="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="main-menu">
                                    <?php
                                    foreach ($header_menus_records as $menu):
                                        $current_class = '';
                                        if ($current_url == $menu['PageURL']) {
                                            $current_class = ' class="active"';
                                        }
                                        echo '<li' . $current_class . '><a href="' . CreateWebURL($menu['PageURL']) . '">' . $menu['PageName'] . '</a></li>';
                                    endforeach;
                                    if (isset($is_member_login)) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url($this->config->item('front_dashboard_page_url')) ?>"><?php echo $this->config->item('front_header_dashboard_heading_text') ?></a>
                                        </li>
                                        <li class="sign-in">
                                            <a class="logout" href="javascript:void(0)"><?php echo $this->config->item('front_header_log_out_heading_text') ?></a>
                                        </li>
                                    <?php } else { ?>
                                        <li class="sign-in">
                                            <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>"><?php echo $this->config->item('front_header_sign_in_heading_text') ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                <?php } ?>
            </div>
        </div>
    </header>