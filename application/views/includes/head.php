<?php
include("head/" . SERVER . ".php")
?>

<?php
//$member_id = $this->GetCurrentMemberID();
$member_detail = $this->session->userdata("member_love_2_laundry_detail");

if ($member_detail != null) {
?>
    <script>
        userId = <?php echo $member_detail["PKMemberID"] ?>;
        hj('identify', userId, {
            name: '<?php echo $member_detail["FirstName"] ?> <?php echo $member_detail["LastName"] ?>',
            email: '<?php echo $member_detail["EmailAddress"] ?>'
        });
    </script>
<?php
}
?>
<link rel='dns-prefetch' href='//www.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//fonts.gstatic.com' />
<link rel='dns-prefetch' href='//ajax.googleapis.com' />
<link rel='dns-prefetch' href='//apis.google.com' />
<link rel='dns-prefetch' href='//google-analytics.com' />
<link rel='dns-prefetch' href='//www.google-analytics.com' />
<link rel='dns-prefetch' href='//ssl.google-analytics.com' />
<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
<link rel='dns-prefetch' href='//ruler.nyltx.com' />
<link rel='dns-prefetch' href='//connect.facebook.net' />
<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
<?php if ($current_url == "register" || $current_url == "login") { ?>
    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo $this->config->item('recaptcha_copy_key'); ?>" defer="defer"></script>
<?php } ?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId: facebook_app_id,
            cookie: true,
            xfbml: true,
            version: 'v11.0'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>

    var iso_country_code = "<?php echo $this->config->item('iso_country_code');?>";
    var north = <?php echo $this->config->item('north');?>;
    var west = <?php echo $this->config->item('west');?>;
    var south = <?php echo $this->config->item('south');?>;
    var east = <?php echo $this->config->item('east');?>;
    var google_map_api_key = "<?php echo $this->config->item('google_map_api_key');?>";
    var google_client_id = "<?php echo $this->config->item('google_client_id');?>";
  //  var areaConfig = <?php //echo json_encode($this->config->item('postal_code_type'));?>;
    
</script>