<div class="bg-dark text-white px-3 pt-4 pb-4 rounded-xl my-2 text-center d-none d-lg-block">
	<h3 class="h5 font-weight-bold mb-3">PLACE ORDER NOW</h3>
	<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
		<div class="form-group mb-2">
			<input type="text" class="form-control form-control-lg rounded-pill text-center" name="post_code" id="post_code" placeholder="enter your eircode"  />
		</div>
		<div class="form-group">
			<button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-block btn-primary rounded-pill">Check Prices</button>
		</div>
	</form>
</div>