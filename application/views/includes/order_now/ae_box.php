<div class="bg-dark text-white px-3 pt-4 pb-4 rounded-xl my-2 text-center d-none d-lg-block">
    <h3 class="h5 font-weight-bold mb-3">PLACE ORDER NOW</h3>
    <a href="<?php echo base_url("booking") ?>" class="btn btn-lg btn-block btn-primary rounded-pill">Order Now</a>
</div>