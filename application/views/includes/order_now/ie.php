<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability') ?>">
    <div class="place-order-field">
        <input type="text" name="post_code" id="post_code" placeholder="enter your eircode" class="form-control">
        <br clear="all"/>
        <button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Order Now</button>
    </div>
</form>