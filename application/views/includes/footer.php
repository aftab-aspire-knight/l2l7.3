<?php
include 'footer/' . SERVER . '.php';
?>
<style>

</style>
<div id="myModal" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary border-light">
                <h5 class="modal-title" id="modal_title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modal_message"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$postal_code_type = $this->config->item('postal_code_type');
if ($postal_code_type["title"] == "Area") {
?>
    <div id="mapModal" class="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary border-light">
                    <h5 class="modal-title" id="modal_title">Search Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modal_message_map" style="display: none;">
                        <div class="alert alert-danger d" role="alert">
                            Oops, we are not there yet, will be there soon.
                        </div>
                    </div>
                    <div id="pac-container">
                        <input id="pac-input" class="form-control form-control-lg rounded-pill text-center" type="text" placeholder="Search Location" />
                    </div>
                    <div class="map-container" style="display: none ;">
                        <div id="google_map"></div>
                        <div class="maps"><img class="map-pin" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'map-pin.svg' ?>" /></div>

                    </div>

                    <div id="infowindow-content">
                        <span id="place-name" class="title"></span><br />
                        <span id="place-address"></span>
                    </div>

                </div>
                <div class="modal-footer confirm-location" style="display: none ;">
                    <button type="button" class="btn btn-secondary btn-confirm-location">Confirm Location</button>
                </div>

            </div>
        </div>
    </div>



<?php
} ?>


<?php // } 
$postal_code_type = $this->config->item('postal_code_type');
$google_map_api_key = $this->config->item('google_map_api_key');
?>
<?php

if ($current_url == "home") {
    echo $this->carabiner->display('Front_Home_Template_JQuery');
} else if ($current_url == $this->config->item('front_register_page_url') || $current_url == $this->config->item('front_login_page_url')) {
    echo $this->carabiner->display('Front_Register_Template_JQuery');
} else if ($current_url == $this->config->item('front_member_account_page_url') || $current_url == $this->config->item('front_contact_page_url')) {
    echo $this->carabiner->display('Front_Phone_Template_JQuery');
} else if ($current_url == $this->config->item('front_member_payment_page_url')) {
?>
    <!-- <script type="text/javascript" src="https://js.stripe.com/v3/"></script> -->
<?php
    echo $this->carabiner->display('Front_Payment_Card_Template_JQuery');
} else if ($current_url == $this->config->item('front_select_item_page_url')) {
    echo $this->carabiner->display('Front_Select_Item_Template_JQuery');
} else if ($current_url == $this->config->item('front_address_selection_page_url')) {
    echo $this->carabiner->display('Front_Address_Template_JQuery');
} else if ($current_url == $this->config->item('front_check_out_page_url')) {
    echo $this->carabiner->display('Front_Checkout_Template_JQuery');
} else if ($current_url == "live-tracking") {
    echo $this->carabiner->display('Front_Tracking_Page_Template_JQuery');
} else if ($current_url == "booking") {
    echo $this->carabiner->display('Front_Booking_Template_JQuery');
} else if ($current_url == "pricing") {
    echo $this->carabiner->display('Front_Prices_Template_JQuery');
} else if ($current_url == "orders") {
    echo $this->carabiner->display('Front_Order_Template_JQuery');
} else if ($current_url == "schedule" || $current_url == "schedules") {

    echo $this->carabiner->display('Front_Schedule_Template_JQuery');
} else if ($current_url == "thank-you") {
    echo $this->carabiner->display('Front_Thankyou_Template_JQuery');
} else {
    echo $this->carabiner->display('Front_Page_Template_JQuery');
}
if (isset($new_register_pop_show)) {
?>
    <script type="text/javascript">
        jQuery('#new_register_pop').magnificPopup({
            type: 'inline',
            mainClass: 'mfp-with-zoom',
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out'
            }
        });
        jQuery('#new_register_pop').trigger("click");
    </script>
<?php
}
?>

<?php /* ?>
  <div class="invest-model-container bg-light rounded-xl" style="display: none">
  <div class="row no-gutters align-items-center">
  <div class="col-12 col-md-12 bg-primary text-center p-3 invest-model-content ">
  <button  type="button" class="close btn-lg btn-invest-close" aria-label="Close">
  <span aria-hidden="true">×</span>
  </button>
  <img width="150" height="74" src="<?php echo $this->config->item('front_asset_image_folder_url').'logo.png'; ?>" alt="" class="img-fluid">
  <h2 class="h3 ff-gothamblack text-uppercase">we are <br>crowdfunding</h2>
  <p class="h5">Join us in our Journey to success</p>
  <a rel="noopener noreferrer" target="_blank" href="https://www.seedrs.com/love2laundry" class="btn btn-primary btn-lg rounded-pill btn-dark text-white px-5 mb-3">Invest now</a>
  <h6 class="font-weight-bold">Capital at Risk</h6>
  <p class="fs12"> This has been approved as a financial promotion by Seedrs Limited, which is authorized and regulated by the Financial Conduct Authority</p>
  </div>
  </div>
  </div>
  <?php */ ?>

<?php //if ($current_url == "booking" || $current_url == "register" || $current_url == "login") { 
?>
<script src="<?php echo base_url('assets/js/front/facebook_login.js') ?>" async defer></script>
<script src="<?php echo base_url('assets/js/front/google_login.js') ?>" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
<!--
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js
?libraries=places,geometry&sensor=false&input=amoeba&types=establishment&location=37.76999%2C-122.44696&radius=500&key=AIzaSyC2hJDXNCLDmHh87EdaKIDcWecYNZjiUCY"></script>
-->


<?php // } 
$postal_code_type = $this->config->item('postal_code_type');
$google_map_api_key = $this->config->item('google_map_api_key');
if ($postal_code_type["title"] == "Area") {
?>
    <script src="<?php echo base_url('assets/js/front/area.js?'.rand()) ?>" async defer></script>
<?php
} else {
?>
    <script src="<?php echo base_url('assets/js/front/postcode.js?'.rand()) ?>" async defer></script>
<?php
}
?>
<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_map_api_key; ?>&language=en&libraries=places"></script>


<script>
    $(document).ready(function($) {
        $(".btn-chat").on("click", function() {
            $(this).next().fadeToggle();
            $(this).find("i").toggleClass("fa-times");
            return false;
        });
    });
</script>
</body>

</html>