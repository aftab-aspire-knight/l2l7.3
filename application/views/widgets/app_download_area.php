<?php
    $form_name = "frm_app_link";
    $field_name = "app_mobile_no";
    $button_name = "btn_app_request";
if(isset($app_inner_area)){
    $form_name = "frm_app_link_inner";
    $field_name = "app_mobile_no_inner";
    $button_name = "btn_app_request_inner";
    
?>
<h2 class="py-2">Download our free mobile app<?php //echo $this->config->item('front_app_widget_heading_text')?></h2>
<p class="mb-4">Free same-day collection <span class="px-0 px-lg-3">&bull;</span> Delivery in 24h <br /><span class="px-0 px-lg-3">&bull;</span> Standard wash for just <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('standard_wash'); ?></p>
<form id="<?php echo $form_name?>" name="<?php echo $form_name?>" method="post" action="<?php echo base_url('app-request')?>">
	<?php if(isset($is_ppc_page)){ ?>
	<div class="adword-form-group">
		<input type="text" name="<?php echo $field_name?>" id="<?php echo $field_name?>" placeholder="Enter Your Number" class="adword-field" />
		<button type="submit" id="<?php echo $button_name?>" name="<?php echo $button_name?>" class="adword-ordernow">Get APP Link</button>
	</div>
	<?php }else{ ?>
	<div class="row">
	    <div class="col-lg-6">
        	<a rel="noopener noreferrer" target="_blank" class="ios d-block mb-3" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="160" height="47" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg'?>" alt="Love2Laundry - iOS"></a>
        	<a rel="noopener noreferrer" target="_blank" class="android d-block mb-3" href="https://play.google.com/store/apps/details?id=com.love2laundry.app"><img width="160" height="47" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png'?>" alt="Love2Laundry - Android"></a>
	    </div>
	    <div class="col-lg-6">
	        <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'qrcode.png'?>" width="110" height="110" alt="Install App Love2Laundry" />
	    </div>
	</div>
    <?php /*<div class="form-group form-app-input">
        <span class="locate-app"></span>
        <input type="text" name="<?php echo $field_name?>" id="<?php echo $field_name?>" class="form-control form-control-lg only-number" placeholder="ENTER YOUR NUMBER" />
        <button type="submit" id="<?php echo $button_name?>" class="btn-lg btn btn-primary rounded-pill text-uppercase py-2 px-5">Get APP Link</button>
    </div>*/?>
	<?php } ?>
</form>

<?php }else{ ?>
<h3 class="text-uppercase"><?php echo $this->config->item('front_app_widget_heading_text')?></h3>
<form id="<?php echo $form_name?>" name="<?php echo $form_name?>" method="post" action="<?php echo base_url('app-request')?>">
    <div class="form-group">
        <input type="text" name="<?php echo $field_name?>" id="<?php echo $field_name?>" class="form-control text-center only-number" placeholder="ENTER YOUR NUMBER" />
    </div>
    <div class="form-group text-center">
        <button type="submit" id="<?php echo $button_name?>" class="btn btn-primary btn-lg btn-primary btn-request">Get Download Link</button>
    </div>
    <div class="form-group">
        <p><?php echo $this->config->item('front_app_widget_first_heading_text')?></p>
        <p>
            <a rel="noopener noreferrer" class="app" href="<?php echo isset($apple_store_link)?$apple_store_link:'#'?>" target="_blank">
                <img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'i-tunes.png'?>" alt="Love2Laundry - iOS" />
            </a>
            <a rel="noopener noreferrer" class="app" href="<?php echo isset($google_play_link)?$google_play_link:'#'?>" target="_blank">
                <img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play.png'?>" alt="Love2Laundry - Android" />
            </a>
        </p>
    </div>
</form>
<?php } ?>