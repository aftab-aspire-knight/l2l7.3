<strong class="sub-heading">We are expanding rapidly, check below if we cover your area</strong>
<h2>Enter Your Post Code</h2>
<form class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8" autocomplete="off" id="frm_bottom_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
    <div class="form-group col-sm-8">
        <input type="text" name="post_code" id="post_code" value="" placeholder="ENTER POST CODE" class="form-control text-center field-box" />
    </div>
    <div class="form-group col-sm-4 text-left">
        <button type="button" id="btn_bottom_post_code_search" name="btn_bottom_post_code_search" class="btn btn-primary btn-lg btn-location">Check Prices</button>
    </div>
</form>