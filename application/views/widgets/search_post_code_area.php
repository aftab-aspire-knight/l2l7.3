<h3 class="text-uppercase"><?php echo $this->config->item('front_post_code_search_heading_text')?></h3>
<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
    <div class="form-group">
        <input type="text" name="post_code" id="post_code" placeholder="ENTER POST CODE" class="form-control text-center field-box" />
    </div>
    <div class="form-group text-center">
        <button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-primary btn-lg text-uppercase">Check Prices</button>
    </div>
</form>