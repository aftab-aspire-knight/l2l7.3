<section class="banner">
    <div class="banner-img">
        <?php
        $image_name = $this->config->item('front_asset_image_folder_url') . 'love2laundry-dry-cleaning.jpg';
        if(isset($page_data['HeaderImageName'])){
            if (file_exists($this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'])) {
                $image_name = $this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'];
            }
        }
        ?>
        <img src="<?php echo $image_name?>" alt="Dry Cleaner" />
    </div>
    <div class="innerpage-heading">
        <h2><?php echo isset($page_data['Title'])?$page_data['Title']:''?></h2>
    </div>
</section>