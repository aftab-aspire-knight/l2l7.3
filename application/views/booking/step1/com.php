<?php
//d($addressTypes,1);
?>

<h2 class="order-heading py-2 pb-md-3">Address</h2>
<div id='step1_errors'></div>
<div class="form-group mb-3 mb-md-4">
    <label><?php echo $postal_code_type["label"]; ?></label>
    <input class="form-control form-control-lg text-uppercase" type="text" placeholder="<?php echo $postal_code_type["placeholder"]; ?>" id="postal_code" name="postal_code" value="<?php echo isset($postal_code) ? $postal_code : '' ?>" autocomplete="new-password" />
    <div class="search-result" style="display: none;" id="google_addresses">

    </div>
</div>

<div class="form-group mb-3 mb-md-4 custom-select2" style="display: none;" id="div_addresses">
    <label>Select Your Address</label>
    <select id="addresses" name="addresses" class="custom-select custom-select-lg select2"></select>
</div>
<div class="form-group mb-3 mb-md-4" id="div_reg_address1" style="display: none;">
    <label>Enter your address <span class="red-color">*</span></label>
    <input type="text" placeholder="enter your address" class="form-control form-control-lg  address-building" id="address" name="address" />
</div>

<div class="form-group mb-3 mb-md-4">
    <label>Address details</label>
    <input type="text" placeholder="Please specify any extra address details" class="form-control form-control-lg  address-street" value="<?php echo isset($cart->address2) ? $cart->address2 : "" ?>" id="address2" name="address2" />
</div>
<div class="form-group mb-3 mb-md-4">
    <label>Choose your address type.</label>
    <div class="btn-group btn-block btn-group-toggle" data-toggle="buttons" role="group" aria-label="Address Types">
        <?php
        $i = 0;
        foreach ($addressTypes as $key => $addresType) {
        ?>
            <label class="btn btn-primary <?php if ($i == 0) { ?> active <?php } ?>" for="<?php echo $key; ?>"><input <?php if ($i == 0) { ?> checked <?php } ?> type="radio" class="btn-check" name="address_type" id="<?php echo $key; ?>" autocomplete="off" value="<?php echo $key; ?>">
                <i class="<?php echo $addresType["class"]; ?>"></i><?php echo $addresType["title"]; ?>
            </label>
        <?php
            $i++;
        } ?>
    </div>
</div>
<br clear="all" />
<div id="modal_message_map" style="display: none;">
    <div class="alert alert-danger d" role="alert">
        Oops, we are not there yet, will be there soon.
    </div>
</div>

<div class="map-container" style="display: none;">
    <div id="google_map"></div>
    <div class="maps"><img class="map-pin" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'map-pin.svg' ?>" /></div>
    <div class="next-previous-buttons" style="display: none;">
        <button type="button" class="btn btn-primary btn-order btn-lg btn-block rounded-pill order-look-good text-upercase next" id="step1_next_button">Looks Good</button>
    </div>
</div>

<input type="hidden" <?php if (isset($cart->location)) { ?> value="<?php echo $cart->location ?>" <?php } ?> id="location" name="location" />
<input type="hidden" <?php if (isset($cart->city)) { ?> value="<?php echo $cart->city ?>" <?php } ?> id="town" name="town" />
<!--
<div class="form-group mb-3 mb-md-4 next-previous-buttons"  style="display: none;" >
    <div class="row justify-content-end">
        <div class="col-6 text-right">
            <button type="button" class="btn btn-primary rounded-pill text-uppercase next" >Next</button>
        </div>
    </div>
</div>-->