<h2 class="order-heading py-2 pb-md-3">Address</h2>


<div id='step1_errors'></div>
<div class="form-group mb-3 mb-md-4">
    <label><?php echo $postal_code_type["label"]; ?></label>
    <input onclick="fieldReceivedFocus()" readonly="readonly" class="form-control form-control-lg text-uppercase" type="text" placeholder="<?php echo $postal_code_type["placeholder"]; ?>" id="postal_code" name="postal_code" value="<?php echo isset($postal_code) ? $cart->address1 : '' ?>" autocomplete="new-password" />
    <div class="search-result" style="display: none;" id="google_addresses">

    </div>
</div>

<div class="form-group mb-3 mb-md-4 custom-select2" style="display: none;" id="div_addresses">
    <label>Select Your Address</label>
    <select id="addresses" name="addresses" class="custom-select custom-select-lg select2"></select>
</div>
<div class="form-group mb-3 mb-md-4" id="div_reg_address1" style="display: none;">
    <label>Enter your address <span class="red-color">*</span></label>
    <input type="text" placeholder="enter your address" class="form-control form-control-lg  address-building" id="address" name="address" />
</div>

<script>
    function fieldReceivedFocus() {
        $("#mapModal").modal();
    }
</script>