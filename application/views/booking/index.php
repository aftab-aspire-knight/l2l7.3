<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
$addressTypes = $this->config->item("address_types");
// d($addressTypes,1);
?>

<style>
    /* 
 * Always set the map height explicitly to define the size of the div element
 * that contains the map. 
 */
</style>

<script>
    var vat = <?php echo numberformat($vat) ?>;
    var stripe_key = "<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>";
    var address1 = "";
    var address2 = "";
    var franchise_id = '';
    var collect_date = '';
    var collect_time = '';
    var delivery_date = '';
    var delivery_time = '';
    var addressTypes = <?php echo json_encode($addressTypes);?>;

    <?php if (isset($cart->address1)) { ?>
        addresses = "<?php echo $cart->has_address ?>";
        address1 = "<?php echo $cart->address1 ?>";
        address2 = "<?php echo $cart->address2 ?>";
        franchise_id = "<?php echo $cart->franchise_id ?>";
    <?php } ?>
    <?php if (isset($cart->pickup_date)) { ?>
        collect_date = '<?php echo $cart->pickup_date ?>';
        collect_time = '<?php echo $cart->pickup_time ?>';
        delivery_date = '<?php echo $cart->delivery_date ?>';
        delivery_time = '<?php echo $cart->delivery_time ?>';
    <?php } ?>
</script>

<input type="hidden" id="old_pickup_date" value="<?php echo $cart->pickup_date ?>" />
<input type="hidden" id="old_pickup_time" value="<?php echo $cart->pickup_time ?>" />
<input type="hidden" id="old_delivery_date" value="<?php echo $cart->delivery_date ?>" />
<input type="hidden" id="old_delivery_time" value="<?php echo $cart->delivery_time ?>" />
<input type="hidden" id="stripe_key" value="<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>" />
<input type="hidden" name="has_preferences" id="has_preferences" value="<?php echo $has_preferences; ?>" />
<input type="hidden" id="pickup_time" name="pickup_time" value="<?php if (!empty($cart->pickup_time)) {
                                                                    echo $cart->pickup_time;
                                                                } ?>">
<input type="hidden" id="delivery_time" name="delivery_time" value="<?php if (!empty($cart->delivery_time)) {
                                                                        echo $cart->delivery_time;
                                                                    } ?>">
<input type="hidden" id="is_login" name="is_login" value="<?php echo $is_login ?>">
<input type="hidden" id="payment_method_id" name="payment_method_id" value="">

<header class="header order-header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-md-2">
                <div class="logo">
                    <a rel="home" href="<?php echo base_url() ?>">
                        <img src="<?php echo base_url() ?>assets/images/front/logo.png">
                    </a>
                </div>
            </div>
            <div class="col-6 col-md-10 ">
                <div class="booking-steps">
                    <span class="booking-step header-order-address">1. Address</span>
                    <span class="booking-step header-order-select-items ">2. Item Selection</span>
                    <span class="booking-step header-order-collection-delivery ">3. Collection & Delivery</span>
                    <span class="booking-step header-login-register ">4. Contact Details</span>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="cell example example2" id="example-2">

    <form id="checkout-form" autocomplete="off">
        <div class="order-main py-3 py-lg-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-xl-8">

                        <div class="order-form d-none">

                            <div class="order-address p-3 p-lg-4 mb-2 mb-md-4 steps" data-id="step1_next_button">
                                <?php
                                include("step1/" . SERVER . ".php");
                                ?>
                            </div>

                            <div class="order-select-items steps mini-cart" id="order_select_items" data-id="items_next_button">

                            </div>

                            <div class="order-select-preferences p-3 p-lg-4 mb-2 mb-md-4 steps" id="order_preferences" data-id="preferences_next_button">

                                <h2 class="order-heading py-2 pb-md-3"><?php echo $this->config->item('front_member_laundry_first_heading_text') ?></h2>

                                <?php
                                $area_html = "";
                                $down_area_html = "";
                                foreach ($preference_records as $record) {
                                    //echo "asdadddd";
                                    //d($member_preference_records,1);

                                    $current_area = "";
                                    $current_area .= '<div class="col-12 col-md-6">';
                                    $current_area .= '<div class="form-group">';
                                    $current_area .= '<label>' . $record['Title'] . ' <span class="red-color">*</span></label>';
                                    $current_area .= '<select class="form-control form-control-lg" id="cmb_' . str_replace("-", "_", CreateAccessURL($record['Title'])) . '" name="preferences_list[]">';
                                    foreach ($record['child_records'] as $rec) {
                                        //d($member_preference_records,1);
                                        $selected_preference = "";
                                        if (in_array($rec['PKPreferenceID'], $preferences)) {
                                            $selected_preference = " selected=selected";
                                        }

                                        if (!empty($rec['Price'])) {
                                            $price = '(' . $this->config->item('currency_symbol') . $rec['Price'] . ")";
                                        } else {
                                            $price = "";
                                        }


                                        $current_area .= '<option value="' . $rec['PKPreferenceID'] . '"' . $selected_preference . '>' . $rec['Title'] . $price . '</option>';
                                    }
                                    $current_area .= '</select>';
                                    $current_area .= '</div>';
                                    $current_area .= '</div>';
                                    if ($record['Title'] == "Starch on Shirts") {
                                        $down_area_html .= $current_area;
                                    } else {
                                        $area_html .= $current_area;
                                    }
                                }
                                echo '<div class="row">';
                                echo $area_html;
                                echo '</div>';
                                echo '<hr/>';
                                echo '<h3 class="order-heading">' . $this->config->item('front_member_laundry_second_heading_text') . '</h3>';
                                echo '<div class="row">';
                                echo $down_area_html;
                                echo '<div class="col-12 col-md-6">';
                                echo '<div class="form-group">';
                                //echo '<label>Account Notes</label>';
                                // echo '<textarea class="form-control form-control-lg" id="account_notes" name="account_notes" rows="5">' . (isset($member_detail['AccountNotes']) ? $member_detail['AccountNotes'] : '') . '</textarea>';
                                echo '</div>';
                                echo '</div>';
                                echo '<div class="col-12 col-md-6">';
                                echo '<div class="form-group">';
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                ?>
                                <div class="form-group mb-3 mb-md-4 next-previous-buttons">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-secondary shadow rounded-pill text-uppercase previous" id="preferences_previous_button">Previous</button>
                                        </div>
                                        <div class="col-6 text-right">
                                            <button type="button" class="btn btn-primary rounded-pill text-uppercase next" id="preferences_next_button">Next</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="order-collection-delivery steps" data-id="collection_next_button">
                                <div class="order-collection order-timeslots p-3 p-lg-4 mb-2 mb-md-4">
                                    <div id='order_collection_errors'></div>
                                    <h2 class="order-heading pb-2 pb-md-3">Collection time</h2>
                                    <div class="row align-items-center">
                                        <div class="col-lg-3 col-12 text-center">
                                            <img width="180" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'collection.svg' ?>" />
                                        </div>
                                        <div class="col-md col-12">
                                            <div class="form-group mb-1 custom-select2">
                                                <label>Date</label>
                                                <!--select2-->
                                                <select class="custom-select custom-select-lg" onchange="getPickupTimes()" id="pickup_date" name="pickup_date"></select>
                                            </div>
                                        </div>
                                        <div class="col-md col-12">
                                            <div class="form-group mb-1">

                                                <label>Time</label>
                                                <div id="pickup_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none;">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time=""><i class="fa fa-leaf" aria-hidden="true"></i>
                                                        </span>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="order-delivery order-timeslots p-3 p-lg-4 mb-2 mb-md-4">
                                    <h2 class="order-heading pb-2 pb-md-3">Delivery time</h2>
                                    <div class="row align-items-center">
                                        <div class="col-lg-3 col-12 text-center">
                                            <img width="180" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'delivery.svg' ?>" />
                                        </div>
                                        <div class="col-md col-12">
                                            <div class="form-group mb-1 custom-select2">
                                                <label>Date</label>
                                                <select onchange="getDeliveryTimes()" id="delivery_date" name="delivery_date" class="custom-select custom-select-lg">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md col-12">
                                            <div class="form-group mb-1">
                                                <label>Time</label>
                                                <div id="delivery_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none;">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time=""><i class="fa fa-leaf" aria-hidden="true"></i>
                                                        </span>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-delivery p-3 p-lg-4 mb-2 mb-md-4">
                                    <h2 class="order-heading pb-2 pb-md-3">Order instructions (optional)</h2>
                                    <textarea class="form-control" id="order_notes" id="order_notes" placeholder="Add any special instructions for the driver / cleaning team"><?php echo isset($cart->order_notes) ? $cart->order_notes : ""; ?></textarea>
                                </div>



                                <div class="form-group mb-3 mb-md-4 next-previous-buttons">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" id="collection_previous_button" class="btn btn-secondary shadow rounded-pill text-uppercase previous">Previous</button>

                                        </div>
                                        <div class="col-6 text-right">
                                            <button type="button" class="btn btn-primary rounded-pill text-uppercase next" id="collection_next_button">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div data-id="register_login_next_button" class="order-detail login-register steps <?php if ($is_login == false) { ?> d-none  <?php } ?>">
                                <div class="order-contact-detail p-3 p-lg-4 mb-2 mb-md-4">
                                    <h2 class="order-heading py-2 pb-md-3">Contact Details </h2>
                                    <div id='contact_errors'></div>

                                    
                                    <div class="row">
                                    <?php /*?>
                                        <div class="col-md-6 col-12 mb-2 mb-md-4">
                                            <button type="button" class="btn btn-block loginBtn loginBtn--facebook rounded-pill" onclick="checkLoginState();">Facebook</button>
                                        </div>
                                        <?php */ ?>
                                        <div class="col-md-6 col-12 mb-2 mb-md-4">
                                            <div class="google-btn rounded-pill" id="gs2" data-onsuccess="onSuccess"></div>
                                        </div>
                                    </div>





                                    <div class="login-form d-none">
                                        <h2 class="order-heading py-2 pb-md-3">Contact Details

                                            <a class="order-skip-item edit login-register-a pulsate" href="#" data-show="register-form" data-hide="login-form"><i class="fa fa-sign-in"></i> Create an account?</a>

                                        </h2>


                                        <div class="row">
                                            <div class="form-group col-md-12 col-12 mb-2 mb-md-4">
                                                <label>Email Address</label>
                                                <input class="form-control form-control-lg" type="text" name="login_email" id="login_email" placeholder="Enter email address" value="" />
                                            </div>
                                            <div class="form-group col-md-12 col-12 mb-2 mb-md-4">
                                                <label>Password</label>
                                                <input placeholder="Enter your password" type="password" id="login_password" name="login_password" class="form-control form-control-lg" aria-label="password" value="">
                                            </div>
                                        </div>
                                        <div class="row justify-content-end pb-3">
                                            <div class="col-6 text-right">
                                                <button type="button" data-toggle="modal" data-target="#forgotModal" class="pt-0 btn-link text-primary border-0 small bg-transparent">Forgot password</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-form">
                                        <h2 class="order-heading py-2 pb-md-3">Contact Details

                                            <a class="order-skip-item edit login-register-a pulsate" href="#" data-hide="register-form" data-show="login-form"><i class="fa fa-sign-in"></i> Already have an account?</a>


                                        </h2>
                                        <input type="hidden" id="social_id" name="social_id" value="" />
                                        <input type="hidden" id="type" name="type" value="" />
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label>First Name</label>
                                                    <input class="form-control form-control-lg" type="text" name="first_name" id="first_name" placeholder="Enter first name" value="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label>Last Name</label>
                                                    <input class="form-control form-control-lg" type="text" name="last_name" id="last_name" placeholder="Enter last name" value="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label>Email Address</label>
                                                    <input class="form-control form-control-lg" type="text" name="email" id="email" placeholder="Enter email address" value="" />
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-12">
                                                <label>Phone</label>

                                                <div class="input-group country-code">

                                                    <div class="input-group-prepend custom-select2 ">
                                                        <span class="input-text-country">+<?php echo $this->config->item("country_phone_code") ?></span>
                                                        <select class="custom-select custom-select-lg" id="country_code" name="country_code">
                                                            <?php
                                                            foreach ($codes as $key => $country) {
                                                            ?>
                                                                <option value="<?php echo $key ?>" <?php if ($key == $this->config->item("country_phone_code")) { ?> selected <?php } ?>><?php echo $country ?></option>
                                                            <?php } ?>
                                                        </select>

                                                    </div>

                                                    <input class="form-control form-control-lg only-number req" type="tel" data-error-message="Please enter phone number" placeholder="<?php echo $this->config->item("phone_placeholder") ?>" id="phone" name="phone" value="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label>Password</label>
                                                    <div class="input-group">
                                                        <input placeholder="Create your password" type="password" id="password" name="password" class="form-control form-control-lg" aria-label="password" value="">
                                                        <div class="input-group-append" id="btn_password_show">
                                                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label>Confirm Password</label>
                                                    <div class="input-group">
                                                        <input placeholder="Re-enter your password" type="password" id="confirm_password" name="confirm_password" class="form-control form-control-lg" aria-label="password" value="">
                                                        <div class="input-group-append" id="btn_password_show">
                                                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="row next-previous-buttons">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-secondary shadow rounded-pill text-uppercase previous" id="register_login_previous_button">Previous</button>
                                        </div>
                                        <div class="col-6 text-right">
                                            <button type="button" class="btn btn-primary rounded-pill text-uppercase next" id="register_login_next_button" value="register-form">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="order-contact-detail order-payments p-3 p-lg-4 mb-2 mb-md-4 steps d-none" data-id="order_payments">

                                <h2 class="order-heading py-2 pb-md-3">How can we contact you?</h2>
                                <div style="display:none;" class="col-md-12 col-12" id="payment_errors"></div>
                                <div class="contact-form">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="form-group mb-2 mb-md-4">
                                                <label>First Name</label>
                                                <input class="form-control form-control-lg" type="text" name="payment_first_name" id="payment_first_name" placeholder="Enter first name" value="<?php
                                                                                                                                                                                                if ($is_login) {
                                                                                                                                                                                                    echo $member['FirstName'];
                                                                                                                                                                                                }
                                                                                                                                                                                                ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group mb-2 mb-md-4">
                                                <label>Last Name</label>
                                                <input class="form-control form-control-lg" type="text" name="payment_last_name" id="payment_last_name" placeholder="Enter last name" value="<?php
                                                                                                                                                                                                if ($is_login) {
                                                                                                                                                                                                    echo $member['LastName'];
                                                                                                                                                                                                }
                                                                                                                                                                                                ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group mb-2 mb-md-4">
                                                <label>Email Address</label>
                                                <input class="form-control form-control-lg" type="text" name="payment_email" id="payment_email" placeholder="Enter email address" readonly="readonly" value="<?php
                                                                                                                                                                                                                if ($is_login) {
                                                                                                                                                                                                                    echo $member['EmailAddress'];
                                                                                                                                                                                                                }
                                                                                                                                                                                                                ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <label>Phone</label>
                                            <div class="input-group country-code">
                                                <div class="input-group-prepend custom-select2">

                                                    <span class="input-text-country">+<?php echo $member['CountryCode'] ?></span>
                                                    <select class="custom-select custom-select-lg" id="payment_country_code" name="payment_country_code">

                                                        <?php
                                                        foreach ($codes as $key => $country) {
                                                        ?>
                                                            <option value="<?php echo $key ?>" <?php if ($key == $member['CountryCode']) { ?> selected <?php } ?>><?php echo $country ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <input class="form-control form-control-lg only-number req" type="tel" data-error-message="Please enter phone number" placeholder="<?php echo $this->config->item("phone_placeholder") ?>" id="payment_phone" name="payment_phone" value="<?php
                                                                                                                                                                                                                                                                                            if ($is_login) {
                                                                                                                                                                                                                                                                                                echo ltrim($member['Phone'], 0);
                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                            ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <h2 class="order-heading py-2 pb-md-3">Payments Options </h2>

                                <div class="row <?php if (count($payment_methods) == 1) { ?> d-none <?php } ?> ">
                                    <div class="col-md-12 col-12">
                                        <div class="btn-group btn-group-toggle py-3" data-toggle="buttons">
                                            <?php
                                            $i = 0;
                                            foreach ($payment_methods as $p) {
                                            ?>
                                                <label class="btn btn-primary <?php if ($i == 0) { ?>active<?php } ?>">
                                                    <input type="radio" value="<?php echo $p["code"]; ?>" id="payment_methods_<?php echo $p["code"]; ?>" <?php if ($i == 0) { ?> checked="checked" <?php } ?> name="payment_methods" autocomplete="off" class="form-check-input payment-methods">
                                                    <?php echo $p["title"]; ?>
                                                </label>
                                            <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="row stripe">



                                    <div class="col-md-12 col-12 cards-list" id="cards-list" <?php if (empty($cards)) { ?> style="display: none;" <?php } ?>>
                                        <div class="form-group col-12 offset-md-2 col-md-8 mb-3 mb-md-4">
                                            <button type="button" class="btn btn-block btn-outline-dark rounded-0  order-payment-btn payment-cards-a cards-list" data-show="card-form" data-hide="cards-list"><i class="fas fa-plus mr-2"></i>Add Payment</button>
                                        </div>
                                        <div class="form-group mb-2 mb-md-4">
                                            <label>Select Card <span class="red-color">*</span></label>

                                            <?php
                                            $i = 0;
                                            foreach ($cards as $card) {
                                            ?>
                                                <div class="custom-control custom-radio btn btn-block btn-payment border shadow mb-3 py-3 rounded-0 <?php if ($i == 0) { ?> active <?php } ?>">
                                                    <input type="radio" value="<?php echo $card["id"] ?>" id="payments_cards_<?php echo $card["id"]; ?>" class="custom-control-input payments-cards" <?php if ($i == 0) { ?> checked="checked" <?php } ?> name="payments_cards">

                                                    <label class="custom-control-label form-row px-3 align-items-center" for="payments_cards_<?php echo $card["id"]; ?>">
                                                        <div class="col-7 text-left">
                                                            <p class="m-0">Card Number: <?php echo $card["title"] ?></p>
                                                        </div>
                                                        <div class="col-5 text-md-right">
                                                            <!-- <i class="fab fa-cc-visa fa-2x text-primary"></i> -->
                                                        </div>
                                                    </label>
                                                </div>

                                            <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="cell example example2 card-form col-md-12 col-12" id="card-form" <?php if (!empty($cards)) { ?> style="display: none;" <?php } ?>>
                                        <div class="row">
                                            <div class="form-group col-12 offset-md-2 col-md-8 mb-3 mb-md-4">
                                                <button type="button" <?php if (empty($cards)) { ?> style="display: none;" <?php } ?>class="btn btn-block btn-outline-dark order-payment-btn mx-1 payment-cards-a" data-show="cards-list" data-hide="card-form"><i class="far fa-edit mr-2"></i>Use Existing</a>
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">Card number</label>
                                                    <div id="card-number" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">Expiry Date</label>
                                                    <div id="card-expiry" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group mb-2 mb-md-4">
                                                    <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">cvc</label>
                                                    <div id="card-cvc" class="input empty form-control form-control-lg stripe-input"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-12 mb-1 mb-md-3">
                                                <small>*Please note the final itemisation will done by the facility. The total invoice amount may vary.</small>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="loader">
                            <i class="l2l-setting fas fa-pulse fa-2x"></i>
                        </div>
                    </div>

                    <div class="col-lg-5 col-xl-4 order-summary-container">
                        <div class="order-sticky">
                            <div class="order-summary py-3 px-3 px-xl-4 mb-lg-2 mb-1">
                                <h2 class="order-summary-heading pb-2 pb-md-2 mb-1">Order Summary <button type="button" class="btn btn-sm btn-dark rounded-circle d-block d-lg-none close btn-order-summary-close" aria-label="Close"><span aria-hidden="true">&times;</span></button></h2>

                                <div class="list-group list-group-flush">
                                    <div class="order-address-right list-group-item <?php if (count($cart) > 0) { ?> active <?php } ?> px-0 d-flex order-address-summary">
                                        <span class="badge px-0 pt-1 mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info">
                                            <span>Address <button type="button" class="order-summary-edit" data-show="order-address"><i class="far fa-edit"></i></button></span>
                                            <small id="right_order_address"><?php echo (count($cart) > 0) ? $cart->address1 . " " . $cart->address2 : "" ?></small>
                                        </div>
                                    </div>
                                    <div class="order-select-items-right list-group-item <?php if (!empty($cart->type)) { ?> active <?php } ?> px-0 d-flex ">
                                        <span class="badge px-0 pt-1 mr-2 "><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info media-body">
                                            <span>Item Selection <button type="button" class="order-summary-edit <?php if (empty($cart->type)) { ?> d-none <?php } ?>" data-show="order-select-items"><i class="far fa-edit"></i></button></span>
                                            <div class="mini-cart-right <?php if ($cart->type == "later" && empty($cart_services)) { ?> d-none <?php } ?>">
                                                <?php
                                                if (!empty($cart_services)) {


                                                    foreach ($cart_services as $service) {
                                                ?>
                                                        <div class="form-row mx-0 cart-service-<?php echo $service->service_id; ?>">
                                                            <div class="col-7 cart-name"><?php echo $service->title; ?></div>
                                                            <div class="col-1 cart-qty cart-qty-service-<?php echo $service->service_id; ?>"><?php echo $service->quantity; ?></div>
                                                            <div class="col-4 cart-price cart-price-service-<?php echo $service->service_id; ?>"><?php echo $this->config->item("currency_symbol") . $service->total; ?></div>

                                                        </div>

                                                <?php
                                                    }
                                                }
                                                ?>

                                            </div>
                                            <div class="mini-cart-right-later <?php if ($cart->type == "items" || empty($cart->type)) { ?> d-none <?php } ?>"> Later</div>
                                        </div>
                                    </div>
                                    <div class="list-group-item px-0 d-flex order-collection-delivery-right <?php if (!empty($cart->pickup_date)) { ?> active <?php } ?> ">
                                        <span class="badge px-0 pt-1 <?php if (!empty($cart->pickup_date)) { ?> active <?php } ?> mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info">
                                            <span>Collection & Delivery <button type="button" class="order-summary-edit <?php if (empty($cart->pickup_date)) { ?> d-none <?php } ?>" data-show="order-collection-delivery" type="button"><i class="far fa-edit  "></i></button></span>
                                            <small class="collection <?php if (empty($cart->pickup_date)) { ?> d-none <?php } ?>">Collection: <span id="collection_pickup_date"><?php
                                                                                                                                                                                if (!empty($cart->pickup_date)) {
                                                                                                                                                                                    echo date('D, d M', strtotime($cart->pickup_date));
                                                                                                                                                                                }
                                                                                                                                                                                ?></span> <span id="collection_pickup_time">(<?php
                                                                                                                                                                                                                                if (!empty($cart->pickup_time)) {
                                                                                                                                                                                                                                    echo $cart->pickup_time;
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                ?>)</span></small>
                                            <small class="collection <?php if (empty($cart->pickup_date)) { ?> d-none <?php } ?>">Delivery: <span id="collection_delivery_date"><?php
                                                                                                                                                                                if (empty(!$cart->delivery_date)) {
                                                                                                                                                                                    echo date('D, d M', strtotime($cart->delivery_date));
                                                                                                                                                                                }
                                                                                                                                                                                ?></span> <span id="collection_delivery_time">(<?php
                                                                                                                                                                                                                                if (empty(!$cart->delivery_time)) {
                                                                                                                                                                                                                                    echo $cart->delivery_time;
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                ?>)</span></small>
                                        </div>
                                    </div>
                                    <div class="list-group-item px-0 d-flex login-register-right <?php if ($is_login) { ?> active <?php } ?> ">
                                        <span class="badge px-0 pt-1 mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info contact-detail-right">
                                            <span>Contact Details <button type="button" class="order-summary-edit <?php if (!isset($member['PKMemberID']) || empty($cart->pickup_date)) { ?> d-none <?php } ?>" data-show="order-payments" type="button"><i class="far fa-edit "></i></button></span>
                                            <small class="<?php if (!$is_login) { ?> d-none <?php } ?> contact-detail">Name: <span id="contact_name"><?php
                                                                                                                                                        if ($is_login) {
                                                                                                                                                            echo $member['FirstName'] . " " . $member['LastName'];
                                                                                                                                                        }
                                                                                                                                                        ?></span></small>
                                            <small class="<?php if (!$is_login) { ?> d-none <?php } ?> contact-detail ">Email: <span id="contact_email"><?php
                                                                                                                                                        if ($is_login) {
                                                                                                                                                            echo $member['EmailAddress'];
                                                                                                                                                        }
                                                                                                                                                        ?></span></small>
                                            <small class="<?php if (!$is_login) { ?> d-none <?php } ?> contact-detail">Phone: <span id="contact_phone"><?php
                                                                                                                                                        if ($is_login) {
                                                                                                                                                            echo "+" . $this->config->item("country_phone_code") . ltrim($member['Phone'], 0);
                                                                                                                                                        }
                                                                                                                                                        ?></span></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-minimum form-row pb-2 py-md-1">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Minimum Order Value <?php echo $this->config->item("currency_symbol") ?><span id="item_minimum_amount"><?php echo isset($franchise["MinimumOrderAmount"]) ? $franchise["MinimumOrderAmount"] : 0.00; ?></span> </span>
                                    </div>
                                </div>
                                <div class="order-services-right form-row" style="display: none">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Services Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-services-total"> <?php echo $this->config->item("currency_symbol") ?><span id="item_services_price">0</span></span>
                                    </div>
                                </div>
                                <div class="order-discount-right form-row" style="display: none">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text discount-right-text"> Discount Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-discount-amount"> <?php echo $this->config->item("currency_symbol") ?><span id="item_discount_price">0</span></span>
                                    </div>
                                </div>
                                <div class="order-preferences-right form-row" style="display: none">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text"> Preferences Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-preferences-amount"> <?php echo $this->config->item("currency_symbol") ?><span id="item_preferences_price">0</span></span>
                                    </div>
                                </div>
                                <div class="order-minimum form-row pb-2 py-md-1">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Grand Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <span class="order-amount"> <?php echo $this->config->item("currency_symbol") ?><span id="item_total_price">0.00</span></span>
                                    </div>
                                </div>
                                <div class="order-discount form-row py-2">
                                    <div class="col-9 col-sm-8">
                                        <span class="order-discount-text"><a href="javascript:void(0);" class="discount-button btn-block">Apply discount or referral code </a></span>
                                    </div>
                                    <div class="col-3 col-sm-4">
                                        <button type="button" class="btn btn-sm btn-primary btn-block d-table ml-auto discount-button ">APPLY</button>
                                    </div>
                                </div>
                                <div id="right_below_errors" class="d-none">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <p class="mb-0 order-discount-text">Please login or register to use the discount code.</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="non-responsive-btns d-none">
                                <button type="button" class="btn btn-primary btn-order btn-lg btn-block rounded-pill <?php
                                                                                                                        if ($order_payments == "") {
                                                                                                                            echo "d-none";
                                                                                                                        }
                                                                                                                        ?> next-rgt-button">Next</button>
                                <button type="submit" class="btn btn-primary btn-order btn-lg btn-block rounded-pill
                                <?php
                                if ($order_payments != "") {
                                    echo "d-none";
                                }
                                ?> next-rgt-confirm-button" data-tid="elements_examples.form.pay_button">Confirm Order</button>

                            </div>
                            <div class="order-term-condition my-3">
                                <button type="submit" class="responsive-btns btn btn-primary py-3 mb-3 font-weight-bold rounded-pill btn-block btn-lg order-mobile d-lg-none <?php
                                                                                                                                                                                if ($order_payments != "") {
                                                                                                                                                                                    echo "d-none";
                                                                                                                                                                                }
                                                                                                                                                                                ?>  next-rgt-confirm-button" data-tid="elements_examples.form.pay_button">Confirm Order</button>


                                <button type="button" class="btn btn-dark btn-block btn-lg mb-3 font-weight-bold rounded-pill order-mobile d-none next-rgt-loading-button" disabled="disabled">Please wait...</button>
                                <div class="px-3">
                                    <label class="small mb-0" for="chk_terms">By continuing you agree to our <a target="_blank" href="<?php echo base_url($this->config->item('front_term_page_url')) ?>">Terms & Conditions</a> and Privacy Policy.</label>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="fixed-bottom d-lg-none responsive-btns d-none">
            <button type="button" class="btn btn-primary py-3 font-weight-bold rounded-0 btn-block btn-lg order-mobile <?php
                                                                                                                        if ($order_payments == "") {
                                                                                                                            echo "d-none";
                                                                                                                        }
                                                                                                                        ?> next-rgt-button ">Next</button>

        </div>
    </form>

</div>

<!-- Modal -->
<div class="modal fade" id="discountModal" tabindex="-1" role="dialog" aria-labelledby="discountModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-primary border-light">
                <h6 class="modal-title font-weight-bold" id="discountModalCenterTitle">Please enter discount code or referral</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="order-form">
                    <div id="discount_errors"></div>

                    <div class="form-group form-row mb-0">
                        <div class="col-12 col-sm-6 text-center text-md-left"><label>Discount Type </label></div>
                        <div class="col-12 col-sm-6 text-center">
                            <div class="btn-group btn-group-toggle float-md-right" data-toggle="buttons">
                                <label class="btn btn-primary active">
                                    <input type="radio" name="discount_type" id="discount" autocomplete="off" value="discount" checked="checked"> Discount
                                </label>
                                <label class="btn btn-primary">
                                    <input type="radio" name="discount_type" id="referral" autocomplete="off" value="referral"> Referral
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="form-group mb-0" id="div_reg_address1">
                        <label>Code <span class="red-color">*</span></label>
                        <input type="text" value="" placeholder="Enter enter your discount code or referral code" class="form-control form-control-lg" id="code" name="code" />
                    </div>
                </div>
            </div>
            <div class="modal-footer border-light">
                <button type="button" class="btn btn-dark btn-block btn-lg apply-discount">Apply</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="forgotModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-primary border-light">
                <h5 class="modal-title font-weight-bold" id="discountModalCenterTitle">Forgot Password?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="forgot_errors"></div>
                <div class="order-form">
                    <p class="h6">Just let us know Your Registered Email</p>
                    <div id="msg_box"></div>
                    <div class="form-group mb-0">
                        <label for="login_password">Password</label>
                        <input class="form-control form-control-lg" placeholder="Enter Your Email Address" id="forgot_email_address" name="forgot_email_address" value="" />
                    </div>
                </div>
            </div>
            <div class="modal-footer border-light">
                <button type="button" id="btn_forgot" name="btn_forgot" class="btn btn-dark btn-block btn-lg">Send Me My Password</button>
            </div>
        </div>
    </div>
</div>