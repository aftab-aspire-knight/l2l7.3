<div class="order-summary-list-info media-body mini-cart">
    <?php
    if (count($services) > 0) {
        ?>

        <?php
        foreach ($services as $key => $service) {
            ?>

            <div class="form-row mx-0 service-<?php echo $service->service_id ?>" >
                <div class="col-8 cart-name"><?php echo $service->title ?></div>
                <div class="col-2 cart-qty"><?php echo $service->quantity ?></div>
                <div class="col-2 cart-price-service-<?php echo $service->service_id ?>"><?php echo $this->config->item("currency_symbol") ?><?php echo $service->total ?></div>
            </div>
            <?php
        }
    } else {
        ?>
    <?php } ?>
</div>

