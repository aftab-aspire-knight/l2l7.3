<?php
if (count($predictions) > 0) {
    ?>
    <ul class="result">
        <?php
        foreach ($predictions as $prediction) {
            ?>
            <li><a href="javascript:void(0)"  class="prediction-description" data-description="<?php echo $prediction["description"]; ?>"><?php echo $prediction["description"]; ?></a></li>
                <?php
            }
            ?>
    </ul>
    <?php
} else {
    ?>

    <?php
}
?>

<script>
    $(".prediction-description").click(function () {
        var description = $(this).attr("data-description");

        $("#addresses").html("");
        $("#addresses").html("<option value='" + description + "'>" + description + "</option>").show();
        //$("#div_addresses").show();
        $("#right_order_address").html(description);
        $("#postal_code").val(description);
        $("#google_addresses").hide();
    });
</script>
