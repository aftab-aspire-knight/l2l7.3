<div class="signin d-flex align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 pb-5 pb-md-0 pt-3 pt-md-0 align-self-center">
                <h1 class="ff-gothamblack mb-3">Thank you for placing an <br>order with Love2Laundry</h1>
                <p>Please call us on 0203 600 0296 or contact us for more information.</p>
                <a href="<?php echo base_url() ?>" class="btn btn-primary rounded-pill py-3 px-0 px-md-5"><span class="px-4">Home</span></a>
                <a href="<?php echo base_url($this->config->item('front_dashboard_page_url')) ?>" class="btn btn-outline-dark rounded-pill py-3 px-5">Dashboard</a>
            </div>
            <div class="col-md-5 py-3">

                <div class="px-3 px-lg-5 py-4 rounded-xl bg-white text-center">
                    <div class="mt-n4">
                        <img class="img-fluid mt-n5" src="<?php echo base_url() ?>assets/images/front/thank-you.png" alt="" />
                    </div>
                    <div class="my-3 lead text-uppercase">Set referral code to <span class="font-weight-bold">get <?php echo ($this->config->item('currency_symbol')) ?><?php echo $referral_code_amount ?> <br />discount</span> by referring your friend</div>
                    <span>Your Referral Code</span>
                    <button class="btn btn-dark btn-lg btn-block rounded-pill py-3 px-5 mx-auto d-table my-3 h4"><span class="px-5" id="thanks"><?php echo $member_detail["ReferralCode"] ?></span></button>
                </div>
            </div>
        </div>
    </div>
</div>