<section class="order-process">

    <div class="light-gray">
        <div class="container">
            <div class="row">
                <form class="form-horizontal custom-forms" id="frm_cart_member_address" name="frm_cart_member_address" method="post" action="<?php echo base_url($this->config->item('front_address_selection_page_url')) ?>">
                    <input type="hidden" id="address_url" value="<?php echo base_url($this->config->item('front_address_selection_page_url')) ?>" />
                    <input type="hidden" id="card_id" name="card_id" />
                    <input type="hidden" id="c_valid" name="c_valid" />
                    <input type="hidden" id="hidden_session_postal_code" value="<?php echo isset($hidden_session_postal_code) ? $hidden_session_postal_code : '' ?>" />
                    <input type="hidden" id="hidden_postal_code" />
                    <input type="hidden" id="hidden_building_name" />
                    <input type="hidden" id="hidden_street_name" />
                    <input type="hidden" id="hidden_town" />
                    <input type="hidden" id="stripe_key" value="<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>" />
                    <input type="hidden" id="time_collection_url" value="<?php echo site_url('time-collections') ?>" />

                    <div class="col-md-6">
                        <div class="order-details" id="address_information">
                            <h3>Address Information</h3>
                            <div class="col-sm-12 text-center" id="address_loader">
                                Loading <i class="l2l-laundry-setting l2l-spin"></i>
                            </div>


                            <div id="address_postal_area" class="hide-area">
                                <div id="post_code_look_up">
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-4 control-label">Post Code <span class="red-color">*</span></label>
                                        <div class="col-sm-8" id="post_area">
                                            <div class="col-sm-12 no-padding icon-field">
                                                <input type="text" placeholder="Post Code" class="form-control" id="postal_code" name="postal_code" value="<?php echo isset($session_post_code) ? $session_post_code : '' ?>" />
                                                <i class="l2l-postcode"></i>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-sm-12" style="display: none;" id="div_addresses">
                                    <label class="col-sm-4 control-label">Select your Address <span class="red-color">*</span></label>
                                    <div class="col-sm-8 icon-field">
                                        <select  id="addresses" name="addresses" class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12" id="div_reg_address1" style="display: none;">
                                    <label class="col-sm-4 control-label">Enter your address <span class="red-color">*</span></label>
                                    <div class="col-sm-8 icon-field">
                                        <input type="text" placeholder="Enter your address" class="form-control address-building" id="address" name="address" />
                                        <i class="l2l-building"></i>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-4 control-label">Address Line 2</label>
                                    <div class="col-sm-8 icon-field">
                                        <input type="text" placeholder="Please specify any extra address details" class="form-control address-street" id="street_name" name="street_name" />
                                        <i class="l2l-street"></i>
                                    </div>
                                </div>
                                <input type="hidden" value="London" placeholder="Town" class="form-control address-town" id="town" name="town" />

                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="order-details">
                            <h3>Contact Details</h3>
                            <?php if (!isset($member_detail)) { ?>
                                <a class="edit" href="#login_area" id="btn_login_show"><i class="fa fa-sign-in"></i> Already have an account?</a>
                            <?php } ?>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 control-label">First Name <span class="red-color">*</span></label>
                                <div class="col-sm-8 icon-field">
                                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter Your First Name" value="<?php echo isset($member_detail) ? $member_detail['FirstName'] : '' ?>" <?php echo isset($member_detail) ? ' disabled="disabled"' : '' ?> />
                                    <i class="l2l-first-name"></i>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 control-label">Last Name <span class="red-color">*</span></label>
                                <div class="col-sm-8 icon-field">
                                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Your Last Name" value="<?php echo isset($member_detail) ? $member_detail['LastName'] : '' ?>" <?php echo isset($member_detail) ? ' disabled="disabled"' : '' ?> />
                                    <i class="l2l-last-name"></i>
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 control-label">Email Address <span class="red-color">*</span></label>
                                <div class="col-sm-8 icon-field">
                                    <input type="text" id="email_address" name="email_address" class="form-control" placeholder="Enter Your Email Address" value="<?php echo isset($member_detail) ? $member_detail['EmailAddress'] : '' ?>" <?php echo isset($member_detail) ? ' disabled="disabled"' : '' ?> />
                                    <i class="l2l-email"></i>
                                </div>
                            </div>
                            <?php if (!isset($member_detail)) { ?>
                                <div class="form-group col-sm-12" id="pass_area">
                                    <label class="col-sm-4 control-label">Password <span class="red-color">*</span></label>
                                    <div class="col-sm-8 icon-field">
                                        <input type="password" id="u_password" name="u_password" class="form-control" placeholder="Create Your Password" />
                                        <i class="l2l-password"></i>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 control-label">Phone Number <span class="red-color">*</span></label>
                                <div class="col-sm-8 icon-field">
                                    <input type="tel" id="phone_number" name="phone_number" class="form-control only-number phone-tel" value="<?php echo isset($member_detail) ? $member_detail['Phone'] : '' ?>" <?php echo isset($member_detail) ? ' disabled="disabled"' : '' ?> />
                                    <i class="l2l-phone"></i>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12 text-center" id="time_loader">
                        Loading <i class="l2l-laundry-setting l2l-spin"></i>
                    </div>
                    <div class="col-sm-12 hide-area" id="time_select_area">
                        <div class="row">    
                            <div class="col-sm-6">
                                <div class="dt-picker">

                                    <div class="custom-calender">



                                        <div class="timeslots row" id="pick_time_slots" class="time-values">
                                            <h4 class="time-values">CHOOSE A PICKUP TIME</h4>
                                            <div class="col-sm-6">

                                                <select onchange="getPickupTimes()" id="pickup_date" name="pickup_date" class="form-control select2">

                                                </select>


                                            </div>
                                            <div class="col-sm-6">


                                                <input type="hidden" id="pickup_time" name="pickup_time" value="" >

                                                <div id="pickup_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time="17:00 - 18:00"><i class="fa fa-leaf" aria-hidden="true"></i>
                                                            17:00 - 18:00</span>

                                                        <span class="time-options other-options">Other time options</span>
                                                        <span class="time-slot" data-time="22:00 - 00:00">22:00 - 00:00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="dt-picker">

                                    <div class="custom-calender">
                                        <div class="date-tabs">
                                            <ul id="delivery_date_slots" class="time-values">
                                            </ul>
                                        </div>


                                        <div class="timeslots row" id="delivery_time_slots" class="time-values">
                                            <h4 class="time-values">CHOOSE A DELIVERY TIME</h4>
                                            <div class="col-sm-6">

                                                <select onchange="getDeliveryTimes()" id="delivery_date" name="delivery_date" class="form-control select2">

                                                </select>


                                            </div>
                                            <div class="col-sm-6">


                                                <input type="hidden" id="delivery_time" name="delivery_time" value="" >

                                                <div id="delivery_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none;">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time="17:00 - 18:00"><i class="fa fa-leaf" aria-hidden="true"></i>
                                                            17:00 - 18:00</span>

                                                        <span class="time-options other-options">Other time options</span>
                                                        <span class="time-slot" data-time="22:00 - 00:00">22:00 - 00:00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <br clear="all" />
    <div class="custompopup-cover" id="login_area">
        <button type="button" id="btn_login_close" class="close customclose"><span aria-hidden="true">&times;</span></button>
        <div class="custompopup">
            <i class="l2l-first-name l2l-3x"></i>
            <h3>Login</h3>
            <form class="custom-forms" id="frm_login" name="frm_login" method="post" action="<?php echo base_url('login') ?>">
                <input type="hidden" id="is_order_area" name="is_order_area" />
                <div class="form-group">
                    <div class="icon-field">
                        <input type="text" placeholder="Enter Your Email Address" class="form-control" id="login_email_address" name="login_email_address" />
                        <i class="l2l-email"></i>
                    </div>
                </div>
                <div class="form-group">
                    <div class="icon-field">
                        <input type="password" placeholder="Enter Your Password" class="form-control" id="login_password" name="login_password" />
                        <i class="l2l-password"></i>
                    </div>
                </div>

                <button type="submit" id="btn_login" name="btn_login" class="btn btn-danger btn-block">Login</button>

                <a href="#forgot_area" class="forgot-text" id="btn_forgot_show">Forgot Password</a>

            </form>
            <div id="msg_box_login"></div>
        </div>
    </div>
    <div class="custompopup-cover" id="forgot_area">
        <button type="button" id="btn_forgot_close" class="close customclose"><span aria-hidden="true">&times;</span></button>
        <div class="custompopup">
            <i class="l2l-forgot-password l2l-3x"></i>
            <h3>Forgot Password?</h3>
            <p>Just let us know Your Registered Email</p>
            <div id="msg_box"></div>
            <form class="custom-forms" id="frm_forgot" name="frm_forgot" method="post" action="<?php echo base_url('forgot') ?>">
                <div class="form-group">
                    <div class="icon-field">
                        <input type="text" placeholder="Enter Your Email Address" class="form-control" id="forgot_email_address" name="forgot_email_address"/>
                        <i class="l2l-email"></i>
                    </div>
                </div>
                <button type="submit" id="btn_forgot" name="btn_forgot" class="btn btn-danger btn-block">Send Me My Password</button>
            </form>
        </div>
    </div>
</section>