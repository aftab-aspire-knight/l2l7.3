
<form id="edit-invoice" >
    <input type="hidden" name="franchise_id" id="franchise_id" value="<?php echo $franchise_id ?>" />
    <input type="hidden" name="pickup_time" id="pickup_time" value="" />
    <input type="hidden" name="delivery_time" id="delivery_time" value="" />
    <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
    <input type="hidden" name="referrer" id="referrer" value="<?php echo $referrer ?>" />

    <input type="hidden" id="old_pickup_date" value="<?php echo $order["PickupDate"] ?>" />
    <input type="hidden" id="old_pickup_time" value="<?php echo $order["PickupTime"] ?>" />
    <input type="hidden" id="old_delivery_date" value="<?php echo $order["DeliveryDate"] ?>" />
    <input type="hidden" id="old_delivery_time" value="<?php echo $order["DeliveryTime"] ?>" />

    <script>
        var services = <?php echo json_encode($services) ?>;
    </script>

    <div class="py-3 py-md-4 bg-light">
        <div class="container">
            <div class="row">
                <?php if (!isset($is_admin_view)) { ?>
                    <div class="col-lg-4 col-xl-3 order-summary-container">


                        <div class="order-sticky">
                            <div class="order-summary py-3 px-3 px-xl-4 mb-lg-2 mb-1">
                                <h2 class="order-summary-heading pb-2 pb-md-2 mb-1">Order Summary</h2>

                                <div class="list-group list-group-flush">
                                    <div class="order-address-right list-group-item px-0 d-flex order-address-summary active">
                                        <span class="badge px-0 pt-1 mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info">
                                            <span>Address</span>
                                            <small id="right_order_address"><?php echo $order["BuildingName"] ?>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="order-select-items-right list-group-item px-0 d-flex active">
                                        <span class="badge px-0 pt-1 mr-2 "><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info media-body">
                                            <span>Item Selection </span>


                                            <?php if (count($services) > 0) { ?>
                                                <div class="mini-cart-right ">
                                                    <?php
                                                    foreach ($services as $key => $service) {
                                                        ?>
                                                        <div class="form-row mx-0 cart-service-<?php echo $key ?>">
                                                            <div class="col-7 cart-name"><?php echo $service["title"] ?></div>
                                                            <div class="col-1 cart-qty cart-qty-service-<?php echo $key ?>"><?php echo $service["quantity"] ?></div>
                                                            <div class="col-4 cart-price cart-price-service-<?php echo $key ?>"><?php echo $this->config->item('currency_symbol'); ?><?php echo $service["total"] ?></div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="mini-cart-right">No service added yet</div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="list-group-item px-0 d-flex order-collection-delivery-right active">
                                        <span class="badge px-0 pt-1  mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info">
                                            <span>Collection &amp; Delivery </span>
                                            <small class="collection">Collection: <span  <?php if ($edit_pickup == true) { ?> id="collection_pickup_date" <?php } ?>><?php echo date("D, d F", strtotime($order["PickupDate"])) ?></span> <span <?php if ($edit_pickup) { ?> id="collection_pickup_time" <?php } ?> ><?php echo $order["PickupTime"] ?></span></small>
                                            <small class="collection">Delivery: <span id="collection_delivery_date"><?php echo date("D, d F", strtotime($order["DeliveryDate"])) ?></span> <span id="collection_delivery_time"><?php echo $order["DeliveryTime"] ?></span></small>
                                        </div>
                                    </div>
                                    <div class="list-group-item px-0 d-flex login-register-right  active  ">
                                        <span class="badge px-0 pt-1 mr-2"><i class="fa fa-check-circle"></i></span>
                                        <div class="order-summary-list-info contact-detail-right">
                                            <span>Contact Details </span>
                                            <small class=" contact-detail">Name: <span id="contact_name"><?php echo $member["FirstName"] ?> <?php echo $member["LastName"] ?></span></small>
                                            <small class=" contact-detail ">Email: <span id="contact_email"><?php echo $member["EmailAddress"] ?></span></small>
                                            <small class=" contact-detail">Phone: <span id="contact_phone">+<?php echo $member["CountryCode"] ?><?php echo $member["Phone"] ?></span></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-minimum form-row pb-2 py-md-1">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Minimum Order Value <?php echo $this->config->item('currency_symbol'); ?><span id="item_minimum_amount"><?php echo numberformat($franchise["MinimumOrderAmount"]); ?></span> </span>
                                    </div>
                                </div>
                                <div class="order-services-right form-row" style="">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Services Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-services-total"> <?php echo $this->config->item('currency_symbol'); ?><span id="item_services_price"><?php echo $order["ServicesTotal"] ?></span></span>
                                    </div>
                                </div>
                                <div class="order-discount-right form-row" <?php if ($order["DiscountType"] == "None") { ?> style="display: none" <?php } ?>>
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text discount-right-text">Discount Total(<strong><?php echo $order["DiscountCode"] ?></strong>)</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-discount-amount"> <?php echo $this->config->item('currency_symbol'); ?><span id="item_discount_price"><?php echo $order["DiscountTotal"] ?></span></span>
                                    </div>
                                </div>
                                <div class="order-preferences-right form-row" <?php if ($order["PreferenceTotal"] == "0.00") { ?> style="display: none" <?php } ?> >
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text"> Preferences Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4 text-right">
                                        <span class="order-preferences-amount"> <?php echo $this->config->item('currency_symbol'); ?><span id="item_preferences_price"><?php echo $order["PreferenceTotal"] ?></span></span>
                                    </div>
                                </div>
                                <div class="order-minimum form-row pb-2 py-md-1">
                                    <div class="col-8 col-sm-8">
                                        <span class="order-minimum-text">Grand Total</span>
                                    </div>
                                    <div class="col-4 col-sm-4">
                                        <span class="order-amount"> <?php echo $this->config->item('currency_symbol'); ?><span id="item_total_price"><?php echo $order["GrandTotal"] ?></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                <?php } ?>
                <div class="col-lg-8 col-xl-9">
                    <div class="bg-white p-3 p-lg-5 rounded-xl">
                        <div id="collection_errors" style="display: none"></div>
                        <div class="order-collection-delivery steps" data-id= "collection_next_button">
                            <div class="order-collection order-timeslots mb-2 mb-md-4">
                                <h2 class="order-heading py-2 py-md-3">Collection time</h2>
                                <div class="row align-items-center">
                                    <div class="col-lg-3 col-12 text-center">
                                        <img width="180" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'collection.svg' ?>" />
                                    </div>
                                    <div class="col-lg col-md-6 col-12">
                                        <div class="form-group mb-3 mb-md-4 custom-select2">
                                            <label>Date</label>

                                            <?php if ($edit_pickup == true) { ?>
                                                <select class="custom-select custom-select-lg select2" onchange="getPickupTimes()" id="pickup_date" name="pickup_date"></select>
                                            <?php } else { ?>
                                                <input type="text" readonly="readonly" style="background: #0e0e0e0e" class="custom-select custom-select-lg" id="pickup_date" name="pickup_date" value="<?php echo $order["PickupDate"] ?>" />
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <div class="col-lg col-md-6 col-12">
                                        <div class="form-group mb-3 mb-md-4">

                                            <label>Time</label>
                                            <?php if ($edit_pickup == true) { ?>
                                                <div id="pickup_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none;">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time=""><i class="fa fa-leaf" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <input readonly="readonly" type="text" style="background: #0e0e0e0e" class="custom-select custom-select-lg" value="<?php echo $order["PickupTime"] ?>" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="order-delivery order-timeslots mb-md-4">
                                <h2 class="order-heading py-2 py-md-3">Delivery time</h2>
                                <div class="row align-items-center">
                                    <div class="col-lg-3 col-12 text-center">
                                        <img width="180" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'delivery.svg' ?>" />
                                    </div>
                                    <div class="col-md col-12">
                                        <div class="form-group mb-3 mb-md-4 custom-select2">
                                            <label>Date</label>
                                            <?php if ($edit_delivery == true) { ?>
                                                <select onchange="getDeliveryTimes()" id="delivery_date" name="delivery_date" class="custom-select custom-select-lg select2">
                                                </select>
                                            <?php } else { ?>
                                                <input type="text" readonly="readonly" style="background: #0e0e0e0e" class="custom-select custom-select-lg" id="delivery_date" name="delivery_date" value="<?php echo $order["DeliveryDate"] ?>" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-md col-12">
                                        <div class="form-group mb-3 mb-md-4">
                                            <label>Time</label>
                                            <?php if ($edit_delivery == true) { ?>
                                                <div id="delivery_time_div" class="select-dropdown">
                                                    <span class="selected"><i class="fa fa-leaf" aria-hidden="true"></i></span>
                                                    <i class="fa fa-caret-down select-arrow" aria-hidden="true"></i>
                                                    <div class="dropdown-body" style="display: none;">
                                                        <span class="time-options eco-friendly">Eco-friendly routes</span>

                                                        <span class="time-slot" data-time=""><i class="fa fa-leaf" aria-hidden="true"></i>
                                                        </span>


                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <input readonly="readonly" type="text" style="background: #0e0e0e0e" class="custom-select custom-select-lg" value="<?php echo $order["DeliveryTime"] ?>" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-0 next-previous-buttons">
                                <div class="row">
                                    <div class="col-12 offset-md-6 col-md-6 text-center text-md-right">
                                        <a href="<?php echo $referrer ?>" class="btn btn-primary rounded-pill text-uppercase next">Back</a>
                                        <?php if ($edit_delivery == true) { ?>
                                            <button type="button" class="btn btn-dark rounded-pill text-uppercase px-4 next collection_save_button">Save</button>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <br clear="all"/>
                    <div class="bg-white p-3 p-lg-5 rounded-xl">
                        <div class="order-select-items"></div>
                        <div class="order-select-preferences mb-2 mb-md-4 steps d-none" >

                            <h2 class="order-heading py-2 py-md-3"><?php echo $this->config->item('front_member_laundry_first_heading_text') ?></h2>

                            <?php
                            $area_html = "";
                            $down_area_html = "";
                            foreach ($preference_records as $record) {

                                $current_area = "";
                                $current_area .= '<div class="col-12 col-md-6">';
                                $current_area .= '<div class="form-group">';
                                $current_area .= '<label>' . $record['Title'] . ' <span class="red-color">*</span></label>';
                                $current_area .= '<select class="form-control form-control-lg preferences-list" id="cmb_' . str_replace("-", "_", CreateAccessURL($record['Title'])) . '" name="preferences_list[]">';
                                foreach ($record['child_records'] as $rec) {
                                    //d($member_preference_records,1);
                                    $selected_preference = "";
                                    if (in_array($rec['PKPreferenceID'], $preferences)) {
                                        $selected_preference = " selected=selected";
                                    }

                                    if (!empty($rec['Price'])) {
                                        $price = '(' . $this->config->item('currency_symbol') . $rec['Price'] . ")";
                                    } else {
                                        $price = "";
                                    }
                                    $current_area .= '<option value="' . $rec['PKPreferenceID'] . '"' . $selected_preference . '>' . $rec['Title'] . $price . '</option>';
                                }
                                $current_area .= '</select>';
                                $current_area .= '</div>';
                                $current_area .= '</div>';
                                if ($record['Title'] == "Starch on Shirts") {
                                    $down_area_html .= $current_area;
                                } else {
                                    $area_html .= $current_area;
                                }
                            }
                            echo '<div class="row">';
                            echo $area_html;
                            echo '</div>';
                            echo '<hr/>';
                            echo '<h3>' . $this->config->item('front_member_laundry_second_heading_text') . '</h3>';
                            echo '<div class="row">';
                            echo $down_area_html;
                            echo '<div class="col-12 col-md-6">';
                            echo '<div class="form-group">';
                            //echo '<label>Account Notes</label>';
                            // echo '<textarea class="form-control form-control-lg" id="account_notes" name="account_notes" rows="5">' . (isset($member_detail['AccountNotes']) ? $member_detail['AccountNotes'] : '') . '</textarea>';
                            echo '</div>';
                            echo '</div>';
                            echo '<div class="col-12 col-md-6">';
                            echo '<div class="form-group">';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                            ?>


                        </div>

                        <div class="row next-previous-buttons">
                            <div class="col-6">
                                <a href="javascript:void(0);" class="btn btn-primary rounded-pill text-uppercase items-cancel">Back</a>
                            </div>
                            <?php if ($edit_pickup == true) { ?>
                                <div class="col-6 text-right">
                                    <button type="button" class="btn btn-dark rounded-pill px-4 text-uppercase save-items" >Save</button>
                                </div>
                            <?php } ?>
                            <br clear="all"/><br clear="all"/>
                            <div class="col-12">
                                <div id="update_errors" style="display: none"></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="modalMessage" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-primary border-light">
                <h6 class="modal-title font-weight-bold" id="discountModalCenterTitle">Order Updated</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="order-form">
                    <div class="form-group mb-0" id="modal_description">
                    </div>
                </div>
            </div>
            <div class="modal-footer border-light">
                <button type="button" data-dismiss="modal" class="btn btn-dark btn-block btn-lg apply-discount">Close</button>
            </div>
        </div>
    </div>
</div>