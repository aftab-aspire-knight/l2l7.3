<div class="order-category-list mb-3 mb-md-4">
    <h2 class="order-heading py-2 py-md-3">Select your items</h2>
    <div id='items_errors'></div>
    <div class="owl-carousel owl-theme owl-drag order-category-carousel">
        <?php
        if (isset($category_records)) {
            $count = 0;
            foreach ($category_records as $record) {
                echo '<div class="item">';
                echo '<div class="category-item';
                if ($count == 0) {
                    echo ' mixitup-control-active';
                }
                echo '" data-filter=".category-' . $count . '">';
                echo '<i class="icon l2l-' . $record['DesktopIconClassName'] . '"></i>';
                echo '<span class="category-label">' . $record['Title'] . '</span>';
                echo '</div>';
                echo '</div>';
                $count += 1;
            }
        }
        ?>
    </div>
</div>
<div class="row order-list-items" >
    <?php
    if (isset($category_records)) {
        $count = 0;
        foreach ($category_records as $record) {
            foreach ($record['service_records'] as $rec) {
                echo '<div class="mix category-' . $count . ' col-md-6 col-12 " id="service_item_' . $rec['PKServiceID'] . '" data-id="' . $rec['PKServiceID'] . '">';
                echo '<div class="order-list-item px-3 py-3 mb-2 mb-md-4">';
                echo '<div class="row no-gutters">';
                echo '<div class="col-12 col-xl-6 pr-lg-3 d-none d-md-block">';
                $desktop_image_name = $this->config->item('front_dummy_image_name');
                $desktop_image_path = $this->config->item('front_dummy_image_url');
                $mobile_image_name = $this->config->item('front_dummy_mobile_service_image_name');
                if (!empty($rec['DesktopImageName'])) {
                    //if (file_exists($this->config->item("dir_upload_service") . $rec['DesktopImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['DesktopImageName'])) {
                    $desktop_image_name = $rec['DesktopImageName'];
                    $desktop_image_path = $this->config->item("front_service_folder_url") . $rec['DesktopImageName'];
                    //}
                }

                if (!empty($rec['MobileImageName'])) {
                    if (file_exists($this->config->item("dir_upload_service") . $rec['MobileImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['MobileImageName'])) {
                        $mobile_image_name = $rec['MobileImageName'];
                    }
                }
                /* Service discout price franchise wise starts */
                if (isset($rec['DiscountPercentage']) && $rec['DiscountPercentage'] > 0) {
                    $actualPrice = $rec['Price'];
                    $DiscountPercentage = $rec['DiscountPercentage'];
                    $tmpPrice = ($DiscountPercentage / 100) * $actualPrice;
                    $finalPrice = $actualPrice - $tmpPrice;
                } else {
                    $finalPrice = $rec['Price'];
                }
                $finalPrice = number_format($finalPrice, 2);
                /* ends */

                echo '<a class="service-image add-item-cart action-item" href="#" data-id="' . $rec['PKServiceID'] . '" data-title="' . $rec['Title'] . '" data-price="' . $finalPrice . '" data-desktop-image="' . $desktop_image_name . '" data-mobile-image="' . $mobile_image_name . '" data-category-id="' . $record["PKCategoryID"] . '"   data-category="' . $record["Title"] . '"   data-package="' . $rec["Package"] . '" data-preference-show="' . $rec["PreferencesShow"] . '" data-popup-message="' . $record["PopupMessage"] . '">';
                echo '<img data-img="' . $desktop_image_path . '" class="img-fluid" src="' . $desktop_image_path . '">';
                echo '</a>';
                echo '</div>';
                echo '<div class="col-12 col-xl-6">';
                echo '<h2 class="service-description">' . $rec['Title'] . $rec["Package"] . '</h2>';
                if (!empty($rec['Content']) && strlen($rec['Content']) > 5) {
                    echo '<div class="service-option">' . strip_tags($rec['Content']) . '</div>';
                } else {
                    echo '<p>&nbsp;</p>';
                }

                echo '<div class="service-price">';
                if (isset($rec['DiscountPercentage']) && $rec['DiscountPercentage'] > 0) {
                    echo '<span class="service-price-orignal">' . $this->config->item('currency_sign') . $rec['Price'] . '</span>  ' . $this->config->item('currency_sign') . $finalPrice;
                } else {
                    echo $this->config->item('currency_sign') . $finalPrice;
                }
                echo '</div>';
                //echo '<span class="item-price">' . $this->config->item('currency_sign') . $rec['Price'] .'</span>';
                echo '<div class="service-cart">';
                echo '<button class="plus add-item-cart action-item" data-id="' . $rec['PKServiceID'] . '" data-title="' . $rec['Title'] . '" data-price="' . $finalPrice . '" data-desktop-image="' . $desktop_image_name . '" data-mobile-image="' . $mobile_image_name . '" data-category-id="' . $record["PKCategoryID"] . '"  data-category="' . $record["Title"] . '" data-package="' . $rec["Package"] . '" data-preference-show="' . $rec["PreferencesShow"] . '" data-popup-message="' . $record["PopupMessage"] . '" data-type="add">+</button>';
                echo '<button class="minus remove-item-cart disabled action-item" data-id="' . $rec['PKServiceID'] . '" data-title="' . $rec['Title'] . '" data-price="' . $finalPrice . '" data-desktop-image="' . $desktop_image_name . '" data-mobile-image="' . $mobile_image_name . '" data-category-id="' . $record["PKCategoryID"] . '"  data-category="' . $record["Title"] . '" data-package="' . $rec["Package"] . '" data-preference-show="' . $rec["PreferencesShow"] . '" data-popup-message="' . $record["PopupMessage"] . '"       data-type="remove" >-</button>';
                if (isset($rec['DiscountPercentage']) && $rec['DiscountPercentage'] > 0) {
                    echo '<span class="service-offers badge badge-primary badge-pill mt-1 ml-2">' . $rec['DiscountPercentage'] . '% </span>';
                }

                $qty = isset($services[$rec['PKServiceID']]) ? $services[$rec['PKServiceID']] : "0";

                echo '<span class="qty in-cart-qty ml-2 " id="qty' . $rec['PKServiceID'] . '" >' . $qty . '</span>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            $count += 1;
        }
    }
    ?>
</div>