<?php
if (isset($order_records)) {
    foreach ($order_records as $record) {
        echo '<div class="mb-3 bg-white rounded-xl order-item">';
        echo '<ul class="list-order list-group list-group-horizontal-sm">';
        echo '<li class="col-sm-2 list-group-item bg-dark border-0 rounded-left text-white"><span class="text-white d-inline-block pr-3 pb-2">ORDER ID</span><span id="order_id" class="text-white"> ';
        echo $record['InvoiceNumber'] . '</span><br />';
        $is_edit_button_show = true;
        if ($record['OrderStatus'] == "Pending" || $record['OrderStatus'] == "Processed") {
            $delivery_time_array = explode("-", $record['DeliveryTime']);
            //echo $pDate = date("Y-m-d",strtotime($record['PickupDate']));

            //echo $record['PickupDate'] . ' ' . $delivery_time_array[0] . ':00';

            $strDeliveryDate = strtotime($record['DeliveryDate'] . ' ' . $delivery_time_array[0] . ':00');
            //date('Y-m-d H:i:s',$pickupDate);
            $strNow = time();
            $hour = ($strDeliveryDate - $strNow) / (60 * 60);

            if ($hour < 4) {
                $is_edit_button_show = false;
            } else {
                // if ($hour <= $edit_order_hour_limit) {
                $is_edit_button_show = true;
                // }
            }
        }
        if ($record['OrderStatus'] == "Pending") {
            if ($is_edit_button_show) {
                echo '<a href="' . base_url("orders/edit") . '/' . $record['PKInvoiceID'] . '" class="px-1 text-white add-items"><i class="far fa-edit"></i> Edit</a>';
            }
            echo '<a href="' . base_url($this->config->item('front_member_order_cancel_page_url')) . '" data-id="' . $record['InvoiceNumber'] . '" class="px-1 text-white cancel-order"><i class="far fa-eye-slash"></i> Cancel</a>';
        } else if ($record['OrderStatus'] == "Processed") {
            if ($is_edit_button_show) {
                echo '<a href="' . base_url("orders/edit") . '/' . $record['PKInvoiceID'] . '" class="px-1 text-white add-items"><i class="far fa-edit"></i> Edit</a>';
            }
        } else if ($record['OrderStatus'] == "Cancel") {
            echo '<a href="' . base_url($this->config->item('front_member_reorder_page_url')) . '" data-id="' . $record['InvoiceNumber'] . '" class="px-1 text-white reorder"><i class="fas fa-ellipsis-h"></i> Reorder</a>';
        }
        echo '<a href="' . base_url($this->config->item('front_member_order_detail_page_url') . '/' . $record['InvoiceNumber']) . '" class="px-1 text-white" id="btn_order_detail"><i class="fas fa-eye"></i> View</a>';
        echo '</li>';
        echo '<li class="col-sm-2 list-group-item border-0"><span>CREATED</span><span id="created_date_time">' . $record['CreatedDate'] . '<br />' . $record['CreatedTime'] . '</span></li>';

        echo '<li class="col-sm-2 list-group-item border-0"><span>SERVICES</span>';
        echo '<ul class="list-unstyled" id="service_listing">';
        $service_count = 0;
        foreach ($record['service_records'] as $rec) {
            echo '<li>' . $rec['Title'] . '</li>';
            $service_count += 1;
            if ($service_count > 0) {
                echo '<li>More ...</li>';
                break;
            }
        }
        echo '</ul>';
        echo '</li>';
        echo '<li class="col-sm-1 list-group-item border-0"><span>TOTAL</span><span class="grandtotal" id="grand_total">' . $record['Currency'] . $record['GrandTotal'] . '</span></li>';
        echo '<li class="col-sm list-group-item border-0"><span>COLLECTION TIME</span><span id="pick_date_time">' . $record['PickupTime'] . '<br />' . $record['PickupDate'] . '</span></li>';
        echo '<li class="col-sm list-group-item border-0 rounded-right"><span>DELIVERY TIME</span><span id="delivery_date_time">' . $record['DeliveryTime'] . '<br />' . $record['DeliveryDate'] . '</span></li>';

        echo '</ul>';
        echo '</div>';
    }
} else {
    echo '<div class="alert alert-danger"><p>No Order Records</p></div>';
}
?>
<div class="well well-sm text-center text-uppercase hide-area" id="order_loader"><i class="fa fa-spinner fa-spin"></i>Loading</div>