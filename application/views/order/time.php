<section class="order-process">
    <div class="icon-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Choose your collection & delivery times</h2>
                    <h4>Time Slots Available to you are shown below.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="light-gray tb-pad">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center" id="time_loader">
                    Loading <i class="l2l-laundry-setting l2l-spin"></i>
                </div>
                <div class="col-sm-12 hide-area" id="time_select_area">
                    <div class="col-sm-6">
                        <div class="dt-picker">
                            <h4>Pick Up</h4>
                            <p id="selected_pickup_time"></p>
                            <div class="custom-calender">
                                <div class="date-tabs">
                                    <ul id="pick_date_slots">
                                    </ul>
                                </div>
                                <div class="timeslots" id="pick_time_slots">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="dt-picker">
                            <h4>Delivery</h4>
                            <p id="selected_delivery_time"></p>
                            <div class="custom-calender">
                                <div class="date-tabs">
                                    <ul id="delivery_date_slots">
                                    </ul>
                                </div>
                                <div class="timeslots" id="delivery_time_slots">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ul class="legend">
                            <li class="not-available"><i></i><span>Not Available</span></li>
                            <li class="available"><i></i><span>Available</span></li>
                            <li class="selected-slot"><i></i><span>Selected Time Slot</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>