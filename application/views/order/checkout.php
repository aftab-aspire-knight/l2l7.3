<style>
    /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
    .StripeElement {
        box-sizing: border-box;

        height: 40px;

        padding: 10px 12px;

        border: 1px solid transparent;
        border-radius: 4px;
        background-color: white;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>
<section class="order-process" id="check_out_page_area">
    <div class="light-gray ">
        <div class="container">
            <div class="row" id="ch_area">
                <div class="col-md-6">
                    <div class="order-details">
                        <h3>Time Information</h3>
                        <a href="<?php echo base_url($this->config->item('front_address_selection_page_url')) ?>" class="edit"><i class="fa fa-pencil"></i> Edit</a>
                        <dl class="dl-horizontal">
                            <dt>Pick Up Date Time</dt><dd id="pick_date_time_area"><?php echo $pickup_time ?> <?php echo date("d F, Y", strtotime($pickup_date)) ?></dd><hr/>
                            <dt>Delivery Date Time</dt><dd id="delivery_date_time_area"><?php echo $delivery_time ?> <?php echo date("d F, Y", strtotime($delivery_date)) ?></dd>
                        </dl>
                    </div>

                    <div class="order-details">
                        <h3>Your Basket</h3>
                        <a href="<?php echo base_url($this->config->item('front_select_item_page_url')) ?>" class="edit"><i class="fa fa-pencil"></i> Edit</a>
                        <ul class="cart-listing" id="check_out_cart_listing">

                        </ul>
                    </div>

                    <div class="order-details custom-forms" id="credit_card_order_area">
                        <h3>Credit Card Details</h3>
                        <input type="hidden" id="c_valid" >
                        <input type="hidden" id="stripe_key" value="<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>" />

                        <div id="card_list_area">
                            <?php if (isset($member_card_records)) { ?>
                                <div class="form-group col-sm-12">
                                    <label class="col-sm-4 control-label">Select Card <span class="red-color">*</span></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="card_list" id="card_list">
                                            <option value="">Select</option>
                                            <option value="0">Add New</option>
                                            <?php
                                            foreach ($member_card_records as $rec) {

                                                if (strlen($rec['Number']) != 4) {
                                                    continue;
                                                }

                                                echo '<option value="' . $rec['PKCardID'] . '||' . $rec['Title'] . '||' . $rec['Name'] . '||' . $rec['Number'] . '||' . $rec['NumberMasked'] . '||' . $rec['Month'] . '||' . $rec['Year'] . '||' . $rec['Code'] . '||' . $rec['CodeMasked'] . '">' . $rec['Title'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="form-group col-sm-10 col-sm-offset-1" id="card-element">

                        </div>
                        <br clear="all" />

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="order-details">
                        <h3>Address Information</h3>
                        <a href="<?php echo base_url($this->config->item('front_address_selection_page_url')) ?>" class="edit"><i class="fa fa-pencil"></i> Edit</a>
                        <dl class="dl-horizontal" id="address_area">
                        </dl>
                    </div>
                    <div class="order-details">
                        <form class="form-horizontal custom-forms" id="frm_checkout" action="<?php echo base_url('order-checkout') ?>">
                            <div id="card-errors"></div>
                            <h3>Confirm Order</h3>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Order Notes This Order Only</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" placeholder="Order Notes This Order Only" id="order_notes" name="order_notes"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Payment Mode</label>
                                <div class="col-sm-8">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="radio" name="payment_mode" id="payment_mode"checked> Stripe
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <?php
                                    $rand = rand();
                                    $_SESSION['rand'] = $rand;
                                    ?>
                                    <input type="hidden" value="<?php echo $rand; ?>" id="randcheck" />
                                    <label>
                                        <input type="checkbox" checked id="chk_news"> I want to receive exclusive <a target="_blank" href="<?php echo base_url(); ?>">Love2Laundry</a> news & special offers 
                                    </label>
                                </div>
                            </div>														
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <label>
                                        <input type="checkbox" id="chk_terms"> Accept our <a target="_blank" href="<?php echo base_url($this->config->item('front_term_page_url')) ?>">Terms and Conditions</a>
                                    </label>
                                </div>
                            </div>
                            <p><strong><span class="red-color">*</span></strong>Please note the final itemisation will be done by the facility. The total invoice amount may vary</p>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-primary btn-lg btn-block text-uppercase" id="btn_place_order">Place Order Now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<script src="https://js.stripe.com/v3/"></script>
<script>
    // Create a Stripe client.
    var stripe = Stripe('<?php echo isset($stripe_js_key) ? $stripe_js_key : '' ?>');

// Create an instance of Elements.
    var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
    var style = {

        base: {
            iconColor: '#c4f0ff',
            color: '#000000',
            fontWeight: 500,
            fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
            fontSize: '16px',
            fontSmoothing: 'antialiased',

            ':-webkit-autofill': {
                color: '#fce883',
            },
            '::placeholder': {
                color: '#87BBFD',
            },
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

// Create an instance of the card Element.
    var card = elements.create('card', {hidePostalCode: true, iconStyle: 'solid', style: style});

// Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

// Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });




</script>