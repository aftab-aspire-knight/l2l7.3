<?php
$postalCodeType = $this->config->item("postal_code_type");
$show_prices_pdf = $this->config->item("show_prices_pdf");
$show_prices_pdf=false;
?>


<div class="py-3 py-md-4" id="body_scroll" data-spy="scroll" data-target="#sidebar">
    <div class="container">
        <div class="row">

            <div class="col-lg-8">
                <h1 class="ff-gothamblack">Dry Cleaning &amp; Laundry Prices</h1>
                <p>At Love2Laundry we make doing your laundry simple. We can save you time so you can enjoy doing the things you love. Our dry cleaning prices are simple and affordable. Check out our price list below:</p>

                <div class="bg-dark rounded-xl text-white p-2 p-md-4 my-3">
                    <div class="form-row align-items-center">
                        <div class="col-md-6 py-2 py-md-0 font-weight-bold align-self-center">
                            <img class="img-fluid mr-2" src="<?php echo $this->config->item('front_asset_image_folder_url') . '24h.svg' ?>" alt="Express 24Hour Service" />
                            <span class="d-inline-block">Express 24Hour Service</span>
                        </div>
                        <?php if ($show_prices_pdf == true) { ?>
                            <div class="col-md-6 py-2 py-md-0 font-weight-bold text-right">
                                <a class="text-white" href="<?php echo base_url() . "uploads/prices/Pricing.pdf" ?>"><img class="img-fluid mr-2" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'pdf.png' ?>" alt="Download Prices PDF" />
                                    <span class="d-inline-block">Download Prices</span>
                                </a>
                                <?php /*
                            <img class="img-fluid mr-2" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'collection-return.png' ?>" alt="Contactless Collection & Return" /> 
                            <span class="d-inline-block">Contactless Collection & Return </span>
                            */ ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="order-services"></div>
                <div class="bg-primary rounded-xl p-3 my-4">
                    <div class="form-row align-items-center text-center">
                        <div class="col-md-6"><img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'Trustpilot_logo_black.png' ?>" alt="Trustpilot Reviews Love2Laundry" /></div>
                        <div class="col-md-6">Can’t find your item? <strong class="ff-gothamblack">Get in touch.</strong></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 price-estimate-container modal-dialog-scrollable">
                <form id="form-services">
                    <input type="hidden" name="id" id='id' value="">
                    <input type="hidden" name="postal_code" id='postal_code' value="<?php echo $this->config->item("default_postal_code"); ?>">
                    <input type="hidden" name="amount" id='amount' value="0.00">
                    <div class="bg-primary py-2 py-lg-3 text-center rounded-xl px-3 px-lg-4 my-2 mb-md-4">
                        <h3 class="font-weight-bold">I'm too busy</h3>
                        <a href="<?php echo base_url('booking'); ?>" class="btn btn-dark rounded-pill my-2" id="btn_cart_item_later" onclick="ga('send', { 'hitType': 'event', 'eventCategory': 'button', 'eventAction': 'click', 'eventLabel': 'im too busy', 'nonInteraction':true]);">Skip this step <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="we'll collect your bag and bill you after" data-original-title=""></i></a>
                        <div class="pt-1 h6">JUST SEND ME A BAG AND I’LL FILL IT</div>

                    </div>
                    <div class="bg-light price-summary px-3 px-lg-4 mb-md-4 sidebar">
                        <div class="d-flex align-items-center justify-content-between pt-4 pb-3">
                            <div>
                                <h3 class="h4">Price Estimator</h3>
                            </div>
                            <div>
                                <a href="<?php echo base_url() . "pricing" ?>" class="btn btn-sm btn-dark rounded-pill">Reset</a>
                                <button type="button" class="btn btn-sm btn-dark rounded-pill d-inline-block d-md-none close" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                        <p class="mb-3">Use this calculator to get a rough total of your order. No need to list the actual items when you schedule a collection.</p>
                        <div class="cart-list py-3" id="cart_service_right">

                        </div>

                        <div class="price-estimate form-row py-3">
                            <div class="col-6 col-sm-8">
                                <span class="price-summary-text d-inline-block font-weight-bold py-2">Estimate</span>
                            </div>
                            <div class="col-6 col-sm-4">
                                <span class="price-estimate-amount ff-gothamblack"><?php echo $this->config->item('currency_symbol'); ?>0.00</span>
                            </div>
                        </div>
                        <button type="button" class="btn btn-lg btn-primary btn-block rounded-pill continue-order">Continue Order</button>
                        <div class="py-4">
                            <h6 class="font-weight-bold">Minimum order value is <?php echo $this->config->item('currency_symbol'); ?><span class="minimum-order-amount"><?php echo numberformat($this->config->item("minimum_order_value")) ?></span>.</h6>
                            <p>Please note that the final price may vary and it will be calculated after the cleaning process.</p>
                        </div>
                    </div>
                    <div class="bg-dark rounded-xl p-2 p-lg-4 my-2 my-lg-4 text-white text-center customer-offer">
                        <div class="ff-gothamblack display-4"><?php echo $this->config->item('discount_off'); ?> OFF</div>
                        <div class="h4 mb-0">All Laundry &amp; Dry Cleaning <span class="d-block h3">FOR NEW CUSTOMERS</span></div>
                    </div>
                    <div class="bg-light rounded-xl px-2 py-4 my-2 my-lg-4 text-center d-none d-md-block" id="downloadApp">
                        <div class="h5">Download our free mobile app</div>
                        <div class="my-3">
                            <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="140" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                            <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="140" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
                        </div>

                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
<div class="fixed-bottom d-block d-lg-none">
    <button class="btn btn-primary py-3 font-weight-bold rounded-0 btn-block btn-lg price-mobile">Next</button>
</div>
<!-- Modal -->

<div <?php if ($postalCodeType["title"] == "Area") { ?> style="display: none;" <?php } else { ?> class="modal fade" id="postalcodeModal" tabindex="-1" role="dialog" aria-labelledby="postalcodeModalCenterTitle" aria-hidden="true" <?php } ?>>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-primary border-light">
                <h6 class="modal-title font-weight-bold" id="postalcodeModalCenterTitle">Tell us about your location
                </h6>
            </div>
            <div class="modal-body">
                <div class="order-form">
                    <div id="postal_code_errors"></div>

                    <div class="form-group mb-0">
                        <label>Enter your <?php echo $postalCodeType["title"]; ?></label>
                    </div>
                    <div class="form-group mb-0" id="div_reg_address1">
                        <label>Code <span class="red-color">*</span></label>
                        <input type="text" value="<?php echo $this->config->item("default_postal_code"); ?>" placeholder="<?php echo $postalCodeType["placeholder"]; ?>" class="form-control form-control-lg" id="code" name="code" />
                    </div>
                </div>
            </div>
            <div class="modal-footer border-light">
                <button type="button" class="btn btn-dark btn-block btn-lg apply-postal-code">Apply</button>
            </div>
        </div>
    </div>
</div>