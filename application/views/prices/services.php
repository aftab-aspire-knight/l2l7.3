<div class="py-3 py-lg-4 mb-3 mb-md-4">
    <div id='items_errors'></div>
    <!--<div class="owl-carousel owl-prices-category owl-theme owl-drag">
        <?php
        if (isset($category_records)) {
            $count = 0;
            foreach ($category_records as $record) {
                echo '<div class="item">';
                echo '<div class="category-item text-center';
                if ($count == 0) {
                    echo ' mixitup-control-active ';
                }
                echo '" data-filter=".category-' . $count . '">';
                echo '<i class="icon l2l-' . $record['DesktopIconClassName'] . ' fa-lg d-table mx-auto p-2 mb-2"></i></i>';
                echo '<span class="category-label">' . $record['Title'] . '</span>';
                echo '</div>';
                echo '</div>';
                $count += 1;
            }
        }
        ?>
    </div>-->
    <div class="row" id="category-offset">
    <?php
        if (isset($category_records)) {
            $count = 0;
            foreach ($category_records as $record) {
                echo '<div class="col-4 col-md-2 py-3">';
                echo '<div class="category-item text-center';
                if ($count == 0) {
                    echo ' mixitup-control-active ';
                }
                echo '" data-filter=".category-' . $count . '">';
                echo '<i class="icon l2l-' . $record['DesktopIconClassName'] . ' fa-lg d-table mx-auto p-2 mb-1"></i></i>';
                echo '<span class="category-label">' . $record['Title'] . '</span>';
                echo '</div>';
                echo '</div>';
                $count += 1;
            }
        }
        ?>
    </div>
    
</div>


<div class="order-list-items">
    <?php
    if (isset($category_records)) {
        $count = 0;
        foreach ($category_records as $record) {
            ?>

            <div class="bg-light price-item mix category-<?php echo $count; ?> py-3 py-lg-4 my-3">
                <h2 class="h3 border-bottom pb-3 px-4"><i class="icon l2l-<?php echo $record['DesktopIconClassName']; ?> fa-lg"></i> <?php echo $record['Title']; ?></h2>
                <ul class="list-group list-group-flush">
                    <?php
                    foreach ($record['service_records'] as $rec) {

                        $desktop_image_name = $this->config->item('front_dummy_image_name');
                        $desktop_image_path = $this->config->item('front_dummy_image_url');
                        $mobile_image_name = $this->config->item('front_dummy_mobile_service_image_name');
                        if (!empty($rec['DesktopImageName'])) {
                            //if (file_exists($this->config->item("dir_upload_service") . $rec['DesktopImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['DesktopImageName'])) {
                            $desktop_image_name = $rec['DesktopImageName'];
                            $desktop_image_path = $this->config->item("front_service_folder_url") . $rec['DesktopImageName'];
                            //}
                        }

                        if (!empty($rec['MobileImageName'])) {
                            if (file_exists($this->config->item("dir_upload_service") . $rec['MobileImageName']) && file_exists($this->config->item("dir_upload_service_thumb") . $rec['MobileImageName'])) {
                                $mobile_image_name = $rec['MobileImageName'];
                            }
                        }
                        /* Service discout price franchise wise starts */
                        if (isset($rec['DiscountPercentage']) && $rec['DiscountPercentage'] > 0) {
                            $actualPrice = $rec['Price'];
                            $DiscountPercentage = $rec['DiscountPercentage'];
                            $tmpPrice = ($DiscountPercentage / 100) * $actualPrice;
                            $finalPrice = $actualPrice - $tmpPrice;
                        } else {
                            $finalPrice = $rec['Price'];
                        }
                        $finalPrice = number_format($finalPrice, 2);
                        ?>
                        <li class="list-group-item  d-flex no-gutters align-items-center py-3 px-4">
                            <div class="col-lg-8">
                                <div><?php echo $rec['Title']; ?></div>
                                <div class="list-group-item-desc">
                                    <?php
                                        if (!empty($rec['Content']) && strlen($rec['Content']) > 5) {
                                            echo  strip_tags($rec['Content']) ;
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-4 text-right">
                                <div class="d-inline-block font-weight-normal"><?php echo $this->config->item('currency_sign') . $finalPrice ?></div> 
				<?php echo '<span class="qty in-cart-qty ml-2 " id="qty' . $rec['PKServiceID'] . '" >0</span>';?>				
                                <?php echo '<button type="button" class="btn btn-dark plus ml-2 action-item" data-id="' . $rec['PKServiceID'] . '" data-title="' . $rec['Title'] . '" data-price="' . $finalPrice . '" data-desktop-image="' . $desktop_image_name . '" data-mobile-image="' . $mobile_image_name . '" data-category-id="' . $record["PKCategoryID"] . '"   data-category="' . $record["Title"] . '"   data-package="' . $rec["Package"] . '" data-preference-show="' . $rec["PreferencesShow"] . '" data-popup-message="' . $record["PopupMessage"] . '" data-type="add">+</button>'; ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php
            $count++;
        }
        ?>


    <?php } ?>
</div>

