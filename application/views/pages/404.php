<div class="signin align-items-center d-flex">
<div class="error404 py-3 py-md-5 mx-auto">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . '404-wash-laundry.png' ?>" alt="Error 404 Not Found" />
			</div>
			<div class="col-lg-7">
				<h1 class="ff-gothamblack my-3 display-4">Error 404</h1>
				<p>Oops, something went wrong. Let’s get you back on track.</p>
				<p class="mt-1 font-weight-bold">In the meantime, try our service at a different location</p>
				<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
					<div class="place-order-field">
						<input type="text" name="post_code" id="post_code" placeholder="enter postcode" class="form-control border border-primary">
						<button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Order Now</button>
					</div>
				</form>
				<?php if(!isset($is_member_login)) {?>
				<p class="find-location text-left">Already ordered with us? <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>">Click here!</a></p>
				<?php }?>
			</div>
		</div>
	</div>
</div>
</div>
<?php /*<div class="signin align-items-center d-flex">
<div class="error404 py-3 py-md-5 mx-auto">
	<div class="container">
		<div class="row align-items-center text-center">
			<div class="col-lg-3">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'sad-women.png' ?>" alt="" />
			</div>
			<div class="col-lg-6">
				<h1 class="ff-gothamblack my-3 display-4">Error 404</h1>
				<p>The page you are looking for was moved, removed, renamed or might never existed.</p>
				<p class="mt-5 font-weight-bold">In the meantime, try our service at a different location</p>
				<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
					<div class="place-order-field">
						<input type="text" name="post_code" id="post_code" placeholder="enter postcode" class="form-control border border-primary">
						<button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Order Now</button>
					</div>
				</form>
				<a id="btn_home" href="<?php echo base_url()?>" class="btn btn-dark btn-lg rounded-pill px-5">Go Home</a>
			</div>
			<div class="col-lg-3">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'upset-men.png' ?>" alt="" />
			</div>
		</div>
	</div>
</div>
</div> */ ?>