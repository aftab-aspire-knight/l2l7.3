<div class="page-section bg-light py-3 py-md-4">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-8 order-2 order-md-1">
                <h1 class="ff-gothamblack mb-3">On Demand Laundry <br>Service for Airbnb Hosts</h1>
                <h2 class="h4 font-weight-bold">Hotel Quality Linen Cleaning Service in London</h2>
                <?php $this->load->view('includes/order_now/'. SERVER); ?>
                
                <?php if (!isset($is_member_login)) { ?>
                    <p class="find-location">Already ordered with us? <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>">Click here!</a></p>
                <?php } ?>

                <ul class="list-unstyled text-left text-md-center">
                    <li class="d-inline-block px-2 py-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i> Same Day Pick Up </li>
                    <li class="d-inline-block px-2 py-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i> Flexible Pay as You Go Billing</li>
                    <li class="d-inline-block px-2 py-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i> Free Delivery</li>
                    <li class="d-inline-block px-2 py-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i> Plugin Processes (Become a Partner)  </li>
                    <li class="d-inline-block px-2 py-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i> 25% Off First Order With Code TRYME25</li>
                </ul>
            </div>

        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-lg-3">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-lg-6 text-center">
                <img class="img-fluid mb-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-linen-cleaning-service.png' ?>" alt="London Hotel Quality Linen Cleaning Service" />
            </div>
            <div class="col-lg-3 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="140" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="140" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<div class="featured bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-6 col-lg-3">
                <img class="lcn-award-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'lcn-award.png' ?>" alt="LCN Award" />
            </div>
            <div class="col-6 col-lg-3">
                <img class="les-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'london-evening-standard.png' ?>" alt="London Evening Standard">
            </div>
            <div class="col-6 col-lg-3">
                <img class="bcs-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-cleaning-service.png' ?>" alt="Best Cleaning Service">
            </div>
            <div class="col-6 col-lg-3">
                <img class="price-promise-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'price-promise.png' ?>" alt="Laundry Price Promise" />
            </div>
        </div>
    </div>
</div>
<div class="page-content-section py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <h2 class="h1 my-3">What we offer</h2>
            </div>
        </div>
        <div class="row py-md-5">
            <div class="col-md-12 col-lg-5">
                <img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'bed-linen.jpg' ?>" alt="Bed linen and towels" />
            </div>
            <div class="col-md-12 col-lg-7 pl-lg-5">
                <h3 class="h4 mb-3">Bed linen & towels</h3>
                <p>We offer a complete range of sheets, pillowcases, duvet covers and towels that will be remembered by guests long after checking out as the pinnacle of softness and feel.</p>

                <table class="table table-borderless text-center">
                    <thead>
                        <tr>
                            <th scope="col">Bedrooms sets</th>
                            <th scope="col"><img class="d-block mx-auto mb-2" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-single-set.png' ?>" alt="" />Single set</th>
                            <th scope="col"><img class="d-block mx-auto mb-2"  src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-double-set.png' ?>" alt="" />Double set</th>
                            <th scope="col"><img class="d-block mx-auto mb-2"  src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-king-set.png' ?>" alt="" />King set</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-primary">
                            <th scope="row">Bed sheet</th>
                            <td>1</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <th scope="row">Duvet cover</th>
                            <td>1</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr class="table-primary">
                            <th scope="row">Pillow case</th>
                            <td>2</td>
                            <td>4</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <th scope="row">Large bath towel</th>
                            <td>1</td>
                            <td>2</td>
                            <td>2</td>
                        </tr>
                        <tr class="table-primary">
                            <th scope="row">Hand towel</th>
                            <td>1</td>
                            <td>2</td>
                            <td>2</td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-center">We also offer bath mats, kitchen cloths and more.</p>
            </div>
        </div>
        <div class="row py-md-5">
            <div class="col-md-12 col-lg-5 order-1 order-md-2 text-lg-right align-self-center">
                <img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'toiletries.png' ?>" alt="Toiletries" />
            </div>
            <div class="col-md-12 col-lg-7 order-2 order-md-1">
                <table class="table table-borderless text-center">
                    <thead>
                        <tr>
                            <th width="75%" class="text-left" scope="col"><h3 class="h4 mb-3">Toiletries</h3>
                                <p class="font-weight-normal">Our hotel-grade toiletries set includes all the essentials that your guests need.</p></th>
                            <th width="25%" scope="col"><img class="d-block mx-auto mb-2"  src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-toiletries-set.png' ?>" alt="Toiletries" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-primary">
                            <td scope="row">Shampoo</td>
                            <td>2 x 30ml</td>
                        </tr>
                        <tr>
                            <td scope="row">Shower gel</td>
                            <td>2 x 30ml</td>
                        </tr>
                        <tr class="table-primary">
                            <td scope="row">Body lotion</td>
                            <td>2 x 30ml</td>
                        </tr>
                        <tr>
                            <td scope="row">Conditioner</td>
                            <td>2 x 30ml</td>
                        </tr>
                        <tr class="table-primary">
                            <td scope="row">Body soap</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td scope="row">Hand soap</td>
                            <td>1</td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-center">We also offer bath mats, kitchen cloths and more.</p>
            </div>
        </div>
    </div>
</div>
<?php if (isset($widget_work_section_navigation_records)) { ?>
    <div class="how-it-work">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                    <h2 class="text-center mb-2">How Love2Laundry Works</h2>
                    <p class="mb-3 mb-md-5">We deliver on demand bed linen and towels for your guests</p>	
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center dotted">
                    <h3>MAKE A BOOKING</h3>
                    <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'hw-1.png'; ?>" class="img-fluid" alt="" /></div>
                    <span class="how-it-work-counter">01</span>
                    <p>Simply book a delivery for a convenient time to any address in London and with the items you need.</p>
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center dotted">
                    <h3>WE DELIVER</h3>
                    <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'hw-2.png'; ?>" class="img-fluid" alt="" /></div>
                    <span class="how-it-work-counter">02</span>
                    <p>We deliver exactly when you need it within a 2-hour slot. For instance, you can schedule a delivery when your cleaner is at the flat.</p>
                </div>
                <div class="col-12 col-lg-4 col-md-6 text-center dotted">
                    <h3>HAPPY GUEST, HAPPY HOST</h3>
                    <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'delivery-guy.png'; ?>" class="img-fluid" alt="" /></div>
                    <span class="how-it-work-counter">03</span>
                    <p>Receive 5-star reviews with our no-effort service. Exceed customer expectations, and get more bookings by being a Superhost</p>
                </div>
                <div class="col-md-12 text-center pt-3 pt-md-5 how-it-work-button">
                    <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg text-uppercase rounded-pill">Order Now</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<div class="laundry-prices py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 text-center laundry-prices-content">
                <h4 class="text-uppercase">UNMATCHED VALUE!</h4>
                <h2>Check Out our Prices</h2>
                <p class="mb-0">Our prices are simple and affordable which are easy on the pocket in comparison with high street prices.</p>
                <p>Save money on linen hire with our <strong>same day turnaround</strong> on freshly cleaned bed linen.</p>			
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
                            <div class="laundry-prices-inner">
                                <img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'single-bed.png' ?>" alt="Single Bed Set" />
                                <div class="laundry-prices-heading">Single <br>Bed Set</div>
                            </div>
                        </div>
                        <div class="laundry-prices-text">Starting From</div>
                        <div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>10.00</div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
                            <div class="laundry-prices-inner">
                                <img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'double-bed.png' ?>" alt="Double Bed Set" />
                                <div class="laundry-prices-heading">Double <br>Bed Set</div>
                            </div>
                        </div>
                        <div class="laundry-prices-text">Starting From</div>
                        <div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>12.00</div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
                            <div class="laundry-prices-inner">
                                <img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'king-bed.png' ?>" alt="King Bed Set" />
                                <div class="laundry-prices-heading">King <br>Bed Set</div>
                            </div>
                        </div>
                        <div class="laundry-prices-text">Starting From</div>
                        <div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>15.00</div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
                            <div class="laundry-prices-inner">
                                <img height="70" class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'wash-dry-fold.png' ?>" alt="Wash, Dry, Fold">
                                <div class="laundry-prices-heading">Cleaning <br> Services</div>
                            </div>
                        </div>
                        <div class="laundry-prices-text">Starting From</div>
                        <div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>12.00</div>
                        <div class="laundry-prices-weight">Per Hour</div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center py-3">
                <a href="<?php echo base_url('pricing') ?>" class="btn btn-white btn-lg rounded-pill py-3 px-md-5 mt-0 mt-md-3">SEE ALL PRICES HERE</a>
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <h2 class="h2 mb-3 mb-md-4">Professional Clean  Linen Services In London</h2>
                <p>In need of laundry and clean linen services in London? Love2Laundry collects your dirty washing from wherever you are in London at your chosen time. We make sure your garments are cleaned, ironed and folded or hung up and we then deliver them to your Airbnb business. All within the same day!</p>
                <p>We provide a one-stop shop for Airbnb cleaning solutions taking care of pillow cases, duvet covers, hand towels, bath towels and bed sheets plus anything else in between.</p>
            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-6 col-lg-3 py-3 text-center align-self-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-linen.png' ?>" alt="" />
                </div>
                Linen Cleaning
            </div>
            <div class="col-6 col-lg-3 py-3 text-center align-self-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-bed-linen.png' ?>" alt="" />
                </div>
                Home Cleaning
            </div>
            <div class="col-6 col-lg-3 py-3 text-center align-self-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-cushion.png' ?>" alt="" />
                </div>
                Cushion Cleaning
            </div>
            <div class="w-100 d-none d-md-block"></div>
            <div class="col-6 col-lg-3 py-3 text-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-towel.png' ?>" alt="" />
                </div>
                Towel Cleaning
            </div>
            <div class="col-6 col-lg-3 py-3 text-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-curtain.png' ?>" alt="" />
                </div>
                Curtain Cleaning
            </div>
            <div class="col-6 col-lg-3 py-3 text-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-clothing.png' ?>" alt="" />
                </div>
                Clothing Cleaning
            </div>
            <div class="col-6 col-lg-3 py-3 text-center">
                <div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-toiletries.png' ?>" alt="" />
                </div>
                Toiletries
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <h2 class="h2 mb-5">Our Services & Prices</h2>
            </div>
        </div>
        <div class="row py-3 py-md-5">
            <div class="col-lg-4 text-center px-3 px-lg-5 my-5  my-md-0">
                <div class="linen-service-offer bg-white rounded-xl shadow py-4">
                    <div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
                        <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-bed-linen.png' ?>" alt="" />
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="font-weight-bold">Full set of bed<br> linen & towels</h3>
                        <p>Single, double and king sets of linen and towels</p>
                    </div>
                    <div class="py-3">
                        <span class="linen-from">from</span>
                        <span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>10.00</span>
                        <span class="linen-unit">+ VAT per set</span>
                    </div>
                    <div class="bg-dark py-2">
                        <span class="text-uppercase text-white">Top Quality Guarantee</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center px-3 px-lg-5 my-5  my-md-0">
                <div class="linen-service-offer bg-white rounded-xl shadow py-4">
                    <div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
                        <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-toiletries.png' ?>" alt="" />
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="font-weight-bold">Toiletries</h3>
                        <p>Our hotel-grade toiletries set includes all the essentials that your guests need.</p>
                    </div>
                    <div class="py-3">
                        <span class="linen-from">from</span>
                        <span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>2.00</span>
                        <span class="linen-unit">+ VAT per set</span>
                    </div>
                    <div class="bg-dark py-2">
                        <span class="text-uppercase text-white">Top Quality Guarantee</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center px-3 px-lg-5 my-5  my-md-0">
                <div class="linen-service-offer bg-white rounded-xl shadow py-4">
                    <div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
                        <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-extra.png' ?>" alt="" />
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="font-weight-bold">Extra items</h3>
                        <p>We also offer bath mats, kitchen cloths, toilet paper and other home essentials.</p>
                    </div>
                    <div class="py-3">
                        <span class="linen-from">from</span>
                        <span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>1.00</span>
                        <span class="linen-unit">+ VAT per set</span>
                    </div>
                    <div class="bg-dark py-2">
                        <span class="text-uppercase text-white">Top Quality Guarantee</span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="contact-today py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <h2 class="text-center mb-3 mb-md-4">Get a Tailored Quote</h2>
                <form class="form-horizontal custom-form" id="frm_business_enquiry" name="frm_business_enquiry" method="post" action="<?php echo base_url('business-enquiry') ?>">
                    <div class="bg-white rounded-xl p-3 p-md-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label>Name</label>
                                    <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="Enter Your Name">

                                </div>
                                <div class="form-group mb-3">
                                    <label>Email Address</label>
                                    <input type="text" class="form-control form-control-lg" placeholder="Enter Your Email Address" id="email_address" name="email_address">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control form-control-lg only-number" placeholder="Enter Your Phone Number" id="phone_number" name="phone_number">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Nature of Enquiry</label>
                                    <select class="custom-select custom-select-lg" id="nature_of_enquiry" name="nature_of_enquiry">
                                        <option value="">Select</option>
                                        <option value="B&B">B&amp;B</option>
                                        <option value="Corporate">Corporate</option>
                                        <option value="Commercial">Commercial</option>
                                        <option value="Airbnb">Airbnb</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-4 pb-1">
                                    <label>Message</label>
                                    <textarea rows="12" class="form-control form-control-lg" placeholder="Comment / Notes / Message" id="message" name="message"></textarea>
                                </div>

                            </div>
                        </div>

                    </div>
                    <button type="submit" id="btn_business_enquiry" name="btn_business_enquiry" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Submit</span></button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text') ?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">

                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe') ?>">
                    <div class="form-group">
                        <div class="input-group  border-bottom mb-3">
                            <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
                            <div class="input-group-append">
                                <button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>