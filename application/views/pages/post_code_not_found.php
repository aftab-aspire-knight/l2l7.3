<div class="postcode-not-found py-3 py-md-5">
	<div class="container">
		<div class="row align-items-center text-center">
			<div class="col-lg-3">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'sad-women.png' ?>" alt="" />
			</div>
			<div class="col-lg-6">
				<h1 class="ff-gothamblack mb-3">Sorry, we are not <br>in your area yet...</h1>
				<p>Unfortunately, we're not serving the address that you requested at the moment. As we're steadily expanding, 
we hope to be serving your area soon.</p>
				<p class="mt-5 font-weight-bold">In the meantime, try our service at a different location</p>
				<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability')?>">
					<div class="place-order-field">
						<input type="text" name="post_code" id="post_code" placeholder="enter postcode" class="form-control">
						<button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Order Now</button>
					</div>
				</form>
				<?php if(!isset($is_member_login)) {?>
					<p class="find-location">Already ordered with us? <a href="<?php echo base_url($this->config->item('front_login_page_url'))?>">Click here!</a></p>
				<?php }?>
			</div>
			<div class="col-lg-3">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'upset-men.png' ?>" alt="" />
			</div>
		</div>
	</div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text')?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">
				
                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe')?>">
					<div class="form-group">
						<div class="input-group  border-bottom mb-3">
						  <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
						  <div class="input-group-append">
							<button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
						  </div>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>