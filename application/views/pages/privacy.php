<div class="py-3 py-md-4">
	<div class="container">
		<div class="row">
			<div class="col-md-3 order-2 order-lg-1">
				<a href="<?php echo base_url('t-c') ?>" class="btn btn-light btn-block py-4 font-weight-bold rounded-xl mb-4">
					<img class="mx-auto d-block mb-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic_terms.png' ?>" alt="" />
					Terms & Conditions
				</a>
				<a href="<?php echo base_url('privacy-policy') ?>" class="btn btn-primary btn-block py-4 font-weight-bold rounded-xl mb-4">
					<img class="mx-auto d-block mb-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic_privacy.png' ?>" alt="" />
					Privacy Policy
				</a>
				<div class="bg-light px-3 pt-4 pb-5 rounded-xl my-3 text-center">
					<h3 class="h5 font-weight-bold mb-3">PLACE ORDER NOW</h3>
					<form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability') ?>">
						<div class="form-group mb-2">
							<input type="text" class="form-control form-control-lg rounded-pill text-center" name="post_code" id="post_code" placeholder="enter postcode"   />
						</div>
						<div class="form-group">
							<button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-block btn-primary rounded-pill">Check Prices</button>
						</div>
					</form>
					<?php if(!isset($is_member_login)) {?>
						<span class="small">Already ordered with us? <a href="<?php echo base_url($this->config->item('front_login_page_url'))?>" class="font-weight-bold">Click here!</a></span>
					<?php }?>
				</div>
				<div class="bg-light px-3 pt-4 pb-4 rounded-xl  my-3 text-center">
					<h3 class="h5 font-weight-bold mb-3">GET THE APP LINK</h3>
					<form id="frm_app_link" name="app_mobile_no" method="post" action="<?php echo base_url('app-request')?>">
						<div class="form-group mb-2">
							<input type="text" class="form-control form-control-lg rounded-pill text-center" id="app_mobile_no" name="app_mobile_no" value="" placeholder="enter your number" />
						</div>
						<div class="form-group">
							<button type="submit" id="btn_app_request" class="btn btn-lg btn-block btn-primary rounded-pill">Download Link</button>
						</div>
					</form>
					<span class="small">Or download the app now!</span>
					<div class="my-2">
						<a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg'?>" alt="Love2Laundry - iOS"></a>
						<a target="_blank" class="android" href="https://play.google.com/store/apps/details?id=com.love2laundry.app"><img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png'?>" alt="Love2Laundry - Android"></a>
					</div>
				</div>
			</div>
			<div class="col-md-9 order-1 order-lg-2">
				<h1 class="ff-gothamblack mb-3"><?php echo isset($page_data['Title'])?$page_data['Title']:''?></h1>
					<?php if(isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5){
						//echo $page_data['Content'];
				} ?>
				<p>This privacy policy applies to you, the User of this Website and Love2Laundry Ltd, the owner and provider of this Website and Application. Love2Laundry Ltd takes the privacy of your information very seriously. This privacy policy applies to our use of any and all Data collected by us or provided by you in relation to your use of the Website. <strong>Please read this privacy policy carefully.</strong></p>
				
				<div class="accordion my-5" id="accordionTC">
				  <div class="accordion-item">
					<div class="accordion-header active" id="heading1">
					  <h2 class="mb-0">
						<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
						  1. Definitions and interpretation
						</button>
					  </h2>
					</div>

					<div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionTC">
					  <div class="accordion-body px-0">
						<p>In this privacy policy, the following definitions are used:</p>
						<dl class="row">
						  <dt class="col-sm-3">Data</dt>
						  <dd class="col-sm-9">
							<p>Collectively all information that you submit to Love2Laundry Ltd via the Website. This definition incorporates, where applicable, the definitions provided in the Data Protection Act 1998;</p>
						  </dd>

						  <dt class="col-sm-3">Cookies</dt>
						  <dd class="col-sm-9">
							<p>A small text file placed on your computer by this Website when you visit certain parts of the Website and/or when you use certain features of the Website. Details of the cookies used by this Website are set out in the clause below (Cookies);</p>
						  </dd>

						  <dt class="col-sm-3">Love2Laundry Ltd, or us</dt>
						  <dd class="col-sm-9"><p>Love2Laundry Ltd, a company incorporated in England and Wales with registered number 09143915 whose registered office is at 42 Albion Street, Rotherhithe, London, SE16 7JQ;</p></dd>

						  <dt class="col-sm-3 text-truncate">UK and EU Cookie Law</dt>
						  <dd class="col-sm-9"><p>The Privacy and Electronic Communications (EC Directive) Regulations 2003 as amended by the Privacy and Electronic Communications (EC Directive) (Amendment) Regulations 2011;</p></dd>
						  <dt class="col-sm-3 text-truncate">User of you</dt>
						  <dd class="col-sm-9"><p>Any third party that accesses the Website and is not either (i) employed by Love2Laundry Ltd and acting in the course of their employment or (ii) engaged as a consultant or otherwise providing services to Love2Laundry Ltd and accessing the Website in connection with the provision of such services; and</p></dd>
						  <dt class="col-sm-3 text-truncate">Website</dt>
						  <dd class="col-sm-9"><p>The website that you are currently using, www.love2laundry.com, and any sub-domains of this site unless expressly excluded by their own terms and conditions.</p></dd>
						  
						</dl>
						<p>In this privacy policy, unless the context requires a different interpretation:</p>
						<ol class="list-style">
							<li>The singular includes the plural and vice versa;</li>
							<li>References to sub-clauses, clauses, schedules or appendices are to sub-clauses, clauses, schedules or appendices of this privacy policy;</li>
							<li>A reference to a person includes firms, companies, government entities, trusts and partnerships;</li>
							<li>"including" is understood to mean "including without limitation";</li>
							<li>Reference to any statutory provision includes any modification or amendment of it;</li>
							<li>he headings and sub-headings do not form part of this privacy policy.</li>
						</ol>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading2">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
						  2. The scope of this privacy policy
						</button>
					  </h2>
					</div>
					<div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>This privacy policy applies only to the actions of Love2Laundry Ltd and Users with respect to this Website. It does not extend to any websites that can be accessed from this Website including, but not limited to, any links we may provide to social media websites.</p>
						
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading3">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
						  3. Data collected
						</button>
					  </h2>
					</div>
					<div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>We may collect the following Data, which includes personal Data, from you:</p>
						<ul>
							<li>Name</li>
							<li>Contact Information such as email addresses and telephone numbers;</li>
							<li>Financial information such as credit / debit card numbers;</li>
							<li>IP address (automatically collected);</li>
							<li>Web browser type and version (automatically collected);</li>
							<li>Operating system (automatically collected);</li>
							<li>in each case, in accordance with this privacy policy.</li>
							<li>Our App requires permissions to access your microphone, camera, location in order to provide services to you</li>
						</ul>
						<p>Our use of Data</p>
						<p>For purposes of the Data Protection Act 1998, Love2Laundry Ltd is the "data controller".</p>
						<p>We will retain any Data you submit for 12 months.</p>
						<p>Unless we are obliged or permitted by law to do so, and subject to any third party disclosures specifically set out in this policy, your Data will not be disclosed to third parties. This includes our affiliates and / or other companies within our group.</p>
						<p>All personal Data is stored securely in accordance with the principles of the Data Protection Act 1998. For more details on security see the clause below <strong>(Security)</strong>.</p>
						<p>Any or all of the above Data may be required by us from time to time in order to provide you with the best possible service and experience when using our Website. Specifically, Data may be used by us for the following reasons:</p>
						<ul>
							<li>internal record keeping;
</li>
							<li>improvement of our products / services;
</li>
							<li>transmission by email of promotional materials that may be of interest to you;</li>
							<li>contact for market research purposes which may be done using email, telephone, fax or mail. Such information may be used to customise or update the Website;</li>
						</ul>
						<p>in each case, in accordance with this privacy policy.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading4">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
						  4. Third party websites and services
						</button>
					  </h2>
					</div>
					<div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Love2Laundry Ltd may, from time to time, employ the services of other parties for dealing with certain processes necessary for the operation of the Website. The providers of such services have access to certain personal Data provided by Users of this Website.</p>
						<p>Any Data used by such parties is used only to the extent required by them to perform the services that we request. Any use for other purposes is strictly prohibited. Furthermore, any Data that is processed by third parties will be processed within the terms of this privacy policy and in accordance with the Data Protection Act 1998.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading5">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
						  5. Links to other websites
						</button>
					  </h2>
					</div>
					<div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>This Website may, from time to time, provide links to other websites. We have no control over such websites and are not responsible for the content of these websites. This privacy policy does not extend to your use of such websites. You are advised to read the privacy policy or statement of other websites prior to using them.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading6">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
						  6. Changes of business ownership and control
						</button>
					  </h2>
					</div>
					<div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Love2Laundry Ltd may, from time to time, expand or reduce our business and this may involve the sale and/or the transfer of control of all or part of Love2Laundry Ltd. Data provided by Users will, where it is relevant to any part of our business so transferred, be transferred along with that part and the new owner or newly controlling party will, under the terms of this privacy policy, be permitted to use the Data for the purposes for which it was originally supplied to us.</p>
						<p>In the above instances, we will take steps with the aim of ensuring your privacy is protected.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading7">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
						  7. Controlling use of your Data
						</button>
					  </h2>
					</div>
					<div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Wherever you are required to submit Data, you will be given options to restrict our use of that Data. This may include the following:</p>
						<p>use of Data for direct marketing purposes; and</p>
						<p>sharing Data with third parties.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading8">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
						  8. Functionality of the Website
						</button>
					  </h2>
					</div>
					<div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>To use all features and functions available on the Website, you may be required to submit certain Data.</p>
						<p>You may restrict your internet browser's use of Cookies. For more information, see the clause below <strong>(Cookies)</strong>.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading9">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
						  9. Accessing your own Data
						</button>
					  </h2>
					</div>
					<div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>You have the right to ask for a copy of any of your personal Data held by Love2Laundry Ltd (where such Data is held) on payment of a small fee, which will not exceed <?php echo $this->config->item('currency_symbol'); ?>10.</p>
						
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading10">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
						  10. Security
						</button>
					  </h2>
					</div>
					<div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Data security is of great importance to Love2Laundry Ltd and to protect your Data we have put in place suitable physical, electronic and managerial procedures to safeguard and secure Data collected via this Website.</p>
						<p>If password access is required for certain parts of the Website, you are responsible for keeping this password confidential.</p>
						<p>We endeavour to do our best to protect your personal Data. However, transmission of information over the internet is not entirely secure and is done at your own risk. We cannot ensure the security of your Data transmitted to the Website.</p>
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading11">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
						  11. Cookies
						</button>
					  </h2>
					</div>
					<div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>This Website may place and access certain Cookies on your computer. Love2Laundry Ltd uses Cookies to improve your experience of using the Website and to improve our range of services. Love2Laundry Ltd has carefully chosen these Cookies and has taken steps to ensure that your privacy is protected and respected at all times.</p>

<p>All Cookies used by this Website are used in accordance with current UK and EU Cookie Law.</p>

<p>Before the Website places Cookies on your computer, you will be presented with a pop-up requesting your consent to set those Cookies. By giving your consent to the placing of Cookies, you are enabling Love2Laundry Ltd to provide a better experience and service to you. You may, if you wish, deny consent to the placing of Cookies; however certain features of the Website may not function fully or as intended.</p>

<p>This Website may place the following Cookies:</p>
<p class="lead">Type of Cookie</p>
<h6>Purpose</h6>

<p>Analytical/performance cookies - They allow us to recognise and count the number of visitors and to see how visitors move around our website when they are using it. This helps us to improve the way our website works, for example, by ensuring that users are finding what they are looking for easily.</p>

<p>Functionality cookies: These are used to recognise you when you return to our website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</p>

<p>Targeting cookies: These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.</p>

<p>You can choose to enable or disable Cookies in your internet browser. By default, most internet browsers accept Cookies but this can be changed. For further details, please consult the help menu in your internet browser.</p>

<p>You can choose to delete Cookies at any time; however, you may lose any information that enables you to access the Website more quickly and efficiently including, but not limited to, personalisation settings.</p>

<p>It is recommended that you ensure that your internet browser is up-to-date and that you consult the help and guidance provided by the developer of your internet browser if you are unsure about adjusting your privacy settings.</p>
						
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading12">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapseSix">
						  12. Transfers outside the European Economic Area
						</button>
					  </h2>
					</div>
					<div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Data which we collect from you may be stored and processed in and transferred to countries outside of the European Economic Area (EEA). For example, this could occur if our servers are located in a country outside the EEA or one of our service providers is situated in a country outside the EEA. These countries may not have data protection laws equivalent to those in force in the EEA.</p>

<p>If we transfer Data outside the EEA in this way, we will take steps with the aim of ensuring that your privacy rights continue to be protected as outlined in this privacy policy. You expressly agree to such transfers of Data.</p>


						
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading13">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
						  13. General
						</button>
					  </h2>
					</div>
					<div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>You may not transfer any of your rights under this privacy policy to any other person. We may transfer our rights under this privacy policy where we reasonably believe your rights will not be affected.</p>

<p>If any court or competent authority finds that any provision of this privacy policy (or part of any provision) is invalid, illegal or unenforceable, that provision or part-provision will, to the extent required, be deemed to be deleted, and the validity and enforceability of the other provisions of this privacy policy will not be affected.</p>

<p>Unless otherwise agreed, no delay, act or omission by a party in exercising any right or remedy will be deemed a waiver of that, or any other, right or remedy.</p>

<p>This privacy policy is governed by and interpreted according to English law. All disputes arising under this privacy policy are subject to the exclusive jurisdiction of the English courts.</p>
	
			
					  </div>
					</div>
				  </div>
				  <div class="accordion-item">
					<div class="accordion-header" id="heading14">
					  <h2 class="mb-0">
						<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
						  14. Changes to this privacy policy
						</button>
					  </h2>
					</div>
					<div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionTC">
					  <div class="accordion-body">
						<p>Love2Laundry Ltd reserves the right to change this privacy policy as we may deem necessary from time to time or as may be required by law. Any changes will be immediately posted on the Website and you are deemed to have accepted the terms of the privacy policy on your first use of the Website following the alterations.</p>

<p>You may contact Love2Laundry Ltd by email at info@love2laundry.com
</p>

<p>14.2 Two discount code cannot be used at the same time. </p>

<p>14.3 If the order has been placed by using the discount code, the loyalty points will not be allocated. Loyalty points only can add up once the order has been placed without using any discount code. </p>

<p>14.4 Maximum cap on each discount code is 25.00 GBP. </p>
						
					  </div>
					</div>
				  </div>
				  
			</div>
		</div>
	</div>
</div>