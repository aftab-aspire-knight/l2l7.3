<div class="page-section bg-bubbles bg-light py-3 py-md-4">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-4 py-md-5 py-3 order-1 order-md-2">
				<h1 class="ff-gothamblack mt-4 mb-3">Refer Friends <br><span class="ff-gothambook">and you both
get <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('refer_friend_discount'); ?> OFF!</span></h1>
				<a href="<?php echo base_url('login') ?>" class="btn btn-primary btn-lg mt-3 rounded-pill px-5 py-3">Share Referral Code</a>
			</div>
			<div class="col-lg-6 text-center py-md-5 py-3 order-2 order-md-1 d-none d-md-block">
				<img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'tell-a-friend.png' ?>" alt="" />
			</div>
		</div>
		<div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-6 col-12 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
	</div>
</div>
<div class="refer-friends py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center mb-2">Refer friends and earn</h2>
				<p class="text-center text-capitalize h6">Earn more without any hasle</p>
            </div>
            <div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
				<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'invite-friend.png' ?>" class="img-fluid" alt="" />
				</div>
				<h3 class="h5 font-weight-bold">Invite your friends</h3>
				<p>Share your referral code with  your  friends and tell them to use it as a discount code when they book.</p>
			</div>
			<div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
				<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'tell-friend-discount-off.png' ?>" class="img-fluid" alt="" />
				</div>
				<h3 class="h5 font-weight-bold">Your friend gets <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('refer_friend_discount'); ?> OFF</h3>
				<p>Your friends will get <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('refer_friend_discount'); ?> off their first order when they use your code.</p>
			</div>
			<div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
				<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'tell-friend-discount.png' ?>" class="img-fluid" alt="" />
				</div>
				<h3 class="h5 font-weight-bold">You get <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('refer_friend_discount'); ?> OFF</h3>
				<p>After any of your friends successfully completes an order, you'll receive <?php echo $this->config->item('currency_symbol'); ?><?php echo $this->config->item('refer_friend_discount'); ?> off your next Love2Laundry order.</p>
			</div>
			
		
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container py-0 py-md-3">
        <div class="row justify-content-center">
			<div class="col-lg-8">
				<h2 class="text-center mb-2">Frequently Asked Questions</h2>
				<div class="accordion mt-5 custom-accordion" id="accordionFaq">
				  <div class="card border-0 mb-3">
					<div class="card-header border-0 rounded-pill" id="heading1">
					  <h2 class="h5 px-3 py-2 mb-0" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">How does this work? <i class="fas fa-chevron-right"></i></h2>
					</div>

					<div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionFaq">
					  <div class="card-body px-4">
						<p>Share your referral code or link with your friends and tell them to use it as a discount code when they book. We will reward you for each new friend that joins.</p>
					  </div>
					</div>
				  </div>
				  <div class="card border-0 mb-3">
					<div class="card-header border-0 rounded-pill" id="heading2">
					  <h2 class="h5 px-3 py-2 mb-0 collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">How many friends can I invite? <i class="fas fa-chevron-right"></i></h2>
					</div>

					<div id="collapse2" class="collapse " aria-labelledby="heading2" data-parent="#accordionFaq">
					  <div class="card-body px-4">
						<p>Share your referral code or link with your friends and tell them to use it as a discount code when they book. We will reward you for each new friend that joins.</p>
					  </div>
					</div>
				  </div>
				  <div class="card border-0 mb-3">
					<div class="card-header border-0 rounded-pill" id="heading3">
					  <h2 class="h5 px-3 py-2 mb-0 collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">How should I prepare for the first laundry collection? <i class="fas fa-chevron-right"></i></h2>
					</div>

					<div id="collapse3" class="collapse " aria-labelledby="heading3" data-parent="#accordionFaq">
					  <div class="card-body px-4">
						<p>Preparing for your first laundry collection is easy with Love2Laundry. One of our dedicated team members will arrive at your chosen location and collect your laundry, providing you with a handy bag to put it in. All you need to do is have all of your laundry ready to go. It couldn’t be simpler</p>
					  </div>
					</div>
				  </div>
				  <div class="card border-0 mb-3">
					<div class="card-header border-0 rounded-pill" id="heading4">
					  <h2 class="h5 px-3 py-2 mb-0 collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">How do I place an order? <i class="fas fa-chevron-right"></i></h2>
					</div>

					<div id="collapse4" class="collapse " aria-labelledby="heading4" data-parent="#accordionFaq">
					  <div class="card-body px-4">
						<p>Getting a collection slot is simple. You can place an order with us online at https://www.love2laundry.com  or via our mobile app.</p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row align-items-center">
			<div class="col-lg-12 py-3">
                <h2 class="text-center mb-2">Ready to Start?</h2>
				<p class="text-center text-capitalize h6">Apply as a Facility Partner and start to operate with us.</p>
				<a href="<?php echo base_url('register') ?>" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Register Now</span></a>
            </div>
			
        </div>
    </div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text')?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">
				
                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe')?>">
					<div class="form-group">
						<div class="input-group  border-bottom mb-3">
						  <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
						  <div class="input-group-append">
							<button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
						  </div>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>
