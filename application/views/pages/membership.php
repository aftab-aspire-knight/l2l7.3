<div class="page-section top-section-bg d-md-flex align-items-center py-3 py-md-4">
    <div class="top-section-container container pt-5">
        <div class="row align-items-center mt-0 mt-md-5">
            <div class="col-lg-7 col-xl-6 py-lg-5 py-3 order-2 order-md-1">
                <h1 class="ff-gothamblack text-center text-md-left mt-md-4 mb-3">Business Services</h1>
                <h2 class="h5 font-weight-bold">We collect, clean &amp; return at a time and location of your choice.</h2>
                <a href="javascript:void(0);" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-dark btn-lg text-uppercase rounded-pill py-3 px-md-5 mt-3 mb-3 btn-order d-block d-md-inline-block order-now-google">Order Now</a>
                <p class="collection-delivery">Collection within 1 hour, next day return.<br>Highest quality and lowest price guaranteed.</p>
            </div>
            <img class="img-fluid top-section-img" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'boy-laundry-cleaning.png' ?>" alt="" />
        </div>
        
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-lg-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-8 col-lg-6 col-12">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<div class="featured bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-3">
                <img width="94" height="100" class="lcn-award-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'lcn-award.png' ?>" alt="LCN Award" />
            </div>
            <div class="col-6 col-md-3">
                <img width="130" height="68" class="les-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'london-evening-standard.png' ?>" alt="London Evening Standard">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="bcs-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-cleaning-service.png' ?>" alt="Best Cleaning Service">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="price-promise-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'price-promise.png' ?>" alt="Laundry Price Promise" />
            </div>
        </div>
    </div>
</div>
<div class="corporate py-3 py-md-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center py-md-5 py-3">
                <h2 class="ff-gothamblack">What's Included</h2>
                <p>Ready to shape the future of laundry? Work with us</p>
            </div>
        </div>
        <div class="row pt-3 pb-md-5">
            <div class="col-6 col-lg-4 col-md-6 text-center">
                <h3 class="h5">Discount</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'grow-business.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Grow your business as a Love2Laundry partner</p>
            </div>
            <div class="col-6 col-lg-4 col-md-6 text-center">
                <h3 class="h5">Free Delivery</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'corporate-partner.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Exclusive staff benefits for corporate partners</p>
            </div>
            <div class="col-6 col-lg-4 col-md-6 text-center">
                <h3 class="h5">Cancel any time!</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'commercial-sector.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Flexible solutions for the hospitality sector</p>
            </div>
            
        </div>
    </div>
</div>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>

<div class="laundry-prices py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center laundry-prices-content">
                <h4 class="text-uppercase">Membership</h4>
                <h2>Our Packages</h2>
                <p>Sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text sample text.</p>
            </div>
            <div class="col-md-12">
                <div class="bg-white shadow-lg rounded-xl py-3 py-lg-4">
                    <div class="row no-gutters">
                        <div class="col-lg-5">
                           
                            <div class="h2 bg-dark text-white p-3" style="height:100px; margin-top:38px;">
                                Pricing
                            </div>
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td height="51px" class="small" width="50%" scope="row"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Sample text sample text sample text sample text sample text</td>
                                    </tr>
                                    <tr class="table-primary">
                                        <td height="51px" class="small" scope="row"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Sample text sample text sample text sample text sample text</td>
                                    </tr>
                                    <tr>
                                        <td height="51px" class="small" scope="row"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Sample text sample text sample text sample text sample text</td>
                                    </tr>
                                    <tr class="table-primary">
                                        <td height="51px" class="small" scope="row"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Sample text sample text sample text sample text sample text</td>
                                    </tr>
                                </tbody>
                            </table>  
                        </div>
                        <div class="col-lg-7">
                            <div class="row no-gutters">
                                <div class="col-lg-4 text-center">
                                    <h4 class="mb-3">Silver</h4>
                                    <div class="h3 bg-primary p-3 text-center" style="height:100px;">
                                        40/3months <span class="small text-white d-block">10% Discount</span>
                                    </div>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-check fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>  
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4 class="mb-3">Gold</h4>
                                    <div class="h3 bg-primary p-3 text-center" style="height:100px;">
                                        40/3months <span class="small text-white d-block">10% Discount</span>
                                    </div>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-check fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4 class="mb-3">Platinum</h4>
                                    <div class="h3 bg-primary p-3 text-center" style="height:100px;">
                                        40/3months <span class="small text-white d-block">10% Discount</span>
                                    </div>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-check fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                                
                                            </tr>
                                            <tr class="table-primary">
                                                <td class="text-center"><i class="fas fa-times fa-sm"></i></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                
            </div>
            <div class="col-md-12 text-center py-3">
                <a href="#" class="btn btn-white btn-lg rounded-pill py-3 px-md-5 mt-0 mt-md-3">Membership valid for 6 month</a>
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <h2 class="h2 mb-3 mb-md-4">Professional Cleaning Membership Services In London</h2>
                <p>In need of laundry and clean linen services in London? Love2Laundry collects your dirty washing from wherever you are in London at your chosen time. We make sure your garments are cleaned, ironed and folded or hung up and we then deliver them to your Airbnb business. All within the same day!</p>
                <p>We provide a one-stop shop for Airbnb cleaning solutions taking care of pillow cases, duvet covers, hand towels, bath towels and bed sheets plus anything else in between.</p>
            </div>
            <div class="col-md-12 text-center py-3">
                <a href="https://www.love2laundry.com/pricing" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3">START SAVING TODAY</a>
            </div>
        </div>
    </div>
</div>
<div class="app-splash py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center hidden-xs">
                <img width="121" height="121" class="l2l-app-icon" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-app-icon.png' ?>" alt="Laundrette & Laundromat London" />
            </div>
            <div class="offset-lg-2 col-lg-8 text-center">
                <?php
                $data['app_inner_area'] = true;
                $this->load->view('widgets/app_download_area', $data);
                ?>
            </div>
        </div>
    </div>
</div>