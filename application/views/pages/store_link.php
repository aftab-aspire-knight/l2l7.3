<div class="page-section bg-light py-3 py-md-4">
	<div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-lg-8 py-3 py-md-5">
				<h1 class="ff-gothamblack mb-3"><?php echo isset($page_data['Title'])?$page_data['Title']:''?></h1>
				<h2 class="h4 font-weight-bold">Please wait ....</h2>
                <h2 id="content"></h2>
                <input type="hidden" value="<?php echo $google_play_link?>" id="google_store_link" />
                <input type="hidden" value="<?php echo $apple_store_link?>" id="apple_store_link" />
                <a id="btn_home" href="<?php echo base_url()?>" class="btn  btn-primary btn-lg rounded-pill hide-area">Go Home</a>
			</div>
			
		</div>
		<div class="row py-3 align-items-end justify-content-between">
			<div class="col-md-3 col-6 d-none d-md-block">
				<!-- TrustBox widget - Mini -->
				<div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
					<a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
				</div>
				<!-- End TrustBox widget -->
			</div>
			<div class="col-md-6 col-12">
				<div class="my-4 text-center">
					<a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="180" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
					<a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="180" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text')?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">
				
                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe')?>">
					<div class="form-group">
						<div class="input-group  border-bottom mb-3">
						  <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
						  <div class="input-group-append">
							<button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
						  </div>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>