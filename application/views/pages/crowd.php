<div class="page-content-section py-3 py-md-5">
    <div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 text-center">
				<a href="https://www.crowdcube.com/" target="_blank">
                    <img src="<?php echo $this->config->item('front_upload_image_folder_url')?>crowd-cube-logo.png" alt="crowd cube logo" width="280">
                </a>
				<h2 class="h1 mb-3 mb-md-4"><?php echo isset($page_data['Title'])?$page_data['Title']:''?></h2>
				<p>When we launch our campaign, we'll be launching on Crowdcube. Joining Crowdcube is free, takes less than 60 seconds and carries no obligation to invest. More on this soon…</p>
                <hr />
				<?php echo $page_data['Content']?>
			</div>
		</div>
    </div>
</div>
<div class="contact-today py-3 py-md-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<h2 class="text-center mb-3 mb-md-4">Expression of Interest</h2>
				<form class="custom-form" id="frm_contact" id="frm_crowd_funding" name="frm_crowd_funding" method="post" action="<?php echo base_url('crowd-funding')?>">
				<div class="bg-white rounded-xl p-3 p-md-5">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group mb-3">
								<label>Name</label>
								<input type="text" class="form-control form-control-lg" placeholder="Your Name" id="name" name="name" />

							</div>
							<div class="form-group mb-3">
								<label>Email Address</label>
								<input type="text" class="form-control form-control-lg" placeholder="Your Email Address" id="email_address" name="email_address" />
							</div>
							<div class="form-group mb-3">
								<label>Amount</label>
								<select class="custom-select custom-select-lg" name="amount" id="amount">
                                    <option value="">Select the amount you might invest</option>
                                    <option value="up to <?php echo $this->config->item('currency_symbol'); ?>50">up to <?php echo $this->config->item('currency_symbol'); ?>50</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>50 - <?php echo $this->config->item('currency_symbol'); ?>250"><?php echo $this->config->item('currency_symbol'); ?>50 - <?php echo $this->config->item('currency_symbol'); ?>250</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>250 - <?php echo $this->config->item('currency_symbol'); ?>1000"><?php echo $this->config->item('currency_symbol'); ?>250 - <?php echo $this->config->item('currency_symbol'); ?>1000</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>1000 - <?php echo $this->config->item('currency_symbol'); ?>5000"><?php echo $this->config->item('currency_symbol'); ?>1000 - <?php echo $this->config->item('currency_symbol'); ?>5000</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>5000 - <?php echo $this->config->item('currency_symbol'); ?>10,000"><?php echo $this->config->item('currency_symbol'); ?>5000 - <?php echo $this->config->item('currency_symbol'); ?>10,000</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>10,000 - <?php echo $this->config->item('currency_symbol'); ?>25,000"><?php echo $this->config->item('currency_symbol'); ?>10,000 - <?php echo $this->config->item('currency_symbol'); ?>25,000</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>25,000 - <?php echo $this->config->item('currency_symbol'); ?>100,000"><?php echo $this->config->item('currency_symbol'); ?>25,000 - <?php echo $this->config->item('currency_symbol'); ?>100,000</option>
                                    <option value="<?php echo $this->config->item('currency_symbol'); ?>100,000+"><?php echo $this->config->item('currency_symbol'); ?>100,000+</option>
                                </select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group mb-4 pb-1">
								<label>Message</label>
								<textarea rows="8" cols="8" class="form-control form-control-lg" type="text" name="message" placeholder="Comments / Notes / Message"></textarea>
							</div>
							
						</div>
					</div>
					
				</div>
				<button type="submit" id="btn_crown_funding_submit" name="btn_crown_funding_submit" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Submit</span></button>
				</form>
			</div>
		</div>
	</div>
</div>