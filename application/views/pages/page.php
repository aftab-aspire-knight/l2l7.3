<?php
$image_name = $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-areas.png';
if (isset($page_data['HeaderImageName']) && !empty($page_data['HeaderImageName'])) {
    if (file_exists($this->config->item("dir_upload_banner") . $page_data['HeaderImageName'])) {
        $image_name = $this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'];
    }
}
?>
<div class="page-section bg-light page-about py-3" style="background-image:url(<?php echo $image_name; ?>);">
    <div class="container">
        <div class="row py-0 py-md-5 align-items-center">
            <div class="col-lg-6 py-md-5 py-0 page-about-inner order-2 order-md-1">
                <?php echo isset($page_data['Title']) ? '<h1 class="ff-gothamblack mt-4 mb-3 text-center text-md-left">' . $page_data['Title'] . '</h1>' : '' ?>
                <?php echo $page_data['Content'] ?>

            </div>
        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-6 col-12 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<?php if(isset($widget_work_section_navigation_records)){?>
<div class="how-it-work">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                <h2 class="text-center">How Love2Laundry Works</h2>
            </div>
            <?php
			$count = 0;
			$image = array(
                $this->config->item('front_asset_image_folder_url') . 'hw-1.png',
                $this->config->item('front_asset_image_folder_url') . 'hw-2.png',
                $this->config->item('front_asset_image_folder_url') . 'laundry-wash.png',
                $this->config->item('front_asset_image_folder_url') . 'delivery-guy.png'
            );
			foreach($widget_work_section_navigation_records as $record){
                echo '<div class="col-6 col-lg-3 col-md-6 text-center dotted">';
				echo '<h3>' . $record["Title"] . '</h3>';
                $image_name = $this->config->item('front_dummy_image_url');
                if($record['ImageName'] != "" && $record['ImageName'] != null){
                    if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                        $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                    }
                }
				echo '<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="' . $image[$count] . '" class="img-fluid" alt="" /></div>';
				echo '<span class="how-it-work-counter">0'.($count + 1).'</span>';
				echo $record["Content"];                
                echo '</div>';
				$count++;
            }
            ?>
			<div class="col-md-12 text-center pt-3 pt-md-5 how-it-work-button">
                <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg text-uppercase rounded-pill">Order Now</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if(isset($widget_about_record) && !empty($widget_about_record)){?>
<section class="page-content-section py-3 py-md-5">
    <div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<?php echo $widget_about_record['Content'] ?>
			</div>
			<div class="col-md-6">
			<?php echo '<h2 class="text-center">LAUNDRY MADE SIMPLE</h2>'; ?>
			<?php echo '<p><a href="https://www.youtube.com/watch?v=L2ff9nUN6jQ" id="l2l-video" class=""><img src="'. $this->config->item('front_asset_image_folder_url') . 'love2laundry-video.jpg'.'" class="img-fluid" alt="On Demand Dry Cleaning and Laundry Service in London" /></a></p>';
			?>
			</div>
		</div>    
	</div>
</section>
<?php }?>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<div class="app-splash py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center hidden-xs">
            <img class="l2l-app-icon" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-app-icon.png'?>" alt="Laundrette & Laundromat London" />
            </div>
            <div class="offset-lg-2 col-lg-8 text-center">
            <?php
            $data['app_inner_area'] = true;
            $this->load->view('widgets/app_download_area',$data);
            ?>
            </div>
        </div>
    </div>
</div>