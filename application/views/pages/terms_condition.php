<div class="py-3 py-md-4">
    <div class="container">
        <div class="row">
            <div class="col-md-3 order-2 order-lg-1">
                <a href="<?php echo base_url('t-c') ?>" class="btn btn-primary btn-block py-4 font-weight-bold rounded-xl mb-4">
                    <img class="mx-auto d-block mb-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic_terms.png' ?>" alt="" />
                    Terms & Conditions
                </a>
                <a href="<?php echo base_url('privacy-policy') ?>" class="btn btn-light btn-block py-4 font-weight-bold rounded-xl mb-4">
                    <img class="mx-auto d-block mb-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic_privacy.png' ?>" alt="" />
                    Privacy Policy
                </a>
                <?php $this->load->view('includes/order_now/' . SERVER . '_box'); ?>
                <div class="bg-light px-3 pt-4 pb-4 rounded-xl  my-3 text-center">
                    <h3 class="h5 font-weight-bold mb-3">GET THE APP LINK</h3>
                    <form id="frm_app_link" name="app_mobile_no" method="post" action="<?php echo base_url('app-request') ?>">
                        <div class="form-group mb-2">
                            <input type="text" class="form-control form-control-lg rounded-pill text-center" id="app_mobile_no" name="app_mobile_no" value="" placeholder="enter your number" />
                        </div>
                        <div class="form-group">
                            <button type="submit" id="btn_app_request" class="btn btn-lg btn-block btn-primary rounded-pill">Download Link</button>
                        </div>
                    </form>
                    <span class="small">Or download the app now!</span>
                    <div class="my-2">
                        <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                        <a target="_blank" class="android" href="https://play.google.com/store/apps/details?id=com.love2laundry.app"><img width="120" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9 order-1 order-lg-2">
                <h1 class="ff-gothamblack mb-3"><?php echo isset($page_data['Title']) ? $page_data['Title'] : '' ?></h1>
                <?php
                if (isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5) {
                    //echo $page_data['Content'];
                }
                ?>
                <p>Please read all these terms and conditions.</p>
                <p>As we can accept your order and make a legally enforceable agreement without further reference to you, you must read these terms and conditions to make sure that they contain all that you want and nothing that you are not happy with.</p>
                <p>Unless you accept these Terms (by ticking the acceptance box when you set up your account, you will not be able to place an order.</p>
                <p>Love2Laundry reserve the right to change the Terms from time to time. We will notify you of any changes which may affect you by email.</p>

                <div class="accordion my-5" id="accordionTC">
                    <div class="accordion-item">
                        <div class="accordion-header active" id="heading1">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    1. Application
                                </button>
                            </h2>
                        </div>

                        <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionTC">
                            <div class="accordion-body px-0">
                                <p>These Terms and Conditions will apply to the purchase of the services by you (the Customer or you). We are Love2Laundry a company registered in England and Wales under number 09143915 whose registered office is at 42 Albion Street, Rotherhithe, London, SE16 7JQ (the Supplier or us or we).</p>
                                <p>These are the terms on which we sell all Services to you. By ordering any of the Services, you agree to be bound by these Terms and Conditions. You can only purchase the Services from the Website and our App if you are eligible to enter into a contract and are at least 18 years old.</p>
                                <p>The Terms restrict our liability to you in certain circumstances.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading2">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                    2. Interpretation
                                </button>
                            </h2>
                        </div>
                        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Contract</dt>
                                    <dd class="col-sm-9">
                                        <p>means the legally-binding agreement between you and us for the supply of the Services;</p>
                                    </dd>

                                    <dt class="col-sm-3">An event outside our Control</dt>
                                    <dd class="col-sm-9">
                                        <p>any occurrence or circumstances over which we have no control including the unavailability of any service, provider or key materials without which we are unable to provide the services;</p>
                                    </dd>

                                    <dt class="col-sm-3">Goods</dt>
                                    <dd class="col-sm-9"><p>mean any goods that we supply to you with the Services, of the number and description as set out in the Order;</p></dd>

                                    <dt class="col-sm-3 text-truncate">Item</dt>
                                    <dd class="col-sm-9"><p>any garment or article collected from you in connection with an order;</p></dd>
                                    <dt class="col-sm-3 text-truncate">Order</dt>
                                    <dd class="col-sm-9"><p>means the Customer's order for the Services submitted on the Application;</p></dd>
                                    <dt class="col-sm-3 text-truncate">Services</dt>
                                    <dd class="col-sm-9"><p>mean the personal dry cleaning or laundry services collected from and delivered to your nominated address;</p></dd>

                                    <dt class="col-sm-3 text-truncate">Service Providers</dt>
                                    <dd class="col-sm-9"><p>any third party we contract with in order to assist us in providing and fulfilling the laundry services;</p></dd>

                                    <dt class="col-sm-3 text-truncate">Terms</dt>
                                    <dd class="col-sm-9"><p>mean these Terms and Conditions as amended from time to time;</p></dd>

                                    <dt class="col-sm-3 text-truncate">We/Our/Us</dt>
                                    <dd class="col-sm-9"><p>means Love2Laundry a company registered in England and Wales under number 09143915 with registered office at 42 Albion Street, Rotherhithe, London, SE16 7JQ</p></dd>

                                    <dt class="col-sm-3 text-truncate">Website</dt>
                                    <dd class="col-sm-9"><p>means our website www.Love2Laundry.com on which the Services are advertised.</p></dd>

                                </dl>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading3">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                    3. Placing an Order
                                </button>
                            </h2>
                        </div>
                        <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>When placing an order with us, please ensure that you have checked the details of your requirement before submitting it.  We will not be liable to you for errors or omissions you make.  Each item needs to be correctly identified.  If you have made a mistake, please contact us immediately so we may rectify this for you.  A contract is only formed between us once we have sent you an Order acceptance. This email will include your billing information.</p>
                                <p>Each order will be assigned an order number.  This order number needs to be quoted in all correspondence and contact with us. </p>
                                <p>All Services which appear on the Application are subject to availability.  If we are unable to fulfil your order, we will notify you in writing as soon as practicable.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading4">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    4. Changes to an Order
                                </button>
                            </h2>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>Changes can be made to an order 4 hours prior to collection or delivery time as set out in the order acceptance.  Changes can be made by contacting our Customer Care at info@love2laundry.com</p>
                                <p>We may make changes to your order as an alternative to cancellation at our discretion but with your consent. </p>
                                <p>Any changes to your order will be confirmed in writing by email.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading5">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    4. Order Cancellation
                                </button>
                            </h2>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>You have a right to cancel your order only under the following circumstances:</p>
                                <p>Up to four hours prior to collection or delivery time as set out in the order acceptance email</p>
                                <p>After we have collected your items if we have been affected by an event outside of our control by contacting our customer care at info@love2laundry.com</p>
                                <p>You agree that once your items have been collected from you, that rights of cancellation have expired and will be lost.  We reserve the right to charge you for orders cancelled in breach of this Clause.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading6">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                    6. Our rights to cancel an order
                                </button>
                            </h2>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>We may cancel your order and subsequently the contract formed between us under the following circumstances:</p>
                                <p>As a result of an event outside of our control;</p>
                                <p>If you fail to make the items available for collection as agreed within the order confirmation</p>
                                <p>If we consider any item not to correspond with the order placed and accepted, is damaged, contains</p>
                                <p>No item description or cleaning instructions or does not fall within the specified items which we accept.</p>
                                <p>If we cancel your order, we will notify you in writing by email as quickly as practicable;</p>
                                <p>You will not incur any charges for cleaning already carried out;</p>
                                <p>We will ensure items are delivered back to you at the original delivery time or as soon as reasonably practicable.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading7">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                    7. Customer responsibilities
                                </button>
                            </h2>
                        </div>
                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>You must co-operate with us in all matters relating to the Services, provide us with all information required to perform the Services.</p>
                                <p>Failure to comply with the above is a Customer default which entitles us to suspend performance of the Services until you remedy it or if you fail to remedy it following our request, we can terminate the Contract with immediate effect on written notice to you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading8">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                    8. Collection and Delivery
                                </button>
                            </h2>
                        </div>
                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>We will endeavour to collect and deliver cleaned items to you at the times specified in the order acceptance.  However, we do not guarantee to do so.  Any delay in our performance of collection and delivery will be communicated to you as quickly as possible by email.</p>
                                <p>If you are unavailable to accept delivery of items, we will contact you to rearrange a suitable time. </p>
                                <p>When re-delivery is attempted and you are not available at the designated time, we apply a re-delivery charge of <?php echo $this->config->item('currency_symbol'); ?>5 for each redelivery attempt.  We will endeavour to agree a mutually agreeable re-delivery time with you. </p>
                                <p>In cases where delivery is urgent, it is the customer’s responsibility to arrange collection from us with a courier at our facility.</p>
                                <p>Failure to accept delivery of items after 90 days from the date of order acceptance will cancel the contract between us and we may dispose of the item or donate it to an accredited charity of our choice.</p>
                                <p>You may give us permission, in writing, to leave an item in a specified secure location without providing us with a signature of acknowledgement.  If you do so, it is at our discretion and entirely at your own risk and we will not be liable to you under any circumstances for damage, theft or loss or items delivered under this clause.</p>
                                <p>We only offer 24-hour service in certain locations and may be affected by items and services selected.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading9">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                    9. Standards of Service
                                </button>
                            </h2>
                        </div>
                        <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>All our work is conducted in line with good industry standards, reasonable skill and care.</p>
                                <p>We will not be held liable for any delay or non-performance of services where it is shown you have failed to provide us with accurate information at the time of order placement.  We cannot accept an order that has an incomplete or inaccurate address or if you fail to accept delivery of items as specified in your order.</p>
                                <p>We will contact you in the event the item sent to us is of a higher risk of damage including but not limited to:</p>
                                <ul>
                                    <li>Items requiring special treatments or instructions for cleaning</li>
                                    <li>Items where there are no cleaning guidelines present</li>
                                    <li>Items which are damaged or stained</li>
                                    <li>Items containing extraneous or hazardous materials, including pins, jewellery, coins, pens, etc.</li>
                                </ul>
                                <p>We provide the services to you at our discretion and we may, with your consent, agree to provide the services regardless of the risk of damage.</p>
                                <dl class="row">
                                    <dt class="col-sm-3">Wash and Fold Orders</dt>
                                    <dd class="col-sm-9">
                                        <p>Please ensure to thoroughly check all the garments for hazardous items, e.g. coins, pens, keys, etc. as we accept no responsibility for any items lost or damaged as a result of the cleaning process. Each bag is limited to weight of 8 Kg. </p>
                                    </dd>

                                    <dt class="col-sm-3">Washing and Drying Process</dt>
                                    <dd class="col-sm-9">
                                        <p>The load is washed at the temperature as set out in our temperature choosing service within our preferences section, then tumble dried where appropriate at a medium heat. Please note that the items are not ironed. Where instructions not given, we wash standard at 30 degree and tumble dry at medium temperature, any shrinkage during the process is not covered for compensation. </p>
                                    </dd>

                                    <dt class="col-sm-3">Colour Separation</dt>
                                    <dd class="col-sm-9"><p>We will separate the clothes into lights and darks for you. Although we will take the utmost care in doing this we will accept no responsibility if there is any bleeding or colour transfer during the cleaning process.</p></dd>

                                    <dt class="col-sm-3">Bag Size, Price and Weight</dt>
                                    <dd class="col-sm-9"><p>We will measure the laundry by weight. The minimum weight is 8kg and every kg over 8kg we will charge accordingly. We will update this after we have weighed it at the facility and you will receive a confirmation email. Any orders below 8kg will be charged at the Wash, Dry & Fold price. Thebe Price will be charged at <?php echo $this->config->item('currency_symbol'); ?>2.00 per kg on a pro rota / adhoc basis once its exceed the minimum bundle of 8 or 16 kg. </p></dd>
                                    <dt class="col-sm-3 text-truncate">Damaged Items</dt>
                                    <dd class="col-sm-9"><p>We will not be liable for damage to items as we wash by the load and do not inspect the care labels of each garment. For garments that need to be specially treated, i.e. leather, silk, cashmere, fur, velvet and other delicate garments, please ensure that these items can be machine washed and tumble dried.</p></dd>
                                    <dt class="col-sm-3 text-truncate">Excluded Items</dt>
                                    <dd class="col-sm-9"><p>The Wash and Fold will not include any Duvets, blankets or bedspreads. If these are included, then it will be added as an individual product under a new order and your bill will be amended accordingly.</p></dd>

                                    <dt class="col-sm-3 text-truncate">Tagging</dt>
                                    <dd class="col-sm-9"><p>Laundry items are tagged by the load and not individually tagged. Therefore, we cannot accept any liability for missing items.</p></dd>

                                    <dt class="col-sm-3 text-truncate">Bulk Ironing Service</dt>
                                    <dd class="col-sm-9"><p>We offer a Bulk Ironing Service.  Once items are received, they will be re-weighed and the customer invoice will be updated if required.  Customers are encouraged to check their updated invoice to ensure they are aware of any price changes.</p></dd>
                                    
                                    <dt class="col-sm-3">Alteration, Shoe Repair & Bulky items</dt>
                                    <dd class="col-sm-9"><p>Alterations, Shoe Repair & Bulky items such as feather duvet / pillow / curtains can take sometime longer, the time can be given upon the receiving of the items with the new tentative slots.</p></dd>

                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading10">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                    10. Monthly Membership Plans
                                </button>
                            </h2>
                        </div>
                        <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p class="lead">We offer a Monthly Membership contract which includes 4 pickups per 30 days. Once the 4 pickups or 30 days has expired, the membership will cease and a new monthly contract needs to be purchased for the next period. Any package cannot be brought forward to the next month and must be used in the four-week period. For subsequent 2, 3, 4 pickups, the customer shall email at info@love2laundry.com with their availability and we shall confirm the slot with the customers. </p>
                                <p>10.1 With monthly subscriptions, the minimum order for each pickup has to be adhered to. For example if the area has a minimum charge of 15.00 per pickup, the 4 pickups will be charges as 60.00 GBP. </p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading11">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                    11. Personal information and Registration
                                </button>
                            </h2>
                        </div>
                        <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>When registering to use the Website you must set up a username and password. You remain responsible for all actions taken under the chosen username and password and undertake not to disclose your username and password to anyone else and keep them secret.</p>

                                <p>We retain and use all information strictly under the Privacy Policy.</p>

                                <p>We may contact you by using e-mail or other electronic communication methods and by pre-paid post and you expressly agree to this.</p>

                                <p>11.1 Garment Damage Policy: Whilst every care is taken handling your clothes, if you believe that we have damaged your garments in any way please contact us within 24 hours. We will do our best to resolve the situation. We will investigate thoroughly and if we feel that we were at negligent, we shall pay fair compensation based on the current value of the garment. This is usually around 1/3 the value of the item or 5-10 times the cost of the dry cleaning depending on the life of the garment.</p>

                                <p>11.2 In the unlikely event of loss or damage to an item, Love2laundry will pay compensation in line with the Fair Compensation Guidelines as provided by the Textile Services Association.</p>

                                <p>The Textile Services Association guidelines indicate that fair and reasonable compensation be paid on the basis of allowing for wear and tear and the age of the item. It is considered reasonable for Love2Laundry to ask for receipts, bank or credit card statements confirming the purchase price prior to agreeing to any compensation. Failure to produce a valid proof of purchase may limit the compensation amount offered.</p>

                                <p>11.3 Subject to the following subparagraphs, we will be compensated (in accordance with 11.1) for loss or damage which is due to Our Negligence.</p>

                                <p>11.3.1 We will not be responsible for any single item valued at more than <?php echo $this->config->item('currency_symbol'); ?>500 unless we have received (and acknowledged) notification via email to info@love2laundry.com</p>

                                <p>11.3.2 Uncollected items may be disposed of after 90 days. We will not be held liable for any loss that you may suffer in such an event.</p>

                                <p>11.3.3 If you believe the depreciated value of your garment/item exceeds 10 times our service charge, please declare the value. In which case the garment/item will be serviced at 1/10th of the declared value of the item</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading12">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapseSix">
                                    12. Fees and Payment
                                </button>
                            </h2>
                        </div>
                        <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>The fees (Fees) for the Services, the price of any Goods (if not included in the Fees) and any additional delivery or other charges is that set out on the Application at the date we accept the Order or such other price as we may agree in writing. Prices for Services may be calculated on a fixed price or on a standard daily rate basis.</p>

                                <p>Fees and charges include VAT at the rate applicable at the time of the Order. However, if the rate of VAT changes between the date we accept your Order and the date of payment, We will have to adjust the rate of the VAT that you pay</p>

                                <p>You must pay by submitting your credit or debit card details with your Order and we can take payment immediately or otherwise before delivery of the Services.</p>

                                <p>The price of the Services will be set out in Our price list as set out in the App or at www.love2laundry.com/pricing and will be the price in force at the time you place your Order. Our prices may change at any time, but price changes will not affect Orders that We have accepted prior to the increase.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading13">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                    13. Our liability to you
                                </button>
                            </h2>
                        </div>
                        <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>In the unlikely event of loss or damage to an itemLove2Laundry will pay compensation in line with the Fair Compensation Guidelines as provided by the Textile Services Association.</p>

                                <p>This indicates that fair and reasonable compensation be paid on the basis of allowing for wear and tear and the age of the item. We reserve the right to ask for receipts, bank or credit card statements confirming the purchase price prior to agreeing to any compensation. Failure to produce a valid proof of purchase may limit the compensation amount offered.</p>

                                <p>Subject to the following, we will be compensating for loss or damage which is due to Our Negligence.</p>

                                <p>We will not be responsible for any single item valued at more than <?php echo $this->config->item('currency_symbol'); ?>500 unless we have received (and acknowledged) notification via email to info@love2laundry.com</p>

                                <p>We will not be responsible to you for any loss or damage that is foreseeable. Loss or damage is foreseeable if it is an obvious consequence of Our breach or if it was contemplated by you and US at the time we entered into a contract for the Services.</p>

                                <p>We will not be responsible for any loss or damage (including, without limitation) any colour loss, shrinkage or other damage, resulting from the following:</p>
                                <ul>
                                    <li>failure to notify us of any special requirements or instructions for cleaning the Item;</li>

                                    <li>the fact that the Item has no label indicating cleaning instructions;</li>

                                    <li>any existing damage to the Item at the time of collection;</li>

                                    <li>any extraneous objects left in or on the Item, including but not limited to: coins, buttons, jewellery, cufflinks, collar stiffeners, pens or tie clips</li>

                                    <li>and extraneous packaging provided with the Item, including but not limited to: clothes hangers, suit bags or personal laundry bags</li>
                                </ul>
                                <p>We do not exclude or limit in any way Our liability for:</p>
                                <ul>
                                    <li>death or personal injury caused by negligence;

                                    <li>fraud or fraudulent misrepresentation;</li>

                                    <li>breach of the terms implied by the Supply of Goods and Services Act 1982 (title and quiet possession);</li>

                                    <li>defective products under the Consumer Protection Act 1987.</li>

                                    <li>We will not be liable for any consequential loss.</li>

                                    <li>We will not be liable for any damage to buttons, zips, and other similar parts (fastenings/belts/embellishments/etc or lost of the items if not mentioned while placing the order.).</li>
                                </ul>			

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading14">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                    14. Loyalty Scheme
                                </button>
                            </h2>
                        </div>
                        <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>For every <?php echo $this->config->item('currency_symbol'); ?>1 spent with us, you earn 1 Loyalty Point.  For every 100 points earned, you receive a <?php echo $this->config->item('currency_symbol'); ?>2 discount.  For every 500 points earned a <?php echo $this->config->item('currency_symbol'); ?>10 discount will be available.</p>

                                <p>14.1 Discount code cannot be used by creating multiple IDs or address. Restricted one-time use per household. </p>

                                <p>14.2 Two discount code cannot be used at the same time. </p>

                                <p>14.3 If the order has been placed by using the discount code, the loyalty points will not be allocated. Loyalty points only can add up once the order has been placed without using any discount code. </p>

                                <p>14.4 Maximum cap on each discount code is 25.00 GBP. </p>
                                <p>14.5 Discount code or loyalty points are not valid on Shoe & Alteration Service.</p>
                                <p>14.6 Love2Laundry reserves the right to withdraw the discount if it has been used in the same household more than 1 time. Each discount is valid for 1 time usage and 1 per household.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading15">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                    15. Quality Guarantee
                                </button>
                            </h2>
                        </div>
                        <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>Each and every item is checked before it is delivered to you.  If for any reason you are not happy, please contact us within 24 hours from delivery and we guarantee to re-clean your items free of charge.  Any complaints under this clause 15 made outside of 24 hours will be considered on a case by case basis at our discretion.</p>

                                <p>To request re-cleaning for your items, please email our Customer Care team, outlining the problem and attach any relevant photos. A Customer Service agent will contact you to arrange a suitable time for recollection.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading16">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                    16. Re-Cleaning
                                </button>
                            </h2>
                        </div>
                        <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>The original dry cleaning ticket must still be attached to the items being returned for re-cleaning and only applies to individual items cleaned by Love2Laundry.</p>

                                <p>If we cannot remove a stain customer will be informed according to the ticket which is attached to their items. In this instance, we are not able to offer a complimentary re-clean.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading17">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                    17. Vouchers & Promotions
                                </button>
                            </h2>
                        </div>
                        <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>New customers are customers who have not placed an order before. Minimum order applies and varies across different postcodes. London <?php echo $this->config->item('currency_symbol'); ?>20, Manchester <?php echo $this->config->item('currency_symbol'); ?>35, Amsterdam €25, Dubai 50 AED</p>

                                <p>Vouchers and promotions are subject to expiry dates and may vary from time to time and may be withdrawn without warning.</p>

                                <p>Love2Laundry vouchers and are subject to our full-service terms and conditions and website terms and conditions.</p>

                                <p>Vouchers cannot be used in conjunction with any other Love2Laundry offer or promotion.</p>

                                <p>Only one voucher can be used in one transaction.</p>

                                <p>Vouchers are strictly non-transferable and have no cash value. Selling of vouchers will render it void.</p>

                                <p>We reserve the right to reject a voucher with reasonable cause.</p>

                                <p>Referral voucher cannot be given to be given / used into the same property. </p>

                                <p>Vouchers can be redeemed at checkout by entering the code found on the voucher.</p>

                                <p>When using a voucher, the minimum order value is <?php echo $this->config->item('currency_symbol'); ?>20 or otherwise specified, including the voucher.
                                </p>
                                <p>Vouchers must be used within 24 hours of activation.</p>

                                <p>Vouchers are limited to one per household</p>

                                <p>Vouchers cannot be applied to shop items</p>

                                <p>Vouchers cannot be applied to bundles, as these bundles are already heavily discounted.</p>

                                <p>Your personal discount code will allow you to book 2 bags of 8kg Wash, Dry & Fold through Love 2 Laundry. These can be booked at the same time or separately using the same code. Your personal code will be distributed to you by your Scape Residence Team. Any additional services must be booked as extras and paid for by yourself at checkout. Delivery and collection arranged directly with Love 2 Laundry. Any questions or queries should also be passed through love 2 laundry. Only applicable to Scape London sites.</p>
                                <p>Discount code or loyalty points are not valid on Shoe & Alteration Service.</p>
                                <p>Love2Laundry reserves the right to withdraw the discount if it has been used in the same household more than 1 time. Each discount is valid for 1 time usage and 1 per household.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading18">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                    18. Referrals
                                </button>
                            </h2>
                        </div>
                        <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>For every successful referral, we give both parties a <?php echo $this->config->item('currency_symbol'); ?>10 discount.</p>

                                <p>All new referral vouchers have an expiry period of 30 days from the day that they are issued.</p>

                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading19">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapse19">
                                    19. Conformity
                                </button>
                            </h2>
                        </div>
                        <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>We have a legal duty to supply the Goods in conformity with the Contract, and will not have conformed if it does not meet the following obligation.</p>
                                <p>Upon delivery, the Goods will:</p>
                                <ul>
                                    <li>be of satisfactory quality;</li>
                                    <li>be reasonably fit for any particular purpose for which you buy the Goods which, before the Contract is made, you made known to us (unless you do not actually rely, or it is unreasonable for you to rely, on our skill and judgment) and be fit for any purpose held out by us or set out in the Contract; and</li>
                                    <li>conform to their description.</li>
                                </ul>
                                <p>It is not a failure to conform if the failure has its origin in your materials.</p>

                                <p>We will supply the Services with reasonable skill and care.</p>

                                <p>In relation to the Services, anything we say or write to you, or anything someone else says or writes to you on our behalf, about us or about the Services, is a term of the Contract (which we must comply with) if you take it into account when deciding to enter this Contract, or when making any decision about the Services after entering into this Contract. Anything you take into account is subject to anything that qualified it and was said or written to you by us or on behalf of us on the same occasion, and any change to it that has been expressly agreed between us (before entering this Contract or later).</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading20">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapse20">
                                    20. Duration, termination and suspension
                                </button>
                            </h2>
                        </div>
                        <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>The Contract continues as long as it takes us to perform the Services.</p>
                                <p>Either you or we may terminate the Contract or suspend the Services at any time by a written notice of termination or suspension to the other if that other:</p>
                                <ul>
                                    <li>commits a serious breach, or series of breaches resulting in a serious breach, of the Contract and the breach either cannot be fixed or is not fixed within 30 days of the written notice; or</li>
                                    <li>is subject to any step towards its bankruptcy or liquidation.
                                    </li>
                                </ul>
                                <p>On termination of the Contract for any reason, any of our respective remaining rights and liabilities will not be affected.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading21">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapse21">
                                    21. Successors and our sub-contractors
                                </button>
                            </h2>
                        </div>
                        <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>Either party can transfer the benefit of this Contract to someone else and will remain liable to the other for its obligations under the Contract. The Supplier will be liable for the acts of any sub-contractors who it chooses to help perform its duties.</p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading22">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">
                                    22. Circumstances beyond the control of either party
                                </button>
                            </h2>
                        </div>
                        <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>In the event of any failure by a party because of something beyond its reasonable control:

                                </p>
                                <ul>
                                    <li>The party will advise the other party as soon as reasonably practicable; and</li>
                                    <li>The party's obligations will be suspended so far as is reasonable, provided that that party will act reasonably, and the party will not be liable for any failure which it could not reasonably avoid, but this will not affect the Customer's above rights relating to delivery (and the right to cancel below).</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <div class="accordion-header" id="heading23">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">
                                    23. Governing law, jurisdiction and complaints
                                </button>
                            </h2>
                        </div>
                        <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#accordionTC">
                            <div class="accordion-body">
                                <p>The Contract (including any non-contractual matters) is governed by the law of England and Wales.</p>

                                <p>Disputes can be submitted to the jurisdiction of the courts of England and Wales or, where the Customer lives in Scotland or Northern Ireland, in the courts of respectively Scotland or Northern Ireland.</p>

                                <p>We try to avoid any dispute, so we deal with complaints as follows: If a dispute occurs, customers should contact us to find a solution. We will aim to respond with an appropriate solution within 5 days.</p>
                                <p>Model Cancellation Form</p>
                                <p>To<br>
                                    Love2Laundry<br>
                                    42 Albion Street<br>
                                    Rotherhithe<br>
                                    London<br>
                                    SE16 7JQ</p>
                                <p>I/We [*] hereby give notice that I/We [*] cancel my/our [*] contract of sale of the following goods [*] [for the supply of the following service [*], Ordered on [*]/received on [*]______________________(date received)</p>
                                <p>Name of the consumer(s):<br>
                                    Address of consumer(s):</p>
                                <p>Signature of consumer(s) (only if this form is notified on paper)<br>Date</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>