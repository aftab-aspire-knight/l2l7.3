<?php

if(isset($ppc_header_widget)){
    $image_name = "";
    if($ppc_header_widget['ImageName'] != "" && $ppc_header_widget['ImageName'] != null){
        if (file_exists($this->config->item("dir_upload_widget") . $ppc_header_widget['ImageName'])) {
            $image_name = $this->config->item("front_widget_folder_url") . $ppc_header_widget['ImageName'];
        }
    }
    ?>
<div class="adword-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-6 text-center">
                <?php if(isset($discount_offer)){ ?>
				<div class="adword-offers">
					<div class="adword-offer-inner">
						<div class="adword-discount"><?php echo $discount_offer; ?> OFF</div>
						<div class="adword-offer-text">All Laundry &amp; Dry Cleaning <span><?php echo $new_customer_offer_text; ?></span></div>
					</div>
				</div>
                <?php
                }if(isset($scape_logo)){ 
                    echo '<img width="300" src="' . $scape_logo . '" alt="dryclean" class="adword-header-img">';
                }
                if(isset($save10_image)){ 
                    echo '<img width="400" src="' . $save10_image . '" alt="dryclean" class="adword-header-img">';
                }
                if(!empty($image_name)){
                    echo '<img src="' . $image_name . '" alt="dryclean" class="adword-header-img">';
                }
                ?>
				<div class="download-ourapp">
					<a href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"
					   target="_blank">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="playstore"></a>
					<a href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"
					   target="_blank">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="istore"></a>
				</div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="adword-content">
                    <?php echo $ppc_header_widget["Content"];?>
					<div class="order-now-box">
						<?php $this->load->view('includes/order_now/'. SERVER); ?>
						<p class="adword-lead">Sit back, relax and let us take the hassle out of your laundry. You can track your collection and return through our easy-to-use app.</p>
						<p class="adword-terms"><a href="<?php echo base_url('t-c') ?>">Terms & Conditions</a> apply: </p>
                    </div>
                    <br clear="all"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<?php }if(isset($widget_work_section_navigation_records)){?>

<section class="adword-hiw">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                <h2 class="text-center">How Love2Laundry Works</h2>
            </div>
            <?php
			$count = 0;
			$title = array("Your Order","We Collect","We Return");
			$image = array($this->config->item('front_asset_image_folder_url') . 'hw-1.png',$this->config->item('front_asset_image_folder_url') . 'hw-2.png',$this->config->item('front_asset_image_folder_url') . 'delivery-guy.png');
			$text = array("Order Online or <br />from our App","We will collect at a time<br />& location that suit you","We will return <br />your clean laundry");
            foreach($widget_work_section_navigation_records as $record){
                echo '<div class="col-lg-4 col-md-4 text-center dotted">';
                $image_name = $this->config->item('front_dummy_image_url');
                if($record['ImageName'] != "" && $record['ImageName'] != null){
                    if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                        $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                    }
                }
				echo '<h3>' . $title[$count] . '</h3>';
				echo '<p>' . $text[$count] . '</p>';
				
                echo '<div class="hiw-image"><img src="' . $image[$count] . '" class="img-responsive" alt="" /></div>';
                echo '</div>';
				$count++;
            }
            ?>
        </div>
    </div>
</section>
<section class="adword-prices">
	<div class="container">
		<div class="row">
			<div class="col-md-6 adword-prices-text">
				<h4 class="text-uppercase">FREE COLLECTION &amp; DELIVERY</h4>
                <h2>Affordable Prices</h2>
				<p>Our pricing is simple and affordable making it easier on your pocket in comparison to high street prices.</p>
				<a class="btn btn-white" href="<?php echo base_url('pricing') ?>">View All Prices</a>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3 col-lg-6 col-6">
						<div class="offer-block offer">
							<div class="offer-block-inner">
								<div class="offer-block-heading">Shirt Offer</div>
								<div class="offer-block-text">Starting From</div>
								<div class="offer-block-price"><?php echo $this->config->item('currency_symbol'); ?>1.80</div>
								<div class="waves"><div id="water"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-lg-6 col-6">
						<div class="offer-block offer">
							<div class="offer-block-inner">
								<div class="offer-block-heading">2 Suits</div>
								<div class="offer-block-text">Only</div>
								<div class="offer-block-price"><?php echo $this->config->item('currency_symbol'); ?>20</div>
								<div class="waves"><div id="water"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-lg-6 col-6">
						<div class="offer-block offer">
							<div class="offer-block-inner">
								<div class="offer-block-heading">2 DRESSES</div>
								<div class="offer-block-text">Only</div>
								<div class="offer-block-price"><?php echo $this->config->item('currency_symbol'); ?>19.49</div>
								<div class="waves"><div id="water"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-lg-6 col-6">
						<div class="offer-block offer">
							<div class="offer-block-inner">
								<div class="offer-block-heading">Wash Dry &amp; Fold</div>
								<div class="offer-block-text">Starting From</div>
								<div class="offer-block-price"><?php echo $this->config->item('currency_symbol'); ?>15</div>
								<div class="waves"><div id="water"></div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="main-content adword-app">
    <div class="container">
        <div class="content-section">
            <div class="row">
                <div class="col-md-6 hidden-xs">
                <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-app.png'?>" class="img-responsive" alt="Laundrette & Laundromat London" />
                </div>
                <div class="col-md-6">
                <?php
                $data['app_inner_area'] = true;
                $this->load->view('widgets/app_download_area',$data);
                ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /*if(isset($widget_price_section_navigation_records)){ ?>
    <section class="ppc-pricing pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h4>FREE collection &amp; delivery</h4>
                    <h2>Affordable Prices</h2>
                    <p>Our prices are simple and affordable which are easy on pocket in comparison with the high street
                        prices</p>
                </div>
                <?php
                foreach($widget_price_section_navigation_records as $record){
                    echo '<div class="col-md-3 col-sm-3 col-xs-6">';
                    echo '<div class="offer">';
                    echo '<div class="offercontent">';
                    echo '<span class="offer-description">' . $record['Title'] . '</span>';
                    if($record['Content'] != "" && $record['Content'] != null && strlen($record['Content']) > 2){
                        echo $record['Content'];
                    }
                    echo '</div>';
                    echo '<div class="waves"><div id="water"></div></div>';
                    echo '</div>';
                    echo '</div>';
                }
                ?>
                <p><a class="btn btn-lg btn-success" href="<?php echo base_url('pricing') ?>">SEE ALL PRICES HERE</a>
                </p>
            </div>
        </div>
    </section>
    <?php
}
if(isset($ppc_faq_widget)){
    echo $ppc_faq_widget["Content"];
}*/
?>
<?php } ?>
