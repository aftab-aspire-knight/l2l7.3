<style>
#_hj_feedback_container{
	display:none;
}
</style>
<section class="top-section" style="background:none">
	<div class="video-foreground">
		<video id="myVideo" loop="" muted="" autoplay="" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'LOVE2LAUNDRY.mp4'?>" class="video-slide"></video>
	</div>
	<div class="top-section-container">
		<h1>We are Launching <br>soon in Dubai.</h1>
		<p>Due to our ongoing growth and success in London and the United Kingdom, we are pleased to announce that Love 2 Laundry will soon be offering our multi-award shortlisted Dry Cleaning and Laundry services in Dubai.</p>
		<p>With the latest and easy to use technology, and our dedication to offering the best level of service and care, let us take all the hassle of having to deal with your Laundry and Dry Cleaning away from you so you can spend the time on more important things in life.</p>
		<p>For more information on why to chose us or to register your early interest please enter your email below to get 50 DHS OFF on your first order.</p>
		<div class="place-order-form">
			<span class="title">Register Your Interest</span>
			<form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe')?>">
				
				<div class="place-order-field">
					<span class="fa fa-envelope locate-me" style="color:#00d399;background:none"></span>
					<input type="text" name="subscriber_email_address" id="subscriber_email_address"  class="form-control input-lg" placeholder="Enter you email address">
					<input type="hidden" name="source" id="source" value="<?php echo isset($current_url)?$current_url:''; ?>" >
					<button type="submit" id="btn_subscriber" name="btn_subscriber" class="btn-lg">Subscribe Now</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php /*
if(isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5){
    echo $page_data['Content'];
}*/
?>

