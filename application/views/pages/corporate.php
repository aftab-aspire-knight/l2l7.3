<div class="page-section bg-bubbles bg-light py-3 py-md-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 py-md-5 py-3 order-2 order-md-1">
                <h1 class="ff-gothamblack mt-4 mb-3">Business Services</h1>
                <p>Work with Us</p>
            </div>
            <div class="col-lg-6 text-right py-md-5 py-3 order-1 order-md-2 d-none d-md-block">
                <img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'corporate-laundry-services.png' ?>" alt="" />
            </div>
        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-6 col-12 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<div class="corporate py-3 py-md-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center py-md-5 py-3">
                <h1 class="ff-gothamblack"><?php echo isset($page_data['Title']) ? $page_data['Title'] : '' ?></h1>
                <p>Ready to shape the future of laundry? Work with us</p>
            </div>
        </div>
        <div class="row pt-3 pb-md-5">
            <div class="col-6 col-lg-3 col-md-6 text-center">
                <h3 class="h5"><?php echo $widget_first_title; ?></h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'grow-business.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Grow your business as a Love2Laundry partner</p>
            </div>
            <div class="col-6 col-lg-3 col-md-6 text-center">
                <h3 class="h5">Corporate</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'corporate-partner.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Exclusive staff benefits for corporate partners</p>
            </div>
            <div class="col-6 col-lg-3 col-md-6 text-center">
                <h3 class="h5">Commercial</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'commercial-sector.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Flexible solutions for the hospitality sector</p>
            </div>
            <div class="col-6 col-lg-3 col-md-6 text-center">
                <h3 class="h5">Airbnb</h3>
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center my-4">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'airbnb-host.png' ?>" class="img-fluid" alt="">
                </div>
                <p>Hotel-quality linen and laundry for Airbnb hosts</p>
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <?php
                $image_name = $this->config->item('front_asset_image_folder_url') . 'corporate-laundry.png';
                if (isset($page_data['HeaderImageName'])) {
                    if (file_exists($this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'])) {
                        $image_name = $this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'];
                    }
                }
                ?>
                <img class="img-fluid mb-3" src="<?php echo $image_name ?>" alt="Corporate Laundry Services" />
            </div>
            <div class="col-lg-6">
                <?php
                if (isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5) {
                    echo $page_data['Content'];
                }
                ?>
                <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg rounded-pill mt-3 px-5 py-3">Order Now</a>
            </div> 
        </div>
    </div>
</div>
<div class="featured bg-dark">
    <div class="container">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<div class="contact-today py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <form class="custom-form" id="frm_business_enquiry" name="frm_business_enquiry" method="post" action="<?php echo base_url('business-enquiry') ?>">
                    <h2 class="text-center mb-3 mb-md-4">Contact us Today</h2>
                    <div class="bg-white rounded-xl p-3 p-md-5">					
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label>Name</label>
                                    <input class="form-control form-control-lg" type="text" id="name" name="name" placeholder="" />
                                </div>
                                <div class="form-group mb-3">
                                    <label>Email Address</label>
                                    <input class="form-control form-control-lg" type="text" id="email_address" name="email_address" placeholder="" />
                                </div>
                                <div class="form-group mb-3">
                                    <label>Phone Number</label>
                                    <input class="form-control form-control-lg" type="text" id="phone_number" name="phone_number" placeholder="" />
                                </div>
                                <div class="form-group mb-3">
                                    <label>Nature of Enquiry</label>
                                    <select class="custom-select custom-select-lg" id="nature_of_enquiry" name="nature_of_enquiry">
                                        <option value="">Select</option>
                                        <option value="B&B">B&B</option>
                                        <option value="Corporate">Corporate</option>
                                        <option value="Commercial">Commercial</option>
                                        <option value="Airbnb">Airbnb</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-4 pb-1">
                                    <label>Message</label>
                                    <textarea rows="12" cols="8" class="form-control form-control-lg" type="text" id="message" name="message" placeholder="Comments / Notes / Message"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <button type="submit" id="btn_business_enquiry" name="btn_business_enquiry" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Submit</span></button>
                </form>
            </div>
        </div>
    </div>
</div>