<?php $this->load->view('widgets/page_banner_area'); ?>
<!--<section class="bg-gray pad-tb20">
    <div class="container">
        <div class="row">
            <div class="col-sm-9"><h4></h4>GET 25% OFF ON YOUR FIRST ORDER WITH CODE:</h4></div>
            <div class="col-sm-3"><button class="btn btn-pad fs24  btn-green">TRYME25</button></div>
        </div>
        
    </div>
</section>
-->
<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php
                if (isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5) {
                    echo $page_data['Content'];
                }
                ?>
                <?php
                if (isset($category_records) && sizeof($category_records)) {
                    $cat = 0;
                    echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
                    foreach ($category_records as $record) {
                        echo '<div class="panel panel-default">';
                        echo '<div class="panel-heading" role="tab" id="heading' . $cat . '">';
                        echo '<h3 class="panel-title">';
                        echo '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $cat . '" aria-expanded="true" aria-controls="collapse' . $cat . '"';
                        if ($cat != 0) {
                            echo ' class="collapsed"';
                        }
                        echo '> ' . $record['Title'] . ' <i class="l2l-minus"></i> </a> </h3>';
                        echo '</div>';
                        echo '<div id="collapse' . $cat . '" class="panel-collapse collapse';
                        if ($cat == 0) {
                            echo ' in';
                        }
                        echo '" role="tabpanel" aria-labelledby="heading' . $cat . '">';
                        echo '<div class="panel-body">';
                        echo '<div data-example-id="hoverable-table" class="bs-example">';
                        echo '<table class="table table-striped pricing-list">';
                        echo '<tbody>';
                        foreach ($record['service_records'] as $rec) {
                            echo '<tr>';
                            echo '<td>' . $rec['Title'] . '</td>';
                            echo '<td>' . $this->config->item('currency_sign') . $rec['Price'] . '</td>';
                            echo '</tr>';
                        }
                        echo '</tbody>';
                        echo '</table>';
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                        $cat += 1;
                    }
                    echo '</div>';
                }
                ?>
            </div>
        </div>
    </div>
</section>