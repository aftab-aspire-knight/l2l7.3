<div class="page-content-section py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <h2 class="h1 mb-3 mb-md-4"><?php echo $page_data['Title'] ?></h2>
                <?php echo $page_data['Content'] ?>
            </div>
            <div class="offset-lg-1 col-lg-10 pt-3 pt-md-5">
                <div class="row">

                    <?php
                    if (isset($franchise_postcode_array)) {
                        foreach ($franchise_postcode_array as $record) {
                            echo '<div class="col-12 px-3 px-md-4 text-center dotted">';
                            echo '<h4 class="mb-4 h5">' . $record["Letter"] . '</h4>';
                            echo '<ul class="list-group flex-row flex-wrap">';
                            foreach ($record["Codes"] as $rec) {
                                echo '<li class="list-group-item list-group-item-light">' . $rec . '</li>';
                            }
                            echo '</ul>';
                            echo '<hr class="bg-primary pt-1 border-0 rounded-pill mx-4" />';
                            echo '</div>';
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>