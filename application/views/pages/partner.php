<div class="page-section bg-bubbles bg-light py-3 py-md-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 py-md-5 py-3 order-2 order-md-1">
                <h1 class="ff-gothamblack mt-4 mb-3">Become a dry cleaning <br>partner with Love2Laundry</h1>
                <p>Get more business without the hassle and let us grow your customer base.</p>
                <a href="<?php echo base_url('register') ?>" class="btn btn-primary btn-lg mt-3 rounded-pill px-5 py-3"><span class="px-5">Register</span></a>
            </div>
            <div class="col-lg-6 text-right py-md-5 py-3 order-1 order-md-2 d-none d-md-block">
                <img class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'partner.png' ?>" alt="" />
            </div>
        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-6 col-12 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<div class="partner-us py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center mb-2">Why Partner With Us?</h2>
                <p class="text-center text-capitalize h6">Earn more without any hasle</p>
            </div>
            <div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'growth.png' ?>" class="img-fluid" alt="" />
                </div>
                <h3 class="h5 font-weight-bold">Rapid growth</h3>
                <p>Your business can become part of one of the fastest-growing laundry and dry cleaning services around and benefit from our exceptional reviews and reputation.</p>
            </div>
            <div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'hw-2.png' ?>" class="img-fluid" alt="" />
                </div>
                <h3 class="h5 font-weight-bold">Serve your customers</h3>
                <p>We handle everything from drop-off to collection, meaning you just have to worry about providing your services. Let us handle everything else!</p>
            </div>
            <div class="col-6 col-md-4 text-center px-3 px-md-5 dotted">
                <div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center mb-5">
                    <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'hand-shake.png' ?>" class="img-fluid" alt="" />
                </div>
                <h3 class="h5 font-weight-bold">Dedicated support</h3>
                <p>We support our partners every step of the way and will ensure yours and your customers’ experience is simple and straightforward.</p>
            </div>


        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 py-3 py-md-5">
                <h2 class="text-center mb-2">No sign-up fee</h2>
                <p class="text-center text-capitalize h6">Just a simple, easy application</p>
            </div>
            <div class="col-lg-7">
                <div class="list-group list-group-flush">
                    <div class="list-group-item border-0 my-2">
                        <h3 class="h5 font-weight-bold mb-2">No hidden costs</h3>
                        <p class="mb-1">Our support team can walk you through the application</p>
                    </div>
                    <div class="list-group-item border-0 my-2">
                        <h3 class="h5 font-weight-bold mb-2">Focus on your work</h3>
                        <p class="mb-1">Our team will handle collection, delivery and customer service</p>
                    </div>
                    <div class="list-group-item border-0 my-2">
                        <h3 class="h5 font-weight-bold mb-2">Consistent support</h3>
                        <p class="mb-1">We’re always on-call to help you out</p>
                    </div>
                    <div class="list-group-item border-0 my-2">
                        <h3 class="h5 font-weight-bold mb-2">We look after everything</h3>
                        <p class="mb-1">From marketing and sales to order tracking and customer service, all you need to do is process the orders and reap the benefits</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-5 text-center">
                <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'facility-support-team.png' ?>" class="img-fluid" alt="" />
            </div>
        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container py-0 py-md-3">
        <div class="row align-items-center">
            <div class="col-6 col-lg-6 text-center">
                <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-washing-machine.png' ?>" class="img-fluid" alt="" />
            </div>
            <div class="col-lg-6">
                <h2 class="font-weight-bold mb-2">Any questions?</h2>
                <p class="h6">Contact is at <a href="mailto:partners@love2laundry.com">partners@love2laundry.com</a> to speak to our team</p>
                <h3 class="h5 font-weight-bold mt-3 mt-md-5 mb-3">Dry cleaning partner requirements</h3>
                <ul class="list-unstyled">
                    <li class="pt-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Open 7 days a week.</li>
                    <li class="pt-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Adequate processing storage space.</li>
                    <li class="pt-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Minimum of 3 washers/dryers.</li>
                    <li class="pt-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Minimum of 1 dry cleaning machine.</li>
                    <li class="pt-2"><i class="far fa-check-circle mr-2 text-primary fa-lg"></i>Ability to use and manage computer & internet orders.</li>
                </ul>
            </div>

        </div>
    </div>
</div>
<div class="py-3 py-md-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 py-3">
                <h2 class="text-center mb-2">Ready Partner?</h2>
                <p class="text-center text-capitalize h6">Apply as a dry cleaning partner with Love2Laundry and start operating now.</p>
                <a href="<?php echo base_url('register') ?>" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Register Now</span></a>
            </div>

        </div>
    </div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text') ?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">

                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe') ?>">
                    <div class="form-group">
                        <div class="input-group  border-bottom mb-3">
                            <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
                            <div class="input-group-append">
                                <button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
