<?php if(isset($widget_work_section_navigation_records)){?>
<div class="how-it-work">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                <h2 class="text-center">How Love2Laundry Works</h2>
            </div>
            <?php
			$count = 0;
			$image = array(
                $this->config->item('front_asset_image_folder_url') . 'hw-1.png',
                $this->config->item('front_asset_image_folder_url') . 'hw-2.png',
                $this->config->item('front_asset_image_folder_url') . 'laundry-wash.png',
                $this->config->item('front_asset_image_folder_url') . 'delivery-guy.png'
            );
			foreach($widget_work_section_navigation_records as $record){
                echo '<div class="col-6 col-lg-3 col-md-6 text-center dotted">';
				echo '<h3>' . $record["Title"] . '</h3>';
                $image_name = $this->config->item('front_dummy_image_url');
                if($record['ImageName'] != "" && $record['ImageName'] != null){
                    if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                        $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                    }
                }
				echo '<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="' . $image[$count] . '" class="img-fluid" alt="" /></div>';
				echo '<span class="how-it-work-counter">0'.($count + 1).'</span>';
				echo '<p>' . $record["Content"] . '</p>';                
                echo '</div>';
				$count++;
            }
            ?>
			<div class="col-md-12 text-center pt-3 pt-md-5 how-it-work-button">
                <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg text-uppercase rounded-pill">Order Now</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<div class="laundry-prices py-3 py-md-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1 text-center laundry-prices-content">
				<h4 class="text-uppercase">UNMATCHED VALUE!</h4>
				<h2>Check Out our Prices</h2>
				<p class="mb-0">Our prices are simple and affordable which are easy on the pocket in comparison with high street prices.</p>
				<p>Save money on linen hire with our <strong>same day turnaround</strong> on freshly cleaned bed linen.</p>			
			</div>
			<div class="col-md-12">
				<div class="row">
					<div class="col-6 col-sm-6 col-lg-3">
						<div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
							<div class="laundry-prices-inner">
								<img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'single-bed.png' ?>" alt="Single Bed Set" />
								<div class="laundry-prices-heading">Single <br>Bed Set</div>
							</div>
						</div>
						<div class="laundry-prices-text">Starting From</div>
						<div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>12.00</div>
					</div>
					<div class="col-6 col-sm-6 col-lg-3">
						<div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
							<div class="laundry-prices-inner">
								<img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'double-bed.png' ?>" alt="Double Bed Set" />
								<div class="laundry-prices-heading">Double <br>Bed Set</div>
							</div>
						</div>
						<div class="laundry-prices-text">Starting From</div>
						<div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>15.00</div>
					</div>
					<div class="col-6 col-sm-6 col-lg-3">
						<div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
							<div class="laundry-prices-inner">
								<img class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'king-bed.png' ?>" alt="King Bed Set" />
								<div class="laundry-prices-heading">King <br>Bed Set</div>
							</div>
						</div>
						<div class="laundry-prices-text">Starting From</div>
						<div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>18.00</div>
					</div>
					<div class="col-6 col-sm-6 col-lg-3">
						<div class="laundry-prices-block py-lg-4 mx-3 mx-lg-4">
							<div class="laundry-prices-inner">
								<img height="70" class="laundry-prices-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'wash-dry-fold.png' ?>" alt="Wash, Dry, Fold">
								<div class="laundry-prices-heading">Wash, Dry <br> and Fold</div>
							</div>
						</div>
						<div class="laundry-prices-text">Starting From</div>
						<div class="laundry-prices-amount"><?php echo $this->config->item('currency_symbol'); ?>15.00</div>
						<div class="laundry-prices-weight">Per 8KG</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 text-center py-3">
				<a href="<?php echo base_url('pricing') ?>" class="btn btn-white">SEE ALL PRICES HERE</a>
			</div>
		</div>
	</div>
</div>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-0 col-sm-3 col-md-offset-1 col-md-2">
                <img class="img-circle img-airbnb" src="<?php echo $this->config->item('front_dummy_image_url'); ?>" alt="Airbnb Host Win Winn Keeper" />
                <span class="airbnb-winner">Airbnb Host <br>Win Winn Keeper</span>
            </div>
            <div class="col-sm-9 col-md-8">
                <blockquote class="blockquote-airbnb"><p>We couldn’t be as productive or efﬁcient without Love2Laundry, they are 100% fundamental for a successful short let operation.</p>
                <a href="<?php echo base_url('blog/airbnb-management') ?>">Read Full Case Study Here</a></blockquote>
            </div>
        </div>
    </div>
</section>
<div class="py-3 py-md-5">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 text-center">
				<h2 class="h2 mb-3 mb-md-4">Professional Clean  Linen Services In London</h2>
				<p>In need of laundry and clean linen services in London? Love2Laundry collects your dirty washing from wherever you are in London at your chosen time. We make sure your garments are cleaned, ironed and folded or hung up and we then deliver them to your Airbnb business. All within the same day!</p>
                <p>We provide a one-stop shop for Airbnb cleaning solutions taking care of pillow cases, duvet covers, hand towels, bath towels and bed sheets plus anything else in between.</p>
			</div>
			
		</div>
		<div class="row py-3 py-md-5 justify-content-center">
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-linen.png' ?>" alt="" />
				</div>
				Linen Cleaning
			</div>
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-bed-linen.png' ?>" alt="" />
				</div>
				Bed Linen Cleaning
			</div>
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-cushion.png' ?>" alt="" />
				</div>
				Cushion Cleaning
			</div>
		</div>
		<div class="row py-3">
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-towel.png' ?>" alt="" />
				</div>
				Towel Cleaning
			</div>
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-curtain.png' ?>" alt="" />
				</div>
				Curtain Cleaning
			</div>
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-clothing.png' ?>" alt="" />
				</div>
				Clothing Cleaning
			</div>
			<div class="col-lg-3 text-center">
				<div class="bg-primary d-table rounded-xl p-4 mx-auto mb-3">
					<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-toiletries.png' ?>" alt="" />
				</div>
				Toiletries
			</div>
		</div>
	</div>
</div>
<div class="py-3 py-md-5">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 text-center">
				<h2 class="h2 mb-3 mb-md-4">Our Services & Prices</h2>
			</div>
		</div>
		<div class="row py-3 py-md-5">
			<div class="col-lg-4 text-center px-3 px-lg-5">
				<div class="linen-service-offer bg-white rounded-xl shadow py-4">
					<div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
						<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-bed-linen.png' ?>" alt="" />
					</div>
					<div class="px-3 mt-4">
						<h3 class="font-weight-bold">Full set of bed<br> linen & towels</h3>
						<p>Single, double and king sets of linen and towels</p>
					</div>
					<div class="py-3">
						<span class="linen-from">from</span>
						<span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>10.00</span>
						<span class="linen-unit">+ VAT per set</span>
					</div>
					<div class="bg-dark py-2">
						<span class="text-uppercase text-white">Top Quality Guarantee</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4 text-center px-3 px-lg-5">
				<div class="linen-service-offer bg-white rounded-xl shadow py-4">
					<div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
						<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-toiletries.png' ?>" alt="" />
					</div>
					<div class="px-3 mt-4">
						<h3 class="font-weight-bold">Toiletries</h3>
						<p>Our hotel-grade toiletries set includes all the essentials that your guests need.</p>
					</div>
					<div class="py-3">
						<span class="linen-from">from</span>
						<span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>2.08</span>
						<span class="linen-unit">+ VAT per set</span>
					</div>
					<div class="bg-dark py-2">
						<span class="text-uppercase text-white">Top Quality Guarantee</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4 text-center px-3 px-lg-5">
				<div class="linen-service-offer bg-white rounded-xl shadow py-4">
					<div class="linen-image bg-primary rounded-circle d-table p-3 mx-auto">
						<img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ic-extra.png' ?>" alt="" />
					</div>
					<div class="px-3 mt-4">
						<h3 class="font-weight-bold">Extra items</h3>
						<p>We also offer bath mats, kitchen cloths, toilet paper and other home essentials.</p>
					</div>
					<div class="py-3">
						<span class="linen-from">from</span>
						<span class="linen-price h1"><?php echo $this->config->item('currency_symbol'); ?>2.08</span>
						<span class="linen-unit">+ VAT per set</span>
					</div>
					<div class="bg-dark py-2">
						<span class="text-uppercase text-white">Top Quality Guarantee</span>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<div class="contact-today py-3 py-md-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<h2 class="text-center mb-3 mb-md-4">Make an enquiry</h2>
				<div class="bg-white rounded-lg p-5">
					<form class="custom-form">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group mb-3">
									<label>Name</label>
									<input class="form-control form-control-lg" type="text" name="name" />
								</div>
								<div class="form-group mb-3">
									<label>Email Address</label>
									<input class="form-control form-control-lg" type="text" name="email" />
								</div>
								<div class="form-group mb-3">
									<label>Phone Number</label>
									<input class="form-control form-control-lg" type="text" name="phone" />
								</div>
								<div class="form-group mb-3">
									<label>Nature of Enquiry</label>
									<select class="custom-select custom-select-lg" id="nature_of_enquiry" name="nature_of_enquiry">
										<option value="">Select</option>
										<option value="B&B">B&amp;B</option>
										<option value="Corporate">Corporate</option>
										<option value="Commercial">Commercial</option>
										<option value="Airbnb">Airbnb</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group mb-4 pb-1">
									<label>Message</label>
									<textarea rows="12" cols="8" class="form-control form-control-lg" type="text" name="message" placeholder="Comments / Notes / Message"></textarea>
								</div>
								
							</div>
						</div>
					</form>
				</div>
				<button type="button" class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Submit</span></button>
			</div>
		</div>
	</div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text')?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">
				
                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe')?>">
					<div class="form-group">
						<div class="input-group  border-bottom mb-3">
						  <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
						  <div class="input-group-append">
							<button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
						  </div>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>