<style>
    #_hj_feedback_container{
        display:none;
    }
</style>
<section class="top-section d-flex align-items-center" style="background:none">
    <div class="video-foreground">
        <video id="myVideo" loop="" muted="" autoplay="" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'LOVE2LAUNDRY.mp4' ?>" class="video-slide"></video>
    </div>
    <div class="top-section-container text-white" style="height:65%">
        <?php
        if (isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5) {
            echo $page_data['Content'];
        }
        ?>
        <div class="place-order-form text-center">
            <span class="title" id="info-message" >Register Your Interest</span>

            <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe') ?>">
                <div class="place-order-field">
                    <input type="text" name="subscriber_email_address" id="subscriber_email_address"  class="form-control input-lg" placeholder="Enter you email address">
                    <input type="hidden" name="source" id="source" value="<?php echo ucfirst(isset($current_url) ? $current_url : ''); ?>" >
                    <button type="submit" id="btn_subscriber" name="btn_subscriber" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Subscribe Now</button>

                </div>
            </form>
        </div>

    </div>
</section>

