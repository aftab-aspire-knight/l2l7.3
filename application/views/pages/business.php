<section class="banner">
    <div class="banner-img">
        <?php
        $image_name = $this->config->item('front_asset_image_folder_url') . 'love2laundry-dry-cleaning.jpg';
        if (isset($page_data['HeaderImageName'])) {
            if (file_exists($this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'])) {
                $image_name = $this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'];
            }
        }
        ?>
        <img src="<?php echo $image_name ?>" alt="Dry Cleaner" />
    </div>
    <div class="innerpage-heading">
        <h2><?php echo isset($page_data['Title']) ? $page_data['Title'] : '' ?></h2>
        <h3>Ready to shape the future of laundry? Work with us</h3>
    </div>
</section>
<section class="getAppBar">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <div class="download-ourapp">
                    <a target="_blank" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img src="/uploads/images/appstore-itunes.png" alt="istore"></a>
                    <a target="_blank" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img src="/uploads/images/appstore-googleplay.png" alt="playstore"></a>

                </div>
            </div>
            <div class="col-md-3">
                <h3>GET APP LINK DIRECTLY</h3>
            </div>
            <div class="col-md-5">
                <div class="bs-applink">
                    <form id="frm_app_link" name="frm_app_link" method="post" action="<?php echo base_url('app-request') ?>">
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control text-center only-number" placeholder="ENTER NUMBER" name="app_mobile_no" id="app_mobile_no">
                        </div>
                        <div class="form-group col-sm-4 text-left">
                            <button type="submit" id="btn_app_request" class="btn btn-primary btn-lg btn-bs-location">Get Download Link</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<?php
if (isset($page_data['Content']) && $page_data['Content'] != "" && $page_data['Content'] != null && strlen($page_data['Content']) > 5) {
    echo $page_data['Content'];
}
?>
<!--
<section class="bg-gray pad-tb20">
    <div class="container">
        <div class="row">
            <div class="col-sm-9"><h4></h4>GET 25% OFF ON YOUR FIRST ORDER WITH CODE:</h4></div>
            <div class="col-sm-3"><button class="btn btn-pad fs24  btn-green">TRYME25</button></div>
        </div>
        
    </div>
</section>
-->
<section class="order-process">
    <div class="bs-light-gray tb-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center" style="margin-bottom: 50px;">CONTACT US TODAY</h2>
                </div>

                <form class="form-horizontal custom-forms" id="frm_business_enquiry" name="frm_business_enquiry" method="post" action="<?php echo base_url('business-enquiry') ?>">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Name <span class="red-color">*</span></label>
                            <div class="col-sm-8 icon-field">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Your Name">
                                <i class="icon l2l-first-name"></i>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Email Address <span class="red-color">*</span></label>
                            <div class="col-sm-8 icon-field">
                                <input type="text" class="form-control" placeholder="Enter Your Email Address" id="email_address" name="email_address">
                                <i class="icon l2l-mail-read"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Phone Number <span class="red-color">*</span></label>
                            <div class="col-sm-8 icon-field">
                                <input type="text" class="form-control only-number" placeholder="Enter Your Phone Number" id="phone_number" name="phone_number">
                                <i class="icon l2l-phone"></i>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nature of Enquiry <span class="red-color">*</span></label>
                            <div class="col-sm-8 icon-field">
                                <select class="form-control" id="nature_of_enquiry" name="nature_of_enquiry">
                                    <option value="">Select</option>
                                    <option value="B&B">B&B</option>
                                    <option value="Corporate">Corporate</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Airbnb">Airbnb</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Message <span class="red-color">*</span></label>
                            <div class="col-sm-8 icon-field">
                                <textarea class="form-control" placeholder="Comment / Notes / Message" id="message" name="message"></textarea>
                                <i class="icon l2l-paperclip"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8 icon-field">
                                <button type="submit" id="btn_business_enquiry" name="btn_business_enquiry" class="btn btn-primary btn-lg btn-block text-uppercase">Submit</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section><section class="section">    <div class="container">        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="light" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->    </div></section>
