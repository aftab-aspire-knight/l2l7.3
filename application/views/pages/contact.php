<?php
$image_name = $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-areas.png';
if (isset($page_data['HeaderImageName']) && !empty($page_data['HeaderImageName'])) {
    if (file_exists($this->config->item("dir_upload_banner") . $page_data['HeaderImageName'])) {
        $image_name = $this->config->item("front_banner_folder_url") . $page_data['HeaderImageName'];
    }
}
?>
<div class="page-section bg-light page-contact py-3" style="background-image:url(<?php echo $image_name; ?>);">
    <div class="container">
        <div class="row py-3 py-md-5 align-items-center">
            <div class="col-lg-6 py-md-5 py-3 page-contact-inner order-2 order-md-1">
                <?php echo isset($page_data['Title']) ? '<h1 class="ff-gothamblack mt-4 mb-3 text-center text-md-left">' . $page_data['Title'] . '</h1>' : '' ?>
                <?php echo $page_data['Content'] ?>

            </div>
        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-6 col-12 text-center text-lg-right ">
                <a target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                <a target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
            </div>
        </div>
    </div>
</div>
<div class="page-content-section py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="offset-lg-1 col-lg-10 text-center">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card shadow-sm py-3 rounded-xl">
                            <p class="m-0 h3 text-dark"><i class="fas fa-phone fa-flip-horizontal"></i> <?php echo isset($website_telephone_no) ? $website_telephone_no : '' ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card shadow-sm py-3 rounded-xl ">
                            <p class="m-0 h3 text-dark"><i class="fas fa-envelope"></i> <?php echo isset($website_email_address) ? $website_email_address : '' ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /*<div class="contact-today py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <h2 class="text-center mb-3 mb-md-4">Get in Touch</h2>
                <form class="custom-form" id="frm_contact" name="frm_contact" method="post" action="<?php echo base_url('contact-us') ?>">
                    <div class="bg-white rounded-xl p-3 p-md-5">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label>Name</label>
                                    <input type="text" class="form-control form-control-lg" placeholder="Your Name" id="name" name="name" />

                                </div>
                                <div class="form-group mb-3">
                                    <label>Email Address</label>
                                    <input type="text" class="form-control form-control-lg" placeholder="Your Email Address" id="email_address" name="email_address" />
                                </div>
                                <div class="form-group mb-3">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control form-control-lg only-number" placeholder="Your Phone Number" id="phone_number" name="phone_number" />
                                </div>
                                <div class="form-group mb-3">
                                    <label>Address</label>
                                    <textarea class="form-control form-control-lg" placeholder="Your Complete Address" style="height:60px;" id="address" name="address"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mb-4 pb-1">
                                    <label>Message</label>
                                    <textarea rows="12" cols="8" class="form-control form-control-lg" type="text" name="message" placeholder="Comments / Notes / Message"></textarea>
                                </div>

                            </div>
                        </div>

                    </div>
                    <button type="submit" id="btn_contact" name="btn_contact"  class="btn btn-primary btn-lg d-table mx-auto mt-4 mt-md-5 rounded-pill px-5 py-3"><span class="px-5">Submit</span></button>
                </form>
            </div>
        </div>
    </div>
</div> */?>