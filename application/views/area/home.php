<div class="page-section position-relative bg-light py-3 py-md-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 py-lg-5 py-3 order-2 order-md-1">
                <?php echo isset($page_data['HeaderTitle']) ? '<h1 class="ff-gothamblack mt-4 mb-3 text-center text-md-left">' . $page_data['HeaderTitle'] . '</h1>' : '' ?>
                <?php $this->load->view('includes/order_now/'. SERVER); ?>
                <?php if (!isset($is_member_login)) { ?>
                    <p class="find-location text-left">Already ordered with us? <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>">Click here!</a></p>
                <?php } ?>
            </div>
            <?php
                $image_name = $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-areas.png';
                if (isset($page_data['Section_1_Image_Name']) && !empty($page_data['Section_1_Image_Name'])) {
                    if (file_exists($this->config->item("dir_upload_banner") . $page_data['Section_1_Image_Name'])) {
                        $image_name = $this->config->item("front_banner_folder_url") . $page_data['Section_1_Image_Name'];
                    }
                }
                ?>
                <img class="img-fluid top-section-img" width="610" height="500" src="<?php echo $image_name; ?>" alt="<?php echo $page_data['Title']; ?>" />
            <!--<div class="col-lg-6 text-right py-lg-5 py-3 order-1 order-md-2 d-none d-md-block">
                
            </div>-->
        </div>
        <div class="row py-3 align-items-end justify-content-between">
            <div class="col-md-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-9 col-12 text-center text-lg-right ">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-xl-3">
                        <div class="mt-2 text-center">
                            <a rel="noopener noreferrer" target="_blank" class="ios d-block mb-2" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                            <a rel="noopener noreferrer" target="_blank" class="android d-block mb-2" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                        <img src="<?php echo $this->config->item('front_asset_image_folder_url') . 'qrcode.png'?>" width="110" height="110" alt="Install App Love2Laundry" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="featured bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-3">
                <img width="94" height="100" class="lcn-award-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'lcn-award.png' ?>" alt="LCN Award" />
            </div>
            <div class="col-6 col-md-3">
                <img width="130" height="68" class="les-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'london-evening-standard.png' ?>" alt="London Evening Standard">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="bcs-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-cleaning-service.png' ?>" alt="Best Cleaning Service">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="price-promise-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'price-promise.png' ?>" alt="Laundry Price Promise" />
            </div>
        </div>
    </div>
</div>
<?php if (isset($widget_work_section_navigation_records)) { ?>
    <div class="how-it-work" id="hiw">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                    <h2 class="text-center">How Love2Laundry Works</h2>
                </div>
                <?php
                $count = 0;
                $image = array(
                    $this->config->item('front_asset_image_folder_url') . 'hw-1.png',
                    $this->config->item('front_asset_image_folder_url') . 'hw-2.png',
                    $this->config->item('front_asset_image_folder_url') . 'laundry-wash.png',
                    $this->config->item('front_asset_image_folder_url') . 'delivery-guy.png'
                );
                foreach ($widget_work_section_navigation_records as $record) {
                    echo '<div class="col-6 col-lg-3 col-md-6 text-center dotted">';
                    echo '<h3>' . $record["Title"] . '</h3>';
                    $image_name = $this->config->item('front_dummy_image_url');
                    if ($record['ImageName'] != "" && $record['ImageName'] != null) {
                        if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                            $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                        }
                    }
                    echo '<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="' . $image[$count] . '" width="100" height="80" class="img-fluid" alt="" /></div>';
                    echo '<span class="how-it-work-counter">0' . ($count + 1) . '</span>';
                    echo '<p>' . $record["Content"] . '</p>';
                    echo '</div>';
                    $count++;
                }
                ?>
                <div class="col-md-12 text-center pt-3 pt-md-5 how-it-work-button">
                    <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg text-uppercase rounded-pill">Order Now</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>
<?php if (isset($widget_price_record)) { ?>
    <div class="laundry-prices py-3 py-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center laundry-prices-content">
                    <?php
                    if ($this->config->item('front_home_price_section_heading_text') != "") {
                        echo '<h4 class="text-uppercase">' . $this->config->item('front_home_price_section_heading_text') . '</h4>';
                    }
                    echo '<h2>' . $widget_price_record['Title'] . '</h2>';
                    if ($widget_price_record['Content'] != "" && $widget_price_record['Content'] != null && strlen($widget_price_record['Content']) > 5) {
                        echo $widget_price_record['Content'];
                    }
                    ?>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <?php
                        if (isset($widget_price_section_navigation_records)) {
                            foreach ($widget_price_section_navigation_records as $record) {
                                echo '<div class="col-6 col-sm-6 col-lg-3">';
                                echo '<div class="laundry-prices-block">';
                                echo '<img class="laundry-prices-promise pulse" src="' . $this->config->item('front_asset_image_folder_url') . 'price-promise-stamp.png" width="97" height="95" alt="Laundry Price Promise" />';
                                echo '<div class="laundry-prices-inner">';
                                $image_name = $this->config->item('front_dummy_image_url');
                                if ($record['ImageName'] != "" && $record['ImageName'] != null) {
                                    if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                                        $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                                    }
                                }
                                echo '<img width="70" height="86" class="laundry-prices-image" src="' . $image_name . '" alt="' . $record['Title'] . '" />';
                                echo '<div class="laundry-prices-heading">' . $record['Title'] . '</div>';
                                echo '</div>';
                                echo '</div>';
                                if ($record['Content'] != "" && $record['Content'] != null && strlen($record['Content']) > 2) {
                                    echo $record['Content'];
                                }
                                echo '</div>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-12 text-center py-3">
                    <?php
                    if ($widget_price_record['Link'] != "" && $widget_price_record['Link'] != null) {
                        echo '<a href="' . CreateWebURL($widget_price_record['Link']) . '" class="btn btn-white btn-lg rounded-pill py-3 px-md-5 mt-0 mt-md-3">' . $this->config->item('front_home_price_section_link_heading_text') . '</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (isset($page_data['Section_1_Title']) && isset($page_data['Section_1_Content'])) { ?>
    <div class="page-content-section py-3 py-md-5">
        <div class="container">
            <div class="row">
                <div class="offset-lg-1 col-lg-10 text-center">
                    <h2 class="h1 mb-3 mb-md-4"><?php echo $page_data['Section_1_Title'] ?></h2>
                    <?php echo $page_data['Section_1_Content'] ?>
                </div>
                <div class="offset-lg-1 col-lg-10 pt-3 pt-md-5">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 px-3 px-md-4 text-center dotted">
                            <i class="icon l2l-laundry fa-3x py-4"></i>
                            <h4 class="mb-4 h5">Dry Cleaning</h4>
                            <ul class="list-group list-group-area list-group-flush">
                                <li class="list-group-item list-group-item-light">Blouses</li>
                                <li class="list-group-item list-group-item-secondary">Dresses</li>
                                <li class="list-group-item list-group-item-light">Outdoor Wear</li>
                                <li class="list-group-item list-group-item-secondary">Knitwear</li>
                                <li class="list-group-item list-group-item-light">Jackets</li>
                                <li class="list-group-item list-group-item-secondary">Towelling</li>
                                <li class="list-group-item list-group-item-light">Accessories</li>
                            </ul>
                            <hr class="bg-primary pt-1 border-0 rounded-pill mx-4" />
                        </div>
                        <div class="col-lg-3 col-md-6 px-3 px-md-4 text-center dotted">
                            <i class="icon l2l-iron fa-3x py-4"></i>
                            <h4 class="mb-4 h5">Ironing</h4>
                            <ul class="list-group list-group-area list-group-flush">
                                <li class="list-group-item list-group-item-light">Suits</li>
                                <li class="list-group-item list-group-item-secondary">Dresses</li>
                                <li class="list-group-item list-group-item-light">Blouses</li>
                                <li class="list-group-item list-group-item-secondary">Skirts</li>
                                <li class="list-group-item list-group-item-light">Jeans</li>
                                <li class="list-group-item list-group-item-secondary">Duvet Covers</li>
                                <li class="list-group-item list-group-item-light">Bedding</li>
                            </ul>
                            <hr class="bg-primary pt-1 border-0 rounded-pill mx-4" />
                        </div>
                        <div class="col-lg-3 col-md-6 px-3 px-md-4 text-center dotted">
                            <i class="icon l2l-alterations fa-3x py-4"></i>
                            <h4 class="mb-4 h5">Alterations</h4>
                            <ul class="list-group list-group-area list-group-flush">
                                <li class="list-group-item list-group-item-light">Patching</li>
                                <li class="list-group-item list-group-item-secondary">Invisible Zips</li>
                                <li class="list-group-item list-group-item-light">Seams</li>
                                <li class="list-group-item list-group-item-secondary">Trousers</li>
                                <li class="list-group-item list-group-item-light">Skirt</li>
                                <li class="list-group-item list-group-item-secondary">Dress Length</li>
                                <li class="list-group-item list-group-item-light">Accessories</li>
                            </ul>
                            <hr class="bg-primary pt-1 border-0 rounded-pill mx-4" />
                        </div>
                        <div class="col-lg-3 col-md-6 px-3 px-md-4 text-center dotted">
                            <i class="icon l2l-shoe-repair fa-3x py-4"></i>
                            <h4 class="mb-4 h5">Shoe Repair</h4>
                            <ul class="list-group list-group-area list-group-flush">
                                <li class="list-group-item list-group-item-light">Shoe Cleaning</li>
                                <li class="list-group-item list-group-item-secondary">Boots Cleaning</li>
                                <li class="list-group-item list-group-item-light">New Insoles</li>
                                <li class="list-group-item list-group-item-secondary">Zip Repairs</li>
                                <li class="list-group-item list-group-item-light">Stiletto Heel</li>
                                <li class="list-group-item list-group-item-secondary">Half Soles</li>
                                <li class="list-group-item list-group-item-light">Sole &amp; Heel</li>
                            </ul>
                            <hr class="bg-primary pt-1 border-0 rounded-pill mx-4" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="app-splash py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center hidden-xs">
                <img width="121" height="121" class="l2l-app-icon" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-app-icon.png' ?>" alt="Laundrette & Laundromat London" />
            </div>
            <div class="offset-lg-2 col-lg-8 text-center">
                <?php
                $data['app_inner_area'] = true;
                $this->load->view('widgets/app_download_area', $data);
                ?>
            </div>
        </div>
    </div>
</div>      
<?php if (isset($page_data['Section_2_Title']) && isset($page_data['Section_2_Content'])) { ?>
    <div class="bg-light py-3 py-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 order-2 order-md-1 align-self-center">
                    <h2 class="h1 mb-3 mb-md-4"><?php echo $page_data['Section_2_Title'] ?></h2>
                    <?php echo $page_data['Section_2_Content'] ?>
                </div>
                <div class="col-lg-4 align-self-center order-1 order-md-2">
                    <img width="300" height="660" class="img-fluid ml-auto d-none d-md-block" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-dry-cleaning.png' ?>" alt="" />
                </div>
                <?php
                /* if(isset($page_data['Section_2_Image_Name']) && !empty($page_data['Section_2_Image_Name'])){
                  if (file_exists($this->config->item("dir_upload_banner") . $page_data['Section_2_Image_Name'])) {
                  echo '<div class="col-lg-4 align-self-center order-1 order-md-2">';
                  echo '<img src="' . $this->config->item("front_banner_folder_url") . $page_data['Section_2_Image_Name'] . '" lass="img-fluid ml-auto d-block" alt="" />';
                  echo '</div>';
                  }
                  } */
                ?>

            </div>
        </div>
    </div>
<?php } ?>
<?php if ($page_data['Template'] == "Area" && isset($page_data['area_page_array'])) { ?>
    <div class="area-cover">
        <div class="map-area-canvas" data-latitude="<?php echo isset($page_data['Latitude']) ? $page_data['Latitude'] : '' ?>" data-longitude="<?php echo isset($page_data['Longitude']) ? $page_data['Longitude'] : '' ?>" style="background-image:url(<?php echo $this->config->item('front_asset_image_folder_url') . SERVER .'-map.jpg' ?>)"></div>
        <?php /* <div id="map_area_canvas" class="map-area-canvas" data-latitude="<?php echo isset($page_data['Latitude'])?$page_data['Latitude']:''?>" data-longitude="<?php echo isset($page_data['Longitude'])?$page_data['Longitude']:''?>"></div> */ ?>

        <div class="row no-gutters">
            <div class="col-md-5 offset-md-0 offset-lg-1 col-lg-4 px-0">
                <div class="listing-area">
                    <h2 class="font-weight-bold">Our Areas</h2>

                    <div class="list-area-scroll list-group">
                        <?php
                        foreach ($page_data['area_page_array'] as $rec) {

                            echo '<a href="' . CreateWebURL($rec['AccessURL']) . '" class="area-item list-group-item d-flex justify-content-between align-items-center bg-white rounded-xl py-3 px-4 mb-1" data-latitude="' . $rec['Latitude'] . '" data-longitude="' . $rec['Longitude'] . '"><span>' . $rec['Title'] . '</span>';
                            echo '<span class="badge "><i class="fa fa-chevron-right"></i></span></a>';
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text') ?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">

                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe') ?>">
                    <div class="form-group">
                        <div class="input-group  border-bottom mb-3">
                            <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
                            <div class="input-group-append">
                                <button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>