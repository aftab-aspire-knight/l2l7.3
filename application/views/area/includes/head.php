<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" defer="defer" />
<link rel="preload" as="font" href="/assets/fonts/front/gothamblack-regular-webfont.woff2" type="font/woff" crossorigin>
<link rel="preload" as="font" href="/assets/fonts/front/gothamblack-regular-webfont.woff" type="font/woff" crossorigin>
<link rel="preload" as="font" href="/assets/fonts/front/gothambook-regular-webfont.woff2" type="font/woff" crossorigin>
<link rel="preload" as="font" href="/assets/fonts/front/gothambook-regular-webfont.woff" type="font/woff" crossorigin>
<link rel="preload" as="font" href="/assets/fonts/front/l2l/l2l.woff" type="font/woff" crossorigin>
<?php
include("head/" . SERVER . ".php")
?>
<link rel="preconnect" href="https://www.googletagmanager.com/" crossorigin />
<link rel="preconnect" href="https://cdnjs.cloudflare.com/" crossorigin />
<link rel="preconnect" href="https://snap.licdn.com/" crossorigin />
<link rel="preconnect" href="https://front.optimonk.com/" crossorigin />
<link rel="preconnect" href="https://ruler.nyltx.com/" crossorigin />
<link rel="preconnect" href="https://static.hotjar.com/" crossorigin />

<link rel='dns-prefetch' href='//www.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//fonts.gstatic.com' />
<link rel='dns-prefetch' href='//ajax.googleapis.com' />
<link rel='dns-prefetch' href='//apis.google.com' />
<link rel='dns-prefetch' href='//google-analytics.com' />
<link rel='dns-prefetch' href='//www.google-analytics.com' />
<link rel='dns-prefetch' href='//ssl.google-analytics.com' />
<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
<link rel='dns-prefetch' href='//ruler.nyltx.com' />
<link rel='dns-prefetch' href='//connect.facebook.net' />
<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
<?php if($current_url == "register" || $current_url == "login"){ ?>
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $this->config->item('recaptcha_copy_key'); ?>" defer="defer"></script>
<?php } ?>