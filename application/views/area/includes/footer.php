<?php
echo $this->carabiner->display('Area_Page_Template_JQuery');
if (isset($new_register_pop_show)) {
?>
    <script type="text/javascript">
        jQuery('#new_register_pop').magnificPopup({
            type: 'inline',
            mainClass: 'mfp-with-zoom',
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out'
            }
        });
        jQuery('#new_register_pop').trigger("click");
    </script>
    <?php
}
include 'footer/' . SERVER . '.php';
include 'body/' . SERVER . '.php';	
?>

</body>
</html>