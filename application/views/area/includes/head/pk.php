<script>
    var site_url = "<?php echo $this->config->item('base_url') ?>";
    var dir = "";
    var iso_code = "<?php echo $this->config->item('iso_code') ?>";
    var currency = "<?php echo $this->config->item('currency') ?>";
    var currency_symbol = "<?php echo $this->config->item('currency_symbol') ?>";
    var currency_code = "<?php echo $this->config->item('currency_code') ?>";
    var currency_sign = "<?php echo $this->config->item('currency_sign') ?>";
    var intl_tel = "<?php echo $this->config->item('intl_tel') ?>";
    var api_key = "<?php echo $this->config->item('loqate_key') ?>";
    var loq_api_url = "<?php echo $this->config->item('loqate_api_url') ?>";
    var iso_country_code = "<?php echo $this->config->item('iso_country_code') ?>";
    var locality = "<?php echo $this->config->item('locality') ?>";
    var post_code_length = "<?php echo $this->config->item('post_code_length') ?>";
    var recaptcha_copy_key = "<?php echo $this->config->item('recaptcha_copy_key') ?>";
    var facebook_app_id = "<?php echo $this->config->item('facebook_app_id') ?>";
    var google_client_id = "<?php echo $this->config->item('google_client_id') ?>";
</script>

<!-- Google Analytics Code for LOVE2LAUNDRY -->
<script type="text/javascript">
    (function (a, e, c, f, g, b, d) {
        var
                h = {ak: "965891363", cl: "pkDyCLH4q1wQo6rJzAM"};
        a[c] = a[c] ||
                function () {
                    (a[c].q = a[c].q || []).push(arguments)
                };
        a[f] ||
                (a[f] = h.ak);
        b = e.createElement(g);
        b.async = 1;
        b.src = "//www.gstatic.com/wcm/loader.js";
        d = e.getElementsByTagName(g)[0];
        d.parentNode.insertBefore(b, d);
        a._googWcmGet = function (b, d, e) {
            a[c](2, b, h, d, null, new
                    Date, e)
        }
    })(window, document, "_googWcmImpl", "_googWcmAk", "script");
</script>
<script>(function (w, d, t, r, u) {
        var f, n, i;
        w[u] = w[u] || [], f = function () {
            var o = {ti: "5078868"};
            o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
        }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
            var s = this.readyState;
            s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
        }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
    })(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5078868&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- TrustBox script --> <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.sync.bootstrap.min.js" async></script> <!-- End Trustbox script -->
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq)
            return;
        n = f.fbq = function () {
            n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '397101313972207'); // Insert your pixel ID here.
    fbq('track', 'PageView');
<?php
if ($current_url == $this->config->item('front_address_selection_page_url')) {
    echo "fbq('track', 'AddPaymentInfo');";
} else if ($current_url == $this->config->item('front_check_out_page_url')) {
    echo "fbq('track', 'InitiateCheckout');";
} else if ($current_url == $this->config->item('front_thank_you_page_url')) {
    if (isset($invoice_fb_total)) {
        echo "fbq('track', 'CompleteRegistration', {value:" . $invoice_fb_total . ",currency: 'GBP'});";
    }
}
?>
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=397101313972207&ev=PageView&noscript=1"
               /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<script>
    window.addEventListener("load", function () {
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#000"
                },
                "button": {
                    "background": "#40bcdc",
                    "text": "#ffffff"
                }
            },
            "position": "bottom-left",
            "content": {
                "message": "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies.",
                "link": "Find out more here",
                "href": "https://www.love2laundry.ae/t-c.html#cookie"
            }
        })
    });
</script>


<!--EMBED THE FRESHCHAT WIDGET CODE 5-16-2018 -->
<script src="https://wchat.freshchat.com/js/widget.js"></script>
<!--EMBED THE FRESHCHAT WIDGET CODE 5-16-2018 ends -->
<!-- Hotjar Tracking Code for https://www.love2laundry.pk -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1876695,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-K6VLGSX');
</script>
<!-- End Google Tag Manager -->
