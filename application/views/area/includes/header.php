<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php echo isset($page_data['MetaTitle']) ? $page_data['MetaTitle'] : $header_title; ?></title>
        <?php if (isset($page_data['MetaDescription'])) { ?>
            <meta name="description" content="<?php echo html_entity_decode($page_data['MetaDescription']); ?>">
        <?php } if (isset($page_data['MetaKeyword'])) { ?>
            <meta name="keywords" content="<?php echo $page_data['MetaKeyword']; ?>">
        <?php
        } if (isset($page_data['NoIndexFollowTag'])) {
            if (($page_data['NoIndexFollowTag'] == "Yes")) {
                echo ' <meta name="robots" content="noindex,nofollow" />';
            }
        }
        echo '<link rel="canonical" href="' . base_url($this->uri->uri_string()) . '" />';
        ?>
        <!-- Fav and touch icons -->
        <link rel="shortcut icon" type="image/x-icon"
              href="<?php echo base_url('assets/images/front/l2l-fav/favicon.ico') ?>"/>
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-57x57.png') ?>"/>
        <link rel="apple-touch-icon" sizes="72x72"
              href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-72x72.png') ?>"/>
        <link rel="apple-touch-icon" sizes="114x114"
              href="<?php echo base_url('assets/images/front/l2l-fav/apple-icon-114x114.png') ?>"/>
              <?php
              echo $this->carabiner->display('Area_Template_Css');
              include 'head.php';
              ?>
    </head>
    <body id="body_area">
        <header class="header page-header bg-light">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-2 col-md-12 col-12">
                        <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon text-white"></span>
                        </button>
                        <a class="logo" href="<?php echo site_url() ?>" rel="home"><img width="100" height="100" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo-black.png' ?>" alt="Love2Laundry.com" /></a>
                        <a class="logo-fixed" href="<?php echo site_url() ?>" rel="home"><img width="140" height="69" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo.png' ?>" alt="Love2Laundry.com"></a>
                    </div>
<?php if (isset($header_menus_records) && sizeof($header_menus_records) > 0) { ?>
                        <div class="col-lg-10 col-md-12 col-12 text-right" id="main_menu">
                            <nav class="navigation navbar navbar-expand-lg" id="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                                <div class="collapse navbar-collapse" id="navbarResponsive">
                                    <ul class="main-menu">
                                        <?php
                                        foreach ($header_menus_records as $menu):
                                            $current_class = '';
                                            if ($current_url == $menu['PageURL']) {
                                                $current_class = ' class="active"';
                                            }
                                            echo '<li' . $current_class . '><a href="' . CreateWebURL($menu['PageURL']) . '">' . $menu['PageName'] . '</a></li>';
                                        endforeach;
                                        if (isset($is_member_login)) {
                                            ?>
                                            <li>
                                                <a href="<?php echo base_url($this->config->item('front_dashboard_page_url')) ?>"><?php echo $this->config->item('front_header_dashboard_heading_text') ?></a>
                                            </li>
                                            <li class="sign-in">
                                                <a class="logout" href="javascript:void(0)"><?php echo $this->config->item('front_header_log_out_heading_text') ?></a>
                                            </li>
    <?php } else { ?>
                                            <li class="sign-in">
                                                <a href="<?php echo base_url($this->config->item('front_login_page_url')) ?>"><?php echo $this->config->item('front_header_sign_in_heading_text') ?></a>
                                            </li>
    <?php } ?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
<?php } ?>
                </div>
            </div>
        </header>