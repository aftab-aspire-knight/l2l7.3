<?php if (!isset($is_order_page)) { ?>
    <footer class="footer">
        <div class="footer-main-nav pt-4 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <?php if (isset($service_menus_records)) { ?>
                        <div class="col-md-6 col-lg-3 mb-3">
                            <h4>About</h4>
                            <?php
                            /*
                              echo '<ul class="list-angle-right">';
                              foreach ($service_menus_records as $menu):
                              echo '<li><a href="' . CreateWebURL($menu['PageURL']) . '">' . $menu['PageName'] . '</a></li>';
                              endforeach;
                              echo '</ul>';
                             */
                            ?>
                            <ul class="list-angle-right">
                                <li><a href="<?php echo base_url('about-love-2-laundry') ?>">About L2L</a></li>
                                <li><a href="<?php echo base_url('pricing') ?>">Pricing</a></li>
                                <li><a target="_blank" href="https://lov2laundry.freshdesk.com/support/solutions" rel="noopener">FAQ's</a></li>
                                <li><a href="<?php echo base_url('contact-us') ?>">Contact Us</a></li>
                                <li><a href="<?php echo base_url('all-areas') ?>">All Areas</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-6 col-lg-3 mb-3">
                        <h4>Other</h4>
                        <ul class="list-angle-right">
                            <li><a href="<?php echo base_url('refer-a-friend') ?>">Refer friends</a></li>
                            <li><a href="<?php echo base_url('partner') ?>">Partner</a></li>
                            <li><a href="<?php echo base_url('t-c') ?>">Terms</a></li>
                            <li><a href="https://www.love2laundry.com/blog/">Blog</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3 mb-3">
                        <h4>Services</h4>
                        <ul class="list-angle-right">
                            <li><a href="<?php echo base_url('airbnb-laundry-service') ?>">For Airbnb hosts</a></li>
                            <li><a href="<?php echo base_url('dry-cleaners-near-me') ?>">Dry Cleaners Near Me</a></li>
                            <li><a href="<?php echo base_url('ironing-services') ?>">Ironing Services</a></li>
                            <li><a href="<?php echo base_url('laundry-services') ?>">Laundry Services</a></li>
                            <li><a href="<?php echo base_url('business-services') ?>">Corporate</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-lg-3 mb-3">
                        <h4>Apps</h4>
                        <ul class="list-angle-right">
                            <li><a href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>" target="_blank">Love2laundry iOS</a></li>
                            <li><a href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>" target="_blank">Love2laundry Android</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-locations">
            <div class="container px-0 px-md-3">
                <h4 class="text-dark border-top pt-3">Love2Laundry in UK</h4>
                <div class="footer-locations-menu py-3">
                    <?php /*
                      if ($this->config->item('front_footer_cleaner_sub_heading_text') != "") {
                      echo '<h5>' . $this->config->item('front_footer_cleaner_sub_heading_text') . '</h5>';
                      }
                      <ul class="list-unstyled d-none d-md-table">
                      <?php
                      foreach ($cleaner_menus_records as $menu):
                      echo '<li><a href="' . CreateWebURL($menu['PageURL']) . '">' . $menu['PageName'] . '</a></li>';
                      endforeach;
                      ?>
                      </ul> */ ?>
                    <ul class="list-unstyled d-none d-md-table">
                        <li><a href="<?php echo base_url('central-london') ?>">London</a></li>
                        <li><a href="<?php echo base_url('greater-manchester') ?>">Manchester</a></li>
                        <li><a href="<?php echo base_url('birmingham-city-centre') ?>">Birmingham</a></li>
                        <li><a href="<?php echo base_url('greater-manchester/bolton') ?>">Bolton</a></li>
                        <li><a href="<?php echo base_url('greater-manchester/bury') ?>">Bury</a></li>
                    </ul>
                </div>
                <h4 class="text-dark">Love2Laundry Abroad</h4>
                <div class="footer-countries list-countries list-group list-group-horizontal-lg rounded-0">
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="https://www.love2laundry.com"><span class="d-flex align-items-center"><img class="mr-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'uk.svg' ?>" alt="UK">United Kingdom</span> <span class="badge ml-3"><i class="fa fa-chevron-right"></i></span></a>
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="https://www.love2laundry.nl"><span class="d-flex align-items-center"><img class="mr-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'nl.svg' ?>" alt="Nl">Netherlands</span> <span class="badge ml-3"><i class="fa fa-chevron-right"></i></span></a>
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="https://www.love2laundry.ae"><span class="d-flex align-items-center"><img class="mr-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'ae.svg' ?>" alt="UAE">UAE</span> <span class="badge ml-3"><i class="fa fa-chevron-right"></i></span></a>
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="https://www.kuwait.love2laundry.com"><span class="d-flex align-items-center"><img class="mr-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'kwd.svg' ?>" alt="KWD">Kuwait</span> <span class="badge ml-3"><i class="fa fa-chevron-right"></i></span></a>
                    <a class="list-group-item d-flex justify-content-between align-items-center" href="https://www.love2laundry.pk"><span class="d-flex align-items-center"><img class="mr-3" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'pk.svg' ?>" alt="Pakistan">Pakistan</span> <span class="badge ml-3"><i class="fa fa-chevron-right"></i></span></a>
                </div>
            </div>
        </div>
        <div class="footer-text">
            <div class="container">
                <div class="row copyright align-items-center">
                    <div class="col-md-6 col-lg-4">
                        <?php echo isset($footer_title) ? '<p>' . $footer_title . '</p>' : ''; ?>
                    </div>
                    <div class="col-md-6 col-lg-4 text-md-right text-lg-center pt-4">
                        <?php if (isset($facebook_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $facebook_link ?>"><i class="fab fa-facebook-square fa-2x"></i></a>
                        <?php } if (isset($twitter_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $twitter_link ?>"><i class="fab fa-twitter-square fa-2x"></i></a>
                        <?php } if (isset($google_plus_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $google_plus_link ?>"><i class="fab fa-google-plus-square fa-2x"></i></a>
                        <?php } if (isset($you_tube_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $you_tube_link ?>"><i class="fab fa-youtube-square fa-2x"></i></a>
                        <?php }if (isset($pinterest_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $pinterest_link ?>"><i class="fab fa-pinterest-square fa-2x"></i></a>
                        <?php } if (isset($linked_in_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $linked_in_link ?>"><i class="fab fa-linkedin-square fa-2x"></i></a>
                        <?php } if (isset($instagram_link)) { ?>
                            <a class="mx-1" target="_blank" href="<?php echo $instagram_link ?>"><i class="fab fa-instagram fa-2x"></i></a>
                        <?php } ?>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="payment-accepted">
                            <span class="payment-text">Accepted payment methods:</span>
                            <span class="fab fa-cc-visa fa-2x px-1"></span>
                            <span class="fab fa-cc-mastercard fa-2x px-1"></span>
                            <span class="fab fa-cc-discover fa-2x px-1"></span>
                            <span class="fab fa-cc-amex fa-2x px-1"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </footer>
<?php } else if (isset($is_order_page) && $is_order_page == true && $show_minimum_order == true) { ?>
    <a href="#" class="show-cart-bar hidden-xs" id="btn_show_cart_area">
        <i class="l2l-cart"></i>
    </a>

    <a id="btn_category_popup_message" href="#category_popup_message" data-effect="mfp-zoom-in" class="hide-area">Zoom</a>
    <div id="category_popup_message" class="white-popup mfp-with-anim mfp-hide text-center">
    </div>
<?php } if (isset($new_register_pop_show)) {
    ?>
    <a id="new_register_pop" href="#new_register_popup" data-effect="mfp-zoom-in" class="hide-area">Zoom</a>
    <div id="new_register_popup" class="white-popup mfp-with-anim mfp-hide text-center">
        <h3>Thanks For Registering with Us</h3>
        <p>Thanks for registering with Love2Laundry. Please click below to place your first order & get 25% off. By Using the code "TRYME25"</p>
        <a href="<?php echo base_url() ?>" class="btn btn-primary btn-block btn-lg">Order Now</a>
    </div>
<?php
}

?>
<!--EMBED THE FRESHCHAT WIDGET CODE 5-16-2018 ends -->
<style>

    #wh-widget-send-button{
        margin-bottom:50px !important;
    }

</style>
<!-- WhatsHelp.io widget -->
<div class="whatsapp">
    <a target="_blank" href="https://api.whatsapp.com/send?phone=447951230768">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" class="wh-messenger-svg-whatsapp wh-svg-icon"><path d=" M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path></svg>
    </a>
</div>
<!-- /WhatsHelp.io widget -->
