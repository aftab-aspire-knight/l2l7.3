<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53FH3CM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Ruler Analytics Conversion -->
<script type="text/javascript">
    var __raconfig = __raconfig || {};
    __raconfig.uid = '5a01d9999018c';
    __raconfig.action = 'track';
    (function () {
        var ra = document.createElement('script');
        ra.type = 'text/javascript';
        ra.src = 'https://ruler.nyltx.com/lib/1.0/ra-bootstrap.min.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ra, s);
    }());
</script>
<!-- Twitter Code Conversion -->
<script src="//platform.twitter.com/oct.js" defer="defer" type="text/javascript"></script>
<script type="text/javascript">
    window.onload = function ()
    {

        twttr.conversion.trackPid('l5d35');
    }
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l5d35&p_id=Twitter" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l5d35&p_id=Twitter" /></noscript>
<script type="text/javascript">
    (function (e, a) {
        var t, r = e.getElementsByTagName("head")[0], c = e.location.protocol;
        t = e.createElement("script");
        t.type = "text/javascript";
        t.charset = "utf-8";
        t.async = !0;
        t.defer = !0;
        t.src = c + "//front.optimonk.com/public/" + a + "/js/preload.js";
        r.appendChild(t);
    })(document, "17739");
</script>
<!-- Linkedin Track Code Conversion -->
<script type="text/javascript"> _linkedin_data_partner_id = "154484";</script>
<script type="text/javascript"> (function () {
        var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";
        b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);
    })();</script>
<noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=154484&fmt=gif" /> </noscript>
