<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6VLGSX"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
if (isset($is_home_page)) {
    ?> 
    <div class="crowd-header">
        <div class="container">

            <div class="text text-center">
                <i class="mt-3 ml-4 icon l2l-gift l2l-lg"></i>  
                <!-- Get 25% off your first order with code TRYME25 -->
                <span style="font-size:15px;">COVID-19; ‘business as usual’, government guidelines strictly followed, contactless collection and return.</span>
                <?php /* <a href="<?php echo base_url() ?>login" target="_blank">friend you refer &gt;&gt;</a> */ ?>
                <button id="btn_refer_close" class="btn-refer-close">×</button>
            </div>

        </div>
    </div>
    <?php
}
?>