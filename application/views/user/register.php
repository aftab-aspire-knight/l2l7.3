<script>
    var address1 = "";
    var address2 = "";
</script>
<?php
$this->config->load('country_codes');
$codes = $this->config->item("codes");
$postal_code_type = $this->config->item("postal_code_type");
?>
<div class="signin align-items-center d-flex px-3 px-md-0 register-content py-3 py-md-0">
    <form id="frm_register" name="frm_register" method="post" action="<?php echo base_url('register') ?>" class="col-md-8 form-signin custom-form px-md-5 py-4 rounded-xl bg-white">
        <input type="hidden" id="_token" name="_token" value="" />
        <input type="hidden" id="town" name="town" value="" />
        <input type="hidden" value="+44" readonly />
        <input type="hidden" id="action" name="action" value="register" />
        <input type="hidden" id="social_id" name="social_id" value="<?php echo $social_id != "" ? $social_id : "" ?>" />
        <input type="hidden" id="type" name="type" value="<?php echo $type != "" ? $type : "" ?>" />

        <a href="<?php echo base_url() ?>"><img class="mb-3 d-table mx-auto" width="160" src="<?php echo base_url("assets/images/front/logo.png") ?>"alt="Love2Laundry.com"></a>
        <div class="hide-area reg-area"></div>

        
        <div class="row" >
        <?php /*?>
            <div class="col-md-6 col-12 mb-2 mb-md-4">
                <button type="button" class="btn btn-block loginBtn loginBtn--facebook rounded-pill" onclick="checkLoginState();">Facebook</button>
            </div> <?php */?>
            <div class="col-md-6 col-12 mb-2 mb-md-4">
                <div class="google-btn rounded-pill" id="gs2" data-onsuccess="onSuccess"></div>
            </div>
        </div>
       

        <div class="row" >


            <div class="col-md-6">
                <div class="form-group mb-2 mb-md-3">
                    <label>First Name</label>
                    <input class="form-control form-control-lg req" type="text" data-error-message="Please enter first name"  placeholder="First Name" id="reg_first_name" name="reg_first_name" value="<?php echo $reg_first_name != "" ? $reg_first_name : "" ?>"  />
                </div>
                <div class="form-group mb-2 mb-md-3">
                    <label>Last Name</label>
                    <input class="form-control form-control-lg req" data-error-message="Please enter last name" placeholder="Last Name" id="reg_last_name" name="reg_last_name" value="<?php echo $reg_last_name != "" ? $reg_last_name : "" ?>" />
                </div>
                <div class="form-group mb-2 mb-md-3 ">
                    <label>Phone</label>
                    <div class="input-group ">
                        <div class="input-group-prepend country-code"> 
                            <span class="input-text-country">+<?php echo $this->config->item("country_phone_code") ?></span>
                            <select class="custom-select custom-select-lg" id="countryCode" name="countryCode" >

                                <?php
                                foreach ($codes as $key => $country) {
                                    ?>
                                    <option value="<?php echo $key ?>" <?php if ($key == $this->config->item("country_phone_code")) { ?> selected <?php } ?>><?php echo $country ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <input class="form-control form-control-lg only-number req" type="tel" data-error-message="Please enter phone number"  placeholder="<?php echo $this->config->item("phone_placeholder") ?>" id="reg_phone_number" name="reg_phone_number" value="" />
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-2 mb-md-3">
                    <label>Email</label>
                    <input type="email" class="form-control form-control-lg req" data-error-message="Please enter email address" data-invalid-error-message="Please enter valid email address" id="reg_email_address" name="reg_email_address" placeholder="Email Address" value="<?php echo $reg_email != "" ? $reg_email : "" ?>" />
                </div>
                <div class="form-group mb-2 mb-md-3">
                    <label>Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control form-control-lg req" aria-label="password" data-error-message="Please enter password" placeholder="Password" id="reg_password" name="reg_password" value="" >
                        <div class="input-group-append" id="btn_password_show">
                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-2 mb-md-3">
                    <label><?php echo $postal_code_type["label"]; ?></label>
                    <input class="form-control form-control-lg req" type="text" data-error-message="Please enter postcode" placeholder="<?php echo $postal_code_type["placeholder"]; ?>" id="register_post_code" name="register_post_code" />

                </div>
                <div class="form-group mb-2 mb-md-3" style="display: none;" id="div_addresses">
                    <select  id="addresses" name="addresses" class="form-control" >
                    </select>
                </div>
                <div class="form-group mb-2 mb-md-3" id="div_reg_address1" style="display: none;">
                    <label>Address</label>
                    <input type="text" data-error-message="Please enter your address" placeholder="Enter your address" class="form-control form-control-lg p-field address-building " id="reg_address" name="reg_address" />
                    <div class="search-result" style="display: none;" id="google_addresses"></div>
                </div>

                <div class="form-group mb-2 mb-md-3">
                    <label>Specify any extra address details</label>
                    <input type="text" data-error-message="Please enter address" placeholder="Please specify any extra address details" class="form-control form-control-lg p-field address-building " id="reg_address2" name="reg_address2" />
                </div>
            </div>
            <div class="col-md-12 text-center">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" data-error-message="Please agree to our terms and conditions" name="term_condition" class="custom-control-input" id="term_condition">
                    <label class="custom-control-label text-capitalize" for="term_condition">I agree to the Love 2 Laundry <a target="_blank" href="<?php echo base_url($this->config->item('front_term_page_url')) ?>">Terms And Conditions</a></label>
                </div>

            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-primary rounded-pill py-3 px-5 mx-auto mb-3 d-table my-3 btn-reg-next" type="submit" data-next-tab="submit"><span class="px-5">Register</span></button>
                <div class="text-center">
                    <a class="h6 mr-1" href="<?php echo base_url() ?>" class="links">Love2Laundry</a> &bull; <a class="h6 ml-1" href="<?php echo base_url("login") ?>" class="links">Login to my Account</a>            
                </div>

            </div>
        </div>


    </form>
</div>