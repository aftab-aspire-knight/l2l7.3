
<div class="signin align-items-center d-flex px-3 px-md-0">
    <form class="col-md-4 form-signin custom-form px-md-5 py-4 rounded-xl bg-white" id="frm_login" name="frm_login" method="post" action="<?php echo base_url('login') ?>">
        <a href="<?php echo base_url() ?>"><img class="mb-3 d-table mx-auto" width="160" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'logo.png' ?>" alt="<?php echo $this->config->item('website_name') ?>"></a>

        

        <div class="row" >
        <?php /*?>
            <div class="col-md-6 col-12 mb-2 mb-md-4">
                <button type="button" class="btn btn-block loginBtn loginBtn--facebook rounded-pill" onclick="checkLoginState();">Facebook</button>

                <!--
                <fb:login-button class="fb-login-button" data-width="100%" data-size="large" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="false" scope="public_profile,email" onlogin="checkLoginState();">
                </fb:login-button>
                -->
            </div>
            <?php */?>
            <div class="col-md-6 col-12 mb-2 mb-md-4">
                <!--<div class="g-signin2" data-onsuccess="onSuccess" data-theme="dark"></div>-->


                <div class="google-btn rounded-pill" id="gs2" data-onsuccess="onSuccess"></div>

            </div>
        </div>
        

        <div class="form-group mb-3">
            <label for="login_email_address">Email</label>
            <input class="form-control form-control-lg" type="text" name="login_email_address" placeholder="" />
        </div>
        <div class="form-group mb-3">
            <label for="login_password">Password</label>
            <input class="form-control form-control-lg" type="password" name="login_password" placeholder="" />
        </div>
        <div class="form-group mb-3">
            <div class="form-row align-items-center">
                <div class="col-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="login_remember" name="login_remember" value="remember">
                        <label class="custom-control-label text-capitalize" for="login_remember">Remember me</label>
                    </div>
                </div>
                <div class="col-6 text-right"><button type="button" data-toggle="modal" data-target="#forgotModal" class="bg-transparent border-0 small">Forgot password</button></div>
            </div>
        </div>

        <button type="submit" id="btn_login" name="btn_login" class="btn btn-primary btn-lg rounded-pill py-2 px-5 mx-auto mb-3 d-table"><span class="px-5">Sign in</span></button>
        <div class="text-center">
            <a href="<?php echo base_url($this->config->item('front_register_page_url')) ?>" class="btn btn-outline-dark btn-lg rounded-pill py-2 px-5 mb-2" type="submit">Create an account</a>
        </div>
    </form>
</div>
<!-- Modal -->
<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="forgotModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <form class="custom-form" id="frm_forgot" name="frm_forgot" method="post" action="<?php echo base_url('forgot') ?>">
                <div class="modal-header bg-primary border-light">
                    <h5 class="modal-title font-weight-bold" id="discountModalCenterTitle">Forgot Password?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="order-form">
                        <p class="h6">Just let us know Your Registered Email</p>
                        <div id="msg_box"></div>
                        <div class="form-group mb-0">
                            <label for="login_password">Password</label>
                            <input class="form-control form-control-lg" placeholder="Enter Your Email Address" id="forgot_email_address" name="forgot_email_address" />
                        </div>						
                    </div>
                </div>
                <div class="modal-footer border-light">
                    <button type="submit" id="btn_forgot" name="btn_forgot"  class="btn btn-dark btn-block btn-lg">Send Me My Password</button>
                </div>
            </form>
        </div>
    </div>
</div>