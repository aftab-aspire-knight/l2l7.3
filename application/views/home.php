<?php
include("home/" . SERVER . ".php");
?>
<div class="featured bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-3">
                <img width="94" height="100" class="lcn-award-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'lcn-award.png' ?>" alt="LCN Award" />
            </div>
            <div class="col-6 col-md-3">
                <img width="130" height="68" class="les-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'london-evening-standard.png' ?>" alt="London Evening Standard">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="bcs-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'best-cleaning-service.png' ?>" alt="Best Cleaning Service">
            </div>
            <div class="col-6 col-md-3">
                <img width="97" height="100" class="price-promise-image" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'price-promise.png' ?>" alt="Laundry Price Promise" />
            </div>
        </div>
    </div>
</div>
<?php if (isset($widget_work_section_navigation_records)) { ?>
    <div class="how-it-work" id="hiw">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-center text-uppercase">Dry Cleaning & Laundry Made Easy</h4>
                    <h2 class="text-center">How Love2Laundry Works</h2>
                </div>
                <?php
                $count = 0;
                $image = array(
                    $this->config->item('front_asset_image_folder_url') . 'hw-1.png',
                    $this->config->item('front_asset_image_folder_url') . 'hw-2.png',
                    $this->config->item('front_asset_image_folder_url') . 'laundry-wash.png',
                    $this->config->item('front_asset_image_folder_url') . 'delivery-guy.png'
                );
                
                foreach ($widget_work_section_navigation_records as $record) {
                    echo '<div class="col-6 col-lg-3 col-md-6 text-center py-3 py-md-2 dotted">';
                    echo '<h3>' . $record["Title"] . '</h3>';
                    $image_name = $this->config->item('front_dummy_image_url');
                    if ($record['ImageName'] != "" && $record['ImageName'] != null) {
                        if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                            $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                        }
                    }
                    echo '<div class="how-it-work-image rounded-circle mx-auto d-flex align-items-center justify-content-center"><img src="' . $image[$count] . '" width="100" height="80" class="img-fluid" alt="" /></div>';
                    echo '<span class="how-it-work-counter">0' . ($count + 1) . '</span>';
                    echo $record["Content"];
                    echo '</div>';
                    $count++;
                }
                ?>
                <div class="col-md-12 text-center pt-3 pt-md-5 how-it-work-button">
                    <a href="<?php echo base_url('booking') ?>" class="btn btn-primary btn-lg text-uppercase rounded-pill">Order Now</a>
                </div>
            </div>
        </div>
    </div>
<?php } ///////?>
<div class="py-3 py-md-4 bg-dark">
    <div class="container py-3">
        <!-- TrustBox widget - Carousel --> <div class="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8912dec7e10d38f59f36" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="130px" data-style-width="100%" data-theme="dark" data-stars="5" data-schema-type="Organization"> <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a> </div> <!-- End TrustBox widget -->
    </div>
</div>

<div class="laundry-prices py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center laundry-prices-content">
                <?php
                if ($this->config->item('front_home_price_section_heading_text') != "") {
                    echo '<h4 class="text-uppercase">' . $this->config->item('front_home_price_section_heading_text') . '</h4>';
                }
                echo '<h2>' . $widget_price_record['Title'] . '</h2>';
                if ($widget_price_record['Content'] != "" && $widget_price_record['Content'] != null && strlen($widget_price_record['Content']) > 5) {
                    echo $widget_price_record['Content'];
                }
                ?>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <?php
                    if (isset($widget_price_section_navigation_records)) {
                        foreach ($widget_price_section_navigation_records as $record) {
                            echo '<div class="col-6 col-sm-6 col-lg-3 py-3 py-lg-0">';
                            echo '<div class="laundry-prices-block">';
                            echo '<img class="laundry-prices-promise pulse" src="' . $this->config->item('front_asset_image_folder_url') . 'price-promise-stamp.png" width="97" height="95" alt="Laundry Price Promise" />';
                            echo '<div class="laundry-prices-inner">';
                            $image_name = $this->config->item('front_dummy_image_url');
                            if ($record['ImageName'] != "" && $record['ImageName'] != null) {
                                if (file_exists($this->config->item("dir_upload_widget") . $record['ImageName'])) {
                                    $image_name = $this->config->item("front_widget_folder_url") . $record['ImageName'];
                                }
                            }
                            echo '<img width="70" height="86" class="laundry-prices-image" src="' . $image_name . '" alt="' . $record['Title'] . '" />';
                            echo '<div class="laundry-prices-heading">' . $record['Title'] . '</div>';
                            echo '</div>';
                            echo '</div>';
                            if ($record['Content'] != "" && $record['Content'] != null && strlen($record['Content']) > 2) {
                                echo $record['Content'];
                            }
                            echo '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-12 text-center py-3">
                <?php
                if ($widget_price_record['Link'] != "" && $widget_price_record['Link'] != null) {
                    echo '<a href="' . CreateWebURL($widget_price_record['Link']) . '" class="btn btn-white btn-lg rounded-pill py-3 px-md-5 mt-0 mt-md-3">' . $this->config->item('front_home_price_section_link_heading_text') . '</a>';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="app-splash py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-center pb-5 pb-md-0">
                    <img width="626" height="383" class="img-fluid" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'excellent-laundry-cleaning-quality.png' ?>" alt="Laundrette & Laundromat" />	
                    <a href="<?php echo base_url('booking') ?>" class="btn btn-white btn-lg rounded-pill py-3 px-md-5 schedule-collection">SCHEDULE A COLLECTION</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-center hidden-xs">
                    <img width="121" height="121" class="l2l-app-icon" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'laundry-cleaning-app-icon.png' ?>" alt="Laundrette & Laundromat" />
                </div>
                <div class=" text-center">
                    <?php
                    $data['app_inner_area'] = true;
                    $this->load->view('widgets/app_download_area', $data);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4>Love2laundry <a class="text-white" href="<?php echo base_url('airbnb-laundry-service') ?>">short-let</a> linen services</h4>
            </div>
            <div class="col-md-6">
                <h4><i class="fa fa-leaf" aria-hidden="true"></i> Zero-emission delivery vehicles</h4>
            </div>
        </div>
    </div>
</div>

<?php /*
<div class="newsletter bg-dark d-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-7">
                <h3><?php echo $this->config->item('front_home_mailing_section_heading_text') ?></h3>
                <p>Subscribe to our newsletter to receive special offers and company news with updates.</p>
            </div>
            <div class="col-md-6 col-lg-4">

                <form autocomplete="off" id="frm_subscriber" name="frm_subscriber" method="post" action="<?php echo base_url('subscribe') ?>">
                    <div class="form-group">
                        <div class="input-group  border-bottom mb-3">
                            <input type="text" name="subscriber_email_address" id="subscriber_email_address" class="form-control form-control-lg bg-transparent border-0 text-white rounded-0" placeholder="enter email address" aria-label="enter email address">
                            <div class="input-group-append">
                                <button type="submit" id="btn_subscriber" name="btn_subscriber" class="input-group-text bg-transparent border-0 text-white"><i class="fas fa-paper-plane fa-2x"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
*/ ?>

