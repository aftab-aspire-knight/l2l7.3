<div class="page-section top-section-bg bg-light d-md-flex align-items-center">
    <div class="top-section-container container pt-5">
        <div class="row align-items-center mt-0 mt-md-5">
            <div class="col-lg-5 col-xl-6 text-right pt-lg-5 py-3 order-1 order-md-2">
                <?php $image_name = $this->config->item('front_asset_image_folder_url') . 'laundry_and_dry_cleaning_service.png'; ?>
                <img width="610" height="500" class="img-fluid" src="<?php echo $image_name; ?>" alt="<?php echo $page_data['Title']; ?>" />
            </div>
            <div class="col-lg-7 col-xl-6 py-lg-5 py-3 order-2 order-md-1">
                <?php echo isset($page_data['HeaderTitle']) ? '<h1>' . $page_data['HeaderTitle'] . '</h1>' : '' ?>
                <h1 class="ff-gothamblack text-center text-md-left mt-md-4 mb-3">Market Leading Laundry<br> & Dry Cleaning Service</h1>
                <h2 class="h5 font-weight-bold">We collect, clean & return at a time and location of your choice.</h2>
                <div class="place-order-form">
                    <span class="title" id="info-message" ><?php echo (isset($_GET['msg']) && $_GET['msg'] != '') ? $_GET['msg'] : ''; ?></span>
                    <script>
                        setTimeout(function () {
                            document.getElementById('info-message').style.display = 'none';
                            /* or
                             var item = document.getElementById('info-message')
                             item.parentNode.removeChild(item); 
                             */
                        }, 3000);
                    </script>
                    <form autocomplete="off" id="frm_header_post_code_search" method="post" action="<?php echo base_url('check-availability') ?>">
                        <div class="place-order-field">
                            <input type="text" name="post_code" id="post_code" placeholder="enter your postcode" class="form-control">
                            <button type="button" id="btn_header_post_code_search" name="btn_header_post_code_search" class="btn btn-lg btn-primary rounded-pill text-uppercase py-3 px-3 px-lg-4 btn-order">Order Now</button>
                        </div>
                    </form>
                    <p class="collection-delivery">Collection within 1 hour, next day return.<br />Highest quality and lowest price guaranteed.</p>
                </div>
            </div>
        </div>
        <div class="row py-md-3 align-items-end">
            <div class="col-md-3 col-lg-3 col-6 d-none d-md-block">
                <div class="d-table mx-auto ml-md-0">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget l2l-trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="57d12ad90000ff000594733c" data-style-height="90px" data-style-width="130px" data-theme="light">
                        <a href="https://uk.trustpilot.com/review/www.love2laundry.com" target="_blank">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="col-md-8 col-lg-6 col-12">
                <div class="my-4 text-center">
                    <a rel="noopener noreferrer" target="_blank" class="ios" href="<?php echo isset($apple_store_link) ? $apple_store_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'appstore-badge.svg' ?>" alt="Love2Laundry - iOS"></a>
                    <a rel="noopener noreferrer" target="_blank" class="android" href="<?php echo isset($google_play_link) ? $google_play_link : '#' ?>"><img width="150" height="45" src="<?php echo $this->config->item('front_asset_image_folder_url') . 'google-play-badge.png' ?>" alt="Love2Laundry - Android"></a>
                </div>
            </div>
        </div>
    </div>
</div>
