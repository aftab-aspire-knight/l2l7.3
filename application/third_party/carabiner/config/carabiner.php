<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



$version = 1.3116 + rand(1, 100000);


/**
 * Carabiner 1.45 configuration file.
 * CodeIgniter-library for Asset Management
 */
/*
  |--------------------------------------------------------------------------
  | Script Directory
  |--------------------------------------------------------------------------
  |
  | Path to the script directory.  Relative to the CI front controller.
  |
 */

$config['script_dir'] = 'assets/js/';


/*
  |--------------------------------------------------------------------------
  | Style Directory
  |--------------------------------------------------------------------------
  |
  | Path to the style directory.  Relative to the CI front controller
  |
 */

$config['style_dir'] = 'assets/css/';

/*
  |--------------------------------------------------------------------------
  | Cache Directory
  |--------------------------------------------------------------------------
  |
  | Path to the cache directory. Must be writable. Relative to the CI
  | front controller.
  |
 */

$config['cache_dir'] = 'assets/cache/';




/*
 * The following config values are not required.  See Libraries/Carabiner.php
 * for more information.
 */



/*
  |--------------------------------------------------------------------------
  | Base URI
  |--------------------------------------------------------------------------
  |
  |  Base uri of the site, like http://www.example.com/ Defaults to the CI
  |  config value for base_url.
  |
 */

$config['base_uri'] = base_url();


/*
  |--------------------------------------------------------------------------
  | Development Flag
  |--------------------------------------------------------------------------
  |
  |  Flags whether your in a development environment or not. Defaults to FALSE.
  |
 */

$config['dev'] = FALSE;


/*
  |--------------------------------------------------------------------------
  | Combine
  |--------------------------------------------------------------------------
  |
  | Flags whether files should be combined. Defaults to TRUE.
  |
 */

$config['combine'] = FALSE;


/*
  |--------------------------------------------------------------------------
  | Minify Javascript
  |--------------------------------------------------------------------------
  |
  | Global flag for whether JS should be minified. Defaults to TRUE.
  |
 */

$config['minify_js'] = FALSE;


/*
  |--------------------------------------------------------------------------
  | Minify CSS
  |--------------------------------------------------------------------------
  |
  | Global flag for whether CSS should be minified. Defaults to TRUE.
  |
 */

$config['minify_css'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Force cURL
  |--------------------------------------------------------------------------
  |
  | Global flag for whether to force the use of cURL instead of file_get_contents()
  | Defaults to FALSE.
  |
 */

$config['force_curl'] = FALSE;


/*
  |--------------------------------------------------------------------------
  | Predifined Asset Groups
  |--------------------------------------------------------------------------
  |
  | Any groups defined here will automatically be included.  Of course, they
  | won't be displayed unless you explicity display them ( like this: $this->carabiner->display('jquery') )
  | See docs for more.
  |
  | Currently created groups:
  |	> jQuery (latest in 1.xx version)
  |	> jQuery UI (latest in 1.xx version)
  |	> Ext Core (latest in 3.xx version)
  |	> Chrome Frame (latest in 1.xx version)
  |	> Prototype (latest in 1.x.x.x version)
  |	> script.aculo.us (latest in 1.x.x version)
  |	> Mootools (1.xx version)
  |	> Dojo (latest in 1.xx version)
  |	> SWFObject (latest in 2.xx version)
  |	> YUI (latest core JS/CSS in 2.x.x version)
  |
 */

// Start Admin Css ==============================================
$config['groups']['Admin_Core_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/pace/css/pace-theme-flash.css'), '', base_url('assets/plugins/pace/css/pace-theme-flash.css')),
        array(base_url('assets/plugins/jquery-slider/css/jquery.sidr.light.css'), '', base_url('assets/plugins/jquery-slider/css/jquery.sidr.light.css')),
        array(base_url('assets/plugins/jquery-scrollbar/css/jquery.scrollbar.css'), '', base_url('assets/plugins/jquery-scrollbar/css/jquery.scrollbar.css')),
        array(base_url('assets/plugins/boostrapv3/css/bootstrap.min.css'), '', base_url('assets/plugins/boostrapv3/css/bootstrap.min.css')),
        array(base_url('assets/plugins/boostrapv3/css/bootstrap-theme.min.css'), '', base_url('assets/plugins/boostrapv3/css/bootstrap-theme.min.css')),
        array('admin/font-awesome.css', '', 'admin/font-awesome.css')
    )
);
$config['groups']['Admin_Main_Css'] = array(
    'css' => array(
        array('admin/animate.min.css', '', 'admin/animate.min.css'),
        array('admin/style.css', '', 'admin/style.css'),
        array('admin/responsive.css', '', 'admin/responsive.css')
    )
);
$config['groups']['Admin_Custom_Css'] = array(
    'css' => array(
        array('admin/custom-icon-set.css', '', 'admin/custom-icon-set.css')
    )
);
$config['groups']['Admin_Data_Table_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/jquery-datatable/css/jquery.dataTables.css'), '', base_url('assets/plugins/jquery-datatable/css/jquery.dataTables.css')),
        array(base_url('assets/plugins/datatables-responsive/css/datatables.responsive.css'), '', base_url('assets/plugins/datatables-responsive/css/datatables.responsive.css'))
    )
);
$config['groups']['Admin_Switch_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/ios-switch/css/ios7-switch.css'), '', base_url('assets/plugins/ios-switch/css/ios7-switch.css'))
    )
);
$config['groups']['Admin_DatePicker_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/bootstrap-datepicker/css/datepicker.css'), '', base_url('assets/plugins/bootstrap-datepicker/css/datepicker.css'))
    )
);
$config['groups']['Admin_TimePicker_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css'), '', base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css'))
    )
);
$config['groups']['Admin_Nestable_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/jquery-nestable/css/jquery.nestable.css'), '', base_url('assets/plugins/jquery-nestable/css/jquery.nestable.css'))
    )
);
$config['groups']['Admin_Select2_Css'] = array(
    'css' => array(
        array("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css", '', "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css")
    )
);
$config['groups']['Admin_Calendar_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/fullcalendar/css/fullcalendar.css'), '', base_url('assets/plugins/fullcalendar/css/fullcalendar.css'))
    )
);
$config['groups']['Admin_Tag_Input_Css'] = array(
    'css' => array(
        array(base_url('assets/plugins/bootstrap-tag/css/bootstrap-tagsinput.css'), '', base_url('assets/plugins/bootstrap-tag/css/bootstrap-tagsinput.css'))
    )
);
// End Admin Css ==============================================
// Start Admin Jquery ==============================================
$config['groups']['JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array(base_url('assets/plugins/jquery-ui/js/jquery-ui-1.10.1.custom.min.js'), base_url('assets/plugins/jquery-ui/js/jquery-ui-1.10.1.custom.min.js'))
    )
);
$config['groups']['Admin_Core_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/modernizr.js'), base_url('assets/plugins/modernizr.js')),
        array(base_url('assets/plugins/boostrapv3/js/bootstrap.min.js'), base_url('assets/plugins/boostrapv3/js/bootstrap.min.js')),
        array(base_url('assets/plugins/pace/js/pace.min.js'), base_url('assets/plugins/pace/js/pace.min.js')),
        array(base_url('assets/plugins/breakpoints.js'), base_url('assets/plugins/breakpoints.js')),
        array(base_url('assets/plugins/jquery.unveil.min.js'), base_url('assets/plugins/jquery.unveil.min.js')),
        array(base_url('assets/plugins/jquery.lazyload.min.js'), base_url('assets/plugins/jquery.lazyload.min.js')),
        array(base_url('assets/plugins/jquery.blockui.js'), base_url('assets/plugins/jquery.blockui.js')),
        array(base_url('assets/plugins/jquery-slider/js/jquery.sidr.min.js'), base_url('assets/plugins/jquery-slider/js/jquery.sidr.min.js')),
        array(base_url('assets/plugins/jquery-scrollbar/js/jquery.scrollbar.min.js'), base_url('assets/plugins/jquery-scrollbar/js/jquery.scrollbar.min.js'))
    )
);
$config['groups']['Admin_Data_Table_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-datatable/js/jquery.dataTables.js'), base_url('assets/plugins/jquery-datatable/js/jquery.dataTables.js')),
        array(base_url('assets/plugins/jquery-datatable/js/dataTables.tableTools.js'), base_url('assets/plugins/jquery-datatable/js/dataTables.tableTools.js')),
        array(base_url('assets/plugins/datatables-responsive/js/datatables.responsive.js'), base_url('assets/plugins/datatables-responsive/js/datatables.responsive.js')),
        array(base_url('assets/plugins/datatables-responsive/js/lodash.min.js'), base_url('assets/plugins/datatables-responsive/js/lodash.min.js')),
        array(base_url('assets/plugins/jquery-datatable/js/datatables.js'), base_url('assets/plugins/jquery-datatable/js/datatables.js'))
    )
);
$config['groups']['Admin_Switch_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/ios-switch/js/ios7-switch.js'), base_url('assets/plugins/ios-switch/js/ios7-switch.js'))
    )
);
$config['groups']['Admin_DatePicker_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'), base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'))
    )
);
$config['groups']['Admin_TimePicker_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'), base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'))
    )
);
$config['groups']['Admin_Nestable_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-nestable/js/jquery.nestable.js'), base_url('assets/plugins/jquery-nestable/js/jquery.nestable.js'))
    )
);
$config['groups']['Admin_Select2_JQuery'] = array(
    'js' => array(
        array("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.js", "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.js")
    )
);
$config['groups']['Admin_Calendar_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/fullcalendar/js/fullcalendar.min.js'), base_url('assets/plugins/fullcalendar/js/fullcalendar.min.js'))
    )
);
$config['groups']['Admin_Tag_Input_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/bootstrap-tag/js/bootstrap-tagsinput.min.js'), base_url('assets/plugins/bootstrap-tag/js/bootstrap-tagsinput.min.js'))
    )
);
$config['groups']['Admin_Rel_Copy_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/relCopy.js'), base_url('assets/plugins/relCopy.js'))
    )
);
$config['groups']['Admin_Auto_Number_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-number-animate/js/jquery.animateNumbers.js'), base_url('assets/plugins/jquery-number-animate/js/jquery.animateNumbers.js'))
    )
);
$config['groups']['Admin_Auto_Numeric_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-autonumeric/js/autoNumeric.js'), base_url('assets/plugins/jquery-autonumeric/js/autoNumeric.js'))
    )
);
$config['groups']['Admin_Mask_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-inputmask/js/jquery.inputmask.min.js'), base_url('assets/plugins/jquery-inputmask/js/jquery.inputmask.min.js'))
    )
);
$config['groups']['Print_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/print.js'), base_url('assets/plugins/print.js'))
    )
);
$config['groups']['Admin_Nav_JQuery'] = array(
    'js' => array(
        array('admin/nav.js', 'admin/nav.js')
    )
);
$config['groups']['Admin_Func_JQuery'] = array(
    'js' => array(
        array('admin/func.js', 'admin/func.js')
    )
);
$config['groups']['Admin_Main_JQuery'] = array(
    'js' => array(
        array('admin/main.js', 'admin/main.js')
    )
);
$config['groups']['Validation_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js'))
    )
);
// Front Template CSS
$config['groups']['Front_Home_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);
$config['groups']['Front_Phone_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.css', '', 'front/bootstrap.css'),
        array('front/style.css?v=' . $version, '', 'front/style.css?v=' . $version),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/intlTelInput.css', '', 'front/intlTelInput.css'),
        array('front/magnific-popup.css', '', 'front/magnific-popup.css'),
        array('front/customicons.css', '', 'front/customicons.css'),
        array('front/font-awesome.css', '', 'front/font-awesome.css'),
    )
);
$config['groups']['Front_Dashboard_Template_Css'] = array(
    'css' => array(
        array('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', '', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'),
        array('front/order.css?v=' . $version, '', 'front/order.css?v=' . $version),
        array('front/owl.carousel.min.css', '', 'front/owl.carousel.min.css'),
        array('front/intlTelInput.css', '', 'front/intlTelInput.css'),
        array('front/magnific-popup.css', '', 'front/magnific-popup.css'),
        array('front/customicons.css', '', 'front/customicons.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v' . $version),
        array('https://use.fontawesome.com/releases/v5.7.2/css/all.css', '', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css'),
        array('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css', '', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css'),
    )
);
$config['groups']['Front_Booking_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/social.css?v=' . $version, '', 'front/social.css?v=' . $version),
        array('front/order.css?v=' . $version, '', 'front/order.css?v=' . $version),
        array('front/owl.carousel.min.css', '', 'front/owl.carousel.min.css'),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('front/select2.min.css', '', 'front/select2.min.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);


$config['groups']['Front_Order_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/order.css?v=' . $version, '', 'front/order.css?v=' . $version),
        array('front/owl.carousel.min.css', '', 'front/owl.carousel.min.css'),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('front/select2.min.css', '', 'front/select2.min.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);

$config['groups']['Front_Schedule_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/order.css?v=' . $version, '', 'front/order.css?v=' . $version),
        array('front/owl.carousel.min.css', '', 'front/owl.carousel.min.css'),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('front/select2.min.css', '', 'front/select2.min.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);

$config['groups']['Front_Prices_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/owl.carousel.min.css', '', 'front/owl.carousel.min.css'),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);

$config['groups']['Front_Thankyou_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);

$config['groups']['Front_Page_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);
$config['groups']['Front_Tracking_Page_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.css', '', 'front/bootstrap.css'),
        array('front/style.css?v=' . $version, '', 'front/style.css?v=' . $version),
        array('front/magnific-popup.css', '', 'front/magnific-popup.css'),
        array('front/customicons.css', '', 'front/customicons.css'),
        array('front/mapbox.css', '', 'front/mapbox.css')
    )
);
$config['groups']['Front_New_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.css', '', 'front/bootstrap.css'),
        array('front/font-awesome.css', '', 'front/font-awesome.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version)
    )
);
$config['groups']['Area_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);
$config['groups']['Offers_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=' . $version, '', 'front/template-style.css?v=' . $version),
        array('front/offers.css?v=' . $version, '', 'front/offers.css?v=' . $version),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);
$config['groups']['Front_New_Template_Ae_Css'] = array(
    'css' => array(
        array('front/ae_files/style.css', '', 'front/ae_files/style.css')
    )
);

// Front Template Jquery
$config['groups']['Front_Home_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array('front/flexslider.min.js', 'front/flexslider.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Page_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Area_Page_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Tracking_Page_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array('front/jquery.nicescroll.min.js', 'front/jquery.nicescroll.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/mapbox.js', 'front/mapbox.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Phone_Post_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/ideal.postcodes.js', 'front/ideal.postcodes.js'),
        array('front/intlTelInput.js', 'front/intlTelInput.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);

$config['groups']['Front_Register_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/common.js', 'front/common.js?' . $version),
        array('front/' . SERVER . '_address_api.js', 'front/' . SERVER . '_address_api.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js',
            'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);

$config['groups']['Front_Register_Template_Css'] = array(
    'css' => array(
        array('front/bootstrap.min.css', '', 'front/bootstrap.min.css'),
        array('front/template-style.css?v=4.0.3', '', 'front/template-style.css?v=' . $version),
        array('front/social.css?v=' . $version, '', 'front/social.css?v=' . $version),
        array('front/l2licons.css', '', 'front/l2licons.css'),
        array('front/order.css?v=' . $version, '', 'front/order.css?v=' . $version),
        array('front/select2.min.css', '', 'front/select2.min.css'),
        array('https://use.fontawesome.com/releases/v5.14.0/css/all.css', '', 'https://use.fontawesome.com/releases/v5.14.0/css/all.css'),
    )
);

$config['groups']['Front_Phone_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/intlTelInput.js', 'front/intlTelInput.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Payment_Card_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/jquery.creditCardValidator.js', 'front/jquery.creditCardValidator.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Select_Item_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/owl.carousel.min.js', 'front/owl.carousel.min.js'),
        array('front/mixitup.min.js', 'front/mixitup.min.js'),
        array('front/jquery.nicescroll.min.js', 'front/jquery.nicescroll.min.js'),
        array('front/intlTelInput.js', 'front/intlTelInput.js'),
        array('front/jquery.creditCardValidator.js', 'front/jquery.creditCardValidator.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_Address_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/ideal.postcodes.js', 'front/ideal.postcodes.js'),
        array('front/jquery.nicescroll.min.js', 'front/jquery.nicescroll.min.js'),
        array('front/intlTelInput.js', 'front/intlTelInput.js'),
        array('front/jquery.creditCardValidator.js', 'front/jquery.creditCardValidator.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array('front/moment.js', 'front/moment.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);

$config['groups']['Front_Booking_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/owl.carousel.min.js', 'front/owl.carousel.min.js'),
        array('front/mixitup.min.js', 'front/mixitup.min.js'),
        array('front/select2.min.js', 'front/select2.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/common.js', 'front/common.js?' . $version),
        array('front/booking.js', 'front/booking.js?' . $version),
        array('front/collection_time.js', 'front/collection_time.js'),
        array('front/' . SERVER . '_address_api.js', 'front/' . SERVER . '_address_api.js'),
        array('https://js.stripe.com/v3/', 'https://js.stripe.com/v3/'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/payment.js', 'front/payment.js'),
    )
);


$config['groups']['Front_Order_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/owl.carousel.min.js', 'front/owl.carousel.min.js'),
        array('front/mixitup.min.js', 'front/mixitup.min.js'),
        array('front/select2.min.js', 'front/select2.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/common.js', 'front/common.js?' . $version),
        array('front/orders.js', 'front/orders.js'),
        array('front/collection_time.js', 'front/collection_time.js'),
    )
);

$config['groups']['Front_Schedule_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/owl.carousel.min.js', 'front/owl.carousel.min.js'),
        array('front/mixitup.min.js', 'front/mixitup.min.js'),
        array('front/select2.min.js', 'front/select2.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/common.js', 'front/common.js?' . $version),
        array('front/' . SERVER . '_address_api.js', 'front/' . SERVER . '_address_api.js'),
        //array('front/orders.js', 'front/orders.js'),
        array('https://js.stripe.com/v3/', 'https://js.stripe.com/v3/'),
        array('front/payment_schedule.js', 'front/payment_schedule.js'),
        array('front/schedule.js', 'front/schedule.js'),
    )
);



$config['groups']['Front_Prices_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/owl.carousel.min.js', 'front/owl.carousel.min.js'),
        array('front/mixitup.min.js', 'front/mixitup.min.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/common.js', 'front/common.js?' . $version),
        array('front/prices.js', 'front/prices.js?' . $version),
    )
);

$config['groups']['Front_Thankyou_Template_JQuery'] = array(
    'js' => array(
        array('front/jquery-3.4.1.min.js', 'front/jquery-3.4.1.min.js'),
        array('front/bootstrap.bundle.min.js', 'front/bootstrap.bundle.min.js'),
        array('front/thankyou.js', 'front/thankyou.js'),
    )
);


$config['groups']['Front_Checkout_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/jquery.magnific-popup.min.js', 'front/jquery.magnific-popup.min.js'),
        array('front/jquery.nicescroll.min.js', 'front/jquery.nicescroll.min.js'),
        array('front/jquery.creditCardValidator.js', 'front/jquery.creditCardValidator.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js')),
        array('front/' . SERVER . '_address_api.js', 'front/' . SERVER . '_address_api.js'),
        array('front/cookie.js', 'front/cookie.js'),
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
$config['groups']['Front_New_Template_JQuery'] = array(
    'js' => array(
        array(base_url('assets/plugins/jquery-1.11.3.js'), base_url('assets/plugins/jquery-1.11.3.js')),
        array('front/bootstrap.min.js', 'front/bootstrap.min.js'),
        array('front/jquery.nicescroll.min.js', 'front/jquery.nicescroll.min.js'),
        array(base_url('assets/plugins/jquery.validate.js'), base_url('assets/plugins/jquery.validate.js')),
        array(base_url('assets/plugins/jquery.form.js'), base_url('assets/plugins/jquery.form.js'))
    )
);
$config['groups']['Front_New_Template_Custom_JQuery'] = array(
    'js' => array(
        array('front/custom.js', 'front/custom.js?' . $version)
    )
);
