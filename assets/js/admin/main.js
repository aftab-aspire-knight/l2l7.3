if ($('.recordTable').length > 0) {
    $.fn.dataTableExt.oApi.fnStandingRedraw = function (oSettings) {
        //redraw to account for filtering and sorting
        // concept here is that (for client side) there is a row got inserted at the end (for an add)
        // or when a record was modified it could be in the middle of the table
        // that is probably not supposed to be there - due to filtering / sorting
        // so we need to re process filtering and sorting
        // BUT - if it is server side - then this should be handled by the server - so skip this step
        if (oSettings.oFeatures.bServerSide === false) {
            var before = oSettings._iDisplayStart;
            oSettings.oApi._fnReDraw(oSettings);
            //iDisplayStart has been reset to zero - so lets change it back
            oSettings._iDisplayStart = before;
            oSettings.oApi._fnCalculateEnd(oSettings);
        }

        //draw the 'current' page
        oSettings.oApi._fnDraw(oSettings);
    };
}
var blockUI = function (el) {
    $(el).block({
        message: '<div class="loading-animator"></div>',
        css: {
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.3,
            cursor: 'wait'
        }
    });
}

// wrapper function to  un-block element(finish loading)
var unblockUI = function (el) {
    $(el).unblock();
}
var GetQueryStringParams = function (sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
var scrollToPosition = function (elem) {
    $('html, body').stop().animate({
        'scrollTop': elem.offset().top - 100
    }, 500, 'swing', function () {
        //window.location.hash = target;

    });
}
//Box Error
var MessageBoxError = function (id, err, time, ctrl) {
    $(id).fadeTo(200, 0.1, function () {
        $(id).removeClass().addClass('alert alert-error').fadeTo(900, 1,
                function () {
                    setTimeout(function () {
                        $(id).fadeOut('slow');
                        if (ctrl != "") {
                            $(ctrl).focus();
                        }
                    }, time);
                });
        $(id).html(err);
    });
}
//Box Success
var MessageBoxSuccess = function (id, msg, time, rdt) {
    $(id).fadeTo(200, 0.1, function () //start fading the messagebox
    {
        //add message and change the class of the box and start fading
        $(id).removeClass().addClass('alert alert-success').fadeTo(900, 1,
                function () {
                    setTimeout(function () {
                        $(id).fadeOut('slow');
                        if (rdt != "") {
                            window.location = rdt;
                        }
                    }, time);
                });
        $(id).html(msg);
    });
}
//Nestable Update Field
var updateOutput = function (e) {
    var list = e.length ? e : $(e.target);
    if ((list.attr("id") == "nestable_menus") || (list.attr("id") == "assigned_menus") || (list.attr("id") == "nestable_widget") || (list.attr("id") == "nestable_locker") || (list.attr("id") == "nestable_franchise_post_code") || (list.attr("id") == "nestable_time")) {
        var output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            //output.html('JSON browser support required for this demo.');
        }
    }
};
var RemoveCharacter = function (output, limit) {
    return output.substr(0, output.length - limit);
}
//Change Switch
var ChangeSwitch = function (element, value) {
    if (value == "Enabled") {
        $(element).prev().addClass("on");
        $(element).attr("checked", "checked");
    } else {
        $(element).prev().removeClass("on").addClass("off");
        $(element).removeAttr("checked");
    }
}
//Remove Record
$(document).on("click", ".removeRow", function (e) {
    var currentRow = $(this);
    var result = confirm('Are you sure you want to delete?');
    if (result == true) {
        var url = $(this).attr("href");
        var p_type = $(this).attr("data-type");
        var row = $(currentRow).parents('tr');
        var oTable = $('.recordTable').dataTable();
        var colSpan = currentRow.parents('tr').find("td").length;
        row.html("<td colspan='" + colSpan + "'><span style='color:green;'>removing ...</span></td>");
        $.ajax({
            type: "POST",
            url: url,
            cache: false,
            async: false,
            success: function (data)
            {
                console.log(data);
                if (data == "success") {
                    row.html("<td colspan='" + colSpan + "'><span style='color:red'>Record Deleted Successfully</span></td>").fadeOut("slow", function () {
                        var pos = oTable.fnGetPosition(this);
                        oTable.fnDeleteRow(pos, null, false);
                        oTable.fnStandingRedraw();
                    });
                } else {
                    row.html("<td colspan='" + colSpan + "'><span style='color:red'>" + data + "</span></td>");
                    setTimeout(function () {
                        oTable.fnStandingRedraw();
                    }, 2000);
                }
            },
            error: function (data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
    }
    return false;
});


//Bind services to franchise
$(document).on("click", "#btn_franchise_add_services", function (e) {
    var url = $('#add_services_action').val();
    var franchise_id = $('#franchise_id').val();
    if (url.length == 0)
    {
        alert('Service can to be added');
        return false;
    }
    $.ajax({
        type: "POST",
        url: url, data: {franchise_id: franchise_id},
        cache: false,
        async: false,
        success: function (data)
        {
            if (data == "success") {
                location.reload();
            } else {
                alert(data);
            }
        },
        error: function (data) {
            MessageBoxError("#msg_box", data, 2000, "");
        }
    });
    return false;
});



// update service franchise price 

function update_franchise_price_services(id, price, title, discount)
{

    var url = $('#update_services_price').val();

    if (id == '')
    {
        alert('Service id is not defind');
        return false;
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {id: id, price: price, title: title, discount: discount},
        cache: false,
        async: false,
        success: function (data)
        {
            if (data == "success") {
                alert(data);
            } else {
                alert(data);
            }
        },
        error: function (data) {
            MessageBoxError("#msg_box", data, 2000, "");
        }
    });
    return false;
}
// remove service franchise  

function remove_franchise_services(id)
{

    var url = $('#remove_services').val();

    if (id == '')
    {
        alert('Service id is not defind');
        return false;
    }
    if (confirm("Press a button!\nEither OK or Cancel."))
    {
        $.ajax({
            type: "POST",
            url: url,
            data: {id: id},
            cache: false,
            async: false,
            success: function (data)
            {
                if (data == "success") {
                    alert(data);
                } else {
                    alert(data);
                }
            },
            error: function (data) {
                MessageBoxError("#msg_box", data, 2000, "");
            }
        });
    }

    return false;
}

// Enable/Disable Form Button Attributes
var EnableDisableButton = function (object_type, btn1, btn1_icon_class, btn1_text, btn2) {
    var btn_1_context = $("#" + btn1);
    btn_1_context.html("");
    if (object_type == "Disable") {
        btn_1_context.attr("disabled", "disabled");
        if (btn2 != "") {
            $("#" + btn2).attr("disabled", "disabled");
        }
        $('<i>').addClass("fa fa-spinner fa fa-2x fa-spin").appendTo(btn_1_context);
    } else {
        btn_1_context.removeAttr("disabled");
        if (btn2 != "") {
            $("#" + btn2).removeAttr("disabled");
        }
        btn_1_context.html("&nbsp;&nbsp;" + btn1_text);
        if (btn1_icon_class != "") {
            $('<i>').addClass(btn1_icon_class).prependTo(btn_1_context);
        }
    }
}
// Ajax Submit Code
var AjaxFormSubmit = function (form_name, msg_box, btn1, btn1_icon_class, btn1_text, btn2) {
    var options = {
        complete: function (response) {
            $(".sp-error").remove();
            console.log(response.responseText);
            var data = response.responseText.split("||");
            if (data[0] == "success") {
                if (data[1] == "t") {
                    window.location = data[2];
                } else if (data[1] == "i") {
                    MessageBoxSuccess("#" + msg_box, '<a href="' + data[2] + '" target="_blank">Click Here to open order page</a>', 1000000, "");
                    Clear_Storage_Record("love_2_laundry_session_cart");
                    Clear_Storage_Record("love_2_laundry_session_expire_time");
                    Clear_Storage_Record("love_2_laundry_discount_record");
                    Clear_Storage_Record("love_2_laundry_preference_record");
                    Clear_Storage_Record("love_2_laundry_time_collection_record");
                    Clear_Storage_Record("love_2_laundry_pickup_date_time_record");
                    Clear_Storage_Record("love_2_laundry_delivery_date_time_record");
                    Clear_Storage_Record("love_2_laundry_location_collection_record");
                    Clear_Storage_Record("love_2_laundry_address_record");
                    Clear_Storage_Record("love_2_laundry_credit_card_record");
                    Clear_Storage_Record("love_2_laundry_credit_card_token_record");
                    Clear_Storage_Record("love_2_laundry_invoice_edit_load_record");
                    EnableDisableButton("Enabled", btn1, btn1_icon_class, btn1_text, btn2);
                } else if (data[1] == "s") {
                    MessageBoxSuccess("#" + msg_box, data[2], 1000, "");
                } else if (data[1] == "r") {
                    MessageBoxSuccess("#" + msg_box, data[2], 1000, data[2]);
                } else {
                    MessageBoxError("#" + msg_box, data[1], 2000, "");
                }
            } else {
                if (data[1] == "msg") {
                    alert(data[2]);
                } else {
                    $('#' + data[2]).addClass("error");
                    var error_span = $('<span>').attr("for", data[2]).addClass("error sp-error").html(data[3]);
                    if (data[1] == "a") {
                        error_span.insertAfter($('#' + data[2]));
                    } else {
                        error_span.insertAfter($('#' + data[2]).parent());
                    }
                    setTimeout(function () {
                        error_span.remove();
                    }, 2000);
                    $('#' + data[2]).focus();
                }
                EnableDisableButton("Enabled", btn1, btn1_icon_class, btn1_text, btn2);
            }
        },
        error: function (response) {
            var data = response.responseText;
            MessageBoxError("#" + msg_box, data, 2000, "");
        }
    };
    $("#" + form_name).ajaxSubmit(options);
}
//Document Ready Start
$(document).ready(function () {
    if ($('.only-number').length > 0) {
        $('.only-number').keypress(function (event) {
            if (event.charCode != 0) {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                var enter = (!event.charCode ? event.which : event.charCode);
                if ((!regex.test(key)) && (enter != 13)) {
                    event.preventDefault();
                    return false;
                }
            }
        });
    }
    if ($('.not-zero').length > 0) {
        $('.not-zero').keyup(function () {
            var value = $(this).val();
            if (value.indexOf('0') == 0) {
                $(this).val(1);
            }
        });
        $('.not-zero').focusout(function () {
            var value = $(this).val();
            if (value == "") {
                $(this).val(1);
            }
        });
    }
    //Spinner
    if ($('.spin').length > 0) {
        $(".spin").spinner({
            min: 0,
            max: 500
        });
    }
    //Rating Spinner
    if ($('.rating-spin').length > 0) {
        $(".rating-spin").spinner({
            min: 0,
            max: 5
        });
    }
    //Accordian Panel
    if ($('.accordianpanel').length > 0) {
        $('.accordianpanel').collapse({
            toggle: false
        });
    }
    if ($('.animate-number').length > 0) {
        $('.animate-number').each(function () {
            $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-animation-duration")));
        });
    }
    if ($('.auto').length > 0) {
        $('.auto').autoNumeric('init');
    }
    //Rel Copy Code
    var removeLink = ' <a class="remove" href="#" onclick="$(this).parent().slideUp(function(){ $(this).remove() }); return false"><button type="button" class="btn btn-danger btn-cons">Remove</button></a>';
    if ($('a.copy').length > 0) {
        $('a.copy').relCopy({append: removeLink});
    }
    // Nestable Admin Menu Code
    if ($('#assigned_menus').length > 0) {
        // activate Nestable for list 1
        $('#assigned_menus').nestable({
            group: 1,
            maxDepth: 2
        }).on('change', updateOutput);

        // activate Nestable for list 2
        $('#unassigned_menus').nestable({
            group: 1,
            maxDepth: 1
        });

        // output initial serialised data
        updateOutput($('#assigned_menus').data('output', $('#assigned_output')));
    }

    // Nestable Navigation Code
    if ($('#nestable_menus').length > 0) {
        $('#nestable_menus').nestable({
            group: 1,
            maxDepth: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    }
    // Nestable Widget Code
    if ($('#nestable_widget').length > 0) {
        $('#nestable_widget').nestable({
            group: 1,
            maxDepth: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable_widget').data('output', $('#assigned_output')));
    }
    // Nestable Locker Code
    if ($('#nestable_locker').length > 0) {
        $('#nestable_locker').nestable({
            group: 1,
            maxDepth: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable_locker').data('output', $('#assigned_output')));
    }
    // Nestable Franchise Post Code
    if ($('#nestable_franchise_post_code').length > 0) {
        $('#nestable_franchise_post_code').nestable({
            group: 1,
            maxDepth: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable_franchise_post_code').data('output', $('#assigned_output')));
    }
    // Nestable Time Code
    if ($('#nestable_time').length > 0) {
        $('#nestable_time').nestable({
            group: 1,
            maxDepth: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable_time').data('output', $('#assigned_output')));
    }
    // Date Picker
    if ($('.date').length > 0) {
        $('.date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
    }
    if ($('.i-date').length > 0) {
        $('.i-date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        }).on("changeDate", function (e) {
            console.log(e);
            // `e` here contains the extra attributes
        });
        ;
    }
    // Select2
    if ($('.select2').length > 0) {
        $('.select2').select2();
    }
    // IOS Switch
    if ($('.ios').length > 0) {
        var Switch = require('ios7-switch')
                , checkbox = document.querySelector('.ios')
                , mySwitch = new Switch(checkbox);
        //mySwitch.toggle();
        mySwitch.el.addEventListener('click', function (e) {
            e.preventDefault();
            mySwitch.toggle();
        }, false);
    }
    //Login Form
    if ($("#frm_login").length > 0) {
        $('#frm_login').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                email_address: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                password: {
                    required: "Please enter password"
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_login", "", "", "");
                AjaxFormSubmit("frm_login", "msg_box", "btn_login", "fa fa-sign-in", "Login", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }

    //Forgot Form
    if ($("#frm_forgot").length > 0) {
        $('#frm_forgot').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                email_address: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                password: {
                    required: "Please enter password"
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_forgot", "", "", "btn_reset");
                AjaxFormSubmit("frm_forgot", "msg_box", "btn_forgot", "fa fa-unlock", "Send", "btn_reset");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Administrator Form
    if ($("#frm_administrator").length > 0) {
        $('#frm_administrator').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                u_password: {
                    required: function () {
                        return $('#admin_id').val() === ""
                    }
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                u_password: {
                    required: "Please enter password"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_administrator", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //User Form
    if ($("#frm_user").length > 0) {
        $('#frm_user').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                u_password: {
                    required: function () {
                        return $('#admin_id').val() === ""
                    }
                },
                franchise: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                u_password: {
                    required: "Please enter password"
                },
                franchise: {
                    required: "Please select franchise"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_user", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Banner Form
    if ($("#frm_banner").length > 0) {
        $("#frm_banner").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                image_file: {
                    required: function () {
                        return $('#pic_path').val() === ""
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                image_file: {
                    required: "Please select image"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_banner", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    ///

    if ($("#frm_notification").length > 0) {
        $("#frm_notification").submit(function () {

        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                content: {
                    required: "Please enter content"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_notification", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }




    //Page Form
    if ($("#frm_page").length > 0) {
        $("#frm_page").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                content: {
                    required: function () {
                        return $('#template').val() == "None"
                    }
                },
                access_url: {
                    required: true
                },
                latitude: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                longitude: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                header_title: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                section_1_title: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                section_1_content: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                section_2_title: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                },
                section_2_content: {
                    required: function () {
                        return $('#template').val() != "None"
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                content: {
                    required: "Please enter content"
                },
                access_url: {
                    required: "Please enter url"
                },
                latitude: {
                    required: "Please enter latitude"
                },
                longitude: {
                    required: "Please enter longitude"
                },
                header_title: {
                    required: "Please enter header title"
                },
                section_1_title: {
                    required: "Please enter title"
                },
                section_1_content: {
                    required: "Please enter content"
                },
                section_2_title: {
                    required: "Please enter title"
                },
                section_2_content: {
                    required: "Please enter content"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_page", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Update Form
    if ($("#frm_profile").length > 0) {
        $('#frm_profile').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "");
                AjaxFormSubmit("frm_profile", "msg_box", "btn_submit", "", "Save", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Email Responder Form
    if ($("#frm_email_responder").length > 0) {
        $("#frm_email_responder").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                from_email: {
                    required: true
                },
                to_email: {
                    required: true
                },
                subject: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                from_email: {
                    required: "Please enter email from"
                },
                to_email: {
                    required: "Please enter email to"
                },
                subject: {
                    required: "Please enter subject"
                },
                content: {
                    required: "Please enter content"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_email_responder", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Sms Responder Form
    if ($("#frm_sms_responder").length > 0) {
        $("#frm_sms_responder").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                content: {
                    required: "Please enter content"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_sms_responder", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Tag Form
    if ($("#frm_tag").length > 0) {
        $("#frm_tag").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                tag: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                tag: {
                    required: "Please enter tag"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_tag", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Widget Form
    if ($("#frm_widget").length > 0) {
        $("#frm_widget").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                image_file: {
                    required: function () {
                        if ($("#pic_path").val() == "" && tinyMCE.activeEditor.getContent() == "") {
                            console.log(tinyMCE.activeEditor.getContent());
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                image_file: {
                    required: "Please select image or enter content"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_widget", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Widget Section Form
    if ($("#frm_widget_section").length > 0) {
        $("#frm_widget_section").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                widget: {
                    required: function () {
                        if ($("#w_list").find("li").length == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                widget: {
                    required: "Please add atleast one widget to section"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_widget_section", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Navigation Form
    if ($("#frm_navigation").length > 0) {
        $("#frm_navigation").submit(function () {
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                n_title: {
                    required: true
                },
                page: {
                    required: function () {
                        if ($("#p_list").find("li").length == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                n_title: {
                    required: "Please enter title"
                },
                page: {
                    required: "Please add atleast one page or custom link to section"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_navigation", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Discount Form
    if ($("#frm_discount").length > 0) {
        $("#frm_discount").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                code: {
                    required: true
                },
                worth: {
                    required: true
                },
                d_type: {
                    required: true
                },
                status: {
                    required: true
                },
                discount_for: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "Please enter code"
                },
                worth: {
                    required: "Please enter worth"
                },
                d_type: {
                    required: "Please select type"
                },
                status: {
                    required: "Please select status"
                },
                discount_for: {
                    required: "Please select discount for"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_discount", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Testimonial Form
    if ($("#frm_testimonial").length > 0) {
        $("#frm_testimonial").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                content: {
                    required: true
                },
                testimonial_date: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                name: {
                    required: "Please enter name"
                },
                email_address: {
                    required: "Please enter email address"
                },
                content: {
                    required: "Please enter content"
                },
                testimonial_date: {
                    required: "Please select testimonial date"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_testimonial", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Category Form
    if ($("#frm_category").length > 0) {
        $("#frm_category").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                mobile_title: {
                    required: true
                },
                desktop_icon_class_name: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                mobile_title: {
                    required: "Please enter mobile title"
                },
                desktop_icon_class_name: {
                    required: "Please enter desktop icon class name"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_category", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Service Form
    if ($("#frm_service").length > 0) {
        $("#frm_service").submit(function () {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                category: {
                    required: true
                },
                price: {
                    required: true
                },
                title: {
                    required: true
                }
            },
            messages: {
                category: {
                    required: "Please select category"
                },
                price: {
                    required: "Please enter price"
                },
                title: {
                    required: "Please enter title"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_service", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Preference Form
    if ($("#frm_preference").length > 0) {
        $("#frm_preference").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_preference", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Location Form
    if ($("#frm_location").length > 0) {
        $("#frm_location").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                locker: {
                    required: function () {
                        if ($("#l_list").find("li").length == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                locker: {
                    required: "Please enter locker"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_location", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Member Form
    if ($("#frm_member").length > 0) {
        $('#frm_member').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                u_password: {
                    required: function () {
                        return $('#member_id').val() == ""
                    }
                },
                phone: {
                    required: true
                },
                postal_code: {
                    required: true
                },
                building_name: {
                    required: function () {
                        return ($('#building_name').val() == "" && $('#street_name').val() == "")
                    }
                },
                street_name: {
                    required: function () {
                        return ($('#building_name').val() == "" && $('#street_name').val() == "")
                    }
                },
                town: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address"
                },
                u_password: {
                    required: "Please enter password"
                },
                phone: {
                    required: "Please enter phone no"
                },
                postal_code: {
                    required: "Please enter post code"
                },
                building_name: {
                    required: "Please enter building name or number"
                },
                street_name: {
                    required: "Please enter street name"
                },
                town: {
                    required: "Please enter town"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_member", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Franchise Form
    if ($("#frm_franchise").length > 0) {
        $('#frm_franchise').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                post_code: {
                    required: function () {
                        if ($("#l_list").find("li").length == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                name: {
                    required: "Please enter name"
                },
                email_address: {
                    required: "Please enter email address"
                },
                post_code: {
                    required: "Please enter post code"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_franchise", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Setting Form
    if ($("#frm_setting").length > 0) {
        $("#setting_tab a").click(function (e) {
            e.preventDefault();
            $("#active_tab").val($(this).attr("href"));
            $(this).tab('show');
        });
        $("#frm_setting").submit(function () {
            var is_valid = true;
            var field_id = "";
            if ($("#active_tab").val() == "#tab_website") {
                $("#tab_website").find("input").each(function () {
                    if ($(this).val() == "") {
                        field_id = "#" + $(this).attr("id");
                        is_valid = false;
                        return;
                    }
                });
            } else if ($("#active_tab").val() == "#tab_email") {
                $("#tab_email").find("input").each(function () {
                    if ($(this).val() == "") {
                        field_id = "#" + $(this).attr("id");
                        is_valid = false;
                        return;
                    }
                });
            } else if ($("#active_tab").val() == "#tab_order") {
                $("#tab_order").find("input").each(function () {
                    if ($(this).val() == "") {
                        field_id = "#" + $(this).attr("id");
                        is_valid = false;
                        return;
                    }
                });
            } else if ($("#active_tab").val() == "#tab_social") {
                $("#tab_social").find("input").each(function () {
                    if ($(this).val() == "") {
                        field_id = "#" + $(this).attr("id");
                        is_valid = false;
                        return;
                    }
                });
            } else if ($("#active_tab").val() == "#tab_stripe") {
                $("#tab_stripe").find("input").each(function () {
                    if ($(this).val() == "") {
                        field_id = "#" + $(this).attr("id");
                        is_valid = false;
                        return;
                    }
                });
            }
            if (is_valid == true) {
                EnableDisableButton("Disable", "btn_submit", "", "", "");
                AjaxFormSubmit("frm_setting", "msg_box", "btn_submit", "", "Save", "");
            } else {
                var error_span = $('<span>').addClass("error").html("This field is required.");
                error_span.insertAfter($(field_id));
                scrollToPosition(error_span);
                setTimeout(function () {
                    error_span.remove();
                }, 2000);
            }
            return false;
        });
    }
    //Configuration Form
    if ($("#frm_configuration").length > 0) {
        $("#btn_empty").click(function () {
            var result = confirm('Are you sure you want to clear cache?');
            if (result == true) {
                $("#frm_configuration").submit();
            }
        });
    }

    //Subscribers Form
    if ($("#frm_subscriber").length > 0) {
        $("#frm_subscriber").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                email_address: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                }
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_subscriber", "msg_box", "btn_submit", "", "Save", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Unavailable Time Form
    if ($("#frm_unavailable_time").length > 0) {
        $("#frm_unavailable_time").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                disable_date_from: {
                    required: true
                }
            },
            messages: {
                disable_date_from: {
                    required: "Please select date from"
                }
            },
            submitHandler: function (form) {
                var is_valid = true;
                if ($("#t_list").find("li").length == 0) {
                    is_valid = false;
                    var span_error = $('<span>').attr("for", "disable_time").addClass("error sp-error").html("<br/>Please select time");
                    span_error.insertAfter($('#disable_time'));
                    setTimeout(function () {
                        span_error.remove();
                    }, 2000);
                } else {
                    if ($("#disable_date_to").val() != "") {
                        if ($("#disable_date_to").data('datepicker').date.getTime() < $("#disable_date_from").data('datepicker').date.getTime()) {
                            is_valid = false;
                            var span_error = $('<span>').attr("for", "disable_date_to").addClass("error sp-error").html("<br/>Date To will not be smaller then Date From");
                            span_error.insertAfter($('#disable_date_to'));
                            setTimeout(function () {
                                span_error.remove();
                            }, 2000);
                        }
                    }
                }
                if (is_valid == true) {
                    EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                    AjaxFormSubmit("frm_unavailable_time", "msg_box", "btn_submit", "", "Save", "btn_option");
                }
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    //Invoice Form Add
    if ($("#frm_invoice_add").length > 0) {
        $('#frm_invoice_add').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                member: {
                    required: true
                },
                postal_code: {
                    required: true
                }
            },
            messages: {
                member: {
                    required: "Please select member",
                },
                postal_code: {
                    required: "Please enter postal code"
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            },
            submitHandler: function (form) {
                EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                AjaxFormSubmit("frm_invoice_add", "msg_box", "btn_submit", "", "Check Availability", "btn_option");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    if ($("#frm_invoice_edit").length > 0) {
        $('#frm_invoice_edit').validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                ip_address: {
                    required: function (element) {
                        return $("#order_from").val() != "Mobile";
                    }
                },
                franchise: {
                    required: true
                },
                member_card: {
                    required: function () {
                        if ($("#payment_method").val() == "Stripe") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                payment_method: {
                    required: true
                },
                payment_status: {
                    required: true
                },
                order_status: {
                    required: true
                },
                locker: {
                    required: function () {
                        if ($("#location").val() != "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                postal_code: {
                    required: true
                },
                building_name: {
                    required: function () {
                        if ($("#location").val() != "") {
                            return false;
                        } else {
                            return true;
                        }
                    }
                },
                street_name: {
                    required: function () {
                        return false;
                        /*
                         if ($("#location").val() != "") {
                         return false;
                         } else {
                         return true;
                         }*/
                    }
                },
                town: {
                    required: function () {
                        if ($("#location").val() != "") {
                            return false;
                        } else {
                            return true;
                        }
                    }
                },
                pick_date: {
                    required: true
                },
                pick_time: {
                    required: true
                },
                delivery_date: {
                    required: true
                },
                delivery_time: {
                    required: true
                }
            },
            messages: {
            },
            invalidHandler: function (event, validator) {
                // 'this' refers to the form
                var errors = validator.numberOfInvalids();

                if (errors > 0) {
                    $('#maincontent a[href="#home"]').tab('show');
                }

            },
            errorPlacement: function (error, element) {
                // alert($(this).valid())
                error.insertAfter(element.parent());

            },
            submitHandler: function (form) {


                var is_valid = true;
                // if($("#payment_status").val() == "Completed"){
                //     if(parseFloat($("#session_cart_grand_total").val()) < parseFloat($("#old_session_cart_grand_total").val())){
                //         is_valid = false;
                //         alert("You cannot reduce order amount from the cart");
                //     }
                // }

                if (is_valid) {
                    EnableDisableButton("Disable", "btn_submit", "", "", "btn_option");
                    //var credit_cart_array = $("#member_card").val().split("||");
                    // $("#invoice_token").val(response['id']);
                    AjaxFormSubmit("frm_invoice_edit", "msg_box", "btn_submit", "", "Save", "btn_option");
                    /*   
                     if(Stripe != undefined){
                     
                     
                     Stripe.setPublishableKey($("#stripe_jquery_api_key").val());
                     Stripe.createToken({
                     number: credit_cart_array[1],
                     cvc: credit_cart_array[4],
                     exp_month: credit_cart_array[2],
                     exp_year: credit_cart_array[3]
                     }, function(status, response) {
                     if (response.error) {
                     alert(response.error.message);
                     EnableDisableButton("Enabled","btn_submit","","","btn_option");
                     }else{
                     
                     }
                     }); 
                     }else{
                     alert("Payment Gateway is currently offline");
                     }*/
                }

                return false; // required to block normal submit since you used ajax
            }
        });
    }
    if ($(".tinymce").length > 0) {
        var read_only = false;
        if ($("#p-href").length > 0) {
            if ($("#p-href").html() == "View") {
                read_only = true;
            }
        }
        //Tiny MCE
        tinyMCE.init({
            // General options
            mode: "textareas",
            width: "100%",
            editor_selector: "tinymce",
            editor_deselector: "mceNoEditor",
            inline_styles: true,
            entity_encoding: 'named',
            extended_valid_elements: "iframe[src|width|height|name|align]",
            convert_urls: false,
            remove_script_host: false,
            verify_html: false,
            valid_children: "+a[div|h1|h2|h3|h4|h5|h6|p|#text]",
            valid_elements: '*[*]',
            entities: '160,nbsp',
            readonly: read_only,
            relative_urls: false,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste responsivefilemanager"
            ],
            toolbar: "responsivefilemanager insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            image_advtab: true,
            external_filemanager_path: "/assets/filemanager/",
            filemanager_title: "Filemanager",
            external_plugins: {"filemanager": "/assets/filemanager/plugin.min.js"},
            autosave_ask_before_unload: false
        });
    }
});
//Document Ready End