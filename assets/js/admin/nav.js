//Navigation Title
$(document).on("keyup",".nav-title",function () {
    if ($(this).val() == "") {
        var oldValue = $(this).parents(".dd3-item").find("#p_text").val();
        $(this).closest(".dd3-item").data('name', oldValue);
        $(this).closest(".dd3-item").attr('data-name', oldValue);
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    } else {
        $(this).closest(".dd3-item").data('name', $(this).val());
        $(this).closest(".dd3-item").attr('data-name', $(this).val());
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    }
});
// Navigation URL
$(document).on("keyup",".nav-url",function () {
    if ($(this).val() == "") {
        var oldValue = $(this).parents(".dd3-item").find("#p_url").val();
        $(this).closest(".dd3-item").data('url', oldValue);
        $(this).closest(".dd3-item").attr('data-url', oldValue);
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    } else {
        $(this).closest(".dd3-item").data('url', $(this).val());
        $(this).closest(".dd3-item").attr('data-url', $(this).val());
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    }
});
//Nestable Navigation Add
var addToNestable = function (nest_type,handle, ptype, pid, ptext, purl, optext) {
    var listSize = parseInt(Math.floor((Math.random() * 100000) + 1));
    var listHtml = "";
    listHtml += '<li class="dd-item dd3-item" data-id="' + pid + '" data-name="' + ptext + '" data-url="' + purl + '"><div class="' + handle + ' dd3-handle"></div>';
    listHtml += '<input type="hidden" id="p_id" name="p_id[]" value="' + pid + '" />';
    listHtml += '<input type="hidden" id="p_text" name="p_text[]" value="' + optext + '" />';
    listHtml += '<input type="hidden" id="p_url" name="p_url[]" value="' + purl + '" />';
    listHtml += '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
    listHtml += '<div class="accordion-heading"> <a href="#collapse' + listSize + '" data-toggle="collapse" class="accordion-toggle collapsed">' + ptext + ' (' + ptype + ')<i class="fa fa-plus"></i> </a> </div>';
    listHtml += '<div class="accordion-body collapse" id="collapse' + listSize + '" style="height: 0px;"><div class="accordion-inner">';
    listHtml += '<div class="form-group"><label class="form-label">Title</label><div class="controls">';
    listHtml += '<input type="text" id="nav_label' + listSize + '" name="nav_label[]" class="form-control nav-title" value="' + ptext + '"></div></div>';
    if (pid != 0) {
        listHtml += '<div class="original-name"><p>Original : ' + optext + '</p></div>';
    } else {
        listHtml += '<div class="form-group"><label class="form-label">URL</label><div class="controls">';
        listHtml += '<input type="text" id="nav_url' + listSize + '" name="nav_url[]" class="form-control nav-url" value="' + purl + '"></div></div>';
    }
    listHtml += '<div class="control-button"><a class="remove-nav">Remove</a><a class="cancel-nav">Close</a></div>';
    listHtml += '</div></div></div></div></div>';
    if(nest_type == "a"){
        listHtml += '</li>';
        $('#nestable_menus > ol').append(listHtml);
        updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
    }else{
        return listHtml;
    }
}
//Nestable Widget Add
var addToWidgetNestable = function(handle,pid,ptext){
    var listSize = parseInt(Math.floor((Math.random() * 100000) + 1));
    var listHtml = "";
    listHtml += '<li class="dd-item dd3-item" data-id="' + pid + '"><div class="' + handle + ' dd3-handle"></div>';
    listHtml += '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
    listHtml += '<div class="accordion-heading"> <a href="#collapse' + listSize + '" data-toggle="collapse" class="accordion-toggle collapsed">' + ptext + '<i class="fa fa-plus"></i> </a> </div>';
    listHtml += '<div class="accordion-body collapse" id="collapse' + listSize + '" style="height: 0px;"><div class="accordion-inner">';
    listHtml +=	'<div class="control-button"><a class="remove-nav">Remove</a></div>';
    listHtml += '</div></div></div></div></div></li>';
    $('#nestable_widget > ol').append(listHtml);
    updateOutput($('#nestable_widget').data('output', $('#assigned_output')));
}
//Nestable Widget Add
var addToLockerNestable = function(handle,ptext){
    var listSize = parseInt(Math.floor((Math.random() * 100000) + 1));
    var listHtml = "";
    listHtml += '<li class="dd-item dd3-item" data-name="' + ptext + '"><div class="' + handle + ' dd3-handle"></div>';
    listHtml += '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
    listHtml += '<div class="accordion-heading"> <a href="#collapse' + listSize + '" data-toggle="collapse" class="accordion-toggle collapsed">' + ptext + '<i class="fa fa-plus"></i> </a> </div>';
    listHtml += '<div class="accordion-body collapse" id="collapse' + listSize + '" style="height: 0px;"><div class="accordion-inner">';
    listHtml +=	'<div class="control-button"><a class="remove-nav">Remove</a></div>';
    listHtml += '</div></div></div></div></div></li>';
    $('#nestable_locker > ol').append(listHtml);
    updateOutput($('#nestable_locker').data('output', $('#assigned_output')));
}
//Nestable Post Code Add
var addToPostCodeNestable = function(handle,ptext){
    var listSize = parseInt(Math.floor((Math.random() * 100000) + 1));
    var listHtml = "";
    listHtml += '<li class="dd-item dd3-item" data-code="' + ptext + '"><div class="' + handle + ' dd3-handle"></div>';
    listHtml += '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
    listHtml += '<div class="accordion-heading"> <a href="#collapse' + listSize + '" data-toggle="collapse" class="accordion-toggle collapsed">Code : ' + ptext + '<i class="fa fa-plus"></i> </a> </div>';
    listHtml += '<div class="accordion-body collapse" id="collapse' + listSize + '" style="height: 0px;"><div class="accordion-inner">';
    listHtml +=	'<div class="control-button"><a class="remove-nav">Remove</a></div>';
    listHtml += '</div></div></div></div></div></li>';
    $('#nestable_franchise_post_code > ol').append(listHtml);
    updateOutput($('#nestable_franchise_post_code').data('output', $('#assigned_output')));
}
//Nestable Time Add
var addToTimeNestable = function(handle,ptext){
    var listSize = parseInt(Math.floor((Math.random() * 100000) + 1));
    var listHtml = "";
    listHtml += '<li class="dd-item dd3-item" data-text="' + ptext + '"><div class="' + handle + ' dd3-handle"></div>';
    listHtml += '<div class="dd3-content"><div class="accordion accordianpanel"><div class="accordion-group">';
    listHtml += '<div class="accordion-heading"> <a href="#collapse' + listSize + '" data-toggle="collapse" class="accordion-toggle collapsed">' + ptext + '<i class="fa fa-plus"></i> </a> </div>';
    listHtml += '<div class="accordion-body collapse" id="collapse' + listSize + '" style="height: 0px;"><div class="accordion-inner">';
    listHtml +=	'<div class="control-button"><a class="remove-nav">Remove</a></div>';
    listHtml += '</div></div></div></div></div></li>';
    $('#nestable_time > ol').append(listHtml);
    updateOutput($('#nestable_time').data('output', $('#assigned_output')));
}
$(document).on("click", ".remove-nav", function (e) {
    $(this).closest(".dd3-item").html("Removing").fadeOut("slow", function () {
        $(this).remove();
        if($('#nestable_menus').length > 0){
            updateOutput($('#nestable_menus').data('output', $('#assigned_output')));
        }else if($('#nestable_widget').length > 0){
            updateOutput($('#nestable_widget').data('output', $('#assigned_output')));
        }else if($('#nestable_franchise_post_code').length > 0){
            updateOutput($('#nestable_franchise_post_code').data('output', $('#assigned_output')));
        }else if($('#nestable_pages').length > 0){
            updateOutput($('#nestable_pages').data('output', $('#assigned_output')));
        }else if($('#nestable_locker').length > 0){
            updateOutput($('#nestable_locker').data('output', $('#assigned_output')));
        }else if($('#nestable_time').length > 0){
            updateOutput($('#nestable_time').data('output', $('#assigned_output')));
        }
    });
});
$(document).on("click", ".cancel-nav", function (e) {
    $(this).parents(".accordion-group").find(".accordion-heading").find(".accordion-toggle").addClass("collapsed");
    $(this).parents(".accordion-body").removeClass("in");
    $(this).parents(".accordion-body").css("height", "0");
});
$(document).on("click", "#btn_page", function (e) {
    $(".sp-error").remove();
    if($("#page").val() != ""){
        var page_array = $("#page").val().split("||");
        addToNestable("a","dd-handle", "Page", page_array[0], page_array[1], page_array[2], page_array[1]);
       // $('#page').select2("val","");
    }else{
        var span_error = $('<span>').attr("for","page").addClass("error sp-error").html("<br/>Please select page");
        span_error.insertAfter($('#page'));
        setTimeout(function(){
            span_error.remove();
        },2000);
    }
});
// Navigation Link Add
$(document).on("keyup", ".link", function (e) {
    if ($(this).val() == "") {
        $(this).css("border", "1px solid red");
    } else {
        $(this).css("border", "");
    }
});
$(document).on("click", "#btn_link", function (e) {
    if ($("#url").val() == "") {
        $("#url").css("border", "1px solid red");
    }
    if ($("#title").val() == "") {
        $("#title").css("border", "1px solid red");
    }
    if ($("#url").val() !== "" && $("#title").val() !== "") {
        addToNestable("a","dd-handle", "Link", "0", $("#title").val(), $("#url").val(), $("#title").val());
        $("#url").val("");
        $("#title").val("");
    }
});
$(document).on("click", "#btn_widget", function (e) {
    $(".sp-error").remove();
    if($("#widget").val() != ""){
        var widget_array = $("#widget").val().split("||");
        addToWidgetNestable("dd-handle",widget_array[0],widget_array[1]);
        //$('#widget').select2("val","");
    }else{
        var span_error = $('<span>').attr("for","widget").addClass("error sp-error").html("<br/>Please select widget");
        span_error.insertAfter($('#widget'));
        setTimeout(function(){
            span_error.remove();
        },2000);
    }
});
$(document).on("click", "#btn_locker", function (e) {
    $(".sp-error").remove();
    if($("#locker").val() != ""){
        addToLockerNestable("dd-handle",$("#locker").val());
        $("#locker").val("");
    }else{
        var span_error = $('<span>').attr("for","widget").addClass("error sp-error").html("<br/>Please enter locker");
        span_error.insertAfter($('#locker'));
        setTimeout(function(){
            span_error.remove();
        },2000);
    }
});
$(document).on("click", "#btn_franchise_post_code", function (e) {
    var is_valid = true;
    if ($("#post_code").val() == "") {
        $("#post_code").css("border", "1px solid red");
        is_valid = false;
    }else{
        if($("#post_code").val().length < 2){
            $("#post_code").css("border", "1px solid red");
            is_valid = false;
        }
    }
    if (is_valid == true) {
        addToPostCodeNestable("dd-handle", $("#post_code").val().toLowerCase());
        $("#post_code").val("");
    }
});

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

$(document).on("click", "#btn_time_add", function (e) {
    $(".sp-error").remove();
    
    if($("#disable_time").val() != ""){
        
        var disable_time = $("#disable_time").val();
        
        if(disable_time!="specific"){
            addToTimeNestable("dd-handle",disable_time);
        }else{
            var specific_from = parseInt($("#specific_from").val());
            var specific_till = parseInt($("#specific_till").val());
            
            
            for(var i=specific_from;i<specific_till;i++){
                var time = pad(i,2) + ":00-" + pad(i+1,2) + ":00";
                addToTimeNestable("dd-handle",time);   
            }
            
        }
        
        
        //$('#widget').select2("val","");
    }else{
        var span_error = $('<span>').attr("for","disable_time").addClass("error sp-error").html("<br/>Please select time");
        span_error.insertAfter($('#disable_time'));
        setTimeout(function(){
            span_error.remove();
        },2000);
    }
});