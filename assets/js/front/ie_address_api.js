
var address_url = "";
var getAddresses = function (code) {

    address_url = loq_api_url + "Capture/Interactive/Find/v1.10/json3.ws?Key=" + api_key + "&Text=" + code + "&IsMiddleware=false&Countries=" + iso_country_code + "&Limit=10&Language=en&origin=52.377956,4.897070";
    console.log(address_url);

    $("#div_addresses").hide();

    if (code != "") {
        $.ajax({
            type: "get",
            dataType: "json",
            url: address_url,
            cache: true,
            success: function (response) {


                if (response.Items.length > 0) {

                    // $(".address-building").val("");
                    // $(".address-street").val("");

                    $("#addresses").html("");
                    optionHtml = "<option value='' >Please select your address</option>";
                    optionHtml += "<option value='-1' >Not on the list?</option>";
                    $("#addresses").html(optionHtml);
                    $("#div_addresses").show();

                    $.each(response.Items, function (i, item) {

                        if (item.Type == "Address") {
                            optionHtml = "<option value='" + item.Text + " - " + item.Description + "' >" + item.Text + " - " + item.Description + "</option>";
                            $("#addresses").append(optionHtml);

                        } else {
                            getAddresses2(response.Items[0].Id)
                        }

                    });

                    //$("#addresses").append(html);
                } else {
                    $("#addresses").html("");
                    $("#div_addresses").hide();
                }
            },
            error: function (data) {
                // alert("Some went wrong.");
                console.log(data);
                //EnableDisableForm("Enable", jQuery("#body_area"), current_context, btn_1_text);
            }
        });
    }
}

var getAddresses2 = function (id) {

    address_url = address_url + "&Container=" + id;
    $.ajax({
        type: "get",
        dataType: "json",
        url: address_url,
        cache: true,
        success: function (response) {

            var html = "";

            if (response.Items.length > 0) {

                $.each(response.Items, function (i, item) {
                    html += "<option value='" + item.Text + " - " + item.Description + "' >" + item.Text + " - " + item.Description + "</option>";

                });

                $("#addresses").append(html);
            } else {
                //$("#addresses").html("");
                //$("#div_addresses").hide();
            }
        },
        error: function (data) {
            // alert("Some went wrong.");
            console.log(data);
            //EnableDisableForm("Enable", jQuery("#body_area"), current_context, btn_1_text);
        }
    });

}




