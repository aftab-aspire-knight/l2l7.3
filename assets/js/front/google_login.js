
var auth2;
var googleUser; // The current user
function onSuccess(googleUser) {
    var profile = googleUser.getBasicProfile();

    console.log('ID: ' + profile.getId());
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);


    socialLogin(profile.getId(), profile.getGivenName(), profile.getFamilyName(), profile.getEmail(), "google")


}
function onFailure(error) {
    console.log(error);
}
function googleLogout() {

    if (gapi.auth2.getAuthInstance()) {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

}

function renderButton() {

    gapi.load('auth2', function () {
        console.log('User signed auth2.');
        gapi.auth2.init();
    });

    gapi.signin2.render('gs2', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'light',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });

}

setTimeout(function () {
    $(".abcRioButtonContents span:first").text("Google");
    $(".abcRioButtonContents span:last").text("Google");
}, 2000);