//pca Intigration
if (typeof (pca) != "undefined")
{
    var is_valid = true;
    var EmailValid = true;
    var PhoneValid = true;
    pca.on("load", function (type, id, control) {
        $("#countrycode").hide();
    });

    pca.on("load", function (type, Id, control) {
        control.listen("validate", function (isValid) {
            if (isValid) {
                EmailValid = true;
            } else if (type == 'emailvalidation')
            {
                //is_valid = false;
                EmailValid = false;
                $('#reg_email_address').addClass("error");
                $('<span>').addClass("error").attr("id", $('#reg_email_address').attr("id") + "_error").html($('#reg_email_address').attr("data-invalid-error-message")).insertAfter($('#reg_email_address'));
                var parentCLass = $('.l2l-last-name').parents('li:first').attr('class');
                if (parentCLass != 'active')
                {
                    $('.btn-reg-back').click();

                }

            }

            if (PhoneValid == true && EmailValid == true)
            {
                is_valid = true;
            }


        });

        control.listen("validate", function (phoneValid) {
            if (phoneValid) {
                PhoneValid = true;
            } else if (type == 'mobilevalidation')
            {
                //is_valid = false;
                PhoneValid = false;
                $('#reg_phone_number').addClass("error");
                $('<span>').addClass("error").attr("id", $(this).attr("id") + "_error").html($('#reg_phone_number').attr("data-error-message")).insertAfter($('#reg_phone_number'));

                var parentCLass = $('.l2l-last-name').parents('li:first').attr('class');
                if (parentCLass != 'active')
                {
                    $('.btn-reg-back').click();

                }
            }
            if (PhoneValid == true && EmailValid == true)
            {
                is_valid = true;
            }
        });

    });
//pcs Ends		

}

var nav_bar_white_offset = 0;
var franchise_id = 0;
var more_pickup = 0;
var click_pickup = 0;
var click_delivery = 0;

var select_item_page_url = "select-items";
var select_address_page_url = "address";
var checkout_page_url = "checkout";
var store_link_generator_page_url = "store-link-generator";
var session_variable_name = "love_2_laundry_session_cart_v4";
var session_expire_time_variable_name = "love_2_laundry_session_expire_time_v4";
var discount_variable_name = "love_2_laundry_discount_record_v4";
var preference_variable_name = "love_2_laundry_preference_record_v4";
var time_collection_variable_name = "love_2_laundry_time_collection_record_v4";
var pickup_date_time_variable_name = "love_2_laundry_pickup_date_time_record_v4";
var delivery_date_time_variable_name = "love_2_laundry_delivery_date_time_record_v4";
var location_collection_variable_name = "love_2_laundry_location_collection_record_v4";
var address_variable_name = "love_2_laundry_address_record_v4";

var regularly_variable_name = "love_2_laundry_regularly_record_v4";
var regularly_names_array = [];
regularly_names_array[0] = 'Just once';
regularly_names_array[1] = 'Weekly';
regularly_names_array[2] = 'Every two weeks';

var credit_card_variable_name = "love_2_laundry_credit_card_record_v4";
var credit_card_token_variable_name = "love_2_laundry_credit_card_token_record_v4";
var invoice_edit_load = "love_2_laundry_invoice_edit_load_record_v4";
var web_name = "Love2Laundry";
var web_url = site_url;
var api_url = web_url + "api/";
var category_image_path = web_url + "uploads/categories/";
var category_thumb_image_path = web_url + "uploads/categories/thumb/";
var service_image_path = web_url + "uploads/services/";
var service_thumb_image_path = web_url + "uploads/services/thumb/";
var upload_image_path = web_url + "uploads/images/";
var is_login = false;

if (location.hash) {
    location.href = location.hash;
}

var ClearAllCache = function () {
    Clear_Storage_Record(session_variable_name);
    Clear_Storage_Record(session_expire_time_variable_name);
    Clear_Storage_Record(discount_variable_name);
    Clear_Storage_Record(preference_variable_name);
    Clear_Storage_Record(time_collection_variable_name);
    Clear_Storage_Record(pickup_date_time_variable_name);
    Clear_Storage_Record(delivery_date_time_variable_name);
    Clear_Storage_Record(location_collection_variable_name);
    Clear_Storage_Record(address_variable_name);
    Clear_Storage_Record(credit_card_variable_name);
    Clear_Storage_Record(credit_card_token_variable_name);
    Clear_Storage_Record(invoice_edit_load);
}
var Current_Page_URL = function () {
    return window.location.pathname.replace("/l2l/", "").replace("/", "").replace(".html", "").toLowerCase();
}
var IsEmailValid = function (email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
var scrollToItem = function (elem) {
    $('html, body').stop().animate({
        'scrollTop': elem.offset().top - 100
    }, 500, 'swing', function () {
        //window.location.hash = target;

    });
}
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
if ($('.navbar-white').length > 0) {
    nav_bar_white_offset = $('.navbar-white').offset();
}
if ($('#btn_order_top').length > 0) {
    $(document).on("click", "#btn_order_top", function () {
        scrollToItem($(".intro"));
        return false;
    });
}
if ($('#btn_select_time').length > 0) {
    $(document).on("click", "#btn_select_time", function () {
        console.log($(this));
        scrollToItem($("#time_select_area"));
        return false;
    });
}
if ($('#btn_enter_detail').length > 0) {
    $(document).on("click", "#btn_enter_detail", function () {
        scrollToItem($("#address_information"));
        return false;
    });
}
var ResizingWithScroll = function (current_scroll_position, is_scroll) {
    var window_width = $(window).width();
    var window_height = $(window).height();
    if (is_scroll == false) {
        if ($("#basket").length > 0) {
            $("#basket").niceScroll({cursorborder: "", cursorcolor: "#cfcfcf", boxzoom: false});
        }
        if ($("#additems").length > 0) {
            $("#additems").niceScroll({cursorborder: "", cursorcolor: "#cfcfcf", boxzoom: false});
        }
        if ($("#wash_preference_area").length > 0) {
            $("#wash_preference_area").niceScroll({cursorborder: "", cursorcolor: "#cfcfcf", boxzoom: false});
        }
    }
    if ($(".page-section").length > 0) {
        var intro_section_context = $(".page-section").height();
        if (current_scroll_position > intro_section_context) {
            $('.header').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
        }
    }
    if ($(".top-section").length > 0) {
        var intro_section_context = $(".top-section");
        intro_section_context.css({'width': window_width, 'height': window_height});
        var intro_section_height = parseInt(intro_section_context.css("height").replace("px", ""));
        if (current_scroll_position > intro_section_height) {
            $('.header').addClass('fixed');
        } else {
            $('.header').removeClass('fixed');
        }

    }
    if (window_width < 768) {
        if ($(".crowd-fixed").length > 0) {
            var crowd_height = $(".crowd-header").height();
            if (isMobile.Android()) {
                $("#body_area, #new_template_body").removeClass("crowd-fixed");
                $('.header').css('top', crowd_height);
                $('.top-section-container').css('margin-top', crowd_height);
            } else if (isMobile.iOS()) {
                $("#body_area, #new_template_body").removeClass("crowd-fixed");
                $(".header").removeAttr("style");
                $(".top-section-container").removeAttr("style");
            } else {
                $('.header').css('top', crowd_height);
                $('.top-section-container').css('margin-top', crowd_height);
            }
        }
    }
    if (window_width > 991) {
        if ($(".crowd-header").length > 0) {
            if ($(".crowd-header").css("display") == "block") {
                $("#body_area, #new_template_body").addClass("crowd-fixed");
            }
        }
    }
}
/*
 var isMobile = {
 Android: function () {
 return navigator.userAgent.match(/Android/i);
 },
 BlackBerry: function () {
 return navigator.userAgent.match(/BlackBerry/i);
 },
 iOS: function () {
 return navigator.userAgent.match(/iPhone|iPad|iPod/i);
 },
 Opera: function () {
 return navigator.userAgent.match(/Opera Mini/i);
 },
 Windows: function () {
 return navigator.userAgent.match(/IEMobile/i);
 },
 any: function () {
 return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
 }
 };
 */
var Update_Time_Collection_Section = function (type, date_position, time_position) {
    var time_collection = Get_Storage_Record(time_collection_variable_name);
    if (time_collection != null) {
        date_position = parseInt(date_position);
        time_position = parseInt(time_position);

        var collection_array = [];
        var server_date_time_array = $("#server_date_time").val().split(" ");
        var selected_pick_date_time = Get_Storage_Record(pickup_date_time_variable_name);
        var selected_delivery_date_time = Get_Storage_Record(delivery_date_time_variable_name);
        var collection = JSON.parse(time_collection);
        var delivery_option = collection.delivery_option;
        var tab_area = null;
        var list_area = null;
        var start_position = 0;
        if (type == "Pick") {
            tab_area = $("#pick_date_slots");
            list_area = $("#pick_time_slots");
        } else {
            tab_area = $("#delivery_date_slots");
            list_area = $("#delivery_time_slots");
        }
        //tab_area.html("");
        //list_area.html("");
        if (type == "Pick") {
            collection_array = collection.pick_up;
            if (date_position == 12) {
                date_position += 1;
            }
            var p_date_position = parseFloat(date_position / 4).toFixed(2);
            if (p_date_position >= 1) {
                if (p_date_position == 3) {
                    p_date_position -= 1;
                }
                start_position = parseInt(p_date_position) * 4;
            }
        } else {
            collection_array = collection.delivery;
            start_position = date_position;
            if (start_position >= 20) {
                start_position = 20;
            }
        }
        for (var i = 0; i < 4; i++) {
            start_position = parseInt(start_position);
            var current_array = collection_array[start_position];
            if (!current_array)
            {
                continue;
            }
            var tab_list = $('<li>').appendTo(tab_area);
            if (start_position == date_position) {
                tab_list.addClass("active");
            }
            var tab_anchor = $('<a>').attr("href", "#").attr("data-position", start_position).appendTo(tab_list);
            if (type == "Pick") {
                tab_anchor.addClass("btn-pickup-date-select");
            } else {
                tab_anchor.addClass("btn-delivery-date-select");
            }
            var span_day = $('<span>').addClass("day").html(current_array.Day_Full_Name).appendTo(tab_anchor);
            var span_date = $('<span>').addClass("date").html("-" + current_array.Month_Short_Name).appendTo(tab_anchor);
            $("<strong>").html(current_array.Date_Number).prependTo(span_date);
            var ul_list = $('<ul>').appendTo(list_area);
            if (type == "Pick") {
                ul_list.addClass("pickup-time-listing").attr("id", "pickup_time_listing_" + start_position);
            } else {
                ul_list.addClass("delivery-time-listing").attr("id", "delivery_time_listing_" + start_position);
            }
            if (start_position != date_position) {
                ul_list.hide();
            } else {
                ul_list.addClass("active");
            }
            var current_time_array = current_array.Times;
            for (var k = 0; k < current_time_array.length; k++) {
                var ul_li_list = $('<li>').appendTo(ul_list);
                if (current_time_array[k].Class == "Available") {
                    ul_li_list.addClass("available");
                }
                var list_anchor = $('<a>').attr("href", "#").attr("data-date-position", start_position).attr("data-time-position", k).html(current_time_array[k].Time).appendTo(ul_li_list);
                if (type == "Pick") {
                    ul_li_list.addClass("pick-time");
                    list_anchor.addClass("btn-pickup-time-select");
                } else {
                    ul_li_list.addClass("delivery-time");
                    list_anchor.addClass("btn-delivery-time-select");
                    if (i == 0) {
                        /*Hassan changes 29-01-2018*/
                        if (k < time_position && time_position > 2) {
                            ul_li_list.removeClass("available");
                        }
                    }
                    var pick_selected_time_context = $(".pick-time.selected").find("a");
                    if (pick_selected_time_context.length > 0) {
                        var pick_collection_array = collection.pick_up;
                        var p_date = parseInt(pick_selected_time_context.attr("data-date-position"));
                        var p_time = parseInt(pick_selected_time_context.attr("data-time-position"));

                        if (delivery_option != "None") {
                            if (pick_collection_array[p_date + 1] != undefined) {
                                if (pick_collection_array[p_date + 1]['Day_Full_Name'] == "Sunday") {

                                    p_date += 1;
                                    if (p_time < 8) {
                                        p_time = 8;
                                    }
                                }
                            }


                            if (pick_collection_array[p_date]['Day_Full_Name'] == "Saturday") {
                                if (delivery_option == "Saturday Sunday Both Pickup Delivery To Monday After 3") {
                                    p_date += 1;
                                    if (p_time < 8) {
                                        p_time = 8;

                                    }
                                }
                            }

                        }
                        if (p_date == start_position) {
                            /*Hassan changes 29-01-2018*/
                            if (k < p_time) {
                                ul_li_list.removeClass("available");
                            }
                            if (k < p_time && time_position > 2) {
                                ul_li_list.removeClass("available");
                            }
                        }
                    }
                }
            }
            start_position += 1;
        }
        var tab_last_list = $('<li>').addClass("more-dates").appendTo(tab_area);
        var tab_last_anchor = $('<a>').attr("href", "javascript:void(0);").html("More Dates").appendTo(tab_last_list);
        var ul_list = $('<ul>').appendTo(list_area);
        ul_list.hide();
        var loop_date_count = 0;
        if (type == "Pick") {
            tab_last_anchor.attr("id", "btn-more-pickup-date-select");
            ul_list.addClass("pickup-time-listing").attr("id", "pickup_time_listing_more");
            if (more_pickup == 1)
            {
                loop_date_count = 16;
            } else
            {
                loop_date_count = 4;
            }
        } else {
            tab_last_anchor.attr("id", "btn-more-delivery-date-select");
            ul_list.addClass("delivery-time-listing").attr("id", "delivery_time_listing_more");
            if (more_pickup == 1)
            {
                loop_date_count = 24;
            } else
            {
                loop_date_count = 6;
            }
        }
        for (var j = 0; j < loop_date_count; j++) {
            var current_array = collection_array[j];
            var ul_li_list = $('<li>').addClass("available").appendTo(ul_list);
            var list_anchor = $('<a>').attr("href", "#").attr("data-date-position", j).appendTo(ul_li_list);
            if (type == "Pick") {
                list_anchor.addClass("btn-more-pickup-time-select");
            } else {
                list_anchor.addClass("btn-more-delivery-time-select");
            }
            $('<span>').addClass("day").html(current_array.Day_Full_Name).appendTo(list_anchor);
            $('<span>').addClass("date").html(current_array.Date_Number + " - ").appendTo(list_anchor);
            $('<span>').addClass("month").html(current_array.Month_Short_Name).appendTo(list_anchor);
            if (type == "Delivery") {
                if (j < date_position) {
                    ul_li_list.removeClass("available");
                }
            }
        }
        $("<i>").addClass("l2l-calender").prependTo(tab_last_anchor);
        if (type == "Pick") {
            var is_search_in_list = true;
            if (selected_pick_date_time != null) {
                var selected_pick_date_time_array = selected_pick_date_time.split("|||");
                if (selected_pick_date_time_array.length == 3) {
                    is_search_in_list = false;
                    $("#selected_pickup_time").html(selected_pick_date_time_array[1] + " " + selected_pick_date_time_array[2]);
                } else {
                    date_position = parseInt(selected_pick_date_time_array[0]);
                    time_position = parseInt(selected_pick_date_time_array[1]);
                }
            }
            if (is_search_in_list == true) {
                var pick_up_context = $("#pickup_time_listing_" + date_position);
                if (pick_up_context != null) {
                    var is_found = false;
                    if (time_position != 0) {
                        var pick_li_context = $(pick_up_context.find("li")[time_position]);
                        if (pick_li_context.hasClass("available")) {
                            is_found = true;
                            var dt = collection_array[date_position];
                            $("#selected_pickup_time").html(dt.Times[time_position].Time + " " + dt.Day_Full_Name + ", " + dt.Month_Full_Name + " " + dt.Date_Name);
                            pick_li_context.addClass("selected");
                        }
                    }
                    if (is_found == false) {
                        if (!pick_up_context.find("li").hasClass("available")) {
                            date_position = (date_position + 1);
                        }
                        pick_up_context = $("#pickup_time_listing_" + date_position);
                        $(".pickup-time-listing").hide();
                        $(".btn-pickup-date-select").parent().removeClass("active");
                        $(".btn-pickup-date-select").each(function () {
                            if ($(this).attr("data-position") == date_position) {
                                $(this).parent().addClass("active");
                            }
                        });
                        $("#pickup_time_listing_" + date_position).addClass("active").fadeIn();
                        pick_up_context.find("li").each(function () {
                            if ($(this).hasClass("available")) {
                                var selected_array = [];
                                selected_array['Date_Position'] = $(this).find("a").attr("data-date-position");
                                selected_array['Time_Position'] = $(this).find("a").attr("data-time-position");
                                Add_Storage_Record(pickup_date_time_variable_name, selected_array['Date_Position'] + "|||" + selected_array['Time_Position']);
                                var dt = collection_array[selected_array['Date_Position']];
                                $("#selected_pickup_time").html(dt.Times[selected_array['Time_Position']].Time + " " + dt.Day_Full_Name + ", " + dt.Month_Full_Name + " " + dt.Date_Name);
                                $(this).addClass("selected");
                                return false;
                            }
                        });
                    }
                }
            }
            if (selected_delivery_date_time != null) {
                var selected_delivery_date_time_array = selected_delivery_date_time.split("|||");
                if (selected_delivery_date_time_array.length == 2) {
                    date_position = parseInt(selected_delivery_date_time_array[0]);
                    time_position = parseInt(selected_delivery_date_time_array[1]);
                }
            }
            //Delivery Differences of 10
            if (Get_Storage_Record(pickup_date_time_variable_name) != null) {
                var selected_pickup_date_time = Get_Storage_Record(pickup_date_time_variable_name).split("|||");
                date_position = parseInt(selected_pickup_date_time[0]);
                time_position = parseInt(selected_pickup_date_time[1]);
            }
            var is_difference_loop = true;

            if (delivery_option != "None") {
                if (collection_array[date_position + 1] != undefined) {
                    if (collection_array[date_position + 1]['Day_Full_Name'] == "Sunday") {

                        date_position += 1;
                        is_difference_loop = false;
                    }
                }
            }
            if (is_difference_loop == true) {
                for (var i = 0; i < 13; i++) {
                    var delivery_collection = collection.delivery[date_position];
                    if (delivery_collection != undefined) {
                        if (delivery_collection.Times[time_position] == undefined) {
                            date_position += 1;
                            // Code Change 06-02-2017
                            time_position = 1;
                            if (delivery_collection.Times.length <= 13) {
                                i = 13;
                            }
                        }
                        time_position += 1;
                    }
                }
            }
            Update_Time_Collection_Section("Delivery", date_position, time_position);
        } else {
            var is_search_in_list = true;
            if (selected_delivery_date_time != null) {
                var selected_delivery_date_time_array = selected_delivery_date_time.split("|||");
                if (selected_delivery_date_time_array.length == 3) {
                    $("#selected_delivery_time").html(selected_delivery_date_time_array[1] + " " + selected_delivery_date_time_array[2]);
                    is_search_in_list = false;
                } else {
                    date_position = parseInt(selected_delivery_date_time_array[0]);
                    time_position = parseInt(selected_delivery_date_time_array[1]);
                }
            }
            if (is_search_in_list == true) {
                var delivery_context = $("#delivery_time_listing_" + date_position);
                if (delivery_context != null) {
                    var is_found = false;
                    if (time_position != 0) {
                        var delivery_li_context = $(delivery_context.find("li")[time_position]);
                        if (delivery_li_context.hasClass("available")) {
                            Add_Storage_Record(delivery_date_time_variable_name, date_position + "|||" + time_position);
                            is_found = true;
                            var dt = collection_array[date_position];
                            $("#selected_delivery_time").html(dt.Times[time_position].Time + " " + dt.Day_Full_Name + ", " + dt.Month_Full_Name + " " + dt.Date_Name);
                            delivery_li_context.addClass("selected");
                        }
                    }
                    if (is_found == false) {
                        if (!delivery_context.find("li").hasClass("available")) {
                            date_position = (date_position + 1);
                        }
                        delivery_context = $("#delivery_time_listing_" + date_position);
                        $(".delivery-time-listing").hide();
                        $(".btn-delivery-date-select").parent().removeClass("active");
                        $(".btn-delivery-date-select").each(function () {
                            if ($(this).attr("data-position") == date_position) {
                                $(this).parent().addClass("active");
                            }
                        });
                        $("#delivery_time_listing_" + date_position).addClass("active").fadeIn();
                        delivery_context.find("li").each(function () {
                            if ($(this).hasClass("available")) {
                                var selected_array = [];
                                selected_array['Date_Position'] = $(this).find("a").attr("data-date-position");
                                selected_array['Time_Position'] = $(this).find("a").attr("data-time-position");
                                Add_Storage_Record(delivery_date_time_variable_name, selected_array['Date_Position'] + "|||" + selected_array['Time_Position']);
                                var dt = collection_array[selected_array['Date_Position']];
                                $("#selected_delivery_time").html(dt.Times[selected_array['Time_Position']].Time + " " + dt.Day_Full_Name + ", " + dt.Month_Full_Name + " " + dt.Date_Name);
                                $(this).addClass("selected");
                                return false;
                            }
                        });
                    }
                }
            }
            $("#time_select_area").show();
            $("#time_loader").hide();
        }
    }

    if (click_pickup == 1)
    {
        click_pickup = 0;
        click_delivery = 0;
        $("#pick_date_slots").find("li").removeClass("active");
        $(".pickup-time-listing").hide();
        $("#pickup_time_listing_more").fadeIn();

    } else if (click_delivery == 1)
    {
        click_pickup = 0;
        click_delivery = 0;
        $("#delivery_date_slots").find("li").removeClass("active");
        $(".delivery-time-listing").hide();
        $("#delivery_time_listing_more").fadeIn();

    }
}

var FetchTimeCollection = function () {

    $("#time_loader").show();
    // $(".time-values").html("");
    $("#time_select_area").hide();
    $("#time_error_area").remove();

    setTimeout(function () {
        if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0)
        {
            AsyncVAR = true;
        } else {
            AsyncVAR = false
        }
        $.ajax({
            type: "POST",
            url: $("#time_collection_url").val(),
            cache: false,
            async: AsyncVAR,
            data: "post_code=" + $("#postal_code").val() + "&more_pickup=" + more_pickup,
            success: function (response) {
                var response_data = response.split("||");
                if (response_data[0] == "success") {
                    var collection = JSON.parse(response_data[1]);
                    $("#franchise_id").val(collection.id);
                    franchise_id = collection.id;
                    $("#minimum_order_amount_later").val(collection.minimum_order_amount);
                    $("#minimum_order_amount").val(collection.minimum_order_amount);
                    if (Total_Cart_Item() != 0) {
                        $("#item_minimum_amount").html("Minimum Order Value " + $("#website_currency").val() + $("#minimum_order_amount").val());
                    } else {
                        $("#item_minimum_amount").html("Minimum Order Value " + $("#website_currency").val() + $("#minimum_order_amount").val());
                    }
                    setTimeout(function () {
                        $("#item_minimum_amount").addClass("blink");
                        setTimeout(function () {
                            $("#item_minimum_amount").removeClass("blink");
                        }, 500);
                    }, 1000);
                    //alert("sssss");
                    getPickupDates();
                    //Update_Time_Collection_Section("Pick", date_position, time_position);
                } else {
                    var error_area = $('<div>').attr("id", "time_error_area").addClass("col-md-12").insertAfter($("#time_select_area"));
                    $('<div>').addClass("alert alert-danger").html(response_data[1]).appendTo(error_area);
                    scrollToItem($("#time_error_area"));
                    $("#time_loader").hide();
                }
            },
            error: function (data) {
                if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0)
                {
                    getPickupDates();
                    return false;
                }

                console.log(data);
                $("#time_loader").hide();
                var error_area = $('<div>').attr("id", "time_error_area").addClass("col-md-12").insertAfter($("#time_select_area"));
                $('<div>').addClass("alert alert-danger").html("some thing went wrong").appendTo(error_area);
            }
        });
    }, 1000);
}
var EnableDisableForm = function (type, main_context, btn_1_context, btn_1_text) {
    if (type == "Disable") {
        main_context.find("input[type='text']").attr("readonly", "readonly");
        main_context.find("input[type='radio']").attr("readonly", "readonly");
        main_context.find("input[type='checkbox']").attr("readonly", "readonly");
        main_context.find("select").attr("readonly", "readonly");
        main_context.find("textarea").attr("readonly", "readonly");
        main_context.find("button").attr("disabled", "disabled");
        main_context.find("a").addClass("disabled");
        if (btn_1_context.attr("id") == "btn_subscriber" && btn_1_context.hasClass("btnsend")) {
            btn_1_context.html("");
            $('<i>').addClass("fas fa-spinner fa-spin").prependTo(btn_1_context);
        } else {
            btn_1_context.html("&nbsp;&nbsp;" + btn_1_text);
            if ($("#new_template_body").length > 0) {
                $('<i>').addClass("fa fa-spinner fa-spin").prependTo(btn_1_context);
            } else {
                $('<i>').addClass("l2l-setting fas fa-pulse").prependTo(btn_1_context);
            }
        }
        $(".sp-error").remove();
        $(".sp-correct").remove();
        $(".msg-box").remove();
    } else {
        main_context.find("input[type='text']").removeAttr("readonly");
        main_context.find("input[type='radio']").removeAttr("readonly");
        main_context.find("input[type='checkbox']").removeAttr("readonly");
        main_context.find("select").removeAttr("readonly");
        main_context.find("textarea").removeAttr("readonly");
        main_context.find("button").removeAttr("disabled");
        main_context.find("a").removeClass("disabled");
        btn_1_context.html(btn_1_text);
    }
}
var AjaxFormSubmit = function (frm_name, msg_box, btn_1, body_area, is_refresh) {
    var main_context = $("#" + frm_name);
    var form_context = main_context;
    var btn_1_context = $("#" + btn_1);
    var btn_1_text = btn_1_context.html();
    if (body_area != "") {
        main_context = $("#" + body_area);
    }
    EnableDisableForm("Disable", main_context, btn_1_context, btn_1_text);
    var options = {
        complete: function (response) {
            console.log(response.responseText);
            var data = response.responseText.split("||");
            if (data[0] == "success") {
                if (data[1] == "t") {
                    window.location = data[2];
                } else if (data[1] == "field") {
                    var field_context = $('#' + data[2]);
                    field_context.addClass("correct");
                    var span = $('<span>').addClass("correct sp-correct").html(data[3]);
                    span.insertAfter(field_context);
                    setTimeout(function () {
                        span.remove();
                        field_context.removeClass("correct");
                    }, 2000);
                } else {
                    if (msg_box != "") {
                        var msg_area = $("#" + msg_box);
                        msg_area.addClass("msg-box alert alert-success").html(data[2]);
                        setTimeout(function () {
                            msg_area.html("");
                            msg_area.removeClass();
                            if (data[1] == "forgot") {
                                $("#forgot_area").fadeOut();
                            } else if (data[1] == "testimonial") {
                                $("#testimonial_form_box").hide();
                                $("#testimonial_slide_box").fadeIn();
                            } else if (data[1] == "login") {
                                $("span.error").remove();
                                $(".error").removeClass("error");
                                $("#btn_login_close").trigger("click");
                                var field_data = JSON.parse(data[3]);
                                $("#pass_area").remove();
                                $("#btn_login_show").remove();
                                $("#first_name").val(field_data.FirstName).attr("disabled", "disabled");
                                $("#last_name").val(field_data.LastName).attr("disabled", "disabled");
                                $("#email_address").val(field_data.EmailAddress).attr("disabled", "disabled");
                                $("#phone_number").val(field_data.Phone).attr("disabled", "disabled");
                                $("#location").html("");
                                is_login = true;

                                //$("#location").trigger("change");
                                if (field_data.member_card_records) {
                                    var card_list_area = $("<div>").addClass("form-group col-sm-6").appendTo($("#card_list_area"));
                                    var card_label = $("<label>").addClass("col-sm-4 control-label").html("Select Card ").appendTo(card_list_area);
                                    $("<span>").addClass("red-color").html("*").appendTo(card_label);
                                    var card_input_area = $("<div>").addClass("col-sm-8").appendTo(card_list_area);
                                    var card_input = $("<select>").addClass("form-control").attr("id", "card_list").attr("name", "card_list").appendTo(card_input_area);
                                    $("<option>").attr("value", "").html("Select").appendTo(card_input);
                                    $("<option>").attr("value", "0").html("Add New").appendTo(card_input);
                                    for (var i = 0; i < field_data.member_card_records.length; i++) {
                                        var current_record = field_data.member_card_records[i];
                                        $("<option>").attr("value", current_record.ID + "||" + current_record.Title + "||" + current_record.Name + "||" + current_record.CardNumber + "||" + current_record.CardNumberMasked + "||" + current_record.ExpireMonth + "||" + current_record.ExpireYear + "||" + current_record.SecurityCode + "||" + current_record.SecurityCodeMasked).html(current_record.Title).appendTo(card_input);
                                    }
                                }
                                //$("#locker_area").hide();
                                setTimeout(function () {
                                    FetchTimeCollection();
                                }, 1000);
                            }
                        }, 2000);
                        scrollToItem(msg_area);
                    } else {
                        $('<br>').attr("clear", "all").appendTo(form_context);
                        var msg_area = $('<div>').addClass("msg-box alert alert-success").html(data[2]).appendTo(form_context);
                        setTimeout(function () {
                            msg_area.remove();
                        }, 2000);
                    }
                    if (data[1] == "msg-t") {
                        setTimeout(function () {
                            window.location = data[3];
                        }, 2000);
                    } else if (data[1] == "forgot" || data[1] == "testimonial") {
                        form_context.hide();
                    }
                }
                if (is_refresh != "") {
                    form_context[0].reset();
                    EnableDisableForm("Enable", main_context, btn_1_context, btn_1_text);
                }
            } else {
                if (data[1] == "t") {
                    window.location = data[2];
                } else if (data[1] == "msg") {
                    var msg_area = $('<div>').addClass("msg-box alert alert-danger").html(data[2]).appendTo(form_context);
                    setTimeout(function () {
                        msg_area.remove();
                    }, 2000);
                } else {

                    var msg_area = $('<div>').addClass("msg-box alert alert-danger").html(data[2]).appendTo(form_context);
                    setTimeout(function () {
                        msg_area.remove();
                    }, 5000);

                    var error_field_context = $('#' + data[1]);
                    error_field_context.addClass("error");
                    var error_span = $('<span>').attr("id", error_field_context.attr("id") + "_error").addClass("error sp-error").html(data[2]);
                    error_span.insertAfter(error_field_context);
                    setTimeout(function () {
                        error_span.remove();
                    }, 2000);
                    error_field_context.focus();
                    if (form_context.attr("id") == "frm_register" && data[1] == "reg_email_address") {
                        $(".reg-area").hide();
                        $($(".reg-area")[0]).fadeIn();
                    }
                }
                EnableDisableForm("Enable", main_context, btn_1_context, btn_1_text);
            }
        },
        error: function (response) {
            console.log(response.responseText);
            EnableDisableForm("Enable", main_context, btn_1_context, btn_1_text);
        }
    };
    form_context.ajaxSubmit(options);
}
var Clear_Storage_Record = function (name) {
    localStorage.removeItem(name);
}
var Add_Storage_Record = function (name, value) {
    localStorage.setItem(name, value);
}
var Get_Storage_Record = function (name) {
    if (localStorage.getItem(name) == null) {
        return null;
    } else {
        return localStorage.getItem(name);
    }
}
var Total_Cart_Price = function () {
    var cart_records = Get_Storage_Record(session_variable_name);
    if (cart_records == null) {
        return "0.00";
    } else {
        var json_array = [];
        var total_price = 0;
        json_array = JSON.parse(cart_records);
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            total_price += parseFloat((parseInt(data.qty) * parseFloat(data.price)));
        }
        return total_price.toFixed(2);
    }
}
var Current_Item_Price = function (id) {
    var item_total_price = 0;
    var cart_records = Get_Storage_Record(session_variable_name);

    if (cart_records != null) {
        var json_array = [];
        json_array = JSON.parse(cart_records);
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            if (data.id == id) {
                item_total_price = parseFloat(data.price);
                break;
            }
        }
    }
    return item_total_price;
}
var Total_Cart_Item = function () {
    var total_qty = 0;
    var cart_records = Get_Storage_Record(session_variable_name);
    if (cart_records != null) {
        var json_array = [];
        json_array = JSON.parse(cart_records);
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            total_qty += parseInt(data.qty);
        }
    }
    return total_qty;
}
var Current_Item_Total = function (id) {
    var item_total_qty = 0;
    var cart_records = Get_Storage_Record(session_variable_name);
    if (cart_records != null) {
        var json_array = [];
        json_array = JSON.parse(cart_records);
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            if (data.id == id) {
                item_total_qty = parseInt(data.qty);
                break;
            }
        }
    }
    return item_total_qty;
}
var Add_Cart_Item = function (id, title, price, desktop_img, mobile_img, qty, category_id, package, popup_message, preference_show) {
    var json_array = [];
    var cart_records = Get_Storage_Record(session_variable_name);
    var current_data = {"id": id, "title": encodeURIComponent(title), "price": price, "desktop_img": encodeURIComponent(desktop_img), "mobile_img": encodeURIComponent(mobile_img), "qty": qty, "category_id": category_id, "package": package, "preference_show": preference_show};
    if (cart_records == null) {
        json_array.push(JSON.stringify(current_data));
        Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
        localStorage.setItem(session_expire_time_variable_name, new Date());
        if (popup_message != "") {
            $("#category_popup_message").html("<p>" + popup_message + "</p>");
            // $("#btn_category_popup_message").click();
        }
    } else {
        json_array = JSON.parse(cart_records);
        var is_found = false;
        var is_popup_show = true;
        if (popup_message != "") {
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                if (data.category_id == category_id) {
                    is_popup_show = false;
                    break;
                }
            }
        } else {
            is_popup_show = false;
        }
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            if (data.id == id && data.title == encodeURIComponent(title)) {
                current_data.qty = (parseInt(data.qty) + parseInt(qty));
                json_array[i] = JSON.stringify(current_data);
                is_found = true;
                break;
            }
        }
        if (is_found == false) {
            json_array.push(JSON.stringify(current_data));
        }
        if (is_popup_show == true) {
            $("#category_popup_message").html("<p>" + popup_message + "</p>");
            // $("#btn_category_popup_message").click();
        }
        Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
    }
    Update_Cart_Section();
}
var Remove_Cart_Item = function (id, title) {
    var json_array = [];

    var cart_records = Get_Storage_Record(session_variable_name);
    if (cart_records != null) {
        json_array = JSON.parse(cart_records);
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            if (data.id == id && data.title == encodeURIComponent(title)) {
                data.qty -= 1;
                if (data.qty == 0) {
                    json_array.splice(i, 1);
                } else {
                    json_array[i] = JSON.stringify(data);
                }
                break;
            }
        }
        if (json_array.length > 0) {
            Add_Storage_Record(session_variable_name, JSON.stringify(json_array));
        } else {
            Clear_Storage_Record(session_variable_name);
            Clear_Storage_Record(session_expire_time_variable_name);
        }
        Update_Cart_Section();
    }
}
var Update_Cart_Section = function () {

    var total_cart_items = Total_Cart_Item();
    if (parseInt(total_cart_items) > 0)
    {

        $('#continue_text').html('Your Basket');


    } else
    {
        $('#continue_text').html('Skip to Continue');

    }
    $("#item_total_count").html(total_cart_items);
    var cart_area = $("#cart_listing");
    cart_area.html("");
    if (Total_Cart_Item() != 0) {
        var package_count = 0;
        $("#clear_cart_area").show();
        var json_array = [];
        var show_preference_area = false;
        var package_price_total = 0;
        json_array = JSON.parse(Get_Storage_Record(session_variable_name));
        for (var i = 0; i < json_array.length; i++) {
            var data = JSON.parse(json_array[i]);
            var row = $('<tr>').appendTo(cart_area);
            $('<td>').html(decodeURIComponent(data.title)).appendTo(row);
            $('<td>').addClass("text-right").html(data.qty).appendTo(row);
            $('<td>').addClass("text-right").html($("#website_currency").val() + data.price).appendTo(row);
            $('<td>').addClass("text-right").html($("#website_currency").val() + parseFloat(data.qty * data.price).toFixed(2)).appendTo(row);
            var anchor_area = $('<td>').addClass("text-right").appendTo(row);
            var anchor = $('<a>').attr("href", "#").addClass("remove-view-cart").attr("data-id", data.id).attr("data-title", decodeURIComponent(data.title)).appendTo(anchor_area);
            $('<i>').addClass("l2l-trashcan").appendTo(anchor);
            if (data.package == "Yes") {
                package_count += data.qty;
                package_price_total = parseFloat(parseFloat(package_price_total) + parseFloat(data.price)).toFixed(2);
            }
            if (data.preference_show == "Yes") {
                show_preference_area = true;
            }
        }
        var sub_total = parseFloat(Total_Cart_Price());
        var discount_total = 0;
        var preference_total = 0;
        if (show_preference_area == true) {
            $("#btn_open_preference_view").show();
            var preference_records = Get_Storage_Record(preference_variable_name);
            if (preference_records != null) {
                var preference_array = preference_records.split("[]");
                for (var k = 0; k < preference_array.length; k++) {
                    var current_preference_array = decodeURIComponent(preference_array[k]).split("||");
                    if (current_preference_array[3] != "" && current_preference_array[3] != undefined) {
                        var pref_price = parseFloat(current_preference_array[3]);
                        if (current_preference_array[4] == "Yes") {
                            if (package_count > 0) {
                                pref_price = (pref_price * package_count);
                            }
                        }
                        preference_total += pref_price;
                    }
                }
            }
        } else {
            $("#btn_open_preference_view").hide();
        }
        sub_total = sub_total.toFixed(2);
        preference_total = preference_total.toFixed(2);
        var grand_total = parseFloat(sub_total) + parseFloat(preference_total);
        var discount_record = Get_Storage_Record(discount_variable_name);

        if (discount_record != null) {

            var price_after_package = parseFloat(grand_total - package_price_total).toFixed(2);

            var discount_array = discount_record.split("|");
            if (discount_array[3] == "Percentage") {
                discount_total = parseFloat((price_after_package * discount_array[2]) / 100).toFixed(2);
            } else {
                discount_total = parseFloat(discount_array[2]).toFixed(2);
            }

            grand_total = parseFloat(grand_total - discount_total).toFixed(2);
        } else {
            grand_total = grand_total.toFixed(2);
        }

        var sub_total_row = $('<tr>').appendTo(cart_area);
        $('<td>').attr("rowspan", 4).appendTo(sub_total_row);
        var sub_total_label_area = $('<td>').attr("colspan", 3).addClass("text-right no-border").appendTo(sub_total_row);
        $('<strong>').html("Sub Total").appendTo(sub_total_label_area);
        $('<td>').addClass("text-right").html($("#website_currency").val() + sub_total).appendTo(sub_total_row);
        if (preference_total > 0) {
            var preference_total_row = $('<tr>').appendTo(cart_area);
            var preference_total_label_area = $('<td>').attr("colspan", 3).addClass("text-right no-border").appendTo(preference_total_row);
            $('<strong>').html("Preferences Total").appendTo(preference_total_label_area);
            $('<td>').addClass("text-right").html($("#website_currency").val() + preference_total).appendTo(preference_total_row);
        }
        if (discount_record != null) {
            var discount_array = discount_record.split("|");
            var discount_total_row = $('<tr>').appendTo(cart_area);
            var discount_total_label_area = $('<td>').attr("colspan", 3).addClass("text-right no-border").appendTo(discount_total_row);
            if (discount_array[3] == "Percentage") {
                $('<strong>').html(discount_array[0] + " Total<br/><small>Type : " + discount_array[0] + "<br/>Worth : (" + discount_array[2] + "%)</small>").appendTo(discount_total_label_area);
            } else {
                $('<strong>').html(discount_array[0] + " Total<br/><small>Type : " + discount_array[0] + "<br/>Worth : (" + $("#website_currency").val() + discount_array[2] + ")</small>").appendTo(discount_total_label_area);
            }
            $('<td>').addClass("text-right").html("(" + $("#website_currency").val() + discount_total + ")").appendTo(discount_total_row);
        }
        var grand_total_row = $('<tr>').addClass("bg-success").appendTo(cart_area);
        var grand_total_label_area = $('<td>').attr("colspan", 3).addClass("text-right no-border").appendTo(grand_total_row);
        $('<strong>').html("Grand Total").appendTo(grand_total_label_area);
        $('<td>').addClass("text-right").html($("#website_currency").val() + grand_total).appendTo(grand_total_row);
        $("#item_total_price").html($("#website_currency").val() + grand_total);
    } else {
        $("#clear_cart_area").hide();
        $("#btn_open_preference_view").hide();
        $("#item_total_price").html($("#website_currency").val() + "0.00");
        var row = $('<tr>').appendTo(cart_area);
        $('<td>').attr("colspan", "5").html("No items in cart.<br/>You can add items later and complete your order with minimum amount of " + $("#website_currency").val() + $("#minimum_order_amount").val()).appendTo(row);
    }
    if (Current_Page_URL() == select_item_page_url) {
        $("#item_minimum_amount").html("Minimum Order Value " + $("#website_currency").val() + $("#minimum_order_amount").val());
    } else {
        if (Total_Cart_Item() != 0) {
            $("#item_minimum_amount").html("Minimum Order Value " + $("#website_currency").val() + $("#minimum_order_amount").val());
        } else {
            $("#item_minimum_amount").html("Minimum Order Value " + $("#website_currency").val() + $("#minimum_order_amount").val());
        }
    }
    setTimeout(function () {
        $("#item_minimum_amount").addClass("blink");
        setTimeout(function () {
            $("#item_minimum_amount").removeClass("blink");
        }, 500);
    }, 1000);
}
var Update_Check_Out_Cart_Section = function () {
    if ($("#check_out_cart_listing").length > 0) {
        var total_cart_items = Total_Cart_Item();
        var cart_area = $("#check_out_cart_listing");
        cart_area.html("");
        var show_preference_area = false;
        var package_price_total = 0;
        if (Total_Cart_Item() != 0) {
            var package_count = 0;
            var json_array = [];
            json_array = JSON.parse(Get_Storage_Record(session_variable_name));
            var header_row = $("<li>").appendTo(cart_area);
            $("<span>").addClass("icon").html("&nbsp;").appendTo(header_row);
            $("<span>").addClass("productname").html("Description").appendTo(header_row);
            $("<span>").addClass("qty").html("Qty").appendTo(header_row);
            $("<span>").addClass("price").html("Price").appendTo(header_row);
            $("<span>").html("&nbsp;").appendTo(header_row);
            for (var i = 0; i < json_array.length; i++) {
                var data = JSON.parse(json_array[i]);
                var row = $('<li>').appendTo(cart_area);
                var img_span = $('<span>').addClass("basket-icon-img").appendTo(row);
                var img_path = decodeURIComponent(data.desktop_img);
                if (img_path == "logo.jpg") {
                    img_path = upload_image_path + img_path;
                } else {
                    img_path = service_thumb_image_path + img_path;
                }
                $("<img>").addClass("img-circle img-responsive").attr("style", "width:50px;").attr("src", img_path).appendTo(img_span);
                $('<span>').addClass("productname").html(decodeURIComponent(data.title)).appendTo(row);
                $('<span>').addClass("qty").html(data.qty).appendTo(row);
                $('<span>').addClass("price").html($("#website_currency").val() + parseFloat(data.qty * data.price).toFixed(2)).appendTo(row);
                $('<a>').attr("href", "#").addClass("remove-view-cart l2l-trashcan").attr("data-id", data.id).attr("data-title", decodeURIComponent(data.title)).appendTo(row);
                if (data.package == "Yes") {
                    package_count += data.qty;
                    package_price_total = parseFloat(parseFloat(package_price_total) + parseFloat(data.price)).toFixed(2);
                }
                if (data.preference_show == "Yes") {
                    show_preference_area = true;
                }
            }
        } else {
            var row = $('<li>').appendTo(cart_area);
            $('<p>').html("No items in cart.<br/>You can add items later and complete your order with minimum amount of " + $("#website_currency").val() + $("#minimum_order_amount").val()).appendTo(row);
        }
        var sub_total = parseFloat(Total_Cart_Price());
        var discount_total = 0;
        var preference_total = 0;
        if (sub_total > 0) {
            if (show_preference_area == true) {
                var preference_records = Get_Storage_Record(preference_variable_name);
                if (preference_records != null) {
                    var preference_array = preference_records.split("[]");
                    for (var k = 0; k < preference_array.length; k++) {
                        var current_preference_array = decodeURIComponent(preference_array[k]).split("||");
                        if (current_preference_array[3] != "" && current_preference_array[3] != undefined) {
                            var pref_price = parseFloat(current_preference_array[3]);
                            if (current_preference_array[4] == "Yes") {
                                if (package_count > 0) {
                                    pref_price = (pref_price * package_count);
                                }
                            }
                            preference_total += pref_price;
                        }
                    }
                }
            }
        } else {
            sub_total = parseFloat($("#minimum_order_amount").val());
        }
        sub_total = sub_total.toFixed(2);
        preference_total = preference_total.toFixed(2);
        var grand_total = parseFloat(sub_total) + parseFloat(preference_total);
        var discount_record = Get_Storage_Record(discount_variable_name);
        if (discount_record != null) {
            var price_after_package = parseFloat(grand_total - package_price_total).toFixed(2);
            var discount_array = discount_record.split("|");
            if (discount_array[3] == "Percentage") {
                discount_total = parseFloat((price_after_package * discount_array[2]) / 100).toFixed(2);
            } else {
                discount_total = parseFloat(discount_array[2]).toFixed(2);
            }
            grand_total = parseFloat(grand_total - discount_total).toFixed(2);
        } else {
            grand_total = grand_total.toFixed(2);
        }
        var sub_total_row = $('<li>').addClass("checkout-total first").appendTo(cart_area);
        $("<div>").html("Sub Total").addClass("checkout-title").appendTo(sub_total_row);
        $("<div>").html($("#website_currency").val() + sub_total).addClass("checkout-value").appendTo(sub_total_row);
        if (preference_total > 0) {
            var preference_total_row = $('<li>').addClass("checkout-total").appendTo(cart_area);
            $("<div>").html("Preferences Total").addClass("checkout-title").appendTo(preference_total_row);
            $("<div>").html($("#website_currency").val() + preference_total).addClass("checkout-value").appendTo(preference_total_row);
        }
        if (Total_Cart_Item() == 0) {
            Clear_Storage_Record(discount_variable_name);
        } else {
            if (Get_Storage_Record(discount_variable_name) == null) {
                var discount_row = $('<li>').addClass("basket-discount").attr("id", "discount_area").appendTo(cart_area);
                $('<label>').html("Discount Code").appendTo(discount_row);
                $('<input>').attr("type", "text").attr("id", "discount_code").addClass("form-control field-box blank").appendTo(discount_row);
                $('<button>').addClass("btn btn-danger").attr("type", "button").attr("id", "btn_apply_discount").html("Apply").appendTo(discount_row);
                if ($("#is_first_order").val() == "Yes") {
                    $("<p>").html("<strong>OR</strong>").appendTo(discount_row);
                    $('<label>').html("Referral Code").appendTo(discount_row);
                    $('<input>').attr("type", "text").attr("id", "referral_code").addClass("form-control field-box blank").appendTo(discount_row);
                    $('<button>').addClass("btn btn-danger").attr("type", "button").attr("id", "btn_apply_referral").html("Apply").appendTo(discount_row);
                }
            } else {
                var discount_total_row = $('<li>').addClass("checkout-total first").appendTo(cart_area);
                if (discount_array[3] == "Percentage") {
                    $("<div>").html(discount_array[0] + " Total<br/><small>Type : " + discount_array[0] + "<br/>Worth : (" + discount_array[2] + "%)</small>").addClass("checkout-title").appendTo(discount_total_row);
                } else {
                    $("<div>").html(discount_array[0] + " Total<br/><small>Type : " + discount_array[0] + "<br/>Worth : (" + $("#website_currency").val() + discount_array[2] + ")</small>").addClass("checkout-title").appendTo(discount_total_row);
                }
                var discount_price_area = $("<div>").html("(" + $("#website_currency").val() + discount_total + ")").addClass("checkout-value").appendTo(discount_total_row);
                $("<br>").appendTo(discount_price_area);
                var remove_discount_area = $('<small>').appendTo(discount_price_area);
                $('<a>').attr("href", "#").attr("id", "remove_discount").addClass("pull-right btn btn-sm btn-default").text("Remove").appendTo(remove_discount_area);
            }

        }
        var grand_total_row = $('<li>').addClass("checkout-total first").appendTo(cart_area);
        $("<div>").html("Grand Total").addClass("checkout-title").appendTo(grand_total_row);
        $("<div>").html($("#website_currency").val() + grand_total).addClass("checkout-value").appendTo(grand_total_row);
        $("#cart_sub_total").val(sub_total);
        $("#cart_preference_total").val(preference_total);
        $("#cart_discount_total").val(discount_total);
        $("#cart_grand_total").val(grand_total);
    }
}
var startTimer = function (duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}
$(function ($) {
    var fiveMinutes = 60 * 30,
            display = $('.notify-spot');
    if (display.length > 0) {
        startTimer(fiveMinutes, display);
    }
});
$(document).on("click", "#remove_discount", function () {
    var result = confirm('Are you sure you want to remove ?');
    if (result == true) {
        Clear_Storage_Record(discount_variable_name);
        Update_Cart_Section();
        Update_Check_Out_Cart_Section();
    }
    return false;
});
$(document).on("click", "#btn_apply_discount", function () {
    var current_context = $(this);
    var is_valid = true;
    if ($("#discount_code").val() == "") {
        $("#discount_code").addClass("error-area");
        is_valid = false;
    }
    if (is_valid == true) {

        var json_array_package = [];
        var price_of_package = 0;
        var containsPackage = 0;
        json_array_package = JSON.parse(Get_Storage_Record(session_variable_name));

        for (var i = 0; i < json_array_package.length; i++) {
            var data_package = JSON.parse(json_array_package[i]);
            if (data_package.package == "Yes") {
                containsPackage = 1;
                price_of_package += parseFloat(data_package.price);
            }

        }

        $(".discount-error").remove();
        current_context.attr("disabled", "disabled");
        current_context.html("&nbsp;&nbsp;Apply");
        $("#discount_code").attr("disabled", "disabled");
        $('<i>').addClass("l2l-laundry-setting l2l-spin").prependTo(current_context);
        $.ajax({
            type: "post",
            url: $("#order_discount_url").val(),
            cache: false,
            data: "code=" + $("#discount_code").val() + "&total=" + $("#cart_grand_total").val() + "&contains_package=" + containsPackage + "&price_of_package=" + price_of_package,
            success: function (response) {
                var data = response.split("||");
                if (data[0] == "success") {
                    Add_Storage_Record(discount_variable_name, data[1]);
                    Update_Cart_Section();
                    Update_Check_Out_Cart_Section();
                    $("#discount_area").remove();
                } else {
                    var error_li = $("<li>").addClass("discount-error").insertAfter($("#discount_area"));
                    $("<div>").addClass("alert alert-danger text-right").html(data[1]).appendTo(error_li);
                    setTimeout(function () {
                        error_li.remove();
                    }, 2000);
                }
                current_context.html("Apply");
                current_context.removeAttr("disabled");
                $("#discount_code").removeAttr("disabled");
            },
            error: function (data) {
                console.log(data);
                current_context.html("Apply");
                current_context.removeAttr("disabled");
                $("#discount_code").removeAttr("disabled");
            }
        });
    }
});
$(document).on("click", "#btn_apply_referral", function () {
    var current_context = $(this);
    var is_valid = true;
    if ($("#referral_code").val() == "") {
        $("#referral_code").addClass("error-area");
        is_valid = false;
    }
    if (is_valid == true) {
        $(".discount-error").remove();
        current_context.attr("disabled", "disabled");
        current_context.html("&nbsp;&nbsp;Apply");
        $("#referral_code").attr("disabled", "disabled");
        $('<i>').addClass("l2l-laundry-setting l2l-spin").prependTo(current_context);
        $.ajax({
            type: "post",
            url: $("#order_referral_url").val(),
            cache: false,
            data: "code=" + $("#referral_code").val(),
            success: function (response) {
                var data = response.split("||");
                if (data[0] == "success") {
                    Add_Storage_Record(discount_variable_name, data[1]);
                    Update_Cart_Section();
                    Update_Check_Out_Cart_Section();
                    $("#discount_area").remove();
                } else {
                    var error_li = $("<li>").addClass("discount-error").insertAfter($("#discount_area"));
                    $("<div>").addClass("alert alert-danger text-right").html(data[1]).appendTo(error_li);
                    setTimeout(function () {
                        error_li.remove();
                    }, 2000);
                }
                current_context.html("Apply");
                current_context.removeAttr("disabled");
                $("#referral_code").removeAttr("disabled");
            },
            error: function (data) {
                console.log(data);
                current_context.html("Apply");
                current_context.removeAttr("disabled");
                $("#referral_code").removeAttr("disabled");
            }
        });
    }
});
var AddPreferences = function () {
    var preferences = "";
    $(".footer-preferences").each(function () {
        preferences += encodeURIComponent($(this).val()) + "[]";
    });
    preferences += $("#account_notes").val() + "[]" + $("#additional_instructions").val();
    Add_Storage_Record(preference_variable_name, preferences);
    Update_Cart_Section();
    Update_Check_Out_Cart_Section();
}
var SelectPreference = function () {
    var preferences = Get_Storage_Record(preference_variable_name);
    if (preferences != null) {
        var count = 0;
        var preference_array = preferences.split("[]");
        $(".footer-preferences").each(function () {
            $(this).find("option").each(function () {
                if (preference_array[count]) {
                    if ($(this).attr("value") == decodeURIComponent(preference_array[count])) {
                        $(this).attr("selected", "selected");
                    } else {
                        //$(this).removeAttr("selected");
                    }
                }
            });
            count += 1;
        });
        $("#account_notes").val(preference_array[count]);
        $("#additional_instructions").val(preference_array[count + 1]);
    }
}
if ($(".cart-bar").length > 0) {
    if ($("#itemlisting").length > 0) {
        if ($("#invoice_type").val() == "edit") {
            $.ajax({
                type: "POST",
                url: $("#select_item_page_url").val(),
                cache: false,
                async: false,
                success: function (response) {
                    var data = response.split("||");
                    if (data[0] == "success") {
                        ClearAllCache();
                        console.log(data);
                        var record = JSON.parse(data[1]);
                        $("#account_notes").val(record.AccountNotes);
                        $("#additional_instructions").val(record.AdditionalInstructions);
                        if (record.service_records.length > 0) {
                            for (var i = 0; i < record.service_records.length; i++) {
                                var current_service = record.service_records[i];
                                Add_Cart_Item(current_service.ServiceID, current_service.Title, current_service.Price, current_service.DesktopImage, current_service.MobileImage, current_service.Quantity, current_service.CategoryID, current_service.Package, "", current_service.PreferenceArea);
                            }
                        }
                        if (record.preference_records.length > 0) {
                            for (var i = 0; i < record.preference_records.length; i++) {
                                var current_preference = record.preference_records[i];
                                var combo_id = "#cmb_" + current_preference.FieldName;
                                var selected_index = 0;
                                $(combo_id + ' option').each(function () {
                                    var current_value_array = $(this).val().split("||");
                                    if (current_value_array[0] == current_preference.PreferenceID) {
                                        $(combo_id).prop('selectedIndex', selected_index);
                                    }
                                    selected_index += 1;
                                });
                            }
                            AddPreferences();
                        }
                        var selected_address = "";
                        selected_address = "Location=" + record.Location + "|||";
                        selected_address += "PostCode=" + record.PostalCode + "|||";
                        if (record.Location == "Add" || record.Location == "0" || record.Location == "" || record.Location == null) {
                            selected_address += "BuildingName=" + record.BuildingName + "|||";
                            selected_address += "StreetName=" + record.StreetName + "|||";
                            selected_address += "Town=" + record.Town;
                        } else {
                            selected_address += "Locker=" + record.Locker;
                        }
                        Add_Storage_Record(address_variable_name, selected_address);
                        if (record.card_record) {
                            Add_Storage_Record(credit_card_variable_name, record.card_record.CardID + "||" + record.card_record.Title + "||" + record.card_record.Name + "||" + record.card_record.CardNumber + "||" + record.card_record.CardNumberMasked + "||" + record.card_record.ExpireMonth + "||" + record.card_record.ExpireYear + "||" + record.card_record.SecurityCode + "||" + record.card_record.SecurityCodeMasked + "||true");
                        }
                        Add_Storage_Record(pickup_date_time_variable_name, record.PickupDate + "|||" + record.PickupTime + "|||" + record.Pick_Full_Date);
                        Add_Storage_Record(delivery_date_time_variable_name, record.DeliveryDate + "|||" + record.DeliveryTime + "|||" + record.Delivery_Full_Date);
                        if (record.DiscountType != "None") {
                            if (record.DiscountType == "Discount") {
                                Add_Storage_Record(discount_variable_name, "Discount|" + record.DiscountID + "|" + record.DiscountWorth + "|" + record.DType + "|" + record.DiscountCode);
                            } else {
                                Add_Storage_Record(discount_variable_name, "Referral|" + record.DiscountID + "|" + record.DiscountWorth + "|" + record.DType + "|" + record.DiscountCode);
                            }
                        }
                        Update_Cart_Section();
                        Update_Check_Out_Cart_Section();
                        // Add_Storage_Record(invoice_edit_load,true);
                    } else {
                        //$("#invoice_type").val("add");
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    }
    $(document).on("click", "#btn_cart_item_later", function () {
        Clear_Storage_Record(session_variable_name);
    });
    $(document).on("click", "#clear_cart_area", function () {
        var current_context = $(this);
        var result = confirm('Are you sure you want to clear cart?');
        if (result == true) {
            Clear_Storage_Record(session_variable_name);
            $(".mix").each(function () {
                $(this).find(".in-cart-qty").html(0);
                $(this).find(".less").addClass("disabled");
            });
            Update_Cart_Section();
            Update_Check_Out_Cart_Section();
        }
        return false;
    });
    $(document).on("click", ".add-view-cart", function () {
        var current_context = $(this);
        var parent_area = $("#service_item_" + current_context.attr("data-id"));
        Add_Cart_Item(current_context.attr("data-id"), current_context.attr("data-title"), current_context.attr("data-price"), current_context.attr("data-desktop,image"), current_context.attr("data-mobile,image"), 1, current_context.attr("data-category-id"), current_context.attr("data-package"), current_context.attr("data-popup-message"), current_context.attr("data-preference-show"));
        var current_item_total = Current_Item_Total(current_context.attr("data-id"));
        if (current_item_total > 99) {
            parent_area.find(".in-cart-qty").html("99+");
        } else {
            parent_area.find(".in-cart-qty").html(current_item_total);
        }
        parent_area.find(".less").removeClass("disabled");
        Update_Cart_Section();
        Update_Check_Out_Cart_Section();
        return false;
    });
    $(document).on("click", ".remove-view-cart", function () {
        var current_context = $(this);
        Remove_Cart_Item(current_context.attr("data-id"), current_context.attr("data-title"));
        var parent_area = $("#service_item_" + current_context.attr("data-id"));
        var current_item_total = Current_Item_Total(current_context.attr("data-id"));
        if (current_item_total == 0) {
            parent_area.find(".less").addClass("disabled");
        }
        if (current_item_total > 99) {
            parent_area.find(".in-cart-qty").html("99+");
        } else {
            parent_area.find(".in-cart-qty").html(current_item_total);
        }
        Update_Cart_Section();
        Update_Check_Out_Cart_Section();
        return false;
    });

    $(document).on("click", ".add-item-cart", function () {
        var current_context = $(this);
        var parent_area = current_context.parents(".mix");
        var category = current_context.attr("data-category-id");

        //alert(category);

        Add_Cart_Item(current_context.attr("data-id"), current_context.attr("data-title"), current_context.attr("data-price"), current_context.attr("data-desktop-image"), current_context.attr("data-mobile-image"), 1, current_context.attr("data-category-id"), current_context.attr("data-package"), current_context.attr("data-popup-message"), current_context.attr("data-preference-show"));
        parent_area.find(".less").removeClass("disabled");
        Update_Cart_Section();
        Update_Check_Out_Cart_Section();
        var qty_area = parent_area.find(".in-cart-qty");
        qty_area.removeClass("blink");
        qty_area.addClass("blink");

        var qty = parseInt(qty_area.text());
        category = parseInt(category);

        var body = '<h5>SAFE TO INCLUDE</h5><ul class="list-check"><li><i class="fa fa-check"></i> All personal laundry that can be washed & tumble dried.</li><li><i class="fa fa-check"></i> You can include bed linen and towel but we advise you to put them in a separate bag as they usually require a different washing/drying cycle.</li><li><i class="fa fa-check"></i> Bath mats</li></ul><h5>DO NOT INCLUDE</h5><ul class="list-cross"><li><i class="fa fa-times"></i> Dry clean only items</li><li><i class="fa fa-times"></i> Items that cannot be tumble dried.</li><li><i class="fa fa-times"></i> All items that are not suitable for machine washing and tumble drying</li><li><i class="fa fa-times"></i> Any garments that can shrunk during tumble drying process</li></ul>';
        if (qty == 0 && category == 10) {
            openModal("<h4>Wash Dry and Fold order what to include</h4>", body);
        }



        setTimeout(function () {
            var current_item_total = Current_Item_Total(current_context.attr("data-id"));
            if (current_item_total > 99) {
                qty_area.html("99+");
            } else {
                qty_area.html(current_item_total);
            }
            qty_area.removeClass("blink");
        }, 400);
        return false;
    });
    $(document).on("click", ".remove-item-cart", function () {
        var current_context = $(this);
        var current_item_total = Current_Item_Total(current_context.attr("data-id"));
        if (current_item_total != 0) {
            console.log(current_item_total);
            var parent_area = current_context.parents(".mix");
            Remove_Cart_Item(current_context.attr("data-id"), current_context.attr("data-title"));
            current_item_total = Current_Item_Total(current_context.attr("data-id"));
            if (current_item_total == 0) {
                parent_area.find(".less").addClass("disabled");
            }
            var qty_area = parent_area.find(".in-cart-qty");
            qty_area.removeClass("blink");
            qty_area.addClass("blink");
            setTimeout(function () {
                if (current_item_total > 99) {
                    qty_area.html("99+");
                } else {
                    qty_area.html(current_item_total);
                }
                qty_area.removeClass("blink");
            }, 400);
            Update_Cart_Section();
            Update_Check_Out_Cart_Section();
        }
        return false;
    });


    $(document).on("click", "#btn_open_cart_view", function () {
        var current_context = $(this);
        var time_out_time = 0;
        if ($("#wash_preference_area").css("display") == "block") {
            $("#btn_open_preference_view").trigger("click");
            time_out_time = 500;
        }
        setTimeout(function () {
            $("#cart_list_view_area").slideToggle("slow", function () {
                if (current_context.attr("data-text") == "My Cart") {
                    current_context.attr("data-text", "Close");
                    current_context.html("Close ");
                    $('<i>').addClass("l2l-save").prependTo(current_context);
                } else {
                    current_context.attr("data-text", "My Cart");
                    current_context.html("My Cart ");
                    $('<i>').addClass("l2l-shopping-cart").prependTo(current_context);
                }
            });
        }, time_out_time);
        return false;
    });
    $(document).on("click", ".btn_open_preference_view", function () {
        var current_context = $(this);
        //var href = $(this).data("href");
        var time_out_time = 0;
        if ($("#cart_list_view_area").css("display") == "block") {
            $("#btn_open_cart_view").trigger("click");
            time_out_time = 500;
        }
        setTimeout(function () {
            $("#wash_preference_area").slideToggle("slow", function () {
                if (current_context.attr("data-text") == "Preferences") {
                    SelectPreference();
                    current_context.attr("data-text", "Update");
                    current_context.html("Update ");

                    //$("#btn_cart_select_page_continue").hide();
                    $('<i>').addClass("l2l-save").prependTo(current_context);
                } else {


                    AddPreferences();
                    current_context.attr("data-text", "Preferences");
                    current_context.html("Preferences ");
                    //$("#btn_cart_select_page_continue").show()
                    $('<i>').addClass("l2l-laundry-setting").prependTo(current_context);
                }

            });
        }, time_out_time);


        return false;
    });

    ///////
    $(document).on("click", ".basket", function () {
        var current_context = $(this);
        var href = $(this).data("href");
        var time_out_time = 0;
        var total_cart_items = Total_Cart_Item();
        var minimum_order = parseFloat($("#minimum_order_amount").val());
        var sub_total = parseFloat(Total_Cart_Price());

        if (total_cart_items > 0 && sub_total < minimum_order) {
            openModal("Please fix the following issue", "Minimum Order Amount " + currency_symbol + minimum_order);
            return false;
        }


        if ($("#cart_list_view_area").css("display") == "block") {
            $("#btn_open_cart_view").trigger("click");
            time_out_time = 500;
        }
        setTimeout(function () {
            AddPreferences();
            window.top.location.href = href;
        }, time_out_time);


        return false;
    });

    // Clear_Preferences_Record();
    if (Get_Storage_Record(preference_variable_name) == null) {
        AddPreferences();
    }
    SelectPreference();
}
if ($("#btn_cart_address_page_continue").length > 0) {
    $(document).on("change", "#location", function () {
        var current_context = $(this);
        if (current_context.val() == "0" || current_context.val() == "Add") {

            $("#locker_area").hide();
            $("#locker").html("");
            $("#address_postal_area").show();

            $("#addresses").html();
            if (current_context.val() == "0") {
                $("#div_addresses").hide();

                $("#postal_code").val($("#hidden_postal_code").val());
                $("#building_name").val($("#hidden_building_name").val());
                $("#street_name").val($("#hidden_street_name").val());
                //$("#town").val("London");
            } else {
                $("#div_addresses").show();
                $("#postal_code").val($("#hidden_session_postal_code").val());
                $("#building_name").val("");
                $("#street_name").val("");
                //$("#town").val("");
            }

        } else {
            $("#locker_area").show();
            $("#locker").html("");
            var location_array = JSON.parse(Get_Storage_Record(location_collection_variable_name));
            if (location_array[0].length > 0) {
                for (var i = 0; i < location_array[0].length; i++) {
                    var current_array = location_array[0][i];
                    $('<option>').attr("value", "").html("Select").appendTo($("#locker"));
                    for (var k = 0; k < current_array['lockers'].length; k++) {
                        $('<option>').attr("value", current_array['lockers'][k]['Title']).html(current_array['lockers'][k]['Title']).appendTo($("#locker"));
                    }

                }
            }
            $("#address_postal_area").hide();
        }
        setTimeout(function () {
            var postal_code = $("#postal_code").val();
            if (postal_code.length >= post_code_length) {
                if (current_context.val() == "Add") {
                    getAddresses(postal_code);
                }
                FetchTimeCollection();
            }
        }, 1000);
    });




    $(document).on("click", "#btn_post_code", function () {

        var postal_code = $("#postal_code").val();
        if (postal_code.length >= post_code_length) {
            getAddresses(postal_code);
            FetchTimeCollection();
        }
    });

    $.ajax({
        type: "POST",
        url: $("#address_url").val(),
        cache: false,
        async: false,
        success: function (response) {
            if (response) {
                Add_Storage_Record(location_collection_variable_name, response);
                var location_array = JSON.parse(response);

                $("#location").html("");
                $('<option>').attr("value", "Add").html("Your Post Code").appendTo($("#location"));
                if (location_array[1]) {

                    $("<option>").attr("value", "0").html(location_array[1].Text).appendTo($("#location"));
                    $("#hidden_postal_code").val(location_array[1].PostalCode);
                    $("#hidden_building_name").val(location_array[1].BuildingName);
                    $("#hidden_street_name").val(location_array[1].StreetName);
                    $("#hidden_town").val(location_array[1].Town);
                    $("#address_postal_area").show();

                    $("#div_default").show();


                } else {
                    $("#locker_area").hide();
                    $("#locker").html("");
                    $("#address_postal_area").show();
                }
                if (location_array[0].length > 0) {
                    for (var i = 0; i < location_array[0].length; i++) {
                        var current_array = location_array[0][i];
                        $('<option>').attr("value", current_array['LocationID'] + "||" + current_array['Title']).html(current_array['Title']).appendTo($("#location"));
                    }
                }

                $("#location").trigger("change");


                $("#address_loader").hide();
            }
        },
        error: function (data) {
            console.log(data);
            $("#address_loader").hide();
        }
    });
    $(document).on("click", ".btn-pickup-time-select", function () {
        var current_context = $(this);
        if (current_context.parent().hasClass("available")) {
            $(".pick-time").removeClass("selected");
            Add_Storage_Record(pickup_date_time_variable_name, current_context.attr("data-date-position") + "|||" + current_context.attr("data-time-position"));
            Clear_Storage_Record(delivery_date_time_variable_name);

            Update_Time_Collection_Section("Pick", current_context.attr("data-date-position"), current_context.attr("data-time-position"));
        }
        return false;
    });
    $(document).on("click", ".btn-delivery-time-select", function () {
        var current_context = $(this);
        if (current_context.parent().hasClass("available")) {
            $(".delivery-time").removeClass("selected");
            Add_Storage_Record(delivery_date_time_variable_name, current_context.attr("data-date-position") + "|||" + current_context.attr("data-time-position"));
            Update_Time_Collection_Section("Delivery", current_context.attr("data-date-position"), current_context.attr("data-time-position"));
        }
        return false;
    });
    $(document).on("click", ".btn-more-pickup-time-select", function () {
        var current_context = $(this);
        if (current_context.parent().hasClass("available")) {
            var dt = parseInt(current_context.attr("data-date-position"));
            Update_Time_Collection_Section("Pick", dt, 0);
            $(".pickup-time-listing").hide();
            $(".btn-pickup-date-select").parent().removeClass("active");
            $(".btn-pickup-date-select").each(function () {
                if ($(this).attr("data-position") == dt) {
                    $(this).parent().addClass("active");
                }
            });
            $("#pickup_time_listing_" + dt).addClass("active").fadeIn();
        }
        return false;
    });
    $(document).on("click", ".btn-more-delivery-time-select", function () {
        var current_context = $(this);
        if (current_context.parent().hasClass("available")) {
            Update_Time_Collection_Section("Delivery", current_context.attr("data-date-position"), 0);
            $(".delivery-time-listing").hide();
            $(".btn-delivery-date-select").parent().removeClass("active");
            $(".btn-delivery-date-select").each(function () {
                if ($(this).attr("data-position") == current_context.attr("data-date-position")) {
                    $(this).parent().addClass("active");
                }
            });
            $("#delivery_time_listing_" + current_context.attr("data-date-position")).addClass("active").fadeIn();
        }
        return false;
    });
    $(document).on("click", ".btn-pickup-date-select", function () {
        var current_context = $(this);
        $("#pick_date_slots").find("li").removeClass("active");
        current_context.parent().addClass("active");
        $(".pickup-time-listing").removeClass("active").hide();
        $("#pickup_time_listing_" + current_context.attr("data-position")).addClass("active").fadeIn();
        return false;
    });
    $(document).on("click", ".btn-delivery-date-select", function () {
        var current_context = $(this);
        $("#delivery_date_slots").find("li").removeClass("active");
        current_context.parent().addClass("active");
        $(".delivery-time-listing").removeClass("active").hide();
        $("#delivery_time_listing_" + current_context.attr("data-position")).addClass("active").fadeIn();
        return false;
    });
    $(document).on("click", "#btn-more-pickup-date-select", function () {
        if (more_pickup == 1)
        {
            $("#pick_date_slots").find("li").removeClass("active");
            $(".pickup-time-listing").hide();
            $("#pickup_time_listing_more").fadeIn();
            return false;
        } else
        {
            more_pickup = 1;
            Clear_Storage_Record(time_collection_variable_name);
            //Clear_Storage_Record(pickup_date_time_variable_name);
            //Clear_Storage_Record(delivery_date_time_variable_name);

            delay(function () {
                click_pickup = 1;
                click_delivery = 0;

                FetchTimeCollection();


            }, 1000);
        }
    });
    $(document).on("click", "#btn-more-delivery-date-select", function () {
        if (more_pickup == 1)
        {
            $("#delivery_date_slots").find("li").removeClass("active");
            $(".delivery-time-listing").hide();
            $("#delivery_time_listing_more").fadeIn();
            return false;
        } else
        {
            more_pickup = 1;
            Clear_Storage_Record(time_collection_variable_name);
            //Clear_Storage_Record(pickup_date_time_variable_name);
            //Clear_Storage_Record(delivery_date_time_variable_name);

            delay(function () {
                click_pickup = 0;
                click_delivery = 1;

                FetchTimeCollection();

            }, 1000);
        }
    });
    // FetchTimeCollection();
    $(document).on("keyup", "#postal_code", function () {
        Clear_Storage_Record(time_collection_variable_name);
        Clear_Storage_Record(pickup_date_time_variable_name);
        Clear_Storage_Record(delivery_date_time_variable_name);
        delay(function () {
            FetchTimeCollection();
        }, 1000);
    });
    $(document).on("click", "#btn_cart_address_page_continue", function () {
        var current_context = $(this);
        $("#cart_error").remove();




        var regularity = 0;

        if ($('#regularity_0').is(":checked"))
        {
            regularity = 0;

        } else if ($('#regularity_1').is(":checked"))
        {
            regularity = 1;

        } else if ($('#regularity_2').is(":checked"))
        {
            regularity = 2;

        }

        var pickup_date = $("#pickup_date").val();
        var pickup_time = $("#pickup_time").val();

        var delivery_date = $("#delivery_date").val();
        var delivery_time = $("#delivery_time").val();

        var building_name = $("#building_name").val();
        var street_name = $("#street_name").val();
        var town = $("#town").val()
        var location = $("#location").val()
        var title = "Please fix the following issue";

        var is_valid = $("#frm_cart_member_address").valid();

        /*
         if (building_name == "") {
         openModal(title, "Please enter your building name.");
         return false;
         }
         
         if (street_name == "") {
         openModal(title, "Please enter your street name.");
         return false;
         }
         
         if (town == "") {
         openModal(title, "Please enter your town/city.");
         return false;
         }
         
         if (pickup_date == "") {
         openModal(title, "Please select proper pickup date.");
         return false;
         }
         
         if (pickup_time == "") {
         openModal(title, "Please select proper pickup time if you can't please change the pickup date.");
         return false;
         }
         
         if (delivery_date == "") {
         openModal(title, "Please select proper delivery date if you can't please change the pickup time.");
         return false;
         }
         
         if (delivery_time == "") {
         openModal(title, "Please select proper delivery time if you can't please change the delivery date.");
         return false;
         }
         */


        Add_Storage_Record(regularly_variable_name, "regularity =" + regularity + " |||" + regularity);


        var addresses = $("#addresses").val();
        var address = "";
        if (addresses == -1) {
            address = $("#address").val();
        } else {
            address = $("#addresses").val();
        }

        var selected_address = "";
        selected_address = "Location=" + $("#location").val() + "|||";
        selected_address += "PostCode=" + $("#postal_code").val() + "|||";

        selected_address += "BuildingName=" + address + "|||";
        selected_address += "StreetName=" + $("#street_name").val() + "|||";
        selected_address += "Town=" + $("#town").val();


        Add_Storage_Record(address_variable_name, selected_address);


        if (is_valid)
        {
            $("#btn_cart_address_page_continue").addClass("disabled");



            $("body").find("input[type='text']").attr("readonly", "readonly");
            $("body").find("input[type='radio']").attr("readonly", "readonly");
            $("body").find("input[type='checkbox']").attr("readonly", "readonly");
            $("body").find("select").attr("readonly", "readonly");
            $("body").find("textarea").attr("readonly", "readonly");
            $("body").find("button").attr("disabled", "disabled");
            $("body").find("a").addClass("disabled");
            current_context.html("&nbsp;&nbsp;Continue");
            $('<i>').addClass("l2l-laundry-setting l2l-spin").prependTo(current_context);

            $.ajax({
                type: "POST",
                url: $("#frm_cart_member_address").attr("action"),
                data: $("#frm_cart_member_address").serialize(),
                cache: false,
                async: false,
                success: function (response) {

                    if (response) {
                        var data = response.split("||");
                        //
                        if (data[0] == "success") {
                            window.location = data[1];
                        } else {
                            // $("#btn_cart_address_page_continue").removeClass("disabled");
                            var error_field_context = $('#' + data[1]);
                            error_field_context.addClass("error");
                            var error_span = $('<span>').attr("id", error_field_context.attr("id") + "_error").addClass("error sp-error").html(data[2]);
                            error_span.insertAfter(error_field_context);
                            scrollToItem(error_field_context);
                            setTimeout(function () {
                                error_span.remove();
                            }, 2000);
                            error_field_context.focus();
                        }
                        $("body").find("input[type='text']").removeAttr("readonly");
                        $("body").find("input[type='radio']").removeAttr("readonly");
                        $("body").find("input[type='checkbox']").removeAttr("readonly");
                        $("body").find("select").removeAttr("readonly");
                        $("body").find("textarea").removeAttr("readonly");
                        $("body").find("button").removeAttr("disabled");
                        $("body").find("a").removeClass("disabled");
                        current_context.html("&nbsp;&nbsp;Continue");
                        $('<i>').addClass("fa fa-arrow-right").prependTo(current_context);

                    }
                    return false;
                },
                error: function (data) {
                    console.log(data);
                    $("#btn_cart_address_page_continue").removeClass("disabled");
                    $("body").find("input[type='text']").removeAttr("readonly");
                    $("body").find("input[type='radio']").removeAttr("readonly");
                    $("body").find("input[type='checkbox']").removeAttr("readonly");
                    $("body").find("select").removeAttr("readonly");
                    $("body").find("textarea").removeAttr("readonly");
                    $("body").find("button").removeAttr("disabled");
                    $("body").find("a").removeClass("disabled");
                    current_context.html("&nbsp;&nbsp;Continue");
                    $('<i>').addClass("fa fa-arrow-right").prependTo(current_context);
                }
            });
        }



        return false;
    });


    $(document).on("change", "#card_list", function () {
        var current_context = $(this);
        if (current_context.val() == "" || current_context.val() == "0") {
            $(".c-field").removeAttr("disabled");
            $("#card_number").removeAttr("disabled");
            $("#card_id").val("");
            $("#c_valid").val("false");
            $(".c-field").val("");
            $("#card_number").attr("placeholder", "Number");
            $("#security_code_cv").attr("placeholder", "Security Code (CV2)");
            $("#card-element").show();
        } else {

            $("#card-element").hide();
            /*
             $("#card_number").val("");
             $("#security_code_cv").val("");
             var current_array = current_context.val().split("||");
             $(".c-field").attr("disabled", "disabled");
             $("#card_number").attr("disabled", "disabled");
             $("#card_id").val(current_array[0]);
             $("#c_valid").val("true");
             $("#title").val(current_array[1]);
             $("#name").val(current_array[2]);
             $("#card_number").attr("placeholder", current_array[4]);
             $("#expiry_month").val(current_array[5]);
             $("#expiry_year").val(current_array[6]);
             $("#security_code_cv").attr("placeholder", current_array[8]);
             */
        }
    });
    if (Get_Storage_Record(address_variable_name) != null) {



        var selected_address = Get_Storage_Record(address_variable_name).split("|||");

        var location_array = selected_address[0].split("=");
        if (location_array[1] == "") {
            location_array[1] = "0";
        }
        $("#location").val(location_array[1]);
        $("#location").trigger("change");
        var post_code_array = selected_address[1].split("=");
        $("#postal_code").val(post_code_array[1]);
        var third_array = selected_address[2].split("=");
        if (third_array[0] == "Locker") {
            $("#locker").val(third_array[1]);
        } else {
            $("#address_postal_area").show();
            $("#locker_area").hide();
            $("#building_name").val(third_array[1]);
            var street_array = selected_address[3].split("=");
            $("#street_name").val(street_array[1]);
            var town_array = selected_address[4].split("=");
            $("#town").val(town_array[1]);
        }
    }

    if (Get_Storage_Record(regularly_variable_name) != null) {

        var selected_regularly = Get_Storage_Record(regularly_variable_name).split("|||");
        var regularly_array = selected_regularly[1].split("=");
        var regularity = parseInt(regularly_array);
        $('#regularity_' + regularity).prop('checked', true);
    }

    if (Get_Storage_Record(credit_card_variable_name) != null) {
        var current_array = Get_Storage_Record(credit_card_variable_name).split("||");
        if (current_array[0] != "0" && current_array[0] != "") {
            $("#card_list").val(current_array[0] + "||" + current_array[1] + "||" + current_array[2] + "||" + current_array[3] + "||" + current_array[4] + "||" + current_array[5] + "||" + current_array[6] + "||" + current_array[7] + "||" + current_array[8]);
        } else {
            $("#card_list").val(current_array[0]);
        }
        $("#title").val(current_array[1]);
        $("#name").val(current_array[2]);
        if (current_array[9] == true) {
            $("#card_number").attr("placeholder", current_array[4]);
        } else {
            $("#card_number").val(current_array[3]);
        }
        $("#expiry_month").val(current_array[5]);
        $("#expiry_year").val(current_array[6]);
        if (current_array[9] == true) {
            $("#security_code_cv").attr("placeholder", current_array[8]);
        } else {
            $("#security_code_cv").val(current_array[7]);
        }
        $("#card_id").val(current_array[0]);
        $("#c_valid").val(true);
        if (current_array[9] == true) {
            $(".c-field").attr("disabled", "disabled");
            $("#card_number").attr("disabled", "disabled");
        } else {
            $(".c-field").removeAttr("disabled");
            $("#card_number").removeAttr("disabled");
        }
    }
}
if ($("#btn_place_order").length > 0) {

    if (Get_Storage_Record(credit_card_variable_name) != null) {
        var current_array = Get_Storage_Record(credit_card_variable_name).split("||");
        if (current_array[0] != "0" && current_array[0] != "") {
            $("#card_list").val(current_array[0] + "||" + current_array[1] + "||" + current_array[2] + "||" + current_array[3] + "||" + current_array[4] + "||" + current_array[5] + "||" + current_array[6] + "||" + current_array[7] + "||" + current_array[8]);
        } else {
            $("#card_list").val(current_array[0]);
        }
        $("#title").val(current_array[1]);
        $("#name").val(current_array[2]);
        if (current_array[9] == true) {
            $("#card_number").attr("placeholder", current_array[4]);
        } else {
            $("#card_number").val(current_array[3]);
        }
        $("#expiry_month").val(current_array[5]);
        $("#expiry_year").val(current_array[6]);
        if (current_array[9] == true) {
            $("#security_code_cv").attr("placeholder", current_array[8]);
        } else {
            $("#security_code_cv").val(current_array[7]);
        }
        $("#card_id").val(current_array[0]);
        $("#c_valid").val(true);
        if (current_array[9] == true) {
            $(".c-field").attr("disabled", "disabled");
            $("#card_number").attr("disabled", "disabled");
        } else {
            $(".c-field").removeAttr("disabled");
            $("#card_number").removeAttr("disabled");
        }
    }

    $(document).on("change", "#card_list", function () {
        var current_context = $(this);
        if (current_context.val() == "" || current_context.val() == "0") {
            $(".c-field").removeAttr("disabled");
            $("#card_number").removeAttr("disabled");
            $("#card_id").val("");
            $("#c_valid").val("false");
            $(".c-field").val("");
            $("#card_number").attr("placeholder", "Number");
            $("#security_code_cv").attr("placeholder", "Security Code (CV2)");

            $("#card-element").show();
        } else {
            $("#card-element").hide();

            $("#card_number").val("");
            $("#security_code_cv").val("");
            var current_array = current_context.val().split("||");
            $(".c-field").attr("disabled", "disabled");
            $("#card_number").attr("disabled", "disabled");
            $("#card_id").val(current_array[0]);
            $("#c_valid").val("true");
            $("#title").val(current_array[1]);
            $("#name").val(current_array[2]);
            $("#card_number").attr("placeholder", current_array[4]);
            $("#expiry_month").val(current_array[5]);
            $("#expiry_year").val(current_array[6]);
            $("#security_code_cv").attr("placeholder", current_array[8]);
        }
    });

    var selected_address = Get_Storage_Record(address_variable_name).split("|||");

    var selected_regularly = Get_Storage_Record(regularly_variable_name).split("|||");

    var address_area = $("#address_area");
    var location_array = selected_address[0].split("=");

    var regularly_array = selected_regularly[0].split("=");


    var post_code_array = selected_address[1].split("=");
    $("<dt>").html("Post Code").appendTo(address_area);
    $("<dd>").html(post_code_array[1]).appendTo(address_area);
    $("<hr/>").appendTo(address_area);
    var third_array = selected_address[2].split("=");
    if (third_array[0] == "Locker") {
        $("<dt>").html("Locker").appendTo(address_area);
        $("<dd>").html(third_array[1]).appendTo(address_area);
        $("<hr/>").appendTo(address_area);
    } else {
        $("<dt>").html("Address").appendTo(address_area);
        $("<dd>").html(third_array[1]).appendTo(address_area);
        $("<hr/>").appendTo(address_area);
        var street_array = selected_address[3].split("=");

        if (street_array[1] != "") {
            $("<dt>").html("Address Line 2").appendTo(address_area);
            $("<dd>").html(street_array[1]).appendTo(address_area);
            $("<hr/>").appendTo(address_area);
        }

        var town_array = selected_address[4].split("=");
        $("<dt>").html("Town").appendTo(address_area);
        $("<dd>").html(town_array[1]).appendTo(address_area);
        $("<hr/>").appendTo(address_area);
    }

    if (regularly_array[1])
    {
        var number = parseInt(regularly_array[1]);

        var name_regularly = regularly_names_array[number];
        $("<dt>").html("Regularly").appendTo(address_area);
        $("<dd>").html(name_regularly).appendTo(address_area);
        $("<hr/>").appendTo(address_area);
    }


    $(document).on("click", "#btn_place_order", function () {
        var current_context = $(this);

        var is_valid = true;
        if (is_valid) {

            if (Stripe != undefined) {

                var btn_1_text = current_context.html();
                EnableDisableForm("Disable", $("#body_area"), current_context, btn_1_text);

                stripe.createPaymentMethod(
                        'card',
                        card
                        ).then(function (result) {

                    console.log(JSON.stringify(result) + " sssss >>> " + $("#card_list").val())
                    if (result.error && ($("#card_list").val() == 0 || $("#card_list").val() == "" || $("#card_list").val() == undefined)) {
                        Clear_Storage_Record(credit_card_token_variable_name);
                        var msg_area = $('<div>').attr("id", "cart_error").addClass("msg-box alert alert-danger").html(result.error.message).appendTo($("#credit_card_order_area"));
                        setTimeout(function () {
                            msg_area.remove();
                        }, 2000);
                        is_valid = false;
                        EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                        scrollToItem(msg_area);
                    } else {
                        var payment_method_id = "";
                        if ($("#card_list").length > 0) {

                            if ($("#card_list").val() != "0") {
                                var card_list_array = $("#card_list").val().split("||");
                                payment_method_id = card_list_array[7];
                            } else {
                                payment_method_id = result.paymentMethod.id;
                            }
                        } else {
                            payment_method_id = result.paymentMethod.id;
                            console.log(" else ");
                        }
                        console.log(payment_method_id);

                        is_valid = true;
                        $(".term-error").remove();
                        if ($("#chk_terms").prop("checked") == false) {
                            var error_msg = $("<div>").addClass("alert alert-danger term-error").attr("style", "margin-top:10px;").html("Please agree to our terms and conditions").insertBefore(current_context);
                            setTimeout(function () {
                                error_msg.remove();
                            }, 2000);
                            is_valid = false;
                            EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                        }
                        if (is_valid == true) {
                            if (Get_Storage_Record(session_variable_name) != null) {
                                if (parseFloat($("#cart_grand_total").val()) < parseFloat($("#minimum_order_amount").val())) {
                                    var error_msg = $("<div>").addClass("alert alert-danger term-error").attr("style", "margin-top:10px;").html("Minimum Order Amount " + $("#website_currency").val() + $("#minimum_order_amount").val()).insertBefore(current_context);
                                    setTimeout(function () {
                                        error_msg.remove();
                                    }, 5000);
                                    is_valid = false;
                                    EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                                }
                            }
                        }
                        if (is_valid == true) {
                            var chk_news_var = 0;
                            if ($("#chk_news").prop("checked") == false)
                            {
                                chk_news_var = 0;
                            } else
                            {
                                chk_news_var = 1;
                            }

                            $.ajax({
                                type: "post",
                                url: $("#frm_checkout").attr("action"),
                                cache: false,
                                data: "session_cart=" + Get_Storage_Record(session_variable_name) + "&discount_cart=" + Get_Storage_Record(discount_variable_name) + "&preference_cart=" + Get_Storage_Record(preference_variable_name) + "&address_cart=" + Get_Storage_Record(address_variable_name) + "&regularly_cart=" + Get_Storage_Record(regularly_variable_name) + "&credit_card_cart=" + Get_Storage_Record(credit_card_variable_name) + "&credit_card_token_cart=" + Get_Storage_Record(credit_card_token_variable_name) + "&sub_total=" + $("#cart_sub_total").val() + "&preference_total=" + $("#cart_preference_total").val() + "&discount_total=" + $("#cart_discount_total").val() + "&grand_total=" + $("#cart_grand_total").val() + "&chk_news=" + chk_news_var + "&order_notes=" + $("#order_notes").val() + "&randcheck=" + $("#randcheck").val() + "&payment_method_id=" + payment_method_id,

                                success: function (response) {
                                    var data = response.split("||");
                                    if (data[0] == "success") {
                                        ClearAllCache();
                                        window.location = data[1];
                                    } else {
                                        var error_msg = $("<div>").addClass("alert alert-danger term-error").attr("style", "margin-top:10px;").html(data[1]).insertAfter(current_context);
                                        setTimeout(function () {
                                            error_msg.remove();
                                        }, 3000);
                                        EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                                    }
                                },
                                error: function (data) {
                                    console.log(data);
                                    EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                                }
                            });
                        }
                    }

                });
            } else {
                var msg_area = $('<div>').attr("id", "cart_error").addClass("msg-box alert alert-danger").html("Payment Gateway is currently offline.<br/>Please check back later or contact us via call " + $("#website_telephone_number").val()).appendTo($("#credit_card_order_area"));
                setTimeout(function () {
                    msg_area.remove();
                }, 5000);
                scrollToItem(msg_area);
            }
        } else
        {
            return false;
        }
    });
}
$(document).on("click", "a", function () {
    if ($(this).hasClass("disabled")) {
        return false;
    }
});
$(document).on("click", "span.error", function () {
    $(this).prev().focus();
});
$(document).on("keyup change", ".field-box", function () {
    if ($(this).val() == "" && !$(this).hasClass("blank")) {
        $(this).addClass("error-area");
    } else {
        $(this).removeClass("error-area");
    }
});
$(document).on("click", "body", function (e) {
    // var target = $(e.target);
    // console.log(target);
    //$(".field-box").removeClass("error-area");
});
$(document).on("click", "#btn_show_cart_area", function () {
    $(this).toggleClass("cart-active");
    $("#cart_bar").toggleClass("visible");
    return false;
});
$(document).on("click", ".cancel-order", function () {
    var current_context = $(this);
    current_context.html("Wait ...");
    current_context.parent().find("a").addClass("disabled");
    $.ajax({
        type: "POST",
        url: current_context.attr("href"),
        cache: false,
        async: false,
        data: 'id=' + current_context.attr("data-id"),
        success: function (response) {
            if (response) {
                if (response == "success") {
                    current_context.parents(".order-item").html("<div class='col-sm-12'><p class='red-color'>Invoice Canceled Successfully</p></div>").fadeOut("5000", function () {
                        $(this).remove();
                    });
                } else {
                    current_context.html("Cancel");
                    current_context.parent().find("a").removeClass("disabled");
                }
            }
        },
        error: function (data) {
            console.log(data);
            current_context.html("Cancel");
            current_context.parent().find("a").removeClass("disabled");
        }
    });
    return false;
});
$(document).on("click", ".reorder", function () {
    var current_context = $(this);
    current_context.html("Wait ...");
    current_context.parent().find("a").addClass("disabled");
    $.ajax({
        type: "POST",
        url: current_context.attr("href"),
        cache: false,
        async: false,
        data: 'id=' + current_context.attr("data-id"),
        success: function (response) {
            if (response) {
                if (response == "success") {
                    current_context.parents(".order-item").html("<div class='col-sm-12'><p class='red-color'>Invoice Updated Successfully</p></div>").fadeOut("5000", function () {
                        $(this).remove();
                    });
                } else {
                    current_context.html("Reorder");
                    current_context.parent().find("a").removeClass("disabled");
                }
            }
        },
        error: function (data) {
            console.log(data);
            current_context.html("Reorder");
            current_context.parent().find("a").removeClass("disabled");
        }
    });
    return false;
});
//setInterval(function(){
//    if(localStorage.getItem(session_expire_time_variable_name) != null){
//        var current_date = new Date(localStorage.getItem(session_expire_time_variable_name));
//        var diff = Math.abs(current_date - new Date());
//        var minutes = Math.floor((diff/1000)/60);
//        if(minutes >= 30){
//            Clear_Storage_Record(session_variable_name);
//            Clear_Storage_Record(session_expire_time_variable_name);
//            Clear_Storage_Record(discount_variable_name);
//            Clear_Storage_Record(preference_variable_name);
//            Clear_Storage_Record(pickup_date_time_variable_name);
//            Clear_Storage_Record(delivery_date_time_variable_name);
//            Clear_Storage_Record(address_variable_name);
//            Clear_Storage_Record(credit_card_variable_name);
//            Clear_Storage_Record(credit_card_token_variable_name);
//            Clear_Storage_Record(invoice_edit_load);
//            Update_Cart_Section();
//            Update_Check_Out_Cart_Section();
//        }
//    }
//},2000);
/*
 if ($("#new_template_body").length > 0 || $("#body_area").length > 0) {
 var WebFontConfig = {
 google: {families: ['Source+Sans+Pro:300,400,600,700', 'Oxygen']}
 };
 var wf = document.createElement('script');
 wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
 wf.type = 'text/javascript';
 wf.async = 'true';
 var s = document.getElementsByTagName('script')[0];
 s.parentNode.insertBefore(wf, s);
 }
 */


$(document).ready(function ($) {
    
    setTimeout(function () {
        // $(".invest-model-container").fadeIn()
    }, 3000);

    $(".btn-invest-close").click(function () {
        // $(".invest-model-container").fadeOut()
    })

    if ($(".select2").length > 0) {
        $(".select2").select2({
            minimumResultsForSearch: Infinity
        });
    }
    if ($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip()
    }
    if ($(".area-scroll").length > 0) {
        //$(".area-scroll").niceScroll({ autohidemode: "scroll",cursoropacitymin: 1,background:"#8f886b",cursorcolor:"#635d3b",cursorwidth: "12px",cursorborder :"0px solid #635d3b",cursorborderradius:"5px",horizrailenabled:false,cursordragontouch: true,touchbehavior: true,preventmultitouchscrolling: false});
    }
    if ($('.owl-carousel').length > 0) {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            dots: false,
            //mouseDrag:false,
            //touchDrag:false,
            responsive: {
                0: {
                    items: 3,
                    nav: true
                },
                600: {
                    items: 5,
                    nav: false
                },
                1000: {
                    items: 6,
                    nav: false
                },
                1100: {
                    items: 7,
                    nav: true,
                    loop: false
                }
            }
        })
    }
    if ($('#btn_category_popup_message').length > 0) {
        $('#btn_category_popup_message').magnificPopup({
            type: 'inline',
            mainClass: 'mfp-with-zoom',
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out'
            }
        });
    }
    //ClearAllCache();
    var window_width = $(window).width();
    var window_height = $(window).height();
    if (Current_Page_URL() == store_link_generator_page_url) {
        if (isMobile.Android()) {
            window.location = $("#google_store_link").val();
        } else if (isMobile.iOS()) {
            window.location = $("#apple_store_link").val();
        } else {
            $("#content").html("Your device is not allowed");
            $("#btn_home").show();
        }
    }
    if (Current_Page_URL() == checkout_page_url) {
        if (Get_Storage_Record(address_variable_name) == null) {
            window.location = select_address_page_url;
        }
    }
    if (Current_Page_URL() == select_item_page_url || Current_Page_URL() == select_address_page_url || Current_Page_URL() == checkout_page_url || Current_Page_URL() == 'lovetolaundrydev/checkout') {
        Update_Cart_Section();
        Update_Check_Out_Cart_Section();
    }
    if ($(".mix").length > 0) {
        $(".mix").each(function () {
            var current_item_total = Current_Item_Total($(this).attr("data-id"));
            if (current_item_total == 0) {
                $(this).find(".less").addClass("disabled");
            } else {
                $(this).find(".less").removeClass("disabled");
            }
            if (current_item_total > 99) {
                $(this).find(".in-cart-qty").html("99+");
            } else {
                $(this).find(".in-cart-qty").html(current_item_total);
            }
        });
    }
    if ($(".phone-tel").length > 0) {
        $(".phone-tel").intlTelInput({
            //allowExtensions: true,
            //autoFormat: false,
            //autoHideDialCode: false,
            //autoPlaceholder: false,
            //defaultCountry: "auto",
            // geoIpLookup: function(callback) {
            //   $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            //nationalMode: false,
            //numberType: "MOBILE",
            //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            preferredCountries: [intl_tel],
            utilsScript: web_url + "/assets/js/front/utils.js"
        });
    }
    if ($('#owl-demo').length > 0) {
        var owl = $("#owl-demo");
        owl.owlCarousel({
            items: 10, //10 items above 1000px browser width
            itemsDesktop: [1000, 10], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 8], // betweem 900px and 601px
            itemsTablet: [600, 6], //2 items between 600 and 0
            itemsMobile: [400, 4], // itemsMobile disabled - inherit from itemsTablet option
            pagination: false,
            navigation: true,
            rewindNav: false,
            navigationText: ['<i class="l2l-chevron-left"></i>', '<i class="l2l-chevron-right"></i>'],
            afterAction: function () {
                if (this.itemsAmount > this.visibleItems.length) {
                    $('.next').show();
                    $('.prev').show();

                    $('.next').removeClass('disabled');
                    $('.prev').removeClass('disabled');
                    if (this.currentItem == 0) {
                        $('.prev').addClass('disabled');
                    }
                    if (this.currentItem == this.maximumItem) {
                        $('.next').addClass('disabled');
                    }
                } else {
                    $('.next').hide();
                    $('.prev').hide();
                }
            }
        });
        // Custom Navigation Events
        $(".next").click(function () {
            owl.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        })
        $(".play").click(function () {
            owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
        })
        $(".stop").click(function () {
            owl.trigger('owl.stop');
        });
        $('#owl-demo').show();
    }

    ResizingWithScroll($(window).scrollTop(), false);
    if ($('.only-number').length > 0) {
        $('.only-number').keypress(function (event) {
            if (event.keyCode != 0) {
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.keyCode ? event.which : event.keyCode);
                var enter = (!event.keyCode ? event.which : event.keyCode);
                if ((!regex.test(key)) && (enter != 13)) {
                    event.preventDefault();
                    return false;
                }
            }
        });
    }
    if ($('.not-zero').length > 0) {
        $('.not-zero').keyup(function () {
            var value = $(this).val();
            if (value.indexOf('0') == 0) {
                $(this).val(1);
            }
        });
        $('.not-zero').focusout(function () {
            var value = $(this).val();
            if (value == "") {
                $(this).val(1);
            }
        });
    }
    if ($('#l2l-video').length > 0) {
        $('#l2l-video').magnificPopup({
            type: 'iframe',
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                        '<div class="mfp-close"></div>' +
                        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                        '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

                patterns: {
                    youtube: {
                        index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

                        id: 'v=', // String that splits URL in a two parts, second part should be %id%
                        // Or null - full URL will be returned
                        // Or a function that should return %id%, for example:
                        // id: function(url) { return 'parsed id'; }

                        src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe.
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    },
                    dailymotion: {
                        index: 'dailymotion.com',
                        id: function (url) {
                            var m = url.match(/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
                            if (m !== null) {
                                if (m[4] !== undefined) {

                                    return m[4];
                                }
                                return m[2];
                            }
                            return null;
                        },
                        src: 'http://www.dailymotion.com/embed/video/%id%'
                    }
                    // you may add here more sources
                },
                srcAction: 'iframe_src' // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
            }
        });
    }
    if ($('.flexslider').length > 0) {
        $('.flexslider').flexslider({
            animation: "fade"
        });
    }
    var containerEl = document.querySelector('.order-list-items');
    if ($('.order-list-items').length > 0) {
        var mixer = mixitup(containerEl, {
            load: {
                filter: '.category-0'
            }
        });
    }
    if ($('#btn_password_show').length > 0) {
        $(document).on("click", "#btn_password_show", function () {
            $(this).find(".input-group-text").find("i").toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).parent(".input-group").find("input"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    }
    if ($('#btn_refer_close').length > 0) {

        $(document).on("click", "#btn_refer_close", function () {
            $("#body_area, #new_template_body").removeClass("crowd-fixed");
            $(".crowd-header").hide();
            $(".header").removeAttr("style");
            $(".top-section-container").removeAttr("style");
            return false;
        });
    }
    if ($('.cart-bar').length > 0) {
        $("body").addClass("mar-bt");
    }
    if ($('#captcha_refresh').length > 0) {
        $(document).on("click", "#captcha_refresh", function () {
            var current_context = $(this);
            if (!current_context.hasClass("disabled")) {
                current_context.addClass("disabled fa-spin");
                setTimeout(function () {
                    $("#captcha_image").attr("src", $("#captcha_image").attr("src").split("?")[0] + "?" + new Date().getTime());
                    $("#security_code").val("");
                    current_context.removeClass("fa-spin");
                    current_context.removeClass("disabled");
                }, 2000);
            }
            return false;
        });
    }
    if ($('#btn_refresh_code').length > 0) {
        $(document).on("click", "#btn_refresh_code", function () {
            var current_context = $(this);
            if (!current_context.hasClass("disabled")) {
                current_context.find("i").addClass("l2l-spin");
                current_context.addClass("disabled");
                setTimeout(function () {
                    $("#captcha_image").attr("src", $("#captcha_image").attr("src").split("?")[0] + "?" + new Date().getTime());
                    $("#security_code").val("");
                    current_context.find("i").removeClass("l2l-spin");
                    current_context.removeClass("disabled");
                }, 1000);
            }
            return false;
        });
    }
    // Post Code Search
    if ($("#frm_header_post_code_search").length > 0) {
        $(document).on("click", "#btn_header_post_code_search", function () {
            var current_context = $(this);
            var input_context = $("#frm_header_post_code_search").find("#post_code");
            if (input_context != undefined && input_context != null) {
                if (input_context.val().length == 0) {
                    input_context.addClass("error-area");
                } else {
                    ClearAllCache();
                    var view = getCookie(input_context.val());

                    deleteCookie(view);
                    deleteCookie(input_context.val());
                    AjaxFormSubmit("frm_header_post_code_search", "", "btn_header_post_code_search", current_context.html(), "", "");

                }
            }
        });
        $("#frm_header_post_code_search").submit(function () {
            $("#btn_header_post_code_search").trigger("click");
            return false;
        });
    }
    // Post Code Search
    if ($("#frm_bottom_post_code_search").length > 0) {
        $(document).on("click", "#btn_bottom_post_code_search", function () {
            var current_context = $(this);
            var input_context = $("#frm_bottom_post_code_search").find("#post_code");
            if (input_context != undefined && input_context != null) {
                if (input_context.val().length == 0) {
                    input_context.addClass("error-area");
                } else {
                    ClearAllCache();
                    AjaxFormSubmit("frm_bottom_post_code_search", "", "btn_bottom_post_code_search", current_context.html(), "", "");
                }
            }
        });
        $("#frm_bottom_post_code_search").submit(function () {
            $("#btn_bottom_post_code_search").trigger("click");
            return false;
        });
    }
    // App Link
    if ($("#frm_app_link").length > 0) {
        $("#frm_app_link").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                app_mobile_no: {
                    required: true
                }
            },
            messages: {
                app_mobile_no: {
                    required: "Please enter mobile no"
                }
            },
            errorPlacement: function (error, element) {
                if ($("#new_template_body").length > 0) {
                    error.insertAfter($("#btn_app_request"));
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {

                grecaptcha.ready(function () {
                    grecaptcha.execute(recaptcha_copy_key, {action: 'submit'}).then(function (token) {
                        console.log("token ----->   " + token)
                        $("#_token").val(token);
                        AjaxFormSubmit("frm_app_link", "", "btn_app_request", "", "Yes");
                    });
                });

                //AjaxFormSubmit("frm_app_link", "", "btn_app_request", "", "Yes");
                //return false; // required to block normal submit since you used ajax
            }
        });
    }
    // App Link Inner
    if ($("#frm_app_link_inner").length > 0) {
        $("#frm_app_link_inner").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                app_mobile_no_inner: {
                    required: true
                }
            },
            messages: {
                app_mobile_no_inner: {
                    required: "Please enter mobile no"
                }
            },
            submitHandler: function (form) {

                grecaptcha.ready(function () {
                    grecaptcha.execute(recaptcha_copy_key, {action: 'submit'}).then(function (token) {
                        console.log("token ----->   " + token)
                        $("#_token").val(token);
                        AjaxFormSubmit("frm_app_link_inner", "", "btn_app_request_inner", "", "Yes");
                    });
                });

                //AjaxFormSubmit("frm_app_link_inner", "", "btn_app_request_inner", "", "Yes");
                //return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Subscriber
    if ($("#frm_subscriber").length > 0) {
        $("#frm_subscriber").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                subscriber_email_address: {
                    required: true,
                    email: true
                }
            },
            messages: {
                subscriber_email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_subscriber", "", "btn_subscriber", "", "Yes");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Feedback Form
    if ($("#frm_testimonial").length > 0) {
        $(document).on("click", "#btn_open_form_box", function () {
            $("#testimonial_slide_box").hide();
            $("#testimonial_form_box").fadeIn();
            $("#btn_refresh_code").trigger("click");
            $("#frm_testimonial").show();
            $("#frm_testimonial")[0].reset();
            $("#testimonial_rating").val(1);
            $(".testimonial-rating").find("i").removeClass().addClass("l2l-star-o");
            $($(".testimonial-rating")[0]).find("i").removeClass().addClass("l2l-star");
            return false;
        });
        $(document).on("click", "#btn_close_form_box", function () {
            $("#testimonial_form_box").hide();
            $("#testimonial_slide_box").fadeIn();
            return false;
        });
        $(".testimonial-rating").click(function () {
            $("#testimonial_rating").val($(this).attr("data-value"));
            var star = $(this).attr("data-value");
            $(".testimonial-rating").find("i").each(function () {
                if (star == 0) {
                    $(this).removeClass().addClass("l2l-star-o");
                } else {
                    $(this).removeClass().addClass("l2l-star");
                    star -= 1;
                }
            });
            return false;
        });
        $("#frm_testimonial").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                testimonial_name: {
                    required: true
                },
                testimonial_email_address: {
                    required: true,
                    email: true
                },
                testimonial_title: {
                    required: true
                },
                testimonial_comment: {
                    required: true
                },
                security_code: {
                    required: true
                }
            },
            messages: {
                testimonial_name: {
                    required: "Please enter name"
                },
                testimonial_email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                testimonial_title: {
                    required: "Please enter title"
                },
                testimonial_comment: {
                    required: "Please enter feedback"
                },
                security_code: {
                    required: "Please enter security code"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_testimonial", "msg_box", "btn_testimonial", "", "Yes");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Add Referral Code Form
    if ($("#frm_referral_thank_you_page").length > 0) {
        $("#frm_referral_thank_you_page").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                referral_code: {
                    required: true
                }
            },
            messages: {
                referral_code: {
                    required: "Required"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_referral_thank_you_page", "", "btn_referral_submit", "", "Yes");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Contact Form
    if ($("#frm_contact").length > 0) {
        $("#frm_contact").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true
                },
                message: {
                    required: true
                },
                security_code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                phone_number: {
                    required: "Please enter phone number"
                },
                message: {
                    required: "Please enter message"
                },
                security_code: {
                    required: "Please enter security code"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_contact", "", "btn_contact", "", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Crowd Funding Form
    if ($("#frm_crowd_funding").length > 0) {
        $("#frm_crowd_funding").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                amount: {
                    required: true
                },
                security_code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                amount: {
                    required: "Please select amount"
                },
                security_code: {
                    required: "Please enter security code"
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
                if (element.attr("id") == "security_code") {
                    error.css("right", "15px");
                } else {
                    error.css("right", "60px");
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_crowd_funding", "", "btn_crown_funding_submit", "", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    if ($("#frm_business_enquiry").length > 0) {

        $("#frm_business_enquiry").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true
                },
                nature_of_enquiry: {
                    required: true
                },
                message: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                phone_number: {
                    required: "Please enter phone number"
                },
                nature_of_enquiry: {
                    required: "Please select nature of enquiry"
                },
                message: {
                    required: "Please enter message"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_business_enquiry", "", "btn_business_enquiry", "", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Login
    if ($("#frm_login").length > 0) {
        if ($("#is_order_area").length == 0) {
            $.ajax({
                type: "POST",
                url: $("#frm_login").attr("action"),
                data: 't=login-cookie',
                success: function (data) {
                    var is_found = false;
                    if (data) {
                        if (data != "") {
                            var login_cookie_array = data.split("||");
                            $("#login_email_address").val(login_cookie_array[0]);
                            $("#login_password").val(login_cookie_array[1]);
                            $("#login_remember").prop("checked", "checked");
                        }
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
        $("#frm_login").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                login_email_address: {
                    required: true,
                    email: true
                },
                login_password: {
                    required: true
                }
            },
            messages: {
                login_email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                login_password: {
                    required: "Please enter password"
                }
            },
            submitHandler: function (form) {
                if ($("#is_order_area").length == 0) {
                    AjaxFormSubmit("frm_login", "", "btn_login", "", "");
                } else {
                    AjaxFormSubmit("frm_login", "msg_box_login", "btn_login", "", "Yes");
                }
                return false; // required to block normal submit since you used ajax
            }
        });
        $(document).on("click", "#btn_forgot_show", function () {
            $("#frm_forgot").show();
            $("#login_area").fadeOut();
            $($(this).attr("href")).fadeIn();
            return false;
        });
        $(document).on("click", "#btn_forgot_close", function () {
            $(this).parent().fadeOut();
            return false;
        });
        $("#frm_forgot").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                forgot_email_address: {
                    required: true,
                    email: true
                }
            },
            messages: {
                forgot_email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_forgot", "msg_box", "btn_forgot", "", "Yes");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Register
    if ($("#frm_register").length > 0) {

        function ValidateArea(current_context) {
            var is_valid = true;
            current_context.parents(".reg-area").find(".req").each(function () {

                $("#" + $(this).attr("id") + "_error").remove();
                if ($(this).val() == "") {
                    var add_validation = true;

                    if (add_validation) {
                        is_valid = false;
                        $(this).addClass("error");
                        $('<span>').addClass("error").attr("id", $(this).attr("id") + "_error").html($(this).attr("data-error-message")).insertAfter($(this));
                    }
                }
            });
            //alert(is_valid);
            return is_valid;
        }
        $(document).on("keyup change", ".req", function () {
            if ($(this).val() != "") {
                $(this).removeClass("error");
                if ($(this).next().attr("id") == $(this).attr("id") + "_error") {
                    $(this).next().remove();
                }
                if ($(this).attr("id") == "reg_post_code_result") {
                    $(".p-field").each(function () {
                        if ($(this).next().attr("id") == $(this).attr("id") + "_error") {
                            $(this).next().remove();
                            $(this).removeClass("error");
                        }
                    });
                }
            }
        });

        $(document).on("keyup change", "#register_post_code", function () {

            var post_code = $("#register_post_code").val();
            if (post_code.length >= post_code_length) {

                getAddresses(post_code);
            } else {
                $("#div_addresses").hide();
                $("#addresses").html("");
            }
        });


        $(document).on("click", ".btn-reg-next", function (e) {

            var current_context = $(this);
            var is_valid = ValidateArea(current_context);
            if (is_valid) {
                if (current_context.attr("data-next-tab") == "submit") {

                    grecaptcha.ready(function () {
                        grecaptcha.execute(recaptcha_copy_key, {action: 'submit'}).then(function (token) {
                            console.log("token ----->   " + token)
                            $("#_token").val(token);
                            AjaxFormSubmit("frm_register", "", "btn_register", "", "");
                        });
                    });




                } else {
                    $(".reg-area").hide();
                    $($(".reg-area")[current_context.attr("data-next-tab")]).fadeIn();
                    var count = 0;
                    $("#register_steps").find("li").each(function () {
                        if (count == current_context.attr("data-next-tab")) {
                            $(this).addClass("active");
                        } else {
                            $(this).removeClass("active");
                        }
                        count += 1;
                    });
                }
            }
        });
        $(document).on("click", ".btn-reg-back", function () {
            var current_context = $(this);
            $(".reg-area").hide();
            $($(".reg-area")[current_context.attr("data-back-tab")]).fadeIn();
            var count = 0;
            $("#register_steps").find("li").each(function () {
                if (count == current_context.attr("data-back-tab")) {
                    $(this).addClass("active");
                } else {
                    $(this).removeClass("active");
                }
                count += 1;
            });
        });


    }
    // Account Update
    if ($("#frm_account_setting").length > 0) {
        $("#frm_account_setting").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true
                },
                postal_code: {
                    required: true
                },
                building_name: {
                    required: function () {
                        return ($('#building_name').val() == "" && $('#street_name').val() == "")
                    }
                },
                street_name: {
                    required: function () {
                        return ($('#building_name').val() == "" && $('#street_name').val() == "")
                    }
                },
                town: {
                    required: true
                },
                confirm_password: {
                    equalTo: "#u_password"
                },
                security_code: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                phone_number: {
                    required: "Please enter phone number"
                },
                postal_code: {
                    required: "Please enter post code"
                },
                building_name: {
                    required: "Please enter address"
                },
                street_name: {
                    required: "Please enter address line 2"
                },
                town: {
                    required: "Please enter town"
                },
                confirm_password: {
                    equalTo: "Password and confirm password mismatched"
                },
                security_code: {
                    required: "Please enter security code"
                }
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_account_setting", "msg_box", "btn_profile", "", "");
                return false; // required to block normal submit since you used ajax
            }
        });

        $(document).on("click", "#btn_post_code", function () {

            var postal_code = $("#postal_code").val();
            if (postal_code.length >= post_code_length) {
                getAddresses(postal_code);
            }
        });

        $(document).ready(function () {
            var postal_code = $("#postal_code").val();
            
            if ($("#postal_code").length >= post_code_length) {
                getAddresses(postal_code);
            }
            

        });



    }
    // Wash Preference Update
    if ($("#frm_wash_preference").length > 0) {
        $("#frm_wash_preference").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
            },
            messages: {
            },
            submitHandler: function (form) {
                AjaxFormSubmit("frm_wash_preference", "msg_box", "btn_setting", "", "");
                return false; // required to block normal submit since you used ajax
            }
        });
    }
    // Payment Card Update
    if ($("#card_listing").length > 0) {
        $("#frm_payment_card").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                title: {
                    required: true
                },
                name: {
                    required: true
                },
                card_number: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                security_code_cv: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                expiry_month: {
                    required: true
                },
                expiry_year: {
                    required: true
                }
            },
            messages: {
                title: {
                    required: "Please enter title"
                },
                name: {
                    required: "Please enter name"
                },
                card_number: {
                    required: "Please enter number"
                },
                security_code_cv: {
                    required: "Please enter security code (cv2)"
                },
                expiry_month: {
                    required: "Please select month"
                },
                expiry_year: {
                    required: "Please select year"
                }
            },
            submitHandler: function (form) {
                $("#cart_error").remove();
                var is_valid = true;
                if ($("#c_valid").val() != "") {
                    if ($("#c_valid").val() == "false") {
                        $("#card_number").addClass("error");
                        $("<span>").attr("id", "card_number-error").addClass("error").html("Please enter valid information").insertAfter($("#card_number"));
                        is_valid = false;
                    }
                }
                if (is_valid == true) {
                    var card_number = $("#card_number").val();
                    var security_code = $("#security_code_cv").val();
                    if ($("#c_number").val() != "") {
                        card_number = $("#c_number").val();
                    }
                    if ($("#c_security").val() != "") {
                        security_code = $("#c_security").val();
                    }
                    if (Stripe != undefined) {
                        Stripe.setPublishableKey($("#stripe_key").val());
                        Stripe.createToken({
                            number: card_number,
                            cvc: security_code,
                            exp_month: $("#expiry_month").val(),
                            exp_year: $("#expiry_year").val()
                        }, function (status, response) {
                            if (response.error) {
                                var msg_area = $('<div>').attr("id", "cart_error").addClass("msg-box alert alert-danger").html(response.error.message).appendTo($("#frm_payment_card"));
                                setTimeout(function () {
                                    msg_area.remove();
                                }, 2000);
                            } else {
                                AjaxFormSubmit("frm_payment_card", "msg_box", "btn_payment", "", "");
                            }
                        });
                    } else {
                        var msg_area = $('<div>').attr("id", "cart_error").addClass("msg-box alert alert-danger").html("Payment Gateway is currently offline.<br/>Please check back later or contact us via call " + $("#website_telephone_number").val()).appendTo($("#credit_card_order_area"));
                        setTimeout(function () {
                            msg_area.remove();
                        }, 5000);
                        scrollToItem(msg_area);
                    }
                }
                return false; // required to block normal submit since you used ajax
            }
        });
        function show_form_area(type) {
            $("#card_listing").hide();
            $("span.error").remove();
            $(".error").removeClass("error");
            $("#card_id").val("");
            $("#c_number").val("");
            $("#c_security").val("");
            $("#c_type").val(type);
            $("#c_valid").val("false");
            $("#card_number").attr("placeholder", "Number");
            $("#security_code_cv").attr("placeholder", "Security Code (CV2)");
            $("#card-form").fadeIn();
        }
        $(document).on("click", "#add_card", function () {
            show_form_area("Add");
            return false;
        });
        $(document).on("click", "#btn_back", function () {
            $("#card-form").hide();
            $("#card_listing").fadeIn();
        });
        $(document).on("click", ".edit-card", function () {
            var current_context = $(this);
            $(".loader").remove();
            show_form_area("Update");
            var timer_loader_area = $('<div>').addClass("col-sm-12 text-center loader").html("Loading ").prependTo($("#card_info"));
            $('<i>').addClass("l2l-laundry-setting l2l-spin").appendTo(timer_loader_area);
            $.ajax({
                type: "POST",
                url: $("#frm_payment_card").attr("action"),
                data: 'c_type=Edit&card_id=' + current_context.attr("data-id"),
                success: function (response) {
                    if (response) {
                        var data = JSON.parse(response);
                        if (data.ID) {
                            $("#card_id").val(data.ID);
                            $("#title").val(data.Title);
                            $("#c_number").val(data.Number);
                            $("#card_number").attr("placeholder", data.NumberMasked);
                            $("#expiry_month").val(data.Month);
                            $("#expiry_year").val(data.Year);
                            $("#name").val(data.Name);
                            $("#c_security").val(data.Code);
                            $("#security_code_cv").attr("placeholder", data.CodeMasked);
                            $("#c_valid").val("true");
                        }
                    }
                    timer_loader_area.remove();
                },
                error: function (data) {
                    console.log(data);
                    timer_loader_area.remove();
                }
            });
            return false;
        });
        $(document).on("click", ".remove-card", function () {
            var current_context = $(this);
            var result = confirm('Are you sure you want to delete?');
            if (result == true) {
                $(".loader").remove();
                var timer_loader_area = $('<div>').addClass("col-sm-12 text-center loader").html("Removing ").prependTo($("#card_listing"));
                $('<i>').addClass("l2l-laundry-setting l2l-spin").appendTo(timer_loader_area);
                $.ajax({
                    type: "POST",
                    url: $("#frm_payment_card").attr("action"),
                    data: 'c_type=Remove&card_id=' + current_context.attr("data-id"),
                    success: function (response) {
                        if (response == "success") {
                            current_context.parents(".list-" + current_context.attr("data-id")).fadeOut("slow", function () {
                                $(this).remove();
                                if ($(".listing").length == 0) {
                                    $("#list_area").remove();
                                    var msg_area = $('<div>').addClass("alert alert-danger").appendTo($("#card_listing"));
                                    $('<p>').html("No Card Records").appendTo(msg_area);
                                }
                            });
                        }
                        timer_loader_area.remove();
                    },
                    error: function (data) {
                        console.log(data);
                        timer_loader_area.remove();
                    }
                });
            }
            return false;
        });
    }
    if ($("#frm_cart_member_address").length > 0) {
        $("#frm_cart_member_address").validate({
            errorElement: 'span',
            errorClass: 'error',
            focusInvalid: true,
            ignore: "",
            rules: {
                location: {
                    required: false
                },
                locker: {
                    required: function () {
                        required: false
                    }
                },
                postal_code: {
                    required: function () {
                        return true;

                    }
                },
                addresses: {//// dropdown
                    required: function () {

                        return true;
                    }
                },
                address: {
                    required: function () {

                        if ($("#addresses").val() == "-1") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                street_name: {
                    required: function () {
                        return false;

                    }
                },
                town: {
                    required: function () {

                        return true;

                    }
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true
                },
                u_password: {
                    required: function () {
                        if ($("#u_password").length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                card_list: {
                    required: function () {
                        if ($("#card_list").length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                title: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                name: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                card_number: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                security_code_cv: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                expiry_month: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                expiry_year: {
                    required: function () {
                        if ($("#card_id").val() == "") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                location: {
                    required: "Please select delivery address"
                },
                locker: {
                    required: "Please select locker"
                },
                postal_code: {
                    required: "Please enter post code"
                },
                addresses: {
                    required: "Please choose address"
                },
                street_name: {
                    required: "Please enter street name"
                },
                town: {
                    required: "Please enter town"
                },
                first_name: {
                    required: "Please enter first name"
                },
                last_name: {
                    required: "Please enter last name"
                },
                email_address: {
                    required: "Please enter email address",
                    email: "Please enter valid email address"
                },
                phone_number: {
                    required: "Please enter phone number"
                },
                u_password: {
                    required: "Please enter password"
                },
                title: {
                    required: "Please enter title"
                },
                name: {
                    required: "Please enter name"
                },
                card_number: {
                    required: "Please enter number"
                },
                security_code_cv: {
                    required: "Please enter security code (cv2)"
                },
                expiry_month: {
                    required: "Select month"
                },
                expiry_year: {
                    required: "Select year"
                }
            },
            submitHandler: function (form) {
                return false; // required to block normal submit since you used ajax
            }
        });
        $(document).on("click", "#btn_login_show", function () {
            $("#frm_login").show();
            $("body").addClass("modal-open").attr("style", "padding-right:17px;");
            $($(this).attr("href")).attr("style", "width:" + $(document).width() + "px;").fadeIn();
            return false;
        });
        $(document).on("click", "#btn_login_close", function () {
            $(this).parent().fadeOut();
            $("body").removeClass("modal-open").removeAttr("style");
            return false;
        });


        $(document).ready(function () {
            var postal_code = $("#postal_code").val();
            
            if($("#postal_code").length>0){
                getAddresses(postal_code);
            }
            
            FetchTimeCollection();

        });

        $(document).on("keyup change", "#postal_code", function () {

            var post_code = $("#postal_code").val();
            if (post_code.length >= post_code_length) {
                getAddresses(post_code);
                FetchTimeCollection();
            } else {
                $("#div_addresses").hide();
                $("#addresses").html("");
            }
        });



    }
    $('.review-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title') // Extract info from data-* attributes
        var comment = button.data('comment') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use $ here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text(title);
        modal.find('.modal-body').text(comment);
    });
});
$(window).on("resize", function () {
    ResizingWithScroll($(window).scrollTop(), false);
});


$(window).on("scroll", function (e) {
    loadMoreOrders();
});

$(document).on("click", ".more_order_button", function () {

    loadMoreOrders()
    return false;
});

function loadMoreOrders() {

    ResizingWithScroll($(window).scrollTop(), true);
    if ($("#more_order_listing").length > 0) {

        if ($("#more_order_listing").val() == "yes") {

            //if ($(window).scrollTop() >= $(document).height() - $(window).height() - 20) {
            //alert("scrollTop");

            if ($("#is_load_order_wait").val() == "no") {
                $("#is_load_order_wait").val("yes");
                $("#order_loader").show();
                var order_current_type = $("#order_current_type").val();
                var order_listing_url = web_url + "orders/listing" + "?q=" + order_current_type + "&p=" + $("#page_no").val();
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: order_listing_url,
                        cache: false,
                        async: false,
                        success: function (response) {
                            if (response) {
                                var data = JSON.parse(response);
                                $("#more_order_listing").val(data.has_more);
                                //if (data.order_records.length > 0) {
                                //alert(data.list);
                                $("#order_listing_area").append(data.list);
                                //}
                                $("#page_no").val(parseInt($("#page_no").val()) + 1);
                            }
                            $("#order_loader").hide();
                            $("#is_load_order_wait").val("no");
                        },
                        error: function (data) {
                            console.log(data);
                            $("#order_loader").hide();
                        }
                    });
                }, 2000);
            }
            //   }
        }
    }
}

$(window).on("load", function () {
    if ($("#map_area_canvas").length > 0) {
        initialize();
    }
});


// Google Maps API
function initialize() {
    var latlng = new google.maps.LatLng($("#map_area_canvas").attr("data-latitude"), $("#map_area_canvas").attr("data-longitude"));
    var settings = {
        zoom: 12,
        center: latlng,
        scrollwheel: false,
        navigationControl: false,
        scaleControl: false,
        streetViewControl: false,
        draggable: true,
        mapTypeControl: true,
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        navigationControl: true,
        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map;
    // a new Info Window is created
    var infoWindow = new google.maps.InfoWindow();
    var point = [];
    $(".area-item").each(function () {
        var item = [$(this).attr("data-latitude"), $(this).attr("data-longitude"), $(this).find("span").html()];
        point.push(item);
    })
    var image = new google.maps.MarkerImage(
            'https://www.love2laundry.com/uploads/images/mapmarker.png',
            new google.maps.Size(60, 60),
            new google.maps.Point(0, 0),
            new google.maps.Point(21, 62)
            );
    map = new google.maps.Map(document.getElementById("map_area_canvas"), settings);
    for (i = 0; i < point.length; i++) {
        var position = new google.maps.LatLng(point[i][0], point[i][1]);
        var name = point[i][2];
        createMarker(position, name);
    }
    var red_road_styles = [
        {
            featureType: "all",
            stylers: [
                {saturation: -100}
            ]
        },
        {
            featureType: "road.highway",
            stylers: [
                {hue: "#F5F381"},
                {saturation: 86}
            ]
        }
    ];
    map.setOptions({styles: red_road_styles});
    function createMarker(position, name) {
        var marker = new google.maps.Marker({
            draggable: false,
            raiseOnDrag: false,
            icon: image,
            map: map,
            position: position,
            animation: google.maps.Animation.BOUNCE
        });
        google.maps.event.addListener(marker, 'click', function () {
            var iwContent = '<div id="iw_container">' +
                    '<div class="iw_title">' + name + '</div></div>';

            infoWindow.setContent(iwContent);
            infoWindow.open(map, marker);
        });
    }
}

$(document).on("click", "#btn_payment", function () {

    $("#card-errors").hide();
    stripe.createPaymentMethod('card', card
            ).then(function (result) {

        if (result.error) {
            var message = result.error.message;
            $("#card-errors").show().html('<div class="alert alert-danger" role="alert">' + message + '</div>');


        } else {


            $.ajax({
                type: "post",
                url: $("#frm_payment_card").attr("action"),
                cache: false,
                data: "c_type=Add&payment_method_id=" + result.paymentMethod.id,

                success: function (response) {
                    console.log(response);
                    var data = response.split("||");
                    if (data[0] == "success") {
                        ClearAllCache();
                        window.location = data[3];
                    } else {
                        $("#card-errors").show().html('<div class="alert alert-danger" role="alert">' + data[2] + '</div>');
                    }
                },
                error: function (data) {
                    // alert("Some went wrong.");
                    console.log(data);
                    //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
                }
            });
        }
    });




});

if ($('.select-dropdown').length > 0) {
    $(document).on("click", ".select-dropdown", function () {

        var id = $(this).attr("id");

        //$("#" + id).removeClass("is-active");



        //$('.select-dropdown').removeClass("is-active");
        //$(this).removeClass("is-active");
        //alert($("#" + id).hasClass('is-active'))
        if (!$("#" + id).hasClass('is-active')) {

            $("#" + id).addClass("is-active");
        }
        $("#" + id).children(".dropdown-body").toggle();
        //return false;
    });
}

$(document).on("click", ".time-slot", function () {

    var time = $(this).data("time");
    var parent = $(this).parent(".dropdown-body").parent("div");
    var id = parent.attr("id");

    //$(this).parent(".dropdown-body").toggle();
    id = id.replace("_div", "");
    $("#" + id).val(time);
    parent.children(".selected").html("<i class='fa fa-leaf' aria-hidden='true'></i> " + time);



    if (id == "pickup_time") {
        getDeliveryDate();
    }
});



var getPickupDates = function () {

    $("#btn_cart_address_page_continue").addClass("disabled");

    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_pickup_date_records/" + franchise_id,
        cache: false,
        success: function (response) {
            console.log(response.pick);
            var html = "";
            var day = "";
            var disabled = "disabled";

            $.each(response.pick, function (i, item) {

                day = item.Date_Class;

                disabled = "";
                if (day == "Available") {

                    html += "<option value='" + item.Date_Number + "' " + disabled + " >" + item.Date_List + "</option>";
                }


            });
            $("#time_loader").hide();
            $("#time_select_area").show();
            $("#pickup_date").html(html);
            getPickupTimes()
        },
        error: function (data) {
            console.log(data);

        }
    });
}

var getPickupTimes = function () {

    var pickup_date = $("#pickup_date").val();
    $("#btn_cart_address_page_continue").addClass("disabled");


    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_time_records/?id=" + franchise_id + "&date=" + pickup_date + "&type=Pickup&device_id=1234",
        cache: false,
        success: function (response) {

            var html = "";
            var day = "";
            var disabled = "";



            var eco_friendly = 0;
            var eco_friendly_html = '<span class="time-options eco-friendly">Eco-friendly routes</span>';
            var other_options = 0;
            var other_options_html = '<span class="time-options other-options">Other time options</span>';
            var j = 0;
            $("#pickup_time_div").children(".dropdown-body").html("");
            $("#pickup_time").val("");
            $("#pickup_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>');
            $.each(response.data, function (i, item) {
                day = item.Class;

                if (day == "Available") {

                    var hour = item.Hour;

                    if (j == 0) {
                        $("#pickup_time_div").children(".selected").append(item.Time);
                        $("#pickup_time").val(item.Time);
                        j++;
                    }

                    if (hour <= 20) {

                        eco_friendly_html += "<span class='time-slot' data-time='" + item.Time + "'><i class='fa fa-leaf' aria-hidden='true'></i>" + item.Time + "</span>";

                        eco_friendly++;

                    } else if (hour > 20) {
                        other_options_html += "<span class='time-slot' data-time='" + item.Time + "'>" + item.Time + "</span>";
                        other_options++;
                    }

                }
            });
            if (eco_friendly == 0) {
                eco_friendly_html = "";
            }

            if (other_options == 0) {
                other_options_html = "";
            }

            html = eco_friendly_html + other_options_html;
            $("#pickup_time_div").children(".dropdown-body").html(html);

            $("#time_loader").hide();
            //$(".time-values").html("");
            $("#time_select_area").show();
            //$("#pickup_time").html(html);

            var invoice_type = $("#invoice_type").val();
            var editdate = $("#editdate").val();

            if (invoice_type == "edit" && editdate == true) {

            }
            getDeliveryDate()
        },
        error: function (data) {
            // alert("Some went wrong.");
            console.log(data);
            //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
        }
    });
}


var getDeliveryDate = function () {


    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();

    var times = pickup_time.split("-")
    times = times[1].split(":")
    var pickup_time_hour = times[0];
    $("#btn_cart_address_page_continue").addClass("disabled");

    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_delivery_date_records/" + franchise_id + "?date=" + pickup_date + "&pick_date=" + pickup_date + "&pick_time_hour=" + pickup_time_hour + "&",
        cache: false,
        success: function (response) {
            console.log(response.delivery);
            var html = "";
            var day = "";
            var disabled = "disabled";

            $.each(response.delivery, function (i, item) {

                day = item.Date_Class;
                disabled = "";
                if (day == "Available") {
                    html += "<option value='" + item.Date_Number + "' " + disabled + " >" + item.Date_List + "</option>";
                }
            });
            $("#time_loader").hide();
            $("#time_select_area").show();
            $("#delivery_date").html(html);
            getDeliveryTimes()
        },
        error: function (data) {
            // alert("Some went wrong.");
            console.log(data);
            //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
        }
    });
}

var getDeliveryTimes = function () {
    var delivery_date = $("#delivery_date").val();
    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();
    var times = pickup_time.split("-");
    times = times[1].split(":");
    var pickup_time_hour = times[0].replace(/^0+/, '');

    $("#btn_cart_address_page_continue").addClass("disabled");

    //delivery_date= "2020-05-29";

    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_time_records?id=" + franchise_id + "&date=" + delivery_date + "&pick_date=" + pickup_date + "&pick_time_hour=" + pickup_time_hour + "&device_id=1234&type=Delivery",
        cache: false,
        success: function (response) {

            if (response.data.length > 0) {

                var html = "";
                var day = "";

                var eco_friendly = 0;
                var eco_friendly_html = '<span class="time-options eco-friendly">Eco-friendly routes</span>';
                var other_options = 0;
                var other_options_html = '<span class="time-options other-options">Other time options</span>';
                var j = 0;

                $("#delivery_time_div").children(".dropdown-body").html("");
                $("#delivery_time").val("");
                $("#delivery_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>');

                $.each(response.data, function (i, item) {

                    day = item.Class;
                    if (day == "Available") {

                        var hour = item.Hour;

                        if (j == 0) {
                            $("#delivery_time_div").children(".selected").append(item.Time);
                            $("#delivery_time").val(item.Time);
                            j++;
                        }

                        if (hour <= 20) {

                            eco_friendly_html += "<span class='time-slot' data-time='" + item.Time + "'><i class='fa fa-leaf' aria-hidden='true'></i>" + item.Time + "</span>";

                            eco_friendly++;

                        } else if (hour > 20) {
                            other_options_html += "<span class='time-slot' data-time='" + item.Time + "'>" + item.Time + "</span>";
                            other_options++;
                        }

                    }

                });

                if (eco_friendly == 0) {
                    eco_friendly_html = "";
                }

                if (other_options == 0) {
                    other_options_html = "";
                }

                html = eco_friendly_html + other_options_html;
                $("#delivery_time_div").children(".dropdown-body").html(html);

                $("#time_loader").hide();
                $("#time_select_area").show();

                var invoice_type = $("#invoice_type").val();
                var editdate = $("#editdate").val();
                $("#btn_cart_address_page_continue").removeClass("disabled");
                $("#continue_text").text("Continue");

                if (invoice_type == "edit" && editdate == true) {
                    $("#editdate").val(false);


                    $("#delivery_date").val($("#ddate").val()).select2();
                    $("#pickup_date").val($("#pdate").val()).select2();


                    $("#delivery_time").val($("#dtime").val());
                    $("#pickup_time").val($("#ptime").val());


                    $("#pickup_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>' + $("#ptime").val());
                    $("#delivery_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>' + $("#dtime").val());


                }

            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

$(document).on("change", "#addresses", function () {
    var address = $("#addresses").val();

    if (address == -1) {
        $("#div_reg_address1").show();
    } else {
        $("#div_reg_address1").hide();
        $("#reg_address2").val("");
    }

});

var notifyOrder = function () {
    $.ajax({
        type: "post",
        dataType: "json",
        url: web_url + "notify-new-order",
        cache: false,
        success: function (response) {
            console.log(response)
        },
        error: function (data) {
            // alert("Some went wrong.");
            console.log(data);
            //EnableDisableForm("Enable", $("#body_area"), current_context, btn_1_text);
        }
    });
}

var openModal = function (title, message) {

    $("#modal_title").html(title);
    $("#modal_message").html(message);
    $("#myModal").modal();
}

$(document).on("click", ".logout", function () {

    googleLogout();
    facebookLogout();
    var url = web_url + "logout";
    window.location = url;
});