$(document).ready(function () {


    if ($("#addresses").length > 0) {
        if ($(".select2").length > 0) {
            $(".select2").select2({
                minimumResultsForSearch: Infinity
            });
        }
        var postal_code = $("#postal_code").val();

        if (postal_code != "") {
            getFranchise(postal_code);
            getAddresses(postal_code);

        }
        //getPickupDates();
    } else {
        var franchise_id = $("#franchise_id").val();
        getTimings(franchise_id)
    }
});

$("#frm_phone_setting").submit(function (event) {

    event.preventDefault();
    $(".loader").show();

    var data = $("#frm_phone_setting").serialize();
    var type = "POST";
    var dataType = "json";
    var url = site_url + "schedule/updatephone";

    var callBack = function (response) {
        $(".loader").hide();

        window.location = site_url + "schedule/order";

        //getTimings(response.franchise_id);
        //$("#franchise_id").val(response.franchise_id);
        return false;
    };

    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();


        //$("#franchise_id").val("");
        //$(".step2").addClass("d-none");
        displayErrors(jqXHR.responseJSON, "msg_box");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);


});

$(document).on("keyup change", "#postal_code", function (e) {

    //e.preventDefault();
    if (e.keyCode === 13) {
        return false;
    }



    var postal_code = $("#postal_code").val();

    if (postal_code.length >= 4) {
        getAddresses(postal_code);
        getFranchise(postal_code);
    }

    $("#google_addresses").show();
});


$(document).on("change", "#addresses", function () {
    var address = $("#addresses").val();

    if (address == -1) {
        $("#div_reg_address1").show();
    } else {
        $("#div_reg_address1").hide();
        $("#reg_address2").val("");
    }
});

$(document).on("click", ".payment-cards-a", function () {

    var hide = $(this).attr("data-hide");
    var show = $(this).attr("data-show");
    $("#" + hide).hide("d-none");
    $("#" + show).show("d-none");
});


var getFranchise = function (postal_code) {

    $(".loader").show();

    var data = {postcode: postal_code};
    var type = "GET";
    var dataType = "json";
    var url = site_url + "api/validatepostcode";

    var callBack = function (response) {
        $(".loader").hide();
        getTimings(response.franchise_id);
        $("#franchise_id").val(response.franchise_id);
        return false;
    };

    var errorCallBack = function (jqXHR, error, errorThrown) {
        $("#franchise_id").val("");
        $(".step2").addClass("d-none");
        displayErrors(jqXHR.responseJSON, "step1_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

var getTimings = function (franchise_id) {

    var pickup_date = $("#pickup_date").val();
    $("#btn_cart_address_page_continue").addClass("disabled");
    $.ajax({
        type: "post",
        dataType: "json",
        url: site_url + "api/get_time_records/?id=" + franchise_id + "&date=" + pickup_date + "&type=Pickup&device_id=1234",
        cache: false,
        success: function (response) {

            var html = "";
            var day = "";
            var disabled = "";
            var eco_friendly = 0;
            var eco_friendly_html = '<span class="time-options eco-friendly">Eco-friendly routes</span>';
            var other_options = 0;
            var other_options_html = '<span class="time-options other-options">Other time options</span>';
            var j = 0;
            $("#pickup_time_div").children(".dropdown-body").html("");
            $("#pickup_time").val("");
            $("#pickup_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>');
            $.each(response.data, function (i, item) {
                day = item.Class;

                if (day == "Available") {

                    var hour = item.Hour;

                    if (j == 0) {
                        $("#pickup_time_div").children(".selected").append(item.Time);
                        $("#pickup_time").val(item.Time);
                        j++;
                    }

                    if (hour <= 20) {

                        eco_friendly_html += "<span class='time-slot' data-time='" + item.Time + "'><i class='fa fa-leaf' aria-hidden='true'></i>" + item.Time + "</span>";

                        eco_friendly++;

                    } else if (hour > 20) {
                        other_options_html += "<span class='time-slot' data-time='" + item.Time + "'>" + item.Time + "</span>";
                        other_options++;
                    }

                }
            });
            if (eco_friendly == 0) {
                eco_friendly_html = "";
            }

            if (other_options == 0) {
                other_options_html = "";
            }

            html = eco_friendly_html + other_options_html;


            $("#pickup_time_div").children(".dropdown-body").html(html);

            var pickup_time = $("#pickup_time").val();
            $(".step2").removeClass("d-none");
            $("#collection_pickup_time").html(pickup_time);
            /* setTimeout(function () {
             
             }, 2000);*/

        },
        error: function (data) {

            console.log(data);
            //EnableDisableForm("Enable", jQuery("#body_area"), current_context, btn_1_text);
        }
    });
}


var confirmSchedule = function (payment_id, payment_type) {
    $(".loader").show();
    $("#pickup_date").val();
    $('#payment_errors').html();
    var data = $("#checkout-form").serialize();

    var type = "POST";
    var dataType = "json";
    var url = site_url + "schedule/confirmorder";
    var callBack = function (response) {
        $(".loader").hide();
        var postal_code = $("#postal_code").val();

        console.log(response);
        if (response.error == false) {
            window.location = site_url + "schedules";
        }
        return false;
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {

        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "payment_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}





if ($('.select-dropdown').length > 0) {
    $(document).on("click", ".select-dropdown", function () {

        var id = $(this).attr("id");
        if (!$("#" + id).hasClass('is-active')) {
            $("#" + id).addClass("is-active");
        }
        $("#" + id).children(".dropdown-body").toggle();
    });
}



if ($('.remove-schedule').length > 0) {

    $(document).on("click", ".remove-schedule", function () {
        var id = $(this).data("id");
        $("#schedule_id").val(id);
        $('#delete-schedule').modal('show');
    });
    
    
    $(document).on("click", ".confirm-delete-schedule", function () {
        var id = $("#schedule_id").val();
        confirmDeleteSchedule(id);
    });


    var confirmDeleteSchedule = function (id) {
        $(".loader").show();
        var data = {id: id}
        var type = "POST";
        var dataType = "json";
        var url = site_url + "schedule/confirmdeleteschedule";
        var callBack = function (response) {
            $(".loader").hide();

            if (response.error == false) {
                window.location = site_url + "schedules";
            }
            return false;
        };

        var errorCallBack = function (jqXHR, error, errorThrown) {

            $(".loader").hide();
            displayErrors(jqXHR.responseJSON, "payment_errors");
        };
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }
}



$(document).on("click", ".time-slot", function () {

    var time = $(this).data("time");
    var parent = $(this).parent(".dropdown-body").parent("div");
    var id = parent.attr("id");
    id = id.replace("_div", "");
    $("#" + id).val(time);
    parent.children(".selected").html("<i class='fa fa-leaf' aria-hidden='true'></i> " + time);
    $("#collection_" + id).html(time);

});