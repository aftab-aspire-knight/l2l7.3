function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().

    if (response.status === 'connected') {   // Logged into your webpage and Facebook.
        fetchFBInfo();
    } else {


        FB.login(function (response) {

            if (response.authResponse) {
                console.log('Welcome!  Fetching your information.... ');
                //console.log(response); // dump complete info
                access_token = response.authResponse.accessToken; //get access token
                user_id = response.authResponse.userID; //get FB UID

                statusChangeCallback(response);

            } else {
                //user hit cancel button
                console.log('User cancelled login or did not fully authorize.');

            }
        }, {
            scope: 'public_profile,email'
        });



// Not logged into your webpage or we are unable to tell.
        // document.getElementById('status').innerHTML = 'Please log into this webpage.';
    }
}

function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function (response) {   // See the onlogin handler
        statusChangeCallback(response);
    });
}

function fetchFBInfo() {                    // Testing Graph API after login.  See statusChangeCallback() for when this call is made.

    FB.api('/me?fields=id,first_name,last_name,email,permissions,locale', function (response) {


        socialLogin(response.id, response.first_name, response.last_name, response.email, "facebook")
    });
}

function facebookLogout()
{
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            FB.logout(function (response) {});
        }
    });
}
