/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var web_url = site_url;
var api_url = web_url + "apiv7/";
var address_url = "";
var optionHtml = "";
var postal_code = "";
var franchise_id = "1";
$(document).ready(function () {
    $("#postalcodeModal").modal();
    $("#id").val(franchise_id);
    $("#postal_code").val(postal_code);
    showPrices(franchise_id);
    if ($('.owl-prices-category').length > 0) {
        owlCarousel();
    }
});


var showPrices = function (franchise_id) {

    var data = "";
    var type = "GET";
    var dataType = "html";
    var url = web_url + "showprices?franchise_id=" + franchise_id;
    var callBack = function (response) {
        $(".order-services").html(response);
        owlCarousel();
        $('#postalcodeModal').modal("hide")
    };
    var errorCallBack = function () {
        // $(".loader").hide();

    };
    if (franchise_id) {
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }
}



$(document).on("click", ".apply-postal-code", function () {
    $(".loader").show();
    var code = $("#code").val();

    var url = web_url + "booking/validatepostcode";
    var data = {postcode: code};
    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {
        $(".loader").hide();
        postal_code = code;
        franchise_id = response.franchise_id;
        $("#id").val(response.franchise_id);
        $(".minimum-order-amount").text(response.minimum_order_amount);
        $("#postal_code").val(postal_code);
        showPrices(franchise_id);
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "postal_code_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);


});
var total = 0;
$(document).on("click", ".action-item", function () {

    $(".action-item").attr("disabled", true);
    var current_context = $(this);
    var preference = current_context.attr("data-preference-show");
    var type = current_context.attr("data-type");
    var price = current_context.attr("data-price");
    var title = current_context.attr("data-title");
    var id = current_context.attr("data-id");
    var category = current_context.attr("data-category");
    var parent_area = current_context.parents(".mix");
    var category_id = current_context.attr("data-category-id");
    var package = current_context.attr("data-package");
    var desktop_image = current_context.attr("data-desktop-image");
    var mobile_image = current_context.attr("data-mobile-image");
    var popup_message = current_context.attr("data-popup-message");
    var html = "";
    var qty = 0;
    //var price = 0;
    var data = 'data-id="' + id + '" data-title="' + title + '" data-price="' + price + '" data-desktop-image="' + desktop_image + '" data-mobile-image="' + mobile_image + '" data-category-id="' + category_id + '"  data-category="' + title + '" data-package="' + package + '" data-preference-show="' + preference + '" data-popup-message="' + popup_message + '"';

    if (type == "add") {
        total += parseFloat(price);
        if ($(".cart-service-" + id).length == 0) {
            html = '<div class="form-row mx-0 py-2 cart-service-' + id + '" ><div class="col-6 cart-name">' + title + '</div><div class="col-4 cart-qty"><button type="button" class="plus add-item-cart action-item rounded-pill" ' + data + ' data-type="add">+</button><span class="mx-2 cart-qty-service-' + id + '">1</span><button class="minus remove-item-cart action-item rounded-pill disabled" ' + data + ' type="button" data-type="remove">-</button></div><div class="col-2 cart-price cart-price-service-' + id + '">' + currency_symbol + '' + price + '</div><input class="input_' + id + '" type="hidden" name="services[' + id + '][qty]" value=1 ><input type="hidden" name="services[' + id + '][category]" value="' + category + '" ><input type="hidden" name="services[' + id + '][category_id]" value="' + category_id + '" ><input type="hidden" name="services[' + id + '][title]" value="' + title + '" ><input type="hidden" name="services[' + id + '][package]" value="' + package + '" ><input type="hidden" name="services[' + id + '][desktop_image]" value="' + desktop_image + '" ><input type="hidden" name="services[' + id + '][mobile_image]" value="' + mobile_image + '" ><input type="hidden" name="services[' + id + '][preferences]" value="' + preference + '" ></div>';
            $("#cart_service_right").append(html);
            $("#qty" + id).text("1");
        } else {

            qty = $(".cart-qty-service-" + id).text();
            qty = parseInt(qty);
            qty++;
            $(".input_" + id).val(qty);
            price = price * qty;
            $(".cart-qty-service-" + id).text(qty);
            $("#qty" + id).text(qty);
            $(".cart-price-service-" + id).text(currency_symbol + parseFloat(price).toFixed(2));
        }
        $(".price-estimate-amount").text(currency_symbol + parseFloat(total).toFixed(2));

    } else if (type == "remove") {
        total -= parseFloat(price);
        qty = $(".cart-qty-service-" + id).text();
        qty = parseInt(qty);
        qty--;

        if (qty == 0) {
            $(".cart-service-" + id).remove();
            $("#qty" + id).text("0");
        } else {
            price = price * qty;
            $(".cart-qty-service-" + id).text(qty);
            $("#qty" + id).text(qty);
            $(".cart-price-service-" + id).text(currency_symbol + parseFloat(price).toFixed(2));
        }
        $(".price-estimate-amount").text(currency_symbol + parseFloat(total).toFixed(2));


    }
    $("#amount").val(total);



    $(".action-item").attr("disabled", false);
});


$(document).on("click", ".continue-order", function () {
    var data = $("#form-services").serialize();
    var url = web_url + "prices/continueorder";
    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {

        var postal_code = $("#postal_code").val();
        setCookie(postal_code, "order-address");
        window.location = web_url + "booking";
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        displayErrors(jqXHR.responseJSON, "items_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);


});


function owlCarousel() {
    $('.owl-prices-category').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        //mouseDrag:false,
        //touchDrag:false,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            768: {
                items: 5,
                nav: false
            },
            1100: {
                items: 6,
                nav: false
            },
            1200: {
                items: 7,
                nav: true,
                loop: false
            }
        }
    });

    var containerEl = document.querySelector('.order-list-items');
    if ($('.order-list-items').length > 0) {
        var mixer = mixitup(containerEl, {
            load: {
                filter: '.category-0'
            }
        });
    }
}
$(function () {
    if ($(window).width() <= 991) {
        $(document).on('click', '.price-mobile', function () {
            $('.price-estimate-container').addClass("is-active");
            $('.price-estimate-container').modal('show');
        });
    }
    $('[data-toggle="tooltip"]').tooltip();
})
$(window).on("scroll", function () {
    var scrollTop = $(window).scrollTop();
    if (scrollTop > $("#category-offset").offset().top) {
        $(".sidebar").addClass("sticky shadow-sm");
    } else {
        $(".sidebar").removeClass("sticky shadow-sm");
    }
});