var web_url = site_url;
var api_url = web_url + "api/";
var franchise_id = "";
var has_preferences = "";


var saveCollection = function () {
    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();
    var delivery_date = $("#delivery_date").val();
    var delivery_time = $("#delivery_time").val();
    var id = $("#id").val();
    $(".loader").show();
    var data = {pickup_date: pickup_date, pickup_time: pickup_time, delivery_date: delivery_date, delivery_time: delivery_time, id: id}
    var type = "POST";
    var dataType = "json";
    var url = web_url + "orders/savetimings";
    var callBack = function (response) {
        $(".collection").removeClass("d-none");
        $(".order-summary-edit").children(".fa-edit").removeClass("d-none");
        $("#modalMessage").modal();
        $("#modal_description").html(response.message);
        $(".loader").hide();
        notify();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "collection_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}




var getServices = function (franchise_id, invoice_id) {


    var data = "";
    var type = "GET";
    var dataType = "html";
    var url = web_url + "edit-order-items?fid=" + franchise_id + "&invoice_id=" + invoice_id;
    var callBack = function (response) {
        //  $(".loader").hide();
        $(".action-item").attr("disabled", false);
        var order_select_items = $(".order-select-items");
        order_select_items.html(response);
        order_select_items.removeClass("d-none");
        owlCarousel();
    };
    var errorCallBack = function () {
        // $(".loader").hide();
    };
    if (franchise_id) {
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }
}

$(document).on("click", ".action-item", function () {

// $(".action-item").attr("disabled", true);
    var current_context = $(this);
    var preference = current_context.attr("data-preference-show");
    var type = current_context.attr("data-type");
    var price = current_context.attr("data-price");
    var title = current_context.attr("data-title");
    var id = current_context.attr("data-id");
    var category = current_context.attr("data-category");
    var parent_area = current_context.parents(".mix");
    var category_id = current_context.attr("data-category-id");
    var qty_area = parent_area.find(".in-cart-qty");
    var qty = parseInt(qty_area.text());
    $(".loader").hide();
    $(".action-item").attr("disabled", false);
    //var type = current_context.attr("data-type");
    parent_area.find(".less").removeClass("disabled");
    qty_area.removeClass("blink");
    qty_area.addClass("blink");
    category_id = parseInt(category_id);
    var package = current_context.attr("data-package");
    var desktop_image = current_context.attr("data-desktop-image");
    var mobile_image = current_context.attr("data-mobile-image");
    var service_id = current_context.attr("data-id");
    var invoice_id = $("#id").val();
    var q = "franchise_id=" + franchise_id + "&invoice_id=" + invoice_id + "&service_id=" + service_id + "&title=" + title + "&price=" + price;
    q = q + "&category_id=" + category_id + "&qty=" + qty + "&category=" + category;
    q = q + "&package=" + package + "&preference=" + preference + "&type=" + type;
    q = q + "&desktop_image=" + desktop_image + "&mobile_image=" + mobile_image;
    var data = "";
    var type = "GET";
    var dataType = "json";
    var url = web_url + "orders/addremoveitems?" + q;
    var callBack = function (response) {
        $(".loader").hide();
        $("#qty" + response.service_id).html(response.quantity);

        services = response.services;

        var html = "";
        if (response.total_services > 0) {
            $.each(services, function (key, service) {
                html += '<div class="form-row mx-0 cart-service-' + key + '"><div class="col-7 cart-name">' + service.title + '</div><div class="col-1 cart-qty cart-qty-service-' + key + '">' + service.quantity + '</div><div class="col-4 cart-price cart-price-service-' + key + '">' + response.currency + service.total + '</div></div>';
            });
            $(".mini-cart-right ").html(html);
            $("#item_total_price").html(response.grand_total);
            $("#item_discount_price").html(response.discount_amount);
            $("#item_services_price").html(response.services_total);
        } else {
            $(".mini-cart-right ").html('<div class="mini-cart-right">No service added yet</div>');
        }
    };
    var errorCallBack = function () {
        $(".loader").hide();
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    return false;
});



$(document).on("click", ".items-cancel", function () {


    if (!$('.order-select-preferences').is(':visible')) {
        var referrer = $("#referrer").val();
        window.location = referrer;
    } else {
        $(".order-select-preferences").addClass("d-none");
        $(".order-select-items").removeClass("d-none");
    }
    return false;
});

$(document).on("click", ".save-items", function () {
    var found = 0;

    if (!$('.order-select-preferences').is(':visible')) {

        $.each(services, function (index, value) {
            if (value.preference == "Yes") {
                found = 1;
            }
        });

        if (found == 1) {
            showPreferences();
        } else {
            updateInvoice();
        }
    } else {
        updateInvoice();
    }

});


$(document).on("change", ".preferences-list", function () {
    storePreferences();
});


var storePreferences = function () {
    var data = $("#edit-invoice").serialize();
    //var data = $('#checkout-form').find('input[name^="preferences_list"]').serialize();

    var type = "POST";
    var dataType = "json";
    var url = web_url + "orders/storepreferences";
    var callBack = function (response) {

        preferences_total = response.total;

        if (preferences_total > 0) {
            $(".order-preferences-right").show();
        }
        $("#item_preferences_price").html(preferences_total);
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {};
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}





var showPreferences = function ()
{
    $(".order-select-preferences").removeClass("d-none");
    $(".order-select-items").addClass("d-none");
}

var updateInvoice = function ()
{
    $(".loader").show();
    $(".update_errors").html("").hide();

    $("a").attr("disabled", true);
    $("button").attr("disabled", true);
    var data = $("#edit-invoice").serialize();
    $(".loader").show();
    var type = "POST";
    var dataType = "json";
    var url = web_url + "orders/update";
    var callBack = function (response) {
        $(".loader").hide();

        $("a").attr("disabled", false);
        $("button").attr("disabled", false);
        notify();


        $("#modalMessage").modal();
        $("#modal_description").html(response.message);
        $(".loader").hide();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        $("a").attr("disabled", false);
        $("button").attr("disabled", false);

        displayErrors(jqXHR.responseJSON, "update_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}


$(document).on("click", ".collection_save_button", function () {
    saveCollection();
});

var notify = function ()
{
    var type = "GET";
    var dataType = "html";
    var url = web_url + "booking/notifyneworder";

    ajaxRequest(url, type, dataType, {}, {}, {});

}


$(document).ready(function () {
    franchise_id = $("#franchise_id").val();
    id = $("#id").val();
    getPickupDates();

    getServices(franchise_id, id);
});



