
var web_url = site_url;
var api_url = web_url + "api/";
var address_url = "";
var optionHtml = "";

var preferences_total = 0.00;
var services_total = 0.00;
var card_id = "";
var steps = ["order-address", "order-select-items", "order-select-preferences", "order-collection-delivery", "login-register", "order-payments"];

var Step1Next = function (addresses, address1, address2, postal_code, city, location, address_type) {


    
    var data = {addresses: addresses, address: address1, address2: address2, postal_code: postal_code, city: city,location:location,address_type:address_type};
    $("#step1_next_button").attr("disabled", true);
    $(".loader").hide();
    $("#step1_errors").hide();

    var type = "POST";
    var dataType = "json";
    var url = web_url + "/validatestep1";
    var callBack = function (response) {
        $('#step1_next_button').removeAttr("disabled");

        if (response.error === true) {

            displayErrors(response, "step1_errors");
        } else
        {
            $("#right_order_address").html(response.address1 + "\n" + response.address2);
            $(".order-address").addClass("d-none");
            $(".order-address-right").addClass("active");
            $("#has_preferences").val(response.has_preferences);

            Step2Open(response.franchise_id, "1");
            franchise_id = response.franchise_id;

            if (response.services_count > 0) {
                $(".mini-cart-right").html(response.items_right).removeClass("d-none");
                $(".mini-cart-right-later").html("").addClass("d-none");
            } else {
                $(".mini-cart-right").html("").addClass("d-none");
                $(".mini-cart-right-later").html("").removeClass("d-none");
            }

            $('#item_minimum_amount').text(response.minimum_order_amount);
            $('#item_minimum_amount').text(response.minimum_order_amount);
            $('#minimum_order_amount').val(response.minimum_order_amount);
            $('#minimum_order_amount_later').val(response.minimum_order_amount_later);
        }
        $(".loader").hide();

    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        $('#step1_next_button').removeAttr("disabled");
        displayErrors(jqXHR.responseJSON, "step1_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

var Step2Open = function (franchise_id, show) {

    $(".loader").show();
    var data = "";
    var type = "GET";
    var dataType = "html";
    var url = web_url + "order-items?fid=" + franchise_id;
    var callBack = function (response) {
        //  $(".loader").hide();
        $(".action-item").attr("disabled", false);
        var postal_code = $("#postal_code").val();
        setCookie(postal_code, "order-select-items");

        var order_select_items = $(".order-select-items");
        order_select_items.html(response);
        if (show == "1") {
            order_select_items.removeClass("d-none");
            $(".header-order-select-items").addClass("active");
        }
        owlCarousel();


        if (!isMobile()) {
            $(".next-previous-buttons").removeClass("d-none");
        }

        bookingRightView();
        getPickupDates();

    };
    var errorCallBack = function () {

    };
    $(".loader").hide();
    if (franchise_id) {
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }
}

var bookingRightView = function () {

    var data = "";
    var type = "GET";
    var dataType = "json";
    var url = web_url + "bookingrightview";
    var callBack = function (response) {

        services_total = response.services_total;
        var total = 0;
        var discount_amount = 0.00;

        if (services_total > 0) {

            $(".order-services-right").show();
            $("#item_services_price").html(services_total);
            total = parseFloat(services_total) + parseFloat(preferences_total);
            total = parseFloat(total).toFixed(2);
            if (response.discount_amount > 0) {
                $(".order-discount-right").show();
                discount_amount = parseFloat(response.discount_amount).toFixed(2);

                $(".discount-right-text").html("Discount (" + response.discount_code + ") <button type='button' class='close float-none' aria-label='Discount Delete' id='cancel-discount'><span aria-hidden='true'>×</span></button>");
                $("#item_discount_price").html(discount_amount);
            } else {
                //$(".order-discount-right").hide();
            }
        } else {
            $(".order-services-right").hide();
            $("#item_services_price").html("0.00");
            total = response.minimum_order_amount;
            total = parseFloat(total).toFixed(2);
        }

        total = total - discount_amount;
        total = parseFloat(total).toFixed(2);
        $("#item_total_price").html(total);
        $("#item_minimum_amount").html(response.minimum_order_amount);
    };
    var errorCallBack = function () {
        //$(".action-item").attr("disabled", false);
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

var next = function (id) {

    var postal_code = $("#postal_code").val();
    $(".booking-step").removeClass("active");
    switch (id) {
        case "step1_next_button":
            var addresses = $("#addresses").val();
            var address1 = $("#address").val();
            var address2 = $("#address2").val();
            var city = $("#town").val();
            var location = $("#location").val();
            var addressType = $('input[name="address_type"]:checked').val();
            Step1Next(addresses, address1, address2, postal_code, city, location,addressType);
            break;
        case "items_next_button":
            countServices("items");
            break;
        case "btn_cart_item_later":
            countServices("later");
            break;
        case "preferences_next_button":

            $(".order-collection-delivery").removeClass("d-none");
            $(".order-select-preferences").addClass("d-none");
            setCookie(postal_code, "order-collection-delivery");
            storePreferences();

            break;
        case "collection_next_button":
            saveCollection();

            break;

        case "order_payments":
            reviewOrder();
            break;

        case "register_login_next_button":
            var val = $("#register_login_next_button").val();

            if (val == "register-form") {
                register();
            } else {
                login();
            }
            $(".header-login-register").addClass("active");
            break;

    }

}

$(document).on("click", ".next", function () {
    var id = $(this).attr("id");
    next(id);
});

$(document).on("click", ".next-rgt-button", function () {
    var id = $('.steps:not(.d-none)').attr("data-id");
    next(id);
});

$(document).on("change", ".payment-methods", function () {

    var payment_method = $("input[name='payment_methods']:checked").val();
    if (payment_method == "stripe") {
        $(".stripe").removeClass("d-none");
    } else {
        $(".stripe").addClass("d-none");

    }
});



$(document).on("click", "#btn_forgot", function () {

    $(".loader").show();

    $(".forgot_errors").html("").hide();

    var email = $("#forgot_email_address").val();
    var data = {email: email};
    var method = "POST";
    var dataType = "json";
    var url = web_url + "booking/forgotpassword";
    var callBack = function (response) {
        $(".loader").hide();

        if (response.error === true) {
            displayErrors(response, "forgot_errors");
        } else
        {
            $("#forgot_errors").html('<div class="alert alert-success alert-dismissible fade show" role="alert">' + response.message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>').show();
            $("#login_email").val(email);
            $("#forgot_email_address").val("");
        }



    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        displayErrors(jqXHR.responseJSON, "forgot_errors");
        $(".loader").hide();
    };
    ajaxRequest(url, method, dataType, data, callBack, errorCallBack);
});

var countServices = function (type) {
    $(".loader").show();
    var data = {type: type};


    var method = "GET";
    var dataType = "json";
    var url = web_url + "booking/countservices";
    var callBack = function (response) {
        var postal_code = $("#postal_code").val();

        if (type == "later") {
            $(".order-collection-delivery").removeClass("d-none");
            $(".order-select-items").addClass("d-none");
            $(".order-select-items-right").addClass("active");
            $(".order-services-right").addClass("d-none");
            //$(".mini-cart-right-later").removeClass("d-none");
            $(".mini-cart-right").html("").addClass("d-none");
            $(".mini-cart-right-later").html("Later").removeClass("d-none");
            //$response["minimum_order_amount"]
            $("#item_total_price").html(response.minimum_order_amount);

            $("#has_preferences").val(false);
            $(".in-cart-qty").html("0");
            setCookie(postal_code, "order-collection-delivery");
            $(".header-order-collection-delivery").addClass("active");
            $(".order-select-items-right").find(".order-summary-edit").removeClass("d-none");


        } else {

            if (response.error === true) {
                displayErrors(response, "items_errors");
            } else
            {
                var has_preferences = Boolean(response.has_preferences);

                $("#has_preferences").val(has_preferences)

                if (has_preferences == true) {
                    $(".order-select-preferences").removeClass("d-none");
                    $(".order-select-preferences").removeClass("d-none");
                    setCookie(postal_code, "order-select-preferences");
                } else {
                    $(".order-collection-delivery").removeClass("d-none");
                    $(".header-order-collection-delivery").addClass("active");
                    setCookie(postal_code, "order-collection-delivery");
                }

                $(".mini-cart-right-later").addClass("d-none");
                $(".order-select-items").addClass("d-none");
                $(".order-select-items-right").addClass("active");
                $(".item_selection").hide();
                $(".order-select-items-right").find(".order-summary-edit").removeClass("d-none");

            }

        }
        $(".loader").hide();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "items_errors");
    };
    ajaxRequest(url, method, dataType, data, callBack, errorCallBack);

}

var saveCollection = function () {
    $(".loader").show();
    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();

    var delivery_date = $("#delivery_date").val();
    var delivery_time = $("#delivery_time").val();
    var order_notes = $("#order_notes").val();

    var data = {pickup_date: pickup_date, pickup_time: pickup_time, delivery_date: delivery_date, delivery_time: delivery_time, order_notes: order_notes}
    var type = "POST";
    var dataType = "json";
    var url = web_url + "booking/savetimings";
    var callBack = function (response) {

        $(".loader").hide();

        var postal_code = $("#postal_code").val();
        var is_login = $("#is_login").val();

        if (is_login) {
            $(".order-payments").removeClass("d-none");
            setCookie(postal_code, "order-payments");
            hideNextRightBtn();

        } else {
            $(".login-register").removeClass("d-none");
            setCookie(postal_code, "login-register");

        }
        $(".order-collection-delivery-right").addClass("active");
        $(".order-collection-delivery").addClass("d-none");
        $(".collection").removeClass("d-none");
        //$(".order-summary-edit").removeClass("d-none");
        $(".order-collection-delivery-right").find(".order-summary-edit").removeClass("d-none");


    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "order_collection_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

}


$(document).on("click", ".previous", function () {

    var id = $(this).attr("id")
    $(".booking-step").removeClass("active");
    switch (id) {

        case "items_previous_button":

            $(".order-address").removeClass("d-none");
            $(".header-order-address").addClass("active");
            $(".order-select-items").addClass("d-none");
            break;
        case "preferences_previous_button":
            $(".order-select-items").removeClass("d-none");
            $(".header-order-select-items").addClass("active");
            $(".order-select-preferences").addClass("d-none");
            break;
        case "collection_previous_button":

            var has_preferences = $("#has_preferences").val();

            if (has_preferences == "true") {

                $(".order-select-preferences").removeClass("d-none");
            } else {
                $(".order-select-items").removeClass("d-none");
                $(".header-order-select-items").addClass("active");
            }
            $(".order-collection-delivery").addClass("d-none");
            //$(".order-collection-delivery").hide();

            break;
        case "register_login_previous_button":

            $(".order-collection-delivery").removeClass("d-none");
            $(".header-collection-delivery").addClass("active");
            $(".login-register").addClass("d-none");
            break;



    }
    //return false;
});



$(document).on("click", ".login-register-a", function () {
    var hide = $(this).attr("data-hide");
    var show = $(this).attr("data-show");
    $("." + hide).addClass("d-none");
    $("." + show).removeClass("d-none");
    $("#register_login_next_button").val(show);
});



$(document).on("click", ".payment-cards-a", function () {
    var hide = $(this).attr("data-hide");
    var show = $(this).attr("data-show");
    $("#" + hide).hide("d-none");
    $("#" + show).show("d-none");
});

$(document).on("keyup change", "#postal_code", function (e) {

    //e.preventDefault();
    if (e.keyCode === 13) {
        return false;
    }

    var postal_code = $("#postal_code").val();
    getAddresses(postal_code);
    $("#google_addresses").show();
});


var storePreferences = function () {
    var data = $("#checkout-form").serialize();
    //var data = $('#checkout-form').find('input[name^="preferences_list"]').serialize();

    var type = "POST";
    var dataType = "json";
    var url = web_url + "booking/storepreferences";
    var callBack = function (response) {

        preferences_total = response.total;

        if (preferences_total > 0) {
            $(".order-preferences-right").show();
            $("#item_preferences_price").html(preferences_total);
            bookingRightView();
        }


    };
    var errorCallBack = function (jqXHR, error, errorThrown) {};
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

var saveCard = function (payment_method_id, id) {

    $("#payment_errors").html("");
    var first_name = $("#payment_first_name").val();
    var last_name = $("#payment_last_name").val();
    var email = $("#payment_email").val();
    var country_code = $("#payment_country_code").val();
    var phone = $("#payment_phone").val();

    var data = {payment_method_id: payment_method_id, card_id: id, first_name: first_name, last_name: last_name, email: email, country_code: country_code, phone: phone}

    var type = "POST";
    var dataType = "json";
    var url = web_url + "booking/savecard";
    var callBack = function (response) {
        card_id = response.id;
        confirmOrder();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        $('.order-summary-container').addClass("is-active");
        $('.order-summary-container').modal('hide');

        $(".next-rgt-loading-button").attr("disabled", false).addClass("d-none");

        displayErrors(jqXHR.responseJSON, "payment_errors");

        $(".next-rgt-confirm-button").attr("disabled", false).removeClass("d-none");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

var reviewOrder = function () {

    if (isMobile()) {

        var full_name = $("#payment_first_name").val() + " " + $("#payment_last_name").val();
        var email = $("#payment_email").val();
        var phone = $("#payment_phone").val();
        var code = $("#basic-addon1").text();
        $("#contact_name").html(full_name);
        $("#contact_email").html(email);
        $("#contact_phone").html(code + phone);


        $('.order-summary-container').addClass("is-active");
        $('[data-toggle="tooltip"]').tooltip();
        $(".order-term-condition").children(".next-rgt-confirm-button").removeClass('d-none').html("Confirm Order");
    }
}

var confirmOrder = function () {
    $(".loader").show();
    $("#pickup_date").val();
    var payment_method = $("input[name='payment_methods']:checked").val();
    $('#payment_errors').html();
    var first_name = $("#payment_first_name").val();
    var last_name = $("#payment_last_name").val();
    var email = $("#payment_email").val();
    var phone = $("#payment_phone").val();
    var country_code = $("#payment_country_code").val();

    var data = {card_id: card_id, payment_method: payment_method, first_name: first_name, last_name: last_name, email: email, country_code: country_code, phone: phone};
    var type = "POST";
    var dataType = "json";
    var url = web_url + "booking/confirmorder";
    var callBack = function (response) {
        $(".loader").hide();
        var postal_code = $("#postal_code").val();
        deleteCookie(postal_code);
        setCookie(postal_code, "order-address");
        window.location = web_url + "thank-you";
        return false;
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {

        $(".loader").hide();

        if (isMobile()) {
            $('.order-summary-container').removeClass('is-active');
            $(".next-rgt-button").html("Next");
            $('[data-toggle="tooltip"]').tooltip();
        }

        $(".next-rgt-confirm-button").attr("disabled", false).removeClass("d-none");
        $(".next-rgt-loading-button").attr("disabled", false).addClass("d-none");
        displayErrors(jqXHR.responseJSON, "payment_errors");
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
}

$(document).on("change", "#addresses", function () {
    var address = $("#addresses").val();
    var postal_code = $("#postal_code").val();
    if (address == -1) {
        $("#div_reg_address1").show();
        //$(".map-container").hide();
        $(".next-previous-buttons").show();
        $(".map-container").show();
    } else {

        getLatLng(postal_code, function (lat, lng) {

            $("#location").val(lat + "," + lng);
            showMap(lat, lng);
           
            
        });

        $("#right_order_address").html(address);
        $("#div_reg_address1").hide();
        $("#reg_address2").val("");
        $(".next-previous-buttons").show();
        $(".map-container").show();

    }
});

$(document).on("change", ".payments-cards", function () {
    var payment_card = $("input[name='payments_cards']:checked").val();
    $(".btn-payment").removeClass("active");
    $("#payments_cards_" + payment_card).parent(".btn-payment").addClass("active");
    card_id = payment_card;
});


$(document).on("click", ".discount-button", function () {

    $("#right_below_errors").addClass("d-none");

    var isLogin = $("#is_login").val();
    //alert(isLogin);
    if (isLogin) {

        $("#discountModal").modal();
    } else {
        $("#right_below_errors").removeClass("d-none").html('<div class="alert alert-danger alert-dismissible" role="alert"><p class="mb-0 order-discount-text">Please login or register to use the discount code.</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>');
        /*
         $("#modal_title").text("Please see the folliwing message");
         $("#modal_message").text("Please login or register to use the discount code");
         $("#myModal").modal();
         */
    }
});
$(document).on("click", "#btn_password_show", function () {
    $(this).find(".input-group-text").find("i").toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).parent(".input-group").find("input"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
$(document).on("click", ".apply-discount", function () {
    $(".loader").show();
    var code = $("#code").val();
    var discount_type = $("input[name='discount_type']:checked").val();

    var url = web_url + "booking/applydiscountcode";
    var data = {code: code, discount_type: discount_type};
    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {
        $(".loader").hide();

        if (response.error == false) {
            $(".discount-right-text").html("Discount (" + code + ") <button type='button' class='close float-none' aria-label='Discount Delete' id='cancel-discount'><span aria-hidden='true'>×</span></button>");
        }
        bookingRightView();
        $('#discountModal').modal("hide")
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "discount_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);


});



$(document).on("click", "#cancel-discount", function () {

    canceldiscount();
});


var canceldiscount = function () {

    var url = web_url + "booking/canceldiscount";

    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {
        $(".order-discount-right").hide();
        bookingRightView();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "payment_errors");
    };

    ajaxRequest(url, type, dataType, {}, callBack, errorCallBack);


}

$(document).on("click", ".order-summary-edit", function () {

    var postal_code = $("#postal_code").val();
    $(".steps").addClass("d-none");
    var show = $(this).data("show");


    $("." + show).removeClass("d-none");
    $(".booking-step").removeClass("active");
    $(".header-" + show).addClass("active");

    deleteCookie(postal_code);
    setCookie(postal_code, show);


    if (isMobile()) {
        $('.order-summary-container').removeClass('is-active');


        if (show == "order-payments") {
            $(".next-rgt-button").html("Review Order");
        } else {
            $(".next-rgt-button").html("Next");
        }


        $('[data-toggle="tooltip"]').tooltip()
    }

    showNextRightBtn();
});

var hideNextRightBtn = function () {

    if (!isMobile()) {
        $(".next-rgt-button").addClass('d-none');
        $(".next-rgt-confirm-button").removeClass('d-none');
    } else {
        $(".next-rgt-button").html("Review order");

    }
}
var showNextRightBtn = function () {

    $(".next-rgt-confirm-button").addClass('d-none');
    $(".next-rgt-button").removeClass('d-none');
}

var login = function () {

    $(".loader").show();
    $("#contact_errors").hide();

    var email = $("#login_email").val();
    var password = $("#login_password").val();

    var url = web_url + "booking/login";
    var data = {email: email, password: password};
    var type = "POST";
    var dataType = "json";
    var postal_code = $("#postal_code").val();

    var callBack = function (response) {
        //alert("callBack");
        showPaymentsArea(response);
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        //alert("errorCallBack");
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "contact_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

}

function showPaymentsArea(response) {

    var postal_code = $("#postal_code").val();
    var view = getCookie(postal_code);
    //alert(postal_code);
    //alert(view);

    $(".loader").hide();
    $("#is_login").val(true);
    $("#contact_name").html(response.full_name);
    $("#contact_email").html(response.email);
    $("#contact_phone").html(response.country_phone_code + response.phone);
    $("#payment_first_name").val(response.first_name);
    $("#payment_last_name").val(response.last_name);
    $("#payment_phone").val(response.phone);
    $("#payment_email").val(response.email);
    $("#payment_country_code").val(response.country_code).trigger('change');
    $(".contact-detail-right").show();
    $(".contact-detail").removeClass("d-none");

    $(".login-register").addClass("d-none");
    $(".login-register-right").addClass("active");
    $(".login-register").addClass("d-none");
    $(".order-payments").removeClass("d-none");
    hideNextRightBtn();
    if (view == "login-register") {
       // $(".order-payments").removeClass("d-none");
        //setCookie(postal_code, "order-payments");
       // hideNextRightBtn();
    }


    $(".login-register-right").find(".order-summary-edit").removeClass("d-none");

    if (response.cards.length > 0) {

        var i = 0;
        var css = "active";
        var checked = "checked";
        $.each(response.cards, function (i, item) {

            if (i == 0) {
                card_id = item.id;
            }

            var optionHtml = '<div class="custom-control custom-radio btn btn-block btn-payment border shadow mb-3 py-2 rounded-0 ' + css + '">';

            optionHtml += '<input type="radio" value="' + item.id + '" id="payments_cards_' + item.id + '" class="custom-control-input payments-cards" ' + checked + ' name="payments_cards">';
            optionHtml += '<label class="custom-control-label form-row px-3 align-items-center" for="payments_cards_' + item.id + '">';
            optionHtml += '<div class="col-7 text-left"><p class="m-0">Card Number: ' + item.title + '</p></div>';
            optionHtml += '<div class="col-5 text-md-right"></i></div></label></div>';

            $("#cards-list").append(optionHtml);
            css = "";
            checked = ""
            i++;
        });

        $(".payment-cards-a").show();
        $("#cards-list").show();
        $("#card-form").hide();

    } else {
        $("#cards-list").hide();
        $("#card-form").show();
    }


    //
    $("#contact_errors").hide();

}

var register = function () {

    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var password = $("#password").val();
    var country_code = $("#country_code :selected").val();
    var confirm_password = $("#confirm_password").val();
    $("#contact_errors").hide();
    var postal_code = $("#postal_code").val();

    // var data = $("#frm_cart_member_address").serialize();
    var url = web_url + "booking/register";
    var data = {first_name: first_name, last_name: last_name, email: email, country_code: country_code, phone: phone, password: password, confirm_password: confirm_password};
    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {
        $("#is_login").val(true);
        $("#contact_name").html(response.full_name);
        $("#contact_email").html(response.email);
        $("#contact_phone").html(response.phone);

        $(".contact-detail-right").show();
        $(".contact-detail").removeClass("d-none");

        $("#payment_first_name").val(first_name);
        $("#payment_last_name").val(last_name);
        $("#payment_phone").val(phone);
        $("#payment_email").val(email);
        $("#payment_country_code").val(country_code).trigger('change');


        $(".login-register").addClass("d-none");
        $(".order-payments").removeClass("d-none");
        $(".login-register-right").addClass("active");

        $("#cards-list").hide();
        $("#card-form").show();
        $(".login-register-right").find(".order-summary-edit").removeClass("d-none");
        setCookie(postal_code, "order-payments");


        hideNextRightBtn();

    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "contact_errors");
    };

    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

}
$(document).ready(function () {

    if ($(".select2").length > 0) {
        $(".select2").select2({
            minimumResultsForSearch: Infinity
        });
    }

    var postal_code = $("#postal_code").val();
    var view = getCookie(postal_code);


    $(".steps").addClass("d-none");


    if (view != "") {
        if (view == "order-payments") {
            hideNextRightBtn();
        } else {

        }
    } else {
        view = "order-address";
    }
    $("." + view).removeClass("d-none");
    $(".header-" + view).addClass("active");

    if (isMobile()) {
        $(".next-previous-buttons").addClass("d-none");
        $(".responsive-btns").removeClass("d-none");
        $(".responsive-btns").children(".next-rgt-button").removeClass("d-none");
        $('[data-toggle="tooltip"]').tooltip();

    } else {
        $(".next-previous-buttons").removeClass("d-none");
        $(".non-responsive-btns").removeClass("d-none");

        if (view != "order-payments") {
            showNextRightBtn();
        }
    }

    $(".action-item").attr("disabled", false);

    $(".order-form").removeClass("d-none");


    if (postal_code != "") {
        Step2Open(franchise_id, "0");
    }
    $(".loader").hide();
    getAddresses(postal_code);
});

$(window).resize(function () {

    if (isMobile()) {
        $(".next-previous-buttons").addClass("d-none");
        $(".responsive-btns").removeClass("d-none");
        $(".responsive-btns").children(".next-rgt-button").removeClass("d-none");
        $('[data-toggle="tooltip"]').tooltip();

    } else {
        $(".next-previous-buttons").removeClass("d-none");
        $(".non-responsive-btns").removeClass("d-none");
    }
});


$(document).on("click", ".btn-order-summary-close", function () {
    $(".order-summary-container").removeClass("is-active");
});

$(document).on("click", ".remove-cart-service", function () {

    $(".loader").show();
    var service_id = $(this).attr("data-id");
    var q = "service_id=" + service_id;
    var data = "";
    var type = "GET";
    var dataType = "json";
    var url = web_url + "booking/removecartservice?" + q;

    $(".cart-service-" + service_id).remove();
    $("#qty" + service_id).html(0);
    var callBack = function (response) {
        setTimeout(function () {
            $(".loader").hide();
            bookingRightView();

        }, 400);
    };
    var errorCallBack = function () {
        $(".loader").hide();
    };
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);


});

$('input[type=radio][name=address_type]').on('change', function() {
   // alert(this.value);

    console.log(addressTypes[this.value]);
});

$(document).on("click", ".action-item", function () {

    $(".action-item").attr("disabled", true);
    var current_context = $(this);
    var preference = current_context.attr("data-preference-show");
    var type = current_context.attr("data-type");
    var price = current_context.attr("data-price");
    var title = current_context.attr("data-title");
    var id = current_context.attr("data-id");
    var category = current_context.attr("data-category");
    var parent_area = current_context.parents(".mix");
    var category_id = current_context.attr("data-category-id");
    var qty_area = parent_area.find(".in-cart-qty");
    var qty = parseInt(qty_area.text());

    var successcallback = function (type) {

        $(".loader").hide();
        $(".action-item").attr("disabled", false);

        parent_area.find(".less").removeClass("disabled");
        qty_area.removeClass("blink");
        qty_area.addClass("blink");
        category_id = parseInt(category_id);
        var package = current_context.attr("data-package");
        var desktop_image = current_context.attr("data-desktop-image");
        var mobile_image = current_context.attr("data-mobile-image");
        var body = '<h6>SAFE TO INCLUDE</h6><ul class="list-group-item border-0 px-0 fs16 list-group-flush"><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-check text-success mr-2"></i> <span>All personal laundry that can be washed & tumble dried.</span></li><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-check text-success mr-2"></i> You can include bed linen and towel but we advise you to put them in a separate bag as they usually require a different washing/drying cycle.</li><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-check text-success mr-2"></i> Bath mats</li></ul><h6>DO NOT INCLUDE</h6><ul class="list-group-item border-0 px-0 fs16 list-group-flush"><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-times text-danger mr-2"></i> Dry clean only items</li><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-times text-danger mr-2"></i> Items that cannot be tumble dried.</li><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-times text-danger mr-2"></i> All items that are not suitable for machine washing and tumble drying</li><li class="list-group-item px-0 py-1 d-flex justify-content-start align-items-start"><i class="fas fa-times text-danger mr-2"></i> Any garments that can shrunk during tumble drying process</li></ul>';

        if (qty == 0 && category_id == 10) {
            openModal("Wash Dry and Fold order what to include", body);
        }

        var service_id = current_context.attr("data-id");
        var q = "service_id=" + service_id + "&price=" + price;
        q = q + "&category_id=" + category_id + "&qty=" + qty + "&category=" + category;
        q = q + "&package=" + package + "&preference=" + preference + "&type=" + type;
        q = q + "&desktop_image=" + desktop_image + "&mobile_image=" + mobile_image;
        var data = "";
        var type = "GET";
        var dataType = "json";
        var url = web_url + "addremoveitems?" + q;
        var callBack = function (response) {
            setTimeout(function () {
                $(".loader").hide();
                $("#has_preferences").val(response.has_preferences);
                bookingRightView();
                //qty_area.removeClass("blink");
            }, 400);
        };
        var errorCallBack = function () {
            $(".loader").hide();
        };
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
        return false;

    }
    updateView(type, id, title, qty, price, successcallback);
    return false;
});


function owlCarousel() {
    if ($('.order-category-carousel').length > 0) {
        $('.order-category-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            dots: false,
            //mouseDrag:false,
            //touchDrag:false,
            responsive: {
                0: {
                    items: 3,
                    nav: true
                },
                600: {
                    items: 5,
                    nav: false
                },
                1000: {
                    items: 5,
                    nav: true
                },
                1100: {
                    items: 7,
                    nav: true,
                    loop: false
                }
            }
        })
    }
    var containerEl = document.querySelector('.order-list-items');
    if ($('.order-list-items').length > 0) {
        var mixer = mixitup(containerEl, {
            load: {
                filter: '.category-0'
            }
        });
    }
}



function updateView(type, id, title, qty, price, successcallback) {

    if (type == "add") {
        var html = "";
        var q = qty + 1;
        $("#qty" + id).html(q);
        //$("#btn_cart_item_later").hide();
        $(".order-services-right").show();
        var total = parseFloat(price).toFixed(2) * q;
        total = parseFloat(total).toFixed(2);
        if (q == 1) {
            html = '<div class="form-row mx-0 cart-service-' + id + '" ><div class="col-7 cart-name">' + title + '</div><div class="col-1 cart-qty cart-qty-service-' + id + '">' + q + '</div><div class="col-4 cart-price cart-price-service-' + id + '">' + currency_symbol + '' + total + '</div>';
            $(".mini-cart-right").append(html).removeClass('d-none');
            $(".mini-cart-right-later").addClass('d-none');

        } else {
            $(".cart-price-service-" + id).html(currency_symbol + total);
            $(".cart-qty-service-" + id).html(q);
        }



    } else {

        if (qty > 0) {
            q = qty - 1;
            if (q == 0) {
                $(".cart-service-" + id).remove();
                //$(".cart-qty-service-" + id).html();
            } else {
                var total = parseFloat(price).toFixed(2) * q;
                total = parseFloat(total).toFixed(2);
                $(".cart-price-service-" + id).html(currency_symbol + total);
                $(".cart-qty-service-" + id).html(q);
            }


        }
        $("#qty" + id).html(q);
    }

    successcallback(type)
}
$(window).on("scroll", function () {
    var scrollTop = $(window).scrollTop();
    if (scrollTop > $(".order-form").offset().top) {
        $(".order-sticky").addClass("sticky");
    } else {
        $(".order-sticky").removeClass("sticky");
    }
});

$(document).keypress(function (e) {
    if (e.which == 13) {
        return false;
    }
});

$('#checkout-form').submit(function () {
    return false;
});


var openModal = function (title, message) {
    
    $("#modal_title").html(title);
    $("#modal_message").html(message);
    $("#myModal").modal();
}

