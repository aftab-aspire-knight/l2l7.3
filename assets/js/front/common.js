function ajaxRequest(url, type, dataType, data, callBack, errorCallBack) {

    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: dataType,
        cache: false,
        crossDomain: true,

        success: function (result) {
            callBack(result);
        },
        error: function (jqXHR, error, errorThrown) {

            if (dataType == "json") {
                errorCallBack(jqXHR, error, errorThrown);
            } else {
                displayErrors(jqXHR.responseJSON, "errors");
            }
        }
    });
}

function displayErrors(response, id) {

    var errorsHtml = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
    $.each(response.errors, function (key, value) {
        errorsHtml += '<p class="mb-0" >' + value + '</p>'; //showing only the first error.
    });
    errorsHtml += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>';
    $('#' + id).html(errorsHtml).show().focus();
}

function owlCarousel() {
    if ($('.order-category-carousel').length > 0) {
        $('.order-category-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            dots: false,
            //mouseDrag:false,
            //touchDrag:false,
            responsive: {
                0: {
                    items: 3,
                    nav: true
                },
                600: {
                    items: 5,
                    nav: false
                },
                1000: {
                    items: 5,
                    nav: true
                },
                1100: {
                    items: 7,
                    nav: true,
                    loop: false
                }
            }
        })
    }
    var containerEl = document.querySelector('.order-list-items');
    if ($('.order-list-items').length > 0) {
        var mixer = mixitup(containerEl, {
            load: {
                filter: '.category-0'
            }
        });
    }
}



$('.custom-select').change(function (e) {
    var value = $(this).val();
    $(this).prev("span.input-text-country").text("+" + value);
});

$('.phone-number').keyup(function (e) {
    //console.log("phone-number" + $(this).val(''));
    if ($(this).val().match(/^0/)) {
        $(this).val('');
        return false;
    }
});


var isMobile = function () {
    var width = $(document).width();
    if (width < 992) {
        return true;
    } else {
        return false;
    }
}


function socialLogin(id, first_name, last_name, email, type) {


    var address = "";
    var address2 = "";
    var post_code = "";
    var town = "";

    if ($(".order-summary-heading").length > 0) {

        var addresses = $("#addresses").val();
        address2 = $("#address2").val();

        if (addresses == -1) {
            address = $("#address").val();

        } else {
            address = $("#addresses").val();

        }
        post_code = $("#postal_code").val();
        town = $("#town").val();
        //alert(town);
    }

    $(".loader").show();
    var data = {id: id, first_name: first_name, last_name: last_name, email: email, type: type, address: address, address2: address2, post_code: post_code, town: town};
    var url = web_url + "social/login";
    var type = "POST";
    var dataType = "json";

    var callBack = function (response) {
        //alert($(".order-summary-heading").length)
        if ($(".order-summary-heading").length > 0) {

            if (response.result == "login") {
                showPaymentsArea(response);
            } else {
                showPaymentsArea(response);
            }
        } else {

            var action = $("#frm_register").length;
            if (response.result == "login") {
                window.location = web_url + "dashboard";
            } else {

                if ($("#frm_register").length > 0) {

                    $("#reg_first_name").val(response.first_name);
                    $("#reg_last_name").val(response.last_name);
                    $("#reg_email_address").val(response.email);
                    $("#social_id").val(response.id);
                    $("#type").val(type);

                } else {
                    window.location = web_url + "account-setting?q=register";
                }


            }
        }
        $(".loader").hide();
    };
    var errorCallBack = function (jqXHR, error, errorThrown) {
        $(".loader").hide();
        displayErrors(jqXHR.responseJSON, "contact_errors");

    };
    $(".loader").hide();
    ajaxRequest(url, type, dataType, data, callBack, errorCallBack);



}
