
/*
google.maps.event.addDomListener(window, 'load', function () {
    var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    google.maps.event.addListener(places, 'place_changed', function () {
        var place = places.getPlace();

        console.log(place.geometry.longitude);


        var address = place.formatted_address;
        var latitude = place.geometry.location.A;
        var longitude = place.geometry.location.F;
        var mesg = "Address: " + address;
        mesg += "\nLatitude: " + latitude;
        mesg += "\nLongitude: " + longitude;
        alert(mesg);
    });
});
*/

//site_url = site_url + "index.php/";
site_url = site_url + "";
var placeName = "";
var place_id = "";
var pin = "";
var country = "";
var state = "";
var city = "";
var area = "";
var name = "";
var address = "";
var latitude = 0.0;
var longitude = 0.0;
var address_components = "";
var errMessage = document.getElementById("modal_message_map");

var defaultBounds = {
    north: latitude + 0.1,
    south: latitude - 0.1,
    east: longitude + 0.1,
    west: longitude - 0.1,
};

var geocoder = "";

var map;

function initMap() {



    errMessage.style.display = "none";
    var cityBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(north, west),
        new google.maps.LatLng(south, east));


    var latlng = new google.maps.LatLng(latitude, longitude);
    geocoder = new google.maps.Geocoder();
/*
    var mapOptions = {
        bounds: defaultBounds,
        disableDefaultUI: true,
        componentRestrictions: { country: country },
        zoom: 19,
        center: latlng
    }


    map = new google.maps.Map(document.getElementById("google_map"), mapOptions);
    map.setCenter({ lat: latitude, lng: longitude });
*/
    //Create a marker
   /* marker = new google.maps.Marker(
        {
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: latlng,
        });*/

}

window.initMap = initMap;

function showMap(lat, lng) {

    const location = { lat: lat, lng: lng };

    var myLatLng = location;

    map = new google.maps.Map(document.getElementById('google_map'), {
        bounds: defaultBounds,
        disableDefaultUI: true,
        componentRestrictions: { country: country },
        zoom: 19,
        center: myLatLng
    });

    /*
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Google Maps',
        draggable: true
    });


    marker.setMap(map);
    */

    google.maps.event.addListener(map, 'dragend', function () {
        geocodePosition(this.getCenter());
    });

    var addresses = $("#addresses").val();

    if (addresses != "" && addresses != -1) {

        $(".next-previous-buttons").show();
        $(".map-container").show();

    }
}

function getLatLng(address, callBack) {
    geocoder = new google.maps.Geocoder();
    var lat = '';
    var lng = '';

    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            lat = results[0].geometry.location.lat();
            lng = results[0].geometry.location.lng();


            // alert('Latitude: ' + lat + ' Logitude: ' + lng);

            callBack(lat, lng);

        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }

    });


}

function geocodePosition(pos) {


    geocoder = new google.maps.Geocoder();
    longitude = pos.longitude;
    latitude = pos.latitude;
    console.log(pos);

    geocoder.geocode
        ({
            latLng: pos
        },
            function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    lat = results[0].geometry.location.lat();
                    lng = results[0].geometry.location.lng();

                    result = results[0];
                    console.log(result);

                    var searchAddressComponents = results[0].address_components, searchPostalCode = "";

                    $.each(searchAddressComponents, function () {
                        if (this.types[0] == "postal_code") {
                            searchPostalCode = this.short_name;
                        }
                    });
                    searchPostalCode = searchPostalCode.replace(/\s/g, '');
                    //alert(searchPostalCode);
                    ///alert("ssss ssss");
                    address_components = result.address_components
                    $("#location").val(lat+","+lng);
                    //$("#address").val(address);
                } else {
                    console.log('Cannot determine address at this location.' + status);
                }

            }
        );
}