'use strict';
if (stripe_key != "") {
    var stripe = Stripe(stripe_key);
    var elements = stripe.elements({
        // Stripe's examples are localized to specific languages, but if
        // you wish to have Elements automatically detect your user's locale,
        // use `locale: 'auto'` instead.
        locale: window.__exampleLocale
    });
}
(function () {
    'use strict';


    if (stripe_key != "") {



        // Floating labels
        var inputs = document.querySelectorAll('.cell.example.example2 .input');
        Array.prototype.forEach.call(inputs, function (input) {
            input.addEventListener('focus', function () {
                input.classList.add('focused');
            });
            input.addEventListener('blur', function () {
                input.classList.remove('focused');
            });
            input.addEventListener('keyup', function () {
                if (input.value.length === 0) {
                    input.classList.add('empty');
                } else {
                    input.classList.remove('empty');
                }
            });
        });

        var elementStyles = {
            base: {
                color: '#959595',
                fontFamily: 'inherit',
                fontSize: '18px',
                fontSmoothing: 'antialiased',

                '::placeholder': {
                    color: '#CFD7DF',
                },
                ':-webkit-autofill': {
                    color: '#e39f48',
                },
            },
            invalid: {
                color: '#E25950',

                '::placeholder': {
                    color: '#FFCCA5',
                },
            },
        };

        var elementClasses = {
            focus: 'focused',
            empty: 'empty',
            invalid: 'invalid',
        };

        var cardNumber = elements.create('cardNumber', {
            style: elementStyles,
            classes: elementClasses,
        });
        cardNumber.mount('#card-number');

        var cardExpiry = elements.create('cardExpiry', {
            style: elementStyles,
            classes: elementClasses,
        });
        cardExpiry.mount('#card-expiry');

        var cardCvc = elements.create('cardCvc', {
            style: elementStyles,
            classes: elementClasses,
        });

        if ($("#card-cvc").length > 0) {

            cardCvc.mount('#card-cvc');

            registerElements([cardNumber, cardExpiry, cardCvc], 'example2');
        }

        function registerElements(elements, exampleName) {
            var formClass = '.' + exampleName;
            var example = document.querySelector(formClass);

            var form = example.querySelector('form');
            //var errorMessage = error.querySelector('.message');

            function enableInputs() {
                Array.prototype.forEach.call(
                        form.querySelectorAll(
                                "input[type='text'], input[type='email'], input[type='tel']"
                                ),
                        function (input) {
                            input.removeAttribute('disabled');
                        }
                );
            }

            function disableInputs() {
                Array.prototype.forEach.call(
                        form.querySelectorAll(
                                "input[type='text'], input[type='email'], input[type='tel']"
                                ),
                        function (input) {
                            input.setAttribute('disabled', 'true');
                        }
                );
            }

            function triggerBrowserValidation() {
                // The only way to trigger HTML5 form validation UI is to fake a user submit
                // event.
                disableInputs();
                var submit = document.createElement('input');
                submit.type = 'submit';
                submit.style.display = 'none';
                form.appendChild(submit);
                submit.click();
                submit.remove();
            }

            // Listen for errors from each Element, and show error messages in the UI.
            var savedErrors = {};
            elements.forEach(function (element, idx) {
                element.on('change', function (event) {

                    console.log(event.error);
                    //alert(event.error.message);

                });
            });

            // Listen on the form's 'submit' handler...
            form.addEventListener('submit', function (e) {
                e.preventDefault();

//aftab

                //      $(".next-rgt-confirm-button").addClass("d-none");
                //$(".next-rgt-loading-button").removeClass("d-none");
                //aftab

                var payment_method = $("input[name='payment_methods']:checked").val();

                //disableInputs();
                //$(':button').prop('disabled', true); // Disable all the buttons
                // Trigger HTML5 validation UI on the form if any of the inputs fail
                // validation.
                var plainInputsValid = true;
                Array.prototype.forEach.call(form.querySelectorAll('input'), function (
                        input
                        ) {
                    if (input.checkValidity && !input.checkValidity()) {
                        plainInputsValid = false;
                        return;
                    }
                });
                if (!plainInputsValid) {
                    triggerBrowserValidation();
                    return;
                }

                // Show a loading screen...
                example.classList.add('submitting');

                if ($('#card-form').is(':visible'))
                {

                    var client_secret = $("#client_secret").val();
                    var setup_intent_id = $("#setup_intent_id").val();


                    stripe.createPaymentMethod(
                            'card',
                            elements[0]
                            ).then(function (result) {

                        if (result.error) {

                            enableInputs();
                            if (isMobile()) {
                                $('.order-summary-container').removeClass("is-active").modal('hide');
                            }

                            var errorsHtml = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';

                            $.each(result.error, function (key, value) {

                                if (key == "message") {
                                    errorsHtml += '<p class="mb-0" >' + value + '</p>'; //showing only the first error.
                                }

                            });
                            errorsHtml += '<p class="mb-0" >Your card information is not correct. Please try again.</p>';
                            errorsHtml += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>';


                            if ($('.next-rgt-confirm-button').length > 0) {
                                $('.next-rgt-confirm-button').attr("disabled", false);
                                $(".next-rgt-confirm-button").removeClass("d-none");
                                $(".next-rgt-loading-button").addClass("d-none");

                            }
                            $('#payment_errors').html(errorsHtml).show().focus();
                            //$(':button').prop('disabled', false);
                            //$('a').prop('disabled', false);



                        } else {

                            console.log(result);

                            $("#payment_method_id").val(result.paymentMethod.id);
                            $("#payment_type").val("new");
                            confirmSchedule(result.paymentMethod.id, "new");
                            enableInputs();
                            //console.log(result.card);
                            //confirmSchedule(result.paymentMethod.id, "new");

                        }
                    });



                    /*stripe.confirmCardSetup(client_secret, {
                     payment_method: {
                     card: elements[0],
                     billing_details: {
                     name: name,
                     email: email
                     }
                     }
                     }).then(function (result) {
                     $('.loading').css('display', 'none');
                     
                     
                     if (result.error) {
                     
                     $('#card-errors').text(result.error.message);
                     
                     } else {
                     console.log(result.setupIntent);
                     
                     if (result.setupIntent.status === 'succeeded') {
                     
                     
                     $("#payment_method_id").val(result.setupIntent.payment_method);
                     $("#payment_type").val("new");
                     enableInputs();
                     confirmSchedule(result.setupIntent.payment_method, "new");
                     
                     //$('#card-success').text("payment successfully completed.");
                     
                     // setTimeout(
                     //   function(){ window.location.href = "{{url('/success')}}"; 
                     // }, 2000);
                     }
                     return false;
                     }
                     });*/




                } else {
                    enableInputs();
                    var card_id = $("input[name='payments_cards']:checked").val();


                    if (card_id != "") {
                        $("#payment_type").val("exist");
                        $("#payment_method_id").val(card_id);
                        confirmSchedule(card_id, "exist");
                    }
                }


            });
        }
    }

})();