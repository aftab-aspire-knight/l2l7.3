
var getPickupDates = function () {

    $("#btn_cart_address_page_continue").addClass("disabled");

    var q = "";
    if ($("#old_pickup_date").length > 0) {
        var old_pickup_date = $("#old_pickup_date").val();
        q = "?from=" + old_pickup_date;
    }

    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_pickup_date_records/" + franchise_id + q,
        cache: false,
        success: function (response) {

            var html = "";
            var day = "";
            var disabled = "disabled";

            $.each(response.pick, function (i, item) {

                day = item.Date_Class;

                disabled = "";
                if (day == "Available") {

                    html += "<option value='" + item.Date_Number + "' " + disabled + " >" + item.Date_List + "</option>";
                }


            });
            jQuery("#time_loader").hide();
            jQuery("#time_select_area").show();
            $("#pickup_date").html(html);
            getPickupTimes()
        },
        error: function (data) {
            console.log(data);

        }
    });
}


var change = 1;
var getPickupTimes = function () {

    var pickup_date = $("#pickup_date").val();
    $("#btn_cart_address_page_continue").addClass("disabled");
    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_time_records/?id=" + franchise_id + "&date=" + pickup_date + "&type=Pickup&device_id=1234",
        cache: false,
        success: function (response) {

            var html = "";
            var day = "";
            var disabled = "";
            var eco_friendly = 0;
            var eco_friendly_html = '<span class="time-options eco-friendly">Eco-friendly routes</span>';
            var other_options = 0;
            var other_options_html = '<span class="time-options other-options">Other time options</span>';
            var j = 0;
            $("#pickup_time_div").children(".dropdown-body").html("");
            $("#pickup_time").val("");
            $("#pickup_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>');
            $.each(response.data, function (i, item) {
                day = item.Class;

                if (day == "Available") {

                    var hour = item.Hour;

                    if (j == 0) {
                        $("#pickup_time_div").children(".selected").append(item.Time);
                        $("#pickup_time").val(item.Time);
                        j++;
                    }

                    if (hour <= 20) {

                        eco_friendly_html += "<span class='time-slot' data-time='" + item.Time + "'><i class='fa fa-leaf' aria-hidden='true'></i>" + item.Time + "</span>";

                        eco_friendly++;

                    } else if (hour > 20) {
                        other_options_html += "<span class='time-slot' data-time='" + item.Time + "'>" + item.Time + "</span>";
                        other_options++;
                    }

                }
            });
            if (eco_friendly == 0) {
                eco_friendly_html = "";
            }

            if (other_options == 0) {
                other_options_html = "";
            }

            html = eco_friendly_html + other_options_html;
            $("#pickup_time_div").children(".dropdown-body").html(html);

            setTimeout(function () {
                var pickup_date = $("#pickup_date option:selected").text();
                var pickup_time = $("#pickup_time").val();
                $("#collection_pickup_date").html(pickup_date);
                $("#collection_pickup_time").html(pickup_time);
            }, 2000);

            getDeliveryDate()
        },
        error: function (data) {

            console.log(data);
            //EnableDisableForm("Enable", jQuery("#body_area"), current_context, btn_1_text);
        }
    });
}

var getDeliveryDate = function () {

    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();
    var times = pickup_time.split("-")
    times = times[1].split(":");
    var pickup_time_hour = times[0];
    $("#btn_cart_address_page_continue").addClass("disabled");

    //if ($("#old_pickup_date").val() != "") {
    //    pickup_date = $("#old_pickup_date").val();
    //}


    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_delivery_date_records/" + franchise_id + "?date=" + pickup_date + "&pick_date=" + pickup_date + "&pick_time_hour=" + pickup_time_hour + "&",
        cache: false,
        success: function (response) {
            console.log(response.delivery);
            var html = "";
            var day = "";
            var disabled = "disabled";

            $.each(response.delivery, function (i, item) {

                day = item.Date_Class;
                disabled = "";
                if (day == "Available") {
                    html += "<option value='" + item.Date_Number + "' " + disabled + " >" + item.Date_List + "</option>";
                }
            });
            $("#time_loader").hide();
            $("#time_select_area").show();
            $("#delivery_date").html(html);
            getDeliveryTimes()
        },
        error: function (data) {
            // alert("Some went wrong.");
            console.log(data);
            //EnableDisableForm("Enable", jQuery("#body_area"), current_context, btn_1_text);
        }
    });
}


var getDeliveryTimes = function () {
    var delivery_date = $("#delivery_date").val();
    var pickup_date = $("#pickup_date").val();
    var pickup_time = $("#pickup_time").val();
    var times = pickup_time.split("-");
    times = times[1].split(":");
    var pickup_time_hour = times[0].replace(/^0+/, '');

    $("#btn_cart_address_page_continue").addClass("disabled");




    $.ajax({
        type: "post",
        dataType: "json",
        url: api_url + "get_time_records?id=" + franchise_id + "&date=" + delivery_date + "&pick_date=" + pickup_date + "&pick_time_hour=" + pickup_time_hour + "&device_id=1234&type=Delivery",
        cache: false,
        success: function (response) {

            if (response.data.length > 0) {

                var html = "";
                var day = "";

                var eco_friendly = 0;
                var eco_friendly_html = '<span class="time-options eco-friendly">Eco-friendly routes</span>';
                var other_options = 0;
                var other_options_html = '<span class="time-options other-options">Other time options</span>';
                var j = 0;

                $("#delivery_time_div").children(".dropdown-body").html("");
                $("#delivery_time").val("");
                $("#delivery_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>');

                $.each(response.data, function (i, item) {

                    day = item.Class;
                    if (day == "Available") {

                        var hour = item.Hour;

                        if (j == 0) {
                            $("#delivery_time_div").children(".selected").append(item.Time);
                            $("#delivery_time").val(item.Time);
                            j++;
                        }

                        if (hour <= 20) {

                            eco_friendly_html += "<span class='time-slot' data-time='" + item.Time + "'><i class='fa fa-leaf' aria-hidden='true'></i>" + item.Time + "</span>";

                            eco_friendly++;

                        } else if (hour > 20) {
                            other_options_html += "<span class='time-slot' data-time='" + item.Time + "'>" + item.Time + "</span>";
                            other_options++;
                        }

                    }
                });

                if (eco_friendly == 0) {
                    eco_friendly_html = "";
                }

                if (other_options == 0) {
                    other_options_html = "";
                }

                html = eco_friendly_html + other_options_html;
                $("#delivery_time_div").children(".dropdown-body").html(html);





                setTimeout(function () {
                    var delivery_date = $("#delivery_date option:selected").text();
                    var delivery_time = $("#delivery_time").val();
                    $("#collection_delivery_date").html(delivery_date);
                    $("#collection_delivery_time").html(delivery_time);
                }, 2000);


                $(".collection").removeClass("hide").show();


                if (change == 1) {
                    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
                    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];


                    var formatted_date = "";
                    var current_datetime = "";

                    if ($("#old_pickup_date").val() != "") {

                        var pd = $("#old_pickup_date").val();
                        var dd = $("#old_delivery_date").val();

                        if ($("#pickup_date option[value='" + pd + "']").length == 0) {
                            current_datetime = new Date(pd);
                            formatted_date = days[current_datetime.getDay()]
                                    + ", " + current_datetime.getDate() + " " + months[current_datetime.getMonth()];
                            $("#pickup_date").prepend("<option value='" + pd + "' selected=selected> " + formatted_date + " </option>'");
                        } else {
                            $("#pickup_date").val(pd);
                        }

                        $("#pickup_time").val($("#old_pickup_time").val());



                        if ($("#delivery_date option[value='" + dd + "']").length == 0) {
                            current_datetime = new Date(dd);
                            formatted_date = days[current_datetime.getDay()]
                                    + ", " + current_datetime.getDate() + " " + months[current_datetime.getMonth()];
                            $("#delivery_date").prepend("<option value='" + dd + "' selected=selected> " + formatted_date + " </option>'");
                        } else {
                            $("#delivery_date").val(dd);
                        }

                        $("#delivery_time").val($("#old_delivery_time").val());

                        $("#pickup_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>' + $("#old_pickup_time").val());
                        $("#delivery_time_div").children(".selected").html('<i class="fa fa-leaf" aria-hidden="true"></i>' + $("#old_delivery_time").val());



                        change = 0;

                    }
                }

            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

if (jQuery('.select-dropdown').length > 0) {
    $(document).on("click", ".select-dropdown", function () {

        var id = $(this).attr("id");
        if (!$("#" + id).hasClass('is-active')) {
            $("#" + id).addClass("is-active");
        }
        $("#" + id).children(".dropdown-body").toggle();
    });
}

jQuery(document).on("click", ".time-slot", function () {

    var time = $(this).data("time");
    var parent = $(this).parent(".dropdown-body").parent("div");
    var id = parent.attr("id");
    id = id.replace("_div", "");
    $("#" + id).val(time);
    parent.children(".selected").html("<i class='fa fa-leaf' aria-hidden='true'></i> " + time);
    $("#collection_" + id).html(time);
    if (id == "pickup_time") {
        getDeliveryDate();
    }
});