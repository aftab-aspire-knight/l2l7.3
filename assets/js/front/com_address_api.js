
var addressian_api_url = "https://api.addressian.co.uk/v2/autocomplete/";
//site_url = site_url + "index.php/";
var getAddresses = function (code) {

    var data = "";
    var type = "GET";
    var dataType = "json";
    var url = site_url + "api/getaddresses?postcode=" + code;

    code = code.replace(/\s\s+/g, ' ');

    if (code.length >= 5 && code.length <= 10) {

        $.ajax({
            url: url,
            type: type,
            data: data,
            dataType: dataType,
            cache: false,
            //crossDomain: true,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "x-api-key": "Cj6Dv0n0DR9XykzBaLpYo1oTijnK6qR255Q7E4UR",
                'Content-Type': 'application/json'
            },
            success: function (response) {

                console.log(response)
                if (response.error == false) {

                    var $dropdown = jQuery("#addresses");
                    $dropdown.empty();
                    var optionHtml = "<option value='' >Please select your address</option>";
                    $dropdown.append(optionHtml);
                    var html = "";
                    var l = "";
                    var selected = "";

                    $("#town").val(response.city);
                    //$("#postal_code").val(response.postcode);
                    jQuery.each(response.addresses, function (index, value) {

                        l = value.address;

                        if (l == address1) {
                            html += "<option selected  value='" + l + "' >" + l + "</option>";
                            getLatLng(code, function (lat, lng) {

                                showMap(lat, lng);
                                $("#location").val(lat + "," + lng);

                            });
                        } else {
                            html += "<option  value='" + l + "' >" + l + "</option>";
                        }

                    });
                    html += "<option value='-1'  >Not on the list?</option>";
                    $dropdown.append(html);
                    if (addresses == "-1") {
                        $dropdown.val("-1");
                        $("#div_reg_address1").show();
                        $("#address").val(address1);
                    }
                    $("#div_addresses").show();
                    $("#postal_code").val(code);
                }
            },
            error: function (jqXHR, error, errorThrown) {

                console.log(error)

            }
        });
    } else {
        $("#address").val("");
        $("#div_addresses").hide();
    }
}